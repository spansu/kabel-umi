<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<!--Количество новстей на главной странице-->
	<xsl:variable name="indexNews" select="3" />
	<!--Количество товаров и категорий на главной странице-->
	<xsl:variable name="indexСatalog" select="4" />
	<!--Количество фотографий и фотоальбомов на главной странице-->
	<xsl:variable name="indexPhotoalbum" select="9" />
	<!--Размер фотографий в фотоальбоме-->
	<xsl:variable name="photoW" select="212" />
	<xsl:variable name="photoH" select="140" />
	<!--Размер большой фотографии в фотоальбоме-->
	<xsl:variable name="photoFullW" select="960" />
	<!--Размер фотографий в каталоге в плите-->
	<xsl:variable name="photoCatalogW" select="372" />
	<xsl:variable name="photoCatalogH" select="126" />
	<!--Размер фотографий в галерее у обьекта каталога-->
	<xsl:variable name="photoAlbumCatalogW" select="212" />
	<xsl:variable name="photoAlbumCatalogH" select="140" />
	<!--Размер фотографий в каталоге при выводе таблицей-->
	<xsl:variable name="photoTableCatalogW" select="100" />
	<xsl:variable name="photoTableCatalogH" select="100" />
	<!--Размер большой фотографии в каталоге-->
	<xsl:variable name="photoCatalogFullW" select="235" />
	<!--Формат даты для новостей и блогов-->
	<xsl:variable name="newsDateFormat" select="'d.m.Y'" />
	<!--Формат даты для комментариев-->
	<xsl:variable name="commentDateFormat" select="'d.m.Y%20%D0%B2%20H:i'" />
	<!--Формат контактов (1-расширенная, 0-простая)-->
	<xsl:variable name="contactsForm" select="1" />
	<!--Формат вывода каталога (1-расширенный, 0-простой)-->
	<xsl:variable name="сatalog" select="1" />
	<!--Формат вывода обьектов каталога в плиткой(1-заголовок над картинкой, 0-заголовок под картинкой)-->
	<xsl:variable name="сatalogTitle" select="0" />

	<xsl:template match="/">
		<html lang="{$lang}">
			<head>
				<xsl:call-template name="header" />
			</head>
			<body>
				<xsl:call-template name="application"/>
				<xsl:apply-templates select="." mode="content"/>
				<xsl:call-template name="footer_new" />
				<xsl:call-template name="order_button"/>
				<xsl:call-template name="users_button"/>
			</body>
		</html>
	</xsl:template>

	<!-- Шаблон подвала -->
	<xsl:template name="footer_new">

		<xsl:comment>
			<div class="footer_wrapper">
				<div class="footer1 wrapper">

					<div class="footer_3block">
						<xsl:call-template name="social_block">
							<xsl:with-param name="class" select="'social_gray text'" />
							<xsl:with-param name="showTitleInBlock" select="boolean('false')" />
							<xsl:with-param name="title" select="'&share;'" />
						</xsl:call-template>
					</div>

					<div class="footer_3block">

						<xsl:call-template name="custom_block_1"/>

					</div>

					<div class="footer_3block last">

						<xsl:call-template name="copyright_footer" />

					</div>

					<div class="text code_pre code_footer" umi:element-id="{$infoPageId}" umi:field-name="code_footer" umi:empty="&empty-code-footer;">
						<xsl:apply-templates select="$infoPage/property[@name = 'code_footer']/value" mode="code"/>
					</div>
				</div>
			</div>
		</xsl:comment>


		<footer class="site-footer">
			<div class="site-footer__top site-footer__delivery">
				<div class="container">
					<div class="row">
						<div class="six columns site-footer__delivery__item wicon wicon__delivery">
							<span>Бесплатная доставка</span>
							<small>В пределах КАД</small>
						</div>
						<div class="six columns site-footer__delivery__item wicon wicon__phone">
							<span>8-800-350-00-21</span>
							<small>Бесплатно по РФ</small>
						</div>
					</div>
				</div>
			</div>
			<div class="site-footer__middle">
				<div class="container">
					<div class="row">
						<div class="three columns site-footer__middle__block">
							<h4><span>Разделы сайта</span></h4>
							<ul class="site-footer__menu">
								<li><a href="#">Главная</a></li>
								<li><a href="#">Каталог</a></li>
								<li><a href="#">О компании</a></li>
								<li><a href="#">Доставка</a></li>
								<li><a href="#">Новости</a></li>
								<li><a href="#">Статьи</a></li>
								<li><a href="#">Заявка онлайн</a></li>
								<li><a href="#">Сотрудничество</a></li>
								<li><a href="#">Вакансии</a></li>
								<li><a href="#">Расчёт загрузки</a></li>
								<li><a href="#">Справочная</a></li>
								<li><a href="#">Возврат и обмен</a></li>
								<li><a href="#">Карта сайта</a></li>
								<li><a href="#">Контакты</a></li>
							</ul>
						</div>
						<div class="three columns site-footer__middle__block">
							<div class="site-footer__soc">
								<h4><span>Мы в соц сетях</span></h4>
								<ul class="site-footer__soc__list">
									<li><a href="#" class="wicon wicon__google"></a></li>
									<li><a href="#" class="wicon wicon__tw"></a></li>
									<li><a href="#" class="wicon wicon__mail"></a></li>
									<li><a href="#" class="wicon wicon__fb"></a></li>
									<li><a href="#" class="wicon wicon__rss"></a></li>
									<li><a href="#" class="wicon wicon__vk"></a></li>
									<li><a href="#" class="wicon wicon__a"></a></li>
									<li><a href="#" class="wicon wicon__ya"></a></li>
									<li><a href="#" class="wicon wicon__pencil"></a></li>
									<li><a href="#" class="wicon wicon__ok"></a></li>
								</ul>
							</div>

							<div class="site-footer__pay">
								<h4><span>Мы принимаем</span></h4>
								<ul class="site-footer__pay__list">
									<li><a href="#" class="wicon wicon__visa"></a></li>
									<li><a href="#" class="wicon wicon__master-card"></a></li>
									<li><a href="#" class="wicon wicon__maestro"></a></li>
									<li><a href="#" class="wicon wicon__wm"></a></li>
									<li><a href="#" class="wicon wicon__pay-pal"></a></li>
									<li><a href="#" class="wicon wicon__liqpay"></a></li>
									<li><a href="#" class="wicon wicon__yad"></a></li>
									<li><a href="#" class="wicon wicon__contact"></a></li>
									<li><a href="#" class="wicon wicon__qiwi"></a></li>
									<li><a href="#" class="wicon wicon__sms"></a></li>
									<li><a href="#" class="wicon wicon__pochta-rf"></a></li>
									<li><a href="#" class="wicon wicon__rbk"></a></li>
								</ul>
							</div>

							<div class="site-footer__subscribe">
								<h4><span>Будь вкурсе</span></h4>
								<form action="" method="post" class="subscribe-form">
									<div class="subscribe-form__input-wrapper">
										<input type="text" value="" />
										<button type="submit" class="wicon wicon__email"></button>
									</div>
									<div class="subscribe-form__hint">
										Оставляя свой email адрес Вы автоматически даёте согласие на получение писем с акциями и новостями от нас.
									</div>
								</form>
							</div>
						</div>

						<div class="three columns site-footer__middle__block site-footer__offices__wrap">
							<div class="site-footer__offices">
								<h4><span>Наши адреса</span></h4>
								<div class="site-footer__offices__item">
									<div class="site-footer__offices__item-inner wicon wicon__location">
										<div class="city">г. Санкт-Петербург</div>
										<div class="address">Московское шоссе, 25к1 Б/Ц "ПРЕСТИЖ»</div>
										<a href="#">Смотреть на карте</a>
									</div>
								</div>

								<div class="site-footer__offices__item">
									<div class="site-footer__offices__item-inner wicon wicon__location">
										<div class="city">г. Москва</div>
										<div class="address">Щербинка Южная 10 офис 6</div>
										<a href="#">Смотреть на карте</a>
									</div>
								</div>

								<div class="site-footer__offices__item">
									<div class="site-footer__offices__item-inner wicon wicon__location">
										<div class="city">г. Мурманск</div>
										<div class="address">Домостроительная улица, 2</div>
										<a href="#">Смотреть на карте</a>
									</div>
								</div>
							</div>
						</div>

						<div class="three columns site-footer__middle__block">
							<div class="site-footer__contacts">
								<h4><span>Контакты</span></h4>
								<div class="site-footer__contacts__item">
									<div class="site-footer__contacts__item-inner wicon wicon__phone-white">
										<div class="phone">+ 7 (812) 309 85 60</div>
										<div class="city">г. Санкт-Петербург</div>
										<a href="mailto:info@cable-operator.ru">info@cable-operator.ru</a>
									</div>
								</div>

								<div class="site-footer__contacts__item">
									<div class="site-footer__contacts__item-inner wicon wicon__phone-white">
										<div class="phone">+ 7 (499) 703 44 56</div>
										<div class="city">г. Москва</div>
										<a href="mailto:info@cable-operator.ru">info@cable-operator.ru</a>
									</div>
								</div>

								<div class="site-footer__contacts__item">
									<div class="site-footer__contacts__item-inner wicon wicon__phone-white">
										<div class="phone">+ 7 (8152) 59 64 39</div>
										<div class="city">г. Мурманск</div>
										<a href="mailto:info@cable-operator.ru">info@cable-operator.ru</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>

	</xsl:template>
	
	<!--Шаблоны для всех страниц - контекст передается далее по модулям-->
	<xsl:template match="result" mode="content">

		<xsl:call-template name="page_header_new" />

		<xsl:comment>
		<div class="content not_main wrapper">	

			<div class="left_side main_sidebar">		
				<div class="menu catalog_items_menu">
					<xsl:variable name="id" select="document('usel://getData//catalog/category/')/udata/page/@id" />

					<xsl:apply-templates select="document(concat('udata://content/menu//3/',$id,'/'))/udata/items" mode="multilevel_menu">
						<xsl:with-param name="id" select="$id" />
					</xsl:apply-templates>
				</div>

				<div class="sidebar_banners">

					<xsl:call-template name="banner_1">
						<xsl:with-param name="html" select="0" />
						<xsl:with-param name="width" select="188" />
						<xsl:with-param name="height" select="'auto'" />
					</xsl:call-template>

					<xsl:call-template name="banner_2">
						<xsl:with-param name="html" select="0" />
						<xsl:with-param name="width" select="188" />
						<xsl:with-param name="height" select="'auto'" />
					</xsl:call-template>

					<xsl:call-template name="banner_3">
						<xsl:with-param name="html" select="0" />
						<xsl:with-param name="width" select="188" />
						<xsl:with-param name="height" select="'auto'" />
					</xsl:call-template>

				</div>

			</div>

			<div class="right_side main_content_place">

				<xsl:apply-templates select="." mode="breadcrumb"/>
				<xsl:apply-templates select="document('udata://system/listErrorMessages/')/udata" />
				<xsl:apply-templates select="." mode="main_top_date"/>
				<xsl:apply-templates select="." mode="h1"/>	
					
				<xsl:apply-templates select="." />
				<xsl:apply-templates select="document('udata://system/listErrorMessages/')/udata" />

			</div>

			<br clear="all"/>					

			<xsl:call-template name="seo_text"/>
		</div>
		</xsl:comment>

		<xsl:apply-templates select="." />


		

	</xsl:template>
	
	<!--Шаблон контента для страницы по умолчанию-->
	<xsl:template match="result[page/@is-default]" mode="content">

		<xsl:call-template name="page_header_new" />
		<xsl:comment>
		<div class="main">
				
			<div class="main_image_wrapper">

				<div class="main_image">
					<div class="main_image_left_side">

						<xsl:call-template name="banner_1">
							<xsl:with-param name="html" select="0" />
							<xsl:with-param name="width" select="'566'" />
							<xsl:with-param name="height" select="'350'" />
						</xsl:call-template>

					</div>
					<div class="main_image_right_side">

						<xsl:call-template name="banner_2">
							<xsl:with-param name="html" select="0" />
							<xsl:with-param name="width" select="372" />
							<xsl:with-param name="height" select="164" />
						</xsl:call-template>

						<xsl:call-template name="banner_3">
							<xsl:with-param name="html" select="0" />
							<xsl:with-param name="width" select="372" />
							<xsl:with-param name="height" select="164" />
						</xsl:call-template>

					</div>
					<br clear="all"/>
				</div>

				<xsl:apply-templates select="document('usel://getData//catalog/category/')/udata" mode="only_special_offers_index"/>

			</div>

		</div>

		<div class="content wrapper">


			<div class="news_on_main_wrapper">
				<xsl:apply-templates select="document('usel://getData//news/rubric/')/udata" mode="news_index"/>	
			</div>

			<xsl:apply-templates select="." mode="h1"/>
			<xsl:apply-templates select="."/>


			<xsl:call-template name="seo_text"/>
		</div>

		</xsl:comment>

		<section class="site-content">
			<xsl:call-template name="promo_block" />
			<xsl:call-template name="products_list" />
			<xsl:call-template name="categories_list" />
			<xsl:call-template name="work_shcheme" />
			<xsl:call-template name="on_trust" />

			<xsl:apply-templates select="document('usel://getData//news/rubric/')/udata" mode="publications_index"/>

			<xsl:call-template name="reply_form" />
			<xsl:call-template name="partners" />
			<xsl:call-template name="about_block" />
		</section>

	</xsl:template>

	<!--Шаблон контента для хедера-->
	<xsl:template name="page_header">
		<div class="header_top_line">
			<div class="wrapper">

				<div class="left_side">
					<div class="menu">
						<xsl:apply-templates select="document('udata://content/menu//1/')/udata" mode="main_menu"/>
					</div>
				</div>

				<div class="right_side">
					<xsl:call-template name="header_basket"/>
				</div>

				<br clrar="all"/>

			</div>

		</div>
		<div class="header wrapper">
			<div class="left_side">
				<div class="title_block">
					<a href="/" class="logo">
						<xsl:call-template name="makeThumbnailSqueeze">
							<xsl:with-param name="element_id" select="$infoPageId" />
							<xsl:with-param name="field_name" select="'logo'" />
							<xsl:with-param name="width" select="'68'" />
							<xsl:with-param name="alt" select="result/@title" />
						</xsl:call-template>
					</a>
					<a class="title_company h1" href="/" >
						<span class="main_sitetitle" umi:element-id="{$infoPageId}" umi:empty="&empty-fio;" umi:field-name="imya_i_familiya">
							<xsl:apply-templates select="$infoPage/property[@name = 'imya_i_familiya']" />
						</span>
						<br/>
						<span class="main_siteslogan" umi:element-id="{$infoPageId}" umi:field-name="professiya_ili_slogan" umi:empty="&empty-content;">
							<xsl:apply-templates select="$infoPage/property[@name = 'professiya_ili_slogan']" />
						</span>										
					</a>
				</div>
			</div>
			<div class="right_side">
				<div class="phone_block text" umi:element-id="{$infoPageId}">

					<div class="phone">
						<span class="phone_prefix" umi:field-name="kod_goroda" umi:empty="&empty;">
							<xsl:apply-templates select="$infoPage/property[@name = 'kod_goroda']" />
						</span>
						<span class="phone_number" umi:field-name="telefon" umi:empty="&empty;">
							<xsl:apply-templates select="$infoPage/property[@name = 'telefon']" />
						</span>
					</div>
					<div class="workhours" umi:element-id="{$infoPageId}" umi:field-name="rezhim_raboty" umi:empty="&empty-worktime;">
						<xsl:apply-templates select="$infoPage/property[@name = 'rezhim_raboty']" />
					</div>
													
				</div>
				<div class="contacts text">

					<xsl:apply-templates select="document('udata://search/insert_form/')/udata">
						<xsl:with-param name="class" select="'search2'" />
					</xsl:apply-templates>

				</div>
			</div>	
			
			<br clear="all"/>			
						

		</div>

		<div class="menu catalog_menu wrapper">
			<xsl:variable name="id" select="document('usel://getData//catalog/category/')/udata/page/@id" />

			<xsl:apply-templates select="document(concat('udata://content/menu///',$id,'/'))/udata" mode="main_menu">
				<xsl:with-param name="id" select="$id" />
			</xsl:apply-templates>
		</div>
	
	</xsl:template>


	<xsl:template name="page_header_new">
		<header class="site-header">
			<div class="site-header__top collapsed">

				<div class="container">

					<div class="site-header__top__block site-header__schedule myicon myicon__schedule">
						<span class="site-header__schedule__days site-header__hlight">Пн.-Птн.:</span>
						<span class="site-header__schedule__time">с 09:00 до 18:00</span>
					</div>

					<a href="#" class="site-header__top__block site-header__region myicon myicon__map-marker">
						<span class="site-header__region__caption site-header__hlight">Ваш регион:</span>
						<span class="site-header__region__name">Санкт-Петербург</span>
					</a>

					<div class="site-header__top__block site-header__search">
						<form method="post" action="" class="site-header__search__form">
							<input class="site-header__search__input" type="text" name="search" value="" placeholder="Поиск..." />
							<button class="site-header__search__btn myicon myicon__search" type="submit"></button>
						</form>
					</div>

					<a href="#" class="site-header__top__block site-header__basket myicon myicon__basket">
						<span class="site-header__basket__caption">Товара</span>
						<span class="site-header__basket__products-count">(0)</span>
					</a>

					<a href="#" class="site-header__top__block site-header__signin myicon myicon__signin">
						<span class="site-header__signin__caption">Войти</span>
					</a>

					<a href="#" class="site-header__top__menu"></a>

				</div>

			</div>
			<div class="site-header__middle">
				<div class="container">
					<a href="/" class="site-header__middle__block site-header__logo">Открытый кабельный портал</a>

					<div class="site-header__middle__block site-header__intro">
						<p>
							Оптовые поставки кабельно-проводниковой<br />
							продукции и электротехнических материалов<br />
							на территории России и стран СНГ
						</p>
					</div>

					<div class="site-header__middle__block site-header__contacts">
						<div class="site-header__email myicon myicon__email">info@cable-operator.ru</div>
						<div class="site-header__phone"><span class="city">СПб:</span> <span class="phone-number">+7 (812) 309-85-60</span></div>
						<div class="site-header__phone"><span class="city">Мск.:</span> <span class="phone-number">+7 (499) 703-44-56</span></div>
						<div class="site-header__phone"><span class="city">Мур.:</span> <span class="phone-number">+7 (815) 259-64-39</span></div>
						<button class="site-header__callback myicon myicon__callback">Вам перезвонить?</button>
					</div>
				</div>
			</div>
			<div class="site-header__bottom">
				<div class="container">
					<xsl:apply-templates select="document('udata://content/menu//1/')/udata" mode="main_menu_new"/>
				</div>
			</div>
		</header>
	</xsl:template>

	<!--Шаблон промо-блока-->
	<xsl:template name="promo_block">

		<div class="promo-block">
			<div class="container">
				<div class="row">
					<div class="twelve column">
						<div class="promo-block__slider-wrapper">
							<div class="promo-block__slider">
								<div class="promo-block__slider__slide">
									<h3>
										<span>У Вас еще нет кабеля?</span><br xmlns=""/>тогда мы идём к Вам!
									</h3>
									<a href="#" class="promo-block__slider__more">Подробнее</a>
								</div>
								<div class="promo-block__slider__slide">
									<h3>
										<span>У Вас еще нет кабеля?</span><br xmlns=""/>тогда мы идём к Вам!
									</h3>
									<a href="#" class="promo-block__slider__more">Подробнее</a>
								</div>
								<div class="promo-block__slider__slide">
									<h3>
										<span>У Вас еще нет кабеля?</span><br xmlns=""/>тогда мы идём к Вам!
									</h3>
									<a href="#" class="promo-block__slider__more">Подробнее</a>
								</div>
							</div>
							<a href="javascript:void(0)" class="promo-block__slider__arrow promo-block__slider__arrow-left fa fa-angle-left"></a>
							<a href="javascript:void(0)" class="promo-block__slider__arrow promo-block__slider__arrow-right fa fa-angle-right"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<!--Шаблон выгодное предложение-->
	<xsl:template name="products_list">
		<div class="products-list__wrapper">
			<div class="container">
				<h3><span>Выгодное предложеие</span></h3>
				<div class="products-list">
					<div class="products-list__decorator products-list__decorator-top"></div>
					<div class="products-list__decorator products-list__decorator-bottom"></div>
					<div class="products-list__inner">
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
						<div class="products-list__item">
							<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
							<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
							<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
							<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
							<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
						</div>
					</div>
				</div>
				<div class="products-list__more-wrap">
					<button class="products-list__more-btn"><span>показать еще +12</span></button>
				</div>
			</div>
		</div>
	</xsl:template>

	<!--Шаблон СОПУТСТВУЮЩИЕ КАТЕГОРИИ-->
	<xsl:template name="categories_list">

		<div class="categories-list__wrapper">
			<div class="container">
				<h3><span>Сопутствующие категории</span></h3>
				<div class="categories-list">
					<div class="categories-list__inner">
						<div class="categories-list__item">
							<div class="categories-list__item__header">Кабельные муфты</div>
							<div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
							<div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
							<div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
						</div>
						<div class="categories-list__item">
							<div class="categories-list__item__header">Сетевое<br />оборудование</div>
							<div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
							<div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
							<div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
						</div>
						<div class="categories-list__item">
							<div class="categories-list__item__header">Кабельные муфты</div>
							<div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
							<div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
							<div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
						</div>
						<div class="categories-list__item">
							<div class="categories-list__item__header">Кабельные муфты</div>
							<div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
							<div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
							<div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
						</div>
						<div class="categories-list__item">
							<div class="categories-list__item__header">Кабельные муфты</div>
							<div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
							<div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
							<div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
						</div>
						<div class="categories-list__item">
							<div class="categories-list__item__header">Кабельные муфты</div>
							<div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
							<div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
							<div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
						</div>
						<div class="categories-list__item">
							<div class="categories-list__item__header">Кабельные муфты</div>
							<div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
							<div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
							<div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
						</div>
					</div>
					<a href="javascript:void(0)" class="categories-list__arrow categories-list__arrow-left fa fa-angle-left"></a>
					<a href="javascript:void(0)" class="categories-list__arrow categories-list__arrow-right fa fa-angle-right"></a>
				</div>
			</div>
		</div>

	</xsl:template>

	<!-- Шаблон Наша схема работы -->
	<xsl:template name="work_shcheme">
		<div class="work-shcheme__wrapper">
			<div class="container">
				<h3><span>Наша схема работы</span></h3>
				<div class="work-shcheme__inner">
					<p>Деятельность нашей компании ООО "Открытый Кабельный Портал", направлена на оптовые поставки кабельно-проводниковой продукции и электротехнических материалов на территории России и стран СНГ. Работая с нами, Вы тратите меньше - с нами выгодно.</p>
					<img src="/img/new/work-scheme.jpg" alt="" />
				</div>
			</div>
		</div>
	</xsl:template>

	<!-- Шаблон Нам доверяют -->
	<xsl:template name="on_trust">

		<div class="on-trust__wrapper">
			<div class="container">
				<h3><span>Нам доверяют</span></h3>
				<div class="on-trust">
					<div class="on-trust__inner">
						<div class="on-trust__item">
							<div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
							<div class="on-trust__item__header">
								<span>ООО “ПерспективаЭллектро”</span>
								Сидоров Н.А., директор
							</div>
						</div>
						<div class="on-trust__item">
							<div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
							<div class="on-trust__item__header">
								<span>ООО “ПерспективаЭллектро”</span>
								Сидоров Н.А., директор
							</div>
						</div>
						<div class="on-trust__item">
							<div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
							<div class="on-trust__item__header">
								<span>ООО “ПерспективаЭллектро”</span>
								Сидоров Н.А., директор
							</div>
						</div>
						<div class="on-trust__item">
							<div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
							<div class="on-trust__item__header">
								<span>ООО “ПерспективаЭллектро”</span>
								Сидоров Н.А., директор
							</div>
						</div>
						<div class="on-trust__item">
							<div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
							<div class="on-trust__item__header">
								<span>ООО “ПерспективаЭллектро”</span>
								Сидоров Н.А., директор
							</div>
						</div>
						<div class="on-trust__item">
							<div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
							<div class="on-trust__item__header">
								<span>ООО “ПерспективаЭллектро”</span>
								Сидоров Н.А., директор
							</div>
						</div>
						<div class="on-trust__item">
							<div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
							<div class="on-trust__item__header">
								<span>ООО “ПерспективаЭллектро”</span>
								Сидоров Н.А., директор
							</div>
						</div>
					</div>
					<a href="javascript:void(0)" class="on-trust__arrow on-trust__arrow-left fa fa-angle-left"></a>
					<a href="javascript:void(0)" class="on-trust__arrow on-trust__arrow-right fa fa-angle-right"></a>
				</div>
			</div>
		</div>

	</xsl:template>


	<!-- Шаблон напигите нам -->
	<xsl:template name="reply_form">

		<div class="reply__wrapper">
			<div class="container">
				<h3><span>Напишите нам</span></h3>
				<div class="reply-form">
					<div class="reply__discount-msg">
						<p>Хотите получить скидку в <span>5%</span>? - оставьте заявку онлайн<br />
							и мы подготовим для Вас индивидуальное, выгодное предложение!</p>
					</div>
					<form method="post" action="">
						<div class="row">
							<div class="four columns">
								<div class="reply-form__input-wrapper"><input type="text" placeholder="Ваше имя" value="" /></div>
								<div class="reply-form__input-wrapper"><input type="text" placeholder="Ваше email" value="" /></div>
								<div class="reply-form__input-wrapper"><input type="text" placeholder="Ваше телефон" value="" /></div>
							</div>
							<div class="four columns">
								<div class="reply-form__input-wrapper"><textarea placeholder="Введите текст Вашего сообщения..."></textarea></div>
							</div>
							<div class="four columns">
								<div class="reply-form__input-wrapper">
									<div class="reply-form__file-input-wrapper"><input type="file" value="" /></div>
									<div class="reply-form__hint">
										Вы можете прикрепть файл не более 10 Мб,<br />
										формата  .jpg, .png. .doc. .exls, .pdf, .rar, .zip
									</div>
								</div>
								<div class="reply-form__submit-wrapper">
									<button type="submit" class="reply-form__submit-btn"><span>Отправить</span></button>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="twelve columns">
								<div class="reply-form__hint tex">
									<span>*</span> Мы не распространяем Ваши данные третьим лицам и не используем Ваши конткты для различных рассылок и т.д.
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

	</xsl:template>

	<xsl:template name="partners">

		<div class="partners__wrapper">
			<div class="container">
				<h3><span>Наши партнеры</span></h3>
				<div class="partners__inner">
					<div class="partners__slider">
						<div class="partners__slider__inner">
							<div class="partners__slider__item"><img src="/img/new/partner_amk.png" alt="" /></div>
							<div class="partners__slider__item"><img src="/img/new/partner_legrand.png" alt="" /></div>
							<div class="partners__slider__item"><img src="/img/new/partner_rubinsk.png" alt="" /></div>
							<div class="partners__slider__item"><img src="/img/new/partner_sevkabel.png" alt="" /></div>
							<div class="partners__slider__item"><img src="/img/new/partner_tatkabel.png" alt="" /></div>

							<div class="partners__slider__item"><img src="/img/new/partner_amk.png" alt="" /></div>
							<div class="partners__slider__item"><img src="/img/new/partner_legrand.png" alt="" /></div>
							<div class="partners__slider__item"><img src="/img/new/partner_rubinsk.png" alt="" /></div>
							<div class="partners__slider__item"><img src="/img/new/partner_sevkabel.png" alt="" /></div>
							<div class="partners__slider__item"><img src="/img/new/partner_tatkabel.png" alt="" /></div>
						</div>
						<a href="javascript:void(0)" class="partners__slider__arrow partners__slider__arrow-left fa fa-angle-left"></a>
						<a href="javascript:void(0)" class="partners__slider__arrow partners__slider__arrow-right fa fa-angle-right"></a>
					</div>
				</div>
			</div>
		</div>

	</xsl:template>

	<xsl:template name="about_block">
		<div class="about__wrapper">
			<div class="container">
				<h3><span>О нас</span></h3>
				<div class="about__inner">
					<div class="about__text">
						<img class="about__img" src="/img/new/about_img.jpg" alt="" />
						<p>
						Компания <strong>Открытый Кабельный Портал</strong> занимается комплексными оптовыми поставками кабельной продукции для предприятий России, Белоруссии, Украины и Казахстана. Мы поставляем силовой, слаботочный, волоконно-оптический, коаксиальный кабель, полный спектр электротехнической продукции (светильники, щиты, автоматы, переключатели). Осуществляем сборку щитов.
						</p>
						<p>
							Нашими поставщиками являются кабельные заводы Российской Федерации и стран СНГ (РыбинскКабель,  ИркутскКабель, ПсковКабель, ТатКабель, Паритет, СПКБ, СпецКабель, Арсенал, СпецКабель, СПКБ, и некоторые небольшие производства).
						</p>
						<p>
							У нас есть кабель в наличии, если же вам потребуется что-то особенное, то всегда можно оформить заказ. Благодаря налаженным связям и длительным партнерским отношениям, Открытый Кабельный Портал имеет дилерские скидки на заводах, а следовательно и отличные цены для наших партнеров.Нашим клиентами являются электромонтажные, строительные организации, провайдеры сети Интернет, а также торговые организации. Комплексная поставка на объекты нефте-газовой промышленности. Монтажным организациям по всей территории страны.
						</p>
						<p>
							Главная цель нашей компании, это снабжение и логистика, на предприятиях и организациях по территории России, в таких городах как: Москва, Санкт-Петербург, Воронеж, Курск, Белгород, Тула, Архангельск, Калининград, Мурманск, Ростов, Ставрополь, Краснодар, Волгоград, Пермь, республика Татарстан, Саратов, республика Башкортостан, Оренбург, Самара, Свердловск, Астрахань, Челябинск, Тюмень, Томск, Новосибирск, Кемерово, Хабаровск, Новый Уренгой. Мы предлагаем оптовую цену, кабель из наличия, доставку кабеля в регионы страны.
						</p>
						<p>
							Если Вам нужен кабель оптом или оптовая цена, то Вы можете обратиться в наш отдел продаж, наши специалисты проконсультируют по подбору нужной продукции и мы знаем как важны сроки поставки, поэтому никогда не подводим своих клиентов. А тот факт, что наши цены ниже чем у конкурентов, делает наших клиентов самыми счастливыми людьми на свете.
						</p>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	
</xsl:stylesheet>
