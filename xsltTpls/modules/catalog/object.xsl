<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<xsl:template match="result[@module = 'catalog'][@method = 'object']">
		<xsl:variable name="old_price" select="//property[@name = 'old_price']/value" />
		<xsl:variable name="price" select="//property[@name = 'price']/value" />
		<xsl:variable name="catalog_option_props" select="//group[@name = 'catalog_option_props']" />

		<div class="product__wrapper">
			<div class="container">
				<div class="row">
					<div class="twelve columns">
						<div class="breadcrumbs">
							<a href="#">Главная</a>
							<a href="#">Каталог</a>
							<a href="#">Витая пара</a>
							<a href="#">Кабель</a>
							<a href="#">UTP 4 пары</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="three columns product__side-menu">
						<h3>Кабели и провода</h3>
						<ul class="product__side-menu__list">
							<li><a href="#">(А)ПвБШв</a></li>
							<li><a href="#">(А)ПвБШп</a></li>
							<li><a href="#">(А)ПвБШп(Г)</a></li>
							<li><a href="#">(А)ПвВнг(А)-LS</a></li>
							<li><a href="#">(А)ПвВнг(В)-LS</a></li>
							<li><a href="#">(А)ПвПг NUM NYM</a></li>
							<li><a href="#">ААБл</a></li>
							<li><a href="#">ААШв</a></li>
							<li><a href="#">АВБбШв</a></li>
							<li><a href="#">АВБбШвнг</a></li>
							<li><a href="#">АВБбШвнг(А)</a></li>
							<li><a href="#">АВБбШвнг(А)-LS</a></li>
							<li><a href="#">АВВГ АВВГнг(А)</a></li>
							<li><a href="#">АВВГнг(А)-LS</a></li>
							<li><a href="#">АПвПуг</a></li>
							<li><a href="#">АСБГ</a></li>
							<li><a href="#">АСБл</a></li>
							<li><a href="#">ВВГ</a></li>
							<li><a href="#">ВВГнг</a></li>
							<li><a href="#">ВВГнг-LS</a></li>
							<li><a href="#">ВВГнг(А)</a></li>
							<li><a href="#">ВВГнг(А)</a></li>
							<li><a href="#">ВВГнг(А)-LS</a></li>
							<li><a href="#">ВВГнг(А)-FRLS</a></li>
							<li><a href="#">ВВГнг(А)-LS</a></li>
							<li><a href="#">ВВГЭнг</a></li>
							<li><a href="#">ВВГЭнг(А)-FRLS</a></li>
							<li><a href="#">Кабель ВБбШв</a></li>
							<li><a href="#">Кабель ВБбШвнг</a></li>
							<li><a href="#">Кабель ВБбШвнг(А)</a></li>
							<li><a href="#">Кабель ВБбШвнг(А)-FRLS</a></li>
							<li><a href="#">Кабель ВБбШвнг(А)-LS</a></li>
							<li><a href="#">Кабель ВКбШвнг(А)-LS</a></li>
							<li><a href="#">КГ</a></li>
							<li><a href="#">КГ-ХЛ</a></li>
							<li><a href="#">КГН</a></li>
							<li><a href="#">ПвБШв</a></li>
							<li><a href="#">ПвБШп</a></li>
							<li><a href="#">ППГнг-HF</a></li>
							<li><a href="#">ППГнг(А)-FRHF</a></li>
							<li><a href="#">ППГнг(А)-HF</a></li>
						</ul>
					</div>
					<div class="nine columns product__content__wrapper">
						<h1><span>Кабель КВВГНГ-FRLS 37*1,5</span></h1>
						<div class="product__content__inner">
							<div class="product__info">
								<div class="row">
									<div class="six columns product-img__wrapper">
										<div class="product-img"><img src="/img/new/product_card.jpg" alt="" /></div>
									</div>
									<div class="six columns product-descr__wrapper">
										<div class="product-descr">
											<div class="product-available__wrapper">
												<div class="product-available__header">Наличие на складе <strong>КВВГНГ-FRLS 37*1,5</strong></div>
												<div class="product-available not-available">Есть в наличии!</div>
											</div>

											<div class="product-basket__wrapper">
												<form method="post" action="" class="product-basket__form">
													<div class="row">
														<div class="eight columns">
															<div class="product-basket__header"><small>цена</small> КВВГНГ-FRLS 37*1,5 с НДС</div>
															<div class="product-price__wrapper">
																<div class="product-price">477,00 &#8381;</div>
																<small>За метр погон.</small>
															</div>

														</div>
														<div class="four columns">
															<div class="product-basket__buttons-wrapper">
																<button class="onclick-by__btn">Купить в один клик</button>
																<button class="product-basket__btn"><span>В корзину</span></button>

																<div class="product-count__wrapper">
																	<div class="product-count__input__wrapper">
																		<a href="#">-</a><input type="text" value="1" /><a href="#">+</a>
																		<div class="units"><small>метр погон.</small></div>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</form>
											</div>

											<div class="product__hint">
												<strong>Уважаемые посетители!</strong>
												<p>Стоимость товара может варьироваться в зависимости от курса евро или доллара. Пожалуйста, уточняйте актуальную стоимость у наших менеджеров.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="product-tabs__wrapper">
								<div class="product-tabs__links">
									<ul>
										<li class="active"><a href="#" data-tab="descr"><span>Описание</span></a></li>
										<li><a href="#" data-tab="characteristics"><span>Характеристики</span></a></li>
										<li><a href="#" data-tab="billing"><span>Оплата</span></a></li>
										<li><a href="#" data-tab="delivery"><span>Доставка</span></a></li>
									</ul>
								</div>
								<div class="product-tabs__content">
									<div class="product-tabs__item active" data-tab="descr">
										<p>
											<strong>Описание и расшифровка кабеля КВВГнг-FRLS 37х1,5:</strong><br />
											<strong>К</strong> - Кабель контрольный<br />
											<strong>В</strong> - Изоляция жил из поливинилхлоридного пластиката<br />
											<strong>В</strong> - Оболочка из поливинилхлоридного пластиката<br />
											<strong>Г</strong> - Отсутствие защитных покровов<br />
											<strong>нг-LS</strong> - Изоляция жил и оболочка из поливинилхлоридного пластиката пониженной горючести с пониженным газо- дымовыделением<br />
											<strong>FR</strong> - наличие термического барьера в виде обмотки проводника двумя слюдосодержащими лентами<br />
										</p>
										<p>
											<strong>Элементы конструкции кабеля КВВГнг-FRLS 37х1,5:</strong>
										</p>
										<ol>
											<li>Токопроводящая жила – из медной проволоки.</li>
											<li>Термический барьер - из слюдосодержащей ленты.</li>
											<li>Изоляция – из поливинилхлоридного пластиката пониженной пожароопасности.</li>
											<li>Внутренняя оболочка – из поливинилхлоридного пластиката пониженной пожароопасности.</li>
											<li>Разделительный слой из поливинилхлоридного пластиката пониженной пожароопасности.</li>
											<li>Экран - в виде обмотки из медной фольги или медной ленты номинальной толщиной не менее 0,06 мм с перекрытием не менее 30%,обеспечивающим сплошность экрана при допустимых радиусах изгиба кабеля.</li>
											<li>Наружная оболочка - из поливинилхлоридного пластиката пониженной пожароопасности.<br />
												Контрольные кабели имеют цифровую или цветную маркировку всех изолированных жил, обеспечивающую возможность идентификации каждой жилы при монтаже.</li>
										</ol>
									</div>
									<div class="product-tabs__item" data-tab="characteristics">characteristics</div>
									<div class="product-tabs__item" data-tab="billing">billing</div>
									<div class="product-tabs__item" data-tab="delivery">delivery</div>
								</div>
							</div>

							<div class="other-products">
								<h4><span>Другие модификации провода КВВГНГ-FRLS в нашем каталоге продукции</span></h4>
								<table class="other-products__table">
									<thead>
										<tr>
											<th>Наименование позиции</th>
											<th>Цена за пог. м</th>
											<th>Количество, м</th>
											<th>Заказать</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>КВВГНГ 2Х0.75</td>
											<td>9.56 руб.</td>
											<td>1</td>
											<td>
												<button class="add-to-order__btn">Добавить в заказ</button>
											</td>
										</tr>
										<tr>
											<td>КВВГНГ 2Х0.75</td>
											<td>9.56 руб.</td>
											<td>1</td>
											<td>
												<button class="add-to-order__btn">Добавить в заказ</button>
											</td>
										</tr>
										<tr>
											<td>КВВГНГ 2Х0.75</td>
											<td>9.56 руб.</td>
											<td>1</td>
											<td>
												<button class="add-to-order__btn">Добавить в заказ</button>
											</td>
										</tr>
										<tr>
											<td>КВВГНГ 2Х0.75</td>
											<td>9.56 руб.</td>
											<td>1</td>
											<td>
												<button class="add-to-order__btn">Добавить в заказ</button>
											</td>
										</tr>
										<tr>
											<td>КВВГНГ 2Х0.75</td>
											<td>9.56 руб.</td>
											<td>1</td>
											<td>
												<button class="add-to-order__btn">Добавить в заказ</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="viewed-products">
				<div class="container">
					<h3><span>Вы смотрели</span></h3>
					<div class="viewed-products__list">
						<div class="viewed-products__inner">
							<?php for($i=0;$i<8;$i++){ ?>
							<div class="viewed-products__list__item">
								<div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
								<div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
								<div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
								<div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
								<div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
							</div>
							<?php } ?>
						</div>
						<a href="javascript:void(0)" class="viewed-products__list__arrow viewed-products__list__arrow-left fa fa-angle-left"></a>
						<a href="javascript:void(0)" class="viewed-products__list__arrow viewed-products__list__arrow-right fa fa-angle-right"></a>
					</div>
				</div>
			</div>


			<div class="partners__wrapper">
				<div class="container">
					<h3><span>Наши партнеры</span></h3>
					<div class="partners__inner">
						<div class="partners__slider">
							<div class="partners__slider__inner">
								<div class="partners__slider__item"><img src="/img/new/partner_amk.png" alt="" /></div>
								<div class="partners__slider__item"><img src="/img/new/partner_legrand.png" alt="" /></div>
								<div class="partners__slider__item"><img src="/img/new/partner_rubinsk.png" alt="" /></div>
								<div class="partners__slider__item"><img src="/img/new/partner_sevkabel.png" alt="" /></div>
								<div class="partners__slider__item"><img src="/img/new/partner_tatkabel.png" alt="" /></div>

								<div class="partners__slider__item"><img src="/img/new/partner_amk.png" alt="" /></div>
								<div class="partners__slider__item"><img src="/img/new/partner_legrand.png" alt="" /></div>
								<div class="partners__slider__item"><img src="/img/new/partner_rubinsk.png" alt="" /></div>
								<div class="partners__slider__item"><img src="/img/new/partner_sevkabel.png" alt="" /></div>
								<div class="partners__slider__item"><img src="/img/new/partner_tatkabel.png" alt="" /></div>
							</div>
							<a href="javascript:void(0)" class="partners__slider__arrow partners__slider__arrow-left fa fa-angle-left"></a>
							<a href="javascript:void(0)" class="partners__slider__arrow partners__slider__arrow-right fa fa-angle-right"></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<xsl:comment>
		<!--Краткое описание-->
		<xsl:if test="$сatalog != 3">
			<div class="catalog_object_prefix catalog_object_prefix_bottom" umi:field-name="prefix" umi:field-type="string" umi:empty="&empty-prefix;" umi:element-id="{$pageId}">
				<xsl:apply-templates select="//property[@name = 'prefix']" />
			</div>
		</xsl:if>
		<div class="object" umi:element-id="{$pageId}">
			<!--Изображение-->
			<xsl:if test="$сatalog != 2 and $сatalog != 3">
				<div class="object_image">
					<a class="object_image_a fancybox-group" title="{page/name}" href="{//property[@name = 'photo']/value}" rel="{page/@alt-name}" umi:url-attribute="href">
						<xsl:if test="not(//property[@name = 'photo']/value)">
							<xsl:attribute name="href">&empty-photo;</xsl:attribute>
						</xsl:if>
						<xsl:call-template name="makeThumbnailSqueeze">
							<xsl:with-param name="element_id" select="$pageId" />
							<xsl:with-param name="field_name" select="'photo'" />
							<xsl:with-param name="width" select="$photoCatalogFullW" />
							<xsl:with-param name="alt" select="//property[@name = 'h1']/value" />
						</xsl:call-template>
					</a>
				</div>
			</xsl:if>
			<!--Конец-->
			
			<xsl:if test="$isAdmin = 1">
				<div class="hidden catalog-info">
					<span class="hidden-text middle">
						<span>&catalog-type;: </span>
						<span>
							<xsl:value-of select="document(concat('utype://',page/@type-id))/udata/type/@title" disable-output-escaping="yes"/>
						</span>
					</span>
				</div>
				<xsl:call-template name="special_offer" />
			</xsl:if>

			<!--Цена-->
			<xsl:if test="$сatalog = 1">
				<div class="price price_top">
					<span class="price_title" umi:element-id="{$infoPageId}" umi:empty="&empty-price-title;" umi:field-name="catalog_price_title" umi:field-type="string">
						<xsl:apply-templates select="$infoPage/property[@name = 'catalog_price_title']" />
					</span>
					<span class="price price_value" umi:field-name="price" umi:field-type="price" umi:empty="&empty-price;" umi:element-id="{$pageId}">
						<xsl:apply-templates select="//property[@name = 'price']" />
					</span>
					<span class="currency" umi:element-id="{$infoPageId}" umi:empty="&empty-currency;" umi:field-name="currency" umi:field-type="string" >
						<xsl:apply-templates select="$infoPage/property[@name = 'currency']" />
					</span>
				</div>
			</xsl:if>
			<!--Конец-->
            
			<!--Старая цена-->
			<xsl:if test="$сatalog = 1">
				<xsl:if test="($isAdmin = 1) or ($price &lt; $old_price)">
					<div>
						<xsl:attribute name="class">
							<xsl:text>price price_top old_price</xsl:text>
							<xsl:if test = "$price &gt;= $old_price">
								<xsl:text> hidden</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<span class="wrapper_for_old_price">
							<hr class="strike"/>
							<span class="price_title" umi:element-id="{$infoPageId}" umi:empty="&empty-price-title;" umi:field-name="catalog_old_price_title" umi:field-type="string">
								<xsl:apply-templates select="$infoPage/property[@name = 'catalog_old_price_title']" />
							</span>
							<xsl:text> </xsl:text>
							<span class="price" umi:field-name="old_price" umi:field-type="price" umi:empty="&empty-price;" umi:element-id="{$pageId}">
								<xsl:apply-templates select="//property[@name = 'old_price']" />
							</span>
							<xsl:text> </xsl:text>
							<span class="currency" umi:element-id="{$infoPageId}" umi:empty="&empty-currency;" umi:field-name="currency" umi:field-type="string" >
								<xsl:apply-templates select="$infoPage/property[@name = 'currency']" />
							</span>
						</span>
					</div>
				</xsl:if>
			</xsl:if>
			<!--Конец-->
						
			<!-- Опциональные Свойства -->
			<xsl:choose>
				<xsl:when test="$isAdmin = 1">
					<xsl:apply-templates select="document(concat('upage://', $pageId,'?show-empty'))//group[@name = 'catalog_option_props']" mode="catalog_opt_props" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="$catalog_option_props" mode="catalog_opt_props"/>
				</xsl:otherwise>
			</xsl:choose>
			
			<!--Кнопка-->
			<xsl:if test="$сatalog = 1">
				<div class="buy_button">
					<xsl:choose>
						<xsl:when test="//property[@name='common_quantity']/value &gt; 0">
							<a class="button_buy" id="add_basket_{$pageId}" href="/emarket/basket/put/element/{$pageId}/?redirect-uri=/emarket/cart/" umi:element-id="{$infoPageId}" umi:field-name="buy_title" umi:field-type="string" umi:empty="&empty-buy-title;">
								<xsl:value-of select="$infoPage/property[@name = 'buy_title']/value" />
							</a>
						</xsl:when>
						<xsl:otherwise>
							<a class="button_buy button_buy_order" id="add_basket_{$pageId}" href="/emarket/basket/put/element/{$pageId}/?redirect-uri=/emarket/cart/" umi:element-id="{$infoPageId}" umi:field-name="title_order_link" umi:field-type="string" umi:empty="&empty-order-title;">
								<xsl:value-of select="$infoPage/property[@name = 'title_order_link']/value" />
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</xsl:if>
			<!--Конец-->
			
			<!--Цена-->
			<xsl:if test="$сatalog = 1 or $сatalog = 3">
				<div class="price price_bottom">
					<span class="price_title" umi:element-id="{$infoPageId}" umi:empty="&empty-price-title;" umi:field-name="catalog_price_title" umi:field-type="string">
						<xsl:apply-templates select="$infoPage/property[@name = 'catalog_price_title']" />
					</span>
					<xsl:text> </xsl:text>
					<span class="price  price_value" umi:field-name="price" umi:field-type="price" umi:empty="&empty-price;" umi:element-id="{$pageId}">
						<xsl:apply-templates select=".//property[@name = 'price']" />
					</span>
					<xsl:text> </xsl:text>
					<span class="currency" umi:element-id="{$infoPageId}" umi:empty="&empty-currency;" umi:field-name="currency" umi:field-type="string" >
						<xsl:apply-templates select="$infoPage/property[@name = 'currency']" />
					</span>
				</div>
			</xsl:if>
			<!--Конец-->
            
			<!--Старая цена-->
			<xsl:if test="$сatalog = 1 or $сatalog = 3">
				<xsl:if test="($isAdmin = 1) or ($price &lt; $old_price)">
					<div>
						<xsl:attribute name="class">
							<xsl:text>price price_bottom old_price</xsl:text>
							<xsl:if test = "$price &gt; $old_price">
								<xsl:text> hidden</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<span class="wrapper_for_old_price">
							<hr class="strike" />
							<span class="price_title" umi:element-id="{$infoPageId}" umi:empty="&empty-price-title;" umi:field-name="catalog_old_price_title" umi:field-type="string">
								<xsl:apply-templates select="$infoPage/property[@name = 'catalog_old_price_title']" />
							</span>
							<xsl:text> </xsl:text>
							<span class="price" umi:field-name="old_price" umi:field-type="price" umi:empty="&empty-price;" umi:element-id="{$pageId}">
								<xsl:apply-templates select="//property[@name = 'old_price']" />
							</span>
							<xsl:text> </xsl:text>
							<span class="currency" umi:element-id="{$infoPageId}" umi:empty="&empty-currency;" umi:field-name="currency" umi:field-type="string" >
								<xsl:apply-templates select="$infoPage/property[@name = 'currency']" />
							</span>
						</span>
					</div>
				</xsl:if>
			</xsl:if>
			<!--Конец-->

			<!--Таблица-->
			<xsl:if test="$сatalog = 1 or $сatalog = 3">
				<xsl:variable name="pageTypeId" select="page/@type-id" />
				<xsl:variable name="page" select="document(concat('upage://', $pageId,'?show-empty'))/udata" />
				<table class="catalog_propertys">
					<xsl:apply-templates select="document(concat('utype://', $pageTypeId, '.product'))/udata/group/field[@name = 'artikul'][@visible = 'visible']" mode="product_group" />
					<xsl:apply-templates select="document(concat('utype://', $pageTypeId, '.special'))/udata/group/field[@visible = 'visible']" mode="catalog_table_propertys" >
						<xsl:with-param name="page" select="$page" />
					</xsl:apply-templates>
				</table>
				<xsl:if test="(//property[@name = 'description']/value != '&lt;br&gt;') and (//property[@name = 'description']/value) and (//property[@name = 'description']/value != '')">
					<div class="catalog_h2 h2">
						<span umi:element-id="{$infoPageId}" umi:empty="&empty-catalog-description;" umi:field-name="good_descr" umi:field-type="string">
							<xsl:apply-templates select="$infoPage/property[@name = 'good_descr']" />
						</span>
					</div>
				</xsl:if>
			</xsl:if>
			<!--Конец-->
			
			<!--Основное описание-->
			<div class="text" id="description_{$pageId}_{generate-id()}" umi:element-id="{$pageId}" umi:field-name="description" umi:field-type="wysiwyg" umi:empty="&empty-page-content;">
				<xsl:apply-templates select="//property[@name = 'description']" />
			</div>
			<!--Конец-->
			
			<!--Произвольный код на странице-->
			<xsl:choose>
				<xsl:when test="not(//property[@name = 'code']/value) and ($isAdmin = 0)"></xsl:when>
				<xsl:otherwise>
					<div class="content_code code_pre" umi:field-name="code" umi:field-type="text" umi:element-id="{$pageId}" umi:empty="&empty-code;">
						<xsl:apply-templates select="//property[@name = 'code']/value" mode="code"/>
					</div>
				</xsl:otherwise>
			</xsl:choose>
			<!--Конец-->
			
			<div class="cleaner"/>
			
			<!--Файлы для скачивания у объекта каталога-->
			<xsl:apply-templates select="document(concat('udata://filemanager/list_files/',@id,'//1000/1/'))/udata" />
			<!--Конец-->
			
			<!--Фотографии у объекта каталога-->
			<xsl:variable name="photos" select="document(concat('usel://getContent//photoalbum/photo/',page/@id,'/1/'))/udata" />
			<ul class="photo_list catalog_photo_list" umi:element-id="{$pageId}" umi:type-id="{$photoPageTypeId}"  umi:region="list" umi:sortable="sortable" umi:module="photoalbum" umi:add-method="none">
				<xsl:attribute name="umi:photo-type-id"><xsl:value-of select="$photoPageTypeId" /></xsl:attribute>
				<xsl:if test="not($photos/total) or ($photos/total = 0)"><xsl:call-template name="photo_blank" /></xsl:if>
				<xsl:apply-templates select="$photos/page" mode="photoalbum_photo">
					<xsl:with-param name="photo_album_name" select="page/@alt-name" />
					<xsl:with-param name="width" select="$photoAlbumCatalogW" />
					<xsl:with-param name="height" select="$photoAlbumCatalogH" />
					<xsl:with-param name="no_page" select="1" />
				</xsl:apply-templates>
			</ul>
			<!--Конец-->
			
			<!--Социальный блок-->
			<xsl:if test="$infoPageSocial/property[@name = 'show_social_block_catalog']/value = 1">
				<xsl:call-template name="like_block"/>
			</xsl:if>
			<!--Конец-->
			
			<!--Отзывы-->
			<xsl:if test="$isAdmin = 1">
				<div class="hidden">
					<div class="hidden-text">
						<span>&show-reviews-in-directory;:</span>&nbsp;
						<span umi:element-id="{$infoPageId}" umi:field-name="reviewed_in_directory" umi:field-type="boolean">
							<xsl:choose>
								<xsl:when test="$infoPagePreferences/property[@name = 'reviewed_in_directory']/value = 1">&yes;</xsl:when>
								<xsl:otherwise>&no;</xsl:otherwise>
							</xsl:choose>
						</span>
					</div>
				</div>
			</xsl:if>
			<xsl:if test="$infoPagePreferences/property[@name = 'reviewed_in_directory']/value = 1">
				<xsl:call-template name="reviews">
					<xsl:with-param name="url" select="concat('http://',$_http_host,page/@link)" />
				</xsl:call-template>
			</xsl:if>
			<!--Конец-->
		</div>
		</xsl:comment>
	</xsl:template>
	
	<!--Редактируемые поля для каталога-->
	<xsl:template match="field" mode="catalog_table_propertys">
		<xsl:param name="page"/>
		<xsl:variable name="field_name" select="@name" />
		<xsl:apply-templates select="$page//property[@name = $field_name]" mode="eip_props_table" />
	</xsl:template>

	<xsl:template match="field" mode="product_group">
		<xsl:variable name="field_name" select="@name" />
		<xsl:apply-templates select="document(concat('upage://', $pageId,'?show-empty'))//group[@name = 'product']/property[@name = $field_name]" mode="eip_props_table">
			<xsl:with-param name="class" select="'artikul_row'" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="result[@module = 'catalog'][@method = 'object']" mode="prefix">
		<div class="catalog_object_prefix catalog_object_prefix_top" umi:field-name="prefix" umi:field-type="string" umi:empty="&empty-prefix;" umi:element-id="{$pageId}">
			<xsl:apply-templates select=".//property[@name = 'prefix']" />
		</div>
	</xsl:template>
	
	<xsl:template name="special_offer">
		<div class="hidden catalog-info">
			<span class="hidden-text middle">
				<span>&special-offer;: </span>
				<span umi:element-id="{$pageId}" umi:field-name="special_offer" umi:field-type="boolean">
					<xsl:choose>
						<xsl:when test="//property[@name = 'special_offer']/value = 1">&yes;</xsl:when>
						<xsl:otherwise>&no;</xsl:otherwise>
					</xsl:choose>
				</span>
			</span>
		</div>
	</xsl:template>
	
	<!-- Опциональные свойства -->
	<xsl:template match="group" mode="catalog_opt_props">
		<div class="opt_props">
			<xsl:apply-templates select="./property" mode="catalog_opt_props" />
		</div>
	</xsl:template>
	
	<xsl:template match="property[@type = 'optioned']" mode="catalog_opt_props">
		<span class="opt_prop_block" umi:element-id="{$pageId}" umi:field-type="optioned">
			<xsl:attribute name="umi:fieldId"><xsl:value-of select="@id" /></xsl:attribute>
			<xsl:attribute name="umi:fieldName"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:if test="not(value/*)">
				<xsl:attribute name="class">opt_prop_block hidden</xsl:attribute>
			</xsl:if>
			<span class="opt_props_title persist">
				<xsl:value-of select="title" disable-output-escaping="yes"/>:
			</span>
			<xsl:apply-templates select="value/option" mode="catalog_opt_props" />
		</span>
	</xsl:template>
	
	<xsl:template match="value/option" mode="catalog_opt_props">
		<div class="opt_prop">
			<input type="radio">
				<xsl:attribute name="value"><xsl:value-of select="@float" /></xsl:attribute>
				<xsl:attribute name="name"><xsl:value-of select="../../@name" /></xsl:attribute>
				<xsl:attribute name="id"><xsl:value-of select="object/@id" /></xsl:attribute>
			</input>
			<span class="opt_prop_rel">
				<xsl:value-of select="object/@name" disable-output-escaping="yes"/>
			</span>
		</div>
	</xsl:template>
	
</xsl:stylesheet>