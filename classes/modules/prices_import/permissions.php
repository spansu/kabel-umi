<?php
    $permissions = Array(
        'import' => Array(
			'config', 'import', 'upload', 'parceCSV', 'parse_xls', 'get_uploaded_file', 'array_search_', 'del_selected_str',
            'apply_change_id', 'undo_changes', 'get_fields_type', 'validation', 'get_parents', 'get_types_id',
            'cell_change', 'cell_change_parent', 'add_col', 'add_row', 'del_col', 'del_row', 'change_key_id',
            'select_row', 'save_template', 'getListTemplates', 'applyTemplate', 'deleteTemplate', 'get_category_param',
            'change_category_default', 'change_default_type_id', 'import_', 'start_import', 'end_import', 'obj_exists',
            'default_is_active', 'NormalizeStringToURL', 'transformXML', 'getPerPage', 'set_params_div',
            '_charset_count_bad', '_charset_count_chars', '_charset_count_pairs', '_charset_alt_win', '_charset_koi_win',
            '_charset_utf8_win', '_charset_prepare', 'charset_win_lowercase', 'charset_x_win', 'search', 'getCellValue',
            'setSymlink', 'get_category_default', 'getListSymlink', 'detFromListSymlink','not_add_element', 'schedule',
            'export_', 'getRegEdit', 'getTypeIdSchedule', 'putReportSchedule', 'delReportSchedule','log'
        ),
        //'schedule' => Array('activity','edit','del','editSchedule'),
        //'export' => Array('getExportParentId'),
        //'view' => Array('cron', 'getSchedule')
    );
?>