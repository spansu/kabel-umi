<?php

abstract class __prices_import_adm extends baseModuleAdmin {
	public function config() {
		$regedit = regedit::getInstance();

		//Список всех типов данных объекта каталога
		$list_typeId_arr = $this->get_types_id(1);
		$list_typeId = array();
		foreach($list_typeId_arr['id'] as $pos=>$id){
			$list_typeId[$id] = $list_typeId_arr['name'][$pos];
		}

        $langId = cmsController::getInstance()->getCurrentLang()->getId();

        $getDef_parentId = unserialize($regedit->getVal("//modules/prices_import/def_parentId"));
        if (!$getDef_parentId) {
            $regedit->setVar("//modules/prices_import/def_parentId", serialize(array()));
            $getDef_parentId = '';
        }

        $memory_limit = (int) ini_get('memory_limit');
        $mem = array();
        if ($memory_limit) $mem[$memory_limit] = $memory_limit." (по умолчанию)";
        $mem[32] = "32"; $mem[64] = "64"; $mem[128] = "128"; $mem[256] = "256"; $mem[512] = "512";
        $mem[1024] = "1024"; $mem[2048] = "2048"; $mem[4096] = "4096"; $mem[8192] = "8192";

		$params = array(
			'config'=>array(
				"string:def_parentId" => Null,
				"select:def_typeId" => $list_typeId,
				"boolean:def_isActive" => Null,
				"select:encoding" => array("auto"=>"Автоопределение", "UTF-8"=>"UTF-8", "WIN-1251"=>"CP-1251 (ANSI)"),
				"string:symb_separate_type_id" => Null,
                "select:symb_separate_csv" => array(59=>"; (точка с запятой)", 44=>", (запятая)", 9=>"TAB (табуляция)"),
                "string:stores_field" => Null,
                "string:common_quantity_field" => Null,
                "select:memory_limit" => $mem,
                //"int:max_sched_time" => Null
			)
		);

		$mode = getRequest("param0");

		if($mode == "do") {
			$params = $this->expectParams($params);
            $getDef_parentId[$langId] = $params['config']['string:def_parentId'];
			$regedit->setVar("//modules/prices_import/def_parentId", serialize($getDef_parentId));
			$regedit->setVar("//modules/prices_import/def_typeId", $params['config']['select:def_typeId']);
			$regedit->setVar("//modules/prices_import/def_isActive", $params['config']['boolean:def_isActive']);
			$regedit->setVar("//modules/prices_import/encoding", $params['config']['select:encoding']);
			$regedit->setVar("//modules/prices_import/symb_separate_type_id", $params['config']['string:symb_separate_type_id']);
            $regedit->setVar("//modules/prices_import/symb_separate_csv", $params['config']['select:symb_separate_csv']);
            $regedit->setVar("//modules/prices_import/memory_limit", $params['config']['select:memory_limit']);

            //$maxExecutionTime = (int) $params['config']['int:max_sched_time'];
            //if (!$maxExecutionTime) $maxExecutionTime = 60;
            //$regedit->setVar("//modules/prices_import/max_sched_time", $maxExecutionTime);

            $oC = umiObjectTypesCollection::getInstance();
            $ObjCatId = $oC->getBaseType('catalog','object');
            $getBaseType =  $oC -> getType($ObjCatId);
            $fieldTypesCollection = umiFieldTypesCollection::getInstance();
            $getFieldTypesList = $fieldTypesCollection -> getFieldTypesList();

            //Проверка поля "Состояние на складе" =======================================
            $optionedId = false;
            $regedit->setVar("//modules/prices_import/stores_field", '');
            foreach($getFieldTypesList as $fieldType)
                if ($fieldType -> getDataType() == 'optioned') {
                    $optionedId = $fieldType -> getId();
                    break;
                }
            if ($optionedId !== false){
                $fieldStoresState = $params['config']['string:stores_field'];
                foreach($getBaseType -> getAllFields() as $field){
                    if (($field -> getName() == $fieldStoresState) && ($field -> getFieldTypeId() == $optionedId)) {
                        $regedit->setVar("//modules/prices_import/stores_field", $fieldStoresState);
                        break;
                    }
                }
            }
			//===========================================================================

            //Проверка поля "Общее количество на складах" ===============================
            $intId = false;
            $regedit->setVar("//modules/prices_import/common_quantity_field", '');
            foreach($getFieldTypesList as $fieldType)
                if ($fieldType -> getDataType() == 'int') {
                    $intId = $fieldType -> getId();
                    break;
                }
            if ($intId !== false){
                $fieldCommonQuantity = $params['config']['string:common_quantity_field'];
                foreach($getBaseType -> getAllFields() as $field){
                    if (($field -> getName() == $fieldCommonQuantity) && ($field -> getFieldTypeId() == $intId)) {
                        $regedit->setVar("//modules/prices_import/common_quantity_field", $fieldCommonQuantity);
                        break;
                    }
                }
            }
            //===========================================================================

            $this->chooseRedirect();
		}

        $params['config']['string:def_parentId']['value'] = isset($getDef_parentId[$langId]) ? $getDef_parentId[$langId] : '';
		$params['config']['select:def_typeId']['value'] = (int) $regedit->getVal("//modules/prices_import/def_typeId");
		$params['config']['boolean:def_isActive'] = (boolean) $regedit->getVal("//modules/prices_import/def_isActive");
		$params['config']['select:encoding']['value'] = (string) $regedit->getVal("//modules/prices_import/encoding");
		$params['config']['string:symb_separate_type_id'] = (string) $regedit->getVal("//modules/prices_import/symb_separate_type_id");
        $params['config']['select:symb_separate_csv']['value'] = (string) $regedit->getVal("//modules/prices_import/symb_separate_csv");
        $params['config']['select:memory_limit']['value'] = (string) $regedit->getVal("//modules/prices_import/memory_limit");
        $params['config']['string:stores_field']['value'] = (string) $regedit->getVal("//modules/prices_import/stores_field");
        $params['config']['string:common_quantity_field']['value'] = (string) $regedit->getVal("//modules/prices_import/common_quantity_field");
        //$params['config']['int:max_sched_time']['value'] = (int) $regedit->getVal("//modules/prices_import/max_sched_time");


		$this->setDataType("settings");
		$this->setActionType("modify");

		$data = $this->prepareData($params, "settings");

		$this->setData($data);
		return $this->doData();
	}

    public function import() {
        $regedit = regedit::getInstance();
        
        require_once dirname(__FILE__)."/install_helper.php";
        if(registrateModule('prices_import')) {
            $umiHierarchyTypesCollection = umiHierarchyTypesCollection::getInstance();
            $typesCollection = umiObjectTypesCollection::getInstance();
            $getTypesList = $umiHierarchyTypesCollection -> getTypesList();
            $fieldsCollection = umiFieldsCollection::getInstance();
            $fieldTypesCollection = umiFieldTypesCollection::getInstance();

            $idHType = false;
            foreach($getTypesList as $getType)
                if (($getType->getName() == 'prices_import') && ($getType->getExt() == 'schedule')){
                    $idHType = $getType -> getId();
                    break;
                }
            //Создание иерархического типа
            if ($idHType === false) $idHType = $umiHierarchyTypesCollection -> addType("prices_import","Отложенная загрузка прайса","schedule");

            $getTypesByHierarchyTypeId = $typesCollection -> getTypesByHierarchyTypeId($idHType);

            $getTypeId = key($getTypesByHierarchyTypeId);

            if (!count($getTypesByHierarchyTypeId)){
                //Создание типа данных с полями
                $typeID = $typesCollection -> addType(0,"Отложенная загрузка прайса");
                $getTypeId = $typeID;
                $getType = $typesCollection -> getType($typeID);
                $getType -> setHierarchyTypeId($idHType);

                $groups = array(
                    array("settings", "Настройки отложенной загрузки",
                        "fields"=>array(
                            array("template", "Шаблон", "string",""),
                            array("price_list", "Прайс-лист URL", "string","")
                        )
                    ),
                    array("reports", "Отчет о выполненных загрузках",
                        "fields"=>array(
                            array("report", "Отчет", "wysiwyg","")
                        )
                    ),
                    array("system", "Системные данные",
                        "fields"=>array(
                            array("price_list_url", "Прайс-лист", "string",""),
                            array("last_date", "Дата последнего выполнения", "date",""),
                            array("rows_total", "Всего строк", "int",""),
                            array("rows_processed", "Обработано строк", "int",""),
                            array("create", "Создано товаров", "int",""),
                            array("update", "Обновлено товаров", "int",""),
                            array("is_active", "Активность", "boolean",""),
                            array("price_list_id", "id прайс листа (файл)","string", ""),
                            array("file_date", "Дата загрузки файла","date", "")
                        )
                    )
                );
                foreach($groups as $group){
                    $idGroup = $getType -> addFieldsGroup($group[0], $group[1], true, true);
                    $getGroup = $getType -> getFieldsGroup($idGroup);
                    $getFieldTypesList = $fieldTypesCollection -> getFieldTypesList();
                    foreach($getFieldTypesList as $i => $getFieldType) $getFieldTypesList[$getFieldType->getDataType()] = $getFieldType -> getId();
                    foreach($group['fields'] as $field){
                        $newFieldId = $fieldsCollection -> addField($field[0],$field[1],$getFieldTypesList[$field[2]],true,true,false);
                        if ($newFieldId) $getGroup -> attachField($newFieldId);
                        $getField = $fieldsCollection -> getField($newFieldId);
                        if ($getField)  $getField->setTip($field[3]);
                    }
                }
            }

            //if (file_exists($root . "/styles/skins/mac/data/modules/prices_import/system/templates.arr")) unlink($root . "/styles/skins/mac/data/modules/prices_import/system/templates.arr");
            $regedit->setVar("//modules/prices_import/encoding", "auto");
            $regedit->setVar("//modules/prices_import/def_parentId", serialize(array()));
            $regedit->setVar("//modules/prices_import/def_isActive", 1);
            $regedit->setVar("//modules/prices_import/symb_separate_type_id", ";");
            $regedit->setVar("//modules/prices_import/symb_separate_csv", 59);
            $regedit->setVar("//modules/prices_import/type_id_schedule", $getTypeId);


            $oC = umiObjectTypesCollection::getInstance();
            $ObjCatId = $oC->getBaseType('catalog','object');
            $getBaseType =  $oC -> getType($ObjCatId);
            $fieldTypesCollection = umiFieldTypesCollection::getInstance();
            $getFieldTypesList = $fieldTypesCollection -> getFieldTypesList();

            //Проверка поля "Состояние на складе"
            $regedit->setVar("//modules/prices_import/stores_field", '');
            $optionedId = false;
            foreach($getFieldTypesList as $fieldType)
                if ($fieldType -> getDataType() == 'optioned') {
                    $optionedId = $fieldType -> getId();
                    break;
                }
            if ($optionedId !== false){
                foreach($getBaseType -> getAllFields() as $field){
                    if (($field -> getName() == "stores_state") && ($field -> getFieldTypeId() == $optionedId)) {
                        $regedit->setVar("//modules/prices_import/stores_field", 'stores_state');
                        break;
                    }
                }
            }

            //Проверка поля "Общее количество на складах"
            $regedit->setVar("//modules/prices_import/common_quantity_field", '');
            $intId = false;
            foreach($getFieldTypesList as $fieldType)
                if ($fieldType -> getDataType() == 'int') {
                    $intId = $fieldType -> getId();
                    break;
                }
            if ($intId !== false){
                foreach($getBaseType -> getAllFields() as $field){
                    if (($field -> getName() == "common_quantity") && ($field -> getFieldTypeId() == $intId)) {
                        $regedit->setVar("//modules/prices_import/common_quantity_field", 'common_quantity');
                        break;
                    }
                }
            }

            $max_sched_time = (int) ini_get('max_execution_time');
            if (!$max_sched_time) $max_sched_time = 60;
            $regedit->setVar("//modules/prices_import/max_sched_time", $max_sched_time);

            $configIni = file_get_contents(CURRENT_WORKING_DIR."/config.ini");
            if (strpos($configIni, "not-allowed-methods[] = \"prices_import/\"") === false){
                $pos = strpos($configIni, "not-allowed-methods[]");
                $part1 = substr($configIni, 0, $pos);
                $part = "not-allowed-methods[] = \"prices_import/\"".PHP_EOL;
                $part2 = substr_replace($configIni,"",0,$pos);
                file_put_contents(CURRENT_WORKING_DIR."/config.ini",$part1.$part.$part2);
            }
        }
        
        $this->setDataType("list");
        $this->setActionType("view");
        $this->setDataRange(0, 0);
        $data = $this->prepareData(array(), "objects");
        $this->setData($data, 0);
        return $this->doData();
    }

    public function export() {
        $this->setDataType("list");
        $this->setActionType("view");
        $this->setDataRange(0, 0);
        $data = $this->prepareData(array(), "objects");
        $this->setData($data, 0);
        return $this->doData();
    }

    public function schedules() {
        $this->setDataType('list');
        $this->setActionType('view');

        if($this->ifNotXmlMode()) return $this->doData();

        $limit = getRequest('per_page_limit');
        $curr_page = (int) getRequest('p');
        $offset = $limit * $curr_page;

        $sel = new selector('objects');
        $sel->types('object-type')->name('prices_import', 'schedule');

        $sel->limit($offset, $limit);

        selectorHelper::detectFilters($sel);

        $data = $this->prepareData($sel->result, "objects");
        $this->setData($data, $sel->length);
        $this->setDataRangeByPerPage($limit, $curr_page);
        return $this->doData();
    }

    public function activity() {

        $objects = getRequest('object');
        if(!is_array($objects)) {
            $objects = Array($objects);
        }
        $is_active = (bool) getRequest('active');

        foreach($objects as $objectId) {
            $object = $this->expectObject($objectId, false, true);
            $object->setValue("is_active", $is_active);
            $object->commit();
        }

        $this->setDataType("list");
        $this->setActionType("view");
        $data = $this->prepareData($objects, "objects");
        $this->setData($data);

        return $this->doData();
    }

    public function getDatasetConfiguration($param = '') {
        return array(
            'methods' => array(
                array('title'=>getLabel('smc-load'), 'forload'=>true, 'module'=>'prices_import', '#__name'=>"schedules"),
                array('title'=>getLabel('smc-activity'), 'module'=>'prices_import', '#__name'=>'activity', 'aliases' => 'tree_set_activity, activity'),
                array('title'=>getLabel('smc-delete'), 'module'=>'prices_import', '#__name'=>'del', 'aliases' => 'tree_delete_element,delete,del')
            ),
            'types' => array(
                array('common' => 'true', 'id' => 'schedule')
            ),
            'stoplist' => array(),
            'default' => 'template[200px]|price_list[250px]|last_date[200px]'
        );
    }

    public function edit() {
        $object = $this->expectObject("param0");
        $mode = (string) getRequest('param1');

        $inputData = array(
            'object' => $object,
            'allowed-element-types' => array('schedule')
        );

        if($mode == "do") {
            $data = getRequest("data");
            $getPriceList = current($data);
            if ($getPriceList['price_list']) {
                $object -> setValue('price_list_url',$getPriceList['price_list']);
                $object -> commit();
            }

            $this->saveEditedObjectData($inputData);
            $this->chooseRedirect();
        }

        $this->setDataType("form");
        $this->setActionType("modify");

        $data = $this->prepareData($object, "object");

        $this->setData($data);
        return $this->doData();
    }

    public function del() {
        $objects = getRequest('element');
        if(!is_array($objects)) {
            $objects = Array($objects);
        }

        foreach($objects as $objectId) {
            $object = $this->expectObject($objectId, false, true);

            $params = Array(
                'object' => $object
            );

            $this->deleteObject($params);
        }

        $this->setDataType("list");
        $this->setActionType("view");
        $data = $this->prepareData($objects, "objects");
        $this->setData($data);

        return $this->doData();
    }

    public function add() {
        $mode = (string) getRequest('param0');

        $inputData = array(
            'type' => 'schedule',
            'type-id' => getRequest('type-id'),
            'allowed-element-types' => array('schedule')
        );

        if($mode == "do") {
            $data = getRequest("data");
            $object = $this->saveAddedObjectData($inputData);

            $getPriceList = current($data);
            if ($getPriceList['price_list']) {
                $object -> setValue('price_list_url',$getPriceList['price_list']);
                $object -> commit();
            }
            $this->chooseRedirect($this->pre_lang . "/admin/prices_import/edit/{$object->id}/");
        }

        $this->setDataType("form");
        $this->setActionType("create");

        $data = $this->prepareData($inputData, "object");

        $this->setData($data);
        return $this->doData();
    }

}

?>