<?php
//require_once "install_helper.php";

$INFO = Array();

$INFO['name'] = "prices_import";
$INFO['filename'] = "modules/prices_import/class.php";
$INFO['config'] = "1";
$INFO['ico'] = "ico_prices_import";
$INFO['default_method'] = "page";
$INFO['default_method_admin'] = "import";

$INFO['func_perms'] = "";
$INFO['func_perms/import'] = "Импорт прайсов";
$INFO['func_perms/schedules'] = "Задачи модуля";
$INFO['func_perms/export'] = "Экспорт данных";
$INFO['func_perms/view'] = "Плановое выполнение задач";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/prices_import/class.php";
$COMPONENTS[1] = "./classes/modules/prices_import/__admin.php";
$COMPONENTS[2] = "./classes/modules/prices_import/lang.php";
$COMPONENTS[3] = "./classes/modules/prices_import/i18n.php";
$COMPONENTS[4] = "./classes/modules/prices_import/permissions.php";

/*
$umiHierarchyTypesCollection = umiHierarchyTypesCollection::getInstance();
$typesCollection = umiObjectTypesCollection::getInstance();
$getTypesList = $umiHierarchyTypesCollection -> getTypesList();
$fieldsCollection = umiFieldsCollection::getInstance();
$fieldTypesCollection = umiFieldTypesCollection::getInstance();

$idHType = false;
foreach($getTypesList as $getType)
    if (($getType->getName() == 'prices_import') && ($getType->getExt() == 'schedule')){
        $idHType = $getType -> getId();
        break;
    }
//Создание иерархического типа
if ($idHType === false) $idHType = $umiHierarchyTypesCollection -> addType("prices_import","Отложенная загрузка прайса","schedule");

$getTypesByHierarchyTypeId = $typesCollection -> getTypesByHierarchyTypeId($idHType);

$getTypeId = key($getTypesByHierarchyTypeId);

if (!count($getTypesByHierarchyTypeId)){
    //Создание типа данных с полями
    $typeID = $typesCollection -> addType(0,"Отложенная загрузка прайса");
    $getTypeId = $typeID;
    $getType = $typesCollection -> getType($typeID);
    $getType -> setHierarchyTypeId($idHType);

    $groups = array(
        array("settings", "Настройки отложенной загрузки",
            "fields"=>array(
                array("template", "Шаблон", "string",""),
                array("price_list", "Прайс-лист URL", "string","")
            )
        ),
        array("reports", "Отчет о выполненных загрузках",
            "fields"=>array(
                array("report", "Отчет", "wysiwyg","")
            )
        ),
        array("system", "Системные данные",
            "fields"=>array(
                array("price_list_url", "Прайс-лист", "string",""),
                array("last_date", "Дата последнего выполнения", "date",""),
                array("rows_total", "Всего строк", "int",""),
                array("rows_processed", "Обработано строк", "int",""),
                array("create", "Создано товаров", "int",""),
                array("update", "Обновлено товаров", "int",""),
                array("is_active", "Активность", "boolean",""),
                array("price_list_id", "id прайс листа (файл)","string", ""),
                array("file_date", "Дата загрузки файла","date", "")
            )
        )
    );
    foreach($groups as $group){
        $idGroup = $getType -> addFieldsGroup($group[0], $group[1], true, true);
        $getGroup = $getType -> getFieldsGroup($idGroup);
        $getFieldTypesList = $fieldTypesCollection -> getFieldTypesList();
        foreach($getFieldTypesList as $i => $getFieldType) $getFieldTypesList[$getFieldType->getDataType()] = $getFieldType -> getId();
        foreach($group['fields'] as $field){
            $newFieldId = $fieldsCollection -> addField($field[0],$field[1],$getFieldTypesList[$field[2]],true,true,false);
            if ($newFieldId) $getGroup -> attachField($newFieldId);
            $getField = $fieldsCollection -> getField($newFieldId);
            if ($getField)  $getField->setTip($field[3]);
        }
    }
}

$root = CURRENT_WORKING_DIR;

//if (file_exists($root . "/styles/skins/mac/data/modules/prices_import/system/templates.arr")) unlink($root . "/styles/skins/mac/data/modules/prices_import/system/templates.arr");

$regedit = regedit::getInstance();
$regedit->setVar("//modules/prices_import/encoding", "auto");
$regedit->setVar("//modules/prices_import/def_parentId", serialize(array()));
$regedit->setVar("//modules/prices_import/def_isActive", 1);
$regedit->setVar("//modules/prices_import/symb_separate_type_id", ";");
$regedit->setVar("//modules/prices_import/symb_separate_csv", 59);
$regedit->setVar("//modules/prices_import/type_id_schedule", $getTypeId);


$oC = umiObjectTypesCollection::getInstance();
$ObjCatId = $oC->getBaseType('catalog','object');
$getBaseType =  $oC -> getType($ObjCatId);
$fieldTypesCollection = umiFieldTypesCollection::getInstance();
$getFieldTypesList = $fieldTypesCollection -> getFieldTypesList();

//Проверка поля "Состояние на складе"
$regedit->setVar("//modules/prices_import/stores_field", '');
$optionedId = false;
foreach($getFieldTypesList as $fieldType)
    if ($fieldType -> getDataType() == 'optioned') {
        $optionedId = $fieldType -> getId();
        break;
    }
if ($optionedId !== false){
    foreach($getBaseType -> getAllFields() as $field){
        if (($field -> getName() == "stores_state") && ($field -> getFieldTypeId() == $optionedId)) {
            $regedit->setVar("//modules/prices_import/stores_field", 'stores_state');
            break;
        }
    }
}

//Проверка поля "Общее количество на складах"
$regedit->setVar("//modules/prices_import/common_quantity_field", '');
$intId = false;
foreach($getFieldTypesList as $fieldType)
    if ($fieldType -> getDataType() == 'int') {
        $intId = $fieldType -> getId();
        break;
    }
if ($intId !== false){
    foreach($getBaseType -> getAllFields() as $field){
        if (($field -> getName() == "common_quantity") && ($field -> getFieldTypeId() == $intId)) {
            $regedit->setVar("//modules/prices_import/common_quantity_field", 'common_quantity');
            break;
        }
    }
}

$max_sched_time = (int) ini_get('max_execution_time');
if (!$max_sched_time) $max_sched_time = 60;
$regedit->setVar("//modules/prices_import/max_sched_time", $max_sched_time);

$configIni = file_get_contents(CURRENT_WORKING_DIR."/config.ini");
if (strpos($configIni, "not-allowed-methods[] = \"prices_import/\"") === false){
    $pos = strpos($configIni, "not-allowed-methods[]");
    $part1 = substr($configIni, 0, $pos);
    $part = "not-allowed-methods[] = \"prices_import/\"".PHP_EOL;
    $part2 = substr_replace($configIni,"",0,$pos);
    file_put_contents(CURRENT_WORKING_DIR."/config.ini",$part1.$part.$part2);
}

registrateModule('prices_import');*/