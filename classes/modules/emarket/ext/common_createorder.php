<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

abstract class common_createorder {

    public function co_ajaxSearchItem($search_string, $domain_id = 1, $lang_id = 1) {
        $search_string = (string) trim($search_string);
        $search_string_arr = explode(' ', $search_string);
        foreach ($search_string_arr as $key => $value) {
            if (mb_strlen($value) < 3) {
                unset($search_string_arr[$key]);
            }
        }

        $result = array();
        $h = umiHierarchy::getInstance();

        if (is_array($search_string_arr) && count($search_string_arr)) {
            $limit = 20;

            $except_field_ids = '';
            $q = l_mysql_query("SELECT id FROM cms3_object_fields WHERE field_type_id IN "
                    . "(SELECT id FROM cms3_object_field_types "
                    . "WHERE data_type IN ('file','img_file','password','video_file','swf_file'))");

            while ($row = mysql_fetch_row($q)) {
                $except_field_ids .= $row[0] . ', ';
            }

            if ($except_field_ids > 2) {
                $except_field_ids = substr($except_field_ids, 0, -2);
            } else {
                $except_field_ids = '0';
            }

            $q = l_mysql_query("SELECT id FROM cms3_hierarchy_types WHERE name = 'catalog' AND ext = 'object'");
            $row = mysql_fetch_row($q);
            if (isset($row[0])) {
                $catalog_htype_id = intval($row[0]);
                if ($catalog_htype_id > 0) {
                    $q = l_mysql_query("SELECT id FROM cms3_object_types WHERE hierarchy_type_id = '{$catalog_htype_id}'");
                    $catalog_otypes_ids = '';
                    while ($row = mysql_fetch_row($q)) {
                        $catalog_otypes_ids .= $row[0] . ', ';
                    }
                    if (strlen($catalog_otypes_ids) > 2) {
                        $catalog_otypes_ids = substr($catalog_otypes_ids, 0, -2);

                        $query = "SELECT ch.id, co.name, ch.rel, ch.is_active FROM cms3_objects co "
                                . "INNER JOIN cms3_hierarchy ch ON co.id = ch.obj_id "
                                . "INNER JOIN cms3_object_content coc ON co.id = coc.obj_id "
                                . "WHERE co.type_id IN ({$catalog_otypes_ids}) AND coc.field_id NOT IN({$except_field_ids}) "
                                . "AND ch.domain_id = {$domain_id} AND ch.lang_id = {$lang_id} AND (";

                        $cols = array('co.name', 'coc.varchar_val', 'coc.text_val');
                        foreach ($cols as $col) {
                            foreach ($search_string_arr as $value) {
                                $query .= $col . " LIKE '%{$value}%' OR ";
                            }
                        }

                        $query = substr($query, 0, -4) . ') ';

                        $query .= " AND ch.is_deleted = 0 GROUP BY co.id ORDER BY ch.is_active DESC LIMIT 0,{$limit}";

                        $q = l_mysql_query($query);

                        $rels = array();
                        while ($row = mysql_fetch_row($q)) {
                            $rel = $row[2];
                            $rels[0] = '';
                            if ($rel > 0 && !isset($rels[$rel])) {
                                $q2 = l_mysql_query("SELECT name FROM cms3_objects WHERE id = (SELECT obj_id FROM cms3_hierarchy WHERE id = {$rel})");
                                $row2 = mysql_fetch_row($q2);
                                if (is_array($row2)) {
                                    $rels[$rel] = $row2[0];
                                }
                            }

                            if ($row[3]) {
                                $link = $h->getPathById($row[0]);
                            } else {
                                $link = "/admin/catalog/edit/{$row[0]}/";
                            }

                            $result[] = array(
                                'attribute:id' => $row[0],
                                'attribute:name' => $row[1],
                                'attribute:link' => $link,
                                'attribute:parent_name' => $rels[$rel],
                                'attribute:is_active' => $row[3]
                            );
                        }
                    }
                }
            }
        }

        return array('total' => count($result), 'items' => array('nodes:item' => $result));
    }

    public function co_ajaxSearchItemOptions($id) {
        $mainConfiguration = mainConfiguration::getInstance();
        $optioned_groups = $mainConfiguration->get('modules', 'emarket.order-types.optioned.groups');

        $result = array();
        $original_price = 0;
        $controller = cmsController::getInstance();
        $oc = umiObjectsCollection::getInstance();

        $q = l_mysql_query("SELECT id FROM cms3_object_field_types WHERE data_type = 'optioned'");
        $row = mysql_fetch_row($q);
        if (is_array($row)) {
            $optioned_type_id = $row[0];

            if (is_array($optioned_groups) && count($optioned_groups)) {
                $q = l_mysql_query("SELECT obj_id FROM cms3_hierarchy WHERE id = {$id}");
                $row = mysql_fetch_row($q);
                if (isset($row[0])) {
                    $obj_id = $row[0];
                    $obj = umiObjectsCollection::getInstance()->getObject($obj_id);
                    if (is_object($obj)) {
                        $original_price = $obj->getValue('price');

                        $props = array();
                        foreach ($optioned_groups as $group_name) {
                            $arr = $obj->getPropGroupByName($group_name);
                            if (is_array($arr)) {
                                foreach ($arr as $prop_id) {
                                    $q = l_mysql_query("SELECT field_type_id FROM cms3_object_fields WHERE id = " . $prop_id);
                                    $row = mysql_fetch_row($q);
                                    if (isset($row[0]) && $row[0] == $optioned_type_id && !in_array($prop_id, $props)) {
                                        $props[] = $prop_id;
                                    }
                                }
                            }
                        }

                        foreach ($props as $prop) {
                            $prop_obj = $obj->getPropById($prop);
                            if (is_object($prop_obj)) {
                                $prop_values = $prop_obj->getValue();
                                if (is_array($prop_values) && count($prop_values)) {
                                    $guide_items = array();
                                    foreach ($prop_values as $value) {
                                        $value_obj = $oc->getObject($value['rel']);
                                        if (is_object($value_obj)) {
                                            if (!isset($value['float'])) {
                                                $value['float'] = 0;
                                            }
                                            $guide_items[] = array(
                                                'attribute:id' => $value_obj->getId(),
                                                'attribute:title' => $value_obj->getName(),
                                                'attribute:float' => $value['float']
                                            );
                                        }
                                    }
                                }

                                if (!count($guide_items)) {
                                    continue;
                                }

                                $property_title = $prop_obj->getTitle();
                                $property_name = $prop_obj->getName();

                                $result[] = array(
                                    'attribute:name' => $property_name,
                                    'attribute:title' => $property_title,
                                    'guide' => array('nodes:item' => $guide_items)
                                );
                            }
                        }
                    }
                }
            }
        }


        return array('total' => count($result), 'items' => array('nodes:item' => $result), 'original_price' => $original_price);
    }

    public function co_AddItem($mode = 'new') {
        $result = 0;
        $item_id = (int) getRequest('item_id');
        $is_new_oitem = getRequest('is_new_item');

        if ($item_id > 0 || $is_new_oitem) {
            // --- костыль для корректного определения скидки на пользователя
            $cmsController = cmsController::getInstance();
            $oldCurrentMode = $cmsController->getCurrentMode();
            $cmsController->setCurrentMode('admin');

            if ($mode == 'new') {
                $_REQUEST['no-redirect'] = 1;
                if (!$is_new_oitem) {
                    $this->basket('put', 'element', $item_id);
                } else {
                    $new_oitem_arr = getRequest('new_oitem');

                    if (isset($new_oitem_arr['name']) && isset($new_oitem_arr['price'])) {
                        $new_oitem_obj = $this->co_createOrderItem($new_oitem_arr['name'], intval(getRequest('amount')), $new_oitem_arr['price']);
                        if (is_object($new_oitem_obj)) {
                            $order = $this->getBasketOrder();
                            if (is_object($order)) {
                                $order->appendItem($new_oitem_obj);
                                $order->refresh();
                            }
                        }
                    }
                }
                $result = 1;
            } else {
                $order = order::get($mode);
                if (is_object($order)) {
                    if ($is_new_oitem) {
                        $new_oitem_arr = getRequest('new_oitem');

                        $new_oitem_obj = $this->co_createOrderItem($new_oitem_arr['name'], intval(getRequest('amount')), $new_oitem_arr['price']);
                        if (is_object($new_oitem_obj)) {
                            if (is_object($order)) {
                                $order->appendItem($new_oitem_obj);
                                $order->refresh();
                                $result = 1;
                            }
                        }
                    } else {
                        $itemId = $item_id;
                        $amount = (int) getRequest('amount');
                        $options = getRequest('options');
                        $itemType = 'element';

                        $newElement = false;

                        $orderItem = $this->getBasketItem($itemId, false);
                        if (!$orderItem) {
                            $orderItem = $this->getBasketItem($itemId);
                            $newElement = true;
                        }

                        if (!$orderItem) {
                            $result = 0;
                        }

                        if (is_array($options)) {
                            // Get all orderItems
                            $orderItems = $order->getItems();

                            foreach ($orderItems as $tOrderItem) {
                                if (!$tOrderItem instanceOf optionedOrderItem) {
                                    $itemOptions = null;
                                    $tOrderItem = null;
                                    continue;
                                }

                                $itemOptions = $tOrderItem->getOptions();

                                if (sizeof($itemOptions) != sizeof($options)) {
                                    $itemOptions = null;
                                    $tOrderItem = null;
                                    continue;
                                }

                                if ($tOrderItem->getItemElement()->id != $orderItem->getItemElement()->id) {
                                    $itemOptions = null;
                                    $tOrderItem = null;
                                    continue;
                                }

                                foreach ($options as $optionName => $optionId) {
                                    $itemOption = getArrayKey($itemOptions, $optionName);

                                    if (getArrayKey($itemOption, 'option-id') != $optionId) {
                                        $tOrderItem = null;
                                        continue 2;
                                    }
                                }
                                break;
                            }

                            if (!isset($tOrderItem) || is_null($tOrderItem)) {
                                $tOrderItem = orderItem::create($itemId);
                                $order->appendItem($tOrderItem);
                                if ($newElement) {
                                    $orderItem->remove();
                                }
                            }

                            if ($tOrderItem instanceof optionedOrderItem) {
                                foreach ($options as $optionName => $optionId) {
                                    if ($optionId) {
                                        $tOrderItem->appendOption($optionName, $optionId);
                                    } else {
                                        $tOrderItem->removeOption($optionName);
                                    }
                                }
                            }

                            if ($tOrderItem) {
                                $orderItem = $tOrderItem;
                            }
                        }

                        $amount = $amount ? $amount : ($orderItem->getAmount() + 1);
                        $orderItem->setAmount($amount ? $amount : 1);
                        $orderItem->refresh();
                        $order->appendItem($orderItem);

                        $order->refresh();
                        $result = 1;
                    }
                }
            }

            $order = $this->getBasketOrder();
            $order->refresh();
            $order->commit();

            $cmsController->setCurrentMode($oldCurrentMode);
        }
        return array('result' => $result);
    }

    public function co_ajaxEditItem($id, $amount) {
        $result = 0;
        if ($id > 0) {
            // --- костыль для корректного определения скидки на пользователя
            $cmsController = cmsController::getInstance();
            $oldCurrentMode = $cmsController->getCurrentMode();
            $cmsController->setCurrentMode('admin');

            $_REQUEST['amount'] = $amount;
            $this->basket('put','item',$id);
            
            /*$item = orderItem::get($id);
            if (is_object($item)) {
                $item->setAmount(intval($amount));
                $item->refresh();
                $result = 1;
            }*/
            $result = 1;

            $order = $this->getBasketOrder();
            $order->refresh();
            $order->commit();

            $cmsController->setCurrentMode($oldCurrentMode);
        }
        return array('result' => $result);
    }

    public function co_ajaxRemoveItem($id) {
        $result = 0;
        if ($id > 0) {
            // --- костыль для корректного определения скидки на пользователя
            $cmsController = cmsController::getInstance();
            $oldCurrentMode = $cmsController->getCurrentMode();
            $cmsController->setCurrentMode('admin');

            $_REQUEST['no-redirect'] = 1;
            $this->basket('remove', 'item', $id);
            $result = 1;

            $order = $this->getBasketOrder();
            $order->refresh();
            $order->commit();

            $cmsController->setCurrentMode($oldCurrentMode);
        }
        return array('result' => $result);
    }

    public function co_ajaxSetDelivery($delivery_id, $delivery_price = 0, $force_update = false) {
        $order = $this->getBasketOrder();

        // --- костыль для корректного определения скидки на пользователя
        $cmsController = cmsController::getInstance();
        $oldCurrentMode = $cmsController->getCurrentMode();
        $cmsController->setCurrentMode('admin');

        if ($delivery_id == 0) {
            $order->setValue('delivery_id', 0);
            $order->setValue('delivery_price', 0);
        } else {
            $delivery = delivery::get($delivery_id);
            if (is_object(($delivery))) {
                if (!$force_update) {
                    $delivery_price = (float) $delivery->getDeliveryPrice($order);
                } else {
                    $delivery_price = $delivery_price;
                }

                $order->setValue('delivery_id', $delivery_id);
                $order->setValue('delivery_price', $delivery_price);

                $oEventPoint = new umiEventPoint("emarket_set_delivery_price");
                $oEventPoint->setMode("after");
                $oEventPoint->addRef("order", $order);
                $oEventPoint->addRef("delivery_id", $delivery_id);
                $oEventPoint->addRef("delivery_price", $delivery_price);
                $this->setEventPoint($oEventPoint);
            }
        }

        $order->refresh();
        $order->commit();

        $cmsController->setCurrentMode($oldCurrentMode);

        return array('delivery_price' => $delivery_price);
    }

    // не работает, т.к. до момента оформления заказа система не дает назначить скидку, во время order->refresh() скидка обнуляется.
    public function co_ajaxSetItemDiscount($item_id, $discount_value = 0) {
        $discount_value = floatval(trim($discount_value));
        $order = $this->getBasketOrder();

        // --- костыль для корректного определения скидки на пользователя
        $cmsController = cmsController::getInstance();
        $oldCurrentMode = $cmsController->getCurrentMode();
        $cmsController->setCurrentMode('admin');

        $item = $order->getItem($item_id);

        if (!$item instanceof orderItem || $item->getDiscountValue() == $discount_value) {
            return 0;
        } else {
            $item->setDiscountValue($discount_value);
            $item->commit();

            $order->refresh();
            $order->commit();
        }

        $cmsController->setCurrentMode($oldCurrentMode);

        return 1;
    }

    public function co_ajaxSetCustomerId($customer_id = 0) {
        if (!$customer_id) {
            $customer_id = permissionsCollection::getInstance()->getUserId();
        }

        $order = $this->getBasketOrder();
        $customer_obj = umiObjectsCollection::getInstance()->getObject($customer_id);
        if (is_object($customer_obj)) {
            $customer_obj->setOwnerId($customer_id);
            $customer_obj->commit();
        }

        $order->setValue('customer_id', $customer_id);
        // --- костыль для корректного определения скидки на пользователя
        $cmsController = cmsController::getInstance();
        $oldCurrentMode = $cmsController->getCurrentMode();
        $cmsController->setCurrentMode('admin');
        $order->commit();
        if (isset($_REQUEST['data']) && is_array($_REQUEST['data'])) {
            $_new_REQUEST = array();
            $_new_REQUEST[$order->getId()] = $order->getId();
            foreach ($_REQUEST['data'] as $key => $value) {
                $_new_REQUEST[$key] = $value;
            }
            $_REQUEST['data'] = $_new_REQUEST;
        } else {
            $_REQUEST['data'][$order->getId()] = $order->getId();
        }
        $order->refresh();

        $items = $order->getItems();
        foreach ($items as $item) {
            $item->refresh();
            $item->commit();
        }

        $order->commit();

        $order->refresh();
        $cmsController->setCurrentMode($oldCurrentMode);
        // ---

        return array('result' => '1');
    }

    public function co_getNewCustomerTypeId() {
        if (class_exists('umiTypesHelper')) {
            $umiTypesHelper = umiTypesHelper::getInstance();
            $customerTypeId = $umiTypesHelper->getObjectTypeIdByGuid('emarket-customer');
        } else {
            $q = l_mysql_query("SELECT id FROM cms3_object_types WHERE hierarchy_type_id = (SELECT id FROM cms3_hierarchy_types WHERE name = 'emarket' AND ext = 'customer')");
            $row = mysql_fetch_row($q);
            if (isset($row[0])) {
                $customerTypeId = $row[0];
            }
        }
        return $customerTypeId;
    }

    public function co_ajaxSetGuideItem($mode, $param) {
        $controller = cmsController::getInstance();
        $oc = umiObjectsCollection::getInstance();

        $guide_item_id = 0;

        if ($mode == 'add') {
            $data_key = 'new';
            $type_id = $param;
        } else {
            $data_key = $param;
            $guide_item = $oc->getObject($param);
            if (is_object($guide_item)) {
                $type_id = $guide_item->getTypeId();
            }
        }

        $name = trim(getRequest('name'));
        if (strlen($name) == 0 && $type_id > 0) {
            $q = l_mysql_query('SELECT guid FROM cms3_object_types WHERE id = ' . $type_id);
            $row = mysql_fetch_row($q);
            if (isset($row[0]) && strlen($row[0]) > 0) {
                switch ($row[0]) {
                    case 'emarket-deliveryaddress':
                        $name = '';
                        foreach ($_REQUEST['data'][$data_key] as $key => $value) {
                            if ($value != '') {
                                if (($key == 'country' || $key == 'city') && is_numeric($value)) {
                                    $country_obj = $oc->getObject($value);
                                    if (is_object($country_obj)) {
                                        $name .= $country_obj->getName() . ', ';
                                    }
                                } else if ($key == 'order_comments') {
                                    $name .= ' (' . $value . '), ';
                                } else if ($key == 'street') {
                                    $name .= 'ул. ' . $value . ', ';
                                } else if ($key == 'house') {
                                    $name .= 'д. ' . $value . ', ';
                                } else if ($key == 'flat') {
                                    $name .= 'кв. ' . $value . ', ';
                                } else {
                                    $name .= $value . ', ';
                                }
                            }
                        }
                        $name = substr($name, 0, -2);
                        break;
                    case 'emarket-legalperson':
                        if (isset($_REQUEST['data'][$data_key]['name'])) {
                            $name = trim($_REQUEST['data'][$data_key]['name']);
                        }
                        break;
                    case 'emarket-customer':
                        foreach ($_REQUEST['data'][$data_key] as $key => $value) {
                            if ($value != '' && in_array($key, array('fname', 'lname', 'father_name'))) {
                                $name .= $value . ' ';
                            }
                        }
                        $name = substr($name, 0, -1);
                        break;
                    case 'users-user':
                        foreach ($_REQUEST['data'][$data_key] as $key => $value) {
                            if ($value != '' && in_array($key, array('login', 'fname', 'lname', 'father_name'))) {
                                $name .= $value . ' ';
                            }
                        }
                        $name = substr($name, 0, -1);
                        break;
                    default:
                        foreach ($_REQUEST['data'][$data_key] as $key => $value) {
                            if ($value != '') {
                                $name = $value;
                                break;
                            }
                        }
                        break;
                }
            }
        }

        if ($mode == 'add') {
            $guide_item_id = $oc->addObject($name, $type_id);
            $dataModule = $controller->getModule("data");
            if ($dataModule) {
                $dataModule->saveEditedObjectWithIgnorePermissions($guide_item_id, true, true);
            }
        } else {
            if (is_object($guide_item)) {
                $guide_item_id = $param;
                if (strlen($name) > 0) {
                    $guide_item->setName($name);
                }

                $dataModule = $controller->getModule("data");
                if ($dataModule) {
                    $dataModule->saveEditedObjectWithIgnorePermissions($guide_item_id, false, true);
                }
            }
        }
        return array('guide_item_id' => $guide_item_id, 'name' => $name);
    }

    public function co_ajaxSetCustomer($mode, $param) {
        $success = 0;
        $result = $this->co_ajaxSetGuideItem($mode, $param);
        if (isset($result['guide_item_id'])) {
            $customer_id = $result['guide_item_id'];
            $result = $this->co_ajaxSetCustomerId($customer_id);
            if (isset($result['result'])) {
                $customer_obj = umiObjectsCollection::getInstance()->getObject($customer_id);
                if (is_object($customer_obj)) {
                    if (isset($_REQUEST['customer_data_value']) && is_array($_REQUEST['customer_data_value'])) {
                        foreach ($_REQUEST['customer_data_value'] as $prop_name => $prop_data) {
                            $customer_obj->setValue($prop_name, $prop_data);
                        }
                        $customer_obj->commit();
                    }
                }

                $order = $this->getBasketOrder();
                if (is_object($order)) {
                    if (isset($_REQUEST['extended_guide_items']) && is_array($_REQUEST['extended_guide_items'])) {
                        foreach ($_REQUEST['extended_guide_items'] as $property => $value) {
                            if ($property == 'delivery_addresses') {
                                $property = 'delivery_address';
                            }
                            if ($property == 'legal_persons') {
                                $property = 'legal_person';
                            }
                            $order->setValue($property, $value);
                        }
                        $order->commit();
                    }
                }
                $success = $result['result'];
            }
        }

        return array('result' => $success);
    }

    public function co_ajaxGetCustomerInfo() {
        $result = array();
        $order = $this->getBasketOrder();
        $customer_id = $order->getValue('customer_id');
        $q = l_mysql_query('SELECT name FROM cms3_objects WHERE id = ' . $customer_id);
        $row = mysql_fetch_row($q);
        if (is_array($row) && count($row)) {
            $props = array('lname', 'fname', 'father_name', 'e-mail', 'email');
            $customer_obj = umiObjectsCollection::getInstance()->getObject($customer_id);
            if (is_object($customer_obj)) {
                $name = $row[0];

                foreach ($props as $prop) {
                    $value = $customer_obj->getValue($prop);
                    $matches = array();
                    if (strlen($value)) {
                        $name .= ' ' . $value;
                    }
                }

                $name = implode(' ', array_unique(explode(' ', $name)));

                $result = array('result' => array('id' => $customer_id, 'name' => $name));
            }
        }
        return $result;
    }

    public function co_ajaxSearchCustomer($search_string) {
        $search_string = (string) trim($search_string);
        $search_string_arr = explode(' ', $search_string);
        foreach ($search_string_arr as $key => $value) {
            if (mb_strlen($value) < 3) {
                unset($search_string_arr[$key]);
            }
        }

        $result = array();

        if (is_array($search_string_arr) && count($search_string_arr)) {
            $limit = 20;

            $except_field_ids = '';
            $q = l_mysql_query("SELECT id FROM cms3_object_fields WHERE field_type_id IN "
                    . "(SELECT id FROM cms3_object_field_types "
                    . "WHERE data_type IN ('file','img_file','password','video_file','swf_file'))");

            while ($row = mysql_fetch_row($q)) {
                $except_field_ids .= $row[0] . ', ';
            }

            if ($except_field_ids > 2) {
                $except_field_ids = substr($except_field_ids, 0, -2);
            } else {
                $except_field_ids = '0';
            }

            $q = l_mysql_query("SELECT id FROM cms3_hierarchy_types WHERE (name = 'emarket' AND ext = 'customer') OR (name = 'users' AND ext = 'user')");
            $catalog_htype_ids = '';

            while ($row = mysql_fetch_row($q)) {
                $catalog_htype_ids[] = $row[0];
            }
            $catalog_htype_ids = implode(', ', $catalog_htype_ids);

            if (strlen($catalog_htype_ids) > 0) {
                $q = l_mysql_query("SELECT id, guid FROM cms3_object_types WHERE hierarchy_type_id IN ({$catalog_htype_ids})");
                $catalog_otypes_ids = '';
                $types_guids = array();
                while ($row = mysql_fetch_row($q)) {
                    $catalog_otypes_ids[] = $row[0];
                    $types_guids[$row[0]] = $row[1];
                }

                $catalog_otypes_ids = implode(', ', $catalog_otypes_ids);

                if (strlen($catalog_otypes_ids) > 2) {
                    $query = "SELECT co.id, co.name, co.type_id FROM cms3_objects co "
                            . "INNER JOIN cms3_object_content coc ON co.id = coc.obj_id "
                            . "WHERE co.type_id IN ({$catalog_otypes_ids}) AND coc.field_id NOT IN({$except_field_ids}) AND (";

                    $cols = array('co.name', 'coc.varchar_val', 'coc.text_val');
                    foreach ($cols as $col) {
                        foreach ($search_string_arr as $value) {
                            $query .= $col . " LIKE '%{$value}%' OR ";
                        }
                    }
                    $query = substr($query, 0, -4) . ')';

                    $query .= " GROUP BY co.id ORDER BY co.name ASC LIMIT 0,{$limit}";

                    $q = l_mysql_query($query);

                    $props = array('lname', 'fname', 'father_name', 'e-mail', 'email');
                    $oc = umiObjectsCollection::getInstance();
                    while ($row = mysql_fetch_row($q)) {

                        $obj = $oc->getObject($row[0]);
                        if (is_object($obj)) {
                            $name = '';
                            foreach ($props as $prop) {
                                $value = $obj->getValue($prop);
                                if (strlen($value)) {
                                    $name .= $value . ' ';
                                }
                            }
                            $name = substr($name, 0, -1);

                            if (!strlen($name)) {
                                $name = $row[1];
                            } else {
                                $name = implode(' ', array_unique(explode(' ', $name)));
                            }

                            $status = 'зарегистрирован';
                            if ($types_guids[$row[2]] == 'emarket-customer') {
                                $status = 'не зарегистрирован';
                            }


                            $result[] = array(
                                'attribute:id' => $row[0],
                                'attribute:status' => $status,
                                'attribute:name' => $name,
                            );
                        }
                    }
                }
            }
        }

        return array('total' => count($result), 'items' => array('nodes:item' => $result));
    }

    public function co_getExtendedFields($prop_name) {
        $order = $this->getBasketOrder();
        $customer_id = $order->getValue('customer_id');
        $selected_id = 0;
        if ($prop_name == 'delivery_addresses') {
            $selected_id = $order->getValue('delivery_address');
        } else {
            $selected_id = $order->getValue('legal_person');
        }

        $result = array();
        $result['field']['attribute:name'] = $prop_name;
        $result['field']['attribute:input_name'] = 'data[' . $customer_id . '][' . $prop_name . '][]';
        $result['field']['values'] = array();

        if ($customer_id > 0) {
            $customer_obj = umiObjectsCollection::getInstance()->getObject($customer_id);
            if (is_object($customer_obj)) {
                $prop_obj = $customer_obj->getPropByName($prop_name);
                $values = $customer_obj->getValue($prop_name);

                if (is_object($prop_obj)) {
                    $field_obj = $prop_obj->getField();
                    if (is_object($field_obj)) {
                        $result['field']['attribute:type-id'] = intval($field_obj->getGuideId());
                        $result['field']['attribute:title'] = $field_obj->getTitle();
                    }
                } else {
                    return 0;
                }

                if (is_array($values)) {
                    foreach ($values as $value_id) {
                        $q = l_mysql_query('SELECT name FROM cms3_objects WHERE id = ' . $value_id);
                        $row = mysql_fetch_row($q);
                        if (is_array($row)) {
                            $temp = array('attribute:id' => $value_id, 'node:text' => $row[0]);

                            if ($value_id == $selected_id) {
                                $temp['attribute:selected'] = 'selected';
                            }

                            $result['field']['values']['nodes:item'][] = $temp;
                        }
                    }
                }
            }
        }

        $result['field']['attribute:selected'] = $selected_id;

        return $result;
    }

    public function co_delGuideItem($id) {
        $success = 0;
        if (umiObjectsCollection::getInstance()->delObject($id)) {
            $success = 1;
        }
        return array('result' => $success);
    }

    public function co_checkOrderBeforeSubmit() {
        $errors = array();
        $result = 1;
        $order = $this->getBasketOrder();

        $customer_id = $order->getValue('customer_id');

        if (!$customer_id || $customer_id == permissionsCollection::getInstance()->getUserId()) {
            $errors[] = 'Вы не выбрали покупателя';
        }

        if (!is_array($order->getItems()) || !count($order->getItems())) {
            $errors[] = 'Вы не добавили товары в заказ';
        }

        if (count($errors)) {
            $result = 0;
        }

        return array('result' => $result, 'errors' => array('nodes:item' => $errors));
    }

    public function co_createOrderItem($name, $amount = 1, $price = 0) {
        $oc = umiObjectsCollection::getInstance();
        $h = umiHierarchy::getInstance();

        $dummy_page_obj = null;
        $dummy_page_id = 0;
        $domain_id = cmsController::getInstance()->getCurrentDomain()->getId();
        $lang_id = cmsController::getInstance()->getCurrentLang()->getId();

        $q = l_mysql_query("SELECT id FROM cms3_hierarchy WHERE rel = 0 AND alt_name = 'dummycreateorder' AND domain_id = {$domain_id} AND lang_id = {$lang_id} AND is_deleted = 0");
        $row = mysql_fetch_row($q);
        if (!is_array($row)) {
            $q = l_mysql_query("SELECT id FROM cms3_hierarchy_types WHERE name = 'catalog' AND ext = 'object'");
            $row = mysql_fetch_row($q);
            if (is_array($row)) {
                $dummy_page_id = $h->addElement(0, $row[0], 'Прототип товара/услуги (не удалять!)', 'dummycreateorder');
				permissionsCollection::getInstance()->setDefaultPermissions($dummy_page_id);
            }
        } else {
            $dummy_page_id = $row[0];
        }

        if ($dummy_page_id > 0) {
            $dummy_page_obj = $h->getElement($dummy_page_id, 1);
            $dummy_page_obj->setIsActive(1);
            $dummy_page_obj->commit();

            if (is_object($dummy_page_obj)) {
                $objectTypeId = umiObjectTypesCollection::getInstance()->getBaseType('emarket', 'order_item');
                $new_oitem_obj_id = $oc->addObject($name, $objectTypeId);
                $new_oitem_obj = $oc->getObject($new_oitem_obj_id);
                if (is_object($new_oitem_obj)) {
                    $new_oitem_obj->setValue('item_price', $price);
                    $new_oitem_obj->setValue('item_amount', $amount);
                    $new_oitem_obj->setValue('item_link', array($dummy_page_obj));
                    $new_oitem_obj->setValue('item_total_original_price', $price * $amount);
                    $new_oitem_obj->setValue('item_total_price', $price * $amount);
                    $new_oitem_obj->commit();

                    return orderItem::get($new_oitem_obj_id);
                }
            }

            return $new_oitem_obj;
        }

        return 0;
    }

    public function co_getEditForm($obj_id) {
        $object = umiObjectsCollection::getInstance()->getObject($obj_id);

        if (is_object($object)) {
            return $this->co_getCreateForm($object->getTypeId(), $object);
        }
        return "";
    }

    public function co_getCreateForm($type_id = 0, $object = null) {
        $cc = cmsController::getInstance();
        $data = $cc->getModule('data');
        if (is_object($data)) {
            $block_arr = array();

            $groups_arr = $data->getTypeFieldGroups($type_id);

            if (!is_array($groups_arr)) {
                return "";
            }

            $groups = Array();
            foreach ($groups_arr as $group) {

                if (!$group->getIsActive()) {
                    continue;
                }
                if ($group->getName() == "locks") {
                    continue;
                }

                if (!$group->getIsActive() || !$group->getIsVisible()) {
                    continue;
                }

                $line_arr = Array();

                $fields_arr = $group->getFields();

                $fields = Array();

                foreach ($fields_arr as $field) {
                    if (!$field->getIsVisible() && !$all)
                        continue;
                    if ($field->getIsSystem())
                        continue;

                    $field_type_id = $field->getFieldTypeId();
                    $field_type = umiFieldTypesCollection::getInstance()->getFieldType($field_type_id);
                    $data_type = $field_type->getDataType();

                    if (($field->getName() == 'delivery_addresses' || $field->getName() == 'legal_persons') && $data_type == 'relation' && $field_type->getIsMultiple()) {
                        $is_multiple = $field_type->getIsMultiple();
                        $fieldName = $field->getName();

                        $res = array();

                        $res['attribute:type'] = $data_type;
                        $res['attribute:id'] = $field->getId();

                        if ($field->getIsRequired()) {
                            $res['attribute:required'] = 'required';
                        }
                        if ($tip = $field->getTip()) {
                            $res['attribute:tip'] = $tip;
                        }

                        $res['attribute:name'] = $fieldName;
                        $res['attribute:title'] = $field->getTitle();
                        $res['attribute:tip'] = $field->getTip();
                        $res['attribute:field_id'] = $field->getId();
                        if ($is_multiple)
                            $res['attribute:multiple'] = "multiple";

                        $guide_id = $field->getGuideId();

                        if ($guide_id) {
                            $res['attribute:type-id'] = $guide_id;

                            $guide = umiObjectTypesCollection::getInstance()->getType($guide_id);
                            if ($guide instanceof umiObjectType) {
                                if ($guide->getIsPublic()) {
                                    $res['attribute:public-guide'] = true;
                                }
                            }
                        }

                        $res['attribute:input_name'] = "data[new][{$fieldName}]" . (($is_multiple) ? "[]" : "");

                        $values = array();

                        if (is_object($object)) {
                            $_items = $object->getValue($fieldName);
                            foreach ($_items as $obj_id) {
                                $q = l_mysql_query("SELECT name FROM cms3_objects WHERE id = {$obj_id}");
                                $row = mysql_fetch_row($q);
                                if (is_array($row)) {
                                    $values[] = array(
                                        'attribute:id' => $obj_id,
                                        'attribute:xlink:href' => "uobject://{$obj_id}",
                                        'attribute:selected' => "selected",
                                        'node:text' => $row[0]
                                    );
                                }
                            }
                        }

                        $res['values']['nodes:item'] = $values;

                        $fields[] = $res;
                    } else {
                        $fields[] = $data->renderEditField('notemplate', $field, $object);
                    }
                }

                if (empty($fields))
                    continue;

                $line_arr['attribute:name'] = $group->getName();
                $line_arr['attribute:title'] = $group->getTitle();

                $line_arr['nodes:field'] = $line_arr['void:fields'] = $fields;

                $groups[] = $line_arr;
            }

            $block_arr['nodes:group'] = $block_arr['void:groups'] = $groups;
            return $block_arr;
        }
    }

    public function co_transformXML() {
        $stylesheetfile = getRequest('xslpath');
        $require = getRequest('require');
        $result = file_get_contents("udata://" . $require);
        $response = "";
        $proc = new XSLTProcessor;
        if ($proc->hasExsltSupport()) {
            $xslDoc = new DOMDocument();
            $xslDoc->load(CURRENT_WORKING_DIR . "/" . $stylesheetfile);
            $fpath = CURRENT_WORKING_DIR . "/files/";
            $fname = md5(time()) . ".xml";
            file_put_contents($fpath . $fname, $result);
            $xmlDoc = new DOMDocument();
            $xmlDoc->load($fpath . $fname);
            $proc->importStylesheet($xslDoc);
            $response = $proc->transformToXML($xmlDoc);
            unlink($fpath . $fname);
        }
        $buffer = outputBuffer::current();
        $buffer->charset('utf-8');
        $buffer->contentType('text/html');
        $buffer->clear();
        $buffer->push(str_replace("<?xml version=\"1.0\"?>", "", $response));
        $buffer->end();
    }

    public function co_getRequestVar($varname) {
        if (isset($_REQUEST[$varname])) {
            return $_REQUEST[$varname];
        }
    }

}
