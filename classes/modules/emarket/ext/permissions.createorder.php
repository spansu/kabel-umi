<?php

$permissions = array(
    'control' => array(
        'createorder',
        'co_ajaxSearchItem',
        'co_ajaxSearchItemOptions',
        'co_AddItem',
        'co_ajaxEditItem',
        'co_ajaxRemoveItem',
        'co_ajaxSetDelivery',
        'co_ajaxSetCustomerId',
        'co_getNewCustomerTypeId',
        'co_ajaxSetGuideItem',
        'co_ajaxSetCustomer',
        'co_ajaxGetCustomerInfo',
        'co_ajaxSearchCustomer',
        'co_getExtendedFields',
        'co_delGuideItem',
        'co_checkOrderBeforeSubmit',
        'co_createOrderItem',
        'co_ajaxSetItemDiscount',
        /* common functions */
        'co_getCreateForm',
        'co_getEditForm',
        'co_transformXML',
        'co_getRequestVar',
    ),
);
