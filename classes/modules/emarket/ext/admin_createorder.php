<?php

abstract class admin_createorder {

    public function onImplement() {
        if (cmsController::getInstance()->getCurrentMode() != 'admin')
            return;

        $commonTabs = $this->getCommonTabs();
        if ($commonTabs) {
            $all_tabs = $commonTabs->getAll();
            // пересортируем вкладки, чтобы поставить нашу вкладку 2 по счету
            foreach ($all_tabs as $name => $content) {
                $commonTabs->remove($name);
            };
            $i = 0;
            foreach ($all_tabs as $name => $content) {
                $i++;
                $commonTabs->add($name, $content);
                if ($i == 1) {
                    $commonTabs->add('createorder');
                }
            }
        }
    }

    public function createorder() {
		
        require_once dirname(__FILE__) . "/install_helper.php";
        if(registrateModule('emarket', 'createorder')) {
			$config = mainConfiguration::getInstance();
			$not_allowed_methods = $config->get('cache', 'not-allowed-methods');
            $not_allowed_methods = array_unique(array_merge($not_allowed_methods, array('emarket/order')));
            $config->set('cache', 'not-allowed-methods', $not_allowed_methods);
			$config->set('cache', 'streams.cache-enabled', '0');
		}

        $mode = (string) getRequest('param0');
        if ($mode == "do") {
            $order = $this->getBasketOrder();

            $oEventPoint = new umiEventPoint("emarket_create_order");
            $oEventPoint->setMode("before");
            $oEventPoint->addRef("order", $order);
            $this->setEventPoint($oEventPoint);

            $dataModule = cmsController::getInstance()->getModule("data");
            if ($dataModule) {
                $dataModule->saveEditedObjectWithIgnorePermissions($order->getId(), false, true);
            }

            $order->setName('');
            $order->commit();

            $order->order();

            $oEventPoint = new umiEventPoint("emarket_create_order");
            $oEventPoint->setMode("after");
            $oEventPoint->addRef("order", $order);
            $this->setEventPoint($oEventPoint);

            def_module::redirect('/admin/emarket/order_edit/' . $order->getId() . '/');
        } else {
            $s = new selector('objects');
            $s->types('object-type')->name('emarket', 'order');
            $s->where('name')->equals('dummy-ordercreate');
            $s->where('owner')->equals(permissionsCollection::getInstance()->getUserId());
            //$s->option('no-length')->value(true);
            $s->option('return')->value('id');
            $s->limit(0, 1);
            $result = $s->first;

            if (isset($result['id'])) {
                $order_id = $result['id'];
            } else {
                $order = order::create();
                if (is_object($order)) {
                    $order_obj = umiObjectsCollection::getInstance()->getObject($order->getId());
                    if (is_object($order_obj)) {
                        $order_obj->setName('dummy-ordercreate');
						$order_obj->setValue('need_export', 1);
                        $order_obj->setValue('bonus', 0);
                        $order_obj->commit();
                    }
                }
            }

            if (!isset($order_obj)) {
                $order_obj = umiObjectsCollection::getInstance()->getObject($order_id);
                $order_obj->setValue('delivery_price', 0);
                $order_obj->setValue('delivery_id', 0);
                $order_obj->setValue('bonus', 0);
                $order_obj->setValue('need_export', 1);
                $order_obj->setValue('legal_person', 0);
                $order_obj->setValue('delivery_address', 0);
                $order_obj->setValue('customer_id', permissionsCollection::getInstance()->getUserId());
                $order_obj->commit();
                $this->getBasketOrder()->refresh();
            }

            $order_obj->setValue('order_date', new umiDate());

            $this->setHeaderLabel("header-emarket-createorder");

            $inputData = Array(
                "object" => $order_obj,
                "allowed-element-types" => array('emarket', 'order')
            );
        }

        $this->setDataType("form");
        $this->setActionType("modify");
        $data = $this->prepareData($inputData, "object");

        $this->setData($data);
        return $this->doData();
    }

}

?>