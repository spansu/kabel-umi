<?php

$i18n = array(
    'header-emarket-createorder' => 'Создать заказ',
    'group-order-items' => 'Наименования в заказе',
    'group-customer' => 'Информация о покупателе',
    'sets-automaticly' => 'Оставьте это поле пустым для автоматического расчета',
    'order-add-item' => 'Добавить товар в заказ',
    'order-no-items' => 'В заказе пока нет товаров',
    'perms-emarket-createorder' => 'Создание заказов',
    'calc-delivery-price' => 'Вычислить стоимость доставки',
    'customer-type-tip' => 'Выберите тип покупателя, на которого необходимо оформить заказ',
    'customer-type' => 'Тип покупателя',
    'new-customer' => 'Новый покупатель',
    'existing-customer' => 'Существующий покупатель',
    'customer' => 'Покупатель',
    'module-emarket_createorder' => 'Создать заказ'

);

?>