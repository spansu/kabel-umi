<?php
	class geoip extends def_module {

		public function __construct() {
			parent::__construct();

			$this->loadCommonExtension();

			if(cmsController::getInstance()->getCurrentMode() == "admin") {
				$this->__loadLib("__admin.php");
				$this->__implement("__geoip");

				$this->loadAdminExtension();
			}

			$this->loadSiteExtension();
		}

		/**
		 * @desc Return geographical location
		 * @param  IP-address
		 * @return Array(Longitude, Longitude)
		 */
		public function getPosition($ip = false) {

			$info = $this->lookupIp($ip);
			if (isset ($info['lon']) && isset($info['lat'])) {
				$x = $info['lon'];
				$y = $info['lat'];
			} else {
				$x = null;
				$y = null;
			}

			return array($x, $y);
		}

		/**
		 * @desc   Return geographical location info about the IP-address
		 * @param  IP-address
		 * @return Array()
		 */
		public function lookupIp($ip = false) {

			include_once SYS_MODULES_PATH . "geoip/geobaza.php";

			static $cache = Array();

			if($ip === false) {
				$ip = getServer("REMOTE_ADDR");
			}

			$ip = gethostbyname($ip);

			if(isset($cache[$ip])) return $cache[$ip];

			$query = new GeobazaQuery();
			$obj = $query->get($ip);
			$info = array();

			if ($obj->type == 'special') {
				$info['special'] = $obj->name;
			} else {
				$geobaza = $query->get_path($ip);

				$countryName = @$geobaza->country->translations[0]->ru ? $geobaza->country->translations[0]->ru : @$geobaza->country->translations[0]->en;
				$placeName = @$obj->translations[0]->ru ? $obj->translations[0]->ru : @$obj->translations[0]->en;

				$parentName = null;
				if($parent = $obj->parent) {
					$parentName = @$parent->translations[0]->ru ? $parent->translations[0]->ru : @$parent->translations[0]->en;
				}

				$info['country'] = $countryName;
				$info['region'] = ($parentName != $countryName) ? $parentName : null;
				$info['city'] = ($placeName != $countryName) ? $placeName : null;
				$info['lat'] = $obj->geography->center->latitude;
				$info['lon'] = $obj->geography->center->longitude;
			}

			return $cache[$ip] = $info;
		}
	};

?>