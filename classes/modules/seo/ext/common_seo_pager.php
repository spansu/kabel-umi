<?php

//error_reporting(E_ALL);
//ini_set('display_errors',1);

abstract class common_seo_pager
{
    public function sp_doWork($event) {
		if(cmsController::getInstance()->getCurrentMode() != "admin") {
			if($event->getMode() == 'before') {
				$cc = cmsController::getInstance();
				
				if($cc->getCurrentModule() == 'content' && !$cc->getCurrentElementId()) {
					$regedit = regedit::getInstance();
					$path = getRequest('path');
					if(strlen($path) && $regedit->getVal('//modules/seo/seo_pager_is_enabled')) {
						$matches = array();
						
						$config = mainConfiguration::getInstance();
						$suffix = '';
						if($config->get('seo','url-suffix.add')) {
							$suffix = $config->get('seo','url-suffix');
						}
						preg_match("@/p/[0-9]*{$suffix}@",$path,$matches);
						
						if(is_array($matches) && count($matches)) {
							$p = intval(filter_var($matches[0],FILTER_SANITIZE_NUMBER_INT));
							$_REQUEST['p'] = $p-1;
							$_GET['p'] = $p-1;
							$clean_path = str_replace($matches[0],'',$path).$suffix;
							if($p <= 1) {
								def_module::redirect('/'.$clean_path);
							} else {
								$_REQUEST['path'] = $clean_path;
								$_GET['path'] = $clean_path;
								$cc->analyzePath();
							}
						}
					}
				}
			}
		}
	}
	
	public function numpages($total, $per_page, $template = 'default', $maxPages = 5) {
		$total = intval($total);
		$per_page = intval($per_page);
		$p = intval(getRequest('p'));
		if(!$template) {
			$template = 'default';
		}
		$result = array();
		$items = array();
		$is_xslt = def_module::isXSLTResultMode();
		list($v364f9b183bd2dd9e0beb45c754830a6c, $v28d65bb7a643774ada22f54ca0679289, $v839bf4ce4f682ac29d72f21f39abbd61, $v4a1835dbba88e0fe534a30ac2fa8558a, $vb286163c2d7138578b6b3613433eed50, $v7c958e8e1861de153b774584fc4bca8b, $v0a017ef40a7bd9bd3d55a6a131fb2022, $v95b746440a532f0586864ff101da4948, $vc86804a0ba2ef9ad4f4da84c7288edb4, $vedc51c4694ab5eba9c080cb3832bdf35, $vb5974b598b6ced7f5f1c063fbaac66a2, $va2cbeb34cf8f6b425c8854901c7958b6, $vbfda3d6978660f5cd25c31b54d38eed8) = def_module::loadTemplates("numpages/" . $template, "pages_block", "pages_block_empty", "pages_item", "pages_item_a", "pages_quant", "pages_tobegin", "pages_tobegin_a", "pages_toend", "pages_toend_a", "pages_toprev", "pages_toprev_a", "pages_tonext", "pages_tonext_a");
		
		if(!$total || !$per_page || $total <= $per_page) {
			return ($is_xslt) ? "" : $v28d65bb7a643774ada22f54ca0679289;
		}
		
		$config = mainConfiguration::getInstance();
		$suffix = '';
		if($config->get('seo','url-suffix.add')) {
			$suffix = $config->get('seo','url-suffix');
		}
		
		$path = getRequest('path');
		$path = '/'.substr($path,0,strlen($path) - strlen($suffix));
		if(strpos($path,'seo/numpages') !== FALSE) {
			$path = '';
		}
		
		$params = $_GET;
		$params_todelete = array('p','scheme','path','umi_authorization');
		foreach($params_todelete as $param) {
			if (array_key_exists($param, $params)) {
				unset($params[$param]);
			}
		}
		$params = self::sp_protectParams($params);
		$params_str = (sizeof($params)) ? "?" . http_build_query($params, '', '&') : "";
		if ($is_xslt == false) {
            $params_str = str_replace("%", "&#37;", $params_str);
        }
		$params_str = str_replace(array(
            "<",
            ">",
            "%3C",
            "%3E"
        ), array(
            "&lt;",
            "&gt;",
            "&lt;",
            "&gt;"
        ), $params_str);
		
		
		$limit = ceil($total / $per_page);
        if (!$limit)
            $limit = 1;
		
		for($i = 0; $i < $limit; $i++) {
			
			$page_num = $i+1;
			
			if (($p - $maxPages) >= $i)
                continue;
            if (($p + $maxPages) <= $i)
                break;
			
			$item = array();
			
			if($i == 0) {
				$item['attribute:link'] = $path.$suffix.$params_str;
			} else {
				$item['attribute:link'] = $path.'/p/'.$page_num.$suffix.$params_str;
			}
			
			$item['attribute:page-num'] = $page_num-1;
			
			if($p == $i) {
				$item['attribute:is-active'] = 1;
			}
			
			if ($p != "all") {
                $template_name = ($i == $p) ? $v4a1835dbba88e0fe534a30ac2fa8558a : $v839bf4ce4f682ac29d72f21f39abbd61;
            } else {
                $template_name = $v839bf4ce4f682ac29d72f21f39abbd61;
            }
			$item['node:num']   = $page_num;
			$item['void:quant'] = (($i < (($p + $maxPages) - 1)) and ($i < ($limit - 1))) ? $vb286163c2d7138578b6b3613433eed50 : "";
			$items[] = def_module::parseTemplate($template_name, $item);
		}
		
		$result['subnodes:items'] = $result['void:pages'] = $items;
		
		if(!$is_xslt) {
			$result['tobegin'] = ($p == 0 || $limit <= 1) ? $v0a017ef40a7bd9bd3d55a6a131fb2022 : $v7c958e8e1861de153b774584fc4bca8b;
			$result['toprev']  = ($p == 0 || $limit <= 1) ? $vb5974b598b6ced7f5f1c063fbaac66a2 : $vedc51c4694ab5eba9c080cb3832bdf35;
            $result['toend']   = ($p == ($limit - 1) || $limit <= 1) ? $vc86804a0ba2ef9ad4f4da84c7288edb4 : $v95b746440a532f0586864ff101da4948;
            $result['tonext']  = ($p == ($limit - 1) || $limit <= 1) ? $vbfda3d6978660f5cd25c31b54d38eed8 : $va2cbeb34cf8f6b425c8854901c7958b6;
		}
		
		if ($p != 0) {
            $to_begin = $path.$suffix.$params_str;
            if ($is_xslt) {
                $result['tobegin_link'] = array(
                    'attribute:page-num' => 0,
                    'node:value' => $to_begin
                );
            } else {
                $result['tobegin_link'] = $to_begin;
            }
        }
        if ($p < $limit - 1) {
            $to_end = $path.'/p/'.$limit.$suffix.$params_str;
            if ($is_xslt) {
                $result['toend_link'] = array(
                    'attribute:page-num' => $limit - 1,
                    'node:value' => $to_end
                );
            } else {
                $result['toend_link'] = $to_end;
            }
        }
		
        if ($p - 1 >= 0) {
			if($p-1 > 0) {
				$to_prev = $path.'/p/'.$p.$suffix.$params_str;
			} else {
				$to_prev = $path.$suffix.$params_str;
			}

            if ($is_xslt) {
                $result['toprev_link'] = array(
                    'attribute:page-num' => $p-1,
                    'node:value' => $to_prev
                );
            } else {
                $result['toprev_link'] = $to_prev;
            }
        }
		
        if ($p < $limit - 1) {
            $to_next = $path.'/p/'.($p+2).$suffix.$params_str;
            if ($is_xslt) {
                $result['tonext_link'] = array(
                    'attribute:page-num' => $p + 1,
                    'node:value' => $to_next
                );
            } else {
                $result['tonext_link'] = $to_next;
            }
        }
		
		$result['current-page'] = $p;
		
		return def_module::parseTemplate($v364f9b183bd2dd9e0beb45c754830a6c, $result);
	}
	
	public function sp_protectParams($params) {
		foreach ($params as $key => $param) {
            if (is_array($param)) {
                $params[$key] = self::sp_protectParams($param);
            } else {
				
                $param = htmlspecialchars($param);
                $params[$key] = str_replace(array(
                    "%",
                    "<",
                    ">",
                    "%3C",
                    "%3E"
                ), array(
                    "&#037;",
                    "&lt;",
                    "&gt;",
                    "&lt;",
                    "&gt;"
                ), $param);
            }
        }
        return $params;
	}
	
	public function getCanonical($id = 0) {
        if (!$id) {
            $id = cmsController::getInstance()->getCurrentElementId();
        }

        $h = umiHierarchy::getInstance();
        $needed_id = 0;
        $id = intval($id);
        if (!$id)
            return;

        $parsed_url = parse_url($_SERVER['REQUEST_URI']);
        $has_query = 0;
        if (isset($parsed_url['query'])) {
            $has_query = 1;
        }

        if ($id) {
            $q = l_mysql_query("SELECT obj_id FROM cms3_hierarchy WHERE id = " . $id);
            $res = mysql_fetch_row($q);
            if (isset($res[0])) {
                $ids = $h->getObjectInstances($res[0]);
                if (is_array($ids) && count($ids)) {
                    $needed_id = $ids[0];
                }
            }
        }

        if (($needed_id > 0 && $needed_id != $id) || ($has_query && isset($parsed_url['path']))) {
            if ($needed_id > 0) {
                $path = $h->getPathById($needed_id, 0, 1);
            } else {
                $path = $parsed_url['path'];
            }
            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
            return '<link rel="canonical" href="' . $protocol . $_SERVER['HTTP_HOST'] . $path . '" />';
        }
        return;
    }
}

?>