<?php

abstract class admin_seo_pager
{
    public function onImplement() {
        if (cmsController::getInstance()->getCurrentMode() != 'admin')
            return;

        $commonTabs = $this->getCommonTabs();
        if (is_object($commonTabs)) {
            $commonTabs->add('seo_pager');
        }
    }
	
    public function seo_pager() {
		require_once dirname(__FILE__)."/install_helper.php";
		registrateModule('seo','seo_pager');
		
		$regedit = regedit::getInstance();
		$prefix = 'seo_pager_';
		
		$this->setDataType("settings");
        $this->setActionType("modify");
		
		$params = array(
			"globals" => array(
				'boolean:' . $prefix . 'is_enabled' => intval($regedit->getVal('//modules/seo/' . $prefix . 'is_enabled')),
			)
		);
		
        $data = $this->prepareData($params, "settings");
		
		$mode = (string) getRequest('param0');
        if ($mode == "do") {
			$params = $this->expectParams($params);
			$regedit->setVal('//modules/seo/' . $prefix . 'is_enabled', $params['globals']['boolean:' . $prefix . 'is_enabled']);
			$this->chooseRedirect();
		}
		
        $this->setData($data);
        $this->doData();
    }
}