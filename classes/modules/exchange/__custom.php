<?php
	abstract class __custom_exchange {
		protected static $importDirectory = "/sys-temp/1c_import/";

		protected function saveIncomingFileCustom() {
			clearstatcache();
			$file_name = getRequest('filename');
			$buffer = outputBuffer::current('HTTPOutputBuffer');
			$content = $buffer->getHTTPRequestBody();

			if (!strlen($file_name)) return "failure\nEmpty filename.";
			list($dir_name, , $extension) = array_values(getPathInfo($file_name));
			if (!strlen($extension) || !umiFile::isAllowedFileType($extension)) return "failure\nUnknown file type.";

			if (!isset($_SESSION['1c_latest_catalog-file'])) {
				$_SESSION['1c_latest_catalog-file'] = "";
			}
			$i_flag = ($_SESSION['1c_latest_catalog-file'] == $file_name ? FILE_APPEND : 0);

			$base_name = substr($file_name, 0, strlen($file_name) - strlen($extension) - 1);

			$temp_dir = CURRENT_WORKING_DIR . self::$importDirectory;
			if (!is_dir($temp_dir)) {
				mkdir($temp_dir, 0777, true);
				clearstatcache();
				@chmod($temp_dir, 0777);
			}

			if (strtolower($extension) == "xml") {
				file_put_contents($temp_dir . $base_name . "." . $extension, $content, $i_flag);
				clearstatcache();
				@chmod($temp_dir . $base_name . "." . $extension, 0777);
			} else {
				$quota_byte = getBytesFromString( mainConfiguration::getInstance()->get('system', 'quota-files-and-images') );

				if ( $quota_byte != 0 ) {
					$all_size = getBusyDiskSize(array('/files', '/images'));
					if( ($all_size + strlen($content)) >= $quota_byte) {
						return "failure\n max dirsize in /files and /images summary.";
					}
				}

				$images_dir = CURRENT_WORKING_DIR . "/images/cms/data/" . $dir_name . "/";
				if (!is_dir($images_dir)) {
					mkdir($images_dir, 0777, true);
					clearstatcache();
					@chmod($images_dir, 0777);
				}
				file_put_contents(CURRENT_WORKING_DIR . "/images/cms/data/" . $file_name, $content, $i_flag);
				clearstatcache();
				@chmod(CURRENT_WORKING_DIR . "/images/cms/data/" . $file_name, 0777);
			}

			$_SESSION['1c_latest_catalog-file'] = $file_name;

			return "success";
		}

		protected function importCommerceMLCustom() {
			$file_name = getRequest('filename');
			$file_path = CURRENT_WORKING_DIR . self::$importDirectory . $file_name;

			if (!is_file($file_path)) return "failure\nFile $file_path not exists.";
			$import_offset = (int) getSession("1c_import_offset");

			$blockSize = (int) mainConfiguration::getInstance()->get("modules", "exchange.splitter.limit");
			if($blockSize < 0) $blockSize = 25;

			$splitterName = (string) mainConfiguration::getInstance()->get("modules", "exchange.commerceML.splitter");
			if(!trim(strlen($splitterName))) $splitterName = "commerceML2";

			$splitter = umiImportSplitter::get($splitterName);
			$splitter->load($file_path, $blockSize, $import_offset);
			$doc = $splitter->getDocument();
			$xml = $splitter->translate($doc);

			$oldIgnoreSiteMap =  umiHierarchy::$ignoreSiteMap;
			umiHierarchy::$ignoreSiteMap = true;

			$importer = new xmlImporter();
			$importer->loadXmlString($xml);
			$importer->setIgnoreParentGroups($splitter->ignoreParentGroups);
			$importer->setAutoGuideCreation($splitter->autoGuideCreation);
			$importer->setRenameFiles($splitter->getRenameFiles());
			$importer->execute();

			umiHierarchy::$ignoreSiteMap = $oldIgnoreSiteMap;
			//Сбросить кастомный кэш из memcache
			if(method_exists('regedit', 'onController') && regedit::onController()) {
				mainConfiguration::getInstance()->set("cache", "umiruprefix", '');
			}
			$_SESSION['1c_import_offset'] = $splitter->getOffset();
			if ($splitter->getIsComplete()) {
				$_SESSION['1c_import_offset'] = 0;
				return "success\nComplete. Imported elements: " . $splitter->getOffset();
			}

			return "progress\nImported elements: " . $splitter->getOffset();
		}

		protected function exportOrdersCustom($customOrdersExportTemplate = '') {
			$exporter = umiExporter::get("ordersCommerceML");
			if($exporter instanceof ordersCommerceMLExporter && $customOrdersExportTemplate != '') {
				$exporter->setCustomXslTemplate($customOrdersExportTemplate);
			}
			$exporter->setOutputBuffer();
			$result = $exporter->export(array(), array());
			return $result;
		}

		protected function markExportedOrdersCustom() {
			$sel = new selector('objects');
			$sel->types('object-type')->name('emarket', 'order');
			$sel->where('need_export')->equals(1);
			$orders = $sel->result;
			foreach ($orders as $order) {
				$order->need_export = 0;
			}

			return "success";
		}

		protected function importOrdersCustom() {
			self::saveIncomingFileCustom();
			$file_name = getRequest('filename');
			$file_path = CURRENT_WORKING_DIR . self::$importDirectory . $file_name;

			if (!is_file($file_path)) return "failure\nFile $file_path not exists.";

			$splitterName = (string) mainConfiguration::getInstance()->get("modules", "exchange.commerceML.splitter");
			if(!trim(strlen($splitterName))) $splitterName = "commerceML2";

			$splitter = umiImportSplitter::get($splitterName);
			$splitter->load($file_path);
			$doc = $splitter->getDocument();
			$xml = $splitter->translate($doc);

			$importer = new xmlImporter();
			$importer->loadXmlString($xml);
			$importer->setIgnoreParentGroups($splitter->ignoreParentGroups);
			$importer->setAutoGuideCreation($splitter->autoGuideCreation);
			$importer->execute();


			return "success";
		}



		public function autoimport() {
			$customOrdersExportTemplate = trim(getRequest('orders_export_xsl'));
		
			$timeOut = (int) mainConfiguration::getInstance()->get("modules", "exchange.commerceML.timeout");
			if ($timeOut < 0) $timeOut = 0;

			sleep($timeOut);

			umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;

			$buffer = outputBuffer::current('HTTPOutputBuffer');
			$buffer->charset('utf-8');
			$buffer->contentType('text/plain');

			$type = getRequest("type");
			$mode = getRequest("mode");
			$instance1c = getRequest('param0') ? md5(getRequest('param0')) . "/" : '';
			self::$importDirectory = "/sys-temp/1c_import/" . $instance1c;

			$bBadUser = true;
			$permissionsCollection = permissionsCollection::getInstance();
			$oUser = umiObjectsCollection::getInstance()->getObject( $permissionsCollection->getUserId() );
			$arUserGroupIDs = $oUser->groups;
			$arUserGroupNames = array();
			foreach($arUserGroupIDs as $iUserGroupId) {
				if($oUserGroup = umiObjectsCollection::getInstance()->getObject($iUserGroupId)) {
					$arUserGroupNames[] = $oUserGroup->getName();
				}
			}
			if ( $permissionsCollection->isSv() ||
				( ($permissionsCollection->getUserId() != $permissionsCollection->getGuestId()) && in_array("Администраторы", $arUserGroupNames)) )
			{
				$bBadUser = false;
			}

			if ($bBadUser) {
				$buffer->push("failure\nNot authorized.");
				$buffer->end();
				exit();
			}

			if(method_exists('regedit', 'onController') && regedit::onController() && method_exists('regedit', 'getControllerHost')) {
				$host = regedit::getControllerHost(true);
				if(!is_null($host) && umihost_loader::getWithoutException(array('adminzone_module_manager'))) {
					$exchange1C_module = umihost_system_adminzone_module_manager::getInstance()->getModuleByName(umihost_system_adminzone_module_manager::MODULE_EXCHANGE1C);
					if (!umihost_system_adminzone_module_manager::getInstance()->isModuleAvailable($exchange1C_module, $host)) {
						$buffer->push("failure\nNot permitted.");
						$buffer->end();
						exit();
					}
				}
			}

			switch($type . "-" . $mode) {
				case "catalog-checkauth":
					// clear temp
					removeDirectory(CURRENT_WORKING_DIR . self::$importDirectory);
				case "sale-checkauth": {
					$buffer->push("success\nPHPSESSID\n" . session_id());
				} break;
				case "catalog-init":
				case "sale-init": {
					removeDirectory(CURRENT_WORKING_DIR . self::$importDirectory);
					$maxFileSize = (int) mainConfiguration::getInstance()->get("modules", "exchange.commerceML.maxFileSize");
					if ($maxFileSize <= 0) $maxFileSize = 102400;
					$buffer->push("zip=no\nfile_limit={$maxFileSize}");
				} break;
				case "catalog-file": {
					$buffer->push(self::saveIncomingFileCustom());
				} break;
				case "catalog-import" : {
					$buffer->push(self::importCommerceMLCustom());
				} break;

				case "sale-query" : {
					$buffer->push(self::exportOrdersCustom($customOrdersExportTemplate));
				} break;

				case "sale-success" : {
					$buffer->push(self::markExportedOrdersCustom());
				} break;

				case "sale-file" : {
					$buffer->push(self::importOrdersCustom());
				} break;

				default:
					$buffer->push("failure\nUnknown import type ($type) or mode ($mode).");
			}

			$buffer->end();

			umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = false;

		}

		public function updateCatalogRootOldId($catalog_1c_id) {
			$catalogRootId = umiHierarchy::getInstance()->getIdByPath('/market/', true);
			if ($catalogRootId) {
				$sQuery0 = 'SELECT `id` FROM `cms3_import_sources` WHERE `source_name`=\'commerceML2\'';
				$res0 = mysql_fetch_array(l_mysql_query($sQuery0));

				$source_id = $res0['id'];

				$sQuery1 = 'DELETE FROM `cms3_import_relations` WHERE `new_id`='.$catalogRootId. ' AND `source_id` = '.$source_id;
				$res1 = mysql_fetch_array(l_mysql_query($sQuery1));

				$sQuery2 = 'INSERT INTO `cms3_import_relations`(`source_id`, `old_id`, `new_id`)
							VALUES ('.$source_id.', \''.$catalog_1c_id.'\', '.$catalogRootId.')';
				$res2 = mysql_fetch_array(l_mysql_query($sQuery2));


				$sCacheFile = CURRENT_WORKING_DIR . '/sys-temp/runtime-cache/customCache';
				clearstatcache();
				if (file_exists($sCacheFile)) {
					$arCache = file_get_contents($sCacheFile);
					$arCache = unserialize($arCache);
					if (is_array($arCache)) {
						if (isset($arCache['smartCatalogFilter'])) {
							unset($arCache['smartCatalogFilter']);
							file_put_contents($sCacheFile, serialize($arCache));
							clearstatcache();
							@chmod($sCacheFile, 0777);
						}
					}
				}
			}

		}
	};
?>