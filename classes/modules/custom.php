<?php
	class custom extends def_module {
		public $thumbs_path = "./images/cms/thumbs/";

		public function cms_callMethod($method_name, $args) {
			return call_user_func_array(Array($this, $method_name), $args);
		}

		public function __call($method, $args) {
			throw new publicException("Method " . get_class($this) . "::" . $method . " doesn't exists");
		}
		//TODO: Write your own macroses here

		function dateru($timestamp) {
			if($timestamp == 'now') $timestamp = time();
			$month = array(1=>'января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');
			return date('d ',$timestamp).$month[(int)date('m',$timestamp)].date(' Y',$timestamp);
		}

		///////////////////////////////////////////////////////////////////////////////////////////
		// THUMBNAILS - START
		///////////////////////////////////////////////////////////////////////////////////////////
		public function makeThumbnailSqueeze($path = false, $width = 'auto', $height = 'auto', $template = "default", $returnArrayOnly = false, $flags = 0) {
			if(!$template) $template = "default";

			$path_new = $this->getThumbPath($path, $width, $height);
			if(!$path_new) return "";

			if(!file_exists($path_new) || filemtime($path_new) < filemtime($path)) {
				$result = $this->makeSmartThumbnail($path, $width, $height, $flags, $returnArrayOnly, $template, $path_new);
				if($result !== true) return $result;
			}

			//Parsing
			$value = new umiImageFile($path_new);
			$arr = $this->getThumbDataArray($value, $template);


			if($returnArrayOnly) {
				return $arr;
			} else {
				list($tpl) = def_module::loadTemplates("thumbs/".$template, "image");
				return def_module::parseTemplate($tpl, $arr);
			}
		}


		/**
		 * Perform thumbnailing
		 */
		protected function makeSmartThumbnail($path, $width, $height, $flags, $returnArrayOnly, $template, $path_new) {
			$sConvertCommand = false;
			if(method_exists('regedit', 'onController') && regedit::onController()) {
				require_once(CURRENT_WORKING_DIR . "/../../../../umihost/area_init.php");
				$sConvertCommand = umihost_config::get("Imagemagick", "convert");
			}
			if(!$sConvertCommand || is_null($sConvertCommand)) { //Classic thumb logic
				$image = new umiImageFile($path);
				//Hardcore limits - no thumb
				if($image->getWidth() * $image->getHeight() > 14000000 || strtolower($image->getExt()) == "bmp") {
					return $this->noThumbImage($image, $width, $height, $flags, $returnArrayOnly, $template);
				}

				//Delete old thumb
				if(file_exists($path_new)) { unlink($path_new); }
				//Make thumb
				$this->classicThumbImage($image, $width, $height, $path, $path_new, $flags);
			} else { //ImageMagick thumb logic
				$this->createImageMagickThumb($path, $path_new, $width, $height);
			}

			return true;
		}

		/**
		 * Get array for the thumbnail macro result
		 */
		protected function getThumbDataArray($image, $template) {
			$arr = Array();
			$arr['size'] = $image->getSize();
			$arr['filename'] = $image->getFileName();
			$arr['filepath'] = $image->getFilePath();
			$arr['src'] = $image->getFilePath(true);
			$arr['ext'] = $image->getExt();

			$arr['width'] = $image->getWidth();
			$arr['height'] = $image->getHeight();

			$arr['void:template'] = $template;

			if(cmsController::getInstance()->getCurrentMode() == "admin") {
				$arr['src'] = str_replace("&", "&amp;", $arr['src']);
			}

			return $arr;
		}

		/**
		 * Get relative path to thumbnail (false if wrong file given)
		 */
		protected function getThumbPath($path, $width, $height) {
			clearstatcache();

			if(!file_exists($path)) return false;
			$info = pathinfo($path);

			$file_name = $info['filename'];
			$file_ext = strtolower($info['extension']);
			$src_file_ext = $file_ext;
			$file_ext = ($file_ext=='bmp'?'jpg':$file_ext);

			$allowedExts = Array('gif', 'jpeg', 'jpg', 'png', 'bmp');
			if(!in_array($file_ext, $allowedExts)) return false;

			$thumbPath = sha1($info['dirname']);
			if (!is_dir($this->thumbs_path . $thumbPath)) {
				mkdir($this->thumbs_path . $thumbPath, 0777, true);
				@chmod($this->thumbs_path . $thumbPath, 0777);
			}

			$file_name_new = $file_name . '_' . $width . '_' . $height . '.' . $file_ext;
			$path_new = $this->thumbs_path . $thumbPath . '/' . $file_name_new;

			return $path_new;
		}

		/**
		 * Form answer for macros without making a thumb
		 */
		protected function noThumbImage($image, $width, $height, $flags, $returnArrayOnly, $template) {
			$flags = (int)$flags;

			$arr = $this->getThumbDataArray($image, $template);

			$real_width = $width_src = $image->getWidth();
			$real_height = $height_src = $image->getHeight();

			if($width != 'auto' && $height != 'auto' && $width_src <= $width && $height_src <= $height) {
				$real_width  = $width;
				$real_height = $height;
			} else {
				if ($width == "auto" && $height == "auto"){
					$real_height = $height_src;
					$real_width = $width_src;
				}elseif ($width == "auto" || $height == "auto"){
					if ($height == "auto"){
						// Flag: Reduce only
						if($flags & 0x2 && $width > $width_src) {
							$real_height = $height_src;
							$real_width  = $width_src;
						} else {
							$real_width = (int) $width;
							$real_height = (int) round($height_src * ($width / $width_src));
						}
					}elseif($width == "auto"){
						// Flag: Reduce only
						if($flags & 0x2 && $height > $height_src) {
							$real_height = $height_src;
							$real_width  = $width_src;
						} else {
							$real_height = (int) $height;
							$real_width = (int) round($width_src * ($height / $height_src));
						}
					}
				}else{
					// Flag: Keep proportions
					if($flags & 0x1) {
						$kwidth  = (float) $width / $width_src;
						$kheight = (float) $height / $height_src;
						//Sqeeze till the full image fits defined rectangle (biggest side of the image fits the params)
						if($flags & 0x3) {
							$k = min(array($kwidth, $kheight));
						} else {	// Expand till the smallest side of the image fits the params
							$k = max(array($kwidth, $kheight));
						}
						if(($flags & 0x2) && ($k > 1.0)) {
							$k = 1.0;
						}
						$real_width  = (int) round($width_src * $k);
						$real_height = (int) round($height_src * $k);
					} else {
						$real_width  = $width;
						$real_height = $height;
					}
				}
			}

			$arr['width'] = $real_width;
			$arr['height'] = $real_height;

			if($returnArrayOnly) {
				return $arr;
			} else {
				list($tpl) = def_module::loadTemplates("thumbs/{$template}.tpl", "image");
				return def_module::parseTemplate($tpl, $arr);
			}
		}

		/**
		 * Make image thumb classic CMS way
		 */
		protected function classicThumbImage($image, $width, $height, $path, $path_new, $flags) {
			$flags = (int)$flags;

			$file_ext = strtolower($image->getExt());
			$file_ext = ($file_ext=='bmp'?'jpg':$file_ext);

			$width_src = $image->getWidth();
			$height_src = $image->getHeight();

			if($width_src <= $width && $height_src <= $height) {
				copy($path, $path_new);
				$real_width  = $width;
				$real_height = $height;
			} else {

				if ($width == "auto" && $height == "auto"){
					$real_height = $height_src;
					$real_width = $width_src;
				}elseif ($width == "auto" || $height == "auto"){
					if ($height == "auto"){
						// Flag: Reduce only
						if($flags & 0x2 && $width > $width_src) {
							$real_height = $height_src;
							$real_width  = $width_src;
						} else {
							$real_width = (int) $width;
							$real_height = (int) round($height_src * ($width / $width_src));
						}
					}elseif($width == "auto"){
						// Flag: Reduce only
						if($flags & 0x2 && $height > $height_src) {
							$real_height = $height_src;
							$real_width  = $width_src;
						} else {
							$real_height = (int) $height;
							$real_width = (int) round($width_src * ($height / $height_src));
						}
					}
				}else{
					// Flag: Keep proportions
					if($flags & 0x1) {
						$kwidth  = (float) $width / $width_src;
						$kheight = (float) $height / $height_src;
						//Sqeeze till the full image fits defined rectangle (biggest side of the image fits the params)
						if($flags & 0x3) {
							$k = min(array($kwidth, $kheight));
						} else {	// Expand till the smallest side of the image fits the params
							$k = max(array($kwidth, $kheight));
						}
						if(($flags & 0x2) && ($k > 1.0)) {
							$k = 1.0;
						}
						$real_width  = (int) round($width_src * $k);
						$real_height = (int) round($height_src * $k);
					} else {
						$real_width  = $width;
						$real_height = $height;
					}
				}

				$thumb = imagecreatetruecolor($real_width, $real_height);

				$source_array = $image->createImage($path);
				$source = $source_array['im'];

				if ($real_width*4 < $width_src && $real_height*4 < $height_src) {
					$_TMP = array();
					$_TMP['width'] = round($real_width*4);
					$_TMP['height'] = round($real_height*4);

					$_TMP['image'] = imagecreatetruecolor($_TMP['width'], $_TMP['height']);

					if ($file_ext == 'gif') {
						$_TMP['image_white'] = imagecolorallocate($_TMP['image'], 255, 255, 255);
						imagefill($_TMP['image'], 0, 0, $_TMP['image_white']);
						imagecolortransparent($_TMP['image'], $_TMP['image_white']);
						imagealphablending($source, true);
						imagealphablending($_TMP['image'], true);
					} else {
						imagealphablending($_TMP['image'], false);
						imagesavealpha($_TMP['image'], true);
					}
					imagecopyresampled($_TMP['image'], $source, 0, 0, 0, 0, $_TMP['width'], $_TMP['height'], $width_src, $height_src);

					imageDestroy($source);

					$source = $_TMP['image'];
					$width_src = $_TMP['width'];
					$height_src = $_TMP['height'];

					$offset_w = 0;
					$offset_h = 0;
					unset($_TMP);
				}

				if ($file_ext == 'gif') {
					$thumb_white_color = imagecolorallocate($thumb, 255, 255, 255);
					imagefill($thumb, 0, 0, $thumb_white_color);
					imagecolortransparent($thumb, $thumb_white_color);
					imagealphablending($source, true);
					imagealphablending($thumb, true);
				} else {
					imagealphablending($thumb, false);
					imagesavealpha($thumb, true);
				}

				imagecopyresampled($thumb, $source, 0, 0, 0, 0, $real_width, $real_height, $width_src, $height_src);
				$thumb = makeThumbnailFullUnsharpMask($thumb,80,.5,3);

				switch($file_ext) {
					case 'png':
						$res = imagepng($thumb, $path_new);
						break;
					case 'gif':
						$res = imagegif($thumb, $path_new);
						break;
					default:
						$res = imagejpeg($thumb, $path_new, 100);
				}
				if(!$res) {
					throw new coreException(getLabel('label-errors-16008'));
				}

				imageDestroy($source);
				imageDestroy($thumb);
			}
		}

		/**
		 * Make thumb via ImageMagick lib
		 */
		protected function createImageMagickThumb($path, $path_new, $width, $height) {
			set_time_limit(120);
			if($width == "auto") $width = 0;
			if($height == "auto") $height = 0;
			$path = realpath($path);
			$path_new = str_replace($this->thumbs_path, CURRENT_WORKING_DIR . "/images/cms/thumbs/", $path_new);
			umihost_loader::getLib("imagemagick");
			$imageMagick = new imagemagick();
			$imageMagick->makeThumbnail($path, $path_new, $width, $height);
		}

		///////////////////////////////////////////////////////////////////////////////////////////
		// THUMBNAILS - END
		///////////////////////////////////////////////////////////////////////////////////////////

		/**
		 * Проверка хоста на триальность 1 - триален (ограниченный функционал) 2 - не триален (полный функционал)
		 */
		public function isTrial() {
			$bIsTrial = '0';

			if(method_exists('regedit', 'getControllerHost')) {
				$host = regedit::getControllerHost();
				if(!is_null($host)){
					if(!($host->hasFullFunctionality() && $host->IsDevelop != 1)) {
						$bIsTrial = '1';
					}
				}
			}

			return array( 'result' => $bIsTrial );
		}

		/**
		 * Получение текста в футере по умолчанию. Если сайт создан по партнёрской программе, и у партнёра есть свой текст для футера - подставится он. Иначе - текст по умолчанию.
		 *
		 */
		public function getDefaultFooter() {
			$sFooter = "";

			if(method_exists('regedit', 'getControllerHost')) {
				$host = regedit::getControllerHost();
				if(!is_null($host)){
					$sFooter = "<p><span id=\"footer_umi_ru\"><a href=\"http://www.umi.ru\">UMI</a><span id=\"footer_umi_ru_create_block\"><a id=\"footer_umi_ru_create\" href=\"http://www.umi.ru/create/\">Создать свой сайт</a></span></span>: Открой свой сайт и привлеки клиентов</p>";
					umihost_loader::get(array('partner_domain'));
					$iPartnerDomain = $host->partnerDomainId;
					if(intval($iPartnerDomain) != 0) {
						$oPartnerDomain = umihost_system_partner_domain::getById($iPartnerDomain);
						if(!is_null($oPartnerDomain)) {
							$sPartnerCopyright = (string) $oPartnerDomain->copyright;
							if ($sPartnerCopyright != '') {
								$sFooter = $sPartnerCopyright;
							}
						}
					}
				}
			}

			return array( 'result' => $sFooter );
		}

		/**
		 * Системные настройки и возможности для пользователя
		 */
		public function systemConfig() {
			clearstatcache();
			$userCssTime = file_exists(CURRENT_WORKING_DIR . "/css/user.css") ? filemtime(CURRENT_WORKING_DIR . "/css/user.css") : 0;
			$arParams = array(
				array('attribute:name' => 'user_css', 'attribute:filetime' => $userCssTime, 'node:value' => true),
				array('attribute:name' => 'footer_editable', 'node:value' => true),
				array('attribute:name' => 'default_footer', 'node:value' => ""),
				array('attribute:name' => 'design_name', 'node:value' => ""),
				array('attribute:name' => 'color_name', 'node:value' => ""),
				array('attribute:name' => 'decore_css_path', 'node:value' => "/css/decor.css")
			);
			if(method_exists('regedit', 'getControllerHost')) {
				$host = regedit::getControllerHost(true);
				if(!is_null($host)){
					$arParams = array(
						array('attribute:name' => 'user_css', 'attribute:filetime' => $userCssTime, 'node:value' => $host->userCssAvailable()),
						array('attribute:name' => 'footer_editable', 'node:value' => $host->editFooterAvailable()),
						array('attribute:name' => 'default_footer', 'node:value' => $host->defaultHostFooter()),
						array('attribute:name' => 'design_name', 'node:value' => $host->designName()),
						array('attribute:name' => 'color_name', 'node:value' => $host->colorName()),
						array('attribute:name' => 'decore_css_path', 'node:value' => umihost_system_design::getLinkCss($host->designName(), $host->colorName()) . umihost_system_design::SMART_CSS_FILENAME)
					);
				}
			}

			return def_module::parseTemplate(array(), array('config' => array('nodes:option' => $arParams)));
		}

		/**
		 * Получить html для подключения css-файлов для EIP
		 * @param $cache
		 * @param $build
		 *
		 * @return string
		 */
		public function getCompilledCss($cache, $build) {
			if(method_exists('regedit', 'onController') && regedit::onController()) {
				$result = <<<RESOURCES
			<link rel="stylesheet" type="text/css" href="/js/cms/compiled.css?{$cache}" />
RESOURCES;
			} else {
				$result = <<<RESOURCES
			<link type="text/css" rel="stylesheet" href="/js/cms/eip/design.css?{$build}" />
			<link type="text/css" rel="stylesheet" href="/js/cms/eip/img_editor.css?{$build}" />
			<link type="text/css" rel="stylesheet" href="/js/cms/utils/img_area_select.css?{$build}" />
			<link type="text/css" rel="stylesheet" href="/js/cms/panel/design.css?{$build}" />
			<link type="text/css" rel="stylesheet" href="/js/jquery/ui.all.css?{$build}" />
			<link type="text/css" rel="stylesheet" href="/styles/common/css/popup.css?{$build}" />
			<link type="text/css" rel="stylesheet" href="/styles/skins/mac/design/calendar/calendar.css?{$build}" />
			<link type="text/css" rel="stylesheet" href="/styles/skins/_eip/css/theme.css?{$cache}" />
			<link type="text/css" rel="stylesheet" href="/js/cms/panel/umiru_custom.css?{$cache}" />
			<link type="text/css" rel="stylesheet" href="/js/cms/wysiwyg/tinymce/jscripts/tiny_mce/themes/umiru/skins/default/ui.css?{$cache}" />
RESOURCES;
			}
			return $result;
		}

		/**
		 * Получить html для подключения js-файлов для EIP
		 * @param $cache
		 * @param $build
		 *
		 * @return string
		 */
		public function getCompilledJs($cache, $build) {
			if(method_exists('regedit', 'onController') && regedit::onController()) {
				$result = <<<RESOURCES
			<script type="text/javascript" charset="utf-8" src="/js/cms/compiled.js?{$cache}"></script>
RESOURCES;
			} else {
				$result = <<<RESOURCES
			<script type="text/javascript" charset="utf-8" src="/js/cms/admin.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/panel/panel.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/panel/tickets.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/eip/edit_in_place.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/eip/editor.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/wysiwyg/wysiwyg.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/session.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/utils/rgbcolor.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/styles/common/js/relation.control.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/styles/common/js/symlink.control.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/panel/panel_custom.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/eip/edit_in_place_custom.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/eip/editor_custom.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/wysiwyg/wysiwyg_custom.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/eip/img_editor.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/cms/utils/img_area_select.js?{$build}"></script>
RESOURCES;
			}
			return $result;
		}

		/**
		 * Получить html для подключения jQuery
		 * @param $build
		 *
		 * @return string
		 */
		public function getJQueryCompilled($build) {
			if(method_exists('regedit', 'onController') && regedit::onController()) {
				$result = <<<RESOURCES
			<script type="text/javascript" charset="utf-8" src="/js/cms/jquery.compiled.js?{$build}"></script>
RESOURCES;
			} else {
				$result = <<<RESOURCES
			<script type="text/javascript" charset="utf-8" src="/js/jquery/jquery.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/jquery/jquery-ui.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/jquery/jquery-ui-i18n.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/jquery/jquery.umipopups.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/jquery/jquery.jgrowl_minimized.js?{$build}"></script>
			<script type="text/javascript" charset="utf-8" src="/js/jquery/jquery.cookie.js?{$build}"></script>
RESOURCES;
			}
			return $result;
		}
		
		public function includeQuickEditJsCustom() {
			$config = mainConfiguration::getInstance();
			$old_client_js = (int)$config->get('system', 'use-old-client-js');
			if($old_client_js) {
				return self::includeOldQuickEditJs();
			}
			$compilled_js_file = dirname(__FILE__) . "/../../js/cms/compiled.js";
			$compilled_css_file = dirname(__FILE__) . "/../../js/cms/compiled.css";
			clearstatcache();
			if(!file_exists($compilled_js_file)) {
				//TODO exec compile.php + gzip + chmod
			}
			if(!file_exists($compilled_css_file)) {
				//TODO exec compile.php + gzip + chmod
			}

			$build = regedit::getInstance()->getVal("modules/autoupdate/system_build") . "cmp";

			$sJS = '';

			$permissions = permissionsCollection::getInstance();
			$userId      = $permissions->getUserId();

			if(getSession("fake-user")) {
				$objects = umiObjectsCollection::getInstance();

				$user = $objects->getObject($userId);
				$userName = '';
				if ($user instanceof umiObject) {
					$userName = $user->getValue('fname') . " " . $user->getValue('lname') . " (" . $user->getValue('login') . ")";
				}

				$orderId = (int) getSession('admin-editing-order');
				$orderName = '';
				if ($orderId) {
					$order = $objects->getObject($orderId);
					if ($order instanceof umiObject) {
						$orderName = $order->getName();
					}
				}

				$sJS .= <<<HTML

			<script type="text/javascript" src="/js/jquery/jquery.js?{$build}" charset="utf-8"></script>
			<script type="text/javascript" src="/js/guest.js?{$build}" charset="utf-8"></script>
			<script type="text/javascript" src="/js/jquery/jquery.cookie.js?{$build}" charset="utf-8"></script>

			<script type="text/javascript">
				var FAKE_USER = {
					user_name: '$userName',
					order_name: '$orderName'
				};
			</script>
			<link rel="stylesheet" href="/js/cms/panel/design.css?{$build}" type="text/css" />
			<link rel="stylesheet" href="/styles/skins/_eip/css/theme.css?{$build}" type="text/css" />
			<link rel="stylesheet" href="/js/cms/eip/design.css?{$build}" type="text/css" />
			<script type="text/javascript" src="/js/cms/panel/fakeUser.js?{$build}" charset="utf-8"></script>
			<script type="text/javascript" src="/ulang/common.js?{$build}" charset="utf-8"></script>

HTML;
				return $sJS;
			}

			$isAllowed = ($permissions->isAllowedMethod($userId, "content", "sitetree") && $userId != $permissions->getGuestId());
			if($isAllowed) {
				$cmsController = cmsController::getInstance();
				$langsCollection = langsCollection::getInstance();
				$cache = max(filemtime($compilled_js_file), filemtime($compilled_css_file));
				if (method_exists("regedit", "onController") && regedit::onController()) {
					$onController = "true";
					$controllerHelpPageLink = "http://help-cms.ru/";
					if(!class_exists('umihost_loader')) require_once(CURRENT_WORKING_DIR . "/../../../../umihost/area_init.php");
					if (umihost_loader::get(array('params'))) {
						$controllerHelpPageLink = umihost_system_params::get('adminzone_help_path');
					}
					$showAdminzoneMenu = "false";
				} else {
					$onController = "false";
					$controllerHelpPageLink = "";
					$showAdminzoneMenu = "false";
				}
				$langPrefix = '';
				if($cmsController->getLang()->getId()!=$langsCollection->getDefaultLang()->getId()) {
					$langPrefix = $cmsController->getLang()->getPrefix();
				}

				$session_lifetime = SESSION_LIFETIME;
				$session_access = $permissions->isAllowedModule($userId, 'config') ? 'true' : 'false';

				$csrfToken = getArrayKey($_SESSION, 'csrf_token');

				$eipWYSIWYG = $config->get("edit-in-place", "wysiwyg");
				if (!strlen($eipWYSIWYG)) $eipWYSIWYG = 'tinymce4';

				// инициализация служебной информации о странице для frontend
				$pageId = $cmsController->getCurrentElementId();
				$pageData = json_encode(array(
					'pageId'	=> $pageId,
					'page'		=> array(
						'alt-name'	=> ($pageId) ? umiHierarchy::getInstance()->getElement($pageId)->getAltName() : ''
					),
					'title'		=> def_module::parseTPLMacroses(macros_title()),
					'lang'		=> $cmsController->getCurrentLang()->getPrefix(),
					'lang_id'	=> $cmsController->getCurrentLang()->getId(),
					'domain'	=> $cmsController->getCurrentDomain()->getHost(),
					'domain_id'	=> $cmsController->getCurrentDomain()->getId(),
					'meta'		=> array(
						'keywords'		=> macros_keywords(),
						'description'	=> macros_describtion()
					)
				));
				$compilled_css = $this->getCompilledCss($cache, $build);
				$jquery_compilled = $this->getJQueryCompilled($build);
				$compilled_js = $this->getCompilledJs($cache, $build);

				$sJS .= <<<HTML
			{$compilled_css}
			{$jquery_compilled}
			<script type="text/javascript" charset="utf-8" src="/ulang/common.js?{$build}"></script>
			<script type="text/javascript">
				window.cloudController = {
					onController: {$onController},
					showAdminzoneMenu: {$showAdminzoneMenu},
					helpPageLink: "{$controllerHelpPageLink}"
				};
				window.pageData = {$pageData};
			</script>
			{$compilled_js}
			<script type="text/javascript">
				uAdmin({
					'lang_prefix': '{$langPrefix}',
					'csrf': '{$csrfToken}'
				});
				uAdmin({
					'lifetime' : {$session_lifetime},
					'access'   : {$session_access}
				}, 'session');
				uAdmin('type', '{$eipWYSIWYG}', 'wysiwyg');
			</script>
HTML;
			}
			else {
				$jquery_compilled = $this->getJQueryCompilled($build);
				
				$sJS .= <<<HTML
			{$jquery_compilled}
			<script type="text/javascript" charset="utf-8" src="/js/guest.js?{$build}"></script>
HTML;
			}

			return $sJS;
		}
	};
?>