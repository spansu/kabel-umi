<?php

abstract class common_img_alt {

    public function getAltTitle($imagePath = '', $width = null, $height = null, $class = '') {
        if ($this->img_alt_active()) {
            $regedit = regedit::getInstance();
            $cmsController = cmsController::getInstance();
            $currentLang = $cmsController->getCurrentLang()->getPrefix();

            $imageAlt = "";
            $imageTitle = "";

            $domainId = domainsCollection::getInstance()->getDomainId($_SERVER["HTTP_HOST"]);

            $defaultAlt = $regedit->getVal("//settings/img_alt/$domainId/$currentLang/alt");
            $defaultTitle = $regedit->getVal("//settings/img_alt/$domainId/$currentLang/title");

            if (strpos($imagePath, './') === 0) {
                $imagePath = mb_substr($imagePath, 1);
            }

            if (strlen($imagePath)) {
                $queryResources = l_mysql_query("SHOW TABLES");
                while (($queryData = mysql_fetch_assoc($queryResources))) {
                    
                }
                $queryResources = l_mysql_query("SELECT alt, title FROM ext_img_alt WHERE img_path LIKE '%$imagePath'");
                $queryData = mysql_fetch_assoc($queryResources);
                if (is_array($queryData)) {
                    $imageAlt = $queryData["alt"];
                    $imageTitle = $queryData["title"];
                }

                if ($cmsController->getCurrentMode() != "admin") {
                    if (!strlen($imageAlt))
                        $imageAlt = $defaultAlt;
                    if (!strlen($imageTitle))
                        $imageTitle = $defaultTitle;
                }
            }

            /* Используется для вывода в TPL-шабонизаторе */
            if ($width > 0 || $height > 0) {
                $old_value = intval(def_module::isXSLTResultMode());
                def_module::isXSLTResultMode(true);
                $system = system_buildin_load('system');

                $new_thumb = $system->makeThumbnailFull(".$imagePath", $width, $height, 'notemplate', 0, 1, 5, 0, 100);
                def_module::isXSLTResultMode($old_value);

                if (isset($new_thumb['src'])) {
                    $result = "<img src='{$new_thumb['src']}'";
                    if ($width > 0) {
                        $result .= " width='{$width}'";
                    }
                    if ($height > 0) {
                        $result .= " height='{$height}'";
                    }
                    if (strlen($class)) {
                        $result .= " class='{$class}'";
                    }
                    $result .= " alt='{$imageAlt}' title='{$imageTitle}' />";
                    return $result;
                }
            } else {
                return array("alt" => $imageAlt, "title" => $imageTitle);
            }
        }
    }

    public function img_alt_save() {
        $cmsController = cmsController::getInstance();
        $permissionsColection = permissionsCollection::getInstance();
        if ($permissionsColection->isAllowedMethod($permissionsColection->getUserId(), "img_alt_save")) {
            $imagePath = trim(getRequest("img_path"));
            $imageAlt = trim(getRequest("img_alt"));
            $imageTitle = trim(getRequest("img_title"));

            if (!strlen($imageAlt))
                $imageAlt = "";
            if (!strlen($imageTitle))
                $imageTitle = "";

            l_mysql_query("SET NAMES UTF8");
            if (!strlen($imageAlt) && !strlen($imageTitle) && strlen($imagePath)) {
                l_mysql_query("DELETE FROM ext_img_alt WHERE img_path LIKE '$imagePath'");
                return array("status" => "deleted", "alt" => $imageAlt, "title" => $imageTitle);
            }

            if (strlen($imagePath)) {
                $queryResource = l_mysql_query("SELECT * FROM ext_img_alt WHERE img_path LIKE '$imagePath'");
                $queryData = mysql_fetch_assoc($queryResource);
                if (is_array($queryData)) {
                    $rowId = $queryData["id"];
                    l_mysql_query("UPDATE ext_img_alt SET alt = '$imageAlt', title = '$imageTitle' WHERE id = $rowId");
                    return array("status" => "modified", "alt" => $imageAlt, "title" => $imageTitle);
                } else {
                    l_mysql_query("INSERT INTO ext_img_alt (img_path, alt, title) VALUES('$imagePath', '$imageAlt', '$imageTitle')");
                    return array("status" => "created", "alt" => $imageAlt, "title" => $imageTitle);
                }
            }
        }

        return array("status" => "denined");
    }

    public function img_alt_active() {
        $regedit = regedit::getInstance();
        if ($regedit->getVal("//settings/img_alt/is_active"))
            return true;
    }

    public function img_alt_bufferSend($event) {
        static $img_alt_modified = 0;
        if (!$img_alt_modified) {
            $img_alt_modified = 1;
            $cmsController = cmsController::getInstance();
            $currentMode = $cmsController->getCurrentMode();

            if ($currentMode == 'admin') {
                $buffer = &$event->getRef('buffer');
                $included_scripts = '<link href="/styles/skins/modern/data/modules/content/css/ext_img_alt/edit_modal.css" rel="stylesheet" type="text/css" />';
                $included_scripts .= "<script type=\"text/javascript\">$.getScript('/styles/skins/modern/data/modules/content/js/ext_img_alt/edit_modal.js')</script>";
                $buffer = str_replace('</head>', $included_scripts.'</head>', $buffer);
            }
        }
    }
	
	public function img_alt_sync(iUmiEventPoint $e) {
        $queryResource = l_mysql_query("SELECT * FROM ext_img_alt");
        while(($dataArray = mysql_fetch_assoc($queryResource))) {
            if(is_array($dataArray)) {
                $id = $dataArray["id"];
                $imagePath = $dataArray["img_path"];
                if (!file_exists(CURRENT_WORKING_DIR . $imagePath))
                    l_mysql_query("DELETE FROM ext_img_alt WHERE id = $id");
            }
        }
    }
}
