<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "geoip";
	$INFO['default_method'] = "empty";
	$INFO['default_method_admin'] = "info";
	$INFO['func_perms'] = "Группы прав на функционал модуля";
	$INFO['func_perms/config'] = "Права на администрирование модуля";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/geoip/admin.php";
	$COMPONENTS[] = "./classes/components/geoip/class.php";
	$COMPONENTS[] = "./classes/components/geoip/customAdmin.php";
	$COMPONENTS[] = "./classes/components/geoip/customCommon.php";
	$COMPONENTS[] = "./classes/components/geoip/customMacros.php";
	$COMPONENTS[] = "./classes/components/geoip/handlers.php";
	$COMPONENTS[] = "./classes/components/geoip/i18n.en.php";
	$COMPONENTS[] = "./classes/components/geoip/i18n.php";
	$COMPONENTS[] = "./classes/components/geoip/includes.php";
	$COMPONENTS[] = "./classes/components/geoip/install.php";
	$COMPONENTS[] = "./classes/components/geoip/lang.en.php";
	$COMPONENTS[] = "./classes/components/geoip/lang.php";
	$COMPONENTS[] = "./classes/components/geoip/permissions.php";
	$COMPONENTS[] = "./classes/components/geoip/classes/geobaza.php";
	$COMPONENTS[] = "./classes/components/geoip/data/geobaza.dat";
?>
