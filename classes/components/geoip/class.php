<?php
	/**
	 * Базовый класс модуля "GeoIP".
	 *
	 * Модуль умеет определять geo локацию по ip.
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_geo_ip/
	 */
	class geoip extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();

			if (cmsController::getInstance()->getCurrentMode() == "admin") {
				$this->__loadLib("admin.php");
				$this->__implement("GeoipAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("GeoIPCustomAdmin", true);
			} else {
				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("GeoIPCustomMacros", true);
			}

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("GeoIpCustomCommon", true);
		}

		/**
		 * Возвращает ширину и долготу по ip адресу
		 * @param bool|string $ip ip адрес
		 * @return array
		 */
		public function getPosition($ip = false) {
			$info = $this->lookupIp($ip);

			if (isset($info['lon']) && isset($info['lat'])) {
				$x = $info['lon'];
				$y = $info['lat'];
			} else {
				$x = null;
				$y = null;
			}

			return array($x, $y);
		}

		/**
		 * Возвращает полную информацию о geo локации
		 * по ip адресу
		 * @param bool|string $ip ip адрес
		 * @return array
		 */
		public function lookupIp($ip = false) {
			static $cache = Array();

			if ($ip === false) {
				$ip = getServer("REMOTE_ADDR");
			}

			$ip = gethostbyname($ip);

			if (isset($cache[$ip])) {
				return $cache[$ip];
			}

			$query = new GeobazaQuery();
			$obj = $query->get($ip);
			$info = array();

			if ($obj->type == 'special') {
				$info['special'] = $obj->name;
			} else {
				$geobaza = $query->get_path($ip);

				$countryName = @$geobaza->country->translations[0]->en;

				if (@$geobaza->country->translations[0]->ru) {
					$countryName = $geobaza->country->translations[0]->ru;
				}

				$placeName = @$obj->translations[0]->en;

				if (@$obj->translations[0]->ru) {
					$placeName = $obj->translations[0]->ru;
				}

				$parentName = null;

				if ($parent = $obj->parent) {
					$parentName = @$parent->translations[0]->en;

					if (@$parent->translations[0]->ru) {
						$parentName = $parent->translations[0]->ru;
					}
				}

				$info['country'] = $countryName;
				$info['region'] = ($parentName != $countryName) ? $parentName : null;
				$info['city'] = ($placeName != $countryName) ? $placeName : null;
				$info['lat'] = $obj->geography->center->latitude;
				$info['lon'] = $obj->geography->center->longitude;
			}

			return $cache[$ip] = $info;
		}
	};
?>