<?php
	/**
	 * Класс функционала административной панели
	 */
	class GeoipAdmin {

		use baseModuleAdmin;
		/**
		 * @var geoip $module
		 */
		public $module;

		/**
		 * Возвращает данные для создания формы
		 * определения geo локации по ip.
		 * Если передан ключевой параметр $_REQUEST['param0'] = do,
		 * то определяет geo локацию по ip и возвращает ее.
		 * @throws coreException
		 */
		public function info() {
			$params = Array(
				"global" => Array(
					"string:ip"	=> NULL
				)
			);

			$mode = (string) getRequest('param0');

			if ($mode == "do") {
				$params = $this->expectParams($params);
				$info = $this->module->lookupIp($params['global']['string:ip']);

				if (!isset($info['special'])) {
					$params['geoinfo'] = Array(
						'string:country'	=> $info['country'],
						'string:region'		=> $info['region'],
						'string:city'		=> $info['city'],
						'string:latitude'	=> $info['lat'],
						'string:longitude'	=> $info['lon']
					);
				} else {
					$params['geoinfo'] = array(
						'string:special' => $info['special']
					);
				}
			}

			$this->setDataType('settings');
			$this->setActionType('modify');
			$data = $this->prepareData($params, 'settings');
			$this->setData($data);
			$this->doData();
		}
	}
?>