<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "exchange";
	$INFO['config'] = "0";
	$INFO['default_method'] = "empty";
	$INFO['default_method_admin'] = "import";
	$INFO['func_perms'] = "Группы прав на функционал модуля";
	$INFO['func_perms/auto'] = "Права на интеграция с 1С";
	$INFO['func_perms/exchange'] = "Права на ручной импорт и экспорт";
	$INFO['func_perms/get_export'] = "Права на доступ к экспорту данных по http";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/exchange/1CExchange.php";
	$COMPONENTS[] = "./classes/components/exchange/admin.php";
	$COMPONENTS[] = "./classes/components/exchange/class.php";
	$COMPONENTS[] = "./classes/components/exchange/customAdmin.php";
	$COMPONENTS[] = "./classes/components/exchange/customCommon.php";
	$COMPONENTS[] = "./classes/components/exchange/customMacros.php";
	$COMPONENTS[] = "./classes/components/exchange/handlers.php";
	$COMPONENTS[] = "./classes/components/exchange/i18n.en.php";
	$COMPONENTS[] = "./classes/components/exchange/i18n.php";
	$COMPONENTS[] = "./classes/components/exchange/includes.php";
	$COMPONENTS[] = "./classes/components/exchange/install.php";
	$COMPONENTS[] = "./classes/components/exchange/permissions.php";
?>
