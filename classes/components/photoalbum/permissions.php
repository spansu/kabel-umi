<?php
	/**
	 * Группы прав на функционал модуля
	 */
	$permissions = [
		/**
		 * Права на просмотр фотогелерей
		 */
		'albums' => [
			'album',
			'albums',
			'photo',
			'picasaInit'
		],
		/**
		 * Права на администрирование модуля
		 */
		'albums_list' => [
			'lists',
			'add',
			'edit',
			'del',
			'activity',
			'picasa',
			'uploadimages',
			'upload_arhive',
			'getpicasalink',
			'config',
			'album.edit',
			'photo.edit',
			'publish'
		]
	];
?>