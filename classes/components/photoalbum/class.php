<?php
	/**
	 * Базовый класс модуля "Фотогалереи".
	 *
	 * Модуль управляет следующими сущностями:
	 *
	 * 1) Фотографии;
	 * 2) Фотоальбомы;
	 *
	 * Умеет создавать фотографии путем импорта файлов изображений,
	 * как по дному файлу, так и по несколько.
	 *
	 * Интегрирован с Google Picasa desktop Client, умеет импортировать
	 * из него изображения и альбомы.
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_fotogalerei/
	 */
	class photoalbum extends def_module {
		/**
		 * @var int $per_page ограничение на количество выводимых страниц
		 */
		public $per_page = 10;

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();

			$umiRegistry = regedit::getInstance();

			if ($per_page = (int) $umiRegistry->getVal("//modules/photoalbum/per_page")) {
				$this->per_page = $per_page;
			}

			if (cmsController::getInstance()->getCurrentMode() == "admin") {
				$this->__loadLib("admin.php");
				$this->__implement("PhotoalbumAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("PhotoAlbumCustomAdmin", true);

				$this->__loadLib("import.php");
				$this->__implement("ImportPhotoAlbum");

				$this->__loadLib("googlePicasa.php");
				$this->__implement("GooglePicasaPhotoAlbum");
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("PhotoAlbumMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("PhotoAlbumCustomMacros", true);
			}

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("PhotoAlbumCustomCommon", true);
		}

		/**
		 * Инициализирует работу с Google Picasa и перенаправляет на страницу,
		 * которая отвечает за интеграцию с Google Picasa
		 */
		public function picasaInit() {
			if (strstr(getRequest('path'), "photoalbum/picasa")) {
				$rss = getRequest('rss');
				if ($rss) {
					$cacheDirectory = mainConfiguration::getInstance()->includeParam('system.runtime-cache');
					file_put_contents($cacheDirectory . "picasa", serialize($rss));
				}
			}

			$this->redirect('/admin/photoalbum/picasa/');
		}

		/**
		 * Проверяет существование директории для изображения фотоальбомов.
		 * Если директорий нет - создает их.
		 * Возвращает путь до директории.
		 * @param iUmiHierarchyElement $parent фотоальбом
		 * @return string
		 */
		public function _checkFolder($parent) {
			$folder = USER_IMAGES_PATH . "/cms/data";

			if (!$parent) {
				return $folder . '/';
			}

			if (getRequest("param0") == 0 && getRequest('alt-name')) {
				@mkdir ($folder . "/" . translit::convert(getRequest('alt-name')), 0777);
				return $folder;
			}

			/**
			 * @var iUmiHierarchyElement|iUmiEntinty $parent
			 */
			if (!$parent instanceof iUmiHierarchyElement) {
				return $folder . '/';
			}

			$hierarchy = umiHierarchy::getInstance();
			$parentsIds = $hierarchy->getAllParents($parent->getId(), true);

			if (count($parentsIds) == 0) {
				return $folder . '/';
			}

			$parents = $hierarchy->loadElements($parentsIds);
			$altDirs = [];

			foreach ($parents as $parent) {
				if ($parent->getModule() != 'photoalbum' && $parent->getMethod() != 'album') {
					return $folder . '/';
				}

				$altDirs[] = $parent->getAltName();
			}

			foreach ($altDirs as $alt) {
				$folder .= '/' . $alt ;
				if (!file_exists($folder)) {
					@mkdir($folder, 0777);
				}
			}

			return $folder . '/';
		}

		/**
		 * Возвращает ссылки на страницу редактирование сущности и
		 * страницу добавления дочерней сущности
		 * @param int $element_id идентификатор сущности
		 * @param string $element_type тип сущности
		 * @return array|bool
		 */
		public function getEditLink($element_id, $element_type) {
			$prefix = $this->pre_lang;
			switch ($element_type) {
				case "album": {
					$link_add = $prefix . "/admin/photoalbum/add/{$element_id}/photo/";
					$link_edit = $prefix . "/admin/photoalbum/edit/{$element_id}/";
					return [$link_add, $link_edit];
				}
				case "photo": {
					$link_add = false;
					$link_edit = $prefix . "/admin/photoalbum/edit/{$element_id}/";
					return [$link_add, $link_edit];
				}
				default: {
					return false;
				}
			}
		}
	};
?>