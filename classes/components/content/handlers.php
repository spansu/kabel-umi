<?php

	/**
	 * Класс обработчиков событий
	 */
	class ContentHandlers {
		/** @var content $module */
		public $module;

		/**
		 * Обработчик событий редактирования и перемещения страницы.
		 * Проверяет не изменился ли адрес страницы, если изменился
		 * - добавляет редирект со старого адрес на новый
		 * @param umiEventPoint $e
		 * @return bool
		 */
		public function onModifyPageWatchRedirects(umiEventPoint $e) {
			static $links = [];
			$serviceContainer = umiServiceContainers::getContainer();
			/** @var umiRedirectsCollection $redirects */
			$redirects = $serviceContainer->get('redirects');
			$hierarchy = umiHierarchy::getInstance();

			$element = $e->getRef('element');
			/** @var umiHierarchyElement|iUmiEntinty $element */
			if ($element instanceof umiHierarchyElement == false) {
				return false;
			}

			$elementId = $element->getId();
			$link = $hierarchy->getPathById($elementId, false, false, true);

			if ($e->getMode() == 'before') {
				$links[$elementId] = $link;
				return true;
			}

			if ($links[$elementId] != $link) {
				$redirects->add($links[$elementId], $link, 301);
			}

			return true;
		}

		/**
		 * Проверяет тестовое сообщение
		 * @param iUmiEventPoint $event
		 */
		public function testMessages(iUmiEventPoint $event) {
			$userId = $event->getParam('user_id');
			$umiPermissions = permissionsCollection::getInstance();

			if (!$umiPermissions->isAdmin($userId)) {
				return;
			}

			$umiRegistry = regedit::getInstance();
			$lastTestTime = (int) $umiRegistry->getVal('//settings/last_mess_time');

			if (time() < $lastTestTime + 2592000) {
				return;
			}

			$umiMessages = umiMessages::getInstance();
			$umiMessages->testMessages();
		}

		/**
		 * Обработчик события начала редактирования страницы.
		 * Блокирует редактирования страницы для других пользователей
		 * @param iUmiEventPoint $eEvent события начала редактирования
		 */
		public function systemLockPage(iUmiEventPoint $eEvent) {
			/** @var iUmiHierarchyElement|iUmiEntinty $ePage */
			if ($ePage = $eEvent->getRef("element")) {
				$userId = $eEvent->getParam("user_id");
				$lockTime = $eEvent->getParam("lock_time");
				/** @var iUmiObject|iUmiEntinty $oPage */
				$oPage = &$ePage->getObject();
				$oPage->setValue("locktime", $lockTime);
				$oPage->setValue("lockuser", $userId);
				$oPage->commit();
			}
		}

		/**
		 * Обработчик события сохранения отредактированной страницы
		 * Снимает блокировку редактирования страницы для других пользователей
		 * @param iUmiEventPoint $eEvent событие сохранения
		 */
		public function systemUnlockPage(iUmiEventPoint $eEvent) {
			/** @var iUmiHierarchyElement|iUmiEntinty $ePage */
			if ($ePage = $eEvent->getRef("element")) {
				/** @var iUmiObject|iUmiEntinty $oPage */
				$oPage = $ePage->getObject();
				$oPage->setValue("locktime", null);
				$oPage->setValue("lockuser", null);
				$oPage->commit();
			}
		}

		/**
		 * Обработчик события срабатывания системного крона.
		 * Получает список страниц, которые скоро будут отключены
		 * по истечению времени актуальности контента.
		 * Устанавливает им статус предварительного отключения.
		 * Уведомляет их авторов об этом.
		 * @param iUmiEventPoint $oEvent событие срабатывания крона
		 */
		public function cronSendNotification(iUmiEventPoint $oEvent) {
			$sel = new selector('pages');
			$sel->where('is_active')->equals(1);
			$sel->where('is_deleted')->equals([0, 1]);
			$sel->where('notification_date')->less(time());
			$sel->where('notification_date')->notequals(0);
			$sel->where('expiration_date')->more(time());
			$sel->option('no-permissions')->value(true);
			$result = $sel->result();

			$umiHierarchy = umiHierarchy::getInstance();
			$umiRegistry = regedit::getInstance();
			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$lastCheckDate = (int) $umiRegistry->getVal("//modules/content/last-notification-date");
			$domainsCollection = domainsCollection::getInstance();

			/** @var umiHierarchyElement $page */
			foreach ($result as $page) {
				if ($page instanceof umiHierarchyElement == false) {
					continue;
				}

				$notificationDateObject = $page->getValue('notification_date');

				if (!$notificationDateObject instanceof umiDate) {
					continue;
				}

				$notificationDate = $notificationDateObject->getDateTimeStamp();
				if ($lastCheckDate && ($lastCheckDate - (3600 * 24) < time()) && ($lastCheckDate > $notificationDate)) {
					continue;
				}

				$oPage = $page->getObject();
				$page->setValue("publish_status", $this->module->getPageStatusIdByFieldGUID("page_status_preunpublish"));
				$page->commit();

				if (!$publishComments = $page->getValue("publish_comments")) {
					$publishComments = getLabel('no-publish-comments');
				}

				$userId = $oPage->getOwnerId();
				$user = $umiObjectsCollection->getObject($userId);

				if (!$user instanceof umiObject) {
					continue;
				}

				$userEmail = $user->getValue("e-mail");

				if (!umiMail::checkEmail($userEmail)) {
					continue;
				}

				$mailMessage = new umiMail();
				$from = $umiRegistry->getVal("//settings/email_from");
				$mailMessage->setFrom($from);
				$mailMessage->setPriorityLevel("high");
				$mailMessage->setSubject(getLabel('label-notification-mail-header'));

				list ($body) = content::loadTemplatesForMail("mail/notify", "body");

				$block['notify_header'] = getLabel('label-notification-mail-header');
				$block['page_header'] = $page->getName();
				$block['publish_comments'] = $publishComments;
				$domain = $domainsCollection->getDomain($page->getDomainId());

				$pageId = $page->getId();

				$pageHost = getSelectedServerProtocol() . "://" . $domain->getHost() . $umiHierarchy->getPathById($pageId);
				$block['page_link'] = $pageHost;

				$mailHtml = content::parseTemplateForMail($body, $block, $pageId);

				$mailMessage->addRecipient($userEmail);
				$mailMessage->setContent($mailHtml);
				$mailMessage->commit();
				$mailMessage->send();
			}

			$umiRegistry->setVal("//modules/content/last-notification-date", time());
		}

		/**
		 * Обработчик события срабатывания системного крона.
		 * Снимает с публикации страницы, у которых истекло время актуальности
		 * и уведомляет их авторов об этом
		 * @param iUmiEventPoint $oEvent события срабатывания системного крона
		 */
		public function cronUnpublishPage(iUmiEventPoint $oEvent) {
			$sel = new selector('pages');
			$sel->where('is_active')->equals(1);
			$sel->where('expiration_date')->less(time());
			$sel->where('expiration_date')->notequals(0);
			$sel->option('no-permissions')->value(true);
			$result = $sel->result();

			$umiHierarchy = umiHierarchy::getInstance();
			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$umiRegistry = regedit::getInstance();
			$domainsCollection = domainsCollection::getInstance();

			/** @var umiHierarchyElement $page */
			foreach ($result as $page) {
				if (!$page instanceof iUmiHierarchyElement) {
					continue;
				}

				$page->setIsActive(false);
				$page->setValue("publish_status", $this->module->getPageStatusIdByFieldGUID("page_status_unpublish"));
				$page->commit();

				if (!$publishComments = $page->getValue("publish_comments")) {
					$publishComments = getLabel('no-publish-comments');
				}

				/** @var iUmiObject $pageObject */
				$pageObject = $page->getObject();
				$userId = $pageObject->getOwnerId();
				$user = $umiObjectsCollection->getObject($userId);

				if (!$user instanceof umiObject) {
					continue;
				}

				$userEmail = $user->getValue("e-mail");

				if (!umiMail::checkEmail($userEmail)) {
					continue;
				}

				$mailMessage = new umiMail();
				$from = $umiRegistry->getVal("//settings/email_from");
				$mailMessage->setFrom($from);
				$mailMessage->setPriorityLevel("high");
				$mailMessage->setSubject(getLabel('label-notification-expired-mail-header'));

				list($body) = content::loadTemplatesForMail("mail/expired", "body");

				$block['notify_header'] = getLabel('label-notification-expired-mail-header');
				$block['page_header'] = $page->getName();
				$block['publish_comments'] = $publishComments;
				$domain = $domainsCollection->getDomain($page->getDomainId());

				$pageId = $page->getId();

				$pageHost = getSelectedServerProtocol() . "://" . $domain->getHost() . $umiHierarchy->getPathById($pageId);
				$block['page_link'] = $pageHost;

				$mail_html = content::parseTemplateForMail($body, $block, $pageId);

				$mailMessage->addRecipient($userEmail);
				$mailMessage->setContent($mail_html);
				$mailMessage->commit();
				$mailMessage->send();
			}
		}

		/**
		 * Обработчик события сохранения изменений страницы.
		 * Запускает переключение активности страницы, в зависимости от актуальности контента
		 * @param iUmiEventPoint $event события сохранения изменений
		 */
		public function pageCheckExpiration(iUmiEventPoint $event) {
			if ($inputData = $event->getRef("inputData")) {
				$page = getArrayKey($inputData, "element");
				$this->module->saveExpiration($page);
			}
		}

		/**
		 * Обработчик события создания страницы.
		 * Запускает переключение активности страницы, в зависимости от актуальности контента
		 * @param iUmiEventPoint $event событие создания страницы
		 */
		public function pageCheckExpirationAdd(iUmiEventPoint $event) {
			if ($page = $event->getRef("element")) {
				$this->module->saveExpiration($page);
			}
		}

		/**
		 * Обработчик события сохранения изменений поля сущности.
		 * Проверяет сущность на предмет содержания спама
		 * @param iUmiEventPoint $event событие сохранения изменений поля сущности
		 */
		public function onModifyPropertyAntiSpam(iUmiEventPoint $event) {
			/** @var iUmiEntinty|iUmiHierarchyElement $entity */
			$entity = $event->getRef("entity");
			if (($entity instanceof iUmiHierarchyElement) && ($event->getParam("property") == "is_spam")) {
				$type = umiHierarchyTypesCollection::getInstance()->getTypeByName("faq", "question");
				$contentField = ($type->getId() == $entity->getTypeId()) ? 'question' : 'content';
				antiSpamHelper::report($entity->getId(), $contentField);
			}
		}

		/**
		 * Обработчик события сохранения изменений страницы.
		 * Проверяет страницу на предмет содержания спама
		 * @param iUmiEventPoint $event событие сохранения изменений страницы
		 */
		public function onModifyElementAntiSpam(iUmiEventPoint $event) {
			static $cache = [];
			/** @var iUmiEntinty|iUmiHierarchyElement $element */
			$element = $event->getRef("element");

			if (!$element) {
				return;
			}

			if ($event->getMode() == "before") {
				$data = getRequest("data");
				if (isset($data[$element->getId()])) {
					$oldValue = getArrayKey($data[$element->getId()], 'is_spam');
					if ($oldValue != $element->getValue("is_spam")) {
						$cache[$element->getId()] = true;
					}
				}
			} else {
				if (isset($cache[$element->getId()])) {
					$type = umiHierarchyTypesCollection::getInstance()->getTypeByName("faq", "question");
					$contentField = ($type->getId() == $element->getTypeId()) ? 'question' : 'content';
					antiSpamHelper::report($element->getId(), $contentField);
				}
			}
		}
	}

?>
