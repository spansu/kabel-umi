<?php
	/**
	 * Основной класс контента, отвечает за
	 * 1) Работу со страницами контента
	 * 2) eip
	 * 3) Быстрое редактирование в табличном контроле
	 * 4) Автогенерируемые меню
	 * 5) Общие операции со страницами (управление шаблонами и контролем актуальности)
	 * 6) Работа с тегами страниц
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_struktura/
	 */
	class content extends def_module {

		/** @var int $perPage количество элементов на странице */
		public $perPage;

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();
			$this->perPage = intval(regedit::getInstance()->getVal("//settings/elements_count_per_page"));

			if (cmsController::getInstance()->getCurrentMode() == "admin") {
				$configTabs = $this->getConfigTabs();

				if ($configTabs) {
					$configTabs->add("config");
					$configTabs->add("content_control");
				}

				$commonTabs = $this->getCommonTabs();

				if ($commonTabs instanceof iAdminModuleTabs) {
					$commonTabs->add('sitetree', array('sitetree'));
					$commonTabs->add("tree", array('tree'));
				}

				$this->__loadLib("admin.php");
				$this->__implement("ContentAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("ContentCustomAdmin", true);
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("ContentMacros");

				$this->__loadLib("menu.php");
				$this->__implement("ContentMenu");

				$this->__loadLib("tags.php");
				$this->__implement("ContentTags");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("ContentCustomMacros", true);
			}

			$this->__loadLib("eip.php");
			$this->__implement("EditInPlace");

			$this->__loadLib("handlers.php");
			$this->__implement("ContentHandlers");

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("ContentCustomCommon", true);
		}


		/**
		 * Возвращает список всех родительских страниц для заданной страницы,
		 * список включает саму страницу
		 * @param int $elementId идентификатор страницы
		 * @return Array
		 */
		public function get_parents($elementId) {
			return umiHierarchy::getInstance()->getAllParents($elementId, true);
		}

		/**
		 * Возвращает идентификатор языка для построения меню
		 * @param int $rootPageId идентификатор корневого элемента меню
		 * @param iUmiHierarchy $umiHierarchy коллекция иерархических объектов
		 * @return int
		 * @throws publicAdminException если не удалось получить объект корневого элемента
		 */
		public function getLanguageId($rootPageId, iUmiHierarchy $umiHierarchy) {
			if ($rootPageId == 0) {
				return $umiHierarchy->getCurrentLanguageId();
			}

			$rootPage = $umiHierarchy->getElement($rootPageId);

			if (!$rootPage instanceof iUmiHierarchyElement) {
				throw new publicAdminException('Cannot get root page element');
			}

			return $rootPage->getLangId();
		}

		/**
		 * Возвращает идентификатор домена для построения меню
		 * @param int $rootPageId идентификатор корневого элемента меню
		 * @param iUmiHierarchy $umiHierarchy коллекция иерархических объектов
		 * @return int
		 * @throws publicAdminException если не удалось получить объект корневого элемента
		 */
		public function getDomainId($rootPageId, iUmiHierarchy $umiHierarchy) {
			if ($rootPageId == 0) {
				return $umiHierarchy->getCurrentDomainId();
			}

			$rootPage = $umiHierarchy->getElement($rootPageId);

			if (!$rootPage instanceof iUmiHierarchyElement) {
				throw new publicAdminException('Cannot get root page element');
			}

			return $rootPage->getDomainId();
		}

		/**
		 * Возвращает протокол работы сервера
		 * Враппер к getSelectedServerProtocol()
		 * @return String
		 */
		public function getServerProtocol() {
			return getSelectedServerProtocol();
		}

		/**
		 * Заблокировано ли редактирование страницы пользователем
		 * @param int $element_id идентификатор страницы
		 * @param int $user_id идентификатор пользователя
		 * @return bool
		 */
		public function systemIsLockedById($element_id, $user_id){
			$umiHierarchy = umiHierarchy::getInstance();
			$ePage = $umiHierarchy->getElement($element_id);
			$oPage = $ePage->getObject();
			$lockTime = $oPage->getValue("locktime");
			if ($lockTime == null){
				return false;
			}
			$lockUser = $oPage->getValue("lockuser");
			$lockDuration = regedit::getInstance()->getVal("//settings/lock_duration");
			if (($lockTime->timestamp + $lockDuration) > time() && $lockUser!=$user_id){
				return true;
			}

			return false;
		}

		/**
		 * Возвращает идентификатор пользователя, который
		 * заблокировал редактирование страницы
		 * @param int $element_id идентификатор страницы
		 * @return Mixed
		 */
		public function systemWhoLocked($element_id){
			$umiHierarchy = umiHierarchy::getInstance();
			$ePage = $umiHierarchy->getElement($element_id);
			$oPage = $ePage->getObject();
			return $oPage->getValue("lock_user");
		}

		/**
		 * Отключает блокировку у всех страниц системы
		 * @throws publicAdminException
		 */
		public function systemUnlockAll() {
			$permissions = permissionsCollection::getInstance();

			if (!$permissions->isSv()) {
				throw new publicAdminException(getLabel('error-can-unlock-not-sv'));
			}

			$sel = new umiSelection();
			$sel->forceHierarchyTable(true);
			$result = umiSelectionsParser::runSelection($sel);

			foreach ($result as $page_id) {
				$ePage = umiHierarchy::getInstance()->getElement($page_id);
				$oPage = $ePage->getObject();
				$oPage->setValue("locktime", null);
				$oPage->setValue("lockuser", null);
				$oPage->commit();
				$ePage->commit();
			}
		}

		/**
		 * Запускает отключение блокировки у всех страниц системы
		 * и перенаправляет на HTTP_REFERER
		 * @throws publicAdminException
		 */
		public function unlockAll () {
			$this->systemUnlockAll();
			$this->redirect($_SERVER['HTTP_REFERER']);
		}

		/**
		 * Отключает блокировку у страницы
		 * @param int $pageId идентификатор страницы
		 */
		public function unlockPage($pageId) {
			$element = umiHierarchy::getInstance()->getElement($pageId);
			if ($element instanceof umiHierarchyElement) {
				$pageObject = $element->getObject();
				$pageObject->setValue("locktime", 0);
				$pageObject->setValue("lockuser", 0);
				$pageObject->commit();
			}
		}

		/**
		 * Отключает или включает активность страницы,
		 * в зависимости от настроек актуальности
		 * @param iUmiHierarchyElement $page страницы
		 */
		public function saveExpiration(iUmiHierarchyElement $page) {
			/**
			 * @var iUmiHierarchyElement|iUmiEntinty $page
			 */
			/**
			 * @var iUmiObject|iUmiEntinty $pageObject
			 */
			$pageObject = $page->getObject();
			$expirationTime = $pageObject->getValue('expiration_date');

			if ($expirationTime instanceof umiDate) {
				if ($expirationTime->timestamp > time()) {
					$pageObject->publish_status = $this->getPageStatusIdByFieldGUID("page_status_publish");
					$page->setIsActive(true);
				} elseif ($expirationTime->timestamp < time() && $expirationTime->timestamp != NULL ) {
					$pageObject->publish_status = $this->getPageStatusIdByFieldGUID("page_status_unpublish");
					$page->setIsActive(false);
				}
				$pageObject->commit();
				$page->commit();
			}
		}

		/**
		 * Проверяет разрешено ли пользователю редактирования поля
		 * @param iUmiObject $object объект пользователя
		 * @param string $propName имя поля
		 * @return bool
		 * @throws coreException
		 */
		public function checkAllowedColumn(iUmiObject $object, $propName) {
			$userTypeId = umiHierarchyTypesCollection::getInstance()->getTypeByName('users', 'user')->getId();
			$isSv = permissionsCollection::getInstance()->isSv();
			$isObjectCustomer = $object->getTypeGUID() == 'emarket-customer';
			$isObjectUser = umiObjectTypesCollection::getInstance()->getType($object->getTypeId())->getHierarchyTypeId() == $userTypeId;

			$notAllowedProps = array('bonus', 'spent_bonus', 'filemanager_directory', 'groups');

			if (!$isSv && ($isObjectCustomer || $isObjectUser)) {
				if (in_array($propName, $notAllowedProps)) return false;
			}

			return true;
		}

		/**
		 * Очищает строку от ряда символов
		 * @param string $string строку
		 * @return mixed
		 */
		public function filterString($string) {
			return str_replace("\"", "\\\"", str_replace("'", "\'", $string));
		}

		/**
		 * Возвращает список страниц, которым назначены шаблоны $templates
		 * @param array $templates массив с ID шаблонов
		 * @param int $limit максимальное количество получаемых страниц
		 * @param int $offset смещение, относительно которого будет производиться выборка страниц
		 * @return array массив с объектами iUmiHierarchyElement
		 */
		public function getPagesByTemplatesIdList(array $templates, $limit = 0, $offset = 0) {
			$result = array();
			$templatesCollection = templatesCollection::getInstance();

			/** @var int $templateId */
			foreach ($templates as $templateId) {
				$template = $templatesCollection->getTemplate(trim($templateId));
				if (!$template instanceof iTemplate) {
					continue;
				}

				$relatedPages = $template->getRelatedPages($limit, $offset);

				if (!empty($relatedPages) && is_array($relatedPages)) {
					$result = array_merge($result, $relatedPages);
				}
			}

			return array_unique($result);
		}

		/**
		 * Возвращает количество страниц, которым назначены шаблоны $templates
		 * @param array $templates массив с ID шаблонов
		 * @return int число используемых страниц шаблонами
		 */
		public function getTotalPagesByTemplates(array $templates) {
			$total = 0;

			$templatesCollection = templatesCollection::getInstance();
			/** @var int $templateId */
			foreach ($templates as $templateId) {
				$template = $templatesCollection->getTemplate(trim($templateId));
				if (!$template instanceof iTemplate) {
					continue;
				}

				$total += $template->getTotalUsedPages();
			}

			return $total;
		}

		/**
		 * Меняет у массива дочерних страницы язык и домен
		 * @param array $children массив дочерних страниц
		 * @param int $langId идентификатор языка
		 * @param bool|int $domainId идентификатор домена
		 */
		public function changeChildsLang($children, $langId, $domainId = false) {
			$hierarchy = umiHierarchy::getInstance();

			if (!is_array($children)) {
				return;
			}

			foreach ($children as $elementId => $subChild) {
				$element = $hierarchy->getElement($elementId);
				if ($element instanceof umiHierarchyElement) {
					$element->setLangId($langId);

					if ($domainId) {
						$element->setDomainId($domainId);
					}

					$element->commit();

					if (is_array($subChild) && sizeof($subChild))  {
						$this->changeChildsLang($subChild, $langId, $domainId);
					}
				}
			}
		}

		/**
		 * Возвращает идентификатор статуса публикации по его строковому идентификатору
		 * @param string $statusStringId идентификатор статуса
		 * @return bool|int
		 */
		public function getPageStatusIdByFieldGUID($statusStringId = 'page_status_publish') {
			$objectTypeId = null;
			$fields = umiObjectTypesCollection::getInstance()->getTypeByGUID('root-pages-type');

			/**
			 * @var iUmiField $field
			 */
			foreach ($fields->getAllFields() as $field) {
				if ($field->getName() == 'publish_status') {
					$objectTypeId =  $field->getGuideId();
				}
			}

			if (!$objectTypeId) {
				return false;
			}

			$sel = new umiSelection;
			$sel->addObjectType($objectTypeId);
			$result = umiSelectionsParser::runSelection($sel);
			$umiObjectsCollection = umiObjectsCollection::getInstance();

			foreach ($result as $objectId) {
				$statusId = $umiObjectsCollection->getObject($objectId)->getValue("publish_status_id");

				if ($statusId == $statusStringId) {
					return $objectId;
				}
			}

			return false;
		}

		/**
		 * Возвращает адрес редактирования страницы и адрес добавления дочерней страницы
		 * @param int $elementId идентификатор страницы
		 * @return array
		 */
		public function getEditLink($elementId) {
			return array(
				$this->pre_lang . "/admin/content/add/{$elementId}/page/",
				$this->pre_lang . "/admin/content/edit/{$elementId}/"
			);
		}
	};
?>
