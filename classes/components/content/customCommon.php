<?php
	/**
	 * Класс пользовательских методов для всех режимов
	 */
	class ContentCustomCommon {
		/**
		 * @var content $module
		 */
		public $module;

		public function clearCache() {
			$sDir = rtrim(mainConfiguration::getInstance()->includeParam('system.static-cache'), '/');
			if(getServer('WINDIR') || getServer('windir')) {
				$arDirs = glob($sDir . '/*');
				foreach($arDirs as $item) {
					$item = rtrim($item, "\\/ ") . "/";
					exec("rd /s /q $item");
				}
			} else {
				exec("rm -rf $sDir/*");
			}
		}

		/**
		 * Управление активностью страницы из edit-in-place
		 */
		public function activityControl($elementId, $iState = 0) {
			$template_block = array();
			$hierarchy = umiHierarchy::getInstance();
			$oElement = $hierarchy->getElement($elementId);

			if(!($oElement instanceof umiHierarchyElement)) return def_module::parseTemplate(array(), $template_block);

			$bNewActivity = false;

			switch($iState) {
				case 0:
				{
					if($oElement->getIsActive()) {
						$bNewActivity = false;
					} else {
						$bNewActivity = true;
					}
				}
					break;
				case 1:
				{
					$bNewActivity = true;
				}
					break;
				case -1:
				{
					$bNewActivity = false;
				}
					break;
				default:
				{
				}
			}
			$oElement->setIsActive($bNewActivity);
			$oElement->commit();
			$oElement->update();
			$iNewState = ($bNewActivity) ? 1 : -1;
			$arResult = array(
				'success' => true,
				'result' => $iNewState
			);

			$event_after = new umiEventPoint("customElementActivityChanged");
			$event_after->addRef("element", $oElement);
			$event_after->setMode("after");
			$event_after->call();

			$this->clearCache();

			return $arResult;
		}

		/**
		 * Переместить элемент в начало, если требуется
		 * @param umiHierarchyElement $element
		 */
		public function moveElementToTop(&$element) {
			//check if need to move
			$parentId = umiHierarchy::getInstance()->getParent($element->getId());
			if($parentId) {
				$iTypeId = $element->getObjectTypeId();
				$oType = umiObjectTypesCollection::getInstance()->getType($iTypeId);
				if($oType && (
						($oType->getModule() == "catalog" && $oType->getMethod() == "category") ||
						($oType->getModule() == "catalog" && $oType->getMethod() == "object") ||
						($oType->getModule() == "photoalbum" && $oType->getMethod() == "album") ||
						($oType->getModule() == "photoalbum" && $oType->getMethod() == "photo")
					)
				) {
					//move
					$element->commit();
					$element->update();
					$res = umiHierarchy::getInstance()->moveFirst($element->getId(), $parentId);
					$element->commit();
				}
			}
		}

		/**
		 * Запрет на создание элементов типа "Авторская информация" + перемещение элемента в начало
		 */
		public function onAfterAddElement(iUmiEventPoint $event) {

			if($event->getMode() == "after") {
				$element = $event->getRef("element");

				if($element instanceof umiHierarchyElement) {
					$this->moveElementToTop($element);
				}
			}
		}

		/**
		 * перемещение элемента в начало
		 * @param iUmiEventPoint $event
		 */
		public function onAfterQuickAddElement(iUmiEventPoint $event) {
			if($event->getMode() == "after") {
				$elementId = $event->getParam('elementId');
				$element = umiHierarchy::getInstance()->getElement($elementId);

				if($element instanceof umiHierarchyElement) {
					$this->moveElementToTop($element);
				}
			}
		}

		/**
		 * Установка галочек для видимости страницы в меню при импорте
		 */
		public function onAfterImportElementMenuVisible(iUmiEventPoint $event) {
			if($event->getMode() === "after") {
				$element = $event->getRef("element");
				$this->setElementCheckboxes($element);
			}
		}

		/**
		 * Установить галочек для видимости страницы в меню после события eipQuickAdd
		 */
		public function onAfterQuickAddElementMenuVisible(iUmiEventPoint $event) {
			if($event->getMode() === "after") {
				$iElementId = $event->getParam('elementId');
				if($element = umiHierarchy::getInstance()->getElement($iElementId)) {
					$this->setElementCheckboxes($element);
				}
			}
		}

		/**
		 * Установить галочек для видимости страницы в меню после события systemCreateElement
		 */
		public function onAfterAddElementSetMenuVisible(iUmiEventPoint $event) {
			if($event->getMode() == "after") {
				$element = $event->getRef("element");
				$this->setElementCheckboxes($element);
			}
		}

		/**
		 * Установка галочек для страницы
		 *
		 * @param umiHierarchyElement $element Объект страницы
		 */
		public function setElementCheckboxes($element) {
			$iTypeId = $element->getObjectTypeId();

			$oType = umiObjectTypesCollection::getInstance()->getType($iTypeId);
			if($oType) {
				//Невидимость в меню - по умолчанию
				$element->setIsVisible(false);
				$element->show_submenu = false;
				$element->is_expanded = false;

				//Видимость в меню
				$arMenuExcludeTypes = array("Авторская информация");
				if(!in_array($oType->getName(), $arMenuExcludeTypes)) {
					$arMenuTypesList = $this->getVisibleInMenuTypesList();
					foreach($arMenuTypesList as $arModuleMethod) {
						if($oType->getModule() == $arModuleMethod['module'] && $oType->getMethod() == $arModuleMethod['method']) {
							$element->setIsVisible(true);
							$element->show_submenu = true;
							$element->is_expanded = true;
						}
					}
				}

				//Исключение из поиска и индексации
				$arNotSearchTypesList = $this->getNotInSearchIndexTypesList();
				foreach($arNotSearchTypesList as $arNotInSearchType) {
					if((isset($arNotInSearchType['name']) && $oType->getName() == $arNotInSearchType['name']) ||
						(isset($arNotInSearchType['module']) && $oType->getModule() == $arNotInSearchType['module'] && $oType->getMethod() == $arNotInSearchType['method'])
					) {
						$element->robots_deny = true;
						$element->is_unindexed = true;
					}
				}
				$element->commit();
			}
		}

		/**
		 * Список типов данных, которые могут быть видимы в меню
		 *
		 * @return array
		 */
		public function getVisibleInMenuTypesList() {
			$arTypes = array(
				array("module" => "content", "method" => ""),
				array("module" => "catalog", "method" => "category"),
				array("module" => "news", "method" => "rubric"),
				array("module" => "photoalbum", "method" => "album"),
				array("module" => "blogs20", "method" => "blog"),
				array("module" => "blogs", "method" => "blog"),
				array("module" => "webforms", "method" => "page"),
				array("module" => "faq", "method" => "category"),
				array("module" => "faq", "method" => "project"),
			);

			//Получить объект страницы "Для вставки"
			$infoObject = false;
			$oHierarchyType = umiHierarchyTypesCollection::getInstance()->getTypeByName('content', '');
			$arTypes2 = umiObjectTypesCollection::getInstance()->getTypesByHierarchyTypeId($oHierarchyType->getId());
			foreach($arTypes2 as $typeId => $sTypeName) {
				if($sTypeName == "Авторская информация") {
					$sel = new selector('objects');
					$sel->types('object-type')->id($typeId);
					$sel->limit(0, 1);
					$infoObject = $sel->first;
					break;
				}
			}

			if($infoObject) {
				$fieldExists = umiObjectTypesCollection::getInstance()->getType($infoObject->getTypeId())->getFieldId('show_catalog_objects_in_menu');
				if(!$fieldExists || $infoObject->getValue('show_catalog_objects_in_menu')) {
					$arTypes[] = array("module" => "catalog", "method" => "object");
				}
			} else {
				$arTypes[] = array("module" => "catalog", "method" => "object");
			}

			return $arTypes;
		}

		/**
		 * Список типов данных, которые должны быть исключены из поиска и индексации
		 *
		 * @return array
		 */
		public function getNotInSearchIndexTypesList() {
			return array(
				array("name" => "Авторская информация"),
				array("module" => "comments", "method" => "comment"), //Комментарии добавляются через /comments/post, обработчик - в модуле comments, но при импорте - надо чтобы были
				array("module" => "blogs", "method" => "blog_comment"),
				array("module" => "blogs20", "method" => "comment"),
				array("module" => "faq", "method" => "question"),
			);
		}

		/**
		 * Getter for optioned field
		 * expect @element-id, @property-id, @property-name
		 * @return optioned field and guide
		 */
		public function getOptionedAndGuide() {
			$elId = getRequest('element-id');
			$fieldId = getRequest('property-id');
			$fieldName = getRequest('property-name');
			$guideItems = array();

			$element = umiHierarchy::getInstance()->getElement($elId);
			$value = ($element) ? $element->getValue($fieldName) : array();
			$field = umiFieldsCollection::getInstance()->getField($fieldId);
			if($field) {
				$guideId = $field->getGuideId();
				if($guideId) {
					$guideItemsIDs = umiObjectsCollection::getInstance()->getGuidedItems($guideId);
					foreach($guideItemsIDs as $id => $name) {
						$guideItems[] = array(
							'id' => $id,
							'name' => $name
						);
					}
				}
			}
			return def_module::parseTemplate(array(), array(
					'optioned' => array('nodes:value' => $value),
					'guide' => array(
						'attribute:id' => $guideId,
						'nodes:item' => $guideItems
					))
			);
		}

		/**
		 * Setter for Optioned fields
		 * @return fields value & related guide after save
		 */
		public function saveOptioned() {
			$data = getRequest('data');
			if(!is_array($data)) {
				return;
			}

			$fields = array();

			foreach($data as $i) {
				if($i == 'false') {
					continue;
				}
				$elId = $i['element-id'];
				$fieldId = $i['field-id'];
				$fieldName = $i['field-name'];
				$newValue = $i['value'];

				$guideId = umiFieldsCollection::getInstance()->getField($fieldId)->getGuideId();

				foreach($newValue as $key => $val) {
					if($val['rel'][0] == '_') {
						$newGuideItemID = umiObjectsCollection::getInstance()->addObject(substr($val['rel'], 1), $guideId);
						$newValue[$key]['rel'] = $newGuideItemID;
					}
				}

				$guideItems = umiObjectsCollection::getInstance()->getGuidedItems($guideId);

				$element = umiHierarchy::getInstance()->getElement($elId);
				$element->setValue($fieldName, $newValue);
				$element->commit();
				$value = ($element) ? $element->getValue($fieldName) : "[]";
				array_push($fields, array(
					'element-id' => $elId,
					'field-id' => $fieldId,
					'field-name' => $fieldName,
					'value' => $value,
					'guide' => $guideItems
				));
			}

			$this->clearCache();

			$fields['total'] = count($fields);

			return $fields;
		}

		/**
		 * Return children of page by page-id
		 */
		public function getTreeNodes() {
			// Cross domain AJAX header
			outputBuffer::current()->header('Access-Control-Allow-Headers', 'X-Prototype-Version,X-Requested-With');
			outputBuffer::current()->header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');
			outputBuffer::current()->header('Access-Control-Allow-Origin', '*');
			// Parse raw post data - if request was cross-domain from IE
			$vars = array();
			$raw_post_data = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents("php://input");
			if($raw_post_data && !empty($raw_post_data)) {
				parse_str($raw_post_data, $vars);
				foreach($vars as $key => $value) {
					$_REQUEST[$key] = $value;
					$_POST[$key] = $value;
				}
			}

			$elId = intval(getRequest('parent-id'));

			$hierarchy = umiHierarchy::getInstance();

			$sel = new selector('pages');
			$sel->where('is_active')->isnull(false);
			$sel->where('hierarchy')->page($elId)->childs(1);

			$elements = $sel->result;

			$result = Array();
			/** @var umiHierarchyElement $element */
			foreach($elements as $element) {
				if($element->getName() == "Для вставки") {
					continue;
				}
				$result[] = Array(
					'title' => $element->getName(),
					'key' => $element->getId(),
					'module' => $element->getModule(),
					'method' => $element->getMethod(),
					'isActive' => $element->getIsActive() ? 1 : 0,
					'inMenu' => $element->getIsVisible() ? 1 : 0,
					'hasChildren' => !!($hierarchy->getChildsCount($element->getId())),
					//in case of performance issues
					//'hasChildren' => '1'
					'url' => $hierarchy->getPathById($element->getId()),
					'nodelete' => $element->getIsDefault()
				);
			}
			$result['total'] = sizeof($result);

			$result['module'] = 'content';
			$result['method'] = 'getTreeNodes';

			/** @var HTTPOutputBuffer $buffer */
			$buffer = outputBuffer::current();
			$buffer->clear();
			$buffer->contentType("text/javascript");
			$buffer->push(json_encode($result));
			$buffer->end();
			return;
		}

		/**
		 * Load tree,
		 * expect root-id and depth
		 */
		public function getInitialTree() {
			// Cross domain AJAX header
			outputBuffer::current()->header('Access-Control-Allow-Headers', 'X-Prototype-Version,X-Requested-With');
			outputBuffer::current()->header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');
			outputBuffer::current()->header('Access-Control-Allow-Origin', '*');
			// Parse raw post data - if request was cross-domain from IE
			$vars = array();
			$raw_post_data = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents("php://input");
			if($raw_post_data && !empty($raw_post_data)) {
				parse_str($raw_post_data, $vars);
				foreach($vars as $key => $value) {
					$_REQUEST[$key] = $value;
					$_POST[$key] = $value;
				}
			}

			$rootId = intval(getRequest('root-id'));
			$depth = intval(getRequest('depth'));

			function makeTree($elId, $depth) {
				$node = Array();
				if($elId == 0) {
					$node['title'] = 'root';
					$node['key'] = 0;
					$node['hasChildren'] = 1;
					$node['url'] = '/';
					$node['unselectable'] = true;
					$node['nodelete'] = true;
				} else {
					$element = umiHierarchy::getInstance()->getElement($elId);
					$name = (string)$element->getName();
					if($name == "Для вставки") {
						return;
					}
					if($name) {
						$node['title'] = $name;
					}
					$module = (string)$element->getModule();
					if($module) {
						$node['module'] = $module;
					}
					$method = (string)$element->getMethod();
					if($method) {
						$node['method'] = $method;
					}
					if($element->getIsDefault()) {
						$node['nodelete'] = true;
					}
					$node['key'] = $element->getId();
					$node['hasChildren'] = 0;
					$node['isActive'] = $element->getIsActive() ? 1 : 0;
					$node['inMenu'] = $element->getIsVisible() ? 1 : 0;
					$node['url'] = umiHierarchy::getInstance()->getPathById($elId);
				}

				$sel = new selector('pages');
				$sel->where('is_active')->isnull(false);
				$sel->where('hierarchy')->page($elId)->childs(1);
				$children = $sel->result;

				if(!empty($children)) {
					$node['hasChildren'] = 1;
					if($depth > 0) {
						$node['children'] = Array();
						/** @var umiHierarchyElement $child */
						foreach($children as $child) {
							$child = makeTree($child->getId(), $depth - 1);
							if($child) {
								$node['children'][] = $child;
							}
						}
						$node['children']['total'] = sizeof($node['children']);
					}
				}

				return $node;
			}

			$result = makeTree($rootId, $depth);
			$result['module'] = 'content';
			$result['method'] = 'getInitialTree';

			/** @var HTTPOutputBuffer $buffer */
			$buffer = outputBuffer::current();
			$buffer->clear();
			$buffer->contentType("text/javascript");
			$buffer->push(json_encode($result));
			$buffer->end();
			return;
		}

		public function treeAction() {
			// Cross domain AJAX header
			outputBuffer::current()->header('Access-Control-Allow-Headers', 'X-Prototype-Version,X-Requested-With');
			outputBuffer::current()->header('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');
			outputBuffer::current()->header('Access-Control-Allow-Origin', '*');
			// Parse raw post data - if request was cross-domain from IE
			$vars = array();
			$raw_post_data = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents("php://input");
			if($raw_post_data && !empty($raw_post_data)) {
				parse_str($raw_post_data, $vars);
				foreach($vars as $key => $value) {
					$_REQUEST[$key] = $value;
					$_POST[$key] = $value;
				}
			}

			$action = getRequest('action');
			$params = getRequest('params');

			$res = Array();
			$status = 'ok';
			$msg = '';

			$hierarchy = umiHierarchy::getInstance();

			switch($action) {
				case 'setIsActive':
					foreach($params as $param) {
						$pageId = intval($param['page-id']);
						$newValue = $param['value'];
						if(!$pageId) {
							$status = 'error';
							$msg = 'undefined or invalid "page-id"';
							break;
						}
						if(!($newValue == 0 || $newValue == 1)) {
							$status = 'error';
							$msg = 'undefined or invalid "value", should be 0 or 1 only';
							break;
						}
						$element = $hierarchy->getElement($pageId);
						if(!$element) {
							$status = 'error';
							$msg = 'invalid page-id';
							break;
						}
						$element->setIsActive($newValue);
					}
					break;
				case 'setShowInMenu':
					foreach($params as $param) {
						$pageId = intval($param['page-id']);
						$newValue = $param['value'];

						if(!$pageId) {
							$status = 'error';
							$msg = 'undefined or invalid page-id: ' . $pageId;
							break;
						}

						if(!($newValue == 0 || $newValue == 1)) {
							$status = 'error';
							$msg = 'invalid value: ' . $newValue . 'for page-id: ' . $pageId;
							break;
						}

						$element = $hierarchy->getElement($pageId);

						if(!$element) {
							$status = 'error';
							$msg = 'undefined or invalid page-id: ' . $pageId;
							break;
						}

						$element->setIsVisible($newValue);
						$object = $element->getObject();
						$object->setValue('is_expanded', $newValue);
						$object->setValue('show_submenu', $newValue);
						$object->commit();
					}
					break;
				case 'move':
					foreach($params as $param) {
						$pageId = intval($param['page-id']);
						$relId = intval($param['parent-id']);
						$beforeId = intval($param['before-id']);

						$action = $hierarchy->moveBefore($pageId, $relId, $beforeId);

						if(!$action) {
							$status = 'error';
							$msg = 'cannot move page with params page-id: '
								. $pageId . ', parent-id: ' . $relId . ', before-id: ' . $beforeId;
							break;
						}
					}
					break;
				case 'delete':
					$pages = array();
					$res['pages'] = array();
					foreach($params as $param) {
						$pageId = intval($param['page-id']);

						if(!$pageId) {
							$status = 'error';
							$msg = 'undefined or invalid page-id: ' . $pageId;
							continue;
						}

						$page = $hierarchy->getElement($pageId, true);
						if(!$page) {
							$status = 'error';
							$msg = 'cannot delete page with page-id: ' . $pageId;
							continue;
						}
						if($page && $page->getIsDefault()) {
							$status = 'error';
							$msg = 'cannot delete default page with page-id: ' . $pageId;
							continue;
						}
						$hierarchy->unloadElement($pageId);
						$pages[] = $pageId;

					}
					foreach($pages as $pageId) {
						$action = $hierarchy->delElement($pageId);
						if(!$action) {
							$status = 'error';
							$msg = 'cannot delete page with page-id: ' . $pageId;
							continue;
						}
						$res['pages'][] = $pageId;
					}
					break;
				default:
					$status = 'error';
					$msg = 'undefined action';
			}

			$this->clearCache();

			if($status) {
				$res['status'] = $status;
			}
			if($msg) {
				$res['msg'] = $msg;
			}

			$res['module'] = 'content';
			$res['method'] = 'treeAction';
			/** @var HTTPOutputBuffer $buffer */
			$buffer = outputBuffer::current();
			$buffer->clear();
			$buffer->contentType("text/javascript");
			$buffer->push(json_encode($res));
			$buffer->end();
			return;
		}

		public function customSitemap() {
			$limit = 500;
			$offset = 0;

			$pages = new selector('pages');
			$pages->where('is_deleted')->equals(false);
			$pages->where('is_active')->equals(true);
			$pages->where('robots_deny')->isnull(true);
			$pages->order('id')->asc();
			$pages->limit($offset, $limit);
			$result = $pages->result();
			$items = array('nodes:page' => array());
			$pageNum = 1;

			while(!empty($result)) {
				/** @var umiHierarchyElement $page */
				foreach($result as $page) {
					$item = array();
					$item['link'] = $page->link;
					$item['update_time'] = date("c", $page->getUpdatetime());
					$items['nodes:page'][] = $item;
					umiHierarchy::getInstance()->unloadElement($page->getId());
				}

				$pages = new selector('pages');
				$pages->where('is_deleted')->equals(false);
				$pages->where('is_active')->equals(true);
				$pages->where('robots_deny')->isnull(true);
				$pages->order('id')->asc();
				$pages->limit(intval($limit * $pageNum), $limit);
				$result = $pages->result();
				$pageNum++;
			}

			return content::parseTemplate('', $items);
		}

	}
?>