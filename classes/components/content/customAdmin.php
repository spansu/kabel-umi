<?php
	/**
	 * Класс пользовательских методов административной панели
	 */
	class ContentCustomAdmin {
		/**
		 * @var content $module
		 */
		public $module;

		public function adminsitetree() {
			$domains = domainsCollection::getInstance()->getList();
			$permissions = permissionsCollection::getInstance();
			$user_id = $permissions->getUserId();

			$this->module->setDataType("list");
			$this->module->setActionType("view");

			/**
			 * @var int $i
			 * @var domain $domain
			 */
			foreach($domains as $i => $domain) {
				$domain_id = $domain->getId();

				if(!$permissions->isAllowedDomain($user_id, $domain_id)) {
					unset($domains[$i]);
				}
			}

			$data = $this->module->prepareData($domains, "domains");

			$this->module->setData($data, sizeof($domains));
			$this->module->doData();
		}

	}
?>