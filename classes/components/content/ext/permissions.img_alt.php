<?php

$permissions = array(
    'content' => array(
        'getAltTitle', 
        'img_alt_active',
        'img_alt_bufferSend',
		'img_alt_sync',
    ),
    
    'sitetree' => array(
        'img_alt_save'
    )
);