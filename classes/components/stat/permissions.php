<?php
	/**
	 * Группы прав на функционал модуля
	 */
	$permissions = [
		/**
		 * Права на просмотр переходов на сайте
		 */
		'json_get_referer_pages' => [
			'json_get_referer_pages'
		],
		/**
		 * Права на просмотр отчетов статистики
		 */
		'total' => [
			'phrases',
			'phrase',
			'popular_pages',
			'engines',
			'engine',
			'sources',
			'sources_domain',
			'visitors',
			'visitors_by_date',
			'visitor',
			'sectionHits',
			'sectionHitsIncluded',
			'visits',
			'visits_sessions',
			'visits_visitors',
			'auditoryActivity',
			'auditoryLoyality',
			'visitDeep',
			'visitTime',
			'entryPoints',
			'paths',
			'exitPoints',
			'openstatCampaigns',
			'openstatServicesByCampaign',
			'openstatAdsByService',
			'openstatServices',
			'openstatSources',
			'openstatServicesBySource',
			'openstatAds',
			'visits_hits',
			'visits_visitors',
			'visitersCommonHours',
			'auditory',
			'sources_entry'
		],
		/**
		 * Права на управление Яндекс.Метрикой
		 */
		'yandexMetrika' => [
			'get_counters',
			'view_counter',
			'view_counter_json',
			'edit_counter',
			'delete_counter',
			'check_counter',
			'add_counter'
		],
		/**
		 * Права на просмотр облака тегов
		 */
		'tagsCloud' => [
			'get_tags_cloud'
		]
	];
?>