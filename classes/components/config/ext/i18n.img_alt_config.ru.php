<?php

$i18n = array(
    "header-config-img_alt_config"  => "Alt и Title для изображений",
    "option-is_active"              => "Активировать расширение",
    "option-alt"                    => "Значение атрибута alt по умолчанию",
    "option-title"                  => "Значение атрибута title по умолчанию",
    "group-common"                  => "Общие настройки расширения",
    "alt-img-label-for"             => "Настройки для"
);