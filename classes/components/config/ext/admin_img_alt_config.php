<?php

class admin_img_alt_config extends def_module {

    public $module;
    private $prefix;

    public function __construct($module) {
        parent::__construct();
        $this->prefix = "img_alt";
        
        $commonTabs = $module->getCommonTabs();
        if ($commonTabs) {
            $commonTabs->add("img_alt_config");
        }
    }

    public function img_alt_config() {
        

        require_once dirname(__FILE__) . "/install_helper.php";

        if (registrateModule("config", "img_alt_config", array(
                    'config/img_alt_config',
                    'content/getAltTitle',
                    'content/img_alt_active',
                    'content/img_alt_save',
                    'content/img_alt_bufferSend',
                    'content/img_alt_sync',
                ))) {
            $config = mainConfiguration::getInstance();
            $config->set('kernel', 'buffer-send-event-enable', '1');

            $queryData = array();
            $connection = ConnectionPool::getInstance()->getConnection();
            
            $result = $connection->queryResult("SHOW TABLES");
            $result->setFetchType(IQueryResult::FETCH_ROW);
            if ($result->length()) {
                foreach ($result as $row) {
                    $queryData[] = $row[0];
                }

                if (!in_array("ext_img_alt", $queryData)) {
                    $connection->query("CREATE TABLE ext_img_alt (id INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY, img_path TEXT, alt VARCHAR(255), title VARCHAR(255))");
                }
            }
        }

        $domainsCollection = domainsCollection::getInstance();
        $currentLang = cmsController::getInstance()->getCurrentLang()->getPrefix();

        $this->module->setDataType("settings");
        $this->module->setActionType("modify");
        
        $prefix = $this->prefix;
        $regedit = regedit::getInstance();
        $params = array();
        //$fieldNames = array("boolean:is_active", "string:alt", "string:title");
        $groups = array("common" => array("boolean:is_active"));

        $domains = $domainsCollection->getList();
        foreach ($domains as $domain) {
            $groups[$domain->getHost()] = array("string:alt", "string:title");
        }

        $mode = getRequest("param0");
        if ($mode == "do") {
            foreach ($groups as $groupName => $fields) {
                $domainId = $domainsCollection->getDomainId($groupName);
                foreach ($fields as $fieldName) {
                    $fieldNameOrig = explode(":", $fieldName);
                    $fieldNameOrig = $fieldNameOrig[1];
                    $value = getRequest($fieldNameOrig);
                    if ($groupName != "common") {
                        $value = (isset($value[$groupName])) ? $value[$groupName] : false;
                        $params[$groupName][$fieldName] = $regedit->setVal("//settings/$prefix/$domainId/$currentLang/$fieldNameOrig", $value);
                    } else
                        $params[$groupName][$fieldName] = $regedit->setVal("//settings/$prefix/$fieldNameOrig", $value);
                }
            }
            $this->module->chooseRedirect();
        }

        foreach ($groups as $groupName => $fields) {
            $domainId = $domainsCollection->getDomainId($groupName);
            foreach ($fields as $fieldName) {
                $fieldNameOrig = explode(":", $fieldName);
                $fieldNameOrig = $fieldNameOrig[1];
                if ($groupName != "common")
                    $params[$groupName][$fieldName] = $regedit->getVal("//settings/$prefix/$domainId/$currentLang/$fieldNameOrig");
                else
                    $params[$groupName][$fieldName] = $regedit->getVal("//settings/$prefix/$fieldNameOrig");
            }
        }

        $data = $this->module->prepareData($params, "settings");
        $this->module->setData($data);

        return $this->module->doData();
    }

}
