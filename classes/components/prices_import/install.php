<?php

$INFO = Array();

$INFO['name'] = "prices_import";
$INFO['filename'] = "components/prices_import/class.php";
$INFO['config'] = "1";
$INFO['ico'] = "ico_prices_import";
$INFO['default_method'] = "page";
$INFO['default_method_admin'] = "import";

$INFO['func_perms'] = "";
$INFO['func_perms/import'] = "Импорт прайсов";
//$INFO['func_perms/schedules'] = "Задачи модуля";
//$INFO['func_perms/export'] = "Экспорт данных";
//$INFO['func_perms/view'] = "Плановое выполнение задач";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/components/prices_import/class.php";
$COMPONENTS[1] = "./classes/components/prices_import/admin.php";
$COMPONENTS[2] = "./classes/components/prices_import/lang.php";
$COMPONENTS[3] = "./classes/components/prices_import/i18n.php";
$COMPONENTS[4] = "./classes/components/prices_import/permissions.php";