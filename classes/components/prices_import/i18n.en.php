<?php
$i18n = Array(
    'module-prices_import' => "Загрузчик прайсов",
    'header-prices_import-tree' => "Редактирование",
    'header-prices_import-export' => "Экспорт данных",
    'header-prices_import-import' => "Загрузка данных",
    'header-prices_import-config' => "Настройка модуля Загрузчик прайсов",
    'header-prices_import-schedules' => "Отложенная загрузка",
    'header-prices_import-editSchedule' => "Информация о задании",
    'header-prices_import-add' => "Редактирование отложенной загрузки",
    'header-prices_import-edit' => "Редактирование отложенной загрузки",
    'group-config' => "Настройки модуля",
    'option-def_parentId' => "Идентификатор (или URL) родительской страницы по умолчанию",
    'option-def_typeId' => "Тип данных по умолчанию",
    'option-def_isActive' => "Делать «видимыми» новые объекты по умолчанию",
    'option-encoding' => "Кодировка файла по умолчанию",
    'option-symb_separate_type_id' => "Символ-разделитель для полей с множ. выбором",
    'perms-prices_import-admin' => "Управление модулем",
    'option-memory_limit' => "Mаксимальный объем памяти, который разрешается использовать модулю (мегабайт)",
    'option-symb_separate_csv' => "Символ-разделитель для CSV-файлов",
    'option-stores_field' => "Идентификатор поля «Состояние на складе»",
    'option-common_quantity_field' => "Идентификатор поля «Общее количество на складе»",
    'option-max_sched_time' => "Максимальное время выполнения скрипта для планировщика, сек",
    'perms-prices_import-import' => "Импорт прайс-листов",
    'perms-prices_import-schedule' => "Отложенная загрузка прайса",
    'perms-prices_import-view' => "Просмотр данных задания"
);
?>