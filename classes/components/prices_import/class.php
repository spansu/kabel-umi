<?php

$root = CURRENT_WORKING_DIR;
require_once $root.'/classes/components/prices_import/PHPExcel/PHPExcel.php';
$php_excel = new PHPExcel;

class prices_import extends def_module {

    public $module;
    public $per_page = 15;
    public $shift = 0;
    public $cron = false;

    public function __construct() {
        parent::__construct();

        if(cmsController::getInstance()->getCurrentMode() == "admin") {
            $this->__loadLib("admin.php");
            $this->__implement("Prices_importAdmin");
            $this->loadAdminExtension();
        } else {
            $this->loadSiteExtension();
            $this->__loadLib("customMacros.php");
            $this->__implement("Prices_ImportCustomMacros");
        }

        $this->loadCommonExtension();
        $this->loadTemplateCustoms();
    }
    
    public function getEditLink($element_id, $element_type) {
        $element = umiHierarchy::getInstance()->getElement($element_id);

        switch($element_type) {
            case "rubric": {
                // ссылка на добавление страницы (page)
                $link_add = $this->pre_lang . "/admin/prices_import/add/{$element_id}/page/";
                // ссылка на редактирование рубрики (rubric)
                $link_edit = $this->pre_lang . "/admin/prices_import/edit/{$element_id}/";

                return Array($link_add, $link_edit);
                break;
            }

            case "page": {
                // ссылка на редактирование страницы
                $link_edit = $this->pre_lang . "/admin/prices_import/edit/{$element_id}/";
                // запрещаем добавлять подстраницы для типа "page", первый элемент списка = false
                return Array(false, $link_edit);
                break;
            }

            default: {
            return false;
            }
        }
    }

    public function getObjectEditLink($objectId, $baseTypeName = false) {
        return $this->pre_lang . "/admin/prices_import/edit/{$objectId}/";
    }

    public function getTypeIdSchedule(){
        $regedit = regedit::getInstance();
        $type_id_schedule = $regedit->getVal("//modules/prices_import/type_id_schedule");
        return $type_id_schedule;
    }

    public function getObjectTypeEditLink($typeId) {
        return array(
            'create-link' => $this->pre_lang . "/admin/prices_import/addtype/{$typeId}/",
            'edit-link'   => $this->pre_lang . "/admin/prices_import/edittype/{$typeId}/",
        );
    }

    public function upload(){
        $root = CURRENT_WORKING_DIR;
        if(is_uploaded_file($_FILES["file_name"]["tmp_name"])){
            //Удаление старых файлов
            $dir = $root."/files/filesToImport/";
            if($handle = opendir($dir))
            {
                while(false !== ($file = readdir($handle)))
                    if (time() > (filemtime($dir.$file) + 3600*24*7))
                        unlink($dir.$file);
                closedir($handle);
            }

            if (isset($_SESSION['summary_arr'])) unset($_SESSION['summary_arr']);
            if (isset($_SESSION['get_fields_type'])) unset($_SESSION['get_fields_type']);

            //$file_name=time();
            $file_name=uniqid();
            $file_path = $root . "/files/filesToImport/".$file_name.".imp";
            // Если файл загружен успешно, перемещаем его из временной директории в конечную
            move_uploaded_file($_FILES["file_name"]["tmp_name"], $file_path);
            $file_type = substr($_FILES['file_name']['name'], strrpos($_FILES['file_name']['name'], '.')+1);

            $get_settings = array();
            $get_settings['file_type'] = $file_type;
            $get_settings['file_name'] = $_FILES["file_name"]["name"];
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
            //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Загрузка файла ".$_FILES["file_name"]["name"]." (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
            return $file_name;
        }
        return;
    }

    public function parse_xls($file_name = false, $console = false, $type = 'xls'){
        $root = CURRENT_WORKING_DIR;
        switch($type){
            case 'xls':
                $objReader = PHPExcel_IOFactory::createReader('Excel5');
                break;
            case 'xlsx':
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                break;
        }

        try {
            $objPHPExcel = $objReader->load($root . "/files/filesToImport/".$file_name.".imp");
        } catch (Exception $e){
            return "error";
        }

        $array = array();
        $array_udata = array();

        //Количество листов в документе
        $pages = $objPHPExcel -> getSheetCount();
        for ($page = 0; $page<$pages; $page++){
            $objPHPExcel->setActiveSheetIndex($page);
            $aSheet = $objPHPExcel->getActiveSheet();
            foreach($aSheet->getRowIterator() as $row){
                //получим итератор ячеек текущей строки
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                //пройдемся циклом по ячейкам строки
                //этот массив будет содержать значения каждой отдельной строки
                $item = array();
                foreach($cellIterator as $cell){
                    //заносим значения ячеек одной строки в отдельный массив
                    array_push($item, trim($this->getCellValue($aSheet, $cell)));
                }
                //заносим массив со значениями ячеек отдельной строки в "общий массив строк"

                if (implode("",$item) != "") {
                    array_push($array_udata, array("nodes:item"=>$item));
                    array_push($array, $item);
                }
            }
        }

        $result = serialize($array);

        file_put_contents($root . "/files/filesToImport/".$file_name.".arr", $result);
        //Подсчет количества объектов (строк с непустым первым столбцом)
        $total_objects=0;
        foreach($array as $obj){
            if (isset($obj[0])) $total_objects++;
        }
        if ($console) return $array;
        $result_ = array("nodes:row"=>$array_udata, "total_objects" => $total_objects);

        return $result_;
    }

    public function get_uploaded_file($file_name='', $set_shift = false, $compel = false, $langId = false, $forSchedule = false){
        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $root = CURRENT_WORKING_DIR;

        if (!$file_name) return;
        if (!file_exists($root . "/files/filesToImport/".$file_name.".imp")) return;

        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        set_time_limit(0);

        if (is_numeric($set_shift)) $this->shift = $set_shift;

        //Обработка загруженного файла
        $oC = umiObjectTypesCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();
        $ObjCatId = $oC->getBaseType('catalog','object');
        $getBaseType =  $oC -> getType($ObjCatId);
        $getIdBaseType = $getBaseType->getHierarchyTypeId();

        $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");

        $reload = true;
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }

        if (isset($get_settings['code'])) $encode = $get_settings['code']; else $encode = "auto";
        if (isset($get_settings['symb_separate_csv'])) $symb_separate_csv = $get_settings['symb_separate_csv']; else $symb_separate_csv = 59;
        if (!isset($get_settings['is_active_default'])){
            $regedit = regedit::getInstance();
            if ((boolean) $regedit->getVal("//modules/prices_import/def_isActive")) $get_settings['is_active_default'] = 1; else $get_settings['is_active_default'] = 0;
        }

        //$compare_first = array($enabled_head, $encode);
        $compare_first = array($encode,$symb_separate_csv);

        //if (getRequest('head'))	if (getRequest('head')=="on") $enabled_head=1; else $enabled_head=0;
        if (getRequest('code')) $encode = getRequest('code');
        if (getRequest('symb_separate_csv')) $symb_separate_csv = getRequest('symb_separate_csv');

        //$compare_second = array($enabled_head, $encode);
        $compare_second = array($encode,$symb_separate_csv);

        if ($compare_first == $compare_second) $reload = false;

        $get_settings['code'] = $encode;
        $get_settings['symb_separate_csv'] = $symb_separate_csv;

        if ($compel) $reload = true;

        if ((file_exists($root . "/files/filesToImport/".$file_name.".arr")) and (!$reload)){
            $summary_arr=unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".arr"));
            $summary_arr_=array_values($summary_arr);
            if ($summary_arr_!=$summary_arr) {
                $summary_arr = $summary_arr_;
                file_put_contents($root . "/files/filesToImport/".$file_name.".arr",serialize($summary_arr));
            }
            unset($summary_arr_);
        }
        else {
            //unset($get_settings['id']);
            switch(strtolower($get_settings['file_type']))
            {
                case 'csv':
                    setLocale(LC_ALL, 'ru_RU.UTF-8');
                    $file=file_get_contents($root . "/files/filesToImport/".$file_name.".imp");
                    $file = $this->charset_x_win($file);

                    if ($encode != "win-1251") $file=iconv("windows-1251", "utf-8", $file);
                    file_put_contents($root . "/files/filesToImport/".$file_name.".imp", $file);
                    unset($file);
                    $f = fopen($root . "/files/filesToImport/".$file_name.".imp", "rt") or die("Ошибка!");
                    for ($i=0; $data=fgetcsv($f,0,chr($symb_separate_csv)); $i++) {
                        $num = count($data);
                        for ($c=0; $c<$num; $c++)
                            $summary_arr[$i][$c] = $data[$c];
                    }
                    fclose($f);

                    if (!$summary_arr) {
                        if ($forSchedule) return false;
                        echo "error"; die;
                    }
                    break;
                case 'xls':
                    $summary_arr = $this->parse_xls($file_name, true, 'xls');
                    if ($summary_arr == "error") {
                        if ($forSchedule) return false;
                        echo "error"; die;
                    }
                    break;
                case 'xlsx':
                    $summary_arr = $this->parse_xls($file_name, true, 'xlsx');
                    if ($summary_arr == "error") {
                        if ($forSchedule) return false;
                        echo "error"; die;
                    }
                    break;
                default:
                    if ($forSchedule) return false;
                    echo "error"; die;
            }

            //=================================
            file_put_contents($root . "/files/filesToImport/".$file_name.".arr",serialize($summary_arr));
        }
        //==============================================================================================================

        //Получение всех возможных имен полей типов данных =============================================================
        $fields_id=$this->get_fields_type(false,true);
        //==============================================================================================================

        //Сохранение в файл идентификатора типа по умолчанию (первый) ==================================================
        if (!isset($get_settings['type_id_default'])){
            $get_settings['type_id_default'] = $regedit->getVal("//modules/prices_import/def_typeId");
            $get_list_type = $oC->getTypesByHierarchyTypeId($getIdBaseType);			//Получение всех типов данных
            $list_type = array_keys($get_list_type);
            if (!$get_settings['type_id_default'] or !in_array($get_settings['type_id_default'], $list_type)){
                $get_settings['type_id_default'] = $list_type[0];
            }
        }
        //==============================================================================================================

        //Сохранение в файл названия шаблона (если не выбран '') =======================================================
        if (!isset($get_settings['template']))
            $get_settings['template'] = '';
        //==============================================================================================================

        //Сохранение в файл категории по умолчанию =====================================================================
        if (!isset($get_settings['category_default'])){
            $category_default_id = unserialize($regedit->getVal("//modules/prices_import/def_parentId"));
            if (is_numeric($langId)){
                $category_default_id = isset($category_default_id[$langId]) ? $category_default_id[$langId] : false;
                if (strpos($category_default_id, "/") !== false)
                    $category_default_id = $hierarchy -> getIdByPath(trim($category_default_id, "/"));
                else {
                    $category_default_id = is_numeric($category_default_id) ? $category_default_id : false;
                }
            } else $category_default_id = false;
            $category_default_id = strtr($category_default_id, array("//"=>"/"));
            $get_settings['category_default'] = $category_default_id;
        }
        //==============================================================================================================

        //Массив всех идентификаторов полей загруженного файла =========================================================
        $fields_id_file = array();
        //==============================================================================================================

        $total_num_rows = count($summary_arr);

        //Массив id с выделенными строками =============================================================================
        $total_sel_num_rows = 0;
        if ($compel && !$forSchedule) if (file_exists($root . "/files/filesToImport/".$file_name.".sr")) unlink($root . "/files/filesToImport/".$file_name.".sr");
        if (!file_exists($root . "/files/filesToImport/".$file_name.".sr")){
            $this->select_row($file_name, 'sel_all_head');
        }

        $selected_rows = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));

        if (isset($get_settings['unselected_rows'])) {
            $unselected_rows = $get_settings['unselected_rows'];
            unset($get_settings['unselected_rows']);
            foreach($selected_rows as $index=>$selected_row){
                if (array_search($selected_row, $unselected_rows) !== false)
                    unset($selected_rows[$index]);
            }
            $selected_rows = array_values($selected_rows);

            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
            file_put_contents($root . "/files/filesToImport/".$file_name.".sr",serialize($selected_rows));
        }
        if (file_exists($root . "/files/filesToImport/".$file_name.".sr")) {
            $selected_rows = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));
            $total_sel_num_rows = count($selected_rows);
        }
        //==============================================================================================================

        $rows=array();
        $row=array();
        $num_rows = $this->per_page;

        //Определение полной ширины таблицы ============================================================================
        $x_max=0;
        for ($i=0; $i<$total_num_rows; $i++) if (count($summary_arr[$i])>$x_max) $x_max=count($summary_arr[$i]);
        $_SESSION['import_x_max'] = $x_max;
        //==============================================================================================================

        //Если не установлены идентификаторы полей, проставляются сохраненные из предыдущего прайс-листа ===============
        if (!isset($get_settings['id'])){
            $get_settings['id'] = array();
            //Подбор идентификаторов по первой строке таблицы (по названию, по заголовку) ==============================
            for($x = 0; $x<$x_max; $x++){
                $get_settings['id'][$x] = '';
                if (isset($summary_arr[0][$x]) && ($this ->  array_search_($summary_arr[0][$x],$fields_id, "name",false,true))){
                    $getSearchNameField = $this -> array_search_($summary_arr[0][$x], $fields_id, "name", "name", true);
                    if (!in_array($getSearchNameField, $get_settings['id'])) $get_settings['id'][$x] = $getSearchNameField;
                }
                else
                    if (isset($summary_arr[0][$x]) && ($this -> array_search_($summary_arr[0][$x], $fields_id, "title", false, true))){
                        $getSearchTitleField = $this -> array_search_($summary_arr[0][$x], $fields_id, "title", "name", true);
                        if (!in_array($getSearchTitleField, $get_settings['id'])) $get_settings['id'][$x] = $getSearchTitleField;
                    }
            }
            //==========================================================================================================
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        }
        //==============================================================================================================


        //Перебор всех строк массива ===================================================================================
        $start_row = $this->shift;
        $end_row = $num_rows+$this->shift;
        if ($end_row > $total_num_rows) $end_row = $total_num_rows;
        for ($i=$start_row; $i<$end_row; $i++){
            $items=$summary_arr[$i];
            //Перебор всех элементов строки
            $row_item=array();
            $row_item_i=array();

            for ($x=0; $x<$x_max; $x++){
                $row_item_i['attribute:id']=$x+1;

                if (isset($get_settings['id']) && isset($get_settings['id'][$x])){
                    $row_item_i['attribute:field_name']=$get_settings['id'][$x];
                    $row_item_i['attribute:field_type']= $this -> array_search_($get_settings['id'][$x],$fields_id, "name", "field_type");
                    $fields_id_file[] = $row_item_i['attribute:field_name'];
                }

                if (isset($row_item_i['attribute:field_type']) && $row_item_i['attribute:field_type']){
                    switch ($row_item_i['attribute:field_type']){
                        case 'symlink':
                            if (strpos(isset($items[$x]) ? $items[$x] : "", $symb_separate_type_id) === false){
                                $row_item_i['value']=isset($items[$x]) ? $items[$x] : "";
                            }else{
                                $list_elements = explode($symb_separate_type_id, trim(isset($items[$x]) ? $items[$x] : "",$symb_separate_type_id));
                                $row_item_i['nodes:value']=$list_elements;
                            }
                            break;
                        case 'relation':
                            $multiple = $this -> array_search_($get_settings['id'][$x],$fields_id, "name", "relation_multiple");
                            $relation_guide_id = $this -> array_search_($get_settings['id'][$x],$fields_id, "name", "relation_guide_id");
                            if ($multiple) $row_item_i['@multiple']=$multiple;
                            if ($relation_guide_id) $row_item_i['@guide_id']=$relation_guide_id;
                            $row_item_i['value']=isset($items[$x]) ? $items[$x] : "";
                            break;
                        default:
                            $row_item_i['value']=isset($items[$x]) ? $items[$x] : "";
                            break;
                    }

                } else $row_item_i['value']=isset($items[$x]) ? $items[$x] : "";

                $row_item[]=$row_item_i;
                unset($row_item_i);
            }

            $row['nodes:item']=$row_item;
            $row['attribute:head']='';
            $row['attribute:id']=$i;

            $row['attribute:selected']=0;
            if (in_array($i,$selected_rows)) $row['attribute:selected']=1;

            $rows[]=$row;
        }
        $result['nodes:row']=$rows;
        //==============================================================================================================

        //Валидация ====================================================================================================
        $get_errors = $this->validation($file_name);
        if (is_array($get_errors) && count($get_errors)){
            foreach ($get_errors as $row){
                $row_err = $row['row'];
                $col_err = $row['col'];
                $err_err = $row['err'];
                if (is_numeric($row_err) && is_numeric($col_err) && isset($result['nodes:row'][$row_err]['nodes:item'][$col_err])) $result['nodes:row'][$row_err]['nodes:item'][$col_err]['attribute:error']=$err_err;
                switch ($err_err){
                    case "not_permission":
                        if (is_numeric($row_err) && isset($result['nodes:row'][$row_err])) $result['nodes:row'][$row_err]['attribute:error']="not_permission";
                        break;
                    case "type_id_not_fit":
                        if (is_numeric($row_err) && isset($result['nodes:row'][$row_err])) $result['nodes:row'][$row_err]['attribute:error']="type_id_not_fit";
                        break;
                    default:
                        if (is_numeric($row_err) && isset($result['nodes:row'][$row_err])) $result['nodes:row'][$row_err]['attribute:error']="1";
                        break;
                }
            }
        }
        //==============================================================================================================

        //Проверка уникальных значений в столбцах ======================================================================
        $unique_columns = array();		//При условии, что все ячеки столбца уникальные - true;
        for ($x=0; $x<$x_max; $x++){
            $unique_col=array();
            for ($y=0; $y<count($selected_rows); $y++){
                $unique_col[]=isset($summary_arr[$selected_rows[$y]][$x]) ? $summary_arr[$selected_rows[$y]][$x] : "";
            }
            $unique_col=array_unique($unique_col);
            $unique_col_=array();
            if (count($unique_col)==count($selected_rows)){
                $unique_col_['attribute:id']=$x+1;
                $unique_col_['attribute:unique']=true;
                $unique_columns[]=$unique_col_;
            }else {
                $unique_col_['attribute:id']=$x+1;
                $unique_col_['attribute:unique']=false;
                $unique_columns[]=$unique_col_;
            }
        }
        $unique_columns_['nodes:item']=$unique_columns;
        $result['unique_columns']=$unique_columns_;
        //==============================================================================================================

        $result['num_rows']=$num_rows;
        $result['total_num_rows']=$total_num_rows;
        $result['total_sel_num_rows']=$total_sel_num_rows;
        $result['per_page']=$this->per_page;

        if (isset($get_settings['key']) && isset($get_settings['id'])){
            if (in_array($get_settings['key'], $get_settings['id'])){
                $result['key_field'] = $get_settings['key'];
            } else {
                unset($get_settings['key']);
            }
        }

        file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        return $result;
    }

    //Поиск в массиве
    public function array_search_($value=false, $arr=false, $field_search=false, $field_result=false, $ignore_register = false){
        $result = false;
        for ($i=0; $i<count($arr); $i++){
            if ($ignore_register){
                if (mb_strtolower($arr[$i][$field_search]) == mb_strtolower($value)) {
                    if ($field_result) $result = $arr[$i][$field_result];
                    else $result = $i;
                    break;
                }
            } else {
                if ($arr[$i][$field_search] == $value) {
                    if ($field_result) {
                        if (isset($arr[$i][$field_result])) $result = $arr[$i][$field_result]; else return false;
                    }
                    else $result = $i;
                    break;
                }
            }
        }
        if ($result) return $result; else return false;
    }

    //Удалить выделенные строки
    public function del_selected_str($file_name='', $row_id=false){
        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        $root = CURRENT_WORKING_DIR;
        if (file_exists($root . "/files/filesToImport/".$file_name.".sr"))
            $del_str = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));
        else return "";
        if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
            $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
            $rows_file=unserialize($file);
            if (is_numeric($row_id)){
                unset($rows_file[$row_id]);
                $stat = false;
                foreach($del_str as $i => $del_item){
                    if ($del_item == $row_id) {
                        unset($del_str[$i]);
                        $stat = true;
                        continue;
                    }
                    if ($stat) $del_str[$i] = $del_item-1;
                }
                file_put_contents($root . "/files/filesToImport/".$file_name.".sr",serialize($del_str));
            } else {
                foreach($del_str as $item) unset($rows_file[$item]);
                $selected_rows = array();
                file_put_contents($root . "/files/filesToImport/".$file_name.".sr",serialize($selected_rows));
            }
            $file=serialize($rows_file);
            file_put_contents($root . "/files/filesToImport/".$file_name.".arr",$file);
        }
    }

    //Изменение идентификаторов
    public function apply_change_id($file_name=''){
        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        $root = CURRENT_WORKING_DIR;
        $value = getRequest("value");
        $col = getRequest("col");
        if (is_numeric($col)){
            $get_settings = array();
            if (file_exists($root . "/files/filesToImport/".$file_name.".set"))
                $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));

            if (is_array($get_settings) && isset($get_settings['id'])){
                foreach($get_settings['id'] as $col_item=>$item) if ($item == $value) $get_settings['id'][$col_item] = '';
                $get_settings['id'][$col-1] = $value;
                ksort($get_settings['id']);
                file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
                file_put_contents($root . "/styles/skins/modern/data/modules/prices_import/system/match.arr", serialize($get_settings['id']));
                //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Изменены идентификаторы (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
            }
        }
    }

    //Отменить все изменения
    public function undo_changes($file_name=''){
        $root = CURRENT_WORKING_DIR;
        if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
            unlink($root . "/files/filesToImport/".$file_name.".arr");
            //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Отменены все изменения (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
        }
    }

    //Возвращает все поля текущего type-id
    public function get_fields_type($type_id=false, $console=false){
        $getFromSess = isset($_SESSION['get_fields_type']) ? $_SESSION['get_fields_type'] : false;
        if ($getFromSess !== false) {
            if (isset($getFromSess[$type_id ? $type_id : 0][$console ? $console : 0])) return $getFromSess[$type_id ? $type_id : 0][$console ? $console : 0];
        }

        $oC = umiObjectTypesCollection::getInstance();
        $fC = umiFieldTypesCollection::getInstance();
        $regedit = regedit::getInstance();
        $fieldTypesCollection = umiFieldTypesCollection::getInstance();

        $ObjCatId = $oC->getBaseType('catalog','object');
        $getBaseType =  $oC -> getType($ObjCatId);
        $getIdBaseType = $getBaseType->getHierarchyTypeId();

        //Список всех типов полей
        $getFieldTypesList = array();

        foreach ($fC -> getFieldTypesList() as $i=>$field){
            //$getFieldTypesList_["name"]=$field -> getName();
            $getFieldTypesList[$i]=$field -> getDataType();
        }

        $get_list_type = $oC->getTypesByHierarchyTypeId($getIdBaseType);
        $list_type = array_keys($get_list_type);
        if (($type_id) and (in_array($type_id,$list_type))){
            $list_type=array();
            $list_type[]=$type_id;
        }
        $list_fields = array();
        $list_fields[] = array("id" => 0, "title" => "", "name" => "", "field_type" => "", "kind"=>"base");
        $list_fields[] = array("id" => 1, "title" => "Идентификатор страницы", "name" => "id", "field_type" => "int", "kind"=>"base");
        $list_fields[] = array("id" => 2, "title" => "Название", "name" => "name", "field_type" => "string", "kind"=>"base");
        $list_fields[] = array("id" => 3, "title" => "Идентификатор типа данных", "name" => "type-id", "field_type" => "int", "kind"=>"base");
        $list_fields[] = array("id" => 4, "title" => "Родительская страница", "name" => "parent-id", "field_type" => "symlink", "kind"=>"base");
        $list_fields[] = array("id" => 5, "title" => "Активность", "name" => "is-active", "field_type" => "boolean", "kind"=>"base");

        $stores_field = $regedit->getVal("//modules/prices_import/stores_field");
        $common_quantity_field = $regedit->getVal("//modules/prices_import/common_quantity_field");

        //Проверка наличия указанных полей в настройках stores и common_quantity
        $statStoresField = false;
        $statCommonQuantity = false;
        foreach($getBaseType -> getAllFields() as $field){
            if ($field -> getName() == $stores_field) $statStoresField = true;
            if ($field -> getName() == $common_quantity_field) $statCommonQuantity = true;
        }
        if ($statStoresField === false) $regedit->setVar("//modules/prices_import/stores_field", '');
        if ($statCommonQuantity === false) $regedit->setVar("//modules/prices_import/common_quantity_field", '');


        //Список складов
        if ($stores_field){
            $getGuideId = false;
            foreach($getBaseType -> getAllFields() as $field){
                if ($field -> getName() == $stores_field) {
                    $getGuideId = $field -> getGuideId();
                    break;
                }
            }
            if (is_numeric($getGuideId)){
                $objs = new selector('objects');
                $objs->types('object-type')->id($getGuideId);
                foreach($objs -> result() as $item)
                    $list_fields[] = array("title" => "Склад: ".$item->getName(), "name" => "stores_state_".$item->getId(), "field_type" => "int", "kind"=>"stores");
            }
        }

        foreach($list_type as $id){
            $list = $oC->getType($id)->getAllFields();
            foreach($list as $i){
                //for ($search = 0; $search<count($list_fields); $search++) if ($i->getName() == $list_fields[$search]["name"]) break;
                //if ($search < count($list_fields)) continue;
                $types = false;
                if (isset($list_fields[$i->getName()]['types']))
                    $types = $list_fields[$i->getName()]['types'];
                $types[] = $id;
                $types = array_unique($types);
                $list_fields[$i->getName()] = array(
                    "name"=>$i->getName(),
                    "title"=>$i->getTitle(),
                    "field_type"=>$getFieldTypesList[$i->getFieldTypeId()],
                    "kind"=>($i->getName() != $common_quantity_field) ? "typical" : "stores",
                    "types"=>(is_array($types) && count($types)) ? $types : '',
                    "required"=>$i->getIsRequired() ? "1" : "0",
                    "field_id" => $i->getId()
                );
                //Если выпадающий список
                if ($list_fields[$i->getName()]['field_type'] == "relation"){
                    $getFieldType = $i-> getFieldType();
                    $list_fields[$i->getName()]['relation_multiple'] = $getFieldType -> getIsMultiple() ? "1" : "0";
                    $list_fields[$i->getName()]['relation_guide_id'] = $i -> getGuideId();
                }
            }
        }
        $list_fields = array_values($list_fields);

        $pos = 0;
        $list_fields_udata = array();

        for ($i=0; $i<count($list_fields); $i++){
            //Исключенные типы полей
            $excluded = array("swf_file", "tags");
            if (in_array($list_fields[$i]["field_type"], $excluded)) continue;

            $list_fields_udata[] = array(
                '@title' => $list_fields[$i]["title"],
                '@name' => $list_fields[$i]["name"],
                '@id' => $pos,
                '@field_type' => $list_fields[$i]["field_type"],
                '@kind' => $list_fields[$i]["kind"],
                '@required' => isset($list_fields[$i]["required"]) ? $list_fields[$i]["required"] : "0",
                '@field_id' => $list_fields[$i]["field_id"]
            );
            $pos++;
        }
        sort($list_fields_udata);
        if (!$console) {
            $result = array();
            $type_id_list = array();
            $result['nodes:item'] = $list_fields_udata;
            foreach ($get_list_type as $item=>$i){
                $type_id_list_ = array();
                $type_id_list_['attribute:id'] = $item;
                $type_id_list_['attribute:name'] = $i;
                $type_id_list[]=$type_id_list_;
            }
            $result['nodes:type_id'] = $type_id_list;

            $getFromSess[$type_id ? $type_id : 0][$console ? $console : 0] = $result;
            $_SESSION['get_fields_type'] = $getFromSess;
            return $result;
        }
        $getFromSess[$type_id ? $type_id : 0][$console ? $console : 0] = $list_fields;
        $_SESSION['get_fields_type'] = $getFromSess;
        return $list_fields;
    }

    //Валидация
    public function validation($file_name = false, $amount_errors = false){
        if (!$file_name) return "";

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        $root = CURRENT_WORKING_DIR;
        $pC = permissionsCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();
        $oTC = umiObjectTypesCollection::getInstance();
        $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");
        $currentUserId = $pC->getUserId();

        $errors = array();

        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }

        if (file_exists($root . "/files/filesToImport/".$file_name.".sr"))
            $selected_rows = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));

        if (isset($get_settings['id']))	$id = $get_settings['id'];   //Идентификаторы полей
        if (isset($get_settings['category_default'])) $cde = $get_settings['category_default']; //Категория по умолчанию

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr"))
            $arr=unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".arr"));
        else return "";

        $get_id_title = false;

        if (isset($get_settings['id'])){
            $get_id = $get_settings['id'];
            $num_id = count($get_id);
        }
        else {
            $get_id_title = $arr[0];
            $num_id = count($get_id_title);
        }

        //Получение всех возможных имен полей =====================================================
        $fields_id=$this->get_fields_type(false,true);
        //=========================================================================================

        //Список всех категорий ===================================================================
        $get_parents = $this->get_parents(true);
        $parent_id_list = isset($get_parents['id']) ? $get_parents['id'] : array();
        $parent_name_list = isset($get_parents['name']) ? $get_parents['name'] : array();
        for ($i=0; $i<count($parent_name_list); $i++){
            $parent_name_list[$i]=mb_strtolower($parent_name_list[$i]);
        }
        //=========================================================================================

        //Список всех type-id католога ============================================================
        $get_types_list = $this->get_types_id(true);
        $get_types_list_id = $get_types_list['id'];
        $get_types_list_name = $get_types_list['name'];
        for ($i=0; $i<count($get_types_list_name); $i++){
            $get_types_list_name[$i]=mb_strtolower($get_types_list_name[$i]);
        }
        //=========================================================================================
        $start_row = $this->shift;
        $end_row = $this->per_page + $this->shift;
        if (is_numeric($amount_errors)) {
            $start_row = $amount_errors;
            $end_row = $start_row + $this->per_page;
            $finish = $end_row;
        }
        if ($end_row > count($arr)) {
            $end_row = count($arr);
            $finish = 0;
        }
        for ($row = $start_row; $row<$end_row; $row++){
            if (!in_array($row,$selected_rows)) continue;

            //Создание массива с данным объекта(товара) из текущей строки (ключ массива - идентификатор поля)
            $object = array();
            for ($col=0; $col<count($arr[0]); $col++){
                if (isset($id[$col])){
                    if ($arr[$row][$col]) $object[$id[$col]]['value'] = $arr[$row][$col]; else $object[$id[$col]]['value'] = false;
                    $get_filed_type = $this -> array_search_($id[$col], $fields_id, "name", "field_type");
                    if ($get_filed_type) $object[$id[$col]]['fieldType'] = $get_filed_type; else $object[$id[$col]]['fieldType'] = false;
                }
            }

            //Проверка, есть ли в наличии товар. Проверка прав доступа =================================================
            $isset = $this -> obj_exists($file_name, $row);
            if (isset($object['parent-id'])){
                $get_parents_id = $this->get_parents(true);
                $get_parents_id = isset($get_parents_id['id']) ? $get_parents_id['id'] : array();
                $get_parents_name = isset($get_parents_id['name']) ? $get_parents_id['name'] : array();
                if ((in_array($object['parent-id']['value'], $get_parents_id)) or (in_array($object['parent-id']['value'], $get_parents_name)))
                    $parent_id = $object['parent-id']['value'];
                else $parent_id = $cde;
            } else {
                $parent_id = $cde;
                if ($isset){
                    $gParentId = $hierarchy -> getParent($isset);
                    $parent_id = $gParentId ? $gParentId : $parent_id;
                }
            }

            //Проверка прав доступа
            if (!$this -> getObjectPermission(($isset && is_numeric($isset)) ? $isset : false, $parent_id ? $parent_id : false)){
                $errors[] = array(
                    'row'=>$row - $this->shift,
                    'col'=>'',
                    'err'=>'not_permission'
                );
                continue;
            }
            //==========================================================================================================

            $typeIdExist = false;

            for ($col = 0; $col<$num_id; $col++){
                $value = isset($arr[$row][$col]) ? $arr[$row][$col] : "";
                if ($value=='') continue;

                if ($get_id_title) {
                    if (isset($get_id_title[$col])) $get_id_name = $get_id_title[$col];
                }
                else {
                    if (isset($get_id[$col])) $get_id_name = $get_id[$col];
                }
                $field_type = $this -> array_search_($get_id_name, $fields_id, "name", "field_type");

                $stat_error = "";

                //Ссылка на дерево ================================================================
                if ($field_type=="symlink"){
                    $value_ = trim($value, $symb_separate_type_id);
                    if ($get_id_name=="parent-id"){
                        if (is_numeric($value_)){
                            if (!in_array($value_,$parent_id_list)) $stat_error = "parent_id_not_available";
                        } else $stat_error = "parent_id_not_available";
                    } else {
                        $list_elements = explode($symb_separate_type_id, $value_);
                        foreach($list_elements as $element) {
                            if ($element != ''){
                                if (in_array($element,$parent_id_list)) continue;
                                if ($hierarchy->isExists($element)) continue;
                                $stat_error = "parent_id_not_available_in_list";
                                break;
                            }
                        }
                    }
                }
                //=================================================================================

                //type-id =========================================================================
                if ($get_id_name=="type-id"){
                    $typeIdExist = true;
                    $value_ = trim($value, $symb_separate_type_id);
                    //Проверка на отсутствие типа данных
                    if (!in_array($value_,$get_types_list_id)) $stat_error = "type_id_not_available";
                    else{
                        $type_id_current = $value_;
                        //Проверка, есть ли в выбранном типе данные поля, указанные в таблице
                        $get_id_fields_current_row = array();
                        for ($ri = 0; $ri<$num_id; $ri++){		//Формирование массива, содержащего все поля текущей строки
                            $get_id_field_current_row = $get_id[$ri];
                            if ($get_id_field_current_row!="")
                                $get_id_fields_current_row[]=$get_id_field_current_row;									//Все type-id текущей строки
                        }
                        $type_id_feilds = $this->get_fields_type($type_id_current,true);							//Все type-id выбранного типа
                        foreach($get_id_fields_current_row as $item){
                            if (!$this->array_search_($item, $type_id_feilds, "name")) $stat_error = "type_id_not_fit";
                        }
                    }
                }
                //=================================================================================

                //id ==============================================================================
                if ($get_id_name=="id"){
                    if (!$hierarchy->isExists($value)) {
                        $stat_error = "page_not_found";
                    };
                    $ObjCatId = $oTC -> getBaseType('catalog','object');
                    $getBaseType =  $oTC -> getType($ObjCatId);
                    $getIdBaseType = $getBaseType->getHierarchyTypeId();

                    $getPage = $hierarchy -> getElement($value);
                    if ($getPage instanceof umiHierarchyElement){
                        if ($getPage -> getTypeId() != $getIdBaseType){
                            $stat_error = "page_not_OC";
                        }
                    } else {
                        $stat_error = "page_not_found";
                    }
                }
                //=================================================================================

                if ($stat_error!=""){
                    $errors[] = array(
                        'row'=>$row - $this->shift,
                        'col'=>$col,
                        'err'=>$stat_error
                    );
                    unset($stat_error);
                }
            }

            //Если в ячейках не был указан type-id, проверяем, есть ли идентифик., которых нет в типе данных по умолч. =
            if (!$typeIdExist && isset($get_settings['type_id_default'])){
                $type_id_current = $get_settings['type_id_default'];
                $get_id_fields_current_row = array();
                for ($ri = 0; $ri<$num_id; $ri++){		//Формирование массива, содержащего все поля текущей строки
                    $get_id_field_current_row = isset($get_id[$ri]) ? $get_id[$ri] : "";
                    if ($get_id_field_current_row!="")
                        $get_id_fields_current_row[]=$get_id_field_current_row;		    //Все type-id текущей строки
                }
                $type_id_feilds = $this->get_fields_type($type_id_current,true);		//Все type-id выбранного типа
                foreach($get_id_fields_current_row as $item){
                    if (!$this->array_search_($item, $type_id_feilds, "name"))
                        $errors[] = array(
                            'row'=>$row - $this->shift,
                            'col'=>'',
                            'err'=>'type_id_not_fit'
                        );
                }
            }
            //==========================================================================================================

        }
        if (is_numeric($amount_errors)) return array("errors"=>count($errors),"next"=>$finish ? $finish : "0");
        return $errors;
    }

    //Список всех сущетсвующих категорий
    public function get_parents($console=false){
        $objects = new selector('pages');
        $objects->types('hierarchy-type')->name('catalog', 'category');
        $parent_obj = $objects->result();
        $parent_id_list = array();
        $parent_name_list = array();

        $parent_list_udata = array();
        foreach($parent_obj as $obj){
            $parent_id_list[] = $obj->id;
            $parent_name_list[] = $obj->name;
            $parent_udata['attribute:id'] = $obj->id;
            $parent_udata['attribute:name'] = $obj->name;
            $parent_list_udata[] = $parent_udata;
        }
        $result = array("id" => $parent_id_list, "name" => $parent_name_list);

        $result_["nodes:item"] = $parent_list_udata;
        if ($console) return $result;
        else return $result_;
    }

    //Список всех сущетсвующих type-id каталога
    public function get_types_id($console=false){
        $oC = umiObjectTypesCollection::getInstance();
        $ObjCatId = $oC->getBaseType('catalog','object');
        $getBaseType =  $oC -> getType($ObjCatId);
        $getIdBaseType = $getBaseType->getHierarchyTypeId();
        $get_list_type = $oC->getTypesByHierarchyTypeId($getIdBaseType);

        $get_list_type_id = array();
        $get_list_type_name = array();

        $type_udata = array();

        $regedit = regedit::getInstance();
        $id_default = (int) $regedit->getVal("//modules/prices_import/def_typeId");

        foreach ($get_list_type as $item=>$i){
            $get_list_type_id[] = $item;
            $get_list_type_name[] = $i;
            $type_udata_ = array();
            $type_udata_['attribute:id']=$item;
            $type_udata_['attribute:name']=$i;
            if ($item == $id_default) $type_udata_['attribute:default']="1";
            $type_udata[] = $type_udata_;
        }

        $result = array("id" => $get_list_type_id, "name" => $get_list_type_name);
        if ($console) return $result;
        else
            return array("nodes:item"=>$type_udata);
    }

    //Изменение ячейки
    public function cell_change($file_name=false, $row_id = false, $col_id = false, $type = false){
        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);

        $root = CURRENT_WORKING_DIR;
        $value = getRequest('value');
        if (!isset($value)) return "";

        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        switch ($type){
            case "boolean":
                if (($value == "1") or ($value == "Да") or ($value == "да")) $value = 0; else $value = 1;
                break;
            default:
        }

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
            $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
            $rows_file=unserialize($file);
            $rows_file[$row_id][$col_id-1] = $value;
            $file=serialize($rows_file);
            file_put_contents($root . "/files/filesToImport/".$file_name.".arr",$file);
        }

        return $value;
    }

    //Изменение ячейки symlink
    public function cell_change_parent($file_name=false, $row_id = false, $col_id = false, $value = ''){
        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);

        $root = CURRENT_WORKING_DIR;
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
            $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
            $rows_file=unserialize($file);

            if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
                $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
            }

            $regedit = regedit::getInstance();
            $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");
            $value_cell = $rows_file[$row_id][$col_id-1];

            if ($get_settings['id'][$col_id-1] == 'parent-id') $rows_file[$row_id][$col_id-1] = (int) $value;
            else {
                $list_elements = explode($symb_separate_type_id, $value_cell);
                foreach($list_elements as $i=>$element) {
                    $list_elements[$i] = trim($element);
                    if (!is_numeric($list_elements[$i])) unset($list_elements[$i]);
                }
                if (!in_array($value, $list_elements)) $list_elements[] = (int) $value;
                $rows_file[$row_id][$col_id-1] = implode($symb_separate_type_id, $list_elements);
            }

            $file=serialize($rows_file);
            file_put_contents($root . "/files/filesToImport/".$file_name.".arr",$file);
        }

        return $value;
    }

    //Изменение symlink
    public function setSymlink($langId = false){
        $data_url = getRequest('data_url');
        $selected_url = getRequest('selected_url');

        //Парсинг строки $selected_url
        $parent_id = (int) end(explode("/",trim($selected_url,"/")));
        if (!$parent_id) return "";
        $_SESSION['export_parent_id'] = $parent_id;

        parse_str($data_url);

        switch($element){
            case 'default_category':
                $this->change_category_default($file_name, $parent_id, $langId);
                break;
            case 'cell':
                $this->cell_change_parent($file_name, $row_id, $col_id, $parent_id);
                break;
        }
        return "";
    }

    //Получение списка symlink ячейки
    public function getListSymlink(){
        $data_url = getRequest('data_url');
        $selected_url = getRequest('selected_url');

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        parse_str($data_url);

        if ($element == 'cell'){
            $root = CURRENT_WORKING_DIR;
            if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
                $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
                $rows_file=unserialize($file);

                $regedit = regedit::getInstance();
                $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");
                $value_cell = $rows_file[$row_id][$col_id-1];
                $list_elements = explode($symb_separate_type_id, $value_cell);
                foreach($list_elements as $i=>$element_id) {
                    $list_elements[$i] = trim($element_id);
                    if (!is_numeric($list_elements[$i])) {unset($list_elements[$i]); continue;}
                    unset($list_elements[$i]);
                    $list_elements[$i] = $this->get_category_param($element_id);
                }
                return array("items"=>$list_elements);
            }
        }
        return "";
    }

    //Удаление из списка symlink ячейки
    public function detFromListSymlink($del_parent_id = false){
        $data_url = getRequest('data_url');
        parse_str($data_url);

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);

        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        if ($element == 'cell'){
            $root = CURRENT_WORKING_DIR;
            if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
                $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
                $rows_file=unserialize($file);

                $regedit = regedit::getInstance();
                $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");
                $value_cell = $rows_file[$row_id][$col_id-1];
                $list_elements = explode($symb_separate_type_id, $value_cell);

                if (in_array($del_parent_id, $list_elements)) unset($list_elements[array_search($del_parent_id, $list_elements)]);
                $value_cell = implode($symb_separate_type_id, $list_elements);
                $rows_file[$row_id][$col_id-1] = $value_cell;
                $file=serialize($rows_file);
                file_put_contents($root . "/files/filesToImport/".$file_name.".arr",$file);
            }
        }
        return "";
    }

    //Добавление колонки
    public function add_col($file_name=false, $col_id = false){
        $root = CURRENT_WORKING_DIR;

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
            $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
            $rows_file=unserialize($file);

            for ($y=0; $y<count($rows_file); $y++)
            {
                $row_n = array();
                for ($x=0; $x<count($rows_file[$y]); $x++){
                    $row_n[]=$rows_file[$y][$x];
                    if (($x+1)==$col_id) $row_n[]="";
                }
                $rows_file[$y] = $row_n;
            }

            $file=serialize($rows_file);
            file_put_contents($root . "/files/filesToImport/".$file_name.".arr",$file);
        }

        //Файл .id (Добвление колонки в заголовок - строку иденитификаторов)
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }

        if (isset($get_settings['id'])){
            $rows_file = $get_settings['id'];
            $row_n = array();
            for ($x=0; $x<count($rows_file); $x++){
                if ($x==$col_id) $row_n[]="";
                $row_n[]=$rows_file[$x];
            }

            $get_settings['id'] = $row_n;
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        }
        return "";
    }

    //Добавление строки
    public function add_row($file_name=false, $row_id = false){
        $root = CURRENT_WORKING_DIR;

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
            $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
            $rows_file=unserialize($file);

            $new_array = array();
            for ($y=0; $y<count($rows_file); $y++)
            {
                $new_array[]=$rows_file[$y];
                if ($y==$row_id){
                    $row_n=array();
                    for ($x=0; $x<count($rows_file[$y]); $x++){
                        $row_n[]="";
                    }
                    $new_array[]=$row_n;
                }
            }
            $file=serialize($new_array);
            file_put_contents($root . "/files/filesToImport/".$file_name.".arr",$file);
        }
        return "";
    }

    //Удаление колонки
    public function del_col($file_name=false, $col_id = false){
        $col_id=$col_id-1;
        $root = CURRENT_WORKING_DIR;

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
            $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
            $rows_file=unserialize($file);

            for ($y=0; $y<count($rows_file); $y++)
            {
                $row_n = array();
                for ($x=0; $x<count($rows_file[$y]); $x++){
                    if ($x==$col_id) continue;
                    $row_n[]=$rows_file[$y][$x];
                }
                $rows_file[$y] = $row_n;
            }

            $file=serialize($rows_file);
            file_put_contents($root . "/files/filesToImport/".$file_name.".arr",$file);
        }

        //Файл .id
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }

        if (isset($get_settings['id'])){
            $rows_file = $get_settings['id'];
            $row_n = array();
            for ($x=0; $x<count($rows_file); $x++){
                if ($x==$col_id) continue;
                $row_n[]=$rows_file[$x];
            }
            $get_settings['id'] = $row_n;
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        }

        return "";
    }

    //Удаление строки
    public function del_row($file_name=false, $row_id = false){
        $root = CURRENT_WORKING_DIR;

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
            $file=file_get_contents($root . "/files/filesToImport/".$file_name.".arr");
            $rows_file=unserialize($file);
            unset($rows_file[$row_id]);
            $rows_file=array_values($rows_file);

            $file=serialize($rows_file);
            file_put_contents($root . "/files/filesToImport/".$file_name.".arr",$file);
        }
        return "";
    }

    //Изменение ключевого идентификатора
    public function change_key_id($file_name=false, $key_field = false){
        if (!$key_field) return "";
        $root = CURRENT_WORKING_DIR;
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }

        if (isset($get_settings['key'])){
            if ($get_settings['key'] == $key_field) unset($get_settings['key']); else $get_settings['key'] = $key_field;
        } else {
            $get_settings['key'] = $key_field;
        }
        file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Изменен ключевой идентификатор (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
        return "";
    }

    //Выделение/(сброс выделения) строки (checkbox)
    public function select_row($file_name=false, $rowId = false){
        $root = CURRENT_WORKING_DIR;
        if (!$rowId and $rowId!=0) return "";

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        if (($rowId == 'sel_all') or ($rowId == 'sel_all_head')){
            if (file_exists($root . "/files/filesToImport/".$file_name.".arr")){
                $summary_arr=unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".arr"));
                $total = count($summary_arr);

                $selected_rows = array();
                for ($index = 0; $index<$total; $index++){
                    if (($rowId == 'sel_all') and ($index==0)) continue;
                    $selected_rows[] = $index;
                }
                file_put_contents($root . "/files/filesToImport/".$file_name.".sr",serialize($selected_rows));
            }
            return "";
        }

        if ($rowId == 'del_all'){
            file_put_contents($root . "/files/filesToImport/".$file_name.".sr",serialize(array()));
            return "";
        }

        if (!is_numeric($rowId)) return "";
        $rowId = (int) $rowId;
        $root = CURRENT_WORKING_DIR;
        $selected_rows = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".sr"))
            $selected_rows = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));
        if (in_array($rowId,$selected_rows)){
            $id = array_search($rowId,$selected_rows);
            unset($selected_rows[$id]);
        } else {
            $selected_rows[] = $rowId;
        }
        sort($selected_rows);
        file_put_contents($root . "/files/filesToImport/".$file_name.".sr",serialize($selected_rows));
        return "";
    }

    //Сохранение нового шаблона
    public function save_template($file_name=false){
        $root = CURRENT_WORKING_DIR;
        $get_settings = array();

        $template_name = getRequest('template_name');
        if (!$template_name) return;

        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        $arr = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".arr"))
            $arr = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".arr"));	                //Массив данных


        $templates_arr = array();
        if (file_exists($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr")){
            $templates_arr = unserialize(file_get_contents($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr"));
        }
        $selected_rows = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".sr")) {
            $selected_rows = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));
        }
        $unselectedRows = array();
        foreach($arr as $index=>$row){
            if (array_search($index,$selected_rows)===false) $unselectedRows[] = $index;
        }

        if (isset($get_settings['template'])) unset($get_settings['template']);
        if (isset($get_settings['report'])) unset($get_settings['report']);

        $templates_arr[$template_name] = $get_settings;
        $templates_arr[$template_name]['unselected_rows'] = $unselectedRows;

        //Если идентификаторы последних столбцов указаны "Пропустить" -> удаление.
        $ids = $get_settings['id'];
        $ids = array_reverse($ids, true);
        foreach($ids as $index=>$id) {if (!$id) unset($ids[$index]); else break;}
        $ids = array_reverse($ids, true);
        $templates_arr[$template_name]['id'] = $ids;


        file_put_contents($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr", serialize($templates_arr));
        $get_settings['template'] = $template_name;
        file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Сохранен / обновлен шаблон ".$template_name." (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
        return;
    }

    //Получение списка шаблонов
    public function getListTemplates(){
        $root = CURRENT_WORKING_DIR;
        $templates_arr = array();
        if (file_exists($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr")){
            $templates_arr = unserialize(file_get_contents($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr"));
            if (!$templates_arr) $templates_arr = array();
        } else return array("nodes:item"=>array());;

        $list = array();
        if (count($templates_arr)>0){
            foreach($templates_arr as $name=>$template){
                $list_ = array();
                $list_['attribute:name'] = $name;
                $list_['attribute:head'] = isset($template['head']) ? $template['head'] : "";
                $list_['attribute:code'] = isset($template['code']) ? $template['code'] : "";
                $list_['attribute:symb_separate_csv'] = isset($template['symb_separate_csv']) ? $template['symb_separate_csv'] : "";
                $list_['attribute:type_id_default'] = isset($template['type_id_default']) ? $template['type_id_default'] : "";
                $list_['attribute:category_default'] = isset($template['category_default']) ? $template['category_default'] : "";
                $list_['attribute:not_add_element'] = isset($template['not_add_element']) ? $template['not_add_element'] : "";
                $category_default_path = $this->get_category_param($template['category_default']);
                if (!$category_default_path){
                    unset($templates_arr[$name]);
                }
                $list_['attribute:category_default_path'] = $category_default_path['path'];
                $list_['attribute:is_active_default'] = $template['is_active_default'];
                $list[] = $list_;
            }
        }
        return array("nodes:item"=>$list);
    }

    //Применение выбранного шаблона к текущему прайсу
    public function applyTemplate($file_name, $forSchedule = false, $applyTemplate = ""){
        $root = CURRENT_WORKING_DIR;
        $templateName = $applyTemplate ? $applyTemplate : getRequest('template_name');

        $templates_arr = array();
        if (file_exists($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr")){
            $templates_arr = unserialize(file_get_contents($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr"));
        }
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        if (!$templateName) {
            unset($get_settings['id']);
            $get_settings['template'] = '';
            unset($get_settings['key']);
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
            return false;
        }
        if (isset($templates_arr[$templateName])){
            $template = $templates_arr[$templateName];

            if (isset($get_settings['file_type']) && isset($get_settings['symb_separate_csv']) && isset($template['symb_separate_csv']))
                if ($get_settings['file_type'] == "csv"){
                    file_put_contents($root."/test.txt",$get_settings['symb_separate_csv'].",".$template['symb_separate_csv']);
                    if ($get_settings['symb_separate_csv'] != $template['symb_separate_csv']){
                        $get_settings['symb_separate_csv'] = $template['symb_separate_csv'];
                        file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
                        $this->get_uploaded_file($file_name, false, true);
                    }
                }

            //Проверка соответствия шаблона к текущему прайсу
            // 1) Проверка количества столбцов
            if (!empty($_SESSION['import_x_max'])){
                $import_x_max = $_SESSION['import_x_max'];
                $stat = true;
                if (count($template['id']) > $import_x_max) {$stat = false;}

                //Проверка типа файла (если был xls, а стал xlsx или наоборот)
                if (!isset($get_settings['file_type']) or !isset($templates_arr[$templateName]['file_type'])) $stat = false;
                //if (isset($get_settings['file_type']) && isset($templates_arr[$templateName]['file_type']) && ($get_settings['file_type'] != $templates_arr[$templateName]['file_type'])) $stat = false;

                if ($stat){
                    $template['template'] = $templateName;
                    $template['file_type'] = $get_settings['file_type'];
                    file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($template));
                    //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Применен шаблон ".$templateName." (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
                    return true;
                }
            }
            //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Ошибка применения шаблона ".$templateName." (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
            if ($forSchedule) return false;
            $buffer = outputBuffer::current();
            $buffer->charset('utf-8');
            $buffer->contentType('text/plane');
            $buffer->clear();
            $buffer->push("error");
            $buffer->end();
        }
        return false;
    }

    //Удаление выбранного шаблона
    public function deleteTemplate(){
        $templateName = getRequest('template_name');
        if (!$templateName) return;
        $root = CURRENT_WORKING_DIR;
        $templates_arr = array();
        if (file_exists($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr")){
            $templates_arr = unserialize(file_get_contents($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr"));
        }
        if (isset($templates_arr[$templateName])){
            unset($templates_arr[$templateName]);
            file_put_contents($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr", serialize($templates_arr));
            //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Удален шаблон ".$templateName.PHP_EOL,FILE_APPEND);
        }
        return "";
    }

    //Получение данных выбранной категории
    public function get_category_param($parent_id=false){
        $hierarchy = umiHierarchy::getInstance();

        $parent_url = (strpos($parent_id, "/") !== false) ? true : false;

        if (!$parent_id) return array(
            'id'=>$parent_id,
            'name'=>$parent_id. ' Выбрать',
            'path'=>$parent_id. ' Выбрать'
        );

        $get_element = $hierarchy->getElement($parent_id, true);
        if ($get_element instanceof umiHierarchyElement){}
        else return array(
            'id'=>$parent_id,
            'name'=>$parent_id. ' - <b>указанная страница не существует<br/>или не является разделом каталога</b>',
            'path'=>$parent_id. ' - <b>указанная страница не существует<br/>или не является разделом каталога</b>'
        );

        $parent_name = $get_element->getName();

        $path = $parent_name;
        $get_tree_parent = $hierarchy->getParent($parent_id);
        $i = 0;
        while ($get_tree_parent){
            $path=$hierarchy->getElement($get_tree_parent, true)->getName() . " / ".$path;
            $get_tree_parent = $hierarchy->getParent($get_tree_parent);
            $i++;
            if ($i>100) {
                return array(
                    'id'=>$parent_id,
                    'name'=>$parent_id. ' - <b>указанная страница не существует<br/>или не является разделом каталога</b>',
                    'path'=>$parent_id. ' - <b>указанная страница не существует<br/>или не является разделом каталога</b>'
                );
            }
        }
        $result = array();
        $result["id"] = $parent_id;
        $result["name"] = $parent_name;
        $result["path"] = $path;
        return $result;
    }

    //Установка или изменение категории по умолчанию
    public function change_category_default($file_name=false, $id=false, $langId = false){
        $root = CURRENT_WORKING_DIR;
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
            $get_settings['category_default'] = $id;
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
            //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Изменение категории по умолчанию на ".$id." (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
        }
    }

    //Получение категории по умолчанию
    public function get_category_default($file_name=false){
        $root = CURRENT_WORKING_DIR;

        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        $path = $this->get_category_param($get_settings['category_default']);
        return $path;
    }

    //Установка или изменение идентификатора типа по умолчанию
    public function change_default_type_id($file_name=false, $id=false){
        $root = CURRENT_WORKING_DIR;
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        //file_put_contents($root . "/files/filesToImport/".$file_name.".idd", $id);
        $get_settings['type_id_default'] = $id;
        file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Изменен идентификатор типа по умолчанию на ".$id." (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
    }

    //Импорт
    public function import_($file_name = false, $getScheduleId = false) {
        $connection = ConnectionPool::getInstance()->getConnection();

        $root = CURRENT_WORKING_DIR;

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");

        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        $common_quantity_field = $regedit->getVal("//modules/prices_import/common_quantity_field");

        $max_sched_time = (int) $regedit->getVal("//modules/prices_import/max_sched_time");
        if (!$max_sched_time) $max_sched_time = 60;
        $lastCronTime = (int) $regedit->getVal("//modules/prices_import/last_cron_time");

        $getListId = getrequest('id') ? getrequest('id') : $_GET['id'];

        if ($getListId) $rows=explode(",",trim($getListId,","));
        if ($getListId == '0') $rows = array(0);
        $rows_ = array();

        if (!file_exists($root . "/files/filesToImport/".$file_name.".arr")) return;

        if (file_exists($root . "/files/filesToImport/".$file_name.".sr")){
            $selected_rows = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));
            foreach ($rows as $row){
                if (isset($selected_rows[$row])) $rows_[] = $selected_rows[$row];
            }
            $rows = $rows_;
            unset($rows_);
        } else return "";

        $hierarchy = umiHierarchy::getInstance();
        $uOC = umiObjectsCollection::getInstance();
        $oC = umiObjectTypesCollection::getInstance();
        $fieldsCollection = umiFieldsCollection::getInstance();
        $fC = umiFieldTypesCollection::getInstance();
        $pC = permissionsCollection::getInstance();
        $currentUserId = $pC->getUserId();
        $regedit = regedit::getInstance();

        $ObjCatId = $oC->getBaseType('catalog','object');
        $getBaseType =  $oC -> getType($ObjCatId);
        $getIdBaseType = $getBaseType->getHierarchyTypeId();

        //Получение всех возможных имен полей ==========================================================================
        $fields_id=$this->get_fields_type(false,true);
        //==============================================================================================================
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        if (!isset($get_settings['report']['updated']) or !isset($get_settings['report']['created'])
            or !isset($get_settings['report']['processed']) or !isset($get_settings['report']['rows'])
            or !isset($get_settings['report']['time_start'])){
            if (!isset($get_settings['report']['updated'])) $get_settings['report']['updated'] = 0;
            if (!isset($get_settings['report']['created'])) $get_settings['report']['created'] = 0;
            if (!isset($get_settings['report']['processed'])) $get_settings['report']['processed'] = 0;
            if (!isset($get_settings['report']['rows'])) $get_settings['report']['rows'] = count($selected_rows);
            if (!isset($get_settings['report']['time_start'])) $get_settings['report']['time_start'] = time();
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        }

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr"))
            $arr = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".arr"));	                //Массив данных
        if (isset($get_settings['category_default'])) $cde = $get_settings['category_default'];                         //Категория по умолчанию
        if (isset($get_settings['type_id_default'])) $idd = $get_settings['type_id_default'];                           //Идентификатор типа по умолчанию
        if (isset($get_settings['id']))	$id = $get_settings['id'];                                                      //Идентификаторы полей
        if (isset($get_settings['key'])) $key = $get_settings['key'];                                                   //Ключевой идентификатор
        if (isset($get_settings['is_active_default'])) $is = $get_settings['is_active_default'];                        //Активность по умолчанию
        else $is = 1;

        $not_add_element = isset($get_settings['not_add_element']) ? $get_settings['not_add_element'] : false;          //Не добавлять новые товары

        $get_parents_id = $this->get_parents(true);
        $get_parents_id = isset($get_parents_id['id']) ? $get_parents_id['id'] : array();
        $get_parents_name = isset($get_parents_id['name']) ? $get_parents_id['name'] : array();

        $get_list_type = $oC->getTypesByHierarchyTypeId($getIdBaseType);			//Получение всех типов данных
        $list_type = array_keys($get_list_type);
        
        foreach ($rows as $row){
            //Создание массива с данным объекта(товара) из текущей строки (ключ массива - идентификатор поля)
            $object = array();
            for ($col=0; $col<count($arr[0]); $col++){
                if (isset($id[$col]))
                    if ($id[$col]){
                        if (isset($arr[$row][$col])) $object[$id[$col]]['value'] = $arr[$row][$col]; else $object[$id[$col]]['value'] = false;
                        $get_filed_type = $this -> array_search_($id[$col], $fields_id, "name", "field_type");
                        if ($get_filed_type) $object[$id[$col]]['fieldType'] = $get_filed_type; else $object[$id[$col]]['fieldType'] = false;
                    }
            }

            $transArr = array();
            if (isset($cde) && $cde) $transArr['settings']['categoryDefault'] = $cde;
            if (isset($idd) && $idd) $transArr['settings']['typeDefault'] = $idd;
            if (isset($is) && $is) $transArr['settings']['isActiveDefault'] = $is;
            if (isset($key) && $key) $transArr['settings']['key'] = $key;
            if (isset($object) && $object) $transArr['row']=$object;

            $isset = $this -> obj_exists($file_name, $row); 	//Если объект существует, возвращает id объекта и его нужно обновить. "0" - не существует.

            $getPage = false;
            if ($isset) {
                $transArr['result']['sys']['id'] = $isset;
                $getPage = $hierarchy->getElement($isset, true);
                if (!$getPage instanceof umiHierarchyElement) $getPage = false;
            } else $transArr['result']['sys']['id'] = false;

            if (isset($object['id'])) unset($object['id']);

            //Определение id-parent для объекта ========================================================================
            if (isset($object['parent-id'])){
                if ((in_array($object['parent-id']['value'], $get_parents_id)) or (in_array($object['parent-id']['value'], $get_parents_name)))
                    $parent_id = $object['parent-id']['value'];
                else $parent_id = $cde;
            } else {
                if ($getPage) $parent_id = $getPage -> getParentId();
                else $parent_id = $cde;
            }
            $transArr['result']['sys']['parent-id'] = $parent_id;
            unset($object['parent-id']);
            //==========================================================================================================

            //Определение type-id для объекта ==========================================================================
            if (isset($object['type-id']) && $object['type-id'] && (in_array($object['type-id']['value'], $list_type))){
                $type_id = $object['type-id']['value'];
            }
            else {
                if ($getPage) $type_id = $getPage -> getObjectTypeId();
                else if (isset($idd) and $idd) $type_id = $idd; else $type_id = $list_type[0];
            }

            $transArr['result']['sys']['type-id'] = $type_id;
            unset($object['type-id']);
            //==========================================================================================================

            //Определение name для объекта =============================================================================
            if (isset($object['name'])) {
                if (isset($object['name']['value']) && $object['name']['value']) {
                    $name_obj = $object['name']['value'];
                }
                else $name_obj = "Без названия";
                unset($object['name']);
            } else $name_obj = "Без названия";
            if ($getPage && ($name_obj=="Без названия")) $name_obj = $getPage -> getName();
            $transArr['result']['sys']['name'] = $name_obj;
            //==========================================================================================================

            //Определение is-active для объекта ========================================================================
            if (isset($object['is-active'])){
                if (($object['is-active']['value'] == "1") or ($object['is-active']['value'] == "Да") or ($object['is-active']['value'] == "да") or ($object['is-active']['value'] == 1))
                    $is_active = true;
                else $is_active = false;
            }
            else {
                if ($getPage) $is_active = $getPage -> getIsActive();
                else {
                    $is_active = ($is=='1') ? true : false;
                }
            }
            $transArr['result']['sys']['is-active'] = $is_active;
            unset($object['is-active']);
            //==========================================================================================================

            //Сохранение остальных данных ==============================================================================
            foreach ($object as $field_=>$item){
                $transArr['result']['other'][$field_] = $item['value'];
            }
            $transArr['table_row'] = $arr[$row];
            if (isset($get_settings['template'])) $transArr['result']['sys']['template'] = $get_settings['template'];
            //==========================================================================================================
            $oEventPoint = new umiEventPoint("pricesImportProcessRow");
            $oEventPoint->setMode("before");
            $oEventPoint->addRef("prices_import", $transArr);
            $this->setEventPoint($oEventPoint);

            $import = $oEventPoint->getRef('prices_import');

            $get_settings['report']['processed']++;
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));

            if (isset($import['result']['sys']['continue'])) continue;

            $parent_id = $import['result']['sys']['parent-id'];

            $name_obj = addslashes($import['result']['sys']['name']);
            $is_active = $import['result']['sys']['is-active'];
            $type_id = $import['result']['sys']['type-id'];
            $isset = $import['result']['sys']['id'];
            
            //Проверка прав доступа ====================================================================================
            if (!$this -> getObjectPermission(($isset && is_numeric($isset)) ? $isset : false, $parent_id ? $parent_id : false, $getScheduleId)){
                continue;
            }
            //==========================================================================================================

            $rebuildRelationNodes = false;
            if (!$isset){
                if ($not_add_element) continue;
                
                //Создание объекта======================================================================================
                $id_obj = $hierarchy->addElement($parent_id, $getIdBaseType, $name_obj,'',$type_id);
                $element = $hierarchy->getElement($id_obj, true);
                if ($element instanceof umiHierarchyElement){
                    $element->setIsActive($is_active);
                    $element->commit();
                }
                $pC -> setElementPermissions($pC->getGuestId(), $id_obj, 1);
                $pC -> setElementPermissions($currentUserId, $id_obj, 31);
                $element_id_result = $id_obj;
                $obj_id = $element->getObjectId();
                //======================================================================================================
            } else{
                //Обновление объекта ===================================================================================
                $element = $hierarchy->getElement($isset, true);
                if ($element instanceof umiHierarchyElement){
                    //Определение типа объекта, при необходимости меняем тип
                    $objectPageTypeId = $element->getObjectTypeId();
                    if ($type_id != $objectPageTypeId) {
                        $objectPage =  $element -> getObject();
                        $objectPage -> setTypeId($type_id);
                        $objectPage -> commit();
                        $element = $hierarchy->getElement($isset, true);
                    }

                    $obj_id = $element->getObjectId();

                    $connection->query("UPDATE cms3_objects SET name = '{$name_obj}' WHERE id = {$obj_id}");
                    $connection->query("UPDATE cms3_hierarchy SET is_active = '{$is_active}' WHERE obj_id = {$obj_id}");
                    if ($element -> getParentId() != $parent_id) $rebuildRelationNodes = $parent_id;
                } else continue;
                $element_id_result = $isset;
                //======================================================================================================
            }

            //Создание точки восстановления
            //backupModel::getInstance() -> fakeBackup($element->getId());

            //Сохранение данных
            $get_other_items = $import['result']['other'];

            //Проверка наличия полей tilte и h1
            if (!isset($get_other_items['h1']) && !$isset) {
                $get_other_items['h1'] = $name_obj;
                $transArr['result']['other']['h1'] = $name_obj;
            }
            if (!isset($get_other_items['title']) && !$isset) {
                $get_other_items['title'] = $name_obj;
                $transArr['result']['other']['title'] = $name_obj;
            }

            $need_commit = 0;

            if ($element instanceof umiHierarchyElement) {
                foreach ($get_other_items as $field_=>$item) {
                    //Определение field_type (string, symlink, date, img_file ...) по названию поля
                    $field_id = intval($this -> array_search_($field_, $fields_id, "name", "field_id"));

                    $field_type = $this -> array_search_($field_, $fields_id, "name", "field_type");
                    $required = $this -> array_search_($field_, $fields_id, "name", "required");
                    if (($required == 1) && !$item) continue; //Если поле обязательное для заполнения, а значения нет

                    $old_mode = umiObjectProperty::$IGNORE_FILTER_INPUT_STRING;	//Откючение html сущн.
                    umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;
                    $item = addslashes(trim($item));

                    switch($field_type){
                        case 'int':
                            //Если указан склад
                            if (strpos($field_,"stores_state_") !== false){
                                $storesId = (int) trim(strtr($field_, array("stores_state_"=>"")));
                                if ($storesId){
                                    $stores_field = $regedit->getVal("//modules/prices_import/stores_field");
                                    if ($stores_field){
                                        $getFieldsStore = $element -> getValue($stores_field);
                                        if (!is_array($getFieldsStore)) $getFieldsStore = array();
                                        $indexStore = false;
                                        $totalAmount = 0;
                                        foreach($getFieldsStore as $index=>$getFieldStore)
                                            if ($getFieldStore['rel'] == $storesId) {
                                                $indexStore = $index;
                                            } else $totalAmount += $getFieldStore['int'];
                                        if ($indexStore === false){
                                            $getFieldsStore[] = array(
                                                'rel'=>$storesId,
                                                'int'=>(int) trim($item),
                                                'float'=>0
                                            );
                                        } else {
                                            $getFieldsStore[$indexStore] = array(
                                                'rel'=>$storesId,
                                                'int'=>(int) trim($item),
                                                'float'=>0
                                            );
                                        }
                                        $totalAmount += (int) trim($item);
                                        $element->setValue($common_quantity_field, $totalAmount);
                                        $element->setValue($stores_field,$getFieldsStore);
                                        $need_commit = 1;
                                    }
                                }
                            } else {
                                if($field_id) {
                                    $item = intval($item);
                                    $connection->query("UPDATE cms3_object_content SET int_val = {$item} WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                } else {
                                    $need_commit = 1;
                                    $element->setValue($field_,$item);
                                }
                            }
                            break;
                        case 'float':
                            if($field_id) {
                                $item = floatval($item);
                                $connection->query("UPDATE cms3_object_content SET float_val = {$item} WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                            } else {
                                $need_commit = 1;
                                $element->setValue($field_,$item);
                            }
                            break;
                        case 'price':
                            if($field_id) {
                                $item = floatval($item);
                                $connection->query("UPDATE cms3_object_content SET float_val = {$item} WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                            } else {
                                $need_commit = 1;
                                $element->setValue($field_,$item);
                            }
                            break;
                        case 'date':
                            $strtToTime = strtotime($item);
                            if ($strtToTime) {
                                if($field_id) {
                                    $connection->query("UPDATE cms3_object_content SET int_val = {$strtToTime} WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                } else {
                                    $need_commit = 1;
                                    $element->setValue($field_,$strtToTime);
                                }
                            }
                            break;
                        case 'string':
                            if($field_id) {
                                $connection->query("UPDATE cms3_object_content SET varchar_val = '{$item}' WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                            } else {
                                $need_commit = 1;
                                $element->setValue($field_,$item);
                            }
                            break;
                        case 'text':
                            if($field_id) {
                                $connection->query("UPDATE cms3_object_content SET text_val = '{$item}' WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                            } else {
                                $need_commit = 1;
                                $element->setValue($field_,$item);
                            }
                            break;
                        case 'wysiwyg':
                            if($field_id) {
                                $connection->query("UPDATE cms3_object_content SET text_val = '{$item}' WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                            } else {
                                $need_commit = 1;
                                $element->setValue($field_,$item);
                            }
                            break;
                        case 'tags':

                            break;
                        case 'relation': //Выпадающий список
                            if (($item == "") or (!$item)) {
                                if($field_id) {
                                    $connection->query("DELETE FROM cms3_object_content WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                } else {
                                    $element->setValue($field_,array());
                                    $need_commit = 1;
                                }
                                break;
                            }
                            //Получим тип данных по $type_id
                            $get_type_ = $oC->getType($type_id);
                            $id_fields_oc = $get_type_->getFieldId($field_);
                            if (!$id_fields_oc) break;
                            $getField = $fieldsCollection -> getField($id_fields_oc);
                            $getFieldType = $getField -> getFieldType();
                            $Multiple = false;
                            //Проверим, является ли это поле Multiple (с множественным выбором)
                            if ($getFieldType -> getIsMultiple()) {
                                $Multiple = true;
                            }

                            $id_sprav = (int) $fieldsCollection->getField($id_fields_oc)->getGuideId();

                            $resArrList = array();
                            $list_elements = array();

                            //Проверим, в ячейке одно значение или несколько (из реестра берем разделитель)
                            if($Multiple) {
                                $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");
                                $itemValue = trim($item);
                                $itemValue = trim($itemValue, $symb_separate_type_id);
                                $list_elements = explode($symb_separate_type_id, $itemValue);
                            } else {
                                $list_elements[] = trim($item);
                            }

                            if(count($list_elements)) {
                                $q = "SELECT id, LOWER(name) FROM cms3_objects WHERE type_id = {$id_sprav} AND (";
                                $_list_elements = array();
                                $_list_elements_orig = array();
                                foreach($list_elements as $list_element){
                                    $_list_element = mb_strtolower(trim($list_element), "UTF-8");
                                    $_list_elements[] = $_list_element;
                                    $_list_elements_orig[] = $list_element;
                                    $q .= "LOWER(name) LIKE '{$_list_element}' OR ";
                                }
                                $q = substr($q, 0, -4);
                                $q .= ') GROUP BY LOWER(name)';

                                $qresult = $connection->queryResult($q);
                                $qresult->setFetchType(IQueryResult::FETCH_ROW);
                                if($qresult->length()) {
                                    foreach($qresult as $row) {
                                        $pos = array_search($row[1], $_list_elements);
                                        if($pos !== FALSE && isset($_list_elements[$pos])) {
                                            unset($_list_elements[$pos]);
                                            $resArrList[] = $row[0];
                                        }
                                    }
                                }

                                if(count($_list_elements)) {

                                    foreach($_list_elements as $key => $_element) {
                                        if(isset($_list_elements_orig[$key]) && strlen($_list_elements_orig[$key])) {
                                            $get_obj_id = $uOC -> addObject($_list_elements_orig[$key], $id_sprav);
                                            $resArrList[] = $get_obj_id;
                                        }
                                    }
                                }

                                if(count($resArrList)) {
                                    if($field_id) {
                                        $connection->query("DELETE FROM cms3_object_content WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                        foreach($resArrList as $resArrId) {
                                            $connection->query("INSERT INTO cms3_object_content(obj_id,field_id,rel_val) VALUES({$obj_id},{$field_id},{$resArrId})");
                                        }
                                    } else {
                                        $need_commit = 1;
                                        if($Multiple) {
                                            $element->setValue($field_, $resArrList);
                                        } else {
                                            $element->setValue($field_, reset($resArrList));
                                        }
                                    }
                                }
                            }
                            break;
                        case 'img_file':
                            //Проверка абсолютный путь или нет. Если абсолютный переносим файл в каталог
                            if (strpos($item,"http://") === false && strpos($item,"https://") === false) {
                                $imageName = trim($item,".");
                                $imageName = trim($imageName, "/");
                                if(file_exists($root."/".$imageName) && umiImageFile::getIsImage($root."/".$imageName)) {
                                    if($field_id) {
                                        $connection->query("UPDATE cms3_object_content SET text_val = './{$imageName}' WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                    } else {
                                        $element->setValue($field_,"./".$imageName);
                                        $need_commit = 1;
                                    }
                                }
                            } else {
                                //if(!ini_get('allow_url_fopen')) {
                                //    ini_set('allow_url_fopen', 1);
                                //}
                                $verifyValidImg = getimagesize($item);
                                if ($verifyValidImg) {
                                    $imageName = basename($item);
                                    $imageName_a = explode(".",$imageName);
                                    $folderName = $this->NormalizeStringToURL("import_".substr($get_settings['file_name'], 0, strpos($get_settings['file_name'],"."))."_".date('d_m_Y',$file_name));
                                    if (!file_exists($root . "/images/cms/data/".$folderName)) {
                                        mkdir($root . "/images/cms/data/".$folderName);
                                    }
                                    if (count($imageName_a>1)){
                                        $imageName = md5($item).".".end($imageName_a);
                                    }
                                    copy($item, $root . "/images/cms/data/".$folderName."/".$imageName);
                                    if(umiImageFile::getIsImage($root . "/images/cms/data/".$folderName."/".$imageName)) {
                                        if($field_id) {
                                            $connection->query("UPDATE cms3_object_content SET text_val = './images/cms/data/{$folderName}/{$imageName}' WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                        } else {
                                            $element -> setValue($field_,"./images/cms/data/".$folderName."/".$imageName);
                                            $need_commit = 1;
                                        }
                                    }
                                }
                            }
                            break;
                        case 'swf_file':
                        case 'video_file':
                        case 'file':
                            //Проверка абсолютный путь или нет. Если абсолютный переносим файл в каталог
                            if (strpos($item,"http://") === false && strpos($item,"http://") === false) {
                                $imageName = trim($item,".");
                                $imageName = trim($imageName, "/");
                                if(file_exists($root."/".$imageName)) {
                                    if($field_id) {
                                        $connection->query("UPDATE cms3_object_content SET text_val = './{$imageName}' WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                    } else {
                                        $element->setValue($field_,"./".$imageName);
                                        $need_commit = 1;
                                    }
                                }
                            } else {
                                $imageName = basename($item);
                                $imageName_a = explode(".",$imageName);
                                $folderName = $this->NormalizeStringToURL("import_".substr($get_settings['file_name'], 0, strpos($get_settings['file_name'],"."))."_".date('d_m_Y',$file_name));
                                if (!file_exists($root . "/files/".$folderName)) {
                                    mkdir($root . "/files/".$folderName);
                                }

                                if (count($imageName_a>1)){
                                    $imageName = md5($item).".".end($imageName_a);
                                }
                                copy($item, $root . "/files/".$folderName."/".$imageName);
                                if($field_id) {
                                    $connection->query("UPDATE cms3_object_content SET text_val = './files/{$folderName}/{$imageName}' WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                } else {
                                    $element->setValue($field_ ,"./files/".$folderName."/".$imageName);
                                    $need_commit = 1;
                                }
                            }
                            break;
                        case 'boolean':	//Флажок
                            if (($item == "1") || ($item == "Да") || ($item == "да") || ($item == 1)) {
                                if($field_id) {
                                    $connection->query("UPDATE cms3_object_content SET int_val = 1 WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                } else {
                                    $element->setValue($field_, true);
                                    $need_commit = 1;
                                }
                            } else {
                                if($field_id) {
                                    $connection->query("UPDATE cms3_object_content SET int_val = 0 WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                                } else {
                                    $element->setValue($field_, false);
                                    $need_commit = 1;
                                }
                            }
                            break;
                        case 'password':
                            if($field_id) {
                                if(strlen($item)) {
                                    $item = md5($item);
                                }
                                $connection->query("UPDATE cms3_object_content SET varchar_val = '{$item}' WHERE field_id = {$field_id} AND obj_id = {$obj_id}");
                            } else {
                                $element->setValue($field_, $item);
                                $need_commit = 1;
                            }
                            break;
                        case 'optioned':
                            if (($item == "") or (!$item)) {
                                $element->setValue($field_,array());
                                break;
                            }
                            //Получим тип данных по $type_id
                            $get_type_ = $oC->getType($type_id);
                            $id_fields_oc = $get_type_->getFieldId($field_);
                            if (!$id_fields_oc) break;
                            $id_sprav = (int) $fieldsCollection->getField($id_fields_oc)->getGuideId();
                            $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");
                            $itemValue = trim($item);
                            $itemValue = trim($itemValue, $symb_separate_type_id);

                            $list_elements = explode($symb_separate_type_id, $itemValue);
                            $options = array();
                            $get_obj_id = false;
                            foreach($list_elements as $list_element){
                                $objs = new selector('objects');
                                $objs->types('object-type')->id($id_sprav);
                                $objs->where('name')->equals(trim($list_element));
                                if (!$objs->length) $get_obj_id = $uOC -> addObject(trim($list_element), $id_sprav);
                                else {
                                    $res_objs = $objs->first;
                                    if (is_object($res_objs)) $get_obj_id = $res_objs -> getId();
                                }
                                if ($get_obj_id) {
                                    $option_ = array();
                                    $option_['rel'] = $get_obj_id;
                                    $option_['int'] = 1;
                                    $option_['float'] = 0;
                                    $options[] = $option_;
                                }
                            }
                            $element->setValue($field_,$options);
                            break;
                        case 'counter':
                            $element->setValue($field_,$item);
                            $need_commit = 1;
                            break;
                        case 'symlink': //Ссылка на дерево
                            if (($item == "") or (!$item)) {
                                $element->setValue($field_,array());
                                break;
                            }
                            $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");
                            $itemValue = trim($item);
                            $itemValue = trim($itemValue, $symb_separate_type_id);
                            if (strpos($itemValue, $symb_separate_type_id)){
                                $list_elements = explode($symb_separate_type_id, $itemValue);
                                foreach($list_elements as $i=>$list_element){
                                    $list_elements[$i] = trim($list_element);
                                    if (!$hierarchy->getElement($list_elements[$i])) unset($list_elements[$i]);
                                }
                                if (is_array($list_elements) && count($list_elements)) $element -> setValue($field_,$list_elements);
                            } else if (is_numeric($itemValue)) {
                                if ($hierarchy->getElement($itemValue)) $element -> setValue($field_,array($itemValue));
                            }
                            $need_commit = 1;
                            break;
                    }
                }
            }

            if($rebuildRelationNodes) {
                $need_commit = 1;
                $element->setRel($rebuildRelationNodes);
            }

            if($need_commit) {
                $element->commit();
            }

            if($rebuildRelationNodes) {
                $hierarchy->rebuildRelationNodes($rebuildRelationNodes); //Обновление связей, в связи с недоработкой setRel()
            }

            umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = $old_mode;	//Вкл. html сущн.
            unset($id_obj);

            //Сохранение отчета
            if ($isset)	$get_settings['report']['updated']++; else $get_settings['report']['created']++;

            $oEventPointAfter = new umiEventPoint("pricesImportProcessRow");
            $oEventPointAfter->setMode("after");
            $transArr['result']['sys']['id'] = $element_id_result;
            $oEventPointAfter->addRef("prices_import", $transArr);
            $this->setEventPoint($oEventPointAfter);

            $get_settings['report']['last_activity'] = time();  //Время последней активности
            file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));

            if ($getScheduleId) {
                $getSchedule = $uOC -> getObject($getScheduleId);
                if (is_object($getSchedule)){
                    $getSchedule -> setValue("last_date",time());
                    $getSchedule -> commit();
                }
                if ((time() - $lastCronTime) > $max_sched_time) break;
            }
        }
        return array("total"=>count($selected_rows),"updated"=>$get_settings['report']['updated'], "created"=>$get_settings['report']['created']);
    }

    //Установки перед началом импорта
    public function start_import($file_name = false){
        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);

        $root = CURRENT_WORKING_DIR;
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        //Очистка отчета
        if (isset($get_settings['report'])) {unset($get_settings['report']); file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));}

        //Формирование массива для передачи данных
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        $transArr = array();
        $transArr['categoryDefault'] = (isset($get_settings['category_default']) and $get_settings['category_default']) ? $get_settings['category_default'] : '';
        $transArr['typeDefault'] = (isset($get_settings['type_id_default']) and $get_settings['type_id_default']) ? $get_settings['type_id_default'] : '';
        $transArr['key'] = (isset($get_settings['key']) and $get_settings['key']) ? $get_settings['key'] : '';
        $transArr['is_active_default'] = (isset($get_settings['is_active_default']) and $get_settings['is_active_default']) ? $get_settings['is_active_default'] : '';
        $transArr['template'] = (isset($get_settings['template']) and $get_settings['template']) ? $get_settings['template'] : '';

        //Событие перед началом импорта
        $oEventPoint = new umiEventPoint("pricesImportFile");
        $oEventPoint->setMode("before");
        $oEventPoint->addRef("prices_import", $transArr);
        $this->setEventPoint($oEventPoint);
        //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Начат импорт (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
        return;
    }

    //Установки по окончанию импорта
    public function end_import($file_name = false){
        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);

        $root = CURRENT_WORKING_DIR;
        //Формирование массива для передачи данных
        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        $transArr = array();
        $transArr['categoryDefault'] = (isset($get_settings['category_default']) and $get_settings['category_default']) ? $get_settings['category_default'] : '';
        $transArr['typeDefault'] = (isset($get_settings['type_id_default']) and $get_settings['type_id_default']) ? $get_settings['type_id_default'] : '';
        $transArr['key'] = (isset($get_settings['key']) and $get_settings['key']) ? $get_settings['key'] : '';
        $transArr['is_active_default'] = (isset($get_settings['is_active_default']) and $get_settings['is_active_default']) ? $get_settings['is_active_default'] : '';
        $transArr['template'] = (isset($get_settings['template']) and $get_settings['template']) ? $get_settings['template'] : '';

        //Событие по окончанию импорта
        $oEventPoint = new umiEventPoint("pricesImportFile");
        $oEventPoint->setMode("after");
        $oEventPoint->addRef("prices_import", $transArr);
        $this->setEventPoint($oEventPoint);

        //file_put_contents($root."/styles/skins/modern/data/modules/prices_import/system/log.txt", "[".date("d.m.Y, H:i:s",time())."] Импорт завершен (id = ".$file_name.")".PHP_EOL,FILE_APPEND);
        return;
    }

    //Проверка на существование объекта (строки) в базе по ее номеру строки в основном массиве
    public function obj_exists($file_name = false, $row=false) {
        $hierarchy = umiHierarchy::getInstance();
        $typesCollection = umiObjectTypesCollection::getInstance();
        $root = CURRENT_WORKING_DIR;

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set"))
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        if (file_exists($root . "/files/filesToImport/".$file_name.".arr"))
            $arr = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".arr"));	//Массив данных
        if (isset($get_settings['id'])) $id = $get_settings['id'];
        $key = false; if (isset($get_settings['key'])) $key = $get_settings['key'];                     //Ключевой идентификатор

        //Определение типа поля $field_type и его значения $field_value по ключевому идентификатору
        $isset = true;
        if ($key){
            if ($key=="id"){
                $id_ = array_search("id",$id);
                if ($id_ !== false){
                    $id_obj = $arr[$row][$id_];		// id текущего объекта (строки)
                    if (!$hierarchy->isExists($id_obj)) return false;
                    return $id_obj;
                }
            } else {
                $field_type =$key;
                $id_ = array_search($field_type, $id);
                if ($id_ !== false){
                    $value = $arr[$row][$id_];
                    $pages = new selector('pages');
                    $pages->types('hierarchy-type')->name('catalog', 'object');

                    //Выбор только тех типов объектов каталога, в которых есть поле $field_type========
                    $get_fields_type = $this->get_fields_type(false,true);
                    $get_types = $this -> array_search_($field_type, $get_fields_type, "name", "types");
                    if (is_array($get_types) && count($get_types)){
                        foreach($get_types as $type){
                            $pages->types('object-type')->id($type);
                        }
                    }
                    //=================================================================================

                    switch($field_type){
                        case 'is-active':
                            $pages->where('is_active')->equals($value);
                            break;
                        case 'type-id':
                            //Проврка наличия типа данных
                            if (!$typesCollection->getType($value)) return false;
                            $pages->types('object-type')->id($value);
                            break;
                        case 'parent-id':
                            return false;
                        default:
                            $pages->where('is_active')->equals(array(0,1));
                            $pages->where($field_type)->equals($value);
                            break;
                    }

                    $query = $pages->query();
                    
                    $connection = ConnectionPool::getInstance()->getConnection();
                    $qresult = $connection->queryResult($query);
                    $qresult->setFetchType(IQueryResult::FETCH_ASSOC);
                    $num = intval($connection->affectedRows());
                    if($qresult->length()) {
                        $row = $qresult->fetch();
                        $obj_id = isset($row['id']) ? $row['id'] : false;
                        return $obj_id;
                    }
                }
            }
        }
        return false;
    }

    //Изменение активности по умолчанию
    public function default_is_active($file_name = false, $active=false){
        $root = CURRENT_WORKING_DIR;
        if ($active == "true") $active=1; else $active=0;

        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        $get_settings['is_active_default'] = $active;
        file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        return "";
    }

    //Изменение параметра "Не добавлять новые товары"
    public function not_add_element($file_name = false, $active=false){
        $root = CURRENT_WORKING_DIR;
        if ($active == "true") $active=1; else $active=0;

        $get_settings = array();
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        }
        $get_settings['not_add_element'] = $active;
        file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
        return "";
    }

    //Транслитерация
    public function NormalizeStringToURL( $string )
    {
        static $lang2tr = array(
            // russian
            'й'=>'j','ц'=>'c','у'=>'u','к'=>'k','е'=>'e','н'=>'n','г'=>'g','ш'=>'sh',
            'щ'=>'sh','з'=>'z','х'=>'h','ъ'=>'','ф'=>'f','ы'=>'y','в'=>'v','а'=>'a',
            'п'=>'p','р'=>'r','о'=>'o','л'=>'l','д'=>'d','ж'=>'zh','э'=>'e','я'=>'ja',
            'ч'=>'ch','с'=>'s','м'=>'m','и'=>'i','т'=>'t','ь'=>'','б'=>'b','ю'=>'ju','ё'=>'e','и'=>'i',

            'Й'=>'J','Ц'=>'C','У'=>'U','К'=>'K','Е'=>'E','Н'=>'N','Г'=>'G','Ш'=>'SH',
            'Щ'=>'SH','З'=>'Z','Х'=>'H','Ъ'=>'','Ф'=>'F','Ы'=>'Y','В'=>'V','А'=>'A',
            'П'=>'P','Р'=>'R','О'=>'O','Л'=>'L','Д'=>'D','Ж'=>'ZH','Э'=>'E','Я'=>'JA',
            'Ч'=>'CH','С'=>'S','М'=>'M','И'=>'I','Т'=>'T','Ь'=>'','Б'=>'B','Ю'=>'JU','Ё'=>'E','И'=>'I',
            // czech
            'á'=>'a', 'ä'=>'a', 'ć'=>'c', 'č'=>'c', 'ď'=>'d', 'é'=>'e', 'ě'=>'e',
            'ë'=>'e', 'í'=>'i', 'ň'=>'n', 'ń'=>'n', 'ó'=>'o', 'ö'=>'o', 'ŕ'=>'r',
            'ř'=>'r', 'š'=>'s', 'Š'=>'S', 'ť'=>'t', 'ú'=>'u', 'ů'=>'u', 'ü'=>'u',
            'ý'=>'y', 'ź'=>'z', 'ž'=>'z',

            'і'=>'i', 'ї' => 'i', 'b' => 'b', 'І' => 'i',
            // special
            ' '=>'-', '\''=>'', '"'=>'', '\t'=>'', '«'=>'', '»'=>'', '?'=>'', '!'=>'', '*'=>''
        );
        $url = preg_replace( '/[\-]+/', '-', preg_replace( '/[^\w\-\*]/', '', strtolower( strtr( $string, $lang2tr ) ) ) );
        //echo $url."<br>";
        return  $url;
    }

    public function transformXML(){
        $stylesheetfile = getRequest('xslpath');
        $require = getRequest('require');

        $result = file_get_contents("udata://".$require);
        $response = "";
        $proc = new XSLTProcessor;
        if ($proc->hasExsltSupport()) {
            $xslDoc = new DOMDocument();
            $xslDoc->load(CURRENT_WORKING_DIR."/".$stylesheetfile);
            $fpath =  CURRENT_WORKING_DIR . "/files/filesToImport/";
            $fname = md5(time()) . ".xml";
            file_put_contents($fpath.$fname, $result);

            $xmlDoc = new DOMDocument();
            $xmlDoc->load($fpath.$fname);
            $proc->importStylesheet($xslDoc);
            $response = $proc->transformToXML($xmlDoc);
            unlink($fpath.$fname);
        }
        $buffer = outputBuffer::current();
        $buffer->charset('utf-8');
        $buffer->contentType('text/html');
        $buffer->clear();
        $buffer->push(str_replace("<?xml version=\"1.0\"?>","",$response));
        $buffer->end();
    }

    public function getPerPage(){
        return $this->per_page;
    }

    public function set_params_div($file_name = false, $langId = false){
        if (!$file_name) return "";
        $regedit = regedit::getInstance();
        $def_parentId = unserialize($regedit->getVal("//modules/prices_import/def_parentId"));
        if (is_numeric($langId))
            $def_parentId = isset($def_parentId[$langId]) ? $def_parentId[$langId] : 0;
        else $def_parentId = 0;

        $def_typeId = (int) $regedit->getVal("//modules/prices_import/def_typeId");
        if ((boolean) $regedit->getVal("//modules/prices_import/def_isActive")) $def_isActive = 1; else $def_isActive = 0;
        $encoding = (string) $regedit->getVal("//modules/prices_import/encoding");
        $symb_separate_csv = (int) $regedit->getVal("//modules/prices_import/symb_separate_csv");

        $root = CURRENT_WORKING_DIR;
        $userFileName_ = "";
        if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
            $userFileName = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
            $userFileName = explode(".",$userFileName['file_name']);
            $userFileName_ = $userFileName[0];
            $userFileType_ = (isset($userFileName[1]) && $userFileName[1]) ? $userFileName[1] : '';
        }
        $templates_arr = array();
        if (file_exists($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr")){
            $templates_arr = unserialize(file_get_contents($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr"));
        }
        if (isset($templates_arr[$userFileName_])){
            for($i=1; $i<1000; $i++) if (isset($templates_arr[$userFileName_."_".$i])) continue; else break;
            $userFileName_ = $userFileName_."_".$i;
        }

        return array("file_name"=>$file_name, "user_file_name"=>$userFileName_, "user_file_type"=>$userFileType_, "def_parentId"=>$def_parentId, "def_typeId"=>$def_typeId, "def_isActive"=>$def_isActive, "encoding"=>$encoding, "lang_id"=>$langId, "symb_separate_csv"=>$symb_separate_csv);
    }

    //Поиск
    public function search($file_name = false, $row = 0, $col = 0, $next = true){
        $str = getRequest("search");
        if ($str == '') return "empty";
        $root = CURRENT_WORKING_DIR;

        register_shutdown_function(array($this, 'FatalErrorCatcher'), $this->cron);
        $regedit = regedit::getInstance();
        $getMemory_limit = (int) ini_get('memory_limit');
        $setMemory_limit = $regedit->getVal("//modules/prices_import/memory_limit");
        if (($getMemory_limit != $setMemory_limit) && is_numeric($setMemory_limit))
            ini_set('memory_limit', $setMemory_limit.'M');

        if (file_exists($root . "/files/filesToImport/".$file_name.".arr"))
            $arr = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".arr"));	//Массив данных

        if (!$row or !is_numeric($row)) $row = 0; if (!$col or !is_numeric($col)) $col = 0;

        if ($next){
            for($y = $row; $y<count($arr); $y++){
                for($x = ($y == $row) ? ($col+1) : 0; $x<count($arr[0]); $x++){
                    if (mb_strpos(mb_strtolower($arr[$y][$x]), mb_strtolower($str)) === false) {} else return array("row"=>$y, "col"=>$x);
                }
            }
            //Если не было найдено, поиск осуществляется с начала
            for($y = 0; $y<count($arr); $y++){
                for($x = 0; $x<count($arr[0]); $x++){
                    if (mb_strpos(mb_strtolower($arr[$y][$x]), mb_strtolower($str)) === false) {} else return array("row"=>$y, "col"=>$x);
                }
            }
            return "empty";
        } else {
            for($y = $row; $y>=0; $y--){
                for($x = ($y == $row) ? ($col-1) : count($arr[0]); $x>=0; $x--){
                    if (mb_strpos(mb_strtolower($arr[$y][$x]), mb_strtolower($str)) === false) {} else return array("row"=>$y, "col"=>$x);
                }
            }
            //Если не было найдено, поиск осуществляется с конца
            for($y = count($arr); $y>=0; $y--){
                for($x = count($arr[0]); $x>=0; $x--){
                    if (mb_strpos(mb_strtolower($arr[$y][$x]), mb_strtolower($str)) === false) {} else return array("row"=>$y, "col"=>$x);
                }
            }
            return "empty";
        }
        return "empty";
    }

    //Универсальное чтение ячеек таблицы excel
    public function getCellValue($aSheet='', $cell='', $format = 'd.m.Y'){
        //try to find current coordinate in all merged cells ranges
        //if find -> get value from head cell
        foreach($aSheet->getMergeCells() as $currMergedRange){
            if($cell->isInRange($currMergedRange)) {
                $currMergedCellsArray = PHPExcel_Cell::splitRange($currMergedRange);
                $cell = $aSheet->getCell($currMergedCellsArray[0][0]);
                break;
            }
        }
        //simple value
        $val = $cell->getValue();
        //date
        if(PHPExcel_Shared_Date::isDateTime($cell)) {
            $val = date($format, PHPExcel_Shared_Date::ExcelToPHP($val));
        }
        //for incorrect formulas take old value
        if((substr($val,0,1) === '=' ) && (strlen($val) > 1)){
            $val = $cell->getOldCalculatedValue();
        }
        return $val;
    }

    //Проверка прав на изменение, перенос объекта каталога для текущего пользователя
    public function getObjectPermission($objectCatalogId = false, $newParentId = false, $getScheduleId = false){
        $hierarchy = umiHierarchy::getInstance();
        $pC = permissionsCollection::getInstance();
        $uOC = umiObjectsCollection::getInstance();
        $currentUserId = $pC->getUserId();

        if ($getScheduleId !== false)
            if (is_numeric($getScheduleId)){
                $getSchedule = $uOC -> getObject($getScheduleId);
                if (is_object($getSchedule)){
                    $getOwnerId = $getSchedule->getOwnerId();
                    if ($getOwnerId !== Null) $currentUserId = $getOwnerId;
                }
            }

        //Проверка доступа на изменение объекта
        if (is_numeric($objectCatalogId)){
            $getElement = $hierarchy -> getElement($objectCatalogId);
            if ($getElement instanceof umiHierarchyElement){
                $getPerm = $pC -> isAllowedObject($currentUserId, $objectCatalogId);
                $parrentId = $hierarchy -> getParent($objectCatalogId);



                //Проверка доступа на изменение категории
                if (is_numeric($newParentId) && $parrentId && ($newParentId!=$parrentId))
                    if (!$getPerm[4]) return false;

                //Проверка доступа на изменение объекта
                if (!$getPerm[1]) return false;
            }
        }

        return true;
    }


    //Автоопределение кодировки ========================================================================================
    //==================================================================================================================
    // libs/a/a.charset.php
    // (c) Yuri Popoff, Nov 2003, popoff.donetsk.ua
    // A set of functions to process charsets

    public function _charset_count_bad($s)
    { //count "bad" symbols in russian, in windows-1251
        $r=0;
        for($i=0;$i<strlen($s);$i++)
        {
            switch($s[$i])
            {
                case 'ё':
                case 'Ё':
                case '«':
                case '»':
                    break;
                default:
                    $c=ord($s[$i]);
                    if($c>=0x80&&$c<0xc0||$c<32)
                        $r++;
            }
        }
        return $r;
    }

    public function _charset_count_chars($s)
    { //count "good" symbols in russian, in windows-1251
        $r=0;
        for($i=0;$i<strlen($s);$i++)
        {
            $c=ord($s[$i]);
            if($c>=0xc0)
                $r++;
        }
        return $r;
    }

    public function _charset_count_pairs($s)
    { //count "bad" pairs of chars for a string in russian, in windows-1251
        $a=array(
            0 => 'ъыь',
            1 => 'йпфэ',
            2 => 'йфэ',
            3 => 'жйпфхцщъыьэю',
            4 => 'йфщ',
            5 => 'ъыь',
            6 => 'зйтфхшщъыэя',
            7 => 'йпфхщ',
            8 => 'ъыь',
            9 => 'абжийущъыьэюя',
            10 => 'бгйпфхщъыьэюя',
            11 => 'йрцъэ',
            12 => 'джзйъ',
            13 => 'ймпъ',
            14 => 'ъыь',
            15 => 'бвгджзйхщъэю',
            16 => 'йъэ',
            17 => 'й',
            18 => 'жй',
            19 => 'ъыь',
            20 => 'бвгджзйкпхцшщъьэюя',
            21 => 'бжзйфхцчщъыьюя',
            22 => 'бгджзйлнпрстфхцчшщъьэюя',
            23 => 'бгджзйпсфхцчщъыэюя',
            24 => 'бгджзйфхшщъыэя',
            25 => 'бвгджзйклмпстфхцчшщъыэюя',
            26 => 'абвгджзийклмнопрстуфхцчшщъыьэ',
            27 => 'аофъыьэю',
            28 => 'айлрухъыьэ',
            29 => 'абежиоуцчшщъыьэю',
            30 => 'иоуфъыьэя',
            31 => 'аоуфъыьэ'
        );
        $b=array(
            0  => 'ааабавагадаеажазаиайакаланаоасатауафахацачашащаэаюаябгбмбтбхбцбчбшбщбъбьбюбявбвжвхвъвюгзгкгтгчгядддхдэеаебегееежеиеоепесеуефецещеэеюеяжбжвжлжпжржцжчжюзззсзтзшзэзюиаиеижииийиоипиуифицишищиэиюияйпйркзкмкчкшлблвлзлнлшлщмвмгмхмчмэмюнбнвнэоаовогоеожозоиойоколомооопоуофохоцошощоэоюояпмпцрзсгсдсжсзсъсэтбтгтдтзтптштщтътэуаубувужуиуйуоуууфухуцущуюуяфлфмхгхдхкхпхсхшхэцвцмцуцычвчмчрчшшршсшчщнщрщьэвэгэдэзэйэкэмэнэпэтэфэхэяюаюбювюгюдюеюжюзюйюлюмюнюпюрюхюцюшююябягядяеяжязяияйяпяряшящяюяя',
            1  => 'ааажаоапафащаэбабббвбгбдбжбзбкблбмбнбсбтбубхбцбчбшбщбъбыбьбюбявбвввгвдвжвзвквлвмвнвпврвсвтвувхвцвчвшвщвъвьвювягагбгвгггдгегзгигкглгмгнгргсгтгугчгшгядбдвдгдддждздкдлдмдндодпдрдсдтдхдцдчдшдъдыдьдэдюеаебепеуефеэеяжбжвжгжджжжкжлжмжнжожпжржсжцжчжьжюзбзвзгздзезжзззизкзлзмзнзрзсзтзузцзчзшзъзызьзэзюзяиаиэквкдкжкзккклкмкнкскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщмбмвмгмкмлмммнмпмрмсмтмумфмхмцмчмшмщмымьмэмюнбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаооофохрбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщсбсвсгсдсжсзсмснспсрсссфсхсчсшсщсъсысьсэсютатбтвтгтдтзтитктлтмтнтптртстттутфтхтцтчтштщтътытьтэтюуоуууцущуэхгхдхехихкхлхмхнхпхрхсхтхухшхэцвцицкцмцучвчечкчлчмчнчочрчтчучшчьшвшкшлшмшншошпшршсштшушцшчшьшющощрщьъюыбыгыжыиыпырыуыцышыяьбьвьгьдьжьзькьмьньоьпьсьтьфьцьчьшьщюаюбювюгюеюжюзюйюкюмюнюпюхюцючюшющююябявягядяеяжяияйякянярясяхяцячяшяюяя',
            2  => 'аааоауафащаэбабббвбгбдбжбзбкбмбнбсбтбхбцбчбшбщбъбыбьбюбявбвввгвдвжвзвквлвмвнвпвтвувхвцвчвшвщвъвьвюгагбгвгггдгегзгигкгмгнгсгтгчгшгядбдгдддждздкдлдмдндпдсдтдхдцдчдшдъдьдэдюдяеаеиеуефеэжажбжвжгжджежжжкжлжмжнжожпжржсжужцжчжьжюзезжзззкзсзтзузцзчзшзьзэзюиуифиэквкдкжкзкккмкскткцкчкшлблвлглдлжлзлклллмлнлплслтлулфлхлчлшлщлымбмвмгмкмлмммнмпмрмсмтмумфмхмцмчмшмщмьмэмюнбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаофоэпкпмпнпппсптпфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюсбсвсгсдсжсзсиснсрсссфсцсчсшсщсъсьсэтбтвтгтдтзтктлтмтнтптстттутфтхтцтчтштщтътьтэтюубувужуиуоупуууфуцуэхахвхгхдхехихкхлхмхнхпхрхсхтхухшхэцвцицкцмчвчкчлчмчнчрчтчшчьшвшкшлшмшншпшршсштшцшчшьшющащещнщощрщущьъюъяыщьбьвьгьдьжьзькьмьньоьпьфьцьчьшьщюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюсютюхюцючюшющююябявягядяеяияйяпяряцячяшяюяя',
            3  => 'ааакаоафашаэбббвбгбдбжбзбибкблбмбнбобрбсбтбубхбцбчбшбщбъбыбьбюбявбвввгвдвжвзвквлвмвнвпврвсвтвувхвцвчвшвщвъвывьвювягагбгвгггдгзгкглгмгнгогргсгтгугчгшгядбдвдгдддждздидкдлдмдндпдрдсдтдудхдцдчдшдъдыдьдэдюдяеаевегежезепеуехецечешещеэзбзвзгздзезжзззизкзлзмзнзозрзсзтзузцзчзшзъзызьзэзюзяижиуифицищиэквкдкекжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщльмбмвмгмимкмлмммнмпмрмсмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщныньнэоюрбрвргрдржрзркрлрмрпрррсртрфрхрцрчршрщрьсасбсвсгсдсесжсзсислсмснспсрсссусфсхсцсчсшсщсъсысьсэсютатбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътытьтэувугузуиуйукуоупуууфуцуэуячвчкчлчмчнчочрчтчшчьшашвшкшлшмшншошпшршсшушцшчшьшюябявягяеяжязяияйякяляняпярясятяхяцячяшящяюяя',
            4  => 'ааазауащаэбббвбгбдбжбзбкблбмбнбсбтбубхбцбчбшбщбъбыбьбюбявбвввгвдвжвмвнвпврвсвтвхвцвчвшвщвъвьвювягбгвгггдгегзгкгмгнгсгтгугчгшгядбдгдддждздкдлдмдндпдрдсдтдхдцдчдшдъдыдьдэдюдяеуехещеэжбжвжгжджжжкжлжмжнжпжржсжцжчжьжюзбзвзгздзжзззкзлзмзрзсзтзцзчзшзъзызьзюзяигихиэквкдкжкзкккмкнкскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщльлюмбмвмгмкмлмммпмрмсмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнлнннрнснфнхнцнчншнщньнэоаофоэоюояпепкпмпнпппсптпфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпррртрфрхрцрчршрщрьсбсгсдсжсзснсрсссфсхсцсшсщсъсысьсэсюсятбтгтдтзтктлтмтнтптстттутфтхтцтчтштщтътытьтэтюудузуфхгхдхихкхлхмхнхпхрхсхтхухшхэцвцицкцмчвчкчлчмчнчочрчтчшчьшвшкшлшмшншошпшршсшцшчшьшюызыиыуыцыяьвьгьдьжьзьньоьпьсьфьцьчьшьщэгэдэзэйэлэмэпэсэтэфэхэяюаюбювюгюдюеюлюмюсютюхюцючющююябявяеяжязякяляпяряцяшяюяя',
            5  => 'аааеажазаиайаоапарасауафахацачашащаэаюаябббвбгбдбжбзбхбцбчбшбщбъвбвввгвжвпвхвщвъвюгбгвгггзгтгшдгдхдцдюеаебегедееежеиеленеоеуефещеэеюеяжбжвжжжмжчжюзсзцзшзъзэиаибидиеижииийиоириуифицичишищиэиюияйвйойхкжкзкккмкчлдлжлзлплфлхлшлщмвмрмфмхмшмэмюнжнлнфнэоаоеоиойоуочошоюояпмпппфрщсгсжсзсщсъсэсютгтдтзтптфтцтщтътэтюуауиуйуоуууфуцушущуэфмфнфсфчфыхгхкхрхтхшцвцмцучвчмчрчшшвшмшпшршцшчшющнщоэвэгэдэзэйэлэмэнэпэрэсэхэяюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюхюцючюшююябягяеяжязяияйякяпяцячяшящяюяя',
            6  => 'ааабазаиаоапауафацашаэбббвбгбдбжбзбкблбмбнбрбсбтбхбцбчбшбщбъбьбюбявбвввгвдвевжвзвивквлвмвнвовпврвсвтвувхвцвчвшвщвъвывьвювягбгвгггдгегзгкгмгнгогргсгтгчгшгядбдвдгдддждздкдмдндпдрдсдтдхдцдчдшдъдэеаежеиеоеуехеэеюеяжбжвжгжджжжкжлжмжнжожпжржсжужцжчжьжюиаибиуифиэквкдкжкзккклкмкнкркскткцкчкшлблвлглдлелжлзлклллмлнлолплслтлулфлхлчлшлщлыльлюлямамбмвмгмкмлмммнмпмрмсмтмфмхмцмчмшмщмымьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаобоводожоиолооопотоуофохоцощоэоюояпапепипкпмпнпопппрпсптпупфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрырьрюрясасбсвсгсдсесжсзсислсмснсоспсрссстсусфсхсцсчсшсщсъсысьсэсюсяубувугудузумуоупуууфуцушуэцвцецицкцмцочачвчечкчлчмчнчочрчтчучшчььбьвьгьдьжьзькьмьньоьпьфьцьчьшьщюаюбювюгюдюеюжюзюйюкюмюнюпюсютюхюцючюшющюю',
            7  => 'аэбббвбгбдбжбзбкбмбнбсбтбхбцбчбшбщбъбьбюбявбвввгвдвжвзвмвпвсвтвхвцвчвшвщвъвьвюгбгвгггдгзгкгмгсгтгчгшгядбдгдддждздлдмдпдсдтдхдцдчдшдъдэдюеаегедежезеоепесеуехечещеэжбжвжгжджжжкжлжмжнжожпжржсжцжчжьжюзбзгздзезжзззизкзлзмзозсзтзузцзчзшзъзызьзэзюзяибизипифихищиэквкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщльмбмвмгмкмлмммрмсмтмфмхмцмчмшмщмьмэмюнбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщнэоаожоиоуофоцоэоюоярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюсасбсвсгсдсесжсзсислсмснсоспсрссстсусфсхсцсчсшсщсъсысьсэсюсятатбтвтгтдтетзтктлтмтнтптртстттутфтхтцтчтштщтътытэтютяувугужуоуууфуцушуэцвцецицкцмчачвчкчлчмчнчочрчтчшчьшашвшешкшлшмшншошпшршсштшушцшчшьшюыдыжыиылыпытыуышыяьвьгьдьжьзьиькьньоьпьсьфьцьчьшьщэвэгэдэзэйэкэлэмэпэрэсэтэфэхэяюаюбювюгюдюеюжюзюйюкюлюнюпюрюсютюхюцючюшющююягядяжязякяпярясяхяцяшяюяя',
            8  => 'аааеажаиайаоауахачащаэаюбвбгбжбзбмбтбхбцбчбщбъбявбвввгвдвжвзвмвпвтвщвъвюгбгвгггкгсгчгядбдгдддлдпдхдчдшдъдьдэеаебееежеиекеоепеуефечешещеэеюеяжбжвжгжжжлжмжпжржцжчжьжюзззтзцзьзэзюиаибивигидиеижизииийикилиниоипитиуифихицичишищиэиюияйвйгйдйейзйкйлймйойрйфйхйчйшкдкжкзлблглжлзлмлнлплтлфлхлчлшлщмрмтмхмшмщмьмэмюнлнрншнщнэоеожоиойоооуофоцочошощоюояпмпфпцпчпьргрзрфрхрцрщрьсбсгсжсзсрсфсщсъсэтбтгтдтзтптфтхтштщтътюуаубувуеужузуиуйуоуруууфухуцушущуэуюуяфлфнфчхгхдхкхмхтхшхэцвцмчвчлчмчрчшшвшпшршсштшцшчшющощьэвэгэдэзэйэкэлэмэнэпэрэсэфэхэяюаюбювюгюдюеюжюзюйюкюмюрюсюхюцючюшююябягядяияйякярясяцячяшящ',
            9  => 'вбвввгвдвевжвзвивквлвмвнвпврвсвтвувхвцвчвшвщвъвывьвювягбгвгггдгегзгигкглгмгнгогргсгтгугчгшгядбдвдгдддздкдлдмдндпдрдсдхдцдчдшдъдыдьдэдюеаебевегедееежезеиейекемеоепесетеуефехецечешещеэеюеязбзвзгздзжзззизкзлзмзнзозрзсзтзузцзчзшзъзызьзэзюзяквкдкжкзкккмкнкркткцкчкшлблвлглдлжлклллмлнлплслтлфлхлчлшлщльлюлямбмвмгмкмммнмпмрмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчнщньнэоаобоеожоиойоломооопосоуофохоцочошощоэоюояпапипкплпмпнпопппрпсптпупцпчпшпыпьпярарбрвргрдрержрзриркрлрмрнрпрррсртрурфрхрцрчршрщрырьрюрясвсгсдсесжсзслснспсрсссфсхсцсчсшсщсъсьсэсютбтвтгтдтзтктлтмтнтптттфтхтцтчтштщтътытьтэтютяфлфмфнфрфсфтфффчфыхвхгхдхехихкхлхмхнхохпхрхсхтхухшхэцвцицкцмчвчечкчлчмчнчочрчтчучшчьшвшкшлшмшншошпшршсштшцшчшьшю',
            10 => 'ааащаэвбвввгвдвжвзвквлвмвнвпврвсвтвхвцвчвшвщвъвьвювядадбдвдгдддедждздидкдлдмдндпдрдсдтдудхдцдчдшдъдыдьдэдюдяебегежеиеоеуефехецечешещеэеюеяжажбжвжгжджжжижкжлжмжнжожпжржсжужцжчжьжюзбзвзгздзжзззкзлзмзнзрзсзтзузцзчзшзъзызьзэзюзяигижицищиэиюквкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщмбмвмгмимкмлмммнмомпмрмсмтмумфмхмцмчмшмщмымьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэнюоаоцрбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчрщрьсбсдсжсзслсмсрсссхсчсщсъсьсэсютбтвтгтдтзтктлтмтптстттфтхтцтчтштщтътьтэтюуаугужуйуоуфуцуэцацвцкцмцоцуцычачвчичкчлчмчнчочрчтчучшчьшвшкшлшмшншошпшршсштшцшчшьшю',
            11 => 'аааоафаэбббвбгбдбжбзбкблбмбсбтбхбчбшбщбъбьбюбявбвввгвдвжвзвмвпврвтвувхвцвчвшвщвъвывьвювягбгвгггдгзгкглгмгнгсгтгчгшгядбдвдгдддждздкдлдмдпдрдтдхдцдчдшдъдьдэдяеажбжвжгжджжжкжлжмжожпжржсжужцжчжьжюзбзвзгздзжзззизмзозрзсзцзчзшзъзызьзэзюзяиуиэкдкжкзкккмкскткцкчкшлблвлглдлжлзлклллмлнлплтлфлхлчлшлщмбмвмгмкмлмммнмпмрмтмумфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнчншнщньнэнюоэоюпкплпмпнпппрпсптпфпцпчпшпьсбсвсгсдсжсзслсмснспсрсссфсхсцсчсшсщсъсысьсэсютбтвтгтдтзтитктлтмтнтптртстттфтхтцтчтштщтътьтэтютяущфифлфмфнфофрфсфтфуфффчфыхахгхдхехкхлхмхнхпхрхсхтхухшхэчвчлчмчрчтчшшашвшишкшлшмшншошпшршсштшушцшчшьшющнщощрщущьыгыдызыиырыуыцыяюаюгюеюйябяи',
            12 => 'ааабаоапауащбббвбгбдбжбзбкбмбнбсбтбхбцбчбшбщбъбьбюбявбвввгвевжвзвивквлвмвнвпврвсвтвувхвцвчвшвщвъвывьвювягагбгвгггдгегзгигкгмгогргсгтгугчгшгяеаепеуеэиаибижищиэквкдкжкзккклкмкркскткцкчкшлблвлглдлжлзлклллмлолплслтлулфлхлчлшлщлыльмбмвмгмкмлмпмрмсмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоапкпмпнпппспфпцпчпярбрвргрдржрзркрлрмрнрорпрррсртрфрхрцрчршрщрырьрюрясбсвсгсдсжсзслсмснспсрсссфсхсцсчсшсщсъсысэсютатбтвтгтдтзтктлтмтнтотптртстттутфтхтцтчтштщтътытьтэтютяубувуеуиуйуоуууцуэуяфлфмфнфрфсфтфффчфыхахвхгхдхехихкхлхмхнхпхрхсхтхухшхэцвцицкцмцочвчкчлчмчнчочрчтчшчьшашвшкшлшмшншошпшршсштшушцшчшьшющащнщощрщущьыбыдыжызыиыпырыуыцыщыяьбьвьгьжьзькьмьньоьпьфьцьчьшьщэвэгэдэзэйэкэлэмэпэсэтэфэхэяюаюбювюгюдюеюжюзюйюкюмюпюрюсютюхюцючюшющююябядяеяжязяйяпяряхяцяюяя',
            13 => 'ааафаэбббвбгбдбжбзбибкблбмбнбобрбсбтбхбцбчбшбщбъбыбьбюбявбвввгвдвжвзвквлвмвнвпврвсвтвхвцвчвшвщвъвывьвювягбгггзгкгмгнгчгшгядбдвдгдддздкдпдчдъдьдэдяещжбжвжгжджжжкжлжмжнжожпжржсжцжчжьжюзбзвзгздзжзззкзлзмзнзрзсзтзцзчзшзъзьзэзюзяиуиэкдкжкзкккмкскчкшлалблвлглдлелжлзлклллмлнлолплслтлулфлхлчлшлщлыльлюлянбнвнгнднжнзнкнлнннрнснфнхнцнчншнщньнэощрбрвргрдржрзркрлрмрнрпрррсртрурфрхрцрчршрщрырьрюрясгсдсжсзсрсхсчсшсщсъсьсэсюсятбтвтдтзтмтптттфтхтцтчтштщтътьтэубугузуиуоупуууфухуцуэфмфнфсфтфффчфыхахвхгхдхихкхлхмхнхпхсхтхухшхэцвцкцмчвчкчлчмчнчрчтчшчьшвшешкшлшмшншошпшршсшушцшчшьшющащещнщощрщущьыбыгыдыжызыиыкысыуыцыщыяьбьвьдьзьмьньпьфьцьчьщэвэгэдэзэйэкэлэмэнэрэсэтэфэхэяюбювюгюеюжюзюйюкюлюмюнюрюцябядяпяряц',
            14 => 'ааабавагадаеажаиайаламаоауафахацачашащаэаюаябцбювдвжвхвъвюгвгггзгкгсгчгшгядэдюеаебеееиереуефещеэеяжбжвжлжпжржсжчжюзсзтзцзчзшзъзэзюиаибиеижииийиоипиуифиэиюияйвйгйейзйпйрйфйхйшкдкжкмкцкчлфмвмгмхмшмщмэмюнхншнэоаовогоеожозоиойоломоуофошоэоюояпмпфпшсгсзсщсэтэтюуауеужуиуйуоуруууфуцушущуюуяфмфнфтфчхгхдхкхпхсхэцуцычмчрчшшршсшцшчщоэдэйэпэсэхэяюаюбювюгюжюйюкюлюмюнюпюрюхюцючюшягяеяияпяцячяшяюяя',
            15 => 'аааоаэеаебеиемеуефеэеяибиуифиэквкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщмамбмвмгмемкмлмммнмомпмрмсмтмумфмхмцмчмшмщмымьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэнюоапмпппрпсптпфпцпчпшпьпярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюсбсвсгсдсжсзслсмснспсрсссфсхсцсчсшсщсъсьсэсюсятбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътьтэтютяувуеужуиуйумуоуууфуцуэуяфефлфмфнфофсфтфуфффчфыцацвцецкцмцочвчкчлчмчнчочрчтчучшчьшашвшкшлшмшншошпшршсштшцшчшьшюыбыдызыиыкыныпысыуыцычыяьбьвьгьдьжьзьиькьмьньоьпьсьтьфьцьчьшьщябявягядяеяжязяияйякямяняпяряцячяшяюяя',
            16 => 'аэбббвбгбдбжбзбкбмбтбхбцбчбшбщбъбыбьбявбвввгвдвжвзвквлвмвпврвсвтвхвцвчвшвщвъгбгвгггдгзгкгмгтгчгядбдвдгдддздкдлдпдрдтдхдчдшдъдьдэеэжбжвжгжжжлжмжожпжржсжчжьжюзбзвзгздзжзззмзрзсзтзузцзчзшзъзызьзэзюзяиэкдкжкзкккмкркцкчкшлблвлглдлжлклллмлнлплслтлфлхлчлшлщлюмбмвмгмкмммнмпмрмсмтмфмхмцмшмщмьмэмюнвнгнднжнзнкнлнннрнтнфнхнцнчншнщньнэпкпмпппсптпфпцпчпшпьрбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрырьрюрясбсвсгсдсжсзслсмснсрсссфсхсцсчсшсщсъсьсэсютбтгтдтптттхтцтчтштщтътэтюувуоуууцуэфлфмфрфсфтфуфффчфыхвхгхлхрхтхшцвцкцмчвчкчмчнчочрчтчшчьшвшлшмшпшсштшцшчшьшющащнщощрщущьыиырыуыяьвьдьжьзьньоьпьфьцьчьшьщюаюбювюдюеюжюйюпюрюцююяияряц',
            17 => 'ааазаиаоащбббвбгбдбжбзбкбмбнбсбтбубхбцбчбшбщбъбьбявбвввгвдвжвзвквлвмвнвпвсвтвхвцвчвшвщвъвьвюгагбгвгггдгзгкгмгсгтгчгшгядбдгдддждздкдлдмдндпдсдтдхдцдчдшдъдыдьдэдюдяежефеэжбжвжгжджжжкжлжмжнжожпжржсжужцжчжьжюзбзвзгздзезжзззизкзлзмзнзозрзсзтзузцзчзшзъзызьзэзюзяиуищиэкдкжкзкккмкткцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщмбмвмгмкмлмммнмпмтмфмхмцмчмшмщмьмэмюнбнвнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаоэояпкпмпнпппсптпфпцпчпшпьрбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюрясбсгсдсжсзсссфсхсцсшсщсъсьсэсютдтзтптттфтхтштщтъуууцфлфмфнфрфсфтфуфффчфыхгхдхкхмхнхпхсхтхшхэцкцмцуцычвчкчмчнчочрчтчшчьшвшкшлшмшншпшршсшцшчшьшющащищнщощрщущьъюыбыдыжызыиыкыуыцычышыяьвьгьдьжьзьиьньоьпьсьфьцьчьшьщьяэвэгэдэзэйэлэпэсэтэфэхэяюаюбювюгюеюзюйюмюнюпютюхюцючющююябявяияйяляпяряшяюяя',
            18 => 'аааоаэбббвбгбдбебжбзбкбмбнбсбтбхбцбчбшбщбъбьбюбявбвввгвдвжвзвквмвнвпвсвтвхвцвчвшвщвъвьвюгбгвгггдгзгигкглгмгнгсгтгугчгшгядбдвдгдддждздкдлдмдндпдрдсдтдудхдцдчдшдъдьдэдюдяеуеэзбзгздзезжзззизкзлзмзнзрзсзтзузцзчзшзъзьзэзюзяквкдкжкзкккмкткцкчкшлблвлглдлжлклллмлнлплслтлфлхлчлшлщмбмвмгмкмлмммнмпмрмсмтмфмхмцмчмшмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэпкпмпнпппсптпфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпрсртрфрхрцрчршрщсбсгсдсжсзспсссфсхсцсшсщсъсьсэтбтвтгтдтзтктлтмтптстттфтхтцтчтштщтътытьтэтюувууфафлфмфнфрфсфтфффчфыхгхдхехкхмхнхпхрхсхтхшхэцвцицкцмчвчкчлчмчнчочрчтчшчьшкшмшншошпшршсшцшчшьшющнщощрщущьъюъяыбыжызыиытыуыцыяьвьгьжьзьньоьпьцьчьшьщэвэгэдэзэйэлэмэпэрэсэтэфэхэяюбювюгюдюеюжюзюйюнюпюсютюхюцючющююядяеяиялярячяю',
            19 => 'ааабавагаеажаиакамаоапауафахачашащаэаюаябвбгбдбзбмбтбхбшбщбювбвввгвдвжвзвмвпврвтвувхвцвчвщвъвюгбгвгдгзгкгмгсгтгчгшгядбдгдпдхдцдчдъдэеаебегееежеиекеленеоепеуефецечещеэеюеяжвжгжмжпжржцжюзбзгзжзззтзцзшзъзэзюзяиаибивигиеижииийимиоипиуифихичишищиэиюияйгйейзйлйойпйрйфйхйцйшкдкжкзкчкшлблглжлзлмлнлплтлхлчлшлщмвмгмпмтмхмшмщмэмюнбнвнжнзнлнрнфнхнчнщнэнюняоаовогоеожоиойоломонооопоуофохоцочошощоэоюояпмпсптпфпшрзрфрхрщсбсгсдсжсзсфсхсцсчсшсщсъсэсютгтдтзтмтптфтхтцтштщтътэуаубувугудуеужузуиуйукунуоупурусутуууфухуцучушущуэуюуяфафмфнфофрфсфтфчфыхдхрхэцацвцмцоцуцычвшвшмшпшршсштшцшющрщьэвэгэдэзэйэкэмэпэфэхэяюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюхюцючюшююябягядяеяжяияйякяняпярятяхяцячяшящяюяя',
            20 => 'ааадаеажаиаоапафацачащаэаюаяеаебегежезепеуефецечещеэеюивижиуифихищиэлблвлглдлжлзлклллмлнлслтлфлхлчлшлщлыльмбмвмгмимкмлмммнмпмрмсмтмфмхмцмчмшмщмьмэмюмянанбнвнгндненжнзнинкнлнннонрнснтнунфнхнцнчншнщньнэнюняоаоеожозоиооопоуофохоцочошощоэоюоярбрврдржрзркрлрмрнрпрррсртрфрхрцрчршрщрьрюрясасбсвсгсдсжсзсислсмснспсрсссусфсхсцсчсшсщсъсысьсэсюсятбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътэтюуаубувудуеужуиуйукулумуоупусуууфухучушущуэуяфлфмфнфрфсфтфффчфычачвчечкчлчмчнчочрчтчучшчьыбыгыдыеыжызыиыйыкылымыныпысытыуыхыцычышыщыя',
            21 => 'ааагазафацачашащвбвввгвдвевжвзвивквлвмвнвпврвсвтвувхвцвчвшвщвъвывьвювягбгвгггдгегзгигкглгмгнгргсгтгугчгшгядадбдвдгдддедждздидкдлдмдпдрдсдтдудхдцдчдшдъдыдьдэдюдяеаебегедееежезеиекелепесетеуефехецечещеэеюеяипиуифицишиэкаквкдкекжкзкиккклкмкнкскткукцкчкшлблвлглдлжлзлклллмлнлплслтлфлхлчлшлщльмбмвмгмкмлмнмпмрмсмтмфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщныньнэоиоооцошоэоюояпепипкплпмпнпппсптпупфпцпчпшпыпьпярбрвргрдржрзркрлрмрнрпрррсртрфрхрцрчршрщрырясасбсвсгсдсжсзслсмснспсрсссусфсхсцсчсшсщсъсысьсэсютбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътьтэтюубувугузуиуйумунуоусуууфухуцучущуэуяшашвшкшлшмшншпшршсштшушцшчшьшюэвэгэдэзэйэнэрэсэфэхэя',
            22 => 'ааабавагадажазаиайакаоасауафацачашащаэвавбвввгвдвжвзвквлвмвнвовпврвсвтвувхвцвчвшвщвъвывьвювяеаебежеиефецечешещеэеяибигижиуихицичишищиэквкдкжкзккклкмкнкркскткцкчкшмбмвмгмемимкмлмммнмомпмрмсмтмумфмхмцмчмшмщмымьмэмюмяоаободоеожозоиолонооопосотоуофохоцочошощоэоюояуаубувугудуеуиукулумуоупурусутуууфухуцучушущуэуяыбывыдыжызыиыкылынырысытыуыхыцычышыщыя',
            23 => 'ааабазаоапауафацачаэвбвввгвдвжвзвивквлвмвнвпврвсвтвхвцвчвшвщвъвьвювяеаежеоеуефецещеюибидижизиоипиуифиэквкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлулфлхлчлшлщлыльлюлямбмвмгмемимкмлмммнмпмрмсмтмумфмхмцмчмшмщмьмэмюмянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэнюоаобовогодоеожозоиолоооросотоуофохоцошощоэоюоярарбрвргрдржрзриркрлрмрнрорпрррсртрурфрхрцрчршрщрырьрюрятбтвтгтдтзтктлтмтнтптртстттфтхтцтчтштщтътьтэтюуауеузуиуйукуоуууфуцучуэшвшкшлшмшншошпшршсштшцшчшьшюьбьвьгьдьжьзькьмьньоьпьфьцьчьшьщ',
            24 => 'ааазаиаоауацаэвбвввгвдвжвзвквлвмвнвовпврвсвтвувхвцвчвшвщвъвьвювяеаежеоецещеэиаигидижизиииоиуицичиэкдкжкзккклкмкнкркскткцкчкшлблвлглдлжлзлклллмлнлплслтлулфлхлчлшлщльмбмвмгмемкмлмммнмомпмрмсмтмумфмхмцмчмшмщмьмэмюнбнвнгнднжнзнкнлнннрнснфнхнцнчншнщньнэоаодожозоиооохоцочошощоэоюояпепкплпмпнпппсптпупфпцпчпшпыпьпярарбрвргрдрержрзркрлрмрнрорпрррсртрфрхрцрчршрщрырьрюрясасбсвсгсдсесжсзсислсмснсоспсрссстсусфсхсцсчсшсщсъсысьсэсюсятбтвтгтдтзтктлтмтнтптстттфтхтцтчтштщтътьтэтютяувугудужузуиукунуоупуууфухуцуэуяцацвцицкцмцоцучвчечичкчлчмчнчочрчтчучшчььбьвьгьдьеьжьзькьмьньоьпьфьцьчьшьщюаюбювюгюдюеюжюзюйюкюлюмюнюпюсюхюцючюшющюю',
            25 => 'ааабажазаиаоапарауафацачашащаэеаезеуехецещеэеюеяиаибигидижизиииоирисиуифиэиюиянбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэнюняоаоводоеожозоиойоколонооопоросотоуофохоцочошощоэоюоярарбрвргрдржрзркрлрмрнрорпрррсртрурфрхрцрчршрщрырьрюуаубувугудуеужузуиуйукулумуоуууфухуцучушуэуяьбьвьгьдьеьжьзьиькьмьньоьпьсьтьфьцьчьшьщья',
            26 => 'еаебевегеееиейенеоепетеуефецечещеэеюеяюаюбювюгюдюеюжюзюйюкюлюмюпюрюсюхюцючюшющююябягядяеяжяияйякямяпяхяцячяшящяюяя',
            27 => 'бббвбгбдбжбзбмбсбтбхбцбчбшбщбъбюбявбвввгвдвжвзвлвмвпврвсвтвхвцвщвъвьвювягбгвгггдгегзгкгмгсгтгугчгшдбдгдддждздкдмдпдсдтдхдцдчдшдъдыдьдэдюдяеаебевегееежеиейекеленеоепересетеуефецечешещеэеюеяжбжвжлжмжпжржсжужцжчжьжюзбздзжзззизкзмзрзсзцзчзшзъзьзэзюзяиаибивидиеижизииийикилиминиоипиритиуифихицичишищиэиюияйвйгйейзйкйлймйнйойрйсйфйхйцйчйшкдкжкзкккмкскткцкчлблвлглжлзлллмлнлплтлфлхлчлшлщлюмвмгмлмммнмрмтмфмхмцмшмщмьмэмюнбнвнгнднжнзнлнрнтнфнхнцншнщнэпмпппсптпфпцпшпырбргрдржрзрлрмрпрррсртрфрхрцрчрщсбсгсдсжсзснсрсссфсцсщсъсэсютбтгтдтзтмтптттфтхтцтчтштщтътэтюуаубувугуеузуиуйукулумунуоупурусутуууфухуцушущуэуюуяхгхдхкхмхпхрхсхухшхэцицкцмцоцуцычвчмчрчтчшчьшмшпшршсштшчшющащнщощрщьябягядяеяжязяияйякялямяняпярятяхяцячяшящяюяя',
            28 => 'бббвбгбдбжбзбкблбмбнбрбсбтбхбцбчбшбщбъбьбюбявбвввгвдвжвзвквлвмвнвпврвсвтвувхвцвчвшвщвъвьвювягбгвгггдгзгкглгмгнгргсгтгчгшгядбдвдгдддждздидкдлдмдндпдрдтдудхдцдчдъдыдьдэдюдяеаебееежеиеленеоеуехецечещеэеюеяжбжвжгжджжжижкжлжмжнжожпжржсжужцжчжьжюзбзгздзжзззлзмзрзсзтзцзчзшзъзэзюиаибивигидиеижизииийикилиниоиписитиуифичищиэиюияквкдкжкзкккмкркткцкчкшмбмвмгмкмлмммнмпмрмсмтмфмхмцмчмшмщмьмэмюнбнвнгнднжнзнкнлнннрнснтнфнхнцнчншнщньнэоаобовогодоеожозоиойоколомооопосоуофохоцочошощоэоюояпкплпмпнпопппрпспупфпцпчпшпыпьпясвсгсдсесжсзслсмснспсрсссфсхсцсчсшсщсъсьсэсютбтвтгтдтзтктлтмтптстттфтхтцтчтштщтътьтэтютяфефлфмфнфсфтфффчцвцкцмчвчкчлчмчнчрчтчшчьшвшкшлшмшншпшршсштшцшчшьшющнщощрщьюаюбювюгюдюеюжюйюлюмюнюпюрюхюцючюшююябядяеяжязяияйяляпясятяцячяшящяя',
            29 => 'вбвввгвдвжвзвквлвнвпвсвтвувхвцвчвшвщвъвывьвювягагбгвгггдгзгигкглгмгнгргсгтгугчгшгядбдждздкдлдндодпдрдсдтдудхдцдчдшдъдыдьдэдюдязазбзвзгздзжзззкзлзмзнзрзсзтзузцзчзшзъзызьзэзюзяйвйгйдйейзйкймйойпйрйтйхйцйчйшкдкекжккклкмкнкткцкчкшлблвлглдлжлзлклнлолплслтлфлхлчлшлщлылюлямвмгмкмлмнмрмсмтмфмхмцмчмшмщмымьмэмюмянанбнжнзнкнлнрнунфнхнчншнщныньнэнюняпапепкплпмпнпрпсптпупфпцпчпшпыпьпярбргрдржркрмрпрррфрхрцрчршрщрьрюрясасбсвсгсдсесжсзсислсмснсоспсрсссусфсцсчсшсщсъсысьсэсюсятбтвтгтдтзтлтмтптстттфтхтцтчтштщтътьтэтютяфафлфмфнфофрфсфтфуфчфыхвхгхдхехихкхлхмхпхрхсхтхухшхэябявягядяеяжязяияйялямяняпярясятяхяцячяшящяюяя',
            30 => 'ааабавагадаеажазаиайакаламаоапасатауафахацачашащаэаюаябббгбдбжбзбмбрбсбтбхбцбчбшбщбъбьбювавбвввгвдвжвзвквлвмвнвпврвсвтвувхвцвчвшвщвъвьвювягбгвгггдгзгигкглгмгнгргсгтгугчгшгядбдвдгдддздкдлдмдпдрдтдхдчдшдъдыдэдюеаебевегедееежезеиейекеленеоепересеуефехецечешещеэеюеяжбжвжгжджжжкжлжмжожпжржсжужцжчжьжюзбзвздзззкзлзмзрзсзтзцзчзшзъзьзэзюзяйвйгйдйейзйкйлйнйойпйрйсйтйфйхйцйчйшкдкжкзккклкмкркткцкчкшлалблвлглдлжлзлклмлнлолплслтлулфлхлчлшлщлымбмвмгмлмммнмпмрмсмтмумфмхмцмчмшмщмьмэмюмянбнвнднжнзнлнннрнснтнфнчншнщнэнюпаплпмпнпопппрпсптпупфпцпчпшпыпьпярбрвргрдржрзрлрмрррсртрфрхрцрчршрщрюсбсвсгсдсжсзслсмснспсрсссусфсхсцсчсшсщсъсэтбтвтгтдтзтлтмтптртттфтхтцтчтштщтътьтэтютяхвхгхдхкхлхмхпхрхсхтхшхэцацвцецкцмцоцуцычвчлчмчрчтчшшвшешишлшмшошпшршсштшцшчшьшющнщощрщьюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюсюхюцючюшющюю',
            31 => 'бббвбгбдбебжбзбкбмбнбсбтбубхбцбчбшбщбъбыбюбявбвввгвдвжвзвмвпврвтвхвцвчвщвъвюгбгвгггдгзгмгргтгшгядбдвдгдддждздлдмдпдсдтдхдцдшдъдэеаебегееежезеиейеленеоепересеуефехецечещеэеюеяжвжгжджжжлжмжожпжржсжцжчжьжюзбздзжзззмзрзсзтзцзшзъзэзюиаибивигидиеижииийикилимиоипириситиуифихишищиэиюияйвйгйдйейзйлймйнйойпйрйфйхйчйшквкдкжкзкккмкркткцкчлблвлглдлелжлзлллмлнлплфлхлчлшлщлямвмгмммнмрмтмфмхмчмшмэмюмянбнжнзнлнрнтнфнхншнэплпмпппрпсптпфпцпшпьпярбрвргржрзрпррртрфрхрцршрщрьрюрясбсвсгсдсжсзсмспсрсссфсхсцсчсшсщсъсэсютбтгтдтзтлтмтптртттфтхтцтчтштщтътэтютяхвхгхдхехкхмхохрхсхухшхэцвцмцоцучвчлчмчрчтчшшашвшмшошпшршсштшушцшчшьшющощрщьюаюбювюгюдюеюжюзюйюкюлюмюнюпюрюхюцючюшююябявягядяеяжязяияйякяляняпярятяхяцячяшящяюяя',
        );
        $res=0;
        for($i=0;$i<strlen($s)-1;$i++)
        {
            $c1=$s[$i];
            if($c1<'а'||$c1>'я') continue;
            $c2=$s[$i+1];
            if($c2<'а'||$c2>'я') continue;
            $i1=ord($c1)-ord('а');
            if(strpos($a[$i1],$c2)!==false)
            {
                $res++;
                continue;
            }
            if($i>=strlen($s)-2) continue;
            $c3=$s[$i+2];
            if($c3<'а'||$c3>'я') continue;
            $i2=ord($c2)-ord('а');
            if(strpos($a[$i2],$c3)!==false)
            {
                $res++;
                $i++;
                continue;
            }
            $l=0;
            $r=strlen($b[$i1])/2-1;
            while($l<=$r)
            {
                $c=$l+(($r-$l)>>1);
                $ca=$b[$i1][$c*2];
                $cb=$b[$i1][$c*2+1];
                if($ca==$c2&&$cb==$c3)
                {
                    $res++;
                    break;
                }
                if($ca<$c2||$ca==$c2&&$cb<$c3)
                    $l=$c+1;
                else
                    $r=$c-1;
            }
        }
        return $res;
    }

    public function _charset_alt_win($s)
    {
        for($i=0;$i<strlen($s);$i++)
        {
            $c=ord($s[$i]);
            if($c>=0x80&&$c<=0x9f)
                $s[$i]=chr($c-0x80+0xc0);
            else if($c>=0xa0&&$c<=0xaf)
                $s[$i]=chr($c-0xa0+0xe0);
            else if($c>=0xc0&&$c<=0xdf)
                $s[$i]=chr($c-0xc0+0x80);
            else if($c>=0xf0&&$c<=0xff)
                $s[$i]=chr($c-0xf0+0xa0);
        }
        return $s;
    }

    public function _charset_koi_win($s)
    {
        $kw = array(
            //00   01   02   03   04   05   06   07   08   09   0a    0b   0c    0d   0e   0f
            0x80, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138,  139, 140,  141, 142, 143, //0x80 - 0x8f
            144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 0xbb, 156, 0xab, 158, 159, //0x90 - 0x9f
            160, 161, 162, 184, 164, 165, 166, 167, 168, 169, 170,  171, 172,  173, 174, 175, //0xa0 - 0xaf
            176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186,  187, 188,  189, 190, 191, //0xb0 - 0xbf
            254, 224, 225, 246, 228, 229, 244, 227, 245, 232, 233,  234, 235,  236, 237, 238, //0xc0 - 0xcf
            239, 255, 240, 241, 242, 243, 230, 226, 252, 251, 231,  248, 253,  249, 247, 250, //0xd0 - 0xdf
            222, 192, 193, 214, 196, 197, 212, 195, 213, 200, 201,  202, 203,  204, 205, 206, //0xe0 - 0xef
            207, 223, 208, 209, 210, 211, 198, 194, 220, 219, 199,  216, 221,  217, 215, 218  //0xf0 - 0xff
        );
        for($i=0;$i<strlen($s);$i++)
        {
            $c=ord($s[$i]);
            if($c>=128)
                $s[$i]=chr($kw[$c-128]);
        }
        return $s;
    }

    public function _charset_utf8_win($s)
    {
        $r='';
        $state=1;
        for ($i=0;$i<strlen($s);$i++)
        {
            $c=ord($s[$i]);
            switch($state)
            {
                case 1: //not a special symbol
                    if($c<=127)
                    {
                        $r.=$s[$i];
                    }
                    else
                    {
                        if(($c>>5)==6)
                        {
                            $c1=$c;
                            $state=2;
                        }
                        else
                            $r.=chr(128);
                    }
                    break;
                case 2: //an utf-8 encoded symbol has been meet
                    $new_c2=($c1&3)*64+($c&63);
                    $new_c1=($c1>>2)&5;
                    $new_i=$new_c1*256+$new_c2;
                    switch($new_i)
                    {
                        case   1025: $out_c='Ё'; break;
                        case   1105: $out_c='ё'; break;
                        case 0x00ab: $out_c='«'; break;
                        case 0x00bb: $out_c='»'; break;
                        default: $out_c=chr($new_i-848);
                    }
                    $r.=$out_c;
                    $state=1;
                    break;
            }
        }
        return $r;
    }

    public function _charset_prepare($s)
    {
        $r=0;
        $k=0;
        for($i=0;$i<strlen($s)&&$r<255;$i++)
        {
            $c=ord($s[$i]);
            if($c>=0x80)
            {
                $r++;
                $k=$i;
            }
        }
        return substr($s,0,$k+1);
    }

    public function charset_win_lowercase($s)
    {
        for($i=0;$i<strlen($s);$i++)
        {
            $c=ord($s[$i]);
            if($c>=0xc0&&$c<=0xdf)
                $s[$i]=chr($c+32);
            else if($s[$i]>='A'&&$s[$i]<='Z')
                $s[$i]=chr($c+32);
        }
        return $s;
    }

    public function charset_x_win($s)
    {
        // returns a string converted from a best encoding (windows-1251 or koi-8r) to windows-1251
        $sa=$this->_charset_prepare($s);
        $s1=$this->charset_win_lowercase($sa);
        $r1='windows-1251';

        $c1=$this->_charset_count_chars($s1);
        $b1=$this->_charset_count_bad($s1);
        $p1=$this->_charset_count_pairs($s1);
        $w1=$p1*32+$b1*64-$c1;
        $s2=$this->charset_win_lowercase($this->_charset_koi_win($sa));
        $w2=-$c1; //Особенность кодировки koi-8r: тот же диапазон символов, что и для windows-1251
        if($w2<$w1)
        {
            $b2=$this->_charset_count_bad($s2);
            $w2+=64*$b2;
            if($w2<$w1)
            {
                $p2=$this->_charset_count_pairs($s2);
                $w2+=32*$p2;
                if($w2<$w1)
                {
                    $r1='koi-8r';
                    $w1=$w2;
                }
            }
        }

        $s2=$this->charset_win_lowercase($this->_charset_utf8_win($sa));

        $c2=$this->_charset_count_chars($s2);
        $w2=-$c2;
        if($w2<$w1)
        {
            $b2=$this->_charset_count_bad($s2);
            $w2+=64*$b2;
            if($w2<$w1)
            {
                $p2=$this->_charset_count_pairs($s2);
                $w2+=32*$p2;
                if($w2<$w1)
                {
                    $r1='utf';
                    $w1=$w2;
                }
            }
        }

        switch($r1)
        {
            case 'alt':
                return $this->_charset_alt_win($s);
            case 'koi-8r':
                return $this->_charset_koi_win($s);
            case 'utf':
                return $this->_charset_utf8_win($s);
            default:
                return $s;
        }

        return $s;
    }

    //Перехват ошибок
    public function FatalErrorCatcher($forSchedule = ''){
        $error = error_get_last();
        if( $error !== NULL) {
            if ($forSchedule){
                $regedit = regedit::getInstance();
                $regedit->setVar("//modules/prices_import/cron_fatal_error", 1);
            }
            $errstr  = $error["message"];
            if (strpos($errstr,"Allowed memory size") !== false) {echo "memory_error"; die;}
        }
        return;
    }


    //Добалвение задачи "Отложенный импорт"
    public function schedule($file_name=''){
        $root = CURRENT_WORKING_DIR;
        if  (!file_exists($root . "/files/filesToImport/".$file_name.".set")
            or !file_exists($root . "/files/filesToImport/".$file_name.".sr")
            or !file_exists($root . "/files/filesToImport/".$file_name.".arr")
            ) return array("error"=>"file_not_found");

        $oC = umiObjectTypesCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();
        $uOC = umiObjectsCollection::getInstance();
        $ObjCatId = $oC->getBaseType('prices_import','schedule');
        $from = getRequest('from');
        $to = getRequest('to');

        $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
        $selected_rows = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));
        $fName = isset($get_settings['file_name']) ? $get_settings['file_name'] : "Новая задача";

        $objId = $uOC -> addObject($fName." (".date("d.m.Y G:i:s", time()).")", $ObjCatId);
        $getObject = $uOC -> getObject($objId);
        if (is_object($getObject)){
            $getObject -> setValue("id", $file_name);
            $getObject -> setValue("is_active", true);
            $getObject -> setValue("date", time());
            $getObject -> setValue("status", "В очереди");
            $getObject -> setValue("queue", 1);
            $getObject -> setValue("rows_total", count($selected_rows));
            $getObject -> setValue("rows_processed", 0);
            $getObject -> setValue("create", 0);
            $getObject -> setValue("update", 0);
            $getObject -> setValue("last_active", time());
            $getObject -> setValue("from", $from);
            $getObject -> setValue("to", $to);
            $getObject -> commit();

            return array("id"=>$objId);
        }

        return array("error"=>"undefined");
    }

    //Плановое выполнение импорта
    public function cron(){


        $root = CURRENT_WORKING_DIR;

        //Удаление старых файлов
        //Удаление старых файлов
        $dir = $root."/files/filesToImport/";
        if($handle = opendir($dir))
        {
            while(false !== ($file = readdir($handle)))
                if (time() > (filemtime($dir.$file) + 3600*24*7))
                    unlink($dir.$file);
            closedir($handle);
        }

        $margin = 30; //Запас по времени (сек) для завершения импорта. Также нужно учитывать время загрузки и обработки файла

        $this->cron = true;

        $regedit = regedit::getInstance();

        $max_sched_time = (int) $regedit->getVal("//modules/prices_import/max_sched_time");
        if (!$max_sched_time) $max_sched_time = 60;

        $cron_fatal_error = (int) $regedit->getVal("//modules/prices_import/cron_fatal_error");

        //$lastCronTime = (int) $regedit->getVal("//modules/prices_import/last_cron_time");
        $lastCronTime = time();

        $regedit->setVar("//modules/prices_import/last_cron_time", time());
        $regedit->setVar("//modules/prices_import/cron_fatal_error", 0);

        //Определение времени последней активности обработчика для исключения вероятности одновременного запуска двух cron
        $sel = new selector('objects');
        $sel->types('object-type')->name('prices_import', 'schedule');
        $sel->where('last_date')->eqmore(time()-$margin);        //Преверяется, была ли активность за последние $margin секунд
        if ($sel->length) {
            return;
        }

        //Определение текущей загрузки
        $getScheduleId = false;
        $sel = new selector('objects');
        $sel->types('object-type')->name('prices_import', 'schedule');
        $sel->order('id');
        $sel->where('is_active')->equals(true);
        if ($sel -> length){
            foreach($sel->result() as $obj){
                $rows_total = $obj -> getValue('rows_total');
                $rows_processed = $obj -> getValue('rows_processed');
                if (!$rows_total
                    or ($rows_total && !$rows_processed)
                    or ($rows_total && $rows_processed && ($rows_total != $rows_processed))) {
                    $getScheduleId = $obj -> getId();
                    break;
                }
                $getFile = $obj -> getValue('price_list');
                if (!$getFile) continue;

                //Проверка, изменился ли файл

            }
        } else return;
        //if ($getScheduleId === false) return;
        if ($getScheduleId === false) $getScheduleId = $sel -> first -> id;

        $uOC = umiObjectsCollection::getInstance();
        $getSchedule = $uOC -> getObject($getScheduleId);
        if (is_object($getSchedule)){

            $getSchedule -> setValue("last_date",time());
            $getSchedule -> commit();

            $getFile = $getSchedule -> getValue('price_list');
            if (!$getFile) return;

            //Проверка "загружен ли файл" или нужна его повторная загрузка
            $rows_total = $getSchedule -> getValue('rows_total');
            $rows_processed = $getSchedule -> getValue('rows_processed');
            $priceListId = $getSchedule -> getValue('price_list_id');

            if (!$priceListId or !file_exists($root . "/files/filesToImport/".$priceListId.".arr")) {
                $needUpload = true;
            } else {
                $needUpload = false;
                if (($rows_total && $rows_processed && ($rows_total == $rows_processed))) {
                    $needUpload = true;
                }
            }


            $file_name = $priceListId;

            //Если файл не загружен или был изменен -> повторная загрузка
            if ($needUpload){
                if (isset($_SESSION['summary_arr'])) unset($_SESSION['summary_arr']);
                //$file_name=time();
                $file_name=uniqid();
                $file_path = $root . "/files/filesToImport/".$file_name.".imp";


                $getFilePath = (strpos($getFile, "http://")!==false) ? $getFile : ($root."/".trim($getFile,"/"));
                if (!copy($getFilePath, $file_path)) {
                    $this->putReportSchedule($getSchedule,"<b>Ошибка:</b> ошибка чтения прайса. По указанному пути ".trim($getFilePath,$root)." файл не найден.","danger");
                    $getSchedule -> setValue("is_active", false);
                    $getSchedule -> commit();
                    $this->cron();
                    return;
                }

                $file_type = end(explode(".",basename($getFilePath)));
                $getSchedule -> setValue("price_list_id", $file_name);
                //$getSchedule -> setValue("file_date", $getModifyTime);
                $getSchedule -> setValue("rows_total",0);
                $getSchedule -> setValue("rows_processed",0);
                $getSchedule -> setValue("update",0);
                $getSchedule -> setValue("create",0);
                $getSchedule -> setValue("last_date",time());
                $getSchedule -> commit();

                $get_settings = array();
                $get_settings['file_type'] = $file_type;
                $get_settings['file_name'] = basename($getFilePath);
                if (isset($get_settings['report'])) unset($get_settings['report']);

                file_put_contents($root . "/files/filesToImport/".$file_name.".set", serialize($get_settings));
            }

            $this->putReportSchedule($getSchedule,"Чтение прайса","sched1");

            //Загрузка файла и парсинг
            $resultUpload = $this->get_uploaded_file($file_name,false,true,false,true);
            if ($resultUpload === false){
                $this->putReportSchedule($getSchedule,"<b>Ошибка:</b> ошибка чтения прайса. Дальнейшая загрузка остановлена.","sched2");
                $getSchedule -> setValue("is_active", false);
                $getSchedule -> commit();
                $this->cron();
                return;
            }

            //Применение шаблона
            if ($needUpload){
                $templateName = $getSchedule -> getValue('template');
                if (!$templateName){
                    $this->putReportSchedule($getSchedule,"<b>Ошибка:</b> не указан шаблон. Дальнейшая загрузка остановлена.","sched2");
                    $getSchedule -> setValue("is_active", false);
                    $getSchedule -> commit();
                    $this->cron();
                    return;
                }
                $resultApplyTemplate = $this->applyTemplate($file_name, true, $templateName);
                if (!$resultApplyTemplate){
                    $this->putReportSchedule($getSchedule,"<b>Ошибка:</b> несоответствие формата прайса выбранному шаблону загрузки. Дальнейшая загрузка остановлена.","sched2");
                    $getSchedule -> setValue("is_active", false);
                    $getSchedule -> commit();
                    $this->cron();
                    return;
                }
                //Повторная загрузка файла после применения шаблона (вносятся изменения в соотетствии с выбранным шаблоном (например, выделенные строки))
                $resultUpload = $this->get_uploaded_file($file_name,false,true,false,true);
                if ($resultUpload === false){
                    $this->putReportSchedule($getSchedule,"<b>Ошибка:</b> ошибка чтения прайса. Дальнейшая загрузка остановлена.","sched2");
                    $getSchedule -> setValue("is_active", false);
                    $getSchedule -> commit();
                    $this->cron();
                    return;
                }
            }

            $this->putReportSchedule($getSchedule,"Запущен процесс загрузки","sched1");

            if ((time() - $lastCronTime) > $max_sched_time) {
                $this->putReportSchedule($getSchedule,"<b>Предупреждение:</b> Обработка файла заняла ".(time() - $lastCronTime)." сек. Это больше, чем указано в параметре «Максимальное время выполнения скрипта для планировщика» (".$max_sched_time." сек.) в настройках модуля. Полная обработка файла осуществляется для нового файла, поэтому рекомендуем обратить внимание на отчет при следующей обработке cron. Возможно, потребуется увеличить значение параметра «Максимальное время выполнения скрипта для планировщика».","warning");
                return;
            };

            if ($needUpload){
                $this -> start_import($file_name);
            }

            //Импорт
            if (file_exists($root . "/files/filesToImport/".$file_name.".set")){
                $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
                $selected_rows = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".sr"));
                $processed = (int) (isset($get_settings['report']['processed']) ? $get_settings['report']['processed'] : 0);

                if ($processed != 0)
                    $this->putReportSchedule($getSchedule,"Процесс загрузки продолжен со строки №".$processed,"sched3");

                while($processed < count($selected_rows)){
                    $str = '';
                    $startRow = $processed;
                    $endRow = ($processed + 50) < count($selected_rows) ? ($processed + 50) : count($selected_rows);
                    for($index = $startRow; $index < $endRow; $index++) $str .= $index.",";
                    $_GET['id'] = trim($str,",");
                    $resultImport = $this -> import_($file_name, $getScheduleId);

                    if (!isset($resultImport['total'])){
                        $this->putReportSchedule($getSchedule,"<b>Ошибка:</b> ошибка при импорте прайса на участке между ".$startRow." и ".$endRow." строками. Дальнейшая загрузка остановлена.","danger");
                        $getSchedule -> setValue("is_active", false);
                        $getSchedule -> commit();
                        $this->cron();
                        return;
                    }
                    //Получение сведений
                    $getNewSettings = unserialize(file_get_contents($root . "/files/filesToImport/".$file_name.".set"));
                    $rows_total = (int) (isset($getNewSettings['report']['rows']) ? $getNewSettings['report']['rows'] : 0);
                    $processed = (int) (isset($getNewSettings['report']['processed']) ? $getNewSettings['report']['processed'] : 0);
                    $updated = (int) (isset($getNewSettings['report']['updated']) ? $getNewSettings['report']['updated'] : 0);
                    $created = (int) (isset($getNewSettings['report']['created']) ? $getNewSettings['report']['created'] : 0);
                    $last_activity = (int) (isset($getNewSettings['report']['last_activity']) ? $getNewSettings['report']['last_activity'] : 0);
                    $getSchedule -> setValue("rows_total",$rows_total);
                    $getSchedule -> setValue("rows_processed",$processed);
                    $getSchedule -> setValue("update",$updated);
                    $getSchedule -> setValue("create",$created);
                    $getSchedule -> setValue("last_date",$last_activity);
                    $getSchedule -> commit();
                    $this->putReportSchedule($getSchedule,"Обработано: ".$processed.", создано: ".$created.", обновлено: ".$updated.".","sched3");

                    if ((time() - $lastCronTime) > $max_sched_time) break;
                }

                if ($processed == count($selected_rows)){
                    $updated = (int) (isset($getNewSettings['report']['updated']) ? $getNewSettings['report']['updated'] : 0);
                    $created = (int) (isset($getNewSettings['report']['created']) ? $getNewSettings['report']['created'] : 0);
                    $this->putReportSchedule($getSchedule,"Процесс загрузки завершен. Всего обработано: ".count($selected_rows).", создано: ".$created.", обновлено: ".$updated.".","sched4");
                    $this -> end_import($file_name);
                }
            }

            $getSchedule -> commit();
        }
        return;
    }

    //Запсиь строки отчета отложенной загрузки
    public function putReportSchedule($getSchedule = "", $text = "", $type = "info"){    //success, info, warning, danger
        $report = $getSchedule -> getValue("report");
        $old_mode = umiObjectProperty::$IGNORE_FILTER_INPUT_STRING;		//Откючение html сущн.
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;

        $report = $report ? $report : "";
        $report = "<div class='".$type."'>[".date("d.m.Y, H:i:s",time())."] ".$text."</div>".$report;
        $getSchedule -> setValue("report",$report);
        $getSchedule -> commit();
    }

    //Удаление отчета отложенной загрузки
    public function delReportSchedule($objId = false){
        $uOC = umiObjectsCollection::getInstance();
        $getObject = $uOC -> getObject($objId);
        if (is_object($getObject)){
            $getObject -> setValue("report","");
            $getObject -> commit();
        }
        return;
    }

    //Информация о плановом задании
    public function getSchedule($id = false){
        if (!is_numeric($id)) return;
        $uOC = umiObjectsCollection::getInstance();
        $root = CURRENT_WORKING_DIR;
        $getObj = $uOC -> getObject($id);
        if (is_object($getObj)){
            $id = $getObj -> getValue('id');

            $get_settings = array();
            if (file_exists($root . "/files/filesToImport/".$id.".set")){
                $get_settings = unserialize(file_get_contents($root . "/files/filesToImport/".$id.".set"));
            }

            $fileName = isset($get_settings['file_name']) ? ($get_settings['file_name'] ? $get_settings['file_name'] : "") : "";
            $is_active = $getObj -> getValue('is_active');

            $date = $getObj -> getValue('date');
            $status = $getObj -> getValue('status');
            $rows_total = $getObj -> getValue('rows_total');
            $rows_processed = $getObj -> getValue('rows_processed');
            $create = $getObj -> getValue('create');
            $update = $getObj -> getValue('update');
            $last_active = $getObj -> getValue('last_active');
            $from = $getObj -> getValue('from');
            $to = $getObj -> getValue('to');

            return array(   "file_name"=>$fileName,
                            "is_active"=>$is_active,
                            "date"=>$date,
                            "status"=>$status,
                            "rows_total"=>$rows_total,
                            "rows_processed"=>$rows_processed,
                            "create"=>$create,
                            "update"=>$update,
                            "last_active"=>$last_active,
                            "from"=>$from,
                            "to"=>$to
                        );
        }
        return;
    }

    //Получить id категории для экспорта
    public function getExportParentId(){
        if (isset($_SESSION['export_parent_id']))
            if (is_numeric($_SESSION['export_parent_id']))
                return $this->get_category_param($_SESSION['export_parent_id']);
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'category');
        if ($pages->length) {
            $id = $pages->first->id;
            return $this->get_category_param($id);
        }
        return;
    }

    //Экспорт данных
    public function export_(){
        $data = getRequest('data');
        if (!isset($data['parent-id']) or !isset($data['type']) or !isset($data['template'])) return;
        if (!$data['parent-id'] or !$data['type'] or !$data['template']) return;

        $root = CURRENT_WORKING_DIR;
        $templates_arr = array();
        if (file_exists($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr")){
            $templates_arr = unserialize(file_get_contents($root . "/styles/skins/modern/data/modules/prices_import/system/templates.arr"));
        }
        if (!isset($templates_arr[$data['template']]['id'])) return;

        $id = $templates_arr[$data['template']]['id'];

        foreach($id as $i=>$item) if (!$id[$i]) unset($id[$i]);

        $arr = array();
        $regedit = regedit::getInstance();
        $uOC = umiObjectsCollection::getInstance();
        $symb_separate_type_id = $regedit->getVal("//modules/prices_import/symb_separate_type_id");
        $symb_separate_csv = $regedit->getVal("//modules/prices_import/symb_separate_csv");

        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');

        foreach($data['parent-id'] as $parent){
            $pages->where('hierarchy')->page($parent)->childs(10);
        }
        if (!isset($data['unactive']))
            $pages->where('is_active')->equals(array(0,1));

        $pages->order('id');

        $fields_id=$this->get_fields_type(false,true);

        $exist = array();
        if ($pages->length){
            $arr[] = $id;
            foreach($pages->result() as $element){
                //Если нужно исключить виртуальные копии
                $firstId = $this->first_id($element->getId());
                if (isset($data['vcopies'])){
                    if (isset($exist[$firstId])) continue;
                }
                $exist[$firstId] = '';

                $row = array();
                foreach($id as $index=>$field) {
                    $getTypeField = $this -> array_search_($field, $fields_id, "name", "field_type", true);

                    switch($field){
                        case 'id':
                            $row[$index] = $element -> getId();
                            break;
                        case 'name':
                            $row[$index] = $element -> getName();
                            break;
                        case 'type-id':
                            $row[$index] = $element -> getObjectTypeId();
                            break;
                        case 'parent-id':
                            $row[$index] = $element -> getParentId();
                            break;
                        case 'is-active':
                            $row[$index] = $element -> getIsActive();
                            break;
                        default:
                            $value = ($element -> getValue($field)) ? $element -> getValue($field) : "";
                            if ($getTypeField == "boolean"){
                                $value = $value ? "1" : "0";
                            } else {
                                if (is_array($value)) {
                                    foreach($value as $i=>$item){
                                        if (is_numeric($item)) {
                                            $getObj = $uOC -> getObject($item);
                                            if ($getObj instanceof umiObject)
                                                $value[$i] = $getObj -> getName();
                                        }
                                    }
                                    $value = implode($symb_separate_type_id, $value);
                                }
                            }

                            $row[$index] = $value;
                            break;
                    }
                }
                $arr[] = $row;
            }

            $fileName = $data['template']."_".date("d-m-Y_H-i-s",time()).".".$data['type'];

            $typePHPExcel = "Excel5";
            if ($data['type'] == "xls") $typePHPExcel = "Excel5";
            if ($data['type'] == "xlsx") $typePHPExcel = "Excel2007";
            if ($data['type'] == "csv") $typePHPExcel = "CSV";


            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);

            $active_sheet = $objPHPExcel->getActiveSheet();

            //Настройки листа
            $active_sheet->getPageSetup()
                ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
            $active_sheet->getPageSetup()
                ->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            //Поля документа
            $active_sheet->getPageMargins()->setTop(1);
            $active_sheet->getPageMargins()->setRight(0.75);
            $active_sheet->getPageMargins()->setLeft(0.75);
            $active_sheet->getPageMargins()->setBottom(1);
            //Название листа
            $active_sheet->setTitle("Прайс-лист");
            //Настройки шрифта
            $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
            $objPHPExcel->getDefaultStyle()->getFont()->setSize(8);



            //Формирование страницы
            foreach($arr as $y=>$row)
                foreach($row as $x=>$col){
                    $value = $col;
                    if ($typePHPExcel == "CSV") $value = iconv("utf-8", "windows-1251", $col);
                    $active_sheet->setCellValueExplicitByColumnAndRow($x, $y+1, $value, PHPExcel_Cell_DataType::TYPE_STRING);
                }

            if ($typePHPExcel != "CSV") $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $typePHPExcel);
            else $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $typePHPExcel)->setDelimiter(chr($symb_separate_csv));

            $objWriter->save($root."/files/filesToImport/".$fileName);

            return array("file_name"=>"/files/filesToImport/".$fileName);
        }
        return;
    }

    public function first_id($id) {
        $page_id = (int) $id;
        $h = umiHierarchy::getInstance();
        //экземпляр страницы
        $element = $h->getElement($page_id);
        if($element){
            // object_id источника данных
            $object_id = $element->getObjectId();
            // список всех страниц, которые используют данный объект
            $arr_id = $h->getObjectInstances($object_id);
            // первая страница, это страница с наименьшим page_id, т.е. исходная страница
            return $arr_id[0];
        }else  return $page_id;
    }

    public function getRegEdit(){
        $regedit = regedit::getInstance();
        $def_parentId = (int) $regedit->getVal("//modules/prices_import/def_parentId");
        $def_typeId = (int) $regedit->getVal("//modules/prices_import/def_typeId");
        $def_isActive = (boolean) $regedit->getVal("//modules/prices_import/def_isActive");
        $encoding = (string) $regedit->getVal("//modules/prices_import/encoding");
        $symb_separate_type_id = (string) $regedit->getVal("//modules/prices_import/symb_separate_type_id");
        $symb_separate_csv = (string) $regedit->getVal("//modules/prices_import/symb_separate_csv");
        $memory_limit = (string) $regedit->getVal("//modules/prices_import/memory_limit");
        $stores_field = (string) $regedit->getVal("//modules/prices_import/stores_field");
        return array(
            "def_parentId" => $def_parentId,
            "def_typeId" => $def_typeId,
            "def_isActive" => $def_isActive,
            "encoding" => $encoding,
            "symb_separate_type_id" => $symb_separate_type_id,
            "symb_separate_csv" => $symb_separate_csv,
            "memory_limit" => $memory_limit,
            "stores_field" => $stores_field
        );

    }

    //Сохранение лога
    public function log($file_name='', $type_log='error_1'){
        $root = CURRENT_WORKING_DIR;
        $id = getRequest('id');
        $result = array(date("Y.m.d H:i:s", time()),$type_log,$id);
        file_put_contents($root . "/files/filesToImport/".$file_name.".log", print_r($result, true), FILE_APPEND);
        return;
    }

    public function delObjects(){
        $pages = new selector('pages');
        $pages->types('hierarchy-type')->name('catalog', 'object');
        foreach($pages->result() as $element)
        umiHierarchy::getInstance() -> delElement($element -> getId());
        return;
    }
}