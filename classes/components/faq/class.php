<?php
	/**
	 * Базовый класс модуля "FAQ".
	 *
	 * Модуль управляет следующими сущностями:
	 *
	 * 1) Проекты;
	 * 2) Категории;
	 * 3) Вопросы;
	 *
	 * Модуль позволяет создавать вопросы с клиентской части.
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_faq/
	 */
	class faq extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();
			$this->per_page = (int) regedit::getInstance()->getVal("//modules/faq/per_page");

			if (cmsController::getInstance()->getCurrentMode() == "admin") {
				$this->__loadLib("admin.php");
				$this->__implement("FaqAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("FAQCustomAdmin", true);
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("FAQMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("FAQCustomMacros");
			}

			$this->__loadLib("handlers.php");
			$this->__implement("FAQHandlers");

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("FAQCustomCommon", true);
		}

		/**
		 * Уведомляет автора о изменении вопроса
		 * @param umiHierarchyElement $oElement страница вопроса
		 * @return bool
		 */
		public function confirmUserAnswer($oElement) {
			$umiRegistry = regedit::getInstance();
			$bConfirmUserAnswer = (bool) $umiRegistry->getVal("//modules/faq/confirm_user_answer");

			if (!$bConfirmUserAnswer) {
				return true;
			}

			if (!$oElement instanceof umiHierarchyElement || !$oElement->getIsActive()) {
				return false;
			}

			$iAuthorId = $oElement->getValue("author_id");
			$umiObjectsCollection = umiObjectsCollection::getInstance();
			$oAuthor = $umiObjectsCollection->getObject($iAuthorId);

			if (!$oAuthor instanceof umiObject) {
				return false;
			}

			$author_user = null;

			if ($oAuthor->getValue("is_registrated")) {
				$user_id = $oAuthor->getValue("user_id");
				$author_user = $umiObjectsCollection->getObject($user_id);
			}

			if ($author_user instanceof umiObject) {
				$author_name = $author_user->getValue("lname") . " " . $author_user->getValue("fname");
				$author_email = $author_user->getValue("e-mail");
			} else {
				$author_name = $oAuthor->getValue("nickname");
				$author_email = $oAuthor->getValue("email");
			}

			if (!umiMail::checkEmail($author_email)) {
				return false;
			}

			list($sMailSubject, $sMailBody) = self::loadTemplatesForMail(
				"faq/default",
				"answer_mail_subj",
				"answer_mail"
			);

			$block_arr = Array();
			$block_arr['domain'] = $sDomain = $_SERVER['HTTP_HOST'];
			$block_arr['element_id'] = $iElementId = $oElement->getId();
			$block_arr['author_id'] = $oElement->getValue("author_id");

			$umiHierarchy = umiHierarchy::getInstance();
			$bOldFHStatus = $umiHierarchy->forceAbsolutePath(true);
			$block_arr['question_link'] = $umiHierarchy->getPathById($iElementId);
			$umiHierarchy->forceAbsolutePath($bOldFHStatus);

			$block_arr['ticket'] = $iElementId;

			$sSubject = self::parseTemplateForMail($sMailSubject, $block_arr, $iElementId);
			$sBody = self::parseTemplateForMail($sMailBody, $block_arr, $iElementId);

			$from = $umiRegistry->getVal("//settings/fio_from");
			$from_email = $umiRegistry->getVal("//settings/email_from");

			$oMail = new umiMail();
			$oMail->addRecipient($author_email, $author_name);
			$oMail->setFrom($from_email, $from);
			$oMail->setSubject($sSubject);
			$oMail->setContent($sBody);
			$oMail->commit();

			return true;
		}

		/**
		 * Возвращает ссылки на страницу редактирование сущности и
		 * страницу добавления дочерней сущности
		 * @param int|bool $element_id идентификатор сущности
		 * @param string|bool $element_type тип сущности
		 * @return array|bool
		 */
		public function getEditLink($element_id, $element_type) {
			$prefix = $this->pre_lang;

			switch ($element_type) {
				case "project": {
					$link_add = $prefix . "/admin/faq/add/{$element_id}/category/";
					$link_edit = $prefix . "/admin/faq/edit/{$element_id}/";
					return [$link_add, $link_edit];
				}
				case "category": {
					$link_add = $prefix . "/admin/faq/add/{$element_id}/question/";
					$link_edit = $prefix . "/admin/faq/edit/{$element_id}/";
					return [$link_add, $link_edit];
				}
				case "question": {
					$link_edit = $prefix . "/admin/faq/edit/{$element_id}/";
					return [false, $link_edit];
				}
				default: {
					return false;
				}
			}
		}
	}
?>
