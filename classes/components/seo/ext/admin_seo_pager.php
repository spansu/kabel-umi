<?php

class admin_seo_pager extends def_module {

    public $module;
    
    public function __construct($module) {
        parent::__construct();
        
        $commonTabs = $module->getCommonTabs();
        if (is_object($commonTabs)) {
            $commonTabs->add('seo_pager');
        }
    }

    public function seo_pager() {
        require_once dirname(__FILE__) . '/install_helper.php';
        registrateModule('seo', 'seo_pager');

        $regedit = regedit::getInstance();
        $prefix = 'seo_pager_';

        $params = array(
            'globals' => array(
                "boolean:{$prefix}is_enabled" => intval($regedit->getVal("//modules/seo/{$prefix}is_enabled")),
            )
        );
                
        $mode = (string) getRequest('param0');
        if ($mode == 'do') {
            $params = $this->module->expectParams($params);
            $regedit->setVal("//modules/seo/{$prefix}is_enabled", $params['globals']["boolean:{$prefix}is_enabled"]);
            $this->module->chooseRedirect();
        }

        $data = $this->module->prepareData($params, 'settings');
        $this->module->setDataType('settings');
        $this->module->setActionType('modify');
        $this->module->setData($data);
        $this->module->doData();
    }
}
