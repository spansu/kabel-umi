<?php

$i18n = array(
	'header-seo-filter_links' => 'SEO-ссылки для фильтров',
	'header-seo-filter-link-create' => 'Создание ссылки для фильтра',
    'header-seo-filter-link-modify' => 'Редактирование ссылки для фильтра',
	'perms-seo-client' => 'Клиентские разрешения',
	'js-del-shured' => 'Вы уверены, что хотите удалить объект? Он будет удален окончательно.',
);