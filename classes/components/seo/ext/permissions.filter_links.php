<?php
	$permissions = array(
		'seo' => array('fl_toggleActivity', 'filter_links', 'filter_link_edit', 'fl_validateUrl', 'fl_validateCategory', 'fl_validateFilters', 'fl_updateSitemap', 'filter_links_delete', 'dataset_config', 'getDatasetConfiguration'),
		'client' => array('fl_doWork','fl_BufferSend','getSeoFiltersData')
	);
?>