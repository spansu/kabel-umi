<?php
	/**
	 * Группы прав на функционал модуля
	 */
	$permissions = [
		/**
		 * Права на администрирование модуля
		 */
		'seo' => [
			'seo',
			'links',
			'webmaster',
			'add_site',
			'verify_site',
			'refresh_site',
			'handle_url',
			'config',
			'megaindex',
			'yandex'
		]
	];
?>