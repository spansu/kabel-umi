<?php

	/**
	 * Базовый класс модуля "Рассылки".
	 * Модуль управляет следующими сущностями:
	 * 1) Рассылки;
	 * 2) Выпуски;
	 * 3) Сообщения;
	 * 4) Подписчики;
	 * Модуль умеет отправлять рассылки, как в вручную (через админинстративную панель),
	 * так и автоматически по срабатыванию системного крона.
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_rassylki/
	 */
	class dispatches extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();

			$umiRegistry = regedit::getInstance();
			$this->per_page = (int) $umiRegistry->getVal("//modules/dispatches/per_page");

			if (!$this->per_page) {
				$this->per_page = 15;
			}

			if (cmsController::getInstance()->getCurrentMode() == "admin") {
				$commonTabs = $this->getCommonTabs();

				if ($commonTabs) {
					$commonTabs->add('lists');
					$commonTabs->add('subscribers');
					$commonTabs->add('messages', ['releases']);
				}

				$this->__loadLib("admin.php");
				$this->__implement("DispatchesAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("DispatchesCustomAdmin", true);
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("DispatchesMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("DispatchesCustomMacros", true);
			}

			$this->__loadLib("handlers.php");
			$this->__implement("DispatchesHandlers");

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("DispatchesCustomCommon", true);
		}

		/**
		 * Возвращает ссылку на страницу редактирования рассылки
		 * @param int $objectId идентификатор рассылки
		 * @param bool $type контрольный параметр
		 * @return string
		 */
		public function getObjectEditLink($objectId, $type = false) {
			return $this->pre_lang . "/admin/dispatches/edit/" . $objectId . "/";
		}

		/**
		 * Возвращает является ли объект - подписчиком
		 * @param mixed $object проверяемый объект
		 * @return bool
		 */
		public function isSubscriber($object) {
			return ($object instanceof iUmiObject && $object->getTypeGUID() == 'dispatches-subscriber');
		}

		/**
		 * Возвращает является ли объект - рассылкой
		 * @param mixed $object проверяемый объект
		 * @return bool
		 */
		public function isDispatch($object) {
			return ($object instanceof iUmiObject && $object->getTypeGUID() == 'dispatches-dispatch');
		}

		/**
		 * Возвращает подписчика по идентификатору пользователя
		 * @param int $userId идентификатор пользователя
		 * @return umiObject|null
		 * @throws coreException
		 */
		public function getSubscriberByUserId($userId) {
			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'subscriber');
			$sel->where('uid')->equals($userId);
			$sel->limit(0, 1);
			return $sel->first;
		}

		/**
		 * Возвращает подписчика по почтовому ящику
		 * @param string $email почтовый ящик
		 * @return umiObject|null
		 * @throws coreException
		 */
		public function getSubscriberByMail($email) {
			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'subscriber');
			$sel->where('name')->equals($email);
			$sel->limit(0, 1);
			return $sel->first;
		}

		/**
		 * Возвращает список рассылок
		 * @return umiObject[]
		 * @throws selectorException
		 */
		public function getAllDispatches() {
			static $cache = null;

			if (!is_null($cache)) {
				return $cache;
			}

			$dispatches = new selector('objects');
			$dispatches->types('object-type')->name('dispatches', 'dispatch');
			$dispatches->where('is_active')->equals(true);
			$dispatches->option('no-length')->value(true);
			$dispatches->option('load-all-props')->value(true);
			return $cache = $dispatches->result();
		}

		/**
		 * Изменяет рассылку, в которую будут выгружаться новые темы форума,
		 * @param iUmiObject $dispatch новая рассылка
		 * @throws selectorException
		 */
		public function changeLoadFromForumDispatch($dispatch) {
			$umiRegistry = regedit::getInstance();

			/** @var iUmiObject|iUmiEntinty $dispatch */
			$dispatches = new selector('objects');
			$dispatches->types('object-type')->name('dispatches', 'dispatch');
			$dispatches->where('load_from_forum')->equals(true);

			/** @var iUmiObject|iUmiEntinty $object */
			foreach ($dispatches as $object) {
				$object->setValue('load_from_forum', false);
				$object->commit();
			}

			$umiRegistry->setVal('//modules/forum/dispatch_id', $dispatch->getId());
			$dispatch->setValue('load_from_forum', true);
			$dispatch->commit();
		}

		/**
		 * Возвращает сообщения рассылки
		 * @param bool|int $releaseId идентификатор выпуска рассылки
		 * @return umiObject[]
		 * @throws selectorException
		 */
		public function getReleaseMessages($releaseId = false) {
			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'message');

			if ($releaseId) {
				$sel->where('release_reference')->equals($releaseId);
			}

			selectorHelper::detectFilters($sel);
			return $sel->result();
		}

		/**
		 * Возвращает идентификатор выпуска указанной рассылки
		 * @param bool|int $dispatchId идентификатор рассылки
		 * @return bool|int
		 * @throws coreException
		 */
		public function getNewReleaseInstanceId($dispatchId = false) {
			$umiObjects = umiObjectsCollection::getInstance();
			static $releases = [];

			if (!$dispatchId) {
				$dispatchId = getRequest("param0");
			}

			if (isset($releases[$dispatchId])) {
				return $releases[$dispatchId];
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'release');
			$sel->where('status')->isnull();
			$sel->where('disp_reference')->equals($dispatchId);
			$sel->option('return')->value('id');
			$sel->limit(0, 1);
			$result = $sel->result();

			$releaseId = null;
			$isNewRelease = false;

			if ($result) {
				foreach ($result as $info) {
					$releaseId = $info['id'];
				}
			} else {
				$type = selector::get('object-type')->name('dispatches', 'release');
				$releaseId = $umiObjects->addObject("", $type->getId());
				$isNewRelease = true;
			}

			$release = $umiObjects->getObject($releaseId);

			if ($release instanceof umiObject) {
				if ($isNewRelease) {
					$release->setName('-');
					$release->setValue('status', false);
					$release->setValue('disp_reference', $dispatchId);
					$release->commit();
				}

				$releases[$dispatchId] = $release->getId();
			}

			return $releaseId;
		}

		/**
		 * Заполняет выпуск рассылки сообщениями
		 * @param bool|int $dispatchId идентификатор рассылки
		 * @param bool $ignoreRedirect производить редирект
		 * на страницу с формой редактирования после завершения работы
		 * @throws coreException
		 */
		public function fill_release($dispatchId = false, $ignoreRedirect = false) {
			$umiObjects = umiObjectsCollection::getInstance();
			$umiHierarchy = umiHierarchy::getInstance();

			$dispatchId = ($dispatchId) ? $dispatchId : getRequest('param0');
			$dispatch = $umiObjects->getObject($dispatchId);

			if (!$dispatch instanceof umiObject) {
				$this->getFillingResult($ignoreRedirect, $dispatchId);
				return;
			}

			$releaseId = $this->getNewReleaseInstanceId($dispatchId);
			$newsRelation = $dispatch->getValue("news_relation");
			$newsLents = $umiHierarchy->getObjectInstances($newsRelation, false, true);

			if (count($newsLents) === 0) {
				$this->getFillingResult($ignoreRedirect, $dispatchId);
				return;
			}

			$sel = new selector('objects');
			$sel->types('object-type')->name('dispatches', 'release');
			$sel->where('disp_reference')->equals($dispatchId);
			$sel->option('return')->value('id');
			$result = $sel->result();

			$releaseIds = array_map(function ($info) {
				return (int) $info['id'];
			}, $result);

			$elementId = (int) $newsLents[0];

			$sel = new selector('pages');
			$sel->types('hierarchy-type')->name('news', 'item');
			$sel->order('publish_time')->desc();
			$sel->where('hierarchy')->page($elementId);
			$sel->where('lang')->equals(false);
			$sel->limit(0, 50);
			$result = $sel->result();

			/** @var umiHierarchyElement $newsItem */
			foreach ($result as $newsItem) {
				if (!$newsItem instanceof umiHierarchyElement) {
					continue;
				}

				$newsItemId = $newsItem->getId();

				$name = $newsItem->getName();
				$header = $newsItem->getValue('h1');
				$shortBody = $newsItem->getValue('anons');
				$body = $newsItem->getValue('content');
				$publishTime = $newsItem->getValue('publish_time');

				if (!strlen($body)) {
					$body = $shortBody;
				}

				$sel = new selector('objects');
				$sel->types('object-type')->name('dispatches', 'message');
				$sel->where('new_relation')->equals($newsItemId);
				$sel->where('release_reference')->equals($releaseIds);
				$sel->limit(0, 1);
				$result = $sel->result();

				if (count($result) > 0) {
					continue;
				}

				$messageTypeId = selector::get('object-type')->name('dispatches', 'message');
				$messageId = $umiObjects->addObject($name, $messageTypeId);
				$message = $umiObjects->getObject($messageId);

				if (!$message instanceof umiObject) {
					continue;
				}

				$message->setValue('release_reference', $releaseId);
				$message->setValue('header', $header);
				$message->setValue('body', $body);
				$message->setValue('short_body', $shortBody);
				$message->setValue('new_relation', [$newsItemId]);

				if ($publishTime instanceof umiDate) {
					$message->setValue('msg_date', $publishTime);
				}

				$message->commit();
				$umiObjects->unloadObject($message->getId());
			}

			$this->getFillingResult($ignoreRedirect, $dispatchId);
		}

		/**
		 * Отправляет выпуск рассылки всем подписчикам
		 * @param int $dispatchId идентификатор рассылки
		 * @return bool
		 * @throws selectorException
		 */
		public function release_send_full($dispatchId) {
			$umiObjects = umiObjectsCollection::getInstance();
			$umiRegistry = regedit::getInstance();

			$releaseId = $this->getNewReleaseInstanceId($dispatchId);
			$dispatch = $umiObjects->getObject($dispatchId);
			$release = $umiObjects->getObject($releaseId);

			if (!$dispatch instanceof umiObject || !$release instanceof umiObject) {
				return false;
			}

			if ($release->getValue('status')) {
				return false;
			}

			$mailer = new umiMail();

			$mailBlocks = [];
			$mailBlocks['header'] = $dispatch->getName();
			$mailBlocks['messages'] = "";

			list($releaseBody, $releaseMessage) = dispatches::loadTemplatesForMail(
					"dispatches/release",
					"release_body",
					"release_message"
			);

			$sel = new selector('objects');
			$sel->types("hierarchy-type")->name("dispatches", "message");
			$sel->where("release_reference")->equals($releaseId);
			$messages = $sel->result();

			if (!$sel->length()) {
				return false;
			}

			foreach ($messages as $message) {
				if (!$message instanceof umiObject) {
					continue;
				}

				$messageBlocks = [];
				$messageBlocks['body'] = $message->getValue('body');
				$messageBlocks['header'] = $message->getValue('header');
				$messageBlocks['id'] = $message->getId();

				$mailBlocks['messages'] .= dispatches::parseTemplateForMail(
						$releaseMessage,
						$messageBlocks,
						false,
						$message->getId()
				);

				$attachment = $message->getValue('attach_file');

				if ($attachment instanceof umiFile && !$attachment->getIsBroken()) {
					$mailer->attachFile($attachment);
				}

				$umiObjects->unloadObject($messageBlocks['id']);
			}

			$mailer->setFrom($umiRegistry->getVal("//settings/email_from"), $umiRegistry->getVal("//settings/fio_from"));
			$mailer->setSubject($mailBlocks['header']);

			$sel = new selector('objects');
			$sel->types("hierarchy-type")->name("dispatches", "subscriber");
			$sel->where("subscriber_dispatches")->equals($dispatchId);
			$sel->group("name");

			$delay = 0;
			$maxMessages = (int) mainConfiguration::getInstance()->get('modules', 'dispatches.max_messages_in_hour');

			if ($maxMessages && $sel->length() >= $maxMessages) {
				$delay = floor(3600 / $maxMessages);
			}

			/** @var iUmiObject|iUmiEntinty $recipient */
			foreach ($sel->result() as $recipient) {
				$nextMailer = clone $mailer;
				$subscriber = new umiSubscriber($recipient->getId());

				$recipientName = $subscriber->getValue('lname') . " " . $subscriber->getValue('fname') . " " . $subscriber->getValue('father_name');
				$email = $subscriber->getValue('email');

				if (!strlen($email)) {
					$email = $subscriber->getName();
				}

				$mailBlocks['unsubscribe_link'] = $this->getUnSubscribeLink($recipient, $email);

				$nextMailer->setContent(dispatches::parseTemplateForMail(
						$releaseBody,
						$mailBlocks,
						false,
						$subscriber->getId()
				));

				$nextMailer->addRecipient($email, $recipientName);
				$nextMailer->commit();
				$nextMailer->send();

				if ($delay) {
					sleep($delay);
				}

				$umiObjects->unloadObject($recipient->getId());
			}

			$date = new umiDate(time());
			$dispatch->setValue('disp_last_release', $date);
			$dispatch->commit();

			$release->setValue('status', true);
			$release->setValue('date', $date);
			$release->setName($date->getFormattedDate('d-m-Y H:i'));
			$release->commit();

			return true;
		}

		/**
		 * Возвращает ссылку отписки от рассылки
		 * @param iUmiObject $subscriber объект подписчика
		 * @param string $email подписчика
		 * @return string
		 */
		public function getUnSubscribeLink(iUmiObject $subscriber, $email) {
			$protocol = getSelectedServerProtocol();
			$domain = cmsController::getInstance()->getCurrentDomain()->getHost();
			/** @var iUmiObject|iUmiEntinty $subscriber */
			$subscriberId = $subscriber->getId();

			return $protocol . '://' . $domain . $this->pre_lang . '/dispatches/unsubscribe/?id=' . $subscriberId . '&email=' . $email;
		}

		/**
		 * Возвращает результат наполнения выпуска рассылки сообщениями
		 * @param bool $ignore_redirect производить редирект
		 * @param int $iDispId идентификатор рассылки
		 * @return void
		 */
		private function getFillingResult($ignore_redirect, $iDispId) {
			if (!$ignore_redirect) {
				$this->redirect('/admin/dispatches/edit/' . $iDispId . '/');
			}
		}
	}

?>
