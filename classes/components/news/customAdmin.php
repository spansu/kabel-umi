<?php
	/**
	 * Класс пользовательских методов административной панели
	 */
	class NewsCustomAdmin {
		/**
		 * @var news $module
		 */
		public $module;

		public function onAddNewsItem(iUmiEventPoint $event) {
			if ($event->getMode() === "after") {
				/** @var umiHierarchyElement $oElement */
				if($oElement = $event->getRef('element')) {
					$oDate = $oElement->getValue('publish_time');
					if (!$oDate) {
						$oElement->setValue('publish_time', $_SERVER['REQUEST_TIME']);
						$oElement->commit();
					}
				}
			}
			return true;
		}

		public function onAddNewsItemQuick(iUmiEventPoint $event) {
			if ($event->getMode() === "after") {
				$iElementId = $event->getParam('elementId');
				/** @var umiHierarchyElement $oElement */
				if($oElement = umiHierarchy::getInstance()->getElement($iElementId)) {
					$oDate = $oElement->getValue('publish_time');
					if (!$oDate) {
						$oElement->setValue('publish_time', $_SERVER['REQUEST_TIME']);
						$oElement->commit();
					}
				}
			}
			return true;
		}
	}
?>