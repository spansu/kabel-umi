<?php
	/**
	 * Класс пользовательских методов для всех режимов
	 */
	class WebFormsCustomCommon {
		/**
		 * @var webforms $module
		 */
		public $module;

		public function custom_order_send() {

			$domain = htmlspecialchars(trim(getRequest('domain')));
			$page_link = htmlspecialchars(trim(getRequest('page_link')));
			$name = htmlspecialchars(trim(getRequest('order_name')));
			$name_title = htmlspecialchars(trim(getRequest('order_name_title')));
			$phone = htmlspecialchars(trim(getRequest('order_phone')));
			$phone_title = htmlspecialchars(trim(getRequest('order_phone_title')));
			$message = htmlspecialchars(trim(getRequest('order_message')));
			$message_title = htmlspecialchars(trim(getRequest('order_message_title')));

			$ajax_message['status'] = 0;

			if($name == ''){
				$ajax_message['name'] = 1;
			}
			if($phone == ''){
				$ajax_message['phone'] = 1;
			}
			if($phone == ''||$name == ''){
				$ajax_message['status'] = 1;
				echo json_encode($ajax_message);
				exit();
			}

			$mailContent = "<html>";
			$mailContent .= "<body>";
			$mailContent .='<h3>Содержание сообщения:</h3>';
			$mailContent .='<p><b>'.$name_title.'</b> '.$name.'</p>';
			$mailContent .='<p><b>'.$phone_title.'</b> '.$phone.'</p>';
			if($message != ''){
				$mailContent .='<p><b>'.$message_title.'</b> '.$message.'</p>';
			}
			$mailContent .='<p><b>Страница, с которой отправлено сообщение:</b> <a href="'.$page_link.'">'.$page_link.'</a></p>';
			$mailContent .= "</body>\n";
			$mailContent .= "</html>\n";

			$regedit  = regedit::getInstance();
			$mail = new umiMail;

			$sel = new selector('objects');
			$sel->types('object-type')->name('webforms', 'address');
			if (count($sel->result())) {
				$sEmailsSet = $this->module->guessAddressValue($sel->first->getId());
				$sEmailsSet = preg_replace("/[\s;,]+/", ",", $sEmailsSet);
				$arEmailsSet = explode(',', $sEmailsSet);
				foreach ($arEmailsSet as $email) {
					$mail->addRecipient($email);
				}
			} else {
				$email = regedit::getInstance()->getVal("//settings/admin_email");
				$mail->addRecipient($email);
			}
			$mail->setSubject("Письмо с ".$domain);
			$mail->setFrom($regedit->getVal('//settings/email_from'), $regedit->getVal('//settings/fio_from'));
			$mail->setPriorityLevel('highest');
			$mail->setContent($mailContent);
			$mail->commit();
			$mail->send();

			echo json_encode($ajax_message);
			exit();
		}
	}
?>