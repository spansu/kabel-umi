<?php

	/**
	 * Базовый класс модуля "Конструктор форм".
	 * Модуль управляет следующими сущностями:
	 * 1) Адреса;
	 * 2) Формы;
	 * 3) Шаблоны писем;
	 * 4) Сообщения форм;
	 * Модуль позволяет конструировать формы,
	 * назначать им адреса отправки, шаблоны писем
	 * и отправлять их.
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_obratnaya_svyaz/
	 */
	class webforms extends def_module {

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();

			if (cmsController::getInstance()->getCurrentMode() == "admin") {

				$commonTabs = $this->getCommonTabs();

				if ($commonTabs) {
					$commonTabs->add("addresses", [
							"address_edit",
							"address_add"
					]);

					$commonTabs->add("forms", [
							"form_edit",
							"form_add"
					]);

					$commonTabs->add("templates", [
							"template_edit",
							"template_add"
					]);

					$commonTabs->add("messages", [
							"message"
					]);
				}

				$this->__loadLib("admin.php");
				$this->__implement("WebformsAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("WebFormsCustomAdmin", true);
			} else {
				$this->__loadLib("macros.php");
				$this->__implement("WebFormsMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("WebFormsCustomMacros", true);
			}

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("WebFormsCustomCommon", true);
		}

		/**
		 * Возвращает ссылку на страницу, где можно отредактировать объект модуля
		 * @param int $objectId идентификатор объекта
		 * @param bool|string $type тип объекта
		 * @return string
		 * @throws coreException
		 */
		public function getObjectEditLink($objectId, $type = false) {
			$object = umiObjectsCollection::getInstance()->getObject($objectId);
			$umiObjectTypesCollection = umiObjectTypesCollection::getInstance();
			$oType = $umiObjectTypesCollection->getType($object->getTypeId());

			if ($oType->getParentId() == $umiObjectTypesCollection->getTypeIdByHierarchyTypeName("webforms", "form")) {
				$type = "message";
			}

			switch ($type) {
				case 'form' :
				case 'message' : {
					return $this->pre_lang . "/admin/webforms/message/" . $objectId . "/";
				}
				case 'template' : {
					return $this->pre_lang . "/admin/webforms/template_edit/" . $objectId . "/";
				}
				default : {
					return $this->pre_lang . "/admin/webforms/address_edit/" . $objectId . "/";
				}
			}
		}

		/**
		 * Возвращает адрес редактирования страницы и адрес добавления дочерней страницы
		 * @param int $element_id идентификатор страницы
		 * @return array
		 */
		public function getEditLink($element_id) {
			return [false, $this->pre_lang . '/admin/content/edit/' . $element_id . '/'];
		}

		/**
		 * {@inheritdoc}
		 * Применяет к сообщению шаблон письма
		 * @return array|mixed|string
		 * @throws Exception
		 * @throws coreException
		 */
		public function formatMessage($message, $bSplitLongMode = 0) {
			$args = func_get_args();
			$messageId = array_shift($args);

			if (!is_numeric($messageId)) {
				throw new Exception('Message id expected for format message');
			}

			$processAll = array_shift($args);
			$processAll = (is_null($processAll)) ? false : $processAll;

			$umiObjects = umiObjectsCollection::getInstance();
			$umiObjectsTypes = umiObjectTypesCollection::getInstance();

			$messageBody = [
					'from_email_template' => '',
					'from_template' => '',
					'subject_template' => '',
					'master_template' => '',
					'autoreply_from_email_template' => '',
					'autoreply_from_template' => '',
					'autoreply_subject_template' => '',
					'autoreply_template' => ''
			];

			$message = $umiObjects->getObject($messageId);
			$formTypeId = $message->getTypeId();
			$fields = $umiObjectsTypes->getType($formTypeId)->getAllFields(true);

			$sel = new selector('objects');
			$sel->types('object-type')->name('webforms', 'template');
			$sel->where('form_id')->equals($formTypeId);
			$sel->limit(0, 1);

			/** @var umiObject|null $template */
			$template = $sel->first;

			if (!$template) {
				$sTmp = '';
				/** @var iUmiField $field */
				foreach ($fields as $field) {
					$sTmp .= $this->getPropertyValue($message, $field->getName()) . "<br />\n";
				}

				if ($processAll) {
					$messageBody['master_template'] = $sTmp;
				} else {
					$messageBody = $sTmp;
				}
			} else {
				if ($processAll) {
					$messageBody = [];
					$fields = $umiObjectsTypes->getType($template->getTypeId())->getAllFields();

					/** @var iUmiField $field */
					foreach ($fields as $field) {
						$marks = [];
						$fieldName = $field->getName();
						$templateString = str_replace(["&#037;", "&#37;"], "%", $template->getValue($fieldName));
						preg_match_all("/%[A-z0-9_]+%/", $templateString, $marks);

						foreach ($marks[0] as $mark) {
							$templateString = str_replace($mark, $this->getPropertyValue($message, trim($mark, '% ')), $templateString);
						}

						$messageBody[$fieldName] = $templateString;
					}
				} else {
					$templateString = str_replace("&#037;", "%", $template->getValue('master_template'));
					preg_match_all("/%[A-z0-9_]+%/", $templateString, $marks);

					foreach ($marks[0] as $mark) {
						$templateString = str_replace($mark, $this->getPropertyValue($message, trim($mark, '% ')), $templateString);
					}

					$messageBody = $templateString;
				}
			}

			return $messageBody;
		}

		/**
		 * Возвращает идентификатор адреса,
		 * который содержит заданный адрес
		 * @param string $_sAddress адрес
		 * @return mixed
		 * @throws selectorException
		 */
		public function guessAddressId($_sAddress) {
			if (is_numeric($_sAddress)) {
				return $_sAddress;
			}

			$_sFind = str_replace([' ', ','], ['%', '%'], $_sAddress);

			$addresses = new selector('objects');
			$addresses->types('object-type')->name('webforms', 'address');
			$addresses->where('address_list')->ilike($_sFind);
			$addresses->option('no-length')->value(true);
			$addresses->option('load-all-props')->value(true);
			$addresses->option('return')->value('id');
			$addresses = $addresses->result();

			$addressesIds = [];
			foreach ($addresses as $addressId) {
				if (isset($addressId['id'])) {
					$addressesIds[] = $addressId['id'];
				}
			}

			return (!empty($addressesIds)) ? $addressesIds[0] : $_sAddress;
		}

		/**
		 * Административный метод.
		 * Возвращает формы, которые не привязаны к указанному шаблону.
		 * @param bool|int $currentTemplateId идентификатор шаблона письма
		 * @return array
		 * @throws coreException
		 */
		public function getUnbindedForms($currentTemplateId = false) {
			$typesCollection = umiObjectTypesCollection::getInstance();
			$baseType = $typesCollection->getTypeIdByHierarchyTypeName("webforms", "form");
			$forms = $typesCollection->getSubTypesList($baseType);

			$sel = new selector('objects');
			$sel->types('object-type')->name('webforms', 'template');
			$result = $sel->result();

			$exclude = [];
			$currentFormId = null;

			foreach ($result as $template) {
				if (!($template instanceof umiObject)) {
					continue;
				}

				$formId = $template->getValue('form_id');
				$exclude[] = $formId;

				if ($template->getId() == $currentTemplateId) {
					$currentFormId = $formId;
				}
			}

			$forms = array_diff($forms, $exclude);

			if (is_numeric($currentFormId) && !in_array($currentFormId, $forms)) {
				array_unshift($forms, $currentFormId);
			}

			$result = [];

			foreach ($forms as $id) {
				$itemArr = [];
				$itemArr['attribute:id'] = $id;
				$itemArr['node:name'] = $typesCollection->getType($id)->getName();
				$result[] = $itemArr;
			}

			return [
					"items" => [
							"nodes:item" => $result
					]
			];
		}

		/**
		 * Административный метод.
		 * Возвращает адреса для форм.
		 * @param bool|int $iFormId идентификатор выбранной формы
		 * @return mixed
		 * @throws selectorException
		 */
		public function getAddresses($iFormId = false) {
			$sel = new selector('objects');
			$sel->types('object-type')->name('webforms', 'address');
			$result = $sel->result();
			$aBlock = [];
			$aLines = [];

			/**
			 * @var iUmiObject|iUmiEntinty $oObject
			 */
			foreach ($result as $oObject) {
				$aLine = [];
				$aLine['attribute:id'] = $oObject->getId();

				if (in_array($iFormId, explode(',', $oObject->getValue('form_id')))) {
					$aLine['attribute:selected'] = 'selected';
				}

				$aLine['node:text'] = $oObject->getName();
				$aLines[] = self::parseTemplate('', $aLine);
			}

			$aBlock['attribute:input_name'] = "data[address]";
			$aBlock['subnodes:items'] = $aLines;

			return self::parseTemplate('', $aBlock);
		}

		/**
		 * Административный метод.
		 * Возращает список формы
		 * @param bool|int $form_id идентификатор выбранной формы
		 * @return mixed
		 * @throws coreException
		 */
		public function getForms($form_id = false) {
			$objectTypes = umiObjectTypesCollection::getInstance();
			$type_id = $objectTypes->getTypeIdByHierarchyTypeName('webforms', 'form');
			$sub_types = $objectTypes->getSubTypesList($type_id);
			$block_arr = [];
			$lines = [];

			foreach ($sub_types as $typeId) {
				$type = $objectTypes->getType($typeId);

				if ($type instanceof umiObjectType) {
					$line_arr = [];
					$line_arr['attribute:id'] = $typeId;

					if ($form_id == $typeId) {
						$line_arr['attribute:selected'] = 'selected';
					}

					$line_arr['node:text'] = $type->getName();
					$lines[] = self::parseTemplate('', $line_arr);
				}
			}

			$block_arr['subnodes:items'] = $lines;
			return self::parseTemplate('', $block_arr);
		}

		/**
		 * @alias data::checkRequiredFields()
		 * @return array|bool
		 * @throws publicAdminException
		 */
		public function checkRequiredFields($typeId) {
			$cmsController = cmsController::getInstance();
			/**
			 * @var data $dataModule
			 */
			$dataModule = $cmsController->getModule('data');

			if (!$dataModule) {
				throw new publicAdminException('Service unavailable');
			}

			return $dataModule->checkRequiredFields($typeId);
		}

		/**
		 * @alias data::assembleErrorFields()
		 * @return string
		 * @throws publicAdminException
		 */
		public function assembleErrorFields($errorFields) {
			$cmsController = cmsController::getInstance();
			/**
			 * @var data $dataModule
			 */
			$dataModule = $cmsController->getModule('data');

			if (!$dataModule) {
				throw new publicAdminException('Service unavailable');
			}

			return $dataModule->assembleErrorFields($errorFields);
		}

		/**
		 * Возвращает значение поля сообщения
		 * @param umiObject $obj объект сообщения
		 * @param string $propName названия поля
		 * @return int|Mixed|string
		 */
		public function getPropertyValue(umiObject $obj, $propName) {
			if ($propName === 'id') {
				return $obj->getId();
			}

			if ($prop = $obj->getPropByName($propName)) {
				switch ($prop->getDataType()) {
					case 'date' : {
						if (($date = $prop->getValue()) instanceof umiDate) {
							return $date->getFormattedDate();
						}
						return '';
					}
					case 'relation': {
						$result = [];
						$ids = $prop->getValue();

						if (!is_array($ids)) {
							$ids = [$ids];
						}

						foreach ($ids as $id) {
							if ($value = umiObjectsCollection::getInstance()->getObject($id)) {
								$result[] = $value->getName();
							}
						}
						return empty($result) ? '' : implode(', ', $result);
					}
					case 'boolean': {
						$langs = cmsController::getInstance()->langs;
						$value = $prop->getValue();
						return $value ? $langs['boolean_true'] : $langs['boolean_false'];
					}
					default: {
						return $prop->getValue();
					}
				}
			}
			return '';
		}

		/**
		 * Клиентский метод.
		 * Проверяет существование объекта адреса по строковому адресу.
		 * Если адреса нет - кинет исключение errorPanicException
		 * @param string $address адрес
		 * @throws coreException
		 * @throws errorPanicException
		 * @throws privateException
		 */
		public function checkAddressExistence($address) {
			$count = 0;

			if ($address) {
				$find = '%' . $address . '%';

				$sel = new selector('objects');
				$sel->types('object-type')->name('webforms', 'address');
				$sel->where('address_list')->like($find);
				$sel->option('return')->value('count');
				$count = $sel->result();
			}

			if (!$count) {
				/** @var webforms|WebFormsMacros $this */
				$this->reportError("%unknown_address%");
			}
		}

		/**
		 * Клиентский метод.
		 * Возвращает адрес отправки формы.
		 * @param string|int $_iID идентификатор адреса или его строковое представление
		 * @return Mixed
		 */
		public function guessAddressValue($_iID) {
			if (is_numeric($_iID)) {
				$oObjects = umiObjectsCollection::getInstance();

				if (!$oObjects->isExists($_iID)) {
					return $_iID;
				}

				return $oObjects->getObject($_iID)->getValue('address_list');
			} else {
				$this->checkAddressExistence($_iID);
				return $_iID;
			}
		}

		/**
		 * Клиентский метод.
		 * Возвращает имя адреса отправки формы
		 * @param string|int $_iID идентификатор адреса или его строковое представление
		 * @return bool|String
		 */
		public function guessAddressName($_iID) {
			if (is_numeric($_iID)) {
				$oObjects = umiObjectsCollection::getInstance();
				if (!$oObjects->isExists($_iID)) {
					return false;
				}
				return $oObjects->getObject($_iID)->getName();
			} else {
				return $_iID;
			}
		}
	}

?>
