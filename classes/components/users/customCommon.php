<?php
	/**
	 * Класс пользовательских методов для всех режимов
	 */
	class UsersCustomCommon {
		/**
		 * @var users $module
		 */
		public $module;

		public function custom_registrate_do($template = "default") {
			$_REQUEST['login'] = (string) getRequest('email');
			return $this->module->registrate_do($template);
		}

		public function custom_settings_do($template = "default") {
			$_REQUEST['login'] = (string) getRequest('email');
			return $this->module->settings_do($template);
		}

		public function custom_login_do() {

			$login = getRequest('login');
			$password = getRequest('password');

			$ajax_message['status'] = 0;

			if(strlen($login) == 0) {
				$ajax_message['status'] = 1;
				echo json_encode($ajax_message);
				exit();
			}

			$permissions = permissionsCollection::getInstance();

			$user = $permissions->checkLogin($login, $password);

			if($user instanceof iUmiObject) {
				$permissions->loginAsUser($user);

				if ($permissions->isAdmin($user->id)) {
					$session = session::getInstance();
					$session->set('csrf_token', md5(rand() . microtime()));
					if ($permissions->isSv($user->id)) {
						$session->set('user_is_sv', true);
					}
					$session->setValid();
				}

				session::commit();
				system_runSession();

				$oEventPoint = new umiEventPoint("users_login_successfull");
				$oEventPoint->setParam("user_id", $user->id);
				$this->setEventPoint($oEventPoint);

				echo json_encode($ajax_message);
				exit();

			} else {
				$oEventPoint = new umiEventPoint("users_login_failed");
				$oEventPoint->setParam("login", $login);
				$oEventPoint->setParam("password", $password);
				$this->setEventPoint($oEventPoint);

				$ajax_message['status'] = 1;
				echo json_encode($ajax_message);
				exit();
			}
			echo json_encode($ajax_message);
			exit();
		}

		public function user_type() {

			$result['type'] = 0;
			$result['admin'] = 0;

			$permissions = permissionsCollection::getInstance();
			$userId = $permissions->getUserId();
			if ($permissions->isAdmin($userId)){
				$result['type'] = 'admin';
				$result['admin'] = 1;
			}else{
				$guestId = $permissions->getGuestId();
				if ($guestId == $userId){
					$result['type'] =  'guest';
				} else {
					$result['type'] =  'user';
				}
			}

			return $result;
		}

		public function yamaillogin() {
			$arParams = array(
				'login' => getRequest('login'),
				'passwd' => getRequest('passwd'),
				'retpath' => getRequest('retpath'),
				'mode' => 'auth'
			);
			$maildomain = getRequest('maildomain');
			$url = "http://passport.yandex.ru/for/{$maildomain}?" . http_build_query($arParams);
			$buffer = outputBuffer::current();
			$buffer->clear();
			$buffer->contentType("text/html");
			$buffer->push("<html><body><script type='text/javascript'>document.location.href='{$url}';</script></body></html>");
			$buffer->end();
		}

	}
?>