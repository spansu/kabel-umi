<?php
	/**
	 * Класс функционала административной панели
	 */
	class UsersAdmin{

		use baseModuleAdmin;
		/**
		 * @var users $module
		 */
		public $module;

		/**
		 * Алиас users_list()
		 */
		public function users_list_all() {
			$this->users_list();
		}

		/**
		 * Возвращает список пользователей с учетом фильтров
		 * @throws coreException
		 * @throws expectObjectException
		 * @throws selectorException
		 */
		public function users_list() {
			$this->setDataType('list');
			$this->setActionType('view');

			if ($this->module->ifNotXmlMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'user');
			$sel->limit($offset, $limit);

			if (getRequest('param0') == 'outgroup') {
				$sel->where('groups')->isnull(true);
			} else {
				if ($groupId = $this->expectObjectId('param0')) {
					$sel->where('groups')->equals($groupId);
				}
			}

			if (!permissionsCollection::getInstance()->isSv()) {
				$sel->where('guid')->notequals('system-supervisor');
			}

			if ($loginSearch = getRequest('search')) {
				$sel->where('login')->like('%' . $loginSearch . '%');
			}

			selectorHelper::detectFilters($sel);

			$this->setDataRange($limit, $offset);
			$data = $this->prepareData($sel->result, 'objects');

			$this->setData($data, $sel->length);
			$this->doData();
		}

		/**
		 * Возвращает список групп с учетом фильтров
		 * @throws coreException
		 * @throws selectorException
		 */
		public function groups_list() {
			$this->setDataType('list');
			$this->setActionType('view');

			if ($this->module->ifNotXmlMode()) {
				$this->setDirectCallError();
				$this->doData();
				return true;
			}

			$limit = getRequest('per_page_limit');
			$curr_page = (int) getRequest('p');
			$offset = $limit * $curr_page;

			$sel = new selector('objects');
			$sel->types('object-type')->name('users', 'users');

			if (!permissionsCollection::getInstance()->isSv()) {
				$sel->where('guid')->notequals('users-users-15');
			}

			$sel->limit($offset, $limit);

			selectorHelper::detectFilters($sel);

			$this->setDataRange($limit, $offset);

			$data = $this->prepareData($sel->result, "objects");
			$this->setData($data, $sel->length);
			$this->doData();
		}

		/**
		 * Возвращает форму создания объекта,
		 * если передан $_REQUEST['param1'] = do пытается создать объект
		 * @throws coreException
		 * @throws publicAdminException
		 * @throws wrongElementTypeAdminException
		 */
		public function add() {
			$type = (string) getRequest('param0');
			$mode = (string) getRequest('param1');

			$this->setHeaderLabel('header-users-add-' . $type);
			$inputData = array(
				'type'					=> $type,
				'type-id' 				=> getRequest('type-id'),
				'aliases'				=> array('name' => 'login'),
				'allowed-element-types'	=> array('user', 'users')
			);

			if ($mode == "do") {
				$object = $this->saveAddedObjectData($inputData);

				$permissions = permissionsCollection::getInstance();
				if (!$permissions->isSv($permissions->getUserId())) {
					$groups = $object->getValue('groups');
					if (in_array(SV_GROUP_ID, $groups)) {
						unset($groups[array_search(SV_GROUP_ID, $groups)]);
						$object->setValue('groups', $groups);
					}
				}

				$object->setValue('user_dock', 'seo,content,news,blogs20,forum,comments,vote,webforms,photoalbum,dispatches,catalog,emarket,banners,users,stat,exchange,trash');
				$object->commit();

				$this->module->save_perms($object->getId(), __FUNCTION__);
				$this->chooseRedirect($this->module->pre_lang . '/admin/users/edit/' . $object->getId() . '/');
			}

			$this->setDataType('form');
			$this->setActionType('create');

			$data = $this->prepareData($inputData, 'object');

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает форму редактирования объекта,
		 * если передан $_REQUEST['param1'] = do пытается отредактировать объект
		 * @throws coreException
		 * @throws expectObjectException
		 */
		public function edit() {
			$object = $this->expectObject('param0', true);
			$mode = (string) getRequest('param1');
			$objectId = $object->getId();

			$this->setHeaderLabel('header-users-edit-' . $this->getObjectTypeMethod($object));

			$this->checkSv($objectId);

			$inputData = Array(
				'object' => $object,
				'aliases' => Array('name' => 'login'),
				'allowed-element-types' => Array('users', 'user')
			);

			if ($mode == 'do') {
				if (!def_module::checkHTTPReferer()) {
					$this->module->errorNewMessage(getLabel('error-users-non-referer'));
					$this->module->errorPanic();
				}
				/**
				 * @var iUmiObject|iUmiEntinty $object
				 */
				$object = $this->saveEditedObjectData($inputData);
				$objectId = $object->getId();

				if (isset($_REQUEST['data'][$objectId]['password'][0])) {
					$password = $_REQUEST['data'][$objectId]['password'][0];
				} else {
					$password = false;
				}

				$permissions = permissionsCollection::getInstance();
				$guestId = $permissions->getGuestId();
				$userId = $permissions->getUserId();

				if ($object->getId() == $userId) {
					if ($password) {
						$_SESSION['cms_pass'] = $object->password;
					}
				}

				if (in_array($object->getId(), array($userId, $guestId, SV_USER_ID))) {
					if (!$object->is_activated) {
						$object->is_activated = true;
						$object->commit();
					}
				}

				$this->module->save_perms($objectId, __FUNCTION__);
				$this->chooseRedirect();
			}

			$this->setDataType('form');
			$this->setActionType('modify');

			$data = $this->prepareData($inputData, 'object');

			$this->setData($data);
			$this->doData();
		}

		/**
		 * Удаляет пользователей
		 * @throws coreException
		 * @throws expectObjectException
		 * @throws publicAdminException
		 * @throws wrongElementTypeAdminException
		 */
		public function del() {
			$objects = getRequest('element');
			if (!is_array($objects)) {
				$objects = Array($objects);
			}

			foreach ($objects as $objectId) {
				$object = $this->expectObject($objectId, false, true);
				if (!$object) {
					continue;
				}
				$this->checkSv($object->getId());

				$object_id = $object->getId();
				if ($object_id == SV_GROUP_ID) {
					throw new publicAdminException(getLabel('error-sv-group-delete'));
				}

				if ($object_id == SV_USER_ID) {
					throw new publicAdminException(getLabel('error-sv-user-delete'));
				}

				$regedit = regedit::getInstance();
				if ($object_id == $regedit->getVal('//modules/users/guest_id')) {
					throw new publicAdminException(getLabel('error-guest-user-delete'));
				}

				if ($object_id == $regedit->getVal('//modules/users/def_group')) {
					throw new publicAdminException(getLabel('error-sv-group-delete'));
				}

				if ($object_id == permissionsCollection::getInstance()->getUserId()) {
					throw new publicAdminException(getLabel('error-delete-yourself'));
				}

				$params = Array(
					'object'		=> $object,
					'allowed-element-types' => Array('user', 'users')
				);

				$this->deleteObject($params);
			}

			$this->setDataType('list');
			$this->setActionType('view');
			$data = $this->prepareData($objects, 'objects');
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Меняет статус активности пользователей
		 * @throws coreException
		 * @throws expectObjectException
		 * @throws publicAdminException
		 */
		public function activity() {
			$objects = getRequest('object');
			if (!is_array($objects)) {
				$objects = Array($objects);
			}
			$is_active = (bool) getRequest('active');

			foreach($objects as $objectId) {
				$object = $this->expectObject($objectId, false, true);
				$this->checkSv($objectId);

				if (!$is_active) {
					if ($objectId == SV_USER_ID) {
						throw new publicAdminException(getLabel('error-sv-user-activity'));
					}

					$regedit = regedit::getInstance();
					if ($objectId == $regedit->getVal('//modules/users/guest_id')) {
						throw new publicAdminException(getLabel('error-guest-user-activity'));
					}
				}

				$object->setValue('is_activated', $is_active);
				$object->commit();
			}

			$this->setDataType('list');
			$this->setActionType('view');
			$data = $this->prepareData($objects, 'objects');
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает настройки модуля "Пользователи".
		 * Если передано ключевое слово "do" в $_REQUEST['param0'],
		 * то сохраняет переданные настройки.
		 * @return void
		 */
		public function config() {
			$umiRegistry = regedit::getInstance();
			$objectTypesColl = umiObjectTypesCollection::getInstance();

			$params = array(
				'config' => array(
					'guide:def_group' => array(
						'type-id' => $objectTypesColl->getTypeIdByGUID('users-users'),
						'value' => null
					),
					'guide:guest_id' => array(
						'type-id' => $objectTypesColl->getTypeIdByGUID('users-user'),
						'value' => null
					),
					'boolean:without_act' => null,
					'boolean:pages_permissions_changing_enabled_on_add' => null,
					'boolean:pages_permissions_changing_enabled_on_edit' => null
				)
			);

			$mode = getRequest('param0');

			if ($mode == 'do') {
				$params = $this->expectParams($params);
				$umiRegistry->setVar('//modules/users/def_group', $params['config']['guide:def_group']);
				$umiRegistry->setVar('//modules/users/guest_id', $params['config']['guide:guest_id']);
				$umiRegistry->setVar('//modules/users/without_act', $params['config']['boolean:without_act']);
				$umiRegistry->setVar(
					'//modules/users/pages_permissions_changing_enabled_on_add',
					$params['config']['boolean:pages_permissions_changing_enabled_on_add']
				);
				$umiRegistry->setVar(
					'//modules/users/pages_permissions_changing_enabled_on_edit',
					$params['config']['boolean:pages_permissions_changing_enabled_on_edit']
				);
				$this->chooseRedirect();
			}

			$params['config']['guide:def_group']['value'] = $umiRegistry->getVal('//modules/users/def_group');
			$params['config']['guide:guest_id']['value'] = $umiRegistry->getVal('//modules/users/guest_id');
			$params['config']['boolean:without_act'] = $umiRegistry->getVal('//modules/users/without_act');
			$params['config']['boolean:pages_permissions_changing_enabled_on_add'] = $umiRegistry->getVal('//modules/users/pages_permissions_changing_enabled_on_add');
			$params['config']['boolean:pages_permissions_changing_enabled_on_edit'] = $umiRegistry->getVal('//modules/users/pages_permissions_changing_enabled_on_edit');

			$data = $this->prepareData($params, 'settings');
			$this->setDataType('settings');
			$this->setActionType('modify');
			$this->setData($data);
			$this->doData();
		}

		/**
		 * Возвращает список пользователей и групп,
		 * кроме супервайзера и его группы
		 * @return array
		 * @throws coreException
		 * @throws selectorException
		 */
		public function getPermissionsOwners() {
			$this->module->flushAsXML('getPermissionsOwners');

			outputBuffer::current();
			$objects = umiObjectsCollection::getInstance();
			$objectTypes = umiObjectTypesCollection::getInstance();
			$groupTypeId = $objectTypes->getTypeIdByHierarchyTypeName('users', 'users');

			$svGroupId = $objects->getObjectIdByGUID('users-users-15');
			$svId = $objects->getObjectIdByGUID('system-supervisor');

			$restrict = array($svId, $svGroupId);

			$sel = new selector('objects');
			$sel->types('hierarchy-type')->name('users', 'users');
			$sel->types('hierarchy-type')->name('users', 'user');
			$sel->limit(0, 15);
			selectorHelper::detectFilters($sel);

			$items = array();
			foreach ($sel as $object) {
				if (in_array($object->id, $restrict)) {
					continue;
				}
				$usersList = array();

				/**
				 * @var iUmiObject $object
				 */
				if ($object->getTypeId() == $groupTypeId) {
					$users = new selector('objects');
					$users->types('object-type')->name('users', 'user');
					$users->where('groups')->equals($object->id);
					$users->limit(0, 5);
					foreach ($users as $user) {
						$usersList[] = array(
							'attribute:id'		=> $user->id,
							'attribute:name'	=> $user->name,
							'xlink:href'		=> $user->xlink
						);
					}

					$type = 'group';
				} else {
					$type = 'user';
				}

				$items[] = array(
					'attribute:id'		=> $object->id,
					'attribute:name'	=> $object->name,
					'attribute:type'	=> $type,
					'xlink:href'		=> $object->xlink,
					'nodes:user'		=> $usersList
				);
			}

			return array(
				'list' => array(
					'nodes:owner' => $items
				)
			);
		}

		/**
		 * Проверяет, что действия над объектом
		 * супервайзера делает супервайзер
		 * @param int $objectId проверяемый объект, предположительно супервайзер
		 * @throws expectObjectException
		 * @throws publicAdminException
		 */
		public function checkSv ($objectId) {
			$object = $this->expectObject($objectId, true, true);
			$perms = permissionsCollection::getInstance();
			$userId = $perms->getUserId();

			if ($perms->isSv ($object->getId()) && !$perms->isSv($userId))	{
				throw new publicAdminException (getLabel('error-break-action-with-sv'));
			}
		}

		/**
		 * Возвращает количество пользователей всего или в заданной группе
		 * @param bool|int $groupId идентификатор группы
		 * @return Int
		 * @throws coreException
		 * @throws publicException
		 */
		public function getGroupUsersCount($groupId = false) {
			$objectTypes = umiObjectTypesCollection::getInstance();
			$userObjectTypeId = $objectTypes->getTypeIdByHierarchyTypeName('users', 'user');
			$userObjectType = $objectTypes->getType($userObjectTypeId);

			if ($userObjectType instanceof umiObjectType == false) {
				throw new publicException("Can't load user object type");
			}

			$sel = new selector('objects');
			$sel->types('object-type')->id($userObjectTypeId);
			$sel->option('return')->value('count');

			if ($groupId !== false) {
				if ($groupId != 0) {
					$sel->where('groups')->equals($groupId);
				} else {
					$sel->where('groups')->isnull();
				}
			}

			return $sel->result();
		}

		/**
		 * Возвращает настройки для формирования табличного контрола
		 * @param string $param контрольный параметр
		 * @return array
		 */
		public function getDatasetConfiguration($param = '') {

			if ($param == 'groups' || $param === 'users') {
				$loadMethod = "groups_list";
				$type = 'users';
				$default = 'name[400px]';
			} else {
				$loadMethod = 'users_list/' . $param;
				$type = 'user';
				$default = 'name[400px]|fname[250px]|lname[250px]|e-mail[250px]|groups[250px]|is_activated[250px]';
			}

			return array(
				'methods' => array(
					array(
						'title' => getLabel('smc-load'),
						'forload' => true,
						'module' => 'users',
						'#__name' => $loadMethod
					),
					array(
						'title' => getLabel('smc-delete'),
						'module' => 'users',
						'#__name' => 'del',
						'aliases' => 'tree_delete_element,delete,del'
					),
					array(
						'title' => getLabel('smc-activity'),
						'module' => 'users',
						'#__name' => 'activity',
						'aliases' => 'tree_set_activity,activity'
					),
					array(
						'title' => getLabel('smc-copy'),
						'module' => 'content',
						'#__name' => 'tree_copy_element'
					),
					array(
						'title' => getLabel('smc-move'),
						'module' => 'content',
						'#__name' => 'move'
					),
					array(
						'title' => getLabel('smc-change-template'),
						'module' => 'content',
						'#__name' => 'change_template'
					),
					array(
						'title' => getLabel('smc-change-lang'),
						'module' => 'content',
						'#__name' => 'move_to_lang'
					)
				),
				'types' => array(
					array(
						'common' => 'true',
						'id' => $type
					)
				),
				'stoplist' => array(
					'avatar',
					'userpic',
					'user_settings_data',
					'user_dock',
					'orders_refs',
					'activate_code',
					'password',
					'last_request_time',
					'login', 'is_online',
					'delivery_addresses',
					'messages_count'
				),
				'default' => $default
			);
		}

		/**
		 * Получает сожержимое дока
		 * @param bool|int $userId идентификатор пользователя
		 * @return mixed|void
		 */
		public function getFavourites($userId = false) {
			if (!$userId) {
				$userId = getRequest('param0');
			}
			$objects = umiObjectsCollection::getInstance();
			$permissions = permissionsCollection::getInstance();
			$regedit = regedit::getInstance();

			$user = $objects->getObject($userId);
			if ($user instanceof iUmiObject == false) {
				return;
			}

			$isTrashAllowed = $permissions->isAllowedMethod($userId, 'data', 'trash');
			$userDockModules = explode(',', $user->user_dock);

			//Получаем если есть содержимое дока из настроек пользователя
			$settings_data = $user->user_settings_data;
			$settings_data_arr = unserialize($settings_data);

			if (isset($settings_data_arr['dockItems']) && isset($settings_data_arr['dockItems']['common'])){
				$userDockModules = explode(';',$settings_data_arr['dockItems']['common']);
			}

			$items = array();
			foreach ($userDockModules as $moduleName) {
				if ($moduleName == '')  {
					continue;
				}
				if ($regedit->getVal('/modules/' . $moduleName) == false && $moduleName != 'trash') {
					continue;
				}

				if ($permissions->isAllowedModule(false,	$moduleName) == false)	{
					if ($moduleName == 'trash') {
						if ($isTrashAllowed == false) {
							continue;
						}
					} else {
						continue;
					}
				}

				$items[] = users::parseTemplate("", array(
					'attribute:id'	=> $moduleName,
					'attribute:label' => getLabel('module-' . $moduleName)
				));
			}

			return users::parseTemplate("", array(
				'subnodes:items'	=> $items
			));
		}


		/**
		 * Возвращает права пользователей|групп. Применяет в настройках прав
		 * страниц в административной панели.
		 * @param string $module имя модуля
		 * @param string $method имя метода
		 * @param bool $element_id id страницы
		 * @param bool $parent_id id родительской страницы
		 * @return mixed|string
		 */
		public function permissions($module = "", $method = "", $element_id = false, $parent_id = false) {
			if (!$module && !$method && !$element_id && !$parent_id) {
				return "";
			}

			$objectsCollection = umiObjectsCollection::getInstance();
			$permissions = permissionsCollection::getInstance();

			$perms_users = array();
			$perms_groups = array();
			if ($element_id || $parent_id) {
				$typeId = umiObjectTypesCollection::getInstance()->getTypeIdByHierarchyTypeName("users", "user");
				$records     = $permissions->getRecordedPermissions($element_id ? $element_id : $parent_id);
				foreach ($records as $id => $level) {
					$owner = $objectsCollection->getObject($id);
					if (!$owner) continue;
					if ($owner->getTypeId() == $typeId) {
						$ownerGroupsIds = $owner->getValue('groups');
						if (is_array($ownerGroupsIds)) {
							foreach ($ownerGroupsIds as $groupId) {
								$groupLevel = $permissions->isAllowedObject($groupId, $element_id ? $element_id : $parent_id);
								foreach ($groupLevel as $i => $l) {
									$level |= pow(2, $i) * (int) $l;
								}
							}
						}

						$perms_users[] = array(
							'attribute:id'		=> $owner->getGUID() ? $owner->getGUID() : $owner->getId(),
							'attribute:login'	=> $owner->getValue('login'),
							'attribute:access'	=> $level
						);
					} else {
						$perms_groups[] = array(
							'attribute:id'		=> $owner->getGUID() ? $owner->getGUID() : $owner->getId(),
							'attribute:title'	=> $owner->getValue('nazvanie'),
							'attribute:access'	=> $level
						);
					}
				}
			} else {
				$current_user_id = $permissions->getUserId();
				$current_user = $objectsCollection->getObject($current_user_id);
				$current_owners = $current_user->getValue("groups");
				if (!is_array($current_owners)) {
					$current_owners = array();
				}
				$current_owners[] = $current_user_id;

				if (!$method) $method = "page";
				$method_view = $method;
				$method_edit = $method . ".edit";

				$owners = $permissions->getPrivileged(array(array($module, $method_view), array($module, $method_edit)));
				foreach ($owners as $ownerId) {
					if (in_array($ownerId, array(SV_USER_ID, SV_GROUP_ID))) continue;
					/* @var umiObject $owner */
					$owner = selector::get('object')->id($ownerId);
					if (!$owner) continue;

					$r = $e = $c = $d = $m = 0;
					if (in_array($ownerId, $current_owners)) {
						$r = permissionsCollection::E_READ_ALLOWED_BIT;
						$e = permissionsCollection::E_EDIT_ALLOWED_BIT;
						$c = permissionsCollection::E_CREATE_ALLOWED_BIT;
						$d = permissionsCollection::E_DELETE_ALLOWED_BIT;
						$m = permissionsCollection::E_MOVE_ALLOWED_BIT;
					} else {
						$r = $permissions->isAllowedMethod($ownerId, $module, $method_view) ? permissionsCollection::E_READ_ALLOWED_BIT : 0;
						$e = $permissions->isAllowedMethod($ownerId, $module, $method_edit) ? permissionsCollection::E_EDIT_ALLOWED_BIT : 0;
						if ($e) {
							$c = permissionsCollection::E_CREATE_ALLOWED_BIT;
							$d = permissionsCollection::E_DELETE_ALLOWED_BIT;
							$m = permissionsCollection::E_MOVE_ALLOWED_BIT;
						}
					}

					$r = (int)$r & permissionsCollection::E_READ_ALLOWED_BIT;
					$e = (int)$e & permissionsCollection::E_EDIT_ALLOWED_BIT;
					$c = (int)$c & permissionsCollection::E_CREATE_ALLOWED_BIT;
					$d = (int)$d & permissionsCollection::E_DELETE_ALLOWED_BIT;
					$m = (int)$m & permissionsCollection::E_MOVE_ALLOWED_BIT;

					/* @var iUmiObjectType $ownerObjectType */
					$ownerObjectType = selector::get('object-type')->id($owner->getTypeId());
					$ownerType = $ownerObjectType->getMethod();

					if ($ownerType == 'user') {
						$perms_users[] = array(
							'attribute:id'		=> $owner->getGUID() ? $owner->getGUID() : $owner->getId(),
							'attribute:login'	=> $owner->getValue('login'),
							'attribute:access'	=> ($r + $e + $c + $d + $m)
						);
					} else {
						$perms_groups[] = array(
							'attribute:id'		=> $owner->getGUID() ? $owner->getGUID() : $owner->getId(),
							'attribute:title'	=> $owner->getName(),
							'attribute:access'	=> ($r + $e + $c + $d + $m)
						);
					}
				}
			}

			return users::parseTemplate('', array(
				'users'		=> array('nodes:user' => $perms_users),
				'groups'	=> array('nodes:group' => $perms_groups)
			));
		}

		/**
		 * Возвращает права пользователя|группы на страницу
		 * @param int $id id пользователя|группы
		 * @param int $element_id id страницы
		 * @return array
		 */
		public function getUserPermissions($id, $element_id) {
			$allow = permissionsCollection::getInstance()->isAllowedObject($id, $element_id);
			$permission = ((int)$allow[permissionsCollection::E_READ_ALLOWED]   * permissionsCollection::E_READ_ALLOWED_BIT) +
				((int)$allow[permissionsCollection::E_EDIT_ALLOWED]   * permissionsCollection::E_EDIT_ALLOWED_BIT) +
				((int)$allow[permissionsCollection::E_CREATE_ALLOWED] * permissionsCollection::E_CREATE_ALLOWED_BIT) +
				((int)$allow[permissionsCollection::E_DELETE_ALLOWED] * permissionsCollection::E_DELETE_ALLOWED_BIT) +
				((int)$allow[permissionsCollection::E_MOVE_ALLOWED]   * permissionsCollection::E_MOVE_ALLOWED_BIT);
			return array('user' => array('attribute:id' => $id, 'node:name' => $permission));
		}

		/**
		 * Возвращает права пользователя|группы на модули, группы методов и домены системы
		 * @param int|bool $ownerId id пользователя|группы, если не передан - вернет права гостя
		 * @return array
		 */
		public function choose_perms($ownerId = false) {
			$regedit = regedit::getInstance();
			$domainsCollection = domainsCollection::getInstance();
			$permissions = permissionsCollection::getInstance();

			if ($ownerId === false) {
				$ownerId = (int) $regedit->getVal("//modules/users/guest_id");
			}

			$restrictedModules = array('autoupdate', 'backup');

			$modules_arr = array();
			$modules_list = $regedit->getList("//modules");

			foreach ($modules_list as $md) {
				list ($module_name) = $md;

				if (in_array($module_name, $restrictedModules)) {
					continue;
				}

				$func_list = array_keys($permissions->getStaticPermissions($module_name));
				if (!system_is_allowed($module_name)) {
					continue;
				}

				$module_label = getLabel("module-" . $module_name);
				$is_allowed_module = $permissions->isAllowedModule($ownerId, $module_name);


				$options_arr = array();
				if (is_array($func_list)) {
					foreach ($func_list as $method_name) {
						if (!system_is_allowed($module_name, $method_name)) {
							continue;
						}

						$is_allowed_method = $permissions->isAllowedMethod($ownerId, $module_name, $method_name);

						$option_arr = array();
						$option_arr['attribute:name'] = $method_name;
						$option_arr['attribute:label'] = getLabel("perms-" . $module_name . "-" . $method_name, $module_name);
						$option_arr['attribute:access'] = (int) $is_allowed_method;
						$options_arr[] = $option_arr;
					}
				}

				$module_arr = array();
				$module_arr['attribute:name'] = $module_name;
				$module_arr['attribute:label'] = $module_label;
				$module_arr['attribute:access'] = (int) $is_allowed_module;
				$module_arr['nodes:option'] = $options_arr;
				$modules_arr[] = $module_arr;
			}

			$domains_arr = array();
			$domains = $domainsCollection->getList();
			/* @var domain $domain */
			foreach ($domains as $domain) {
				$domain_arr = array();
				$domain_arr['attribute:id'] = $domain->getId();
				$domain_arr['attribute:host'] = $domain->getHost();
				$domain_arr['attribute:access'] = $permissions->isAllowedDomain($ownerId, $domain->getId());
				$domains_arr[] = $domain_arr;
			}

			$result_arr = array();
			$result_arr['domains']['nodes:domain'] = $domains_arr;
			$result_arr['nodes:module'] = $modules_arr;

			return $result_arr;
		}
	}
?>
