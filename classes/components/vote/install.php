<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = Array();
	$INFO['name'] = "vote";
	$INFO['config'] = "0";
	$INFO['default_method'] = "poll";
	$INFO['default_method_admin'] = "lists";
	$INFO['func_perms'] = "Группы прав на функционал модуля";
	$INFO['func_perms/add_poll'] = "Права на добавление опросов";
	$INFO['func_perms/edit_poll'] = "Права на редактирование опросов";
	$INFO['func_perms/del_poll'] = "Права на удаление опросов";
	$INFO['func_perms/poll'] = "Права на просмотр опросов и их результатов";
	$INFO['func_perms/post'] = "Права на на участии в опросах";

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = array();
	$COMPONENTS[] = "./classes/components/vote/admin.php";
	$COMPONENTS[] = "./classes/components/vote/class.php";
	$COMPONENTS[] = "./classes/components/vote/customAdmin.php";
	$COMPONENTS[] = "./classes/components/vote/customCommon.php";
	$COMPONENTS[] = "./classes/components/vote/customMacros.php";
	$COMPONENTS[] = "./classes/components/vote/events.php";
	$COMPONENTS[] = "./classes/components/vote/handlers.php";
	$COMPONENTS[] = "./classes/components/vote/i18n.en.php";
	$COMPONENTS[] = "./classes/components/vote/i18n.php";
	$COMPONENTS[] = "./classes/components/vote/install.php";
	$COMPONENTS[] = "./classes/components/vote/includes.php";
	$COMPONENTS[] = "./classes/components/vote/lang.en.php";
	$COMPONENTS[] = "./classes/components/vote/lang.php";
	$COMPONENTS[] = "./classes/components/vote/macros.php";
	$COMPONENTS[] = "./classes/components/vote/permissions.php";
?>
