<?php
	$classes = [
		'discount'	=> [
			dirname(__FILE__) . '/classes/discounts/discount.php'
		],

		'discountModificator'	=> [
			dirname(__FILE__) . '/classes/discounts/discountModificator.php'
		],

		'itemDiscountRule'	=> [
			dirname(__FILE__) . '/classes/discounts/iItemDiscountRule.php'
		],

		'orderDiscountRule'	=> [
			dirname(__FILE__) . '/classes/discounts/iOrderDiscountRule.php'
		],

		'discountRule'	=> [
			dirname(__FILE__) . '/classes/discounts/discountRule.php'
		],

		'order'	=> [
			dirname(__FILE__) . '/classes/orders/order.php'
		],

		'orderItem'	=> [
			dirname(__FILE__) . '/classes/orders/orderItem.php'
		],

		'iOrderNumber'	=> [
			dirname(__FILE__) . '/classes/orders/number/iOrderNumber.php'
		],

		'delivery'	=> [
			dirname(__FILE__) . '/classes/delivery/delivery.php'
		],

		'payment'	=> [
			dirname(__FILE__) . '/classes/payment/payment.php'
		],

		'customer'	=> [
			dirname(__FILE__) . '/classes/customer/customer.php'
		],

		'emarketTop'	=> [
			dirname(__FILE__) . '/classes/stat/emarketTop.php'
		],

		'UmiCms\Classes\Components\Emarket\Orders\Calculator'	=> [
			dirname(__FILE__) . '/classes/orders/Calculator.class.php'
		]
	];
