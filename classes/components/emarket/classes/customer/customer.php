<?php

	/**
	 * Класс покупателя интернет магазина.
	 * Его источником данных может быть одна из сущностей системы:
	 * 1) Пользователь одноименного модуля;
	 * 2) Незарегистрированный покупатель модуля "Интернет магазин";
	 */
	class customer extends umiObjectProxy {
		/**
		 * @var int $defaultExpiration время жизни незарегистрированного покупателя по умолчанию, в секундах
		 */
		public static $defaultExpiration = 2678400;
		/**
		 * @var bool|int является ли покупатель авторизованным пользователем
		 */
		protected $isAuth;

		/**
		 * Возвращает покупателя
		 * @param bool $nocache не использовать внутренний кеш
		 * @param bool|int $customerId идентификатор сущности-источника данных для покупателя
		 * @return customer
		 */
		public static function get($nocache = false, $customerId = false) {
			static $customer;

			if (!$nocache && !is_null($customer)) {
				return $customer;
			}

			$objects = umiObjectsCollection::getInstance();
			$permissions = permissionsCollection::getInstance();

			if ($permissions->isAuth()) {
				if (false !== $customerId) {
					$object = $objects->getObject($customerId);
				} else {
					$userId = $permissions->getUserId();
					$object = $objects->getObject($userId);
				}
			} else {
				$object = self::getCustomerId(false, $customerId);
			}

			if ($object instanceof iUmiObject) {
				$customer = new customer($object);
				$customer->tryMerge();
				return $customer;
			}
		}

		/**
		 * Конструктор
		 * @param iUmiObject $object сущность-источник данных для покупателя
		 */
		public function __construct(iUmiObject $object) {
			$permissions = permissionsCollection::getInstance();
			$userId = $permissions->getUserId();
			$guestId = permissionsCollection::getGuestId();
			$this->isAuth = ($userId == $guestId) ? false : $userId;
			parent::__construct($object);
		}

		/**
		 * Авторизован ли покупатель
		 * @return bool
		 */
		public function isUser() {
			return (bool) $this->isAuth;
		}

		/**
		 * Осуществляет попытку перенести заказы незарегистрированного покупателя
		 * к заказам авторизованного пользователя
		 */
		public function tryMerge() {
			if (!$this->isUser() || !getCookie('customer-id')) {
				return;
			}

			$guestCustomer = self::getCustomerId();

			if (!$guestCustomer instanceof iUmiObject) {
				return;
			}

			$this->merge($guestCustomer);
		}

		/**
		 * Переносит заказы незарегистрированного покупателя к заказам авторизованного пользователя.
		 * Объект незарегистрированного покупателя после этого будет уничтожен
		 * @param umiObject $customer объект незарегистрированного покупателя
		 * @throws selectorException
		 */
		public function merge(umiObject $customer) {
			$permissions = permissionsCollection::getInstance();
			$userId = $permissions->getUserId();

			if ($customer->getId() == $userId) {
				return;
			}

			$cmsController = cmsController::getInstance();
			$domain = $cmsController->getCurrentDomain();
			$domainId = $domain->getId();

			$sel = new selector('objects');
			$sel->types('object-type')->name('emarket', 'order');
			$sel->where('customer_id')->equals($customer->getId());
			$sel->where('domain_id')->equals($domainId);
			$sel->option('load-all-props')->value(true);
			$sel->order('id')->desc();

			/**
			 * @var iUmiObject|iUmiEntinty $order
			 */
			foreach ($sel as $order) {
				if (!$order->getValue('status_id')) {
					$this->mergeBasket($order);
					continue;
				}

				$order->setValue('customer_id', $userId);
				$order->commit();
			}

			if (!defined('UMICMS_CLI_MODE') || !UMICMS_CLI_MODE) {
				setcookie('customer-id', 0, 1, '/');
			}

			$customer->delete();
		}

		/**
		 * Переносит товарные наименования из корзины незарегистрированного покупателя
		 * в заказ зарегистрированного пользователя.
		 * В конце операции удаляет корзину незарегистрированного покупателя.
		 * @param umiObject $guestBasket корзина незарегистрированного покупателя.
		 */
		protected function mergeBasket(umiObject $guestBasket) {
			/**
			 * @var emarket $emarket
			 */
			$emarket = cmsController::getInstance()->getModule('emarket');
			$orderItems = $guestBasket->getValue('order_items');

			if (!is_array($orderItems) || count($orderItems) == 0) {
				return;
			}

			$userBasket = $emarket->getBasketOrder(false);

			if (!$userBasket instanceof order) {
				return;
			}

			foreach ($orderItems as $orderItemId) {
				/**
				 * @var orderItem $orderItem
				 */
				$orderItem = orderItem::get($orderItemId);
				if ($orderItem instanceof orderItem) {
					$userBasket->appendItem($orderItem);
				}
			}

			$userBasket->commit();
			$guestBasket->delete();
		}

		/**
		 * Удаляет время жизни покупателя.
		 */
		public function freeze() {
			/**
			 * @var umiObjectsExpiration $expiration
			 */
			$expiration = umiObjectsExpiration::getInstance();
			$expiration->clear($this->getId());
		}

		/**
		 * Возвращает идентификатор сущности-источника данных для покупателя
		 * @return string
		 */
		public function __toString() {
			return (string) $this->getId();
		}

		/**
		 * Возвращает идентификатор незарегистрированного покупателя, представляющего
		 * собой текущего покупателя.
		 * При необходимости создает нового незарегистрированного покупателя.
		 * Обновляет время жизни незарегистрированного покупателя.
		 * @param bool $noCookie не используется
		 * @param bool|int $customerId идентификатор незарегистрированного покупателя
		 * @return umiObject
		 */
		protected static function getCustomerId($noCookie = false, $customerId = false) {
			if (false === $customerId) {
				$customerId = (int) getCookie('customer-id');
			}

			/* @var $customer umiObject */
			$customer = selector::get('object')->id($customerId);
			$umiTypesHelper = umiTypesHelper::getInstance();
			$customerTypeId = $umiTypesHelper->getObjectTypeIdByGuid('emarket-customer');
			$userTypeId = $umiTypesHelper->getObjectTypeIdByGuid('users-user');
			$customerIsUser = ($customer instanceof iUmiObject) && ($customer->getTypeId() == $userTypeId);

			if ($customer instanceof iUmiObject != false) {
				if ($customer->getTypeId() != $customerTypeId && !$customerIsUser) {
					$customer = null;
				}
			} else {
				$customer = null;
			}

			if (!$customer) {
				$customerId = self::createGuestCustomer();
				$customer = selector::get('object')->id($customerId);
			}

			if (!$customerId) {
				$customerId = self::createGuestCustomer();
			}

			if ((!defined('UMICMS_CLI_MODE') || !UMICMS_CLI_MODE) && (!$customerIsUser)) {
				setcookie('customer-id', $customerId, (time() + self::$defaultExpiration), '/');
				$_COOKIE['customer-id'] = $customerId;
			}

			/**
			 * @var umiObjectsExpiration $expiration
			 */
			$expiration = umiObjectsExpiration::getInstance();

			if (!$customerIsUser) {
				$expiration->update($customerId, self::$defaultExpiration);
			}

			return $customer;
		}

		/**
		 * Создает незарегистрированного покупателя и возвращает его идентификатор
		 * @return int
		 * @throws coreException
		 */
		protected static function createGuestCustomer() {
			$objectTypes = umiObjectTypesCollection::getInstance();
			$objects = umiObjectsCollection::getInstance();
			$objectTypeId = $objectTypes->getTypeIdByHierarchyTypeName('emarket', 'customer');
			$customerId = $objects->addObject(getServer('REMOTE_ADDR'), $objectTypeId);
			$customer = $objects->getObject($customerId);
			$customer->setOwnerId($objects->getObjectIdByGUID('system-guest'));
			$customer->commit();

			$expiration = umiObjectsExpiration::getInstance();
			$expiration->add($customerId, self::$defaultExpiration);

			return $customerId;
		}

		/**
		 * Возвращает идентификатор последнего незавершенного заказа пользователя на конкретном домене.
		 * @param int $domainId идентификатор домена искомого незавешенного заказа
		 * @return int|bool
		 */
		public function getLastOrder($domainId) {
			if ($orderId = getSession('admin-editing-order')) {
				return $orderId;
			}

			$lastOrders = $this->getValue('last_order');

			if (!is_array($lastOrders) || count($lastOrders) == 0) {
				return false;
			}

			foreach ($lastOrders as $lastOrder) {
				if (!isset($lastOrder['float']) || $lastOrder['float'] != $domainId) {
					continue;
				}

				$orderId = $lastOrder['rel'];
				$order = order::get($orderId);

				if (!$order instanceof order) {
					continue;
				}

				$orderStatus = order::getCodeByStatus($order->getValue('status_id'));
				$paymentStatus = order::getCodeByStatus($order->getValue('payment_status_id'));

				if (
						!$orderStatus || $orderStatus == 'executing' ||
						(
								$orderStatus == 'payment' && $paymentStatus == 'initialized'
						)
				) {
					return $orderId;
				}
			}

			return false;
		}

		/**
		 * Добавляет заказ в список последних заказов покупателя
		 * @param int $orderId идентификатор заказа
		 * @param int $domainId идентификатор домена, на котором оформлялся заказ
		 */
		public function setLastOrder($orderId, $domainId) {
			$lastOrders = $this->getValue('last_order');
			$matchDomain = false;

			foreach ($lastOrders as &$lastOrder) {

				if (!isset($lastOrder['float']) || $lastOrder['float'] != $domainId) {
					continue;
				}

				$lastOrder['rel'] = $orderId;
				$matchDomain = true;
			}

			if (!$matchDomain) {
				$lastOrders[] = [
						"rel" => $orderId,
						"float" => $domainId
				];
			}

			$this->object->setValue('last_order', $lastOrders);
			$this->object->commit();
		}
	}

?>
