<?php
	/** Адрес сервера push уведомлений */
	define('PUSH_SERVER', 'http://push.umi-cms.ru/');

	/** Подключение реализаций скидок */
	discount::init();

	/** Автозагрузчик классов для интеграции с Яндекс.Маркет */
	require_once dirname(__FILE__) . '/../../system/utils/yandex_market/vendor/autoload.php';
