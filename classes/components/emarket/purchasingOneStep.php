<?php
	/**
	 * Класс функционала оформления заказа в 1 шаг
	 */
	class EmarketPurchasingOneStep {
		/**
		 * @var emarket $module
		 */
		public $module;

		/**
		 * Возвращает данные всех этапов оформления заказа для построения единой формы
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 * @throws publicException
		 */
		public function purchasing_one_step($template = 'onestep'){
			/**
			 * @var emarket|EmarketPurchasingOneStep|EmarketPurchasingStagesSteps $module
			 */
			$module = $this->module;
			list($purchasing_one_step) = emarket::loadTemplates(
				"emarket/onestep/{$template}.tpl",
				'purchasing_one_step'
			);

			$order = $this->module->getBasketOrder();

			if ($order->isEmpty()) {
				throw new publicException('%error-market-empty-basket%');
			}

			$result = array();

			if (!permissionsCollection::getInstance()->isAuth()){
				$result['onestep']['customer'] = $this->personalInfo($template);
				if (emarket::isXSLTResultMode()) {
					$result['onestep']['customer']['@id'] = customer::get()->getId();
				}
			}

			$result['onestep']['delivery'] = $module->customerDeliveryList($template);
			$result['onestep']['delivery_choose'] = $module->renderDeliveryList($order, $template);
			$result['onestep']['payment'] = $module->paymentsList($template);

			return emarket::parseTemplate($purchasing_one_step, $result);
		}

		/**
		 * Возвращает список адресов пользователя
		 * @param string $template имя шаблона (для tpl)
		 * @return mixed
		 */
		public function customerDeliveryList($template = 'default') {
			/**
			 * @var emarket|EmarketPurchasingOneStep|EmarketPurchasingStagesSteps $module
			 */
			$module = $this->module;
			$order = $module->getBasketOrder();
			return $module->renderDeliveryAddressesList($order, $template);
		}

		/**
		 * Выводит информацию для построения формы заполнения данных покупателя
		 * @param string $template имя шаблона (для tpl)
		 * @return string
		 */
		public function personalInfo($template = 'onestep') {
			if (permissionsCollection::getInstance()->isAuth()){
				return '';
			}

			$customerId = customer::get()->getId();
			$cmsController = cmsController::getInstance();
			/**
			 * @var DataForms $data
			 */
			$data = $cmsController->getModule('data');

			return $data->getEditForm($customerId, '../../emarket/customer/' . $template);
		}

		/**
		 * Возвращает список способ оплаты
		 * @param string $template имя шаблона (для tpl)
		 * @return array|mixed
		 */
		public function paymentsList($template = 'onestep') {
			/**
			 * @var emarket|EmarketPurchasingOneStep|EmarketPurchasingStagesSteps $module
			 */
			$module = $this->module;
			$order = $module->getBasketOrder(false);
			list($tpl_block, $tpl_item) = emarket::loadTemplates(
				"emarket/payment/{$template}.tpl",
				'payment_block',
				'payment_item'
			);

			$paymentsIds = payment::getList();
			$items = array();
			$currentPaymentId = $order->getValue('payment_id');
			$umiObjectsCollection = umiObjectsCollection::getInstance();

			foreach ($paymentsIds as $paymentId) {
				$payment = payment::get($paymentId, $order);

				/**
				 * @var payment $payment
				 */
				if ($payment->validate($order) == false) {
					continue;
				}

				$paymentObject = $payment->getObject();
				$paymentTypeId = $paymentObject->getValue('payment_type_id');
				$paymentTypeName = $umiObjectsCollection->getObject($paymentTypeId)->getValue('class_name');

				if ($paymentTypeName == 'social') {
					continue;
				}

				$item = array(
					'attribute:id' => $paymentObject->getId(),
					'attribute:name' => $paymentObject->getName(),
					'attribute:type-name' => $paymentTypeName,
					'xlink:href' => $paymentObject->xlink
				);

				if ($paymentId == $currentPaymentId) {
					$item['attribute:active'] = 'active';
				}

				$items[] = emarket::parseTemplate($tpl_item, $item, false, $paymentObject->getId());
			}

			if ($tpl_block && !emarket::isXSLTResultMode()) {
				return emarket::parseTemplate($tpl_block, array(
					'items' => $items
				));
			}

			return array(
				'items' => array(
					'nodes:item'=> $items
				)
			);
		}

		/**
		 * Принимает данные от единой формы оформления заказа и оформляет заказ.
		 * @throws coreException
		 */
		public function saveInfo() {
			/**
			 * @var emarket $module
			 */
			$module = $this->module;
			$order = $module->getBasketOrder(false);

			$cmsController = cmsController::getInstance();
			$data = $cmsController->getModule('data');
			/**
			 * @var DataForms $data
			 */
			$data->saveEditedObject(customer::get()->getId(), false, true);

			$addressId = getRequest('delivery-address');

			if ($addressId == 'new') {
				$collection = umiObjectsCollection::getInstance();
				$types = umiObjectTypesCollection::getInstance();
				$typeId = $types->getTypeIdByHierarchyTypeName("emarket", "delivery_address");
				$customer = customer::get();
				$addressId = $collection->addObject("Address for customer #" . $customer->getId(), $typeId);
				$data->saveEditedObjectWithIgnorePermissions($addressId, true, true);
				$customer->delivery_addresses = array_merge($customer->delivery_addresses, array($addressId));
			}

			$order->delivery_address = $addressId;

			$deliveryId = getRequest('delivery-id');

			if ($deliveryId){
				/**
				 * @var delivery $delivery
				 */
				$delivery = delivery::get($deliveryId);
				$deliveryPrice = (float) $delivery->getDeliveryPrice($order);
				$order->setValue('delivery_id', $deliveryId);
				$order->setValue('delivery_price', $deliveryPrice);
			}

			$order->setValue('payment_id', getRequest('payment-id'));
			$order->refresh();

			$paymentId = getRequest('payment-id');

			if (!$paymentId) {
				$module->errorNewMessage(getLabel('error-emarket-choose-payment'));
				$module->errorPanic();
			}

			$payment = payment::get($paymentId, $order);

			if ($payment instanceof payment) {
				$paymentName = $payment->getCodeName();
				$url = "{$module->pre_lang}/" . $cmsController->getUrlPrefix() . "emarket/purchase/payment/{$paymentName}/";
			} else {
				$url = "{$module->pre_lang}/" . $cmsController->getUrlPrefix() . "emarket/cart/";
			}

			$module->redirect($url);
		}
	}
?>