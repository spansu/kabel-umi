<?php

class common_createorder extends def_module {

    public $module;

    public function __construct() {
        parent::__construct();
    }

    public function co_ajaxSearchItem($search_string, $domain_id = 1, $lang_id = 1) {
        $connection = ConnectionPool::getInstance()->getConnection();

        $search_string = (string) trim($search_string);
        $search_string = $connection->escape($search_string);
        $search_string_arr = explode(' ', $search_string);
        foreach ($search_string_arr as $key => $value) {
            if (mb_strlen($value) < 3) {
                unset($search_string_arr[$key]);
            }
        }

        $result = array();
        $h = umiHierarchy::getInstance();

        if (is_array($search_string_arr) && count($search_string_arr)) {
            $limit = 20;
            $except_field_ids = '';

            $qresult = $connection->queryResult("SELECT id FROM cms3_object_fields WHERE field_type_id IN "
                    . "(SELECT id FROM cms3_object_field_types "
                    . "WHERE data_type IN ('file','img_file','password','video_file','swf_file'))");
            $qresult->setFetchType(IQueryResult::FETCH_ROW);
            if ($qresult->length()) {
                foreach ($qresult as $row) {
                    $except_field_ids .= $row[0] . ', ';
                }
            }

            if ($except_field_ids > 2) {
                $except_field_ids = substr($except_field_ids, 0, -2);
            } else {
                $except_field_ids = '0';
            }

            $qresult = $connection->queryResult("SELECT id FROM cms3_hierarchy_types WHERE name = 'catalog' AND ext = 'object'");
            $qresult->setFetchType(IQueryResult::FETCH_ROW);
            $row = $qresult->fetch();

            if (isset($row[0])) {
                $catalog_htype_id = intval($row[0]);
                if ($catalog_htype_id > 0) {
                    $catalog_otypes_ids = '';
                    $qresult = $connection->queryResult("SELECT id FROM cms3_object_types WHERE hierarchy_type_id = '{$catalog_htype_id}'");
                    $qresult->setFetchType(IQueryResult::FETCH_ROW);
                    if ($qresult->length()) {
                        foreach ($qresult as $row) {
                            $catalog_otypes_ids .= $row[0] . ', ';
                        }
                    }

                    if (strlen($catalog_otypes_ids) > 2) {
                        $catalog_otypes_ids = substr($catalog_otypes_ids, 0, -2);

                        $query = "SELECT ch.id, co.name, ch.rel, ch.is_active FROM cms3_objects co "
                                . "INNER JOIN cms3_hierarchy ch ON co.id = ch.obj_id "
                                . "INNER JOIN cms3_object_content coc ON co.id = coc.obj_id "
                                . "WHERE co.type_id IN ({$catalog_otypes_ids}) AND coc.field_id NOT IN({$except_field_ids}) "
                                . "AND ch.domain_id = {$domain_id} AND ch.lang_id = {$lang_id} AND (";

                        $cols = array('co.name', 'coc.varchar_val', 'coc.text_val');
                        foreach ($cols as $col) {
                            foreach ($search_string_arr as $value) {
                                $query .= $col . " LIKE '%{$value}%' OR ";
                            }
                        }

                        $query = substr($query, 0, -4) . ') ';

                        $query .= " AND ch.is_deleted = 0 GROUP BY co.id ORDER BY ch.is_active DESC LIMIT 0,{$limit}";

                        $rels = array();
                        $qresult = $connection->queryResult($query);
                        $qresult->setFetchType(IQueryResult::FETCH_ROW);

                        if ($qresult->length()) {
                            foreach ($qresult as $row) {
                                $rel = $row[2];
                                $rels[0] = '';
                                if ($rel > 0 && !isset($rels[$rel])) {
                                    $qresult2 = $connection->queryResult("SELECT name FROM cms3_objects WHERE id = (SELECT obj_id FROM cms3_hierarchy WHERE id = {$rel})");
                                    $qresult2->setFetchType(IQueryResult::FETCH_ROW);
                                    $row2 = $qresult2->fetch();
                                    if (is_array($row2)) {
                                        $rels[$rel] = $row2[0];
                                    }
                                }

                                if ($row[3]) {
                                    $link = $h->getPathById($row[0]);
                                } else {
                                    $link = "/admin/catalog/edit/{$row[0]}/";
                                }

                                $result[] = array(
                                    'attribute:id' => $row[0],
                                    'attribute:name' => $row[1],
                                    'attribute:link' => $link,
                                    'attribute:parent_name' => $rels[$rel],
                                    'attribute:is_active' => $row[3]
                                );
                            }
                        }
                    }
                }
            }
        }

        return array('total' => count($result), 'items' => array('nodes:item' => $result));
    }

    public function co_ajaxSearchItemOptions($id) {
        $id = intval($id);
        $connection = ConnectionPool::getInstance()->getConnection();

        $mainConfiguration = mainConfiguration::getInstance();
        $optioned_groups = $mainConfiguration->get('modules', 'emarket.order-types.optioned.groups');

        $result = array();
        $original_price = 0;
        $controller = cmsController::getInstance();
        $oc = umiObjectsCollection::getInstance();

        $qresult = $connection->queryResult("SELECT id FROM cms3_object_field_types WHERE data_type = 'optioned'");
        $qresult->setFetchType(IQueryResult::FETCH_ROW);
        $row = $qresult->fetch();
        if (is_array($row)) {
            $optioned_type_id = $row[0];

            if (is_array($optioned_groups) && count($optioned_groups)) {
                $qresult = $connection->queryResult("SELECT obj_id FROM cms3_hierarchy WHERE id = {$id}");
                $qresult->setFetchType(IQueryResult::FETCH_ROW);
                $row = $qresult->fetch();

                if (isset($row[0])) {
                    $obj_id = $row[0];
                    $obj = umiObjectsCollection::getInstance()->getObject($obj_id);
                    if (is_object($obj)) {
                        $original_price = $obj->getValue('price');

                        $props = array();
                        foreach ($optioned_groups as $group_name) {
                            $arr = $obj->getPropGroupByName($group_name);
                            if (is_array($arr)) {
                                foreach ($arr as $prop_id) {
                                    $qresult = $connection->queryResult("SELECT field_type_id FROM cms3_object_fields WHERE id = " . $prop_id);
                                    $qresult->setFetchType(IQueryResult::FETCH_ROW);
                                    $row = $qresult->fetch();
                                    if (isset($row[0]) && $row[0] == $optioned_type_id && !in_array($prop_id, $props)) {
                                        $props[] = $prop_id;
                                    }
                                }
                            }
                        }

                        foreach ($props as $prop) {
                            $prop_obj = $obj->getPropById($prop);
                            if (is_object($prop_obj)) {
                                $prop_values = $prop_obj->getValue();
                                if (is_array($prop_values) && count($prop_values)) {
                                    $guide_items = array();
                                    foreach ($prop_values as $value) {
                                        $value_obj = $oc->getObject($value['rel']);
                                        if (is_object($value_obj)) {
                                            if (!isset($value['float'])) {
                                                $value['float'] = 0;
                                            }
                                            $guide_items[] = array(
                                                'attribute:id' => $value_obj->getId(),
                                                'attribute:title' => $value_obj->getName(),
                                                'attribute:float' => $value['float']
                                            );
                                        }
                                    }
                                }

                                if (!count($guide_items)) {
                                    continue;
                                }

                                $property_title = $prop_obj->getTitle();
                                $property_name = $prop_obj->getName();

                                $result[] = array(
                                    'attribute:name' => $property_name,
                                    'attribute:title' => $property_title,
                                    'guide' => array('nodes:item' => $guide_items)
                                );
                            }
                        }
                    }
                }
            }
        }

        return array('total' => count($result), 'items' => array('nodes:item' => $result), 'original_price' => $original_price);
    }

    public function co_AddItem($mode = 'new') {
        $result = 0;
        $item_id = (int) getRequest('item_id');
        $is_new_oitem = getRequest('is_new_item');

        if ($item_id > 0 || $is_new_oitem) {
            // --- костыль для корректного определения скидки на пользователя
            $cmsController = cmsController::getInstance();
            $oldCurrentMode = $cmsController->getCurrentMode();
            $cmsController->setCurrentMode('admin');
            if ($mode == 'new') {
                $_REQUEST['no-redirect'] = 1;
                if (!$is_new_oitem) {
                    $this->module->basket('put', 'element', $item_id);
                } else {
                    $new_oitem_arr = getRequest('new_oitem');

                    if (isset($new_oitem_arr['name']) && isset($new_oitem_arr['price'])) {
                        $new_oitem_obj = $this->co_createOrderItem($new_oitem_arr['name'], intval(getRequest('amount')), $new_oitem_arr['price']);
                        if (is_object($new_oitem_obj)) {
                            $order = $this->module->getBasketOrder();
                            if (is_object($order)) {
                                $order->appendItem($new_oitem_obj);
                                $order->refresh();
                            }
                        }
                    }
                }
                $result = 1;
            } else {
                
                $order = order::get($mode);
                if (is_object($order)) {
                    if ($is_new_oitem) {
                        $new_oitem_arr = getRequest('new_oitem');

                        $new_oitem_obj = $this->co_createOrderItem($new_oitem_arr['name'], intval(getRequest('amount')), $new_oitem_arr['price']);
                        if (is_object($new_oitem_obj)) {
                            if (is_object($order)) {
                                $order->appendItem($new_oitem_obj);
                                $order->refresh();
                                $result = 1;
                            }
                        }
                    } else {
                        $itemId = $item_id;
                        $amount = (int) getRequest('amount');
                        $options = getRequest('options');
                        $itemType = 'element';

                        $newElement = false;

                        $orderItem = $this->module->getBasketItem($itemId, false);
                        if (!$orderItem) {
                            $orderItem = $this->module->getBasketItem($itemId);
                            $newElement = true;
                        }

                        if (!$orderItem) {
                            $result = 0;
                        }

                        if (is_array($options)) {
                            // Get all orderItems
                            $orderItems = $order->getItems();

                            foreach ($orderItems as $tOrderItem) {
                                if (!$tOrderItem instanceOf optionedOrderItem) {
                                    $itemOptions = null;
                                    $tOrderItem = null;
                                    continue;
                                }

                                $itemOptions = $tOrderItem->getOptions();

                                if (sizeof($itemOptions) != sizeof($options)) {
                                    $itemOptions = null;
                                    $tOrderItem = null;
                                    continue;
                                }

                                if ($tOrderItem->getItemElement()->id != $orderItem->getItemElement()->id) {
                                    $itemOptions = null;
                                    $tOrderItem = null;
                                    continue;
                                }

                                foreach ($options as $optionName => $optionId) {
                                    $itemOption = getArrayKey($itemOptions, $optionName);

                                    if (getArrayKey($itemOption, 'option-id') != $optionId) {
                                        $tOrderItem = null;
                                        continue 2;
                                    }
                                }
                                break;
                            }

                            if (!isset($tOrderItem) || is_null($tOrderItem)) {
                                $tOrderItem = orderItem::create($itemId);
                                $order->appendItem($tOrderItem);
                                if ($newElement) {
                                    $orderItem->remove();
                                }
                            }

                            if ($tOrderItem instanceof optionedOrderItem) {
                                foreach ($options as $optionName => $optionId) {
                                    if ($optionId) {
                                        $tOrderItem->appendOption($optionName, $optionId);
                                    } else {
                                        $tOrderItem->removeOption($optionName);
                                    }
                                }
                            }

                            if ($tOrderItem) {
                                $orderItem = $tOrderItem;
                            }
                        }

                        $amount = $amount ? $amount : ($orderItem->getAmount() + 1);
                        $orderItem->setAmount($amount ? $amount : 1);
                        $orderItem->refresh();
                        $order->appendItem($orderItem);

                        $order->refresh();
                        $result = 1;
                    }
                }
            }

            $order = $this->module->getBasketOrder();
            $order->refresh();
            $order->commit();

            $cmsController->setCurrentMode($oldCurrentMode);
        }
        return array('result' => $result);
    }

    public function co_ajaxEditItem($id, $amount) {
        $result = 0;
        if ($id > 0) {
            // --- костыль для корректного определения скидки на пользователя
            $cmsController = cmsController::getInstance();
            $oldCurrentMode = $cmsController->getCurrentMode();
            $cmsController->setCurrentMode('admin');

            $_REQUEST['amount'] = $amount;
            $this->module->basket('put', 'item', $id);

            /* $item = orderItem::get($id);
              if (is_object($item)) {
              $item->setAmount(intval($amount));
              $item->refresh();
              $result = 1;
              } */
            $result = 1;

            $order = $this->module->getBasketOrder();
            $order->refresh();
            $order->commit();

            $cmsController->setCurrentMode($oldCurrentMode);
        }
        return array('result' => $result);
    }

    public function co_ajaxRemoveItem($id) {
        $result = 0;
        if ($id > 0) {
            // --- костыль для корректного определения скидки на пользователя
            $cmsController = cmsController::getInstance();
            $oldCurrentMode = $cmsController->getCurrentMode();
            $cmsController->setCurrentMode('admin');

            $_REQUEST['no-redirect'] = 1;
            $this->module->basket('remove', 'item', $id);
            $result = 1;

            $order = $this->module->getBasketOrder();
            $order->refresh();
            $order->commit();

            $cmsController->setCurrentMode($oldCurrentMode);
        }
        return array('result' => $result);
    }

    public function co_ajaxSetDelivery($delivery_id, $delivery_price = 0, $force_update = false) {
        $order = $this->module->getBasketOrder();

        // --- костыль для корректного определения скидки на пользователя
        $cmsController = cmsController::getInstance();
        $oldCurrentMode = $cmsController->getCurrentMode();
        $cmsController->setCurrentMode('admin');

        if ($delivery_id == 0) {
            $order->setValue('delivery_id', 0);
            $order->setValue('delivery_price', 0);
        } else {
            $delivery = delivery::get($delivery_id);
            if (is_object(($delivery))) {
                if (!$force_update) {
                    $delivery_price = (float) $delivery->getDeliveryPrice($order);
                } else {
                    $delivery_price = $delivery_price;
                }

                $order->setValue('delivery_id', $delivery_id);
                $order->setValue('delivery_price', $delivery_price);

                $oEventPoint = new umiEventPoint("emarket_set_delivery_price");
                $oEventPoint->setMode("after");
                $oEventPoint->addRef("order", $order);
                $oEventPoint->addRef("delivery_id", $delivery_id);
                $oEventPoint->addRef("delivery_price", $delivery_price);
                $this->setEventPoint($oEventPoint);
            }
        }

        $order->refresh();
        $order->commit();

        $cmsController->setCurrentMode($oldCurrentMode);

        return array('delivery_price' => $delivery_price);
    }

    // не работает, т.к. до момента оформления заказа система не дает назначить скидку, во время order->refresh() скидка обнуляется.
    public function co_ajaxSetItemDiscount($item_id, $discount_value = 0) {
        $discount_value = floatval(trim($discount_value));
        $order = $this->module->getBasketOrder();

        // --- костыль для корректного определения скидки на пользователя
        $cmsController = cmsController::getInstance();
        $oldCurrentMode = $cmsController->getCurrentMode();
        $cmsController->setCurrentMode('admin');

        $item = $order->getItem($item_id);

        if (!$item instanceof orderItem || $item->getDiscountValue() == $discount_value) {
            return 0;
        } else {
            $item->setDiscountValue($discount_value);
            $item->commit();

            $order->refresh();
            $order->commit();
        }

        $cmsController->setCurrentMode($oldCurrentMode);

        return 1;
    }

    public function co_ajaxSetCustomerId($customer_id = 0) {
        if (!$customer_id) {
            $customer_id = permissionsCollection::getInstance()->getUserId();
        }

        $order = $this->module->getBasketOrder();
        $customer_obj = umiObjectsCollection::getInstance()->getObject($customer_id);
        if (is_object($customer_obj)) {
            $customer_obj->setOwnerId($customer_id);
            $customer_obj->commit();
        }

        $order->setValue('customer_id', $customer_id);
        // --- костыль для корректного определения скидки на пользователя
        $cmsController = cmsController::getInstance();
        $oldCurrentMode = $cmsController->getCurrentMode();
        $cmsController->setCurrentMode('admin');
        $order->commit();
        if (isset($_REQUEST['data']) && is_array($_REQUEST['data'])) {
            $_new_REQUEST = array();
            $_new_REQUEST[$order->getId()] = $order->getId();
            foreach ($_REQUEST['data'] as $key => $value) {
                $_new_REQUEST[$key] = $value;
            }
            $_REQUEST['data'] = $_new_REQUEST;
        } else {
            $_REQUEST['data'][$order->getId()] = $order->getId();
        }
        $order->refresh();

        $items = $order->getItems();
        foreach ($items as $item) {
            $item->refresh();
            $item->commit();
        }

        $order->commit();

        $order->refresh();
        $cmsController->setCurrentMode($oldCurrentMode);
        // ---

        return array('result' => '1');
    }

    public function co_getNewCustomerTypeId() {
        if (class_exists('umiTypesHelper')) {
            $umiTypesHelper = umiTypesHelper::getInstance();
            $customerTypeId = $umiTypesHelper->getObjectTypeIdByGuid('emarket-customer');
        } else {
            $connection = ConnectionPool::getInstance()->getConnection();

            $qresult = $connection->queryResult("SELECT id FROM cms3_object_types WHERE hierarchy_type_id = (SELECT id FROM cms3_hierarchy_types WHERE name = 'emarket' AND ext = 'customer')");
            $qresult->setFetchType(IQueryResult::FETCH_ROW);
            $row = $qresult->fetch();
            if (isset($row[0])) {
                $customerTypeId = $row[0];
            }
        }
        return $customerTypeId;
    }

    public function co_ajaxSetGuideItem($mode, $param) {
        $connection = ConnectionPool::getInstance()->getConnection();

        $controller = cmsController::getInstance();
        $oc = umiObjectsCollection::getInstance();

        $guide_item_id = 0;

        if ($mode == 'add') {
            $data_key = 'new';
            $type_id = $param;
        } else {
            $data_key = $param;
            $guide_item = $oc->getObject($param);
            if (is_object($guide_item)) {
                $type_id = $guide_item->getTypeId();
            }
        }

        $name = trim(getRequest('name'));
        if (strlen($name) == 0 && $type_id > 0) {
            $qresult = $connection->queryResult("SELECT guid FROM cms3_object_types WHERE id = {$type_id}");
            $qresult->setFetchType(IQueryResult::FETCH_ROW);
            $row = $qresult->fetch();

            if (isset($row[0]) && strlen($row[0]) > 0) {
                switch ($row[0]) {
                    case 'emarket-deliveryaddress':
                        $name = '';
                        foreach ($_REQUEST['data'][$data_key] as $key => $value) {
                            if ($value != '') {
                                if (($key == 'country' || $key == 'city') && is_numeric($value)) {
                                    $country_obj = $oc->getObject($value);
                                    if (is_object($country_obj)) {
                                        $name .= $country_obj->getName() . ', ';
                                    }
                                } else if ($key == 'order_comments') {
                                    $name .= ' (' . $value . '), ';
                                } else if ($key == 'street') {
                                    $name .= 'ул. ' . $value . ', ';
                                } else if ($key == 'house') {
                                    $name .= 'д. ' . $value . ', ';
                                } else if ($key == 'flat') {
                                    $name .= 'кв. ' . $value . ', ';
                                } else {
                                    $name .= $value . ', ';
                                }
                            }
                        }
                        $name = substr($name, 0, -2);
                        break;
                    case 'emarket-legalperson':
                        if (isset($_REQUEST['data'][$data_key]['name'])) {
                            $name = trim($_REQUEST['data'][$data_key]['name']);
                        }
                        break;
                    case 'emarket-customer':
                        foreach ($_REQUEST['data'][$data_key] as $key => $value) {
                            if ($value != '' && in_array($key, array('fname', 'lname', 'father_name'))) {
                                $name .= $value . ' ';
                            }
                        }
                        $name = substr($name, 0, -1);
                        break;
                    case 'users-user':
                        foreach ($_REQUEST['data'][$data_key] as $key => $value) {
                            if ($value != '' && in_array($key, array('login', 'fname', 'lname', 'father_name'))) {
                                $name .= $value . ' ';
                            }
                        }
                        $name = substr($name, 0, -1);
                        break;
                    default:
                        foreach ($_REQUEST['data'][$data_key] as $key => $value) {
                            if ($value != '') {
                                $name = $value;
                                break;
                            }
                        }
                        break;
                }
            }
        }

        if ($mode == 'add') {
            $guide_item_id = $oc->addObject($name, $type_id);
            $dataModule = $controller->getModule("data");
            if ($dataModule) {
                $dataModule->saveEditedObjectWithIgnorePermissions($guide_item_id, true, true);
            }
        } else {
            if (is_object($guide_item)) {
                $guide_item_id = $param;
                if (strlen($name) > 0) {
                    $guide_item->setName($name);
                }

                $dataModule = $controller->getModule("data");
                if ($dataModule) {
                    $dataModule->saveEditedObjectWithIgnorePermissions($guide_item_id, false, true);
                }
            }
        }
        return array('guide_item_id' => $guide_item_id, 'name' => $name);
    }

    public function co_ajaxSetCustomer($mode, $param) {
        $success = 0;
        $result = $this->co_ajaxSetGuideItem($mode, $param);
        if (isset($result['guide_item_id'])) {
            $customer_id = $result['guide_item_id'];
            $result = $this->co_ajaxSetCustomerId($customer_id);
            if (isset($result['result'])) {
                $customer_obj = umiObjectsCollection::getInstance()->getObject($customer_id);
                if (is_object($customer_obj)) {
                    if (isset($_REQUEST['customer_data_value']) && is_array($_REQUEST['customer_data_value'])) {
                        foreach ($_REQUEST['customer_data_value'] as $prop_name => $prop_data) {
                            $customer_obj->setValue($prop_name, $prop_data);
                        }
                        $customer_obj->commit();
                    }
                }

                $order = $this->module->getBasketOrder();
                if (is_object($order)) {
                    if (isset($_REQUEST['extended_guide_items']) && is_array($_REQUEST['extended_guide_items'])) {
                        foreach ($_REQUEST['extended_guide_items'] as $property => $value) {
                            if ($property == 'delivery_addresses') {
                                $property = 'delivery_address';
                            }
                            if ($property == 'legal_persons') {
                                $property = 'legal_person';
                            }
                            $order->setValue($property, $value);
                        }
                        $order->commit();
                    }
                }
                $success = $result['result'];
            }
        }

        return array('result' => $success);
    }

    public function co_ajaxGetCustomerInfo() {
        $connection = ConnectionPool::getInstance()->getConnection();

        $result = array();
        $order = $this->module->getBasketOrder();
        $customer_id = $order->getValue('customer_id');

        $qresult = $connection->queryResult("SELECT name FROM cms3_objects WHERE id = {$customer_id}");
        $qresult->setFetchType(IQueryResult::FETCH_ROW);
        $row = $qresult->fetch();
        if (is_array($row) && count($row)) {
            $props = array('lname', 'fname', 'father_name', 'e-mail', 'email');
            $customer_obj = umiObjectsCollection::getInstance()->getObject($customer_id);
            if (is_object($customer_obj)) {
                $name = $row[0];

                foreach ($props as $prop) {
                    $value = $customer_obj->getValue($prop);
                    $matches = array();
                    if (strlen($value)) {
                        $name .= ' ' . $value;
                    }
                }

                $name = implode(' ', array_unique(explode(' ', $name)));

                $result = array('result' => array('id' => $customer_id, 'name' => $name));
            }
        }
        return $result;
    }

    public function co_ajaxSearchCustomer($search_string) {
        $connection = ConnectionPool::getInstance()->getConnection();

        $search_string = (string) trim($search_string);
        $connection->escape($search_string);

        $search_string_arr = explode(' ', $search_string);
        foreach ($search_string_arr as $key => $value) {
            if (mb_strlen($value) < 3) {
                unset($search_string_arr[$key]);
            }
        }

        $result = array();

        if (is_array($search_string_arr) && count($search_string_arr)) {
            $limit = 20;

            $except_field_ids = '';

            $qresult = $connection->queryResult("SELECT id FROM cms3_object_fields WHERE field_type_id IN "
                    . "(SELECT id FROM cms3_object_field_types "
                    . "WHERE data_type IN ('file','img_file','password','video_file','swf_file'))");
            $qresult->setFetchType(IQueryResult::FETCH_ROW);

            if ($qresult->length()) {
                foreach ($qresult as $row) {
                    $except_field_ids .= $row[0] . ', ';
                }
            }

            if ($except_field_ids > 2) {
                $except_field_ids = substr($except_field_ids, 0, -2);
            } else {
                $except_field_ids = '0';
            }

            $catalog_htype_ids = '';
            $qresult = $connection->queryResult("SELECT id FROM cms3_hierarchy_types WHERE (name = 'emarket' AND ext = 'customer') OR (name = 'users' AND ext = 'user')");
            $qresult->setFetchType(IQueryResult::FETCH_ROW);

            if ($qresult->length()) {
                foreach ($qresult as $row) {
                    $catalog_htype_ids[] = $row[0];
                }
            }

            $catalog_htype_ids = implode(', ', $catalog_htype_ids);

            if (strlen($catalog_htype_ids) > 0) {
                $catalog_otypes_ids = '';
                $types_guids = array();
                $qresult = $connection->queryResult("SELECT id, guid FROM cms3_object_types WHERE hierarchy_type_id IN ({$catalog_htype_ids})");
                $qresult->setFetchType(IQueryResult::FETCH_ROW);

                if ($qresult->length()) {
                    foreach ($qresult as $row) {
                        $catalog_otypes_ids[] = $row[0];
                        $types_guids[$row[0]] = $row[1];
                    }
                }

                $catalog_otypes_ids = implode(', ', $catalog_otypes_ids);

                if (strlen($catalog_otypes_ids) > 2) {
                    $query = "SELECT co.id, co.name, co.type_id FROM cms3_objects co "
                            . "INNER JOIN cms3_object_content coc ON co.id = coc.obj_id "
                            . "WHERE co.type_id IN ({$catalog_otypes_ids}) AND coc.field_id NOT IN({$except_field_ids}) AND (";

                    $cols = array('co.name', 'coc.varchar_val', 'coc.text_val');
                    foreach ($cols as $col) {
                        foreach ($search_string_arr as $value) {
                            $query .= $col . " LIKE '%{$value}%' OR ";
                        }
                    }
                    $query = substr($query, 0, -4) . ')';

                    $query .= " GROUP BY co.id ORDER BY co.name ASC LIMIT 0,{$limit}";

                    $props = array('lname', 'fname', 'father_name', 'e-mail', 'email');
                    $oc = umiObjectsCollection::getInstance();

                    $qresult = $connection->queryResult($query);
                    $qresult->setFetchType(IQueryResult::FETCH_ROW);
                    if ($qresult->length()) {
                        foreach ($qresult as $row) {
                            $obj = $oc->getObject($row[0]);
                            if (is_object($obj)) {
                                $name = '';
                                foreach ($props as $prop) {
                                    $value = $obj->getValue($prop);
                                    if (strlen($value)) {
                                        $name .= $value . ' ';
                                    }
                                }
                                $name = substr($name, 0, -1);

                                if (!strlen($name)) {
                                    $name = $row[1];
                                } else {
                                    $name = implode(' ', array_unique(explode(' ', $name)));
                                }

                                $status = 'зарегистрирован';
                                if ($types_guids[$row[2]] == 'emarket-customer') {
                                    $status = 'не зарегистрирован';
                                }


                                $result[] = array(
                                    'attribute:id' => $row[0],
                                    'attribute:status' => $status,
                                    'attribute:name' => $name,
                                );
                            }
                        }
                    }
                }
            }
        }

        return array('total' => count($result), 'items' => array('nodes:item' => $result));
    }

    public function co_getExtendedFields($prop_name) {
        $connection = ConnectionPool::getInstance()->getConnection();
        $order = $this->module->getBasketOrder();
        $customer_id = $order->getValue('customer_id');
        $selected_id = 0;
        if ($prop_name == 'delivery_addresses') {
            $selected_id = $order->getValue('delivery_address');
        } else {
            $selected_id = $order->getValue('legal_person');
        }

        $result = array();
        $result['field']['attribute:name'] = $prop_name;
        $result['field']['attribute:input_name'] = 'data[' . $customer_id . '][' . $prop_name . '][]';
        $result['field']['values'] = array();

        if ($customer_id > 0) {
            $customer_obj = umiObjectsCollection::getInstance()->getObject($customer_id);
            if (is_object($customer_obj)) {
                $prop_obj = $customer_obj->getPropByName($prop_name);
                $values = $customer_obj->getValue($prop_name);

                if (is_object($prop_obj)) {
                    $field_obj = $prop_obj->getField();
                    if (is_object($field_obj)) {
                        $result['field']['attribute:type-id'] = intval($field_obj->getGuideId());
                        $result['field']['attribute:title'] = $field_obj->getTitle();
                    }
                } else {
                    return 0;
                }

                if (is_array($values)) {
                    foreach ($values as $value_id) {
                        $qresult = $connection->queryResult("SELECT name FROM cms3_objects WHERE id = {$value_id}");
                        $qresult->setFetchType(IQueryResult::FETCH_ROW);
                        $row = $qresult->fetch();
                        if (is_array($row)) {
                            $temp = array('attribute:id' => $value_id, 'node:text' => $row[0]);

                            if ($value_id == $selected_id) {
                                $temp['attribute:selected'] = 'selected';
                            }

                            $result['field']['values']['nodes:item'][] = $temp;
                        }
                    }
                }
            }
        }

        $result['field']['attribute:selected'] = $selected_id;

        return $result;
    }

    public function co_delGuideItem($id) {
        $success = 0;
        if (umiObjectsCollection::getInstance()->delObject($id)) {
            $success = 1;
        }
        return array('result' => $success);
    }

    public function co_checkOrderBeforeSubmit() {
        $errors = array();
        $result = 1;
        $order = $this->module->getBasketOrder();

        $customer_id = $order->getValue('customer_id');

        if (!$customer_id || $customer_id == permissionsCollection::getInstance()->getUserId()) {
            $errors[] = 'Вы не выбрали покупателя';
        }

        if (!is_array($order->getItems()) || !count($order->getItems())) {
            $errors[] = 'Вы не добавили товары в заказ';
        }

        if (count($errors)) {
            $result = 0;
        }

        return array('result' => $result, 'errors' => array('nodes:item' => $errors));
    }

    public function co_createOrderItem($name, $amount = 1, $price = 0) {
        $connection = ConnectionPool::getInstance()->getConnection();

        $oc = umiObjectsCollection::getInstance();
        $h = umiHierarchy::getInstance();

        $dummy_page_obj = null;
        $dummy_page_id = 0;
        $domain_id = cmsController::getInstance()->getCurrentDomain()->getId();
        $lang_id = cmsController::getInstance()->getCurrentLang()->getId();

        $qresult = $connection->queryResult("SELECT id FROM cms3_hierarchy WHERE rel = 0 AND alt_name = 'dummycreateorder' AND domain_id = {$domain_id} AND lang_id = {$lang_id} AND is_deleted = 0");
        $qresult->setFetchType(IQueryResult::FETCH_ROW);
        $row = $qresult->fetch();
        if (!is_array($row)) {
            $qresult = $connection->queryResult("SELECT id FROM cms3_hierarchy_types WHERE name = 'catalog' AND ext = 'object'");
            $qresult->setFetchType(IQueryResult::FETCH_ROW);
            $row = $qresult->fetch();
            if (is_array($row)) {
                $dummy_page_id = $h->addElement(0, $row[0], 'Прототип товара/услуги (не удалять!)', 'dummycreateorder');
                permissionsCollection::getInstance()->setDefaultPermissions($dummy_page_id);
            }
        } else {
            $dummy_page_id = $row[0];
        }

        if ($dummy_page_id > 0) {
            $dummy_page_obj = $h->getElement($dummy_page_id, 1);
            $dummy_page_obj->setIsActive(1);
            $dummy_page_obj->commit();

            if (is_object($dummy_page_obj)) {
                $objectTypeId = umiObjectTypesCollection::getInstance()->getBaseType('emarket', 'order_item');
                $new_oitem_obj_id = $oc->addObject($name, $objectTypeId);
                $new_oitem_obj = $oc->getObject($new_oitem_obj_id);
                if (is_object($new_oitem_obj)) {
                    $new_oitem_obj->setValue('item_price', $price);
                    $new_oitem_obj->setValue('item_amount', $amount);
                    $new_oitem_obj->setValue('item_link', array($dummy_page_obj));
                    $new_oitem_obj->setValue('item_total_original_price', $price * $amount);
                    $new_oitem_obj->setValue('item_total_price', $price * $amount);
                    $new_oitem_obj->commit();

                    return orderItem::get($new_oitem_obj_id);
                }
            }

            return $new_oitem_obj;
        }

        return 0;
    }

    public function co_getEditForm($obj_id) {
        $object = umiObjectsCollection::getInstance()->getObject($obj_id);

        if (is_object($object)) {
            return $this->co_getCreateForm($object->getTypeId(), $object);
        }
        return "";
    }

    public function co_getCreateForm($type_id = 0, $object = null) {
        $connection = ConnectionPool::getInstance()->getConnection();

        $cc = cmsController::getInstance();
        $data = $cc->getModule('data');
        if (is_object($data)) {
            $block_arr = array();

            $groups_arr = $data->getTypeFieldGroups($type_id);

            if (!is_array($groups_arr)) {
                return "";
            }

            $groups = Array();
            foreach ($groups_arr as $group) {

                if (!$group->getIsActive()) {
                    continue;
                }
                if ($group->getName() == "locks") {
                    continue;
                }

                if (!$group->getIsActive() || !$group->getIsVisible()) {
                    continue;
                }

                $line_arr = Array();

                $fields_arr = $group->getFields();

                $fields = Array();

                foreach ($fields_arr as $field) {
                    if (!$field->getIsVisible() && !$all)
                        continue;
                    if ($field->getIsSystem())
                        continue;

                    $field_type_id = $field->getFieldTypeId();
                    $field_type = umiFieldTypesCollection::getInstance()->getFieldType($field_type_id);
                    $data_type = $field_type->getDataType();

                    if (($field->getName() == 'delivery_addresses' || $field->getName() == 'legal_persons') && $data_type == 'relation' && $field_type->getIsMultiple()) {
                        $is_multiple = $field_type->getIsMultiple();
                        $fieldName = $field->getName();

                        $res = array();

                        $res['attribute:type'] = $data_type;
                        $res['attribute:id'] = $field->getId();

                        if ($field->getIsRequired()) {
                            $res['attribute:required'] = 'required';
                        }
                        if ($tip = $field->getTip()) {
                            $res['attribute:tip'] = $tip;
                        }

                        $res['attribute:name'] = $fieldName;
                        $res['attribute:title'] = $field->getTitle();
                        $res['attribute:tip'] = $field->getTip();
                        $res['attribute:field_id'] = $field->getId();
                        if ($is_multiple)
                            $res['attribute:multiple'] = "multiple";

                        $guide_id = $field->getGuideId();

                        if ($guide_id) {
                            $res['attribute:type-id'] = $guide_id;

                            $guide = umiObjectTypesCollection::getInstance()->getType($guide_id);
                            if ($guide instanceof umiObjectType) {
                                if ($guide->getIsPublic()) {
                                    $res['attribute:public-guide'] = true;
                                }
                            }
                        }

                        $res['attribute:input_name'] = "data[new][{$fieldName}]" . (($is_multiple) ? "[]" : "");

                        $values = array();

                        if (is_object($object)) {
                            $_items = $object->getValue($fieldName);
                            foreach ($_items as $obj_id) {
                                $qresult = $connection->queryResult("SELECT name FROM cms3_objects WHERE id = {$obj_id}");
                                $qresult->setFetchType(IQueryResult::FETCH_ROW);
                                $row = $qresult->fetch();
                                if (is_array($row)) {
                                    $values[] = array(
                                        'attribute:id' => $obj_id,
                                        'attribute:xlink:href' => "uobject://{$obj_id}",
                                        'attribute:selected' => "selected",
                                        'node:text' => $row[0]
                                    );
                                }
                            }
                        }

                        $res['values']['nodes:item'] = $values;

                        $fields[] = $res;
                    } else {
                        $dataForms = new DataForms();
                        $fields[] = $this->renderEditField('notemplate', $field, $object);
                    }
                }

                if (empty($fields))
                    continue;

                $line_arr['attribute:name'] = $group->getName();
                $line_arr['attribute:title'] = $group->getTitle();

                $line_arr['nodes:field'] = $line_arr['void:fields'] = $fields;

                $groups[] = $line_arr;
            }

            $block_arr['nodes:group'] = $block_arr['void:groups'] = $groups;
            return $block_arr;
        }
    }

    public function co_transformXML() {
        $stylesheetfile = getRequest('xslpath');
        $require = getRequest('require');
        $result = file_get_contents("udata://" . $require);
        $response = "";
        $proc = new XSLTProcessor;
        if ($proc->hasExsltSupport()) {
            $xslDoc = new DOMDocument();
            $xslDoc->load(CURRENT_WORKING_DIR . "/" . $stylesheetfile);
            $fpath = CURRENT_WORKING_DIR . "/files/";
            $fname = md5(time()) . ".xml";
            file_put_contents($fpath . $fname, $result);
            $xmlDoc = new DOMDocument();
            $xmlDoc->load($fpath . $fname);
            $proc->importStylesheet($xslDoc);
            $response = $proc->transformToXML($xmlDoc);
            unlink($fpath . $fname);
        }
        $buffer = outputBuffer::current();
        $buffer->charset('utf-8');
        $buffer->contentType('text/html');
        $buffer->clear();
        $buffer->push(str_replace("<?xml version=\"1.0\"?>", "", $response));
        $buffer->end();
    }

    public function co_getRequestVar($varname) {
        if (isset($_REQUEST[$varname])) {
            return $_REQUEST[$varname];
        }
    }

    /* Дубликаты функций модуля data, так как в указанном модуле они стали приватными.
     * А этого можно было бы избежать, если бы стандартный data/getCreateForm и getEditForm корректно возвращал юридические адреса и адреса доставки (сейчас возвращает абсолютно все данные этих справочников, что неправильно) */
    private function renderEditField($template, umiField $field, $object = false) {
        $field_type_id = $field->getFieldTypeId();
        $field_type = umiFieldTypesCollection::getInstance()->getFieldType($field_type_id);
        $is_multiple = $field_type->getIsMultiple();
        $data_type = $field_type->getDataType();

        switch ($data_type) {
            case "counter":
            case "int": {
                    $res = $this->renderEditFieldInt($field, $is_multiple, $object, $template);
                    $data_type = "int";
                    break;
                }
            case "link_to_object_type": {
                    $res = $this->renderEditFieldInt($field, $is_multiple, $object, $template);
                    break;
                }
            case "price": {
                    $res = $this->renderEditFieldPrice($field, $is_multiple, $object, $template);
                    break;
                }
            case "float": {
                    $res = $this->renderEditFieldInt($field, $is_multiple, $object, $template);
                    break;
                }
            case "color":
            case "string": {
                    $res = $this->renderEditFieldString($field, $is_multiple, $object, $template);
                    break;
                }
            case "date": {
                    $res = $this->renderEditFieldDate($field, $is_multiple, $object, $template);
                    break;
                }
            case "password": {
                    $res = $this->renderEditFieldPassword($field, $is_multiple, $object, $template);
                    break;
                }
            case "relation": {
                    $res = $this->renderEditFieldRelation($field, $is_multiple, $object, $template);
                    break;
                }
            case "symlink": {
                    $res = $this->renderEditFieldSymlink($field, $is_multiple, $object, $template);
                    break;
                }
            case "img_file": {
                    $res = $this->renderEditFieldImageFile($field, $is_multiple, $object, $template);
                    break;
                }
            case "video_file" :
            case "swf_file":
            case "file": {
                    $res = $this->renderEditFieldFile($field, $is_multiple, $object, $template);
                    break;
                }
            case "text": {
                    $res = $this->renderEditFieldText($field, $is_multiple, $object, $template);
                    break;
                }
            case "wysiwyg": {
                    $res = $this->renderEditFieldWYSIWYG($field, $is_multiple, $object, $template);
                    break;
                }
            case "boolean": {
                    $res = $this->renderEditFieldBoolean($field, $is_multiple, $object, $template);
                    break;
                }
            case "tags": {
                    $res = $this->renderEditFieldTags($field, $is_multiple, $object, $template);
                    break;
                }
            case "optioned": {
                    $res = $this->renderEditFieldOptioned($field, $is_multiple, $object, $template);
                    break;
                }
            case "multiple_image": {
                    $res = $this->renderEditFieldMultipleImage($field, $object, $template);
                    break;
                }
            default: {
                    $res = "";
                }
        }

        if ($res === false) {
            return null;
        }

        if (!data::isXSLTResultMode()) {
            $required = $field->getIsRequired();
            $res = data::parseTemplate($res, array(
                        'required' => ($required ? 'required' : ''),
                        'required_asteriks' => ($required ? '*' : '')
            ));
        } else {
            $res['attribute:type'] = $data_type;
            $res['attribute:id'] = $field->getId();

            if ($field->getIsRequired()) {
                $res['attribute:required'] = 'required';
            }
            if ($tip = $field->getTip()) {
                $res['attribute:tip'] = $tip;
            }
        }

        return $res;
    }

    /**
     * Выводит данные поля типов "строка" и "цвет" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldString(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_string");
        $block_arr = Array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();
        $block_arr['node:value'] = ($object) ? $object->getValue($field->getName()) : "";

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }


        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типа "дата" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldDate(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block_string, $template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_string", "reflection_field_date");

        if (!$template_block) {
            $template_block = $template_block_string;
        }

        $block_arr = array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();

        $block_arr['node:value'] = "";
        $block_arr['attribute:timestamp'] = 0;

        if ($object) {
            $oDate = $object->getValue($field->getName());

            if ($oDate instanceof umiDate) {
                $block_arr['attribute:timestamp'] = $oDate->getDateTimeStamp();
                $block_arr['node:value'] = $oDate->getFormattedDate();
                $block_arr['attribute:formatted-date'] = $oDate->getFormattedDate("d.m.Y H:i");
            }

            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типа "простой текст" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldText(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_text");
        $block_arr = array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();
        $block_arr['node:value'] = ($object) ? $object->getValue($field->getName()) : "";

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типа "HTML-текст" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldWYSIWYG(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_wysiwyg");
        $block_arr = array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();
        $block_arr['node:value'] = ($object) ? $object->getValue($field->getName()) : "";

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типов "Число", "Счетчик", "Число с точкой" и "Ссылка на объектный тип" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldInt(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_int");
        $block_arr = Array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();
        $block_arr['node:value'] = ($object) ? $object->getValue($field->getName()) : "";

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типа "Цена" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $isMultiply может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldPrice(iUmiField $field, $isMultiply, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($templateBlock) = data::loadTemplates("data/reflection/{$template}", "reflection_field_int");
        $data = Array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $fieldName = $field->getName();
        $data['attribute:name'] = $fieldName;
        $data['attribute:title'] = $field->getTitle();
        $data['attribute:tip'] = $field->getTip();
        $data['attribute:field_id'] = $field->getId();
        $data['attribute:is_important'] = $field->isImportant();

        /**
         * @var emarket $eMarket
         */
        $eMarket = cmsController::getInstance()->getModule('emarket');

        if ($eMarket instanceof def_module) {
            $currency = $eMarket->getDefaultCurrency();
            /**
             * @var iUmiObject|iUmiEntinty $currency
             */
            if ($currency instanceof iUmiObject) {
                $data['attribute:currency_id'] = $currency->getId();
                $data['attribute:currency_code'] = $currency->getValue('codename');
                $data['attribute:currency_prefix'] = $currency->getValue('prefix');
                $data['attribute:currency_suffix'] = $currency->getValue('suffix');
            }
        }
        $data['node:value'] = '';

        if ($object instanceof iUmiObject) {
            $data['node:value'] = $object->getValue($field->getName());
            $data['void:object_id'] = $object->getId();
        }

        $data['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$fieldName}]" : "data[new][{$fieldName}]";
        return data::parseTemplate($templateBlock, $data);
    }

    /**
     * Выводит данные поля типа "Кнопка-флажок" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldBoolean(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_boolean");
        $block_arr = array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();
        $block_arr['attribute:checked'] = "";
        $block_arr['node:value'] = 0;

        if ($object) {
            $block_arr['node:value'] = (int) $object->getValue($field->getName());
            $block_arr['attribute:checked'] = (bool) $object->getValue($field->getName()) ? "checked" : "";
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типа "Пароль" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldPassword(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_password");
        $block_arr = Array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();
        $block_arr['node:value'] = "";

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}][]" : "data[new][{$field_name}][]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типов "Выпадающие список" и "Выпадающие список со множественным выбором"
     * для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldRelation(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        $controller = cmsController::getInstance();
        $objects = umiObjectsCollection::getInstance();
        $guide_items = array();

        $fieldName = $field->getName();
        if ($guide_id = $field->getGuideId()) {
            switch (true) {
                case ($controller->getCurrentMode() == "admin" && $object instanceof iUmiObject): {
                        if ($object->getTypeGUID() == 'users-user') {
                            $guide_items = $objects->getGuidedItems($guide_id);
                            break;
                        }
                        $val = $object->getValue($fieldName);
                        if (!$val) {
                            break;
                        }
                        if (!is_array($val)) {
                            $val = Array($val);
                        }
                        foreach ($val as $item_id) {
                            $item = $objects->getObject($item_id);
                            if (!$item instanceof iUmiObject) {
                                continue;
                            }
                            $guide_items[$item_id] = $item->getName();
                        }
                        break;
                    }
                default: {
                        $guide_items = $objects->getGuidedItems($guide_id);
                    }
            }
        }

        list(
                $template_block, $template_block_line, $template_block_line_a, $template_mul_block, $template_mul_block_line, $template_mul_block_line_a
                ) = data::loadTemplates("data/reflection/{$template}", "reflection_field_relation", "reflection_field_relation_option", "reflection_field_relation_option_a", "reflection_field_multiple_relation", "reflection_field_multiple_relation_option", "reflection_field_multiple_relation_option_a"
        );

        $block_arr = array();
        $value = $object ? $object->getValue($fieldName) : array();

        if ($fieldName == 'publish_status' && $controller->getCurrentMode() != "admin") {
            return "";
        }
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $block_arr['attribute:name'] = $fieldName;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();

        if ($is_multiple) {
            $block_arr['attribute:multiple'] = "multiple";
        }

        if ($guide_id) {
            $block_arr['attribute:type-id'] = $guide_id;
            $guide = umiObjectTypesCollection::getInstance()->getType($guide_id);

            if ($guide instanceof umiObjectType) {
                if ($guide->getIsPublic()) {
                    $block_arr['attribute:public-guide'] = true;
                }
            }
        }

        $options = ($is_multiple) ? Array() : "";
        foreach ($guide_items as $item_id => $item_name) {
            $item_object = $objects->getObject($item_id);
            if (!is_object($item_object)) {
                continue;
            }

            if ($is_multiple) {
                $selected = (in_array($item_id, $value)) ? " selected" : "";
            } else {
                $selected = ($item_id == $value) ? " selected" : "";
            }

            if ($item_object->getValue("is_hidden") && !$selected) {
                continue;
            }

            if (!$template_block_line && !data::isXSLTResultMode()) {
                $options .= "<option value=\"{$item_id}\"{$selected}>{$item_name}</option>\n";
            } else {
                $line_arr = Array();
                $line_arr['attribute:id'] = $item_id;
                $line_arr['xlink:href'] = "uobject://" . $item_id;
                $line_arr['node:name'] = $item_name;

                if ($selected) {
                    $line_arr['attribute:selected'] = "selected";
                    $line = $is_multiple ? $template_mul_block_line_a : $template_block_line_a;
                } else {
                    $line = $is_multiple ? $template_mul_block_line : $template_block_line;
                }

                $options[] = data::parseTemplate($line, $line_arr, false, $item_id);
            }
        }

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['subnodes:values'] = $block_arr['void:options'] = $options;
        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$fieldName}]" . (($is_multiple) ? "[]" : "") : "data[new][{$fieldName}]" . (($is_multiple) ? "[]" : "");
        return data::parseTemplate((($is_multiple) ? $template_mul_block : $template_block), $block_arr);
    }

    /**
     * Выводит данные поля типа "Ссылка не дерево" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldSymlink(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates(
                        "data/reflection/{$template}", "reflection_field_relation", "reflection_field_relation_option", "reflection_field_relation_option_a", "reflection_field_multiple_relation", "reflection_field_multiple_relation_option", "reflection_field_multiple_relation_option_a"
        );
        $block_arr = array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();

        $options = ($object) ? $object->getValue($field->getName()) : array();

        $block_arr['subnodes:values'] = $block_arr['void:options'] = $options;
        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}][]" : "data[new][{$field_name}][]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типа "Теги" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldTags(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_tags");
        $block_arr = array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();
        $value = ($object) ? $object->getValue($field->getName()) : "";

        if (is_array($value)) {
            $value = implode(", ", $value);
        }

        $block_arr['node:value'] = $value;

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типа "Составное" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldOptioned(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        $block_arr = array();
        $objects = umiObjectsCollection::getInstance();
        $hierarchy = umiHierarchy::getInstance();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();

        if ($guideId = $field->getGuideId()) {
            $block_arr['attribute:guide-id'] = $guideId;
        }

        $inputName = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        $values = ($object) ? $object->getValue($field->getName()) : Array();

        $values_arr = Array();
        foreach ($values as $value) {
            $value_arr = Array();
            foreach ($value as $type => $subValue) {
                switch ($type) {
                    case "tree": {
                            $element = $hierarchy->getElement($subValue);
                            if ($element instanceof umiHierarchyElement) {
                                $value_arr['page'] = $element;
                            }
                            break;
                        }

                    case "rel": {
                            $object = $objects->getObject($subValue);
                            if ($object instanceof umiObject) {
                                $value_arr['object'] = $object;
                            }
                            break;
                        }

                    default: {
                            $value_arr['attribute:' . $type] = $subValue;
                            break;
                        }
                }
            }

            $values_arr[] = $value_arr;
        }

        $block_arr['values']['nodes:value'] = $values_arr;
        $block_arr['attribute:input_name'] = $inputName;
        return $block_arr;
    }

    /**
     * Выводит данные поля типа "Набор изображений" для построения формы редактирования
     * @param iUmiField $field
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldMultipleImage(iUmiField $field, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($fieldBlock, $valueBlock) = data::loadTemplates(
                        'data/reflection/' . $template, 'reflection_field_multiple_image_field', 'reflection_field_multiple_image_value'
        );

        $fieldNode = array();
        /* @var iUmiField|umiEntinty $field */
        $fieldName = $field->getName();
        $fieldNode['attribute:name'] = $fieldName;
        $fieldNode['attribute:title'] = $field->getTitle();
        $fieldNode['attribute:tip'] = $field->getTip();
        $fieldNode['attribute:maxsize'] = $this->module->getAllowedMaxFileSize("img");
        $fieldNode['attribute:field_id'] = $field->getId();
        $fieldNode['attribute:is_important'] = $field->isImportant();

        $inputName = 'data[new][' . $fieldName . ']';
        $values = array();

        if ($object instanceof umiEntinty && is_callable(array($object, 'getValue'))) {
            $inputName = 'data[' . $object->getId() . '][' . $fieldName . ']';
            $values = $object->getValue($fieldName);
        }

        $valuesNode = array();
        /* @var umiImageFile $value */
        foreach ($values as $key => $value) {
            if (!$value instanceof umiImageFile || $value->getIsBroken()) {
                continue;
            }

            $valueNode = array();
            $valueNode['attribute:id'] = (int) $key;
            $valueNode['attribute:alt'] = $value->getAlt();
            $valueNode['attribute:order'] = $value->getOrder();
            $valueNode['attribute:relative-path'] = $value->getFilePath(true);
            $destinationFolder = USER_IMAGES_PATH . "/data/";

            $info = getPathInfo($value->getFilePath(true));
            $info['dirname'] = '.' . $info['dirname'];

            $relativePath = substr($info['dirname'], strlen($destinationFolder)) . '/' . $info['basename'];

            if (substr($relativePath, 0, 1) == '/') {
                $relativePath = substr($relativePath, 1);
            }

            $valueNode['node:value'] = $relativePath;
            $valueNode['attribute:destination-folder'] = $info['dirname'];
            $valuesNode[] = data::parseTemplate($valueBlock, $valueNode);
        }

        $fieldNode['values']['nodes:value'] = $valuesNode;
        $fieldNode['attribute:input_name'] = $inputName;
        return data::parseTemplate($fieldBlock, $fieldNode);
    }

    /**
     * Выводит данные поля типа "Изображение" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldImageFile(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_img_file");

        $block_arr = Array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:maxsize'] = $this->module->getAllowedMaxFileSize("img");
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();

        $value = ($object) ? $object->getValue($field->getName()) : "";

        if ($value instanceof umiFile) {
            $block_arr['attribute:relative-path'] = $value->getFilePath(true);

            switch ($field_name) {
                case "menu_pic_ua" : {
                        $destination_folder = USER_IMAGES_PATH . "/cms/menu/";
                        break;
                    }
                case "header_pic" : {
                        $destination_folder = USER_IMAGES_PATH . "/cms/headers/";
                        break;
                    }
                case "menu_pic_a" : {
                        $destination_folder = USER_IMAGES_PATH . "/cms/menu/";
                        break;
                    }
                default : {
                        $destination_folder = USER_IMAGES_PATH . "/cms/data/";
                        break;
                    }
            }

            $info = getPathInfo($value->getFilePath(true));
            $info['dirname'] = '.' . $info['dirname'];

            $relative_path = substr($info['dirname'], strlen($destination_folder)) . "/" . $info['basename'];
            if (substr($relative_path, 0, 1) == "/") {
                $relative_path = substr($relative_path, 1);
            }
            $block_arr['node:value'] = $relative_path;

            $block_arr['attribute:destination-folder'] = $info['dirname'];
        } else {
            $block_arr['node:value'] = "";
            $folder_name = $field_name . '/';
            $general_name = USER_IMAGES_PATH . "/cms/";
            $destination_folder = $general_name . ((is_dir($general_name . $folder_name)) ? $folder_name : '');

            switch ($field_name) {
                case "menu_pic_ua" :
                    $pFolder = "menu";
                    break;

                case "header_pic" :
                    $pFolder = "headers";
                    break;

                case "menu_pic_a" :
                    $pFolder = "menu";
                    break;

                default :
                    $pFolder = "data";
                    break;
            }

            $block_arr['attribute:destination-folder'] = $destination_folder . $pFolder;
        }

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";
        return data::parseTemplate($template_block, $block_arr);
    }

    /**
     * Выводит данные поля типов "Файл", "Видео" и "Flash" для построения формы редактирования
     * @param iUmiField $field поле
     * @param bool $is_multiple может ли поле хранит набор значений
     * @param iUmiObject|bool $object объект, которому принадлежит поле
     * @param string $template имя шаблона для tpl шаблонизатора
     * @return mixed
     */
    private function renderEditFieldFile(iUmiField $field, $is_multiple, $object, $template) {
        /**
         * @var iUmiObject|iUmiEntinty|bool $object
         */
        list($template_block) = data::loadTemplates("data/reflection/{$template}", "reflection_field_file");

        $regexp = "|^" . CURRENT_WORKING_DIR . "|";

        $block_arr = Array();
        /**
         * @var iUmiField|iUmiEntinty $field
         */
        $field_name = $field->getName();
        $block_arr['attribute:name'] = $field_name;
        $block_arr['attribute:title'] = $field->getTitle();
        $block_arr['attribute:tip'] = $field->getTip();
        $block_arr['attribute:maxsize'] = $this->module->getAllowedMaxFileSize();
        $block_arr['attribute:field_id'] = $field->getId();
        $block_arr['attribute:is_important'] = $field->isImportant();
        /**
         * @var iUmiFile $value
         */
        $value = ($object) ? $object->getValue($field->getName()) : "";
        if ($value instanceof iUmiFile) {
            $block_arr['attribute:relative-path'] = $value->getFilePath(true);
            $block_arr['node:value'] = $value->getFilePath();
        } else {
            $block_arr['node:value'] = "";
        }

        if ($object) {
            $block_arr['void:object_id'] = $object->getId();
        }

        $block_arr['attribute:input_name'] = ($object) ? "data[" . $object->getId() . "][{$field_name}]" : "data[new][{$field_name}]";

        $folder_name = $field_name . '/';
        $general_name = "./files/";

        if ($value instanceof umiFile) {
            if ($value->getIsBroken() == false) {
                $value = false;
            }
        }
        if ($value instanceof iUmiFile) {
            $destination_folder = "." . preg_replace($regexp, "", $value->getDirName());
        } else {
            $destination_folder = $general_name . ((is_dir($general_name . $folder_name)) ? $folder_name : '');
        }

        $block_arr['attribute:destination-folder'] = $destination_folder;
        return data::parseTemplate($template_block, $block_arr);
    }
}
