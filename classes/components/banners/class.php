<?php
	/**
	 * Базовый класс модуля "Баннеры"
	 *
	 * Модуль управляет следующими сущностями:
	 *
	 * 1) Баннеры;
	 * 2) Места показа баннеров;
	 *
	 * Модуль отвечает за вставку того или иного баннера,
	 * в зависимости от окружения. По баннерам ведется
	 * статистика показов и кликов.
	 *
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_bannery/
	 */
	class banners extends def_module {
		/**
		 * @var array $arrVisibleBanners массив с банерами, доступными для отображения и связанными по месту
		 */
		static public $arrVisibleBanners = array();
		/**
		 * @var array $updatedBanners массив с банерами, отрисованными в рамках текущей сессии
		 */
		public $updatedBanners = array();

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();

			$mainConfiguration = mainConfiguration::getInstance();
			$this->isStaticCache = (bool) $mainConfiguration->get('cache', 'static.enabled');
			$this->per_page = 20;
			$this->disableUpdateOpt = (int) $mainConfiguration->get('modules', 'banners.disable-update-optimization');
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() == "admin") {
				$geoIpModule = $cmsController->getModule('geoip');

				if ($geoIpModule instanceof def_module) {
					/**
					 * @var banners|BannersAdmin $this
					 */
					$this->switchGroupsActivity('city_targeting', true);
				}

				$commonTabs = $this->getCommonTabs();

				if ($commonTabs) {
					$commonTabs->add('lists');
					$commonTabs->add('places');
				}

				$this->__loadLib("admin.php");
				$this->__implement("BannersAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("BannersCustomAdmin", true);
			} else {
				/**
				 * @var banners $this
				 */
				$this->__loadLib("macros.php");
				$this->__implement("BannersMacros");

				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("BannersCustomMacros", true);
			}

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("BannersCustomCommon", true);
		}

		/**
		 * Возвращает список баннеров, удовлетворяющий параметрам показа на страницах,
		 * активности, дате начала показа и месту.
		 * @param int $placeId идентификатор места показа баннера
		 * @param int $currentPageId ид страницы, на которой требуется показать баннер
		 * @param array $currentPageParentsIds массив идентификаторов страниц, родительских странице с ид $currentPageId
		 * @param bool $isRandom включен ли режим случайного отображения баннеров
		 * @return array
		 */
		public function getBanners($placeId, $currentPageId, array $currentPageParentsIds, $isRandom = false) {
			$banners = new selector('objects');
			$banners->types('hierarchy-type')->name('banners', 'banner');
			$banners->where("show_start_date")->less(time());
			$banners->where("is_active")->equals(1);
			$banners->where('place')->equals($placeId);
			$banners->option('or-mode')->field('view_pages');
			$banners->where('view_pages')->equals($currentPageId);
			$banners->where('view_pages')->equals($currentPageParentsIds);
			$banners->where('view_pages')->isnull(true);
			$banners->option('no-length')->value(true);
			$banners->option('load-all-props')->value(true);

			if ($isRandom) {
				$banners->order('rand');
			} else {
				$banners->order('id');
			}

			return $banners->result();
		}

		/**
		 * Проверяет находится ли текущая страница среди страниц, на
		 * которых не нужно показывать баннер.
		 * @param umiObject $banner объект баннера
		 * @param int $currentPageId ид текущей страницы
		 * @return bool
		 */
		public function checkNotAllowedPages(umiObject $banner, $currentPageId) {
			$notAllowedPages = $banner->getValue('not_view_pages');

			if (!is_array($notAllowedPages)) {
				$notAllowedPages = (array) $notAllowedPages;
			}

			$umiHierarchy = umiHierarchy::getInstance();
			$notAllowedPagesIds = array();

			if (count($notAllowedPages) > 0) {
				/* @var iUmiHierarchyElement|iUmiEntinty $notAllowedPage*/
				foreach ($notAllowedPages as $notAllowedPage) {
					if (!$notAllowedPage instanceof iUmiHierarchyElement) {
						continue;
					}
					$notAllowedPageId = $notAllowedPage->getId();
					$notAllowedPagesIds[] = $notAllowedPageId;
					$umiHierarchy->unloadElement($notAllowedPageId);
				}

			}
			return (in_array((int) $currentPageId, $notAllowedPagesIds)) ? false : true;
		}

		/**
		 * Проверяет сооответствие настроек времени и даты показа баннера
		 * текущему времени.
		 * @param umiObject $banner объект баннера
		 * @return bool
		 */
		public function checkTimeTargeting(umiObject $banner) {
			$timeTargetingEnabled = $banner->getValue('time_targeting_is_active');

			if (!$timeTargetingEnabled) {
				return true;
			}

			$timeRanges = new ranges();
			$targetingByMonth = $banner->getValue('time_targeting_by_month');

			if (strlen($targetingByMonth)) {
				$months = $timeRanges->get($targetingByMonth, 1);

				if (array_search((int) date("m"), $months) === false) {
					return false;
				}
			}

			$targetingByMonthDays = $banner->getValue('time_targeting_by_month_days');

			if (strlen($targetingByMonthDays)) {
				$monthDays = $timeRanges->get($targetingByMonthDays);
				if (array_search((int) date("d"), $monthDays) === false) {
					return false;
				}
			}

			$targetingByWeekDays = $banner->getValue('time_targeting_by_week_days');

			if (strlen($targetingByWeekDays)) {
				$weekDays = $timeRanges->get($targetingByWeekDays);
				if (array_search((int) date("w"), $weekDays) === false) {
					return false;
				}
			}

			$targetingByHours = $banner->getValue('time_targeting_by_hours');

			if (strlen($targetingByHours)) {
				$hours = $timeRanges->get($targetingByHours);
				if (array_search((int) date("G"), $hours) === false) {
					return false;
				}
			}

			return true;
		}

		/**
		 * Проверяет не истекло ли время показа баннера
		 * @param umiObject $banner объект баннера
		 * @return bool
		 */
		public function checkDateExpiration(umiObject $banner) {
			$showTillDate = $banner->getValue('show_till_date');

			if ($showTillDate instanceof umiDate && $showTillDate->timestamp) {

				if ($showTillDate->timestamp < $showTillDate->getCurrentTimeStamp()) {
					return false;
				}
			}

			return true;
		}

		/**
		 * Проверяет соотстветствие тегов баннера тегам текущего пользователя,
		 * текущей страницы и тегам, полученным путем разбора реферера.
		 * Если баннер соотвествует, то вычисляется его вес, он же и возвращается.
		 * Если баннер не сооответствует, то возвращается false.
		 * @param umiObject $banner объект баннера
		 * @param array $pageTags массив с тегами текущей страницы
		 * @param array $userTags массив с тегами текущего пользователя
		 * @param array $refererTags массив с тегами, полученными путем разбора реферера
		 * @return bool|int
		 */
		public function checkTagsAndCalculateWeight(umiObject $banner, array $pageTags, array $userTags, array $refererTags) {
			$weight = 1;
			$bannerPagesTags = $banner->getValue('tags');

			if (is_array($bannerPagesTags) && count($bannerPagesTags) > 0) {
				$commonTags = array_intersect($bannerPagesTags, $pageTags);
				if (count($commonTags) == 0) {
					return false;
				} else {
					$weight += count($commonTags);
				}
			}

			$bannerUserTags = $banner->getValue("user_tags");

			if (!is_array($bannerUserTags) || count($bannerUserTags) == 0) {
				return $weight;
			}

			$allowedTagsCounter = 0;
			foreach ($bannerUserTags as $bannerUserTag) {
				if (in_array($bannerUserTag, $userTags) || in_array($bannerUserTag, $refererTags)) {
					$allowedTagsCounter++;
				}
			}

			if ($allowedTagsCounter === 0) {
				return false;
			}

			return $weight + $allowedTagsCounter;
		}

		/**
		 * Проверяет не закончилось ли у баннера число показов
		 * @param umiObject $banner объект баннера
		 * @return bool
		 */
		public function checkViewCount(umiObject $banner) {
			$maxViews = (int) $banner->getValue('max_views');
			$viewsCount = (int) $banner->getValue('views_count');
			return ($maxViews <= 0 || $viewsCount <= $maxViews) ? true : false;
		}


		/**
		 * Проверяет соответствие города текущего пользователя, городу
		 * показа в баннере.
		 * @param umiObject $banner объект баннера
		 * @param bool|string $userCity город текущего пользователя
		 * @return bool
		 */
		public function checkCityTargeting(umiObject $banner, $userCity) {
			$cityTargetingEnabled = $banner->getValue('city_targeting_is_active');

			if (!$cityTargetingEnabled || is_bool($userCity)) {
				return true;
			}

			$bannerCity = $banner->getValue("city_targeting_city");

			if (!$bannerCity) {
				return true;
			}

			return ($bannerCity == $userCity) ? true : false;
		}

		/**
		 * Возвращает ссылку на редактирование объектов модуля "Баннеры"
		 * @param integer $object_id id сущности модуля
		 * @param string $object_type строковой идентификатор типа сущности
		 * @return array|bool
		 */
		public function getEditLink($object_id, $object_type) {
			switch ($object_type) {
				case "banner": {
					$link_add = $this->pre_lang . "/admin/banners/banner_add/";
					$link_edit = $this->pre_lang . "/admin/banners/banner_edit/{$object_id}/";
					return [$link_add, $link_edit];
				}
				default: {
					return false;
				}
			}
		}

		/**
		 * Валидирует текущую страниц и ее родителей по настройкам баннера.
		 * Возращает результат - показывать или нет.
		 * @param array $pages массив с ид страниц, на которых нужно показывать баннер
		 * @param array $notPages  массив с ид страниц, на которых не нужно показывать баннер
		 * @return bool
		 */
		public function checkIfValidParent($pages, $notPages) {
			$cmsController = cmsController::getInstance();
			$currentPageId = $cmsController->getCurrentElementId();

			if (count($notPages)) {
				/**
				 * @var iUmiEntinty $notPage
				 */
				foreach($notPages as $notPage) {
					if ($notPage->getId() == $currentPageId) {
						return false;
					}
				}
			}

			if (!is_array($pages) || sizeof($pages) == 0) {
				return true;
			}

			$parents = $this->getCurrentParents();

			foreach($pages as $page) {
				/**
				 * @var iUmiEntinty $page
				 */
				if (in_array($page->getId(), $parents)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Возвращает массив идентификаторов страниц, родительских текущей.
		 * @return Array
		 */
		public function getCurrentParents() {
			static $parents = false;

			if (is_array($parents)) {
				return $parents;
			}

			$cmsController = cmsController::getInstance();
			$iCurrPageId = $cmsController->getCurrentElementId();

			if ($iCurrPageId) {
				return $parents = umiHierarchy::getInstance()->getAllParents($iCurrPageId, true);
			}

			return Array();
		}

		/**
		 * Возвращает идентификатор места показа баннера по его названию
		 * @param string $placeName ид места показа баннера
		 * @return array|bool
		 */
		public function getPlaceId($placeName) {
			static $cache = Array();
			$placeName = (string) $placeName;

			if (isset($cache[$placeName])) {
				return array(0 => (int) $cache[$placeName]);
			}

			$places = new selector('objects');
			$places->types('hierarchy-type')->name('banners', 'place');
			$places->option('no-length')->value(true);
			$places->option('load-all-props')->value(true);
			$places = $places->result();

			if (count($places) == 0) {
				return false;
			}

			foreach ($places as $place) {
				if (!$place instanceof umiObject) {
					continue;
				}
				$cache[$place->getName()] = $place->getId();
			}

			if (isset($cache[$placeName])) {
				return array(0 => (int) $cache[$placeName]);
			}

			return false;
		}

		/**
		 * Возвращает ссылку на редактирование баннера
		 * @param integer $objectId ид баннера
		 * @return string
		 */
		public function getObjectEditLink($objectId, $type = false) {
			return $this->pre_lang . "/admin/banners/edit/" . $objectId . "/";
		}

		/**
		 * Отключает активность запрошенных баннеров, если у них истекло максимальное количество просмотров.
		 * @return void
		 */
		public function saveUpdates() {
			/**
			 * @var iUmiObject|iUmiEntinty $banner
			 */
			foreach($this->updatedBanners as $i => $banner) {
				if ($banner instanceof umiObject) {
					if ($banner->max_views && ($banner->views_count >= $banner->max_views)) {
						$banner->is_active = false;
					}

					$banner->commit();
					unset($this->updatedBanners[$i]);
				}
			}
		}

		/**
		 * Деструктор
		 */
		public function __destruct() {
			$this->saveUpdates();
		}
	};
?>