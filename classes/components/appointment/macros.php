<?php
	/**
	 * Класс макросов, то есть методов, доступных в шаблоне
	 */
	class AppointmentMacros {
		/**
		 * @var appointment $module
		 */
		public $module;

		/**
		 * Алиас AppointmentCommon::employeesList()
		 */
		public function employees($template = 'default', $limit = 25, $selectedId = null) {
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			return $module->employeesList($template, $limit, $selectedId);
		}

		/**
		 * Алиас AppointmentCommon::employeesListByServiceId()
		 */
		public function employeesByServiceId($template = 'default', $serviceId, $limit = 25, $selectedId = null) {
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			return $module->employeesListByServiceId($template, $serviceId, $limit, $selectedId);
		}

		/**
		 * Алиас AppointmentCommon::servicesList()
		 */
		public function services($template = 'default', $limit = 25, $selectedId = null) {
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			return $module->servicesList($template, $limit, $selectedId);
		}

		/**
		 * Алиас AppointmentCommon::statusesList()
		 */
		public function statuses($template = 'default', $selectedCode = null) {
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			return $module->statusesList($template, $selectedCode);
		}

		/**
		 * Алиас AppointmentCommon::serviceGroupsList()
		 */
		public function serviceGroups($template = 'default', $limit = 25, $selectedId = null) {
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			return $module->serviceGroupsList($template, $limit, $selectedId);
		}

		/**
		 * Алиас AppointmentCommon::employeeSchedulesList()
		 */
		public function employeeSchedules($template = 'default', $employeeId) {
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			return $module->employeeSchedulesList($template, $employeeId);
		}

		/**
		 * Алиас AppointmentCommon::employeeServicesIdsList()
		 */
		public function employeeServicesIds($template = 'default', $employeeId) {
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			return $module->employeeServicesIdsList($template, $employeeId);
		}

		/**
		 * Выводит в буффер полные данные о сервисе записи на прием для построения виджета
		 */
		public function getAppointmentsData() {
			$params = [];
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			$groups = $module->getServiceGroups();

			foreach ($groups as $group) {
				$params['groups'][$group->getId()] = $group->getName();
			}

			$timeReplacePattern = $module->timePregReplacePattern;
			$services = $module->getServices();

			foreach ($services as $service) {
				$categoryName = $params['groups'][$service->getGroupId()];

				$params['service'][$service->getId()] = [
					'id' => $service->getId(),
					'name' => $service->getName(),
					'time' => preg_replace($timeReplacePattern, '', $service->getTime()),
					'cost' => $service->getPrice(),
					'group_id' => $service->getGroupId(),
				];

				$params['scats'][$categoryName][] = $service->getId();
			}

			$employees = $module->getEmployees();

			foreach ($employees as $employee) {
				$params['personal'][$employee->getId()] = [
					'id' => $employee->getId(),
					'name' => $employee->getName(),
					'photo' => $employee->getPhoto(),
					'description' => $employee->getDescription(),
				];
			}

			$employeesServices = $module->getEmployeesServices();

			foreach ($employeesServices as $employeeService) {
				$employeeId = $employeeService->getEmployeeId();
				$serviceId = $employeeService->getServiceId();

				$params['personal'][$employeeId]['service'][] = $serviceId;
				$params['service'][$serviceId]['personal'][] = $employeeId;
			}

			$employeesSchedules = $module->getEmployeesSchedules();

			foreach ($employeesSchedules as $employeeSchedule) {
				$timeStart = preg_replace($timeReplacePattern, '', $employeeSchedule->getTimeStart());
				$timeEnd = preg_replace($timeReplacePattern, '', $employeeSchedule->getTimeEnd());
				$time = $timeStart . '-' . $timeEnd;
				$params['personal'][$employeeSchedule->getEmployeeId()]['days'][$employeeSchedule->getDayNumber()] = $time;
			}

			$defaultSchedule = $module->getDefaultSchedule();

			if (is_array($defaultSchedule) && count($defaultSchedule) > 0) {
				$params['default']['days'] = $defaultSchedule;
			}

			$bookedOrders = $module->getBookedOrders();
			$entry = [];

			foreach ($bookedOrders as $order) {
				$orderProps = [
					'time' => preg_replace($timeReplacePattern, '', $order->getTime()),
					'status' => $order->getStatusId(),
					'service' => $order->getServiceId(),
					'personal' => $order->getEmployeeId()
				];

				$timeStamp = $order->getDate();
				$orderDate = new umiDate($timeStamp);
				$orderDate = $orderDate->getFormattedDate('d.m.Y');

				$entry['personal'][$order->getEmployeeId()][$orderDate][] = $orderProps;
				$employeesCount = count($params['service'][$order->getServiceId()]['personal']);
				$counter = 0;

				foreach ($params['service'][$order->getServiceId()]['personal'] as $employeeId) {
					if (isset($entry['personal'][$employeeId][$orderDate])) {
						$counter++;
					}
				}

				if ($employeesCount == $counter) {
					$entry['service'][$order->getServiceId()][$orderDate][] = $orderProps;
					$entry['complete_booked'][$orderDate][] = $orderProps;
				}
			}

			$params['entry'] = $entry;
			$module->printJson($params);
		}

		/**
		 * Воводит буффер сообщение об ошибке
		 * @param string $userMessage собщение для пользователя
		 * @param null|mixed $debugMessage служебная информация
		 */
		public function printError($userMessage, $debugMessage = null) {
			$result = array(
				"error" => true,
				"text" => $userMessage,
				"more" => $debugMessage
			);

			$this->module->printJson($result);
		}

		/**
		 * Создает заявку на запись
		 * @throws Exception
		 */
		public function postAppointment() {
			/**
			 * @var appointment|AppointmentCommon $module
			 */
			$module = $this->module;
			$date = getRequest('date');
			$time = getRequest('time');

			$timestamp = strtotime($date);
			$entryName = $date . ' ' . $time;

			$email = getRequest('email');
			$phone = getRequest('phone');
			$name = getRequest('name');
			$comment = getRequest('commentary');

			if (!empty($name)) {
				$entryName = $name;
			}

			if (!empty($email) && !umiMail::checkEmail($email)) {
				$this->printError(getLabel('error-incorrect-email', 'appointment'));
			}

			if (empty($email) && empty($phone)) {
				$this->printError(getLabel('error-email-and-phone-empty', 'appointment'));
			}

			$email = empty($email) ? null : $email;
			$phone = empty($phone) ? null : $phone;
			$fullService = getRequest('full_service');
			$fullPersonal = getRequest('full_personal');
			$serviceContainer = umiServiceContainers::getContainer();
			/**
			 * @var AppointmentOrdersCollection $ordersCollection
			 */
			$ordersCollection = $serviceContainer->get('AppointmentOrders');
			$collectionMap = $ordersCollection->getMap();

			$statusId = $collectionMap->get('ORDER_STATUS_NOT_CONFIRMED');
			$serviceId = $fullService['id'];
			$employeeId = $fullPersonal['id'];

			if ($employeeId === '*') {
				try {
					$employeeId = $module->getRandomEmployeeIdByServiceId($serviceId);
				} catch (Exception $e) {
					$employeeId = null;
				}
			}

			try {
				$orderData = [
					$collectionMap->get('SERVICE_ID_FIELD_NAME') => $serviceId,
					$collectionMap->get('EMPLOYEE_ID_FIELD_NAME') => $employeeId,
					$collectionMap->get('ORDER_DATE_FIELD_NAME') => new umiDate(),
					$collectionMap->get('DATE_FIELD_NAME') => new umiDate($timestamp),
					$collectionMap->get('TIME_FIELD_NAME') => $time . $this->module->defaultSeconds,
					$collectionMap->get('PHONE_FIELD_NAME') => $phone,
					$collectionMap->get('EMAIL_FIELD_NAME') => $email,
					$collectionMap->get('NAME_FIELD_NAME') => $entryName,
					$collectionMap->get('COMMENT_FIELD_NAME') =>  $comment,
					$collectionMap->get('STATUS_ID_FIELD_NAME') => $statusId
				];

				$order = $ordersCollection->create($orderData);
			} catch (Exception $e) {
				$order = null;
				$this->printError(getLabel('error-incorrect-data-given', 'appointment'), $e->getMessage());
			}

			$result = array(
				"error" => false,
				"text" => getLabel('appointments-success-entry', 'appointment')
			);

			$event = new umiEventPoint('addAppointmentOrder');
			$event->setParam('order', $order);
			appointment::setEventPoint($event);

			$module->printJson($result);
		}
	}
?>