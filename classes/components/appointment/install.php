<?php
	/**
	 * Установщик модуля
	 */

	/**
	 * @var array $INFO реестр модуля
	 */
	$INFO = [
		'name'					=> "appointment",
		'config'				=> "1",
		'default_method'		=> "orders",
		'default_method_admin'	=> "pages",
		'work-time-0'			=> "",
		'work-time-1'			=> "",
		'work-time-2'			=> "",
		'work-time-3'			=> "",
		'work-time-4'			=> "",
		'work-time-5'			=> "",
		'work-time-6'			=> "",
		'func_perms'			=> "Группы прав на функционал модуля",
		'func_perms/enroll'		=> "Права на запись",
		'func_perms/manage'		=> "Права на администрирование модуля",
	];

	/**
	 * @var array $COMPONENTS файлы модуля
	 */
	$COMPONENTS = [
		"./classes/components/appointment/admin.php",
		"./classes/components/appointment/class.php",
		"./classes/components/appointment/common.php",
		"./classes/components/appointment/customAdmin.php",
		"./classes/components/appointment/customCommon.php",
		"./classes/components/appointment/customMacros.php",
		"./classes/components/appointment/events.php",
		"./classes/components/appointment/handlers.php",
		"./classes/components/appointment/i18n.en.php",
		"./classes/components/appointment/i18n.php",
		"./classes/components/appointment/includes.php",
		"./classes/components/appointment/install.php",
		"./classes/components/appointment/lang.en.php",
		"./classes/components/appointment/lang.php",
		"./classes/components/appointment/macros.php",
		"./classes/components/appointment/notifier.php",
		"./classes/components/appointment/permissions.php"
	];
?>