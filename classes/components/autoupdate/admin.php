<?php
	/**
	 * Класс функционала административной панели
	 */
	class AutoupdateAdmin{

		use baseModuleAdmin;
		/**
		 * @var autoupdate $module
		 */
		public $module;

		/**
		 * Возвращает информацию о состоянии обновлений системы
		 * @throws coreException
		 */
		public function versions() {
			$regedit = regedit::getInstance();
			$systemEdition = $regedit->getVal("//modules/autoupdate/system_edition");
			$systemEditionStatus = "%autoupdate_edition_" . $systemEdition . "%";

			if (
				$systemEdition == "commerce_trial" &&
				$_SERVER['HTTP_HOST'] != 'localhost' &&
				$_SERVER['HTTP_HOST'] != 'subdomain.localhost' &&
				$_SERVER['SERVER_ADDR'] != '127.0.0.1'
			) {
				$daysLeft = $regedit->getDaysLeft();
				$systemEditionStatus .= " ({$daysLeft} " . getLabel('label-days-left') . ")";
			}

			$systemEditionStatus = autoupdate::parseTPLMacroses($systemEditionStatus);

			$params = Array(
				"autoupdate" => Array(
					"status:system-edition"		=> NULL,
					"status:last-updated"		=> NULL,
					"status:system-version"		=> NULL,
					"status:system-build"		=> NULL,
					"status:db-driver"			=> NULL,
					"boolean:disabled"			=> NULL,
				)
			);

			$params['autoupdate']['status:system-version'] = $regedit->getVal("//modules/autoupdate/system_version");
			$params['autoupdate']['status:system-build'] = $regedit->getVal("//modules/autoupdate/system_build");
			$params['autoupdate']['status:system-edition'] = $systemEditionStatus;
			$params['autoupdate']['status:last-updated'] = date("Y-m-d H:i:s", $regedit->getVal("//modules/autoupdate/last_updated"));

			$db_driver = "mysql";

			if (defined("DB_DRIVER")) {
				$db_driver = DB_DRIVER;
			}

			$params['autoupdate']['status:db-driver'] = $db_driver;

			$autoupdatesDisabled = false;

			if (defined("CURRENT_VERSION_LINE")) {
				if (in_array(CURRENT_VERSION_LINE, array("start", "demo"))) {
					$autoupdatesDisabled = true;
				}
			}

			$params['autoupdate']['boolean:disabled'] = (int) $autoupdatesDisabled;

			$domainsCollection = domainsCollection::getInstance();

			if (!$domainsCollection->isDefaultDomain()) {
				$params['autoupdate']['check:disabled-by-host'] = $domainsCollection->getDefaultDomain()->getHost();
			}

			$this->setDataType("settings");
			$this->setActionType("view");
			$data = $this->prepareData($params, "settings");
			$this->setData($data);
			$this->doData();
		}

	}
?>
