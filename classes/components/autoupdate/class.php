<?php
	/**
	 * Базовый класс модуля "Автообновление".
	 * Модуль отвечает за:
	 *
	 * 1) Получение информации о состоянии обновлений;
	 * 2) Работу с патчами;
	 * 3) Проверку прав на использование установленных модулей.
	 *
	 * Функционал самого обновления размещен в /smu/installer.php
	 * @link http://help.docs.umi-cms.ru/rabota_s_modulyami/modul_avtoobnovleniya/
	 */
	class autoupdate extends def_module {

		/** @var string имя файла в котором хранится кэш времени окончания поддержки лицензии */
		protected $supportTimeCacheFile = 'support-end.time';

		/**
		 * Конструктор
		 */
		public function __construct() {
			parent::__construct();
			$cmsController = cmsController::getInstance();

			if ($cmsController->getCurrentMode() == "admin") {
				$commonTabs = $this->getCommonTabs();

				if ($commonTabs) {
					$commonTabs->add('versions');
				}

				$this->__loadLib("admin.php");
				$this->__implement("AutoupdateAdmin");

				$this->loadAdminExtension();

				$this->__loadLib("customAdmin.php");
				$this->__implement("AutoUpdateCustomAdmin", true);
			} else {
				$this->loadSiteExtension();

				$this->__loadLib("customMacros.php");
				$this->__implement("AutoUpdateCustomMacros", true);
			}

			$this->__loadLib("service.php");
			$this->__implement("AutoUpdateService");

			$this->loadCommonExtension();
			$this->loadTemplateCustoms();

			$this->__loadLib("customCommon.php");
			$this->__implement("AutoUpdateCustomCommon", true);
		}

		/**
		 * Возвращает оставшееся количество дней работы триальной лицензии
		 * @return array|void
		 */
		public function getDaysLeft () {
			if (($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'subdomain.localhost') && $_SERVER['SERVER_ADDR'] == '127.0.0.1') {
				return;
			}

			$regedit = regedit::getInstance();
			$systemEdition = $regedit->getVal("//modules/autoupdate/system_edition");

			if (strpos($systemEdition, 'trial') !== false || (strpos($systemEdition, 'commerce_enc') !== false)) {
				$daysLeft = $regedit->getDaysLeft();
				return array(
					'trial'	=>	array(
						'attribute:daysleft' => $daysLeft
					)
				);
			}
		}

		/**
		 * Возвращает количество дней, которое осталось до окончания поддержки лицензионного ключа
		 * @return int количество дней
		 * @throws publicException
		 */
		public function getSupportEndDate() {
			$cacheValue = $cacheLastUpdateTime = null;
			$cacheFilePath = $this->getSupportTimeCacheFilePath();
			if (is_file($cacheFilePath)) {
				$cacheValue = file_get_contents($cacheFilePath);
				$cacheLastUpdateTime = filemtime($cacheFilePath);
			}
			$supportEndTime = intval($cacheValue);
			$daysToStorage = 3;
			$hoursInDay = 24;
			$secondsInHour = 3600;
			$storageTime = $daysToStorage * $hoursInDay * $secondsInHour;
			if ($supportEndTime <= 0 || $cacheLastUpdateTime <= 0 || !$this->checkSupportCacheRelevance($cacheLastUpdateTime, $storageTime)) {
				$supportEndTime = $this->requestSupportTime($cacheFilePath);
			}
			$dayNumber = date('j', $supportEndTime);
			$monthEng = date('M', $supportEndTime);
			$monthRus = getLabel('month-' . strtolower($monthEng));
			$year = date('Y', $supportEndTime);

			$status = '';
			$daysAverageInMonth = 30;
			$alertDuration = $daysAverageInMonth * $hoursInDay * $secondsInHour;
			$warningDuration = 3 * $alertDuration;
			$timeRemaining = $supportEndTime - time();

			switch (true) {
				case ($timeRemaining <= $alertDuration):
					$status = 'alert';
					break;

				case ($timeRemaining > $alertDuration && $timeRemaining <= $warningDuration):
					$status = 'warning';
					break;

				//no default
			}

			return array(
				'date' => array(
					'@day' => $dayNumber,
					'@month_rus' => $monthRus,
					'@year' => $year,
					'@timestamp' => $supportEndTime,
					'@status' => $status
				)
			);
		}

		/**
		 * Запрашивает время окончания поддержки текущей лицензии от сервера лицензий
		 * и возвращает его в формате Unix Timestamp
		 * @param bool $cacheTime сохранить время окончания поддержки в кэш
		 * @return int время окончания поддержки в формате Unix Timestamp
		 * @throws publicException
		 */
		public function requestSupportTime($cacheTime = true) {
			$licenseServerUrl = base64_decode('aHR0cDovL3Vkb2QudW1paG9zdC5ydS8=');
			$licenseInfoMacro = base64_decode('dWRhdGE6Ly9jdXN0b20vY2hlY2tMaWNlbnNlLz9rZXljb2RlPQ==');

			$userKeyCode = regedit::getInstance()->getVal('//settings/keycode');

			if (!$userKeyCode) {
				throw new publicException(getLabel('error-domain-key-not-found'));
			}

			$requestUrl = $licenseServerUrl . $licenseInfoMacro . $userKeyCode;
			$licenseInfo = simplexml_load_string(umiRemoteFileGetter::get($requestUrl));

			if (!$licenseInfo instanceof SimpleXMLElement) {
				throw new publicException(getLabel('error-license-data-not-loaded'));
			}

			$licenseTypeNodesList = $licenseInfo->xpath('/udata/license_type');
			$licenseTypeNode = array_shift($licenseTypeNodesList);

			if (!$licenseTypeNode instanceof SimpleXMLElement) {
				throw new publicException(getLabel('error-license-edition-not-received'));
			}

			$isTrialLicense = strpos(strtolower($licenseTypeNode->__toString()), 'trial') !== false;

			if ($isTrialLicense) {
				throw new publicException(getLabel('error-trial-license-has-not-expire-date'));
			}

			$supportTimeNodesList = $licenseInfo->xpath('/udata/support_time');
			$supportTimeNode = array_shift($supportTimeNodesList);

			if (!$supportTimeNode instanceof SimpleXMLElement) {
				throw new publicException(getLabel('error-expiry-date-not-received'));
			}

			$supportEndTime = intval($supportTimeNode->__toString());

			if ($supportEndTime <= 0) {
				throw new publicException(getLabel('error-expiry-date-not-loaded-or-incorrect'));
			}

			if ($cacheTime) {
				file_put_contents($this->getSupportTimeCacheFilePath(), $supportEndTime . PHP_EOL);
			}

			return $supportEndTime;
		}
		/**
		 * Возвращает путь до файла, в котором хранится закэшировнное время окончания поддержки лицензии
		 * @return string
		 */
		public function getSupportTimeCacheFilePath() {
			return SYS_CACHE_RUNTIME . $this->supportTimeCacheFile;
		}
		/**
		 * Очищает кэш с временем окончания поддержки лицензии
		 */
		public function resetSupportTimeCache() {
			if (is_file($this->getSupportTimeCacheFilePath())) {
				unlink($this->getSupportTimeCacheFilePath());
			}
		}
		/**
		 * Проверяет актуальность кэша, в котором хранится время окончания поддержки лицензии
		 * @param int $cacheLastUpdateTime время последнего обновления кэша
		 * @param int $storageInterval количество секунд хранения кэша
		 * @return bool
		 */
		protected function checkSupportCacheRelevance($cacheLastUpdateTime, $storageInterval) {
			return (time() - $cacheLastUpdateTime <= $storageInterval);
		}
	};
?>
