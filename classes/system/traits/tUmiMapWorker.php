<?php
	/**
	 * Трейт работника с картой констант
	 */
	trait tUmiMapWorker {

		/**
		 * @var string $map класс с константами
		 */
		private $map;

		/**
		 * Возвращает экземпляр класса с константами
		 * @return iUmiMap
		 * @throws Exception
		 */
		public function getMap() {
			if (!$this->map instanceof iUmiMap) {
				throw new Exception('You should set iUmiMap first');
			}

			return $this->map;
		}

		/**
		 * Устанавливает экземпляр класса с константами
		 * @param iUmiMap $mapInstance экземпляр класса
		 */
		public function setMap(iUmiMap $mapInstance) {
			$this->map = $mapInstance;
		}
	}
?>
