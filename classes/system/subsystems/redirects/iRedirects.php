<?php
	/**
	 * Интерфейс менеджера редиректов
	 */
	interface iRedirects {

		/**
		 * Возвращает экземпляр класса
		 * @return iRedirects
		 */
		public static function getInstance();

		/**
		 * Создает редирект
		 * @param string $source адрес, откуда перенаправлять
		 * @param string $target адрес, куда перенаправлять
		 * @param int $status код статуса редиректа
		 * @param bool $madeByUser редирект сделан пользователем (не автоматически)
		 * @return mixed
		 */
		public function add($source, $target, $status = 301, $madeByUser = false);

		/**
		 * Возвращает данные редиректов по адресу, откуда перенаправлять
		 * @param string $source адрес, откуда перенаправлять
		 * @param bool $madeByUser редирект сделан пользователем (не автоматически)
		 * @return []
		 */
		public function getRedirectsIdBySource($source, $madeByUser = false);

		/**
		 *  Возвращает данные редиректов по адресу, куда перенаправлять
		 * @param string $target адрес, куда перенаправлять
		 * @param bool $madeByUser редирект сделан пользователем (не автоматически)
		 * @return mixed
		 */
		public function getRedirectIdByTarget($target, $madeByUser = false);

		/**
		 * Удаляет редирект
		 * @param int $id идентификатор редиректа
		 * @return mixed
		 */
		public function del($id);

		/**
		 * Осуществляет редирект, если это необходимо
		 * @param string $currentUri текущая страница
		 * @param bool $madeByUser учитывать редиректы ручные/автоматические
		 * @return mixed
		 */
		public function redirectIfRequired($currentUri, $madeByUser = false);

		/**
		 * Удаляет все редиректы
		 * @return mixed
		 */
		public function deleteAllRedirects();

		/**
		 * Включает обработчики событий изменения страниц
		 * @return mixed
		 */
		public function init();
	};
?>