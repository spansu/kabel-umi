<?php
	/**
	 * Dependency Injection Containers
	 * @see umiServiceContainers::getDefaultContainer()
	 */
	class umiServiceContainer {
		/**
		 * @var array $services настройки инстанцирования сервисов
		 */
		private $services;
		/**
		 * @var array $parameters параметры для инстанцирования сервисов
		 */
		private $parameters;
		/**
		 * @var iUmiService[] $serviceContainer список инстанцированных сервисов
		 */
		private $serviceContainer;

		/**
		 * Конструтор
		 * @param array $services настройки инстанцирования сервисов
		 * @param array $parameters параметры для инстанцирования сервисов
		 */
		public function __construct(array $services = array(), array $parameters = array()) {
			$this->services = $services;
			$this->parameters = $parameters;
			$this->serviceContainer = [];
		}

		/**
		 * Возвращает сервис по его имени
		 * @param string $name имя сервиса
		 * @return iUmiService
		 * @throws Exception
		 */
		public function get($name) {
			if (!$this->has($name)) {
				throw new Exception('Service not found: '.$name);
			}

			if (!isset($this->serviceContainer[$name])) {
				$this->serviceContainer[$name] = $this->createService($name);
			}

			return $this->serviceContainer[$name];
		}

		/**
		 * Существуют ли правила инстанцирования для сервиса
		 * @param string $name имя сервиса
		 * @return bool
		 */
		public function has($name) {
			return isset($this->services[$name]);
		}

		/**
		 * Существуют ли параметры инстанцирования сервиса
		 * @param string $name имя сервиса
		 * @return bool
		 */
		public function hasParameter($name) {
			try {
				$this->getParameter($name);
			} catch (Exception $exception) {
				return false;
			}

			return true;
		}

		/**
		 * Возвращает параметры инстанцирования сервиса
		 * @param string $name имя сервиса
		 * @return array
		 * @throws Exception
		 */
		private function getParameter($name) {
			$tokens  = explode('.', $name);
			$context = $this->parameters;
			while (null !== ($token = array_shift($tokens))) {
				if (!isset($context[$token])) {
					throw new Exception('Parameter not found: '.$name);
				}
				$context = $context[$token];
			}
			return $context;
		}

		/**
		 * Инстанцирует сервис и возвращает его экземпляр
		 * @param string $name имя сервиса
		 * @return iUmiService
		 * @throws Exception
		 */
		private function createService($name) {
			$entry = &$this->services[$name];

			if (!is_array($entry) || !isset($entry['class'])) {
				throw new Exception($name .' service entry must be an array containing a \'class\' key');
			} elseif (!class_exists($entry['class'])) {
				throw new Exception($name .' service class does not exist: ' . $entry['class']);
			} elseif (isset($entry['lock'])) {
				throw new Exception($name .' contains circular reference');
			}

			$entry['lock'] = true;
			$arguments = isset($entry['arguments']) ? $this->resolveArguments($entry['arguments']) :[];

			$reflector = new ReflectionClass($entry['class']);
			$service = $reflector->newInstanceArgs($arguments);

			if (isset($entry['calls'])) {
				$this->initializeService($service, $name, $entry['calls']);
			}

			return $service;
		}

		/**
		 * Формирует список значений аргументов по списку их идентификаторов
		 * @param array $argumentDefinitions список идентификаторов аргументов
		 * @return array
		 * @throws Exception
		 */
		private function resolveArguments(array $argumentDefinitions) {
			$arguments = [];

			foreach ($argumentDefinitions as $argumentDefinition) {
				if ($argumentDefinition instanceof ServiceReference) {
					$argumentServiceName = $argumentDefinition->getName();
					$arguments[] = $this->get($argumentServiceName);
				} elseif ($argumentDefinition instanceof ParameterReference) {
					$argumentParameterName = $argumentDefinition->getName();
					$arguments[] = $this->getParameter($argumentParameterName);
				} else {
					$arguments[] = $argumentDefinition;
				}
			}

			return $arguments;
		}

		/**
		 * Применяет правила инстанцирования к сервису, то есть вызывает все необходимые методы с передачей им параметров
		 * @param iUmiService $service сервис
		 * @param string $name имя сервиса
		 * @param array $callDefinitions правила инстанцирования
		 * @throws Exception
		 */
		private function initializeService($service, $name, array $callDefinitions) {
			foreach ($callDefinitions as $callDefinition) {

				if (!is_array($callDefinition) || !isset($callDefinition['method'])) {
					throw new Exception($name . ' service calls must be arrays containing a \'method\' key');
				} elseif (!is_callable([$service, $callDefinition['method']])) {
					throw new Exception($name . ' service asks for call to uncallable method: ' . $callDefinition['method']);
				}

				$arguments = isset($callDefinition['arguments']) ? $this->resolveArguments($callDefinition['arguments']) : [];
				call_user_func_array([$service, $callDefinition['method']], $arguments);
			}
		}
	}
?>