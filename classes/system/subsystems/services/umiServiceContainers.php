<?php
	/**
	 * Класс списка контейнеров сервисов.
	 * Через него следует получать желаемый контейнер,
	 * @example $serviceContainer = umiServiceContainers::getContainer();
	 */
	class umiServiceContainers {
		/**
		 * @var umiServiceContainer[] $serviceContainers список контейнеров сервисов
		 */
		private static $serviceContainers = [];

		/**
		 * Возвращает контейнер сервисов заданного типа
		 * @param string $type тип
		 * @return umiServiceContainer
		 */
		public static function getContainer($type = 'default') {
			if (isset(self::$serviceContainers[$type])) {
				return self::$serviceContainers[$type];
			}

			return self::$serviceContainers[$type] = self::getDefaultContainer();
		}

		/**
		 * Возвращает контейнер сервисов по умолчанию
		 * @return umiServiceContainer
		 */
		protected static function getDefaultContainer() {
			$services = self::getDefaultServices();
			$arguments = self::getDefaultArguments();
			return new umiServiceContainer($services, $arguments);
		}

		/**
		 * Возвращает список аргументов для контейнера сервисов по умолчанию
		 * @return array
		 * @throws Exception
		 * @throws coreException
		 */
		protected static function getDefaultArguments() {
			return [
				'connection' => ConnectionPool::getInstance()->getConnection(),
				'configuration' => mainConfiguration::getInstance(),
				'httpBuffer' => outputBuffer::current('HTTPOutputBuffer'),
				'umiRedirectsCollection' => 'redirects',
				'AppointmentServicesCollection' => 'AppointmentServices',
				'AppointmentServiceGroupsCollection' => 'AppointmentServiceGroups',
				'AppointmentEmployeesCollection' => 'AppointmentEmployees',
				'AppointmentEmployeesServicesCollection' => 'AppointmentEmployeesServices',
				'AppointmentEmployeesSchedulesCollection' => 'AppointmentEmployeesSchedules',
				'AppointmentOrdersCollection' => 'AppointmentOrders',
				'serverProtocol' => getSelectedServerProtocol(),
				'MailTemplatesCollection' => 'MailTemplates',
				'baseUmiCollectionMap' => new baseUmiCollectionMap(),
				'appointmentEmployeesMap' => new appointmentEmployeesMap(),
				'appointmentEmployeesSchedulesMap' => new appointmentEmployeesSchedulesMap(),
				'appointmentEmployeesServicesMap' => new appointmentEmployeesServicesMap(),
				'appointmentOrdersMap' => new appointmentOrdersMap(),
				'appointmentServiceGroupsMap' => new appointmentServiceGroupsMap(),
				'appointmentServicesMap' => new appointmentServicesMap(),
				'mailTemplatesMap' => new mailTemplatesMap(),
				'umiRedirectsMap' => new umiRedirectsMap()
			];
		}

		/**
		 * Возвращает список правил инстанцирования сервисов для контейнера сервисов по умолчанию
		 * @return array
		 */
		protected static function getDefaultServices() {
			return [
				'redirects' => [
					'class' => 'umiRedirectsCollection',
					'arguments' => [
						new ParameterReference('umiRedirectsCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setConfiguration',
							'arguments' => [
								new ParameterReference('configuration')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('umiRedirectsMap')
							]
						],
						[
							'method' => 'setBuffer',
							'arguments' => [
								new ParameterReference('httpBuffer')
							]
						],
						[
							'method' => 'setProtocol',
							'arguments' => [
								new ParameterReference('serverProtocol')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('umiRedirectsMap')
							]
						]
					]
				],
				'AppointmentServices' => [
					'class' => 'AppointmentServicesCollection',
					'arguments' => [
						new ParameterReference('AppointmentServicesCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('appointmentServicesMap')
							]
						]
					]
				],
				'AppointmentServiceGroups' => [
					'class' => 'AppointmentServiceGroupsCollection',
					'arguments' => [
						new ParameterReference('AppointmentServiceGroupsCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('appointmentServiceGroupsMap')
							]
						]
					]
				],
				'AppointmentEmployees' => [
					'class' => 'AppointmentEmployeesCollection',
					'arguments' => [
						new ParameterReference('AppointmentEmployeesCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('appointmentEmployeesMap')
							]
						]
					]
				],
				'AppointmentEmployeesServices' => [
					'class' => 'AppointmentEmployeesServicesCollection',
					'arguments' => [
						new ParameterReference('AppointmentEmployeesServicesCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('appointmentEmployeesServicesMap')
							]
						]
					]
				],
				'AppointmentEmployeesSchedules' => [
					'class' => 'AppointmentEmployeesSchedulesCollection',
					'arguments' => [
						new ParameterReference('AppointmentEmployeesSchedulesCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('appointmentEmployeesSchedulesMap')
							]
						],
					]
				],
				'AppointmentOrders' => [
					'class' => 'AppointmentOrdersCollection',
					'arguments' => [
						new ParameterReference('AppointmentOrdersCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('appointmentOrdersMap')
							]
						]
					]
				],
				'MailTemplates' => [
					'class' => 'MailTemplatesCollection',
					'arguments' => [
						new ParameterReference('MailTemplatesCollection'),
					],
					'calls' => [
						[
							'method' => 'setConnection',
							'arguments' => [
								new ParameterReference('connection')
							]
						],
						[
							'method' => 'setMap',
							'arguments' => [
								new ParameterReference('mailTemplatesMap')
							]
						]
					]
				]
			];
		}
	}
?>