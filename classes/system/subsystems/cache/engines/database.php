<?php
	/**
	 * Класс кеширования через базу данных
	 */
	class databaseCacheEngine implements iCacheEngine {
		/**
		 * @var bool $connection доступен ли кеширующий механизм
		 */
		private $connected = true;
		/**
		 * @var IConnection|false $connection ресурс соединения с бд
		 */
		private $connection = false;

		/**
		 * Конструктор
		 */
		public function __construct() {
			$this->connection = ConnectionPool::getInstance()->getConnection();
			$this->connected = ($this->connection instanceof IConnection);
		}

		/**
		 * Доступен ли кеширующий механизм
		 * @return bool
		 */
		public function getIsConnected() {
			return (bool) $this->connected;
		}

		/**
		 * Сохраняет объект в кеш
		 * @param string $key ключ кеша
		 * @param object $object сохраняемый объект
		 * @param int $expire время жизни кеша в секундах
		 * @return bool
		 */
		public function saveObjectData($key, $object, $expire) {
			return $this->saveRawData($key, $object, $expire);
		}

		/**
		 * Сохраняет данные в кеш
		 * @param string $key ключ кеша
		 * @param mixed $data данные
		 * @param int $expire время жизни кеша в секундах
		 * @return bool
		 */
		public function saveRawData($key, $data, $expire) {
			$key = $this->connection->escape($key);
			$data = $this->connection->escape(serialize($data));
			$createTime = time();
			$expire = time() + (int) $expire;

			$sql = <<<sql
INSERT INTO `cms3_data_cache` (`key`, `value`, `create_time`, `expire_time`)
	VALUES('$key', '$data', $createTime, $expire)
		ON DUPLICATE KEY UPDATE `value` = '$data', `create_time` = $createTime, `expire_time` = $expire;
sql;
			$connection = $this->getConnection();
			$connection->query($sql);

			return true;
		}

		/**
		 * Загружает объект из кеша и возвращает его
		 * @param string $key ключ кеша
		 * @return object|null
		 */
		public function loadObjectData($key) {
			return $this->loadRawData($key);
		}

		/**
		 * Загружает данные из кеша и возвращает их
		 * @param string $key ключ кеша
		 * @return mixed|null
		 */
		public function loadRawData($key) {
			$escapedKey = $this->connection->escape($key);

			$sql = <<<SQL
SELECT `value`, `expire_time`
	FROM `cms3_data_cache`
		WHERE  `key` = '$escapedKey';
SQL;
			$connection = $this->getConnection();
			$result = $connection->queryResult($sql);

			if ($result->length() == 0) {
				return null;
			}

			foreach ($result as $row) {

				if (time() > $row['expire_time']) {
					return null;
				}

				if (rand(0, 10) == 10) {
					$this->updateEntry($key);
				}

				return unserialize($row['value']);
			}
		}

		/**
		 * Удаляет кеш по ключу
		 * @param string $key ключ кеша
		 * @return bool
		 */
		public function delete($key) {
			$key = $this->connection->escape($key);

			$sql = <<<SQL
DELETE FROM `cms3_data_cache`
	WHERE `key` = '$key';
SQL;

			$connection = $this->getConnection();
			$connection->query($sql);

			return true;
		}

		/**
		 * Удаляет весь кеш
		 * @return bool
		 */
		public function flush() {
			$sql = <<<SQL
TRUNCATE TABLE `cms3_data_cache`;
SQL;
			$connection = $this->getConnection();
			$connection->query($sql);

			return true;
		}

		/**
		 * Возвращает размер кеша в байтах
		 * @return int
		 * @throws coreException если не удалось совершить операцию
		 */
		public function getCacheSize() {
			$sql = <<<SQL
SELECT ROUND(data_length + index_length) AS total_size
	FROM information_schema.tables
		WHERE table_schema = DATABASE()
			AND table_name = 'cms3_data_cache';
SQL;
			$connection = $this->getConnection();
			$result = $connection->queryResult($sql);

			if ($result->length() == 0) {
				throw new coreException('Cant calculate size');
			}

			foreach ($result as $row) {}

			return (int) $row['total_size'];
		}

		/**
		 * Возвращает ресурс соединения с бд
		 * @return IConnection
		 * @throws coreException если ресурс невалидный
		 */
		private function getConnection() {
			$connection = $this->connection;

			if (!$connection instanceof IConnection) {
				throw new coreException('Data base not connected');
			}

			return $connection;
		}

		/**
		 * Обновляет количество обращений к кешу
		 * @param string $key ключ кеша
		 * @return bool
		 */
		private function updateEntry($key) {
			$escapedKey = $this->connection->escape($key);
			$entryTime = time();

			$sql = <<<SQL
UPDATE `cms3_data_cache`
	SET `entry_time` = $entryTime, `entries_number` = `entries_number` + 1
		WHERE `key` = '$escapedKey';
SQL;

			$connection = $this->getConnection();
			$connection->query($sql);

			return true;
		}

		/**
		 * Выполняет периодические операции над кешем:
		 * 	1) Раз в час удаляет кеш с истекшим временем жизни
		 * 	2) Раз в день (ночью) оптимизирует таблицу с кешем
		 * @return bool
		 */
		public function doPeriodicOperations() {
			$umiRegistry = regedit::getInstance();
			$currentDate = getdate(time());
			$nextCleanTime = (int) $umiRegistry->getVal('//settings/next_clean_time');

			if ($currentDate[0] >= $nextCleanTime) {
				$this->dropExpired();
				$umiRegistry->setVal('//settings/next_clean_time', $currentDate[0] + 3600);
			}

			$nextOptimiseTime = (int) $umiRegistry->getVal('//settings/next_optimise_time');

			if ($currentDate[0] >= $nextOptimiseTime && ($currentDate['hours'] > 0 && $currentDate['hours'] <= 6)) {
				$this->optimise();
				$umiRegistry->setVal('//settings/next_optimise_time', $currentDate[0] + 86400);
			}

			return true;
		}

		/**
		 * Удаляет кеш с истекшим временем жизни
		 * @return bool
		 */
		private function dropExpired() {
			$time = time();

			$sql = <<<SQL
DELETE FROM `cms3_data_cache`
	WHERE `expire_time` < $time;
SQL;
			$connection = $this->getConnection();
			$connection->query($sql);

			return true;
		}

		/**
		 * Оптимизирует таблицу с кешем
		 * @return bool
		 */
		private function optimise() {
			$sql = <<<SQL
OPTIMIZE TABLE `cms3_data_cache`;
SQL;
			$connection = $this->getConnection();
			$connection->query($sql);

			return true;
		}
	};
?>