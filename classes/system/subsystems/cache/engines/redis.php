<?php

	/**
	 * Класс реализация механизма кеширования в сервер Redis
	 */
	class redisCacheEngine implements iCacheEngine {
		/**
		 * Свойство в которое сохраняется созданный объект клиента к Redis
		 * @var bool
		 */
		private $_redis = false;
		/**
		 * Хост сервера с Redis
		 * @var string
		 */
		private $_host = '';
		/**
		 * Порт сервера Redis
		 * @var string
		 */
		private $_port = '';
		/**
		 * Номер используемой базы
		 * @var string
		 */
		private $_base = '';
		/**
		 * Ключ доступа к серверу Redis, если необходим
		 * @var string
		 */
		private $_auth = '';
		/**
		 * Префикс для сохраняемых ключей
		 * @var string
		 */
		private $_namespace = 'umic:%u:';

		/**
		 * Возвращает экземпляр объекта клиента к Redis
		 * @return bool|Redis
		 */
		private function getClient() {
			if (class_exists('Redis')) {
				if (!$this->_redis || ($this->_redis && !$this->_redis->ping())) {

					$this->_redis = new Redis();

					if (!$this->_redis->connect($this->_host, $this->_port)) {
						return false;
					}

					if (!empty($this->_auth)) {
						if (!$this->_redis->auth($this->_auth)) {
							return false;
						}
					}

					if (!$this->_redis->select($this->_base)) {
						return false;
					}
				}
			}
			return $this->_redis;
		}

		public function __construct() {
			$conf = mainConfiguration::getInstance();

			$md = hash('crc32', $conf->get('system', 'salt'));
			$this->_namespace = sprintf($this->_namespace, $md);

			$this->_host = $conf->get('cache', 'redis.host');
			if (empty($this->_host)) {
				$this->_host = 'localhost';
			}

			$this->_port = $conf->get('cache', 'redis.port');
			if (is_null($this->_port)) {
				$this->_port = '6379';
			}

			$this->_base = $conf->get('cache', 'redis.base');
			if (empty($this->_base)) {
				$this->_base = '1';
			}

			$this->_auth = $conf->get('cache', 'redis.auth');

			$this->getClient();
		}

		public function saveObjectData($key, $object, $expire) {
			return $this->saveRawData($key, $object, $expire);
		}

		public function saveRawData($key, $string, $expire) {
			$redis = $this->getClient();
			return (bool) $redis ? $redis->set($this->_namespace . md5($key), serialize($string), $expire) : false;
		}

		public function loadObjectData($key) {
			return $this->loadRawData($key);
		}

		public function loadRawData($key) {
			$key = $this->_namespace . md5($key);
			$redis = $this->getClient();
			if (!$redis) {
				return null;
			}
			$res = $redis ? $redis->get($key) : null;
			return $res ? unserialize($res) : null;
		}

		public function delete($key) {
			$key = $this->_namespace . md5($key);
			$redis = $this->getClient();
			return (bool) $redis ? $redis->del($key) : false;
		}

		public function flush() {
			$redis = $this->getClient();
			if (!$redis) {
				return false;
			}
			$keys = $redis->keys($this->_namespace . '*');
			return is_array($keys) && count($keys) > 0 ? $redis->del($keys) : true;
		}

		public function getIsConnected() {
			return (bool) $this->getClient();
		}

		public function getCacheSize() {
			return 'not yet implemented';
		}

	}

?>
