<?php

	/**
	 * Этот класс служит для управления полем объекта.
	 * Обрабатывает тип поля "Счетчик"
	 */
	class umiObjectPropertyCounter extends umiObjectProperty {
		protected $oldValue;

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$fieldId = $this->field_id;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT cnt FROM `cms3_object_content_cnt` WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}' LIMIT 1";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			$cnt = 0;

			if ($result->length() > 0) {
				$fetchResult = $result->fetch();
				$cnt = (int) array_shift($fetchResult);
			}

			$this->oldValue = $cnt;
			return [$cnt];
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$value = sizeof($this->value) ? (int) $this->value[0] : 0;
			$lambda = $value - $this->oldValue;
			$connection = ConnectionPool::getInstance()->getConnection();

			if ((abs($lambda) == 1) && $value !== 0 && $this->oldValue) {
				$sql = "UPDATE `cms3_object_content_cnt` SET cnt = cnt + ({$lambda}) WHERE obj_id = '{$this->object_id}' AND field_id = '{$this->field_id}'";
				$connection->query($sql);
			} else {
				$this->deleteCurrentRows();
				$sql = "INSERT INTO `cms3_object_content_cnt` (obj_id, field_id, cnt) VALUES('{$this->object_id}', '{$this->field_id}', '{$value}')";
				$connection->query($sql);
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function deleteCurrentRows() {
			$objectId = (int) $this->object_id;
			$fieldId = (int) $this->field_id;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "DELETE FROM `cms3_object_content_cnt` WHERE `obj_id` = {$objectId} AND `field_id` = {$fieldId}";
			$connection->query($sql);
		}

		/**
		 * @inherit
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0])) {
				$oldValue = 0;
			} else {
				$oldValue = intval($oldValue[0]);
			}

			if (!isset($newValue[0])) {
				$newValue = 0;
			} else {
				$newValue = intval($newValue[0]);
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}

?>
