<?php

	/**
	 * Этот класс служит для управления полем объекта.
	 * Обрабатывает тип поля "Файл"
	 */
	class umiObjectPropertyFile extends umiObjectProperty {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;
			$isAdminMode = (cmsController::getInstance()->getCurrentMode() == 'admin');

			if ($data = $this->getPropData()) {
				foreach ($data['text_val'] as $val) {
					if (is_null($val)) {
						continue;
					}

					$val = self::unescapeFilePath($val);
					$file = new umiFile($val);

					if ($file->getIsBroken() && !$isAdminMode) {
						continue;
					}

					$res[] = $file;
				}
				return $res;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT text_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}' LIMIT 1";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				$file = new umiFile($val);

				if ($file->getIsBroken() && !$isAdminMode) {
					continue;
				}

				$res[] = $file;
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();

			if (is_null($this->value)) {
				return;
			}

			$connection = ConnectionPool::getInstance()->getConnection();

			foreach ($this->value as $val) {
				if (!$val) {
					continue;
				}

				$val = ($val instanceof iUmiFile) ? $val->getFilePath() : (string) $val;

				if (!@is_file($val)) {
					continue;
				}

				$val = $connection->escape($val);
				$sql = "INSERT INTO {$this->tableName} (obj_id, field_id, text_val) VALUES('{$this->object_id}', '{$this->field_id}', '{$val}')";
				$connection->query($sql);
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0])) {
				$oldValue = '';
			} elseif ($oldValue[0] instanceof umiImageFile) {
				$oldValue = $oldValue[0]->getFilePath();
			} else {
				$oldValue = $oldValue[0];
			}

			if (!isset($newValue[0])) {
				$newValue = '';
			} elseif ($newValue[0] instanceof umiImageFile) {
				$newValue = $newValue[0]->getFilePath();
			} else {
				$newValue = $newValue[0];
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}

?>
