<?php

	/**
	 * Этот класс служит для управления полем объекта
	 * Обрабатывает тип поля "Теги".
	 */
	class umiObjectPropertyTags extends umiObjectProperty {

		/**
		 * @inheritdoc
		 */
		protected function loadValue() {
			$res = [];
			$fieldId = $this->field_id;

			if ($data = $this->getPropData()) {
				foreach ($data['varchar_val'] as $val) {
					if (is_null($val)) {
						continue;
					}
					$res[] = (string) $val;
				}
				return $res;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT varchar_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$fieldId}'";
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($result as $row) {
				$val = array_shift($row);

				if (is_null($val)) {
					continue;
				}

				$res[] = (string) $val;
			}

			return $res;
		}

		/**
		 * @inheritdoc
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();

			if (sizeof($this->value) == 1) {
				$value = trim($this->value[0], ",");
				$value = preg_replace("/[^A-Za-z0-9А-Яа-яЁё'\-$%_,\s]/u", "", $value);
				$value = explode(",", $value);
			} else {
				$value = array_map(
						create_function('$a', "return preg_replace(\"/[^A-Za-z0-9А-Яа-яЁё'\\-\\$%_,\s]?/u\", \"\", \$a); "),
						$this->value
				);
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			foreach ($value as $val) {
				$val = trim($val);

				if (strlen($val) == 0) {
					continue;
				}

				$val = self::filterInputString($val);
				$sql = "INSERT INTO {$this->tableName} (
							obj_id, field_id, varchar_val
						) VALUES(
							'{$this->object_id}', '{$this->field_id}', '{$val}'
						)";
				$connection->query($sql);
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (count($oldValue) !== count($newValue)) {
				return true;
			}

			$normalisedOldValue = [];

			foreach ($oldValue as $oldValueTag) {
				$normalisedOldValue[] = strval($oldValueTag);
			}

			foreach ($newValue as $newValueTag) {
				$normalisedNewValueTag = strval($newValueTag);
				if (!in_array($normalisedNewValueTag, $normalisedOldValue)) {
					return true;
				}
			}

			return false;
		}
	}

?>
