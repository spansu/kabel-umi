<?php
/**
	* Служит для управления доменами (класс domain) в системе. Синглтон, экземпляр коллекции можно получить через статический метод getInstance.
	* Участвует в роутинге урлов в условиях мультидоменности.
	* TODO Check and format all PHPDoc's
*/

	class domainsCollection extends singleton implements iSingleton, iDomainsCollection {
		private $domains = Array(), $def_domain;

		/**
			* Конструктор, подгружает список доменов
		*/
		protected function __construct() {
			$this->loadDomains();
		}

		/**
			* Получить экземпляр коллекции
			* @return domainsCollection экземпляр коллекции
		*/
		public static function getInstance($c = NULL) {
			return parent::getInstance(__CLASS__);
		}

		/**
			* Добавить в систему новый домен
			* @param String $host адрес домен (хост)
			* @param Integer $lang_id id языка (класс lang) по умолчанию для этого домена
			* @param Boolean $is_default=false если true, то этот домен станет основным. Будтье осторожны, при этом может испортиться лицензия
			* @return Integer id созданного домена
		*/
		public function addDomain($host, $lang_id, $is_default = false) {
			if ($domain_id = $this->getDomainId($host)) {
				return $domain_id;
			}

			cacheFrontend::getInstance()->flush();
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "INSERT INTO cms3_domains VALUES(null,'%s',%d,%d)";
			$sql = sprintf($sql,$host,(int)$is_default,$lang_id);
			$connection->query($sql);

			$domain_id = $connection->insertId();

			$this->domains[$domain_id] = $domain = new domain($domain_id);
			$domain->setHost($host);
			$domain->setIsDefault($is_default);
			$domain->setDefaultLangId($lang_id);

			if ($is_default) {
				$this->setDefaultDomain($domain_id);
			}

			$domain->commit();
			return $domain_id;
		}

		/**
		 * Установить домен по умолчанию
		 * @param Integer $domain_id id домена, который нужно сделать доменом по умолчанию
		 * @return bool
		 */
		public function setDefaultDomain($domain_id) {
			if (!$this->isExists($domain_id)) {
				return false;
			}

			cacheFrontend::getInstance()->flush();
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "UPDATE cms3_domains SET is_default = '0' WHERE is_default = '1'";
			$connection->query($sql);

			if ($def_domain = $this->getDefaultDomain()) {
				$def_domain->setIsDefault(false);
				$def_domain->commit();
			}

			$this->def_domain = $this->getDomain($domain_id);
			$this->def_domain->setIsDefault(true);
			$this->def_domain->commit();
			return true;
		}

		/**
		 * Удалить домен из системы
		 * @param Integer $domain_id id домена, который необходимо удалить
		 * @return Boolean true, если удалось удалить домен
		 * @throws coreException
		 */
		public function delDomain($domain_id) {
			$domain_id = (int) $domain_id;

			if (!$this->isExists($domain_id)) {
				throw new coreException("Domain #{$domain_id} doesn't exist.");
			}

			$domain = $this->getDomain($domain_id);
			$domain->delAllMirrows();
			cacheFrontend::getInstance()->flush();

			if ($domain->getIsDefault()) {
				$this->def_domain = false;
			}

			unset($domain);
			unset($this->domains[$domain_id]);

			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "DELETE FROM cms3_hierarchy WHERE domain_id = '{$domain_id}'";
			$connection->query($sql);

			$sql = "DELETE FROM cms3_domains WHERE id = '{$domain_id}'";
			$connection->query($sql);

			return true;
		}

		/**
			* Получить экземпляр домена (класс domain)
			* @param Integer $domain_id id домена, который необходимо получить
			* @return domain экземпляр домен или false в случае неудачи
		*/
		public function getDomain($domain_id) {
			return $this->isExists($domain_id) ? $this->domains[$domain_id] : false;
		}

		/**
			* Получить домен по умолчанию
			* @return domain экземпляр класса domain или false
		*/
		public function getDefaultDomain() {
			return ($this->def_domain) ? $this->def_domain : false;
		}

		/**
			* Получить список доменов в системе
			* @return domain[]
		*/
		public function getList() {
			return $this->domains;
		}

		/**
			* Проверить, существует ли домен $domain_id в системе
			* @param id $domain_id домена
			* @return Boolean true, если домен существует
		*/
		public function isExists($domain_id) {
			return (bool) @array_key_exists($domain_id, $this->domains);
		}

		/**
			* Получть id домена по его хосту (адресу домена)
			* @param String $host адрес домена
			* @param Boolean $user_mirrow=true если параметр равен true, то поиск будет осуществляться в т.ч. и во всех зеркалах домена
			* @return Integer id домена, либо false если домен с таким хостом не найден
		*/
		public function getDomainId($host, $useMirrows = true, $checkIdn=true) {
			foreach($this->domains as $domain) {
				if($domain->getHost() == $host) {
					return $domain->getId();
				} else {
					if($useMirrows) {
						$mirrows = $domain->getMirrowsList();
						foreach($mirrows as $domainMirrow) {
							if($domainMirrow->getHost() == $host) {
								return $domain->getId();
							}
						}
					}
				}
			}
			if( $checkIdn && idn_to_ascii( $host ) != idn_to_utf8( $host ) ) {
				$host = ( $host == idn_to_ascii( $host ) ) ? idn_to_utf8( $host ) : idn_to_ascii( $host );
				return $this->getDomainId( $host, $useMirrows, false);
			}
			return false;
		}

		/**
			* Загружает список доменов из БД в коллекцию
			* @return Boolean true, если операция прошла успешно
		*/
		private function loadDomains() {
			$cacheFrontend = cacheFrontend::getInstance();
			$connection = ConnectionPool::getInstance()->getConnection();
			$domainIds = $cacheFrontend->loadData('domains_list');

			if (!is_array($domainIds)) {
				$sql = "SELECT id, host, is_default, default_lang_id FROM cms3_domains";
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);

				$domainIds = array();
				foreach ($result as $row) {
					$domainIds[$row[0]] = $row;
				}

				$cacheFrontend->saveData('domains_list', $domainIds, 3600);
			}

			foreach ($domainIds as $domain_id => $row) {
				$domain = $cacheFrontend->load($domain_id, 'domain');
				if ($domain instanceof domain == false) {
					try {
						$domain = new domain($domain_id, $row);
					} catch(privateException $e) {
						continue;
					}

					$cacheFrontend->save($domain, 'domain');
				}
				$this->domains[$domain_id] = $domain;

				if ($domain->getIsDefault()) {
					$this->def_domain = $domain;
				}
			}

			return true;
		}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 */
		public function clearCache() {
			$keys = array_keys($this->domains);
			foreach($keys as $key) unset($this->domains[$key]);
			$this->domains = array();
			$this->loadDomains();
		}
		
		/**
		 *  Проверяет, является ли домен доменом по умолчанию
		 *  @param string $host - имя домена. По умолчанию берет текущий домен из $_SERVER['HTTP_HOST']
		 *  @return boolean
		 */
		public function isDefaultDomain( $host = null ) {
			$mainHost = idn_to_ascii( $this->getDefaultDomain()->getHost() );
			$host = ( $host ) ? $host : $_SERVER['HTTP_HOST'];
			$host = idn_to_ascii( $host );
			$hostList = array( $mainHost, 'www.'.$mainHost, );
			if( in_array( $host, $hostList ) ) {
				return true;
			}
			return false;
		}
	}
?>