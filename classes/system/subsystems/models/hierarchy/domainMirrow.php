<?php

	/**
	 * Предоставляет доступ к свойствам зеркала для домена (класс domain).
	 * Зеркало домена используется для создания алиасов.
	 */
	class domainMirrow extends umiEntinty implements iUmiEntinty, iDomainMirrow {
		/** @var string тип сущности для кэширования, @see umiEntinty::updateCache() */
		protected $store_type = 'domain_mirrow';

		/** @var string доменное имя */
		private $host;

		/**
		 * Изменить хост (адрес) зеркала
		 * @param string $host адрес домена
		 */
		public function setHost($host) {
			$this->host = domain::filterHostName($host);
			$this->setIsUpdated();
		}

		/**
		 * Хост (адрес) зеркала
		 * @return string
		 */
		public function getHost() {
			return $this->host;
		}

		/**
		 * Загрузить информацию о зеркале из БД
		 * @param mixed $row массив с данными сущности, @see umiEntinty::__construct()
		 * @return bool
		 * @throws Exception
		 */
		protected function loadInfo($row = false) {
			if ($row === false) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$sql = "SELECT id, host FROM cms3_domain_mirrows WHERE id = '{$this->id}'";
				$result = $connection->queryResult($sql);
				$result->setFetchType(IQueryResult::FETCH_ROW);
				$row = $result->fetch();
			}

			if (list($id, $host) = $row) {
				$this->host = $host;
				return true;
			}

			return false;
		}

		/**
		 * Сохранить внесенные изменения в БД
		 * @return bool
		 */
		protected function save() {
			$host = self::filterInputString($this->host);
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "UPDATE cms3_domain_mirrows SET host = '{$host}' WHERE id = '{$this->id}'";
			$connection->query($sql);
			return true;
		}
	}

?>
