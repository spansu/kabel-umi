<?php
	/**
	 * Карта констант коллекции сотрудников записи на прием
	 */
 	class appointmentEmployeesMap extends baseUmiCollectionMap {
		/**
		 * @const string TABLE_NAME имя таблицы, где хранятся сотрудники
		 */
		const TABLE_NAME = 'cms3_appointment_employees';
	}
?>