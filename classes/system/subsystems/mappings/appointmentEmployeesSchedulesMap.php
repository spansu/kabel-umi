<?php
	/**
	 * Карта констант коллекции расписаний сотрудников записи на прием
	 */
	class appointmentEmployeesSchedulesMap extends baseUmiCollectionMap {
		/**
		 * @const string TABLE_NAME имя таблицы, где хранятся расписания
		 */
		const TABLE_NAME = 'cms3_appointment_employee_schedule';
		/**
		 * @const string DAY_NUMBER_FIELD_NAME название столбца для номера для недели
		 */
		const DAY_NUMBER_FIELD_NAME = 'day';
	}
?>