<?php
	/**
	 * Карта констант нового табличного контрола
	 */
	class tableControlMap implements iUmiMap{

		use tCommonMap;

		/**
		 * @const string FILTERS_REQUEST_KEY ключ данных формы с параметрами фильтрации
		 */
		const FILTERS_REQUEST_KEY = 'fields_filter';
		/**
		 * @const string ORDERS_REQUEST_KEY ключ данных формы с параметрами сортировки
		 */
		const ORDERS_REQUEST_KEY = 'order_filter';
		/**
		 * @const string LIMIT_REQUEST_KEY ключ данных формы с ограничение на количество
		 */
		const LIMIT_REQUEST_KEY = 'per_page_limit';
		/**
		 * @const string ключ данных с идентификатор связанной по иерархии сущности
		 */
		const RELATION_REQUEST_KEY = 'rel';
		/**
		 * @const string ключ данных с данными выбранных сущностей
		 */
		const SELECTED_LIST_REQUEST_KEY = 'selected_list';
		/**
		 * @const string ключ данных с режимом работы
		 */
		const MODE_REQUEST_KEY = 'mode';
		/**
		 * @const string ключ данных с режимом работы перетаскивания "после элемента"
		 */
		const DRAG_AFTER_REQUEST_KEY = 'after';
		/**
		 * @const string ключ данных с режимом работы перетаскивания "до элемента"
		 */
		const DRAG_BEFORE_REQUEST_KEY = 'before';
		/**
		 * @const string ключ данных с режимом работы перетаскивания "дочерним элементу"
		 */
		const DRAG_CHILD_REQUEST_KEY = 'child';
		/**
		 * @const string ключ данных с типом
		 */
		const TYPE_REQUEST_KEY = 'type';
		/**
		 * @const string ключ данных с идентификатором
		 */
		const ID_REQUEST_KEY = 'id';
		/**
		 * @const string PAGE_NUM_REQUEST_KEY ключ данных формы с номером текущей страницы
		 */
		const PAGE_NUM_REQUEST_KEY = 'p';
		/**
		 * @const string DATA_RESULT_KEY корневой ключ в данных ответа
		 */
		const DATA_RESULT_KEY = 'data';
		/**
		 * @const string ERROR_RESULT_KEY ключ ошибки в данных ответа
		 */
		const ERROR_RESULT_KEY = 'error';
		/**
		 * @const string SUCCESS_RESULT_KEY ключ успешной операции в данных ответа
		 */
		const SUCCESS_RESULT_KEY = 'success';
		/**
		 * @const string OFFSET_RESULT_KEY ключ смещения в данных ответа
		 */
		const OFFSET_RESULT_KEY = 'offset';
		/**
		 * @const string TOTAL_RESULT_KEY ключ общего количества в данных ответа
		 */
		const TOTAL_RESULT_KEY = 'total';
		/**
		 * @const string LIMIT_RESULT_KEY ключ ограничения на количество в данных ответа
		 */
		const LIMIT_RESULT_KEY = 'limit';
		/**
		 * @const string FORM_FIELD_NAME_PREFIX ключ с префиксом имен полей формы в данных ответа
		 */
		const FORM_FIELD_NAME_PREFIX = 'field_name_prefix';
	}
?>