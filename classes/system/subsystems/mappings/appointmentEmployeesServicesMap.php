<?php
	/**
	 * Карта констант коллекции связей между сотрудниками и услугами записи на прием
	 */
	class appointmentEmployeesServicesMap extends baseUmiCollectionMap {
		/**
		 * @const string TABLE_NAME имя таблицы, где хранятся связи сотрудник-услуга
		 */
		const TABLE_NAME = 'cms3_appointment_employees_services';
	}
?>