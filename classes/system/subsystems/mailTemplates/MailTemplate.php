<?php

	/**
	 * Class MailTemplate
	 * Шаблон писем
	 */
	class MailTemplate implements
		iUmiCollectionItem,
		iUmiDataBaseWorker,
		iUmiMapWorker,
		iClassConfigManager
	{
		use tUmiDataBaseWorker;
		use tCommonCollectionItem;
		use tUmiMapWorker;
		use tClassConfigManager;

		/** @const string символ, обрамляющий идентификаторы (placeholders) полей в шаблоне */
		const FIELD_WRAPPER_SYMBOL = '%';

		/** @var string имя шаблона */
		private $name;
		/** @var string содержимое шаблона */
		private $content;
		/** @var string класс коллекции шаблонов */
		private $collectionClass = 'MailTemplatesCollection';
		/** @var array конфигурация класса */
		private static $classConfig = [
			'fields' => [
				[
					'name' => 'ID_FIELD_NAME',
					'required' => true,
					'unchangeable' => true,
					'setter' => 'setId',
					'getter' => 'getId'
				],
				[
					'name' => 'NAME_FIELD_NAME',
					'required' => true,
					'setter' => 'setName',
					'getter' => 'getName'
				],
				[
					'name' => 'CONTENT_FIELD_NAME',
					'required' => true,
					'setter' => 'setContent',
					'getter' => 'getContent'
				]
			]
		];

		/** @return MailTemplatesCollection */
		public function getCollectionClass() {
			return $this->collectionClass;
		}

		/**
		 * Устанавливает ID шаблона
		 * @param int $id новый ID шаблона
		 */
		public function setId($id) {
			$this->setDifferentValue('id', $id, 'int');
		}

		/**
		 * Устанавливает имя шаблона
		 * @param string $name новое имя шаблона
		 */
		public function setName($name) {
			$this->setDifferentValue('name', $name, 'string');
		}

		/**
		 * Устанавливает содержимое шаблона
		 * @param string $content новое содержимое шаблона
		 */
		public function setContent($content) {
			$this->setDifferentValue('content', $content, 'string');
		}

		/**
		 * Возвращает ID шаблона
		 * @return int
		 */
		public function getId() {
			return $this->id;
		}

		/**
		 * Возвращает имя шаблона
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * Возвращает содержимое шаблона
		 * @return string
		 */
		public function getContent() {
			return $this->content;
		}

		/** {@inheritdoc} */
		public function commit() {
			if (!$this->isUpdated()) {
				return false;
			}

			$map = $this->getMap();
			$connection = $this->getConnection();
			$tableName = $connection->escape($map->get('TABLE_NAME'));
			$idField = $connection->escape($map->get('ID_FIELD_NAME'));
			$nameField = $connection->escape($map->get('NAME_FIELD_NAME'));
			$contentField = $connection->escape($map->get('CONTENT_FIELD_NAME'));

			$id = $this->getId();
			$name = $connection->escape($this->getName());
			$content = $connection->escape($this->getContent());

			$sql = <<<SQL
UPDATE `$tableName`
	SET `$nameField` = '$name', `$contentField` = '$content'
		WHERE `$idField` = $id;
SQL;
			$connection->query($sql);

			return true;
		}

		/**
		 * Возвращает обработанное содержимое шаблона, такое, что в нем (содержимом)
		 * заменены вставки идентификаторов полей на конкретные значения
		 * @param array $params массив идентификаторов полей и их значений
		 * @return mixed
		 */
		public function getProcessedContent(array $params) {
			$fields = array_keys($params);
			$values = array_values($params);
			$fieldRegEx = sprintf('/^%1$s?([^%1$s]+)%1$s?$/', self::FIELD_WRAPPER_SYMBOL);

			$wrappedFields = array_map(function($value) use ($fieldRegEx) {
				return preg_replace($fieldRegEx, sprintf('%1$s$1%1$s', self::FIELD_WRAPPER_SYMBOL), $value);
			}, $fields);

			return str_replace($wrappedFields, $values, $this->getContent());
		}

		/**
		 * Устанавливает значение свойства в случае, если оно отличается от текущего
		 * @param string $property имя свойства
		 * @param mixed $value новое значение свойства
		 * @param string $type имя типа значения свойства (элементарный PHP-тип)
		 * @return bool
		 */
		protected function setDifferentValue($property, $value, $type) {
			$newValue = $value;
			settype($newValue, $type);

			if ($this->$property != $newValue) {
				$this->setUpdatedStatus(true);
				$this->$property = $newValue;
				return true;
			}

			return false;
		}

	}
?>
