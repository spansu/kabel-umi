<?php
/**
	* Класс, который реализует сборщик мусора: подчищает кеш, удаляет лишнии записи из базы и т.д..
*/
	class garbageCollector implements iGarbageCollector {
		protected $maxIterations = 50000, $executedIterations;

		/**
			* Запустить сборщик мусора. Сбрасывает счетчик итераций в ноль.
		*/
		public function run() {
			$this->executedIterations = 0;
			$this->deleteStaticCache();
			$this->deleteBrokenDBRelations();

		}

		/**
			* Удалить старый статический html-кеш
			* @param Boolean $ignoreMaxLifeTime = false игнорировать время жизни кеша и удалять все
		*/
		protected function deleteStaticCache($ignoreMaxLifeTime = false) {
			if($ignoreMaxLifeTime) {
				$ttl = 0;
			} else {
				$ttl = (int) staticCache::getExpireTime();
			}
			$this->deleteDirectory("./static/userCache", $ttl);
			$this->deleteDirectory("./static/cacheElementsRelations", $ttl);
			$this->deleteDirectory("./static/cacheObjectsRelations", $ttl);

			$config = mainConfiguration::getInstance();
			$this->deleteDirectory($config->includeParam('system.static-cache'), $ttl);
		}

		/**
			* Рекурсивно удалить содержание директории, учитывая максимальное время жизни файла
			* @param String $directoryPath путь до директории, которую необходимо удалить
			* @param Integer $ttl максимальное время жизни файлов в этой директории (в секундах)
		*/
		protected function deleteDirectory($directoryPath, $ttl = 0) {
			$time = time();

			if($this->checkDirectory($directoryPath) == false) {
				return false;
			}

			$dir = new umiDirectory($directoryPath);
			foreach($dir as $item) {
				$this->checkMaxIterations();

				if($item instanceof umiDirectory) {
					$this->deleteDirectory($item->getPath(), $ttl);
					$item->delete();
				} else if ($item instanceof umiFile) {
					if($item->getModifyTime() <= ($time - $ttl)) {
						$item->delete();
					}
				} else {
					throw new coreException("Got unexpected result of type \"" . gettype($item) . "\"");
				}
			}
		}

		/**
			* Проверить директорию на существование и возможность перезаписи
			* @param String $directoryPath путь до проверяемой директории
			* @return Boolean true, если директория существует и перезаписываемая
		*/
		protected function checkDirectory($directoryPath) {
			if(is_dir($directoryPath)) {
				if(is_writable($directoryPath)) {
					return true;
				}
			}
			return false;
		}

		/**
			* Проверить, не превысили ли мы лимит по количеству итераций
		*/
		public function checkMaxIterations() {
			if(++$this->executedIterations > $this->maxIterations) {
				throw new maxIterationsExeededException("Maximum iterations count reached: " . $this->maxIterations);
			}
		}

		/**
			* Удалить мертвые связи в таблицах
		*/
		protected function deleteBrokenDBRelations() {
			if(defined("DB_DRIVER") && DB_DRIVER == "xml") {
				return false;
			}

			// Удаление объектов несуществующих типов
			$this->deleteBrokenForeignRelations("cms3_objects", "type_id", "cms3_object_types", "id");

			// Удаление страниц иерархии, которые ссылаются на несуществующие объекты
			$this->deleteBrokenForeignRelations("cms3_hierarchy", "obj_id", "cms3_objects", "id");

			// Удаление страниц иерархии, у которых отсутствуют родители
			$this->deleteBrokenForeignRelations("cms3_hierarchy", "rel", "cms3_hierarchy", "id");

			//Удаление пустых свойств объектов
			$this->deleteEmptyObjectProperties();
		}

		/**
		 * Удалить связанные записи таблицы $leftTableName, если нет соответствия полей $leftColumnName
		 * в таблице $rightTableName по полю $rightColumnName
		 * @param String $leftTableName название проверяемой таблицы
		 * @param String $leftColumnName название столбца, который связывает проверяемую таблицу
		 * @param String $rightTableName название таблицы, с которой связана проверяемая таблица
		 * @param String $rightColumnName название столбца в связанной таблице
		 * @throws coreException
		*/
		protected function deleteBrokenForeignRelations($leftTableName, $leftColumnName, $rightTableName, $rightColumnName) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$leftTableName = $connection->escape($leftTableName);
			$leftColumnName = $connection->escape($leftColumnName);
			$rightTableName = $connection->escape($rightTableName);
			$rightColumnName = $connection->escape($rightColumnName);

			$leftTableSql = "SELECT `id`, `{$leftColumnName}` FROM `{$leftTableName}`";
			$leftTableResult = $connection->queryResult($leftTableSql, true);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($leftTableSql));
			}

			$leftTableResult->setFetchType(IQueryResult::FETCH_ROW);

			foreach ($leftTableResult as $row) {
				$id = array_shift($row);
				$referenceValue = array_shift($row);

				$this->checkMaxIterations();

				if (!$referenceValue) {
					continue;
				}

				$rightTableSql = "SELECT COUNT(*) FROM `{$rightTableName}` WHERE `{$rightColumnName}` = '{$referenceValue}'";
				$rightTableResult = $connection->queryResult($rightTableSql, true);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($rightTableSql));
				}

				$rightTableResult->setFetchType(IQueryResult::FETCH_ROW);
				if ($rightTableResult->length() > 0) {
					$countRow = $rightTableResult->fetch();
					$count = array_shift($countRow);
				} else {
					$count = 0;
				}

				if ($count > 0) {
					continue;
				}

				$deleteRelationsSql = "DELETE FROM `{$leftTableName}` WHERE `id` = '{$id}'";
				$connection->query($deleteRelationsSql);

				if ($connection->errorOccurred()) {
					throw new coreException($connection->errorDescription($deleteRelationsSql));
				}
			}
		}

		/**
		 * Удалить пустые свойства объектов, то есть пустые записи в таблице cms3_object_content
		 * @throws coreException
		*/
		protected function deleteEmptyObjectProperties() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$limit = $this->maxIterations - $this->executedIterations;
			$sql = <<<SQL
DELETE FROM `cms3_object_content`
WHERE
  `int_val` IS NULL AND
  `varchar_val` IS NULL AND
  `text_val` IS NULL AND
  `rel_val` IS NULL AND
  `tree_val` IS NULL AND
  `float_val` IS NULL
ORDER BY `obj_id`, `field_id`
LIMIT $limit
SQL;

			$connection->queryResult($sql, true);
			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}
		}
	}
?>
