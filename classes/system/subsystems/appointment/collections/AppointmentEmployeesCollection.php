<?php
	/**
	 * Класс коллекции сотрудников записи на прием
	 */
	class AppointmentEmployeesCollection implements
		iUmiCollection,
		iUmiDataBaseWorker,
		iUmiService,
		iUmiMapWorker,
		iClassConfigManager
	{
		use tUmiDataBaseWorker;
		use tUmiService;
		use tCommonCollection;
		use tUmiMapWorker;
		use tClassConfigManager;

		/**
		 * @var string $collectionItemClass класс элемента коллекции, с которым она работает
		 */
		private $collectionItemClass = 'AppointmentEmployee';

		/** @var array конфигурация класса */
		private static $classConfig = [
			'service' => 'AppointmentEmployees',
			'fields' => [
				[
					'name' => 'ID_FIELD_NAME',
					'type' => 'INTEGER_FIELD_TYPE',
					'used-in-creation' => false
				],
				[
					'name' => 'NAME_FIELD_NAME',
					'type' => 'STRING_FIELD_TYPE',
					'required' => true,
				],
				[
					'name' => 'PHOTO_FIELD_NAME',
					'type' => 'IMAGE_FIELD_TYPE',
					'required' => true,
				],
				[
					'name' => 'DESCRIPTION_FIELD_NAME',
					'type' => 'STRING_FIELD_TYPE',
					'required' => true,
				],
			],
			'create-prepare-instancing-callback' => 'convertFilePathToUmiImage'
		];

		/**
		 * {@inheritdoc}
		 */
		public function getCollectionItemClass() {
			return $this->collectionItemClass;
		}

		/**
		 * {@inheritdoc}
		 */
		public function getTableName() {
			return $this->getMap()->get('TABLE_NAME');
		}

		/**
		 * Обработчик метода tCommonCollection::create()#create-prepare-instancing-callback.
		 * Конвертирует пути до изображений в объекты umiImageFile в списке параметров для инициализации объекта
		 * класса элемента колллекции.
		 * @param array $fields имена полей
		 * @param array $values значения полей
		 * @param array $fieldsConfig настройки полей
		 * @return array
		 */
		public function convertFilePathToUmiImage(array $fields, array $values, array $fieldsConfig) {
			$map = $this->getMap();

			foreach ($fieldsConfig as $fieldConfig) {
				$fieldType = $map->get($fieldConfig['type']);
				$fieldName = $map->get($fieldConfig['name']);

				if ($fieldType === $map->get('IMAGE_FIELD_TYPE') && isset($values[$fieldName])) {
					$values[$fieldName] = new umiImageFile($values[$fieldName]);
				}
			}

			return $values;
		}
	}
?>
