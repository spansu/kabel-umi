<?php
	/**
	 * Класс коллекции связей сотрудников с услугами для записи на прием
	 */
	class AppointmentEmployeesServicesCollection implements
		iUmiCollection,
		iUmiDataBaseWorker,
		iUmiService,
		iUmiMapWorker,
		iClassConfigManager
	{
		use tUmiDataBaseWorker;
		use tUmiService;
		use tCommonCollection;
		use tUmiMapWorker;
		use tClassConfigManager;

		/**
		 * @var string $collectionItemClass класс элемента коллекции, с которым она работает
		 */
		private $collectionItemClass = 'AppointmentEmployeeService';
		/** @var array конфигурация класса */
		private static $classConfig = [
			'service' => 'AppointmentEmployeesServices',
			'fields' => [
				[
					'name' => 'ID_FIELD_NAME',
					'type' => 'INTEGER_FIELD_TYPE',
					'used-in-creation' => false
				],
				[
					'name' => 'EMPLOYEE_ID_FIELD_NAME',
					'type' => 'INTEGER_FIELD_TYPE',
					'required' => true,
				],
				[
					'name' => 'SERVICE_ID_FIELD_NAME',
					'type' => 'INTEGER_FIELD_TYPE',
					'required' => true,
				],
			]
		];

		/**
		 * {@inheritdoc}
		 */
		public function getCollectionItemClass() {
			return $this->collectionItemClass;
		}

		/**
		 * {@inheritdoc}
		 */
		public function getTableName() {
			return $this->getMap()->get('TABLE_NAME');
		}
	}
?>