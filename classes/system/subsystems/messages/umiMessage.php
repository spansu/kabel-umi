<?php
	class umiMessage extends umiEntinty implements iUmiEntinty, iUmiMessage {
		protected $store_type = 'message', $title, $content, $senderId, $createTime, $type, $priority, $isSended;

		public function getTitle() {
			return $this->title;
		}
		
		public function setTitle($title) {
			$this->title = (string) $title;
			$this->setIsUpdated();
		}
		
		public function getContent() {
			return $this->content;
		}
		
		public function setContent($content) {
			$this->content = (string) $content;
			$this->setIsUpdated();
		}
		
		public function getSenderId() {
			return $this->senderId;
		}
		
		public function setSenderId($senderId = null) {
			$this->senderId = (int) $senderId;
			$this->setIsUpdated();
		}
		
		public function getType() {
			return $this->type;
		}
		
		public function setType($type) {
			if(in_array($type, umiMessages::getAllowedTypes()) == false) {
				throw new coreException("Unknown message type \"{$type}\"");
			}
			
			$this->type = (string) $type;
			$this->setIsUpdated();
		}
		
		public function getPriority() {
			return $this->priority;
		}
		
		public function setPriority($priority = 0) {
			$this->priority = (int) $priority;
			$this->setIsUpdated();
		}
		
		public function getCreateTime() {
			return $this->createTime;
		}
		
		public function setCreateTime($time) {
			$this->createTime = ($time instanceof umiDate) ? $time : new umiDate($time);
			$this->setIsUpdated();
		}
		
		public function getIsSended() {
			return $this->isSended;
		}
		
		public function getRecipients() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$id = (int) $this->id;
			$sql = <<<SQL
SELECT `recipient_id` FROM `cms3_messages_inbox` WHERE `message_id` = '{$id}'
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			
			$recipients = array();

			foreach ($result as $row) {
				$recipients[] = array_shift($row);
			}

			return $recipients;
		}
		
		public function send($recipients) {
			if($this->getIsSended()) {
				return false;
			}
			
			if(sizeof($recipients)) {
				$connection = ConnectionPool::getInstance()->getConnection();
				$recipientsSql = implode(", ", array_map('intval', $recipients));
				
				$id = (int) $this->id;
				
				$sql = <<<SQL
INSERT INTO `cms3_messages_inbox`
	(`message_id`, `recipient_id`)
		SELECT '{$id}', `id` FROM `cms3_objects`
			WHERE `id` IN ({$recipientsSql})
SQL;
				$connection->query($sql);
			}
			$this->setIsSended(true);
			$this->setIsUpdated();
		}
		
		public function setIsOpened($isOpened, $userId = false) {
			if ($userId == false) {
				$userId = permissionsCollection::getInstance()->getUserId();
			} else {
				$userId = (int) $userId;
			}

			$connection = ConnectionPool::getInstance()->getConnection();
			$isOpened = (int) $isOpened;
			$id = (int) $this->id;
			
			$sql = <<<SQL
UPDATE `cms3_messages_inbox` SET `is_opened` = '{$isOpened}' WHERE `message_id` = '{$id}' AND `recipient_id` = '{$userId}'
SQL;
			$connection->query($sql);
		}
		
		private function setIsSended($isSended) {
			$this->isSended = (bool) $isSended;
		}

		protected function loadInfo($row = false) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$id = (int) $this->id;
			$sql = <<<SQL
SELECT `title`, `content`, `sender_id`, `create_time`, `type`, `priority`, `is_sended`
	FROM `cms3_messages` WHERE `id` = '{$id}'
SQL;
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			if ($result->length() > 0) {
				$row = $result->fetch();
				$this->title = (string) $row['title'];
				$this->content = (string) $row['content'];
				$this->senderId = (int) $row['sender_id'];
				$this->createTime = new umiDate($row['create_time']);
				$this->type = (string) $row['type'];
				$this->priority = (int) $row['priority'];
				$this->isSended = (bool) $row['is_sended'];
			}
		}
		
		protected function save() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$title = $connection->escape($this->title);
			$content  = $connection->escape($this->content);
			$senderId = $this->senderId ? $this->senderId : 'NULL';
			$createTime = $this->createTime->getDateTimeStamp();
			$priority = (int) $this->priority;
			$type = $this->type;
			$isSended = (int) $this->isSended;
			$id = (int) $this->id;
			
			$sql = <<<SQL
UPDATE `cms3_messages`
	SET `title` = '{$title}', `content` = '{$content}',
		`create_time` = '{$createTime}', `priority` = '{$priority}',
		`type` = '{$type}', `sender_id` = {$senderId}, `is_sended` = '{$isSended}'
			WHERE `id` = '{$id}'
SQL;
			$connection->query($sql);
		}
	};
?>