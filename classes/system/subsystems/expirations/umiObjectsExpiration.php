<?php
	class umiObjectsExpiration extends singleton implements iSingleton, iUmiObjectsExpiration {
		protected $defaultExpires = 86400;

		protected function __construct() {

		}

		/**
		 * @param null $c
		 * @return umiObjectsExpiration
		 */
		public static function getInstance($c = NULL) {
			return parent::getInstance(__CLASS__);
		}

		public function isExpirationExists($objectId) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
			SELECT
				`obj_id`
			FROM
				`cms3_objects_expiration`
			WHERE
				`obj_id` = {$objectId}
			LIMIT 1
SQL;
			$queryResult = $connection->queryResult($sql);
			return $queryResult->length() > 0;
		}

		public function getExpiredObjectsByTypeId($typeId, $limit = 50) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$time = time();
			$sql = <<<SQL
			SELECT
				`obj_id`
			FROM
				`cms3_objects_expiration`
			WHERE
				`obj_id`  IN (
					SELECT
						`id`
					FROM
						`cms3_objects`
					WHERE
						`type_id`='{$typeId}'
					)
				AND (`entrytime` +  `expire`) <= {$time}
			ORDER BY (`entrytime` +  `expire`)
			LIMIT {$limit}
SQL;

			$result = array();
			$queryResult = $connection->queryResult($sql);
			if ($queryResult->length() > 0) {
				$queryResult->setFetchType(IQueryResult::FETCH_ASSOC);
				foreach ($queryResult as $row) {
					$result[] = $row['obj_id'];
				}
			}

			return $result;
		}

		public function update($objectId, $expires = false) {
			if ($expires == false) {
				$expires = $this->defaultExpires;
			}
			$connection = ConnectionPool::getInstance()->getConnection();
			$objectId = (int) $objectId;
			$expires = (int) $expires;
			$time = time();
			$sql = <<<SQL
			UPDATE
				`cms3_objects_expiration`
			SET
				`entrytime`='{$time}',
				`expire`='{$expires}'
			WHERE
				`obj_id` = '{$objectId}'
SQL;
			$connection->query($sql);
		}

		public function add($objectId, $expires = false) {
			if ($expires == false) {
				$expires = $this->defaultExpires;
			}
			$connection = ConnectionPool::getInstance()->getConnection();
			$objectId = (int) $objectId;
			$expires = (int) $expires;
			$time = time();

			$sql = <<<SQL
INSERT INTO `cms3_objects_expiration`
	(`obj_id`, `entrytime`, `expire`)
		VALUES ('{$objectId}', '{$time}', '{$expires}')
SQL;
			$connection->query($sql);
		}

		public function clear($objectId) {
			$objectId = (int) $objectId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = <<<SQL
DELETE FROM `cms3_objects_expiration`
	WHERE `obj_id` = '{$objectId}'
SQL;
			$connection->query($sql);
		}

		/**
		 * @deprecated
		 * Больше не используется
		 */
		public function run() {
			return;
		}
	};
?>