<?php
/**
 * Интерфейс соединения с базой данных
 * Пример использования:
 *		$connection = new Connection('localhost', 'root', '', 'umi');
 *    $connection->open();
 *		$connection->query('SHOW TABLES');
 *		$connection->close();
 */
interface IConnection {
	/**
	 * Конструктор соединения
	 * @param String $host хост СУБД
	 * @param String $login имя пользователя БД
	 * @param String $password пароль к БД
	 * @param String $dbname имя БД
	 * @param bool|int $port порт
	 * @param Boolean $persistent true - для сохранения подключения открытым
	 * @param Boolean $critical true - если функционирование подключения критично для системы
	 */
	public function __construct($host, $login, $password, $dbname, $port = false, $persistent = false, $critical = true);

	/**
	 * Устанавливает логгер запросов
	 * @param iMysqlLogger $mysqlLogger
	 */
	public function setLogger(iMysqlLogger $mysqlLogger);
	/**
	 * Открывает соединение
	 * @return Boolean
	 */
	public function open();
	/**
	 * Закрывает текущее соединение
	 */
	public function close();
	/**
	 * Выполняет запрос к БД
	 * @param String  $queryString строка запроса
	 * @param Boolean $noCache true - кэшировать результат, false - не кэшировать
	 * @return Resource|mysqli_result|bool результат выполнения запроса
	 */
	public function query($queryString, $noCache = false);
	/**
	 * Возвращает количество запросов к бд
	 * @return int
	 */
	public function getQueriesCount();
	/**
	 * Выполняет запрос к БД
	 * @param String  $queryString строка запроса
	 * @param Boolean $noCache true - кэшировать результат, false - не кэшировать
	 * @return IQueryResult результат выполнения запроса
	 */
	public function queryResult($queryString, $noCache = false);
	/**
	 * Проверяет, успешно ли завершен последний запрос
	 * @return Boolean true в случае возникновения ошибки, иначе false
	 */
	public function errorOccurred();
	/**
	 * Возвращает описание последней возникшей ошибки
	 * @param string|null $sqlQuery запрос, который привел к ошибке
	 * @return String
	 */
	public function errorDescription($sqlQuery = null);
	/**
	 * Возвращает признак открыто соединение или нет
	 * @return Boolean
	 */
	public function isOpen();
	/**
	* Экранирует входящую строку
	* @param String $input строка для экранирования
	* @return string
	*/
	public function escape($input);

	/**
	* Возвращает массив с описанием соединения:
	*@return Array
	*
	*/
	public function getConnectionInfo();

	/**
	 * Возвращает автоматически генерируемый ID, используя последний запрос
	 * @return int
	 */
	public function insertId();

	/**
	 * Возвращает численный код ошибки выполнения последней операции с MySQL
	 * @return int
	 */
	public function errorNumber();

	/**
	 * Возвращает строку, содержащую версию сервера MySQL
	 * @return string|null
	 */
	public function getServerInfo();

	/**
	 * Очищает кеш запросов
	 * @return void
	 */
	public function clearCache();

	/**
	 * Возвращает текст ошибки последней операции с MySQL
	 * @return string
	 */
	public function errorMessage();

	/**
	 * Возвращает число затронутых прошлой операцией строк
	 * @return int
	 */
	public function affectedRows();
};
?>
