<?php
	interface iUmiImageFile {
		public function getWidth();
		public function getHeight();
		public function getAlt();
		public function setAlt($alt);
	}
?>