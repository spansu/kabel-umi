<?php
	interface iUmiFile {
		public function __construct($filePath);
		public function delete();

		public static function upload($group_name, $var_name, $target_folder, $id = false);

		public function getSize();
		public function getExt();
		public function getFileName();
		public function getDirName();
		public function getModifyTime();
		public function getFilePath($webMode = false);

		public function getIsBroken();

		public function __toString();
		
		public static function getUnconflictPath($path);
		
		public function download($deleteAfterDownload = false);

		public function getOrder();
		public function setOrder($order);

		public function getId();
		public function setId($id);
	}
?>