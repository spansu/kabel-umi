<?php
	/**
	 * Интерфейс сервиса
	 */
	interface iUmiService {

		/**
		 * Конструктор
		 * @param string $serviceName имя сервиса
		 */
		public function __construct($serviceName);

		/**
		 * Возвращает имя сервиса
		 * @return string
		 */
		public function getServiceName();
	}
?>