<?php
	/**
	 * Интерфейс коллекции сущностей
	 */
	interface iUmiCollection {

		/**
		 * Возвращает класс элементов коллекции
		 * @return mixed
		 */
		public function getCollectionItemClass();

		/**
		 * Возвращает имя таблицы, где хранятся элементы коллекции
		 */
		public function getTableName();

		/**
		 * Возвращает список сущностей, в соответствии с настройками
		 * @param array $params настройки
		 * @return iUmiCollectionItem[]
		 */
		public function get(array $params);

		/**
		 * Возвращает количество сущностей, в соответствии с настройками
		 * @param array $params настройки
		 * @return int
		 */
		public function count(array $params);

		/**
		 * Удаляет сущности, в соответствии с настройками
		 * @param array $params настройки
		 * @return bool
		 */
		public function delete(array $params);

		/**
		 * Создает и возвращает сущность, в соответствии с настройками
		 * @param array $params настройки
		 * @return iUmiCollectionItem
		 */
		public function create(array $params);

		/**
		 * Существуют ли сущности, соответствующие настройкам
		 * @param array $params настройки
		 * @return bool
		 */
		public function isExists(array $params);

		/**
		 * Импортирует список сущностей и возвращает результат
		 * @param array $data данные сущностей
		 * @return array
		 */
		public function import(array $data);

		/**
		 * Возвращает данные сущностей, соответствующих настройкам
		 * @param array $params настройки
		 * @return array
		 */
		public function export(array $params);

		/**
		 * Возвращает условия выборки
		 * @param array $params настройки
		 * @return mixed
		 */
		public function getFieldsConditions(array $params);
	}
?>