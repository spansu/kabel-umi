<?php
	/**
	 * Интерфейс работника с картой констант
	 */
	interface iUmiMapWorker {
		/**
		 * Возвращает экземпляр класса с константами
		 * @return iUmiMap
		 */
		public function getMap();

		/**
		 * Устанавливает экземпляр класса с константами
		 * @param iUmiMap $mapInstance экземпляр класса
		 */
		public function setMap(iUmiMap $mapInstance);
	}
?>