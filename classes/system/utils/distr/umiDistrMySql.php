<?php
	class umiDistrMySql extends umiDistrInstallItem {
		protected $tableName, $permissions, $sqls = Array();

		public function __construct($tableName = false) {
			if ($tableName !== false) {
				$this->tableName = $tableName;
				$this->readTableDefinition();
				$this->readData();
			}
		}

		public function pack() {
			return base64_encode(serialize($this));
		}

		public static function unpack($data) {
			return base64_decode(unserialize($data));
		}

		public function restore() {
			//TODO
			//TODO
		}


		protected function readTableDefinition() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SHOW CREATE TABLE {$this->tableName}";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ROW);
			list($umi_temp, $cont) = $result->fetch();
			$this->sqls[] = $cont;
		}


		protected function readData() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "SELECT * FROM {$this->tableName}";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);

			$rows = Array();

			foreach ($result as $row) {
				$rows[] = $row;
			}

			if ($rows) {
				$this->sqls[] = $this->generateInsertRows($rows);
			}
		}


		protected function generateInsertRow($row) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "INSERT INTO {$this->tableName} (";

			$fields = array_keys($row);
			$sz = sizeof($fields);
			for ($i = 0; $i < $sz; $i++) {
				$sql .= "`" . $fields[$i] . "`";

				if ($i < ($sz - 1)) {
					$sql .= ", ";
				}
			}
			unset($fields);

			$sql .= ") VALUES(";

			$sql_init = $sql;
			$sql = "";

			$values = array_values($row);
			$sz = sizeof($values);
			for ($i = 0; $i < $sz; $i++) {
				$sql .= $sql_init;
				
				$val = $values[$i];
				if (strlen($val)) {
					$val = "'" . $connection->escape($val) . "'";
				} else {
					$val = "NULL";
				}
				$sql .= $val;

				if($i < ($sz - 1)) {
					$sql .= ", ";
				}
			}
			unset($values);

			$sql .= ")";

			return $sql;
		}


		protected function generateInsertRows($rows) {
			$connection = ConnectionPool::getInstance()->getConnection();
			$sql = "INSERT INTO {$this->tableName} (";

			$fields = array_keys($rows[0]);
			$sz = sizeof($fields);
			for ($i = 0; $i < $sz; $i++) {
				$sql .= "`" . $fields[$i] . "`";

				if ($i < ($sz - 1)) {
					$sql .= ", ";
				}
			}
			unset($fields);

			$sql .= ") VALUES";

			for ($n = 0; $n < sizeof($rows); $n++) {
				$row = $rows[$n];

				$sql .= "(";
				$values = array_values($row);
				$sz = sizeof($values);
				for ($i = 0; $i < $sz; $i++) {
					$val = $values[$i];
					if (strlen($val)) {
						$val = "'" . $connection->escape($val) . "'";
					} else {
						$val = "NULL";
					}
					$sql .= $val;

					if($i < ($sz - 1)) {
						$sql .= ", ";
					}
				}
				unset($values);
				$sql .= ")";

				if($n < (sizeof($rows) - 1)) {
					$sql .= ", ";
				}
			}

			return $sql;
		}

	};
?>