<?php

    /**
     * Работа с изображениями с помощью модуля php5-gd
     * User: AO
     * Date: 16.02.16
     * Time: 12:05
     */
    class gdProcessor implements iImageProcessor
    {
        /**
         * Обрезка изображения
         * @param $imagePath
         * @param $top - координата y
         * @param $left - координата x
         * @param $width - ширина
         * @param $height - высота
         * @return bool
         */
        public function crop($imagePath, $top, $left, $width, $height)
        {
            if (!file_exists($imagePath)) return false;

            $file = new umiImageFile($imagePath);
            try {
                $file->crop($left, $width, $top, $height);
            } catch (coreException $e) {
                return false;
            }
            return true;
        }

        /**
         * Поворачиваем изображение на 90 градусов
         * @param $imagePath
         * @return bool
         */
        public function rotate($imagePath)
        {
            if (!file_exists($imagePath)) return false;
            $file = new umiImageFile($imagePath);
            try {
                $file->rotate();
            } catch (coreException $e) {
                return false;
            }
            return true;
        }

        /**
         * Изменение размера изображения
         * @param $imagePath
         * @param $width - новая ширина
         * @param $height - новая высота
         * @return bool
         */
        public function resize($imagePath, $width, $height)
        {
            if (!file_exists($imagePath)) return false;

            $oImage = new umiImageFile($imagePath);
            try {
                $oImage->resize($width, $height);
            } catch (coreException $e) {
                return false;
            }
            return true;
        }

        /**
         * Оптимизация размера изображения
         * @param $source
         * @param int $quality
         * @return bool
         */
        public function optimize($source, $quality=75){
            if (!file_exists($source)) return false;
            
            try {
                $info = @getimagesize($source);
            } catch (Exception $e) {
                return false;
            }

            if (!$info) return false;

            if ($info['mime'] == 'image/jpeg'){
                $image = imagecreatefromjpeg($source);

                if (!$image) return false;

                imagejpeg($image, $source, $quality);
                imagedestroy($image);
            } elseif ($info['mime'] == 'image/png') {
                $image = imagecreatefrompng($source);

                if (!$image) return false;

                imageAlphaBlending($image, true);
                imageSaveAlpha($image, true);

                $png_quality = 9 - (($quality * 9 ) / 100 );
                imagePng($image, $source, $png_quality);
                imagedestroy($image);
            }

            return true;
        }

        /**
         * Получаем mime-type изображения и его размеры
         * @param $source
         * @return array|bool
         */
        public function info($source){
            if (!file_exists($source)) return false;
            
            try {
                $info = @getimagesize($source);
            } catch (Exception $e) {
                return false;
            }    
            
            return array(
                'mime'=>$info['mime'],
                'height'=>$info[1],
                'width'=>$info[0]
            );
        }

        /**
         * Создание превью просто ресайз изображения
         * @param $source
         * @param $thumbPath
         * @param $width
         * @param $height
         * @return bool
         * @throws coreException
         */
        public function thumbnail($source, $thumbPath, $width, $height)
        {
            $image = new umiImageFile($source);

            $info = $this->info($source);

            $width_src = $image->getWidth();
            $height_src = $image->getHeight();

            $thumb = imagecreatetruecolor($width, $height);
            $thumb_white_color = imagecolorallocate($thumb, 255, 255, 255);

            $source_array = $image->createImage($source);
            $source = $source_array['im'];

            imagefill($thumb, 0, 0, $thumb_white_color);
            imagecolortransparent($thumb, $thumb_white_color);

            switch($info['mime']) {
                case 'image/gif':
                    imagealphablending($source, TRUE);
                    imagealphablending($thumb, TRUE);
                    break;
                case 'image/png':
                    imagealphablending($thumb, false);
                    imagesavealpha($thumb, true);
                    imagealphablending($source, false);
                    imagesavealpha($source, true);
                    break;

                default:
            }

            imagecopyresampled($thumb, $source, 0, 0, 0, 0, $width, $height, $width_src, $height_src);

            switch($info['mime']) {
                case 'image/gif':
                    $res = imagegif($thumb, $thumbPath);
                    break;
                case 'image/png':
                    $res = imagepng($thumb, $thumbPath);
                    break;
                default:
                    $res = imagejpeg($thumb, $thumbPath, IMAGE_COMPRESSION_LEVEL);
            }
            if(!$res) {
                throw new coreException(getLabel('label-errors-16008'));
            }
            imagedestroy($source);
            imagedestroy($thumb);
            return true;
        }

        /**
         * Создание превью с ресайзом изображения под нужный размер с сохранением пропорций
         * @param $sourcePath
         * @param $thumbPath
         * @param $width
         * @param $height
         * @param $width_src
         * @param $height_src
         * @param $offset_w
         * @param $offset_h
         * @param $isSharpen
         * @param int $quality
         * @return bool
         * @throws coreException
         */
        public function cropThumbnail($sourcePath, $thumbPath, $width, $height, $width_src, $height_src, $offset_w, $offset_h, $isSharpen, $quality=75)
        {

            $thumb = imagecreatetruecolor($width, $height);

            $image = new umiImageFile($sourcePath);
            $source_array  = $image->createImage($sourcePath);
            $source = $source_array['im'];
            $info = $this->info($sourcePath);
            $mime = $info['mime'];
            

            if ($width*4 < $width_src && $height*4 < $height_src) {
                $_TMP=array();
                $_TMP['width'] = round($width*4);
                $_TMP['height'] = round($height*4);

                $_TMP['image'] = imagecreatetruecolor($_TMP['width'], $_TMP['height']);

                if ($mime == 'image/gif') {
                    $_TMP['image_white'] = imagecolorallocate($_TMP['image'], 255, 255, 255);
                    imagefill($_TMP['image'], 0, 0, $_TMP['image_white']);
                    imagecolortransparent($_TMP['image'], $_TMP['image_white']);
                    imagealphablending($source, TRUE);
                    imagealphablending($_TMP['image'], TRUE);
                } else {
                    imagealphablending($_TMP['image'], false);
                    imagesavealpha($_TMP['image'], true);
                }
                imagecopyresampled($_TMP['image'], $source, 0, 0, $offset_w, $offset_h, $_TMP['width'], $_TMP['height'], $width_src, $height_src);

                imageDestroy($source);

                $source = $_TMP['image'];
                $width_src = $_TMP['width'];
                $height_src = $_TMP['height'];

                $offset_w = 0;
                $offset_h = 0;
                unset($_TMP);
            }

            if ($mime == 'image/gif') {
                $thumb_white_color = imagecolorallocate($thumb, 255, 255, 255);
                imagefill($thumb, 0, 0, $thumb_white_color);
                imagecolortransparent($thumb, $thumb_white_color);
                imagealphablending($source, TRUE);
                imagealphablending($thumb, TRUE);
            } else {
                imagealphablending($thumb, false);
                imagesavealpha($thumb, true);
            }

            imagecopyresampled($thumb, $source, 0, 0, $offset_w, $offset_h, $width, $height, $width_src, $height_src);
            if($isSharpen) $thumb = makeThumbnailFullUnsharpMask($thumb,80,.5,3);

            switch($mime) {
                case 'image/gif':
                    $res = imagegif($thumb, $thumbPath);
                    break;
                case 'image/png':
                    $png_quality = 9 - (($quality * 9 ) / 100 );
                    $res = imagepng($thumb, $thumbPath, $png_quality);
                    break;
                default:
                    $res = imagejpeg($thumb, $thumbPath, $quality);
            }
            if(!$res) {
                throw new coreException(getLabel('label-errors-16008'));
                return false;
            }

            imageDestroy($source);
            imageDestroy($thumb);
            
            return true;
        }

        /**
         * Возвращаем тип обработчика изображений
         * @return string
         */
        public function getLibType(){
            return 'gd';
        }
    }
?>