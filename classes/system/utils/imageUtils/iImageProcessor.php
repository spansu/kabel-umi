<?php
    /**
     * Интерфейс описывающий классы обработки изображений
     * User: AO
     * Date: 16.02.16
     * Time: 11:32
     */

    interface iImageProcessor
    {
        /**
         * Обрезка изображения
         * @param $imagePath
         * @param $top - координата y
         * @param $left - координата x
         * @param $width - ширина
         * @param $height - высота
         * @return mixed
         */
        public function crop($imagePath, $top, $left, $width, $height);

        /**
         * Поворот изображения
         * @param $imagePath
         * @return mixed
         */
        public function rotate($imagePath);

        /**
         * Изменение размера изображения
         * @param $imagePath
         * @param $width - новая ширина
         * @param $height - новая высота
         * @return mixed
         */
        public function resize($imagePath, $width, $height);

        /**
         * Возвращаем тип обработчика изображений
         * @return mixed
         */
        public function getLibType();

        /**
         * Оптимизация размера изображения
         * @param $source
         * @param $quality
         * @return mixed
         */
        public function optimize($source, $quality);

        /**
         * Получаем mime-type изображения и его размеры
         * @param $source
         * @return mixed
         */
        public function info($source);

        /**
         * Создание превью просто ресайз изображения
         * @param $source
         * @param $thumb
         * @param $width
         * @param $height
         * @return mixed
         */
        public function thumbnail($source,$thumb,$width,$height);

        /**
         * Создание превью с ресайзом изображения под нужный размер с сохранением пропорций
         * @param $source
         * @param $thumb
         * @param $width
         * @param $width_src
         * @param $height_src
         * @param $offsetw
         * @param $offseth
         * @param $height
         * @param $sharpen
         * @param $quality
         * @return mixed
         */
        public function cropThumbnail($source,$thumb,$width,$width_src, $height_src,$offsetw,$offseth,$height,$sharpen,$quality);
    }
?>