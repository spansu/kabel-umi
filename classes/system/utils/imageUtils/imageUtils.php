<?php
    /**
     * Набор функций для работы с изображениями
     * User: АО
     * Date: 16.02.16
     * Time: 11:40
     */

    class imageUtils
    {
        private static $processor = null; //Сслыка на созданный объект работы с изображениями


        /**
         * Загружаем в соотвествующий класс работы с изображениями исходя из установленных расширений php
         * @return gdProcessor|imageMagickProcessor|null
         * @throws Exception
         */
        public static function getImageProcessor()
        {
            if (self::$processor == null || get_class(self::$processor) == 'gdProcessor') {
                if (extension_loaded('imagick')) {
                    self::$processor = new imageMagickProcessor();
                } else if (extension_loaded('gd')) {
                    self::$processor = new gdProcessor();
                } else {
                    self::throwNoModuleException();
                }
            }
            return self::$processor;
        }

        /**
         * Загружаем класс работающий через расширение php-imagick
         * @return imageMagickProcessor|null
         * @throws Exception
         */
        public static function getImageMagickProcessor (){
            if (extension_loaded('imagick')) {
                if (self::$processor == null || get_class(self::$processor) !== 'imageMagickProcessor') {
                    self::$processor = new imageMagickProcessor();
                }
            } else {
                self::throwNoModuleException();
            }
            return self::$processor;
        }

        /**
         * Загружаем расширение работающие через php5-gd
         * @return gdProcessor|null
         * @throws Exception
         */
        public static function getGDProcessor (){
            if (extension_loaded('gd')) {
                if (self::$processor == null || get_class(self::$processor) !== 'gdProcessor') {
                    self::$processor = new gdProcessor();
                }
            } else {
                self::throwNoModuleException();
            }
            return self::$processor;
        }

        /**
         * Приветный метод генерирующий исключение с ошибкой отсутсвия модуля работы с изображениями
         * @throws Exception
         */
        private static function throwNoModuleException (){
            throw new Exception('It does not install any image processing module.');
        }

    }
?>