<?php

    /**
     * Работа с изображениями с помощью модуля php5-imagick
     * User: AO
     * Date: 16.02.16
     * Time: 11:53
     */
    class imageMagickProcessor implements iImageProcessor
    {
        /**
         * Обрезка изображения
         * @param $imagePath
         * @param $top - координата y
         * @param $left - координата x
         * @param $width - ширина
         * @param $height - высота
         * @return bool
         */
        public function crop($imagePath, $top, $left, $width, $height)
        {
            if (!file_exists($imagePath)) return false;

            $file = new Imagick($imagePath);
            if ($file->cropImage($width,$height,$left,$top)){
                $file->stripImage();
                $file->setImageCompressionQuality(IMAGE_COMPRESSION_LEVEL);
                $file->writeImage();
                $file->destroy();
            } else {
                return false;
            }
            return true;
        }

        /**
         * Поворачиваем изображение на 90 градусов
         * @param $imagePath
         * @return bool
         */
        public function rotate($imagePath)
        {
            if (!file_exists($imagePath)) return false;

            $file = new Imagick($imagePath);
            if ($file->rotateImage(new ImagickPixel('#ffffff'), 90)) {
                $file->stripImage();
                $file->setImageCompressionQuality(IMAGE_COMPRESSION_LEVEL);
                $file->writeImage();
                $file->destroy();
            } else {
                return false;
            }
            return true;
        }

        /**
         * Изменение размера изображения
         * @param $imagePath
         * @param $width - новая ширина
         * @param $height - новая высота
         * @return bool
         */
        public function resize($imagePath, $width, $height)
        {
            if (!file_exists($imagePath)) return false;

            $file = new Imagick($imagePath);
            if ($file->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1)) {
                $file->stripImage();
                $file->setImageCompressionQuality(IMAGE_COMPRESSION_LEVEL);
                $file->writeImage();
                $file->destroy();
            } else {
                return false;
            }

            return true;
        }

        /**
         * Оптимизация размера изображения
         * @param $source
         * @param int $quality
         * @return bool
         */
        public function optimize($source, $quality=75){
            if (!file_exists($source)) return false;

            $file = new Imagick($source);
            $file->setImageCompressionQuality($quality);
            $file->stripImage();
            $file->writeImage();
            $file->destroy();

            return true;
        }


        /**
         * Получаем mime-type изображения и его размеры
         * @param $source
         * @return array|bool
         */
        public function info($source){
            if (!file_exists($source)) return false;

            $file = new Imagick($source);

            $info = array(
                'mime'=>$file->getImageMimeType(),
                'height'=>$file->getImageHeight(),
                'width'=>$file->getImageWidth()
            );

            $file->destroy();

            return $info;
        }

        /**
         * Cоздание превью просто ресайз изображения
         * @param $source
         * @param $thumb
         * @param $width
         * @param $height
         * @return bool
         * @throws coreException
         */
        public function thumbnail($source, $thumb, $width, $height)
        {
            if (!file_exists($source)) return false;

            $file = new Imagick($source);
            $file->setBackgroundColor('rgb(255,255,255)');
            $file->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
            $file->setImageCompressionQuality(IMAGE_COMPRESSION_LEVEL);
            $file->stripImage();
            try{
                $file->writeImage($thumb);
            } catch (Exception $e){
                throw new coreException(getLabel('label-errors-16008'));
                return false;
            }
            $file->destroy();

            return true;
        }

        /**
         * Создание превью с ресайзом изображения под нужный размер с сохранением пропорций
         * @param $source
         * @param $thumb
         * @param $width
         * @param $height
         * @param $width_src
         * @param $height_src
         * @param $offset_w
         * @param $offset_h
         * @param $isSharpen
         * @param $quality
         * @return bool
         * @throws coreException
         */
        public function cropThumbnail($source, $thumb, $width, $height, $width_src, $height_src, $offset_w, $offset_h, $isSharpen, $quality=75)
        {
            $file = new Imagick($source);
            $mime = $file->getImageMimeType();
            $file->setGravity(Imagick::GRAVITY_CENTER);
            if (in_array($mime,array('image/png','image/gif'))){
                $file->setBackgroundColor(new ImagickPixel('transparent'));
            }


            $file->cropImage($width_src, $height_src, $offset_w, $offset_h );

            $file->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
            
            $file->unsharpMaskImage(0.5,0.5,80,3);
            $file->setImageCompressionQuality($quality);
            $file->stripImage();
            try{
                $file->writeImage($thumb);
            } catch (Exception $e){
                throw new coreException(getLabel('label-errors-16008'));
                return false;
            }
            $file->destroy();

            return true;
        }

        /**
         * Возвращаем тип обработчика изображений
         * @return string
         */
        public function getLibType(){
            return 'imagick';
        }


    }
?>