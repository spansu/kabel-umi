<?php
	/**
	 * Класс отвечает за генерацию массива с данными объектного свойства (класс umiObjectProperty).
	 * Этот массив используется в классе xmlTranslator для представления свойства в формате XML.
	 */
	class umiObjectPropertyWrapper extends translatorWrapper {
		/** Нужно ли выводить пустые поля */
		public static $showEmptyFields = false;

		/**
		 * Сгенерировать массив с данными свойства
		 * @param iUmiObjectProperty $data свойство объекта
		 * @return array
		 */
		public function translate($data) {
			return $this->translateData($data);
		}

		/**
		 * Сгенерировать массив с данными свойства
		 * @param iUmiObjectProperty $property свойство объекта
		 * @return array|bool
		 */
		protected function translateData(iUmiObjectProperty $property) {
			$translation = [];

			$value = $property->getValue();
			$field = $property->getField();

			if (!$field instanceof umiField) {
				return $translation;
			}

			$id = $field->getId();
			$name = $field->getName();
			$title = $field->getTitle();
			$type = $field->getFieldType();
			$isImportant = (int) $field->isImportant();
			$dataType = $type->getDataType();

			$isSensitive = ($dataType == "password" || in_array($name, ['user_dock', 'user_settings_data', 'activate_code']));

			if ($isSensitive && !xmlTranslator::$showUnsecureFields) {
				return false;
			}

			$hasValue = (is_object($value)) || (is_array($value) && !empty($value)) || (!is_array($value) && strlen($value));

			if ($hasValue || self::$showEmptyFields || translatorWrapper::$showEmptyFields) {
				$translation['@id'] = $id;
				$translation['@name'] = $name;
				$translation['@type'] = $dataType;

				if ($dataType == 'relation' && $field->getFieldType()->getIsMultiple()) {
					$translation['@multiple'] = 'multiple';
				}

				$translation['title'] = $title;
				$translation['@is-important'] = "$isImportant";

				if ($dataType == 'string') {
					$restriction = baseRestriction::get($field->getRestrictionId());
					if ($restriction instanceof baseRestriction) {
						$translation['@restriction'] = $restriction->getClassName();
					}
				}

				$translation['value'] = [];

				switch ($dataType) {
					case "swf_file":
					case "file":
					case "img_file": {
						if (!$value instanceof umiFile) {
							break;
						}

						$path = $value->getFilePath(true);
						$regexp = "|^" . CURRENT_WORKING_DIR . "|";
						$path = preg_replace($regexp, "", $path);

						if ($value->getIsBroken()) {
							$translation['value']['@path'] = '.' . $path;
							$translation['value']['@is_broken'] = 1;
							$translation['value']['#value'] = $path;
							break;
						}

						$pathInfo = getPathInfo($path);
						$translation['value']['@path'] = '.' . $path;
						$translation['value']['@folder'] = preg_replace($regexp, "", $pathInfo['dirname']);
						$translation['value']['@name'] = $pathInfo['filename'];
						$translation['value']['@ext'] = $pathInfo['extension'];

						if (in_array($dataType, ['img_file', 'swf_file'])) {
							$imageSize = @getimagesize('.' . $value);
							if (is_array($imageSize)) {
								$translation['value']['@width'] = $imageSize[0];
								$translation['value']['@height'] = $imageSize[1];
							}
						}

						$translation['value']['@is_broken'] = '0';
						$translation['value']['#value'] = $path;
						break;
					}

					case "symlink": {
						$translation['value']['nodes:page'] = [];
						foreach ($value as $element) {
							$translation['value']['nodes:page'][] = $element;
						}
						break;
					}

					case "relation": {
						$objects = umiObjectsCollection::getInstance();
						$translation['value']['nodes:item'] = [];
						if (is_array($value)) {
							foreach ($value as $objectId) {
								$translation['value']['nodes:item'][] = $objects->getObject($objectId);
							}
						} else {
							$translation['value']['item'] = $objects->getObject($value);
						}
						break;
					}

					case "date": {
						if ($value instanceof iUmiDate) {
							$magicMethods = ['get_editable_region', 'save_editable_region'];
							$cmsController = cmsController::getInstance();
							$format = (in_array($cmsController->getCurrentMethod(), $magicMethods)) ? false : 'r';

							$translation['value']['@formatted-date'] = $value->getFormattedDate("d.m.Y H:i");
							$translation['value']['@unix-timestamp'] = $value->getFormattedDate("U");
							$translation['value']['#rfc'] = ($value->getDateTimeStamp() > 0) ? $value->getFormattedDate($format) : "";
						}
						break;
					}

					case "optioned": {
						$options = [];
						$hierarchy = umiHierarchy::getInstance();
						$objects = umiObjectsCollection::getInstance();
						foreach ($value as $option) {
							$optionInfo = [];
							foreach ($option as $optionType => $optionValue) {
								switch ($optionType) {
									case "tree": {
										$element = $hierarchy->getElement($optionValue);
										if ($element instanceof iUmiHierarchyElement) {
											$optionInfo['page'] = $element;
										}
										break;
									}

									case "rel": {
										$object = $objects->getObject($optionValue);
										if ($object instanceof iUmiObject) {
											$optionInfo['object'] = $object;
										}
										break;
									}

									default: {
										$optionInfo['@' . $optionType] = $optionValue;
									}
								}
							}
							$options[] = $optionInfo;
						}

						$translation['value']['nodes:option'] = $options;
						break;
					}

					default: {
						if (is_array($value)) {
							unset($translation['value']);
							$translation['nodes:value'] = $value;
							if ($dataType == 'tags') {
								$translation['combined'] = implode(', ', $value);
							}
						} else {
							$cmsController = cmsController::getInstance();
							$value = xmlTranslator::executeMacroses($value, false, $property->getObjectId());

							if (defined("XML_PROP_VALUE_MODE") && $dataType == "wysiwyg" && $cmsController->getCurrentMode() != "admin") {
								if (XML_PROP_VALUE_MODE == "XML") {
									$translation['value'] = ["xml:xvalue" => "<xvalue>{$value}</xvalue>"];
									break;
								}
							}
							$translation['value']['#value'] = $value;
						}
					}
				}
			}

			return $translation;
		}
	}
?>
