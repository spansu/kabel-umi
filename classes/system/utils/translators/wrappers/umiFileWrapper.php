<?php
	class umiFileWrapper extends translatorWrapper {
		public function translate($data) {
			return $this->translateData($data);
		}
		
		protected function translateData(iUmiFile $file) {
			$resultArray = array(
				'attribute:id'		=> $file->getId(),
				'attribute:path'	=> $file->getFilePath(),
				'attribute:size'	=> $file->getSize(),
				'attribute:ext'		=> $file->getExt(),
				'attribute:ord'		=> $file->getOrder(),
				'node:src'			=> $file->getFilePath(true)
			);

			if ($file instanceof umiImageFile) {
				$resultArray['attribute:width'] = $file->getWidth();
				$resultArray['attribute:height'] = $file->getHeight();
				$resultArray['attribute:alt'] = $file->getAlt();
			}
			
			return $resultArray;
		}
	};
?>