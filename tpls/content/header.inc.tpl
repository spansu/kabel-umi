<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>%title%</title>
	<link rel="stylesheet" type="text/css" href="/css/grid.css" />
	<link rel="stylesheet" type="text/css" href="/css/decor.css" />	
	<script type="text/javascript" src="/js/cross-domain.php"></script>
	<script type="text/javascript" src="/js/equal.js"></script>
	%system includeQuickEditJs()%
	%system includeEditInPlaceJs()%
</head>
<body>
	<div class="main">
		<h1>
			<div id="zagolovok" umi:element-id="1" umi:field-name="name_for_template">
				%data getProperty(1,'name_for_template',default,0)%
			</div>
			<span id="profession" umi:element-id="1" umi:field-name="prof_for_template">
				%data getProperty(1,'prof_for_template',default,0)%
			</span>
		</h1>
		%content menu('sl')%