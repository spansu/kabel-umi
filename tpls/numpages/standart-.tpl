<?php
	$FORMS = Array();

	$FORMS['pages_block'] = <<<END

	<div class="numpage">
		<span>Страницы:</span> %toprev%  %tonext%
		<ul>%pages%</ul>
	</div>

END;



	$FORMS['pages_item'] = <<<END
	<li><a href="%link%">%num%</a></li>
END;

	$FORMS['pages_item_a'] = <<<END
	<li><span>%num%</span></li>
END;

	$FORMS['pages_quant'] = <<<END
END;

	$FORMS['pages_block_empty'] = <<<END

END;

	$FORMS['pages_toprev'] = <<<END
		<a href="%toprev_link%" id="toprev">Предыдущая</a>
END;

	$FORMS['pages_toprev_a'] = <<<END
		<u>Предыдущая</u>
END;

	$FORMS['pages_tonext'] = <<<END
		<a href="%tonext_link%" id="tonext">Следующая</a>
END;


	$FORMS['pages_tonext_a'] = <<<END
		<u>Следующая</u>
END;
?>