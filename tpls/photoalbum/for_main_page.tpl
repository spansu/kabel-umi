﻿<?php

$FORMS = Array();

$FORMS['albums_list_block'] = <<<END

Список альбомов:

%lines%

END;

$FORMS['albums_list_block_empty'] = <<<END

Фотоальбомов нет.

END;


$FORMS['albums_list_block_line'] = <<<END

<li>
	<a href="%link%">%name%</a>
</li>

END;


$FORMS['album_block'] = <<<END

<div umi:element-id="%id%" umi:module="photoalbum" umi:method="album" class="photos" id="holder">
%lines%
</div>
END;


$FORMS['album_block_empty'] = <<<END

<p>Фотогалерея пуста.</p>

END;


$FORMS['album_block_line'] = <<<END
			<div class="photo">
				<a href="%link%">
					%data getProperty('%id%', 'photo', 'preview_image')%
				</a>
				<h3><a href="%link%">%name%</a></h3>
				<p umi:element-id="%id%" umi:field-name="descr">%descr%</p>
			</div>
END;


$FORMS['photo_block'] = <<<END
			
		<div class="floatHolder">	
			<div class="photoBig">%data getProperty('%id%', 'photo', 'view_image')%</div>
			%photoalbum album(%parent_id%, 'small')%
		</div>
		<h2 umi:element-id="%h1%" umi:field-name="h1">%h1%</h2>
		<p umi:element-id="%id%" umi:field-name="descr">%descr%</p>

END;

?>