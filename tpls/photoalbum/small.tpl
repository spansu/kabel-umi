﻿<?php

$FORMS = Array();

$FORMS['albums_list_block'] = <<<END

Список альбомов:

%lines%

END;

$FORMS['albums_list_block_empty'] = <<<END

Фотоальбомов нет.

END;


$FORMS['albums_list_block_line'] = <<<END

<li>
	<a href="%link%">%name%</a>
</li>

END;


$FORMS['album_block'] = <<<END

<div class="gallerySmall">
%lines%
</div>

END;


$FORMS['album_block_empty'] = <<<END

<p>Фотогалерея пуста.</p>

END;


$FORMS['album_block_line'] = <<<END
	<a href="%link%">%data getProperty('%id%', 'photo', 'small_image')%</a>
	<!--%system ifEqual('%id%', '%pid%', '111', '222')%-->

END;



?>