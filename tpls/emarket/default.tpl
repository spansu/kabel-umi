<?php
$FORMS = array();
	
$FORMS['price_block'] = <<<END

<h5>Вывод цены</h5>
%price-original%
%price-actual%

%emarket discountInfo(%discount_id%)%

%currency-prices%


END;

$FORMS['price_original'] = <<<END
<!-- %currency_name% -->
<p>
	<strike>%prefix%&nbsp;%original%&nbsp;%suffix%</strike>
</p>
END;

$FORMS['price_actual'] = <<<END
%actual%
END;


$FORMS['order_block'] = <<<END
<h3>Информация о заказе:</h3>
<table width="600px" border="1" cellspacing="0" cellpadding="5">
	<tbody>
        <tr>
			<td><b>Наименования</b></td>
			<td><b>Количество</b></td>
			<td><b>Цена за ед.</b></td>
			<td><b>Цена</b></td>
		</tr>
		%items%
	</tbody>
</table>
<p>Товаров в заказе: <b>%total-amount%</b></p>
<p>Общая сумма заказа: <b>%total-price%</b></p>
END;



$FORMS['order_item'] = <<<END
<tr>
	
	<td>
		<a href="%link%">%name%</a>
	</td>
	
	<td>
		%amount%
	</td>
	
	<td>
		%price%
	</td>
	
	<td>
		%total-price%
	</td>

</tr>
END;

$FORMS['options_block'] = <<<END
Дополнительные опции: %items%
END;

$FORMS['options_block_empty'] = "---";

$FORMS['options_item'] = <<<END
%name% +%price%%list-comma%
END;

$FORMS['order_block_empty'] = <<<END
<p>Корзина пуста</p>
END;


$FORMS['purchase'] = <<<END
%purchasing%

%emarket ordersList()%
END;


$FORMS['orders_block'] = <<<END
<p>Список ваших заказов:</p>
<ul>
	%items%
</ul>
END;

$FORMS['orders_block_empty'] = <<<END
<p>Заказов нет</p>
END;

$FORMS['orders_item'] = <<<END
	<li>%name% (%id%)</li>
END;

$FORMS['purchase_successful'] = <<<END
<p>Заказ успешно добавлен</p>
END;

$FORMS['purchase_failed'] = <<<END
<p>Не удалось добавить заказ</p>
END;

?>