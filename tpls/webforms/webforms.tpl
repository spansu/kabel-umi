<?php

$FORMS = Array();

$FORMS['webforms_block'] = <<<BLOCK
<form action="/webforms/sendmail/" method="post" enctype="multipart/form-data">
	<input type="hidden" name="data[subject]" value="Обратная связь" />
	<table class="feedback">
		<tr>
			<td>Ваше имя:</td>
			<td>
				<input type="text" size="50" value="" name="data[fname]">
				<input type="hidden" name="labels[fname]" value="Имя" />
			</td>
		</tr>
		<tr>
			<td>Ваш e-mail:</td>
			<td>
				<input type="text" size="50" value="" name="data[email_from]">
			</td>
		</tr>
		<tr>
			<td valign="top">Сообщение:</td>
			<td><textarea name="message"></textarea></td>
		</tr>
	</table>

	%system captcha()%
	
	<div class="button">
		<input type="submit" value="Отправить" />
	</div>
</form>
BLOCK;

$FORMS['webforms_to_block'] = <<<TO_BLOCK
TO_BLOCK;

$FORMS['webforms_to_line'] = <<<TO_LINE
TO_LINE;

?>