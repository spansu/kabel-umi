﻿<?php

$FORMS = Array();

$FORMS['error_no_form'] = '<b>Форма не определена</b><br />Обратитесь к администрации ресурса';

$FORMS['send_successed'] = 'Ваше сообщение отправлено';

$FORMS['form_block'] = <<<END

<form enctype="multipart/form-data" method="post" action="/webforms/send/" class="webform">
	<input type="hidden" name="system_form_id" value="%form_id%" />
	<input type="hidden" name="system_template" value="%template%" />
	%address_select%
	%groups%
</form>
END;

$FORMS['address_select_block']  = <<<END
<table border="0" width="400">
    <tr>
        <td style="width:100%;">
            Получатель
        </td>

        <td>
            <select name="system_email_to" style="width: 300px">
                %options%
            </select>
        </td>
    </tr>
</table>
END;

$FORMS['address_select_block_line']  = <<<END
	<option value="%id%">%text%</option>
END;

$FORMS['address_separate_block']  = <<<END
<b>Выберите адреса из списка</b><br />
%lines%
<br />
END;

$FORMS['address_separate_block_line']  = <<<END
<input type="checkbox" id="%id%" name="system_email_to[]" value="%value%" /> <label for="%id%">%description%</label><br />
END;

$FORMS['reflection_block'] = <<<END
%groups%
%system captcha()%

<div class="item">
	<input type="submit" value="Отправить" class="submit">
</div>


END;

$FORMS['reflection_group'] = <<<END

	%fields%

END;

$FORMS['reflection_group_address'] = <<<END

<table border="0" width="500">
    <tr>
        <td>
            Получатель
        </td>

        <td>
            <select name="system_email_to" style="width:300px;">
                %options%
            </select>
        </td>
    </tr>
</table>


END;


$FORMS['reflection_field_string'] = <<<END
		<div class="item">
			<label>%title%:</label>
			<input type="text" name="%input_name%" value="%value%" class="text"/>
		</div>	

END;


$FORMS['reflection_field_text'] = <<<END
		<div class="item">
			<label>%title%:</label>
			<textarea name="%input_name%">%value%</textarea>
		</div>

END;

$FORMS['reflection_field_boolean'] = <<<END
		<div class="item">
			<label>%title%:</label>
			<input type="hidden" id="%input_name%" name="%input_name%" value="%value%" />
			<input onclick="javascript:document.getElementById('%input_name%').value = this.checked;" type="checkbox" %checked% value="1" />
		</div>
END;

$FORMS['reflection_field_file'] = <<<END
		<div class="item">
			    <label>%title%:</label>
			    <input type="file" name="%input_name%" style="width:300px;" />			
		</div>

END;

$FORMS['reflection_field_relation'] = <<<END
		<div class="item">
		    <label>%title%:</label>
		    <select name="%input_name%" style="width: 205px" class="textinputs" style="width:300px;">
			<option />
			%options%
		    </select>
		</div>

END;

$FORMS['reflection_field_relation_option'] = <<<END
    <option value="%id%">%name%</option>
END;


$FORMS['reflection_field_relation_option_a'] = <<<END
    <option value="%id%" selected="selected">%name%</option>
END;


$FORMS['reflection_field_multiple_relation'] = <<<END
	<div class="item">
            <label>%title%:</label>
            <select name="%input_name%" class="textinputs" multiple style="width:300px;">
                <option />
                %options%
            </select>
	</div>

END;

$FORMS['reflection_field_multiple_relation_option'] = <<<END
    <option value="%id%">%name%</option>
END;


$FORMS['reflection_field_multiple_relation_option_a'] = <<<END
    <option value="%id%" selected="selected">%name%</option>
END;

?>