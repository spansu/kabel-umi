<?php

if(isset($_GET['cid']) && intval($_GET['cid']) > 0) {
    $action = 'update/'.intval($_GET['cid']);
    $button_title = 'Обновить';
} else {
    $action = 'add/'.intval($_GET['type_id']);
    $button_title = 'Добавить';
}

?>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <script type="text/javascript" src="/js/jquery/jquery.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/jquery/jquery-ui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/jquery/jquery.umipopups.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/smc/compressed.js"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/jquery.inputmask.bundle.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/jquery.validate.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/popup_script.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/jquery.form.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/customer.js" charset="utf-8"></script>
        <link href="/styles/skins/_eip/css/popup_page.css" rel="stylesheet" type="text/css" />
        <link href="/styles/skins/_eip/css/symlink.control.css" rel="stylesheet" type="text/css" />
        <link href="/styles/skins/modern/data/modules/emarket/css/ext_createorder/popup_forms.css" rel="stylesheet" type="text/css" />
        <link href="/styles/skins/modern/data/modules/emarket/css/ext_createorder/form_styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body id="modal_page">
        <form enctype="multipart/form-data" action="/udata/emarket/co_ajaxSetCustomer/<?php echo $action ?>/.json" method="post" id="customerForm">
            <div id="customer-content"></div>
            <fieldset id="errors_overlay">
            </fieldset>
            <div class="eip_buttons">
                <input type="submit" value="<?php echo $button_title ?>" class="primary ok">
                <input type="button" value="Отменить" class="back" onclick="onCustomerClose()">
                <div style="clear:both;"></div>
            </div>
        </form>
    </body>
</html>