<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file"[
    <!ENTITY nbsp  "&#xA0;">
    <!ENTITY copy  "&#169;">
    <!ENTITY mdash "&#8212;">
    
    <!ENTITY laquo  "&#171;">
    <!ENTITY raquo  "&#187;">
    
    <!ENTITY rarr  "&#8594;">
    <!ENTITY larr  "&#8592;">  
]>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:php="http://php.net/xsl"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:udt="http://umi-cms.ru/2007/UData/templates"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                extension-element-prefixes="php"
                exclude-result-prefixes="xsl php udt date">
    
    <xsl:param name="lang-prefix" select="document('udata://emarket/co_getRequestVar/lang_prefix/')" />
    
    <xsl:template match="udata">
        <xsl:variable name="order-info" select="document(concat('uobject://', @id))/udata" />
        
        <xsl:choose>
            <xsl:when test="count(items/item) &gt; 0">
                <xsl:apply-templates select="items/item" mode="order-items-createorder" />
            </xsl:when>
            <xsl:otherwise>
                <tr>
                    <td colspan="7" style="text-align: center">
                        <xsl:text>В заказе пока нет товаров</xsl:text>
                    </td>
                </tr>
            </xsl:otherwise>
        </xsl:choose>
        <!--<tr>
            <td>
                <strong>
                    <xsl:text>Скидка на заказ (абсолютное значение)</xsl:text>
                </strong>
            </td>
            <td colspan="4">
                <a href="{$lang-prefix}/admin/emarket/discount_edit/{discount/@id}/">
                    <xsl:value-of select="discount/@name" />
                </a>
                <xsl:apply-templates select="document(concat('uobject://', discount/@id, '.discount_modificator_id'))//item" mode="discount-size" />
                <xsl:apply-templates select="discount/description" />
            </td>
            <td>
                <input type="text" class="default" name="order-discount-value" value="{discount_value}" size="3">
                    <xsl:attribute name="value">
                        <xsl:choose>
                            <xsl:when test="../summary/price/discount">
                                <xsl:value-of select="document(concat('udata://emarket/applyPriceCurrency/', ../summary/price/discount, '/'))/udata/price/actual" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="discount_value"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                </input>
            </td>
            <td/>
        </tr>-->
        <xsl:apply-templates select="discount" mode="order-summary"/>
        <xsl:apply-templates select="summary/price/bonus" mode="order-summary"/>
        <xsl:apply-templates select="$order-info//group[@name = 'order_delivery_props']" mode="order_delivery"/>
        
        <tr>
            <td colspan="5">
                <strong>
                    <xsl:text>Итого:</xsl:text>
                </strong>
            </td>
            <td>
                <strong>
                    <xsl:apply-templates select="summary/price/actual" mode="price"/>
                </strong>
            </td>
            <td/>
        </tr>
    </xsl:template>
    
    <xsl:template match="item" mode="order-items-createorder">
        <tr>
            <td>
                <xsl:apply-templates select="document(concat('uobject://',@id))/udata/object" mode="order-item-name" />
            </td>
            <td>
                <xsl:choose>
                    <xsl:when test="price/original &gt; 0">
                        <xsl:apply-templates select="price/original" mode="price" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="price/actual" mode="price" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td>
                <xsl:apply-templates select="discount" />
                <!--<input type="text" class="default item-discount" data-id="{@id}" name="item-discount-value[{@id}]" value="{discount_value}" size="3" />-->
            </td>
            <td>
                <xsl:apply-templates select="price/actual" mode="price" />
            </td>
            <td>
                <input type="number" min="1" class="default change-amount" data-id="{@id}" name="order-amount-item[{@id}]" value="{amount}" size="3" />
            </td>
            <td>
                <xsl:apply-templates select="total-price/actual" mode="price" />
            </td>
            <td class="center">
                <a href="#" data-item-id="{@id}" class="remove-item">
                    <i class="small-ico i-remove" alt=" "></i>
                </a>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="object" mode="order-item-name">
        <xsl:value-of select="@name" />
    </xsl:template>

    <xsl:template match="object[//property/@name = 'item_link']" mode="order-item-name">
        <xsl:choose>
            <xsl:when test="//property/value/page/@link != '/dummycreateorder/'">
                <a href="{$lang-prefix}/admin/catalog/edit/{//property/value/page/@id}/">
                    <xsl:value-of select="@name" disable-output-escaping="yes" />
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@name" disable-output-escaping="yes" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="bonus" mode="order-summary">
        <tr>
            <td>
                <strong>
                    <xsl:text>Использованные бонусы</xsl:text>
                </strong>
            </td>
            <td colspan="4"/>
            <td>
                <xsl:value-of select="."/>
            </td>
            <td />
        </tr>
    </xsl:template>
    
    <xsl:template match="discount" mode="order-summary">
        <tr>
            <td>
                <strong>
                    <xsl:text>Скидка на заказ (абсолютное значение)</xsl:text>
                </strong>
            </td>

            <td colspan="4">
                <a href="{$lang-prefix}/admin/emarket/discount_edit/{@id}/">
                    <xsl:value-of select="@name" />
                </a>
                <xsl:apply-templates select="document(concat('uobject://', @id, '.discount_modificator_id'))//item" mode="discount-size" />
                <xsl:apply-templates select="description" />
            </td>
            <td>
                <xsl:apply-templates select="document(concat('udata://emarket/applyPriceCurrency/', ../summary/price/discount, '/'))/udata/price/actual" mode="price" />
            </td>
            <td />
        </tr>
    </xsl:template>

    <xsl:template match="discount/description" />
    <xsl:template match="discount/description[. != '']">
        <xsl:value-of select="concat(' (', ., ')')" disable-output-escaping="yes" />
    </xsl:template>
    
    <xsl:template match="group[@name = 'order_delivery_props']" mode="order_delivery" />

    <xsl:template match="group[@name = 'order_delivery_props' and count(property[@name = 'delivery_id']/value/item) &gt; 0]" mode="order_delivery">
        <xsl:variable name="delivery-item" select="property[@name = 'delivery_id']/value/item" />
        <xsl:variable name="delivery-price" select="property[@name = 'delivery_price']/value" />
        <tr>
            <td>
                <xsl:text>Доставка: </xsl:text>
                <!--<a href="{$lang-prefix}/admin/emarket/delivery_edit/{$delivery-item/@id}/">-->
                <xsl:value-of select="$delivery-item/@name" />
                <!--</a>-->
            </td>
            <td colspan="4" />

            <td>
                <xsl:apply-templates select="document(concat('udata://emarket/applyPriceCurrency/', $delivery-price, '/'))/udata/price/actual" mode="price" />
            </td>
            <td />
        </tr>
    </xsl:template>
    
    <xsl:template match="*" mode="price">
        <xsl:value-of select="." />
    </xsl:template>

    <xsl:template match="*[../@prefix]" mode="price">
        <xsl:value-of select="concat(../@prefix, ' ', .)" />
    </xsl:template>

    <xsl:template match="*[../@suffix]" mode="price">
        <xsl:value-of select="concat(., ' ', ../@suffix)" />
    </xsl:template>
    
    <xsl:template match="discount">
        <a href="{$lang-prefix}/admin/emarket/discount_edit/{@id}/">
            <xsl:attribute name="title">
                <xsl:value-of select="description" disable-output-escaping="yes" />
            </xsl:attribute>

            <xsl:value-of select="@name" />
        </a>
        <xsl:apply-templates select="document(concat('uobject://', @id, '.discount_modificator_id'))//item" mode="discount-size" />
    </xsl:template>
    
    <xsl:template match="item" mode="discount-size">
        <xsl:apply-templates select="document(concat('uobject://',@id))/udata/object" mode="modificator-size" />
    </xsl:template>

    <xsl:template match="object" mode="modificator-size" />

    <xsl:template match="object[.//property[@name = 'proc']]" mode="modificator-size">
        <xsl:value-of select="concat(' &#8212; ', .//property[@name = 'proc']/value, '%')" />
    </xsl:template>

    <xsl:template match="object[.//property[@name = 'size']]" mode="modificator-size">
        <xsl:value-of select="concat(', ', .//property[@name = 'size']/value)" />
    </xsl:template>
    
</xsl:stylesheet>