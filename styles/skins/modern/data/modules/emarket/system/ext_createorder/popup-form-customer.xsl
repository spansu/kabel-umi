<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

    <xsl:template match="udata">
        <xsl:apply-templates select="group" mode="form">
            <xsl:with-param name="method" select="@method"/>
        </xsl:apply-templates>
        <xsl:if test="@method = 'co_getEditForm'">
            <xsl:variable name="delivery_addresses"
                          select="document('udata://emarket/co_getExtendedFields/delivery_addresses/')"/>
            <xsl:variable name="legal_persons"
                          select="document('udata://emarket/co_getExtendedFields/legal_persons/')"/>

            <xsl:if test="$delivery_addresses//field != ''">
                <fieldset class="collapsible">
                    <legend>
                        <a href="#group_delivery_addresses">
                            <xsl:text>Адреса доставки</xsl:text>
                        </a>
                    </legend>
                    <div id="item_delivery" style="display: block;">
                        <ul>
                            <xsl:apply-templates select="$delivery_addresses//field" mode="form-extended"/>
                        </ul>
                    </div>
                </fieldset>
            </xsl:if>
            <xsl:if test="$legal_persons//field != ''">
                <fieldset class="collapsible">
                    <legend>
                        <a href="#group_legal_persons">
                            <xsl:text>Юридические лица</xsl:text>
                        </a>
                    </legend>
                    <div id="item_legal_persons" style="display: block;">
                        <ul>
                            <xsl:apply-templates select="$legal_persons//field" mode="form-extended"/>
                        </ul>
                    </div>
                </fieldset>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template match="group" mode="form">
        <xsl:param name="method" select="'co_getCreateForm'"/>
        <xsl:if test="@name != 'idetntify_data' and @name != 'statistic_info' and (($method = 'co_getEditForm' and (not(field[@name = 'delivery_addresses']) and not(field[@name = 'legal_persons']))) or $method = 'co_getCreateForm')">
            <fieldset class="collapsible">
                <legend>
                    <a href="#group_{@name}">
                        <xsl:value-of select="@title" disable-output-escaping="yes"/>
                    </a>
                </legend>
                <div id="item_{@name}" style="display: block;">
                    <ul>
                        <xsl:apply-templates select="field" mode="form"/>
                    </ul>
                </div>
            </fieldset>
        </xsl:if>
    </xsl:template>

    <xsl:template match="field" mode="form">
        <li>
            <xsl:if test="@name = 'delivery_addresses' or @name = 'legal_persons'">
                <xsl:attribute name="class">
                    <xsl:text>extended_anchor</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <label for="form-control-{@name}">
                <span>
                    <xsl:value-of select="@title"/>
                    <xsl:apply-templates select="@required" mode="form"/>
                    <xsl:text>:</xsl:text>
                </span>
                <xsl:choose>
                    <xsl:when test="@name = 'delivery_addresses' or @name = 'legal_persons'">
                        <div id="extended-guide-container-{@type-id}">
                            <input type="hidden" name="extended_guide_items[{@name}]" value="0"
                                   id="choosed-{@type-id}"/>

                            <p>
                                <label class="inline">
                                    <input type="radio" class="radio extended-guide-item-input"
                                           name="customer_data[{@name}][]"
                                           value="0" data-type-id="{@type-id}" data-input-name="{@name}"
                                           checked="checked"/>
                                    <acronym>
                                        <xsl:text>Не выбрано</xsl:text>
                                    </acronym>
                                </label>
                            </p>

                            <div class="new-values-container"></div>
                            <p>
                                <label class="inline">
                                    <input type="radio" class="radio extended-guide-item-input"
                                           name="customer_data[{@name}][]"
                                           value="new" data-type-id="{@type-id}" data-input-name="{@name}"/>
                                    <acronym>
                                        <xsl:choose>
                                            <xsl:when test="@name = 'delivery_addresses'">
                                                <xsl:text>Новый адрес</xsl:text>
                                            </xsl:when>
                                            <xsl:when test="@name = 'legal_persons'">
                                                <xsl:text>Новое юр. лицо</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                    </acronym>
                                </label>
                            </p>
                        </div>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="." mode="form-field"/>
                    </xsl:otherwise>
                </xsl:choose>
            </label>
        </li>
    </xsl:template>

    <xsl:template match="field" mode="form-field">
        <input type="text" id="form-control-{@name}" class="{@name}" name="{@input_name}" value="{.}"
               placeholder="{@tip}">
            <xsl:choose>
                <xsl:when test="@required = 'required'">
                    <xsl:attribute name="class">
                        <xsl:text>required </xsl:text>
                        <xsl:value-of select="@name"/>
                        <xsl:if test="@type = 'int' or @type = 'float' or @type = 'counter' or @type = 'price'">
                            <xsl:text> number</xsl:text>
                        </xsl:if>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when
                    test="not(@required) and (@type = 'int' or @type = 'float' or @type = 'counter' or @type = 'price')">
                    <xsl:attribute name="class">
                        <xsl:value-of select="@name"/>
                        <xsl:text> number</xsl:text>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
        </input>
    </xsl:template>

    <xsl:template match="field[@type = 'relation']" mode="form-field">
        <select id="form-control-{@name}" class="{@name}" name="{@input_name}">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="class">
                    <xsl:text>required </xsl:text>
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@multiple = 'multiple'">
                <xsl:attribute name="multiple">multiple</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="values/item" mode="form-field"/>
        </select>
    </xsl:template>

    <xsl:template match="item" mode="form-field">
        <option value="{@id}">
            <xsl:copy-of select="@selected"/>
            <xsl:value-of select="."/>
        </option>
    </xsl:template>

    <xsl:template match="field[@type = 'boolean']" mode="form-field">
        <input type="hidden" name="{@input_name}" value="0"/>
        <input id="form-control-{@name}" type="checkbox" class="boolean {@name}" name="{@input_name}" value="1">
            <xsl:copy-of select="@checked"/>
        </input>
    </xsl:template>

    <xsl:template match="field[@type = 'text' or @type = 'wysiwyg']" mode="form-field">
        <textarea type="text" id="form-control-{@name}" class="{@name}" name="{@input_name}" cols="" rows="5"
                  placeholder="{@tip}">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="class">
                    <xsl:text>required </xsl:text>
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="."/>
        </textarea>
    </xsl:template>

    <xsl:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file']" mode="form-field">
        <xsl:if test="@relative-path != ''">
            <a href="{@relative-path}" target="_blank" class="file-link" title="Открыть файл в новом окне">
                <xsl:value-of select="@relative-path" disable-output-escaping="yes"/>
                <span>
                    <img src="/styles/skins/modern/data/modules/emarket/img/ext_createorder/icon-new-window.gif" alt=""
                         align="absmiddle" />
                </span>
            </a>
            <a href="#" class="del-link del-file" data-file-input-id="form-control-{@name}"
               data-file-input-name="{@input_name}">удалить
            </a>
        </xsl:if>

        <input type="file" id="form-control-{@name}" class="{@name}" name="{@input_name}">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="class">
                    <xsl:text>required </xsl:text>
                    <xsl:value-of select="@name"/>
                    <xsl:if test="@relative-path != ''">
                        <xsl:text> hide</xsl:text>
                    </xsl:if>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="not(@required) and @relative-path != ''">
                <xsl:attribute name="class">
                    <xsl:value-of select="@name"/>
                    <xsl:if test="@relative-path != ''">
                        <xsl:text> hide</xsl:text>
                    </xsl:if>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@relative-path != ''">
                <xsl:attribute name="name">
                    <xsl:text>fake_</xsl:text>
                    <xsl:value-of select="@input_name"/>
                </xsl:attribute>
                <xsl:attribute name="aria-invalid">
                    <xsl:text>false</xsl:text>
                </xsl:attribute>
            </xsl:if>
        </input>
    </xsl:template>

    <xsl:template match="@required" mode="form">
        <span class="required">*</span>
    </xsl:template>

    <xsl:template match="field" mode="form-extended">
        <li class="extended_anchor">
            <label for="form-control-{@name}">
                <span>
                    <xsl:value-of select="@title"/>
                    <xsl:apply-templates select="@required" mode="form"/>
                    <xsl:text>:</xsl:text>
                </span>
                <div id="extended-guide-container-{@type-id}">
                    <input type="hidden" name="extended_guide_items[{@name}]" value="{@selected}" id="choosed-{@type-id}" />
                    <p>
                        <label class="inline">
                            <input type="radio" class="radio extended-guide-item-input" name="customer_data[{@name}][]"
                                   value="0"
                                   data-type-id="{@type-id}" data-input-name="{@name}">
                                <xsl:if test="not(values//item[@selected='selected']/@id)">
                                    <xsl:attribute name="checked">
                                        <xsl:text>checked</xsl:text>
                                    </xsl:attribute>
                                </xsl:if>
                            </input>
                            <acronym>
                                <xsl:text>Не выбрано</xsl:text>
                            </acronym>
                        </label>
                    </p>
                    <xsl:apply-templates select="values/item" mode="form-extended-item">
                        <xsl:with-param name="type_id" select="@type-id"/>
                        <xsl:with-param name="input_name" select="@name"/>
                    </xsl:apply-templates>
                    <div class="new-values-container"></div>
                    <p>
                        <label class="inline">
                            <input type="radio" class="radio extended-guide-item-input" name="customer_data[{@name}][]"
                                   value="new" data-type-id="{@type-id}" data-input-name="{@name}"/>
                            <acronym>
                                <xsl:choose>
                                    <xsl:when test="@name = 'delivery_addresses'">
                                        <xsl:text>Новый адрес</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="@name = 'legal_persons'">
                                        <xsl:text>Новое юр. лицо</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </acronym>
                        </label>
                    </p>
                </div>
            </label>
        </li>
    </xsl:template>

    <xsl:template match="item" mode="form-extended-item">
        <xsl:param name="type_id"/>
        <xsl:param name="input_name"/>

        <div>
            <label class="inline">
                <input type="radio" id="extended-guide-item-{@id}" class="radio extended-guide-item-input"
                       name="customer_data[{$input_name}][]" value="{@id}" data-type-id="{$type_id}"
                       data-input-name="{$input_name}">
                    <xsl:if test="@selected = 'selected'">
                        <xsl:attribute name="checked">
                            <xsl:text>checked</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                </input>
                <input type="hidden" name="customer_data_value[{$input_name}][]" value="{@id}"/>
                <acronym style="white-space: normal">
                    <xsl:value-of select="." disable-output-escaping="yes"/>
                </acronym>
                <div class="edit-wrap">
                    <xsl:text>&#160;</xsl:text>
                    <a href="#" class="change-extended-guide-item" data-item-id="{@id}" data-type-id="{$type_id}">
                        <xsl:text>редактировать</xsl:text>
                    </a>
                    <a href="#" class="del-link del-extended-item" data-item-id="{@id}">удалить</a>
                </div>
            </label>
        </div>
    </xsl:template>

</xsl:stylesheet>