<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <script type="text/javascript" src="/js/jquery/jquery.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/jquery/jquery-ui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/jquery/jquery.umipopups.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/smc/compressed.js"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/jquery.inputmask.bundle.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/jquery.validate.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/popup_script.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/modern/data/modules/emarket/js/ext_createorder/add_item.js" charset="utf-8"></script>
        <link href="/styles/skins/_eip/css/popup_page.css" rel="stylesheet" type="text/css" />
        <link href="/styles/skins/_eip/css/symlink.control.css" rel="stylesheet" type="text/css" />
        <link href="/styles/skins/modern/data/modules/emarket/css/ext_createorder/add_item.css" rel="stylesheet" type="text/css" />
        <link href="/styles/skins/modern/data/modules/emarket/css/ext_createorder/form_styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body id="modal_page">
        <form action="#" method="post" id="addItemForm">
            <input type="hidden" name="is_new_item" value="0" />
            <fieldset class="collapsible">
                <legend><a href="#item">Товар</a></legend>
                <div id="item" style="display: block;">
                    <ul>
                        <li class="item-name-form">
                            <label for="item_name">
                                <span>Найдите товар среди существующих, или <a href="#" class="fake add-new-oitem">добавьте новый товар / услугу</a><span class="required">*</span>:</span>
                                <ul class="pageslist hide">
                                    <li>
                                        <img src="/images/cms/admin/mac/tree/ico_catalog_object.png" />
                                        <span id="item_name" style="max-width: 398px;"></span>
                                        <a id="item_delete" href="#" class="button">
                                            <img src="/images/cms/admin/mac/tree/symlink_delete.png" alt="delete" />
                                        </a>
                                        <a href="#" id="item_link" target="_blank"></a>
                                    </li>
                                </ul>
                                <input type="hidden" name="item_id" id="item_id" />
                                <input type="text" name="item_name" placeholder="Начните вводить название, или любую другую характеристику товара" class="string required" id="search_string" />
                                <div class="symlinkAutosuggest hide">
                                    <ul></ul>
                                </div>
                            </label>
                        </li>
						<li class="new-oitem-form hide">
                            <label for="">
                                <span>Введите название<span class="required">*</span>:</span>
                                <input type="text" name="new_oitem[name]" class="string required" placeholder="Введите название нового товара/услуги" minlength="3" />
                            </label>
                        </li>
                        <li>
                            <label for="item_amount">
                                <span>Укажите количество<span class="required">*</span>:</span>
                                <input type="number" name="amount" value="1" class="int required" id="item_amount" maxlength="3" min="1" max="999" />
                            </label>
                        </li>
						<li class="new-oitem-form hide">
                            <label for="">
                                <span>Укажите стоимость<span class="required">*</span>:</span>
                                <input type="number" name="new_oitem[price]" class="string required float" id="new-oitem-price" value="0" />
								<br/>
								<span>← <a href="#" class="fake add-new-oitem">вернуться к поиску товара среди существующих</a></span>
                            </label>
                        </li>
                    </ul>
                </div>
            </fieldset>
            <fieldset class="collapsible">
                <legend><a href="#options" id="options_tab">Опции товара</a></legend>
                <div id="options" style="display:none;">
                    <p>У выбранного товара нет дополнительных опций.</p>
                </div>
            </fieldset>
            <fieldset>
                <p id="total_price" class="hide">Цена товара (без учета скидок): <span id="total_price_value" data-original-price=""></span></p>
            </fieldset>
            <fieldset id="errors_overlay">
            </fieldset>
            <div class="eip_buttons">
                <input type="submit" value="Добавить" class="primary ok">
                <input type="button" value="Отменить" class="back" onclick="onClose()">
                <div style="clear:both;"></div>
            </div>
        </form>
    </body>
</html>