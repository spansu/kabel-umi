function showPreloader() {
    $.blockUI({
        message: '<img src="/styles/skins/modern/data/modules/emarket/img/ext_createorder/preloader.gif" alt="" width="40" height="40" />',
        overlayCSS: {
            backgroundColor: '#ffffff'
        },
        css: {
            applyPlatformOpacityRules: false,
            backgroundColor: 'transparent',
            border: 'none',
            top: (screen.height - 160) / 2 + 'px',
            left: ($(window).width() - 80) / 2 + 'px',
            width: '80px',
            height: '80px',
            'z-index': 1992
        }
    });
}

function hidePreloader() {
    $.unblockUI();
}

focusOnCustomer = function () {
    $('html, body').animate({
        scrollTop: parseInt($(".g_customer").offset().top - 140)
    }, 0);
}

reloadCart = function () {
    $.ajax({
        dataType: 'html',
        url: '/udata/emarket/co_transformXML/?xslpath=/styles/skins/modern/data/modules/emarket/system/ext_createorder/order-items.xsl&require=emarket/order/' + order_id + '&lang_prefix=' + window.pre_lang,
        beforeSend: function () {
            showPreloader();
        },
        success: function (content) {
            if (content.length > 0) {
                $('#createorder-order-items').html(content);
            }
        },
        complete: function () {
            hidePreloader();
        }
    });
    return false;
}

reloadCustomer = function () {

    $('input[name="customer_type"]').each(function () {
        $(this).prop('checked', false);
    });

    $.ajax({
        dataType: 'json',
        url: '/udata/emarket/co_ajaxGetCustomerInfo/.json',
        success: function (response) {
            if (response.result.id > 0) {
                $('#customer-info span').html('<strong>' + response.result.name + '</strong>&nbsp;(<a href="#" title="Редактировать" class="edit-customer" data-cid="' + response.result.id + '">редактировать</a>)');
                $('#customer-info').removeClass('hide');
            }
        }
    });
    return false;
}

onCloseCustomerFrame = function () {
    $('input[name=customer_type]').each(function () {
        $(this).prop('checked', false);
    });

    focusOnCustomer();
}

scrollToExtended = function () {
    setTimeout(function () {
        $('#popupLayer_addCustomer').css('top', '300px');

        $('html, body').animate({
            scrollTop: parseInt($('#popupLayer_addCustomer').innerHeight() + 200)
        }, 0);

    }, 100);
}

$(document).ready(function () {
    $('body').on("click", ".faq_subtitle", function () {
        var this_ = $(this);
        var data_content_id = $(this).attr("data-content-id");
        var faq_content = $(".faq_content[data-content-id = '" + data_content_id + "']");
        if ($(this).hasClass("closed")) {
            this_.removeClass("closed");
            faq_content.css("display", "block");
        } else {
            this_.addClass("closed");
            faq_content.css("display", "none");
        }
    });

    var wto;

    $.ajaxSetup({
        cache: false,
        error: function (a, b, c) {
            console.log('Error: ' + a + ' ' + b + ' ' + c);
        }
    });

    $('#addOrderItem').live('click', function () {
        jQuery.openPopupLayer({
            name: "addOrderItem",
            title: "Добавить товар в заказ",
            width: '100%',
            height: 300,
            url: "/styles/skins/modern/data/modules/emarket/system/ext_createorder/add_item.php?lang_id=" + window.lang_id + "&domain_id=" + window.domain_id + "&" + Math.random()
        });
        return false;
    });

    $('body').on('keyup', '.change-amount', function () {
        $(this).trigger('change');
    });

    $('body').on('change', '.change-amount', function () {
        clearTimeout(wto);
        var amount = parseInt($(this).val());
        var id = parseInt($(this).data('id'));
        wto = setTimeout(function () {
            if (!isNaN(amount)) {
                $.ajax({
                    dataType: 'json',
                    url: '/udata/emarket/co_ajaxEditItem/' + id + '/' + amount + '/.json',
                    success: function (response) {
                        if (response.result) {
                            reloadCart();
                        }
                    }
                });
            }
        }, 500);
        return false;
    });

    $('body').on('click', '.remove-item', function () {
        var id = parseInt($(this).attr('data-item-id'));
        $.ajax({
            dataType: 'json',
            url: '/udata/emarket/co_ajaxRemoveItem/' + id + '/.json',
            success: function (response) {
                if (response.result) {
                    reloadCart();
                }
            }
        });
        return false;
    });

    $('.select-delivery_id').change(function () {
        var val = parseInt($(this).val());
        if (isNaN(val)) {
            val = 0;
        }

        if (val > 0) {
            $('#calcDeliveryPrice').removeClass('hide');
        } else {
            $('#calcDeliveryPrice').addClass('hide');
        }

        $.ajax({
            dataType: 'json',
            url: '/udata/emarket/co_ajaxSetDelivery/' + val + '/.json',
            success: function (response) {
                if (typeof response.delivery_price !== 'undefined') {
                    $('.delivery-price input').val(response.delivery_price);
                    reloadCart();
                }
            }
        });
        return false;
    });

    $('#calcDeliveryPrice').click(function () {
        var val = parseInt($('.select-delivery_id').val());
        if (isNaN(val)) {
            val = 0;
        }

        $.ajax({
            dataType: 'json',
            url: '/udata/emarket/co_ajaxSetDelivery/' + val + '/.json',
            success: function (response) {
                if (typeof response.delivery_price !== 'undefined') {
                    $('.delivery-price input').val(response.delivery_price);
                    reloadCart();
                }
            }
        });
        return false;
    });

    $(document).on('keyup', '.delivery-price input', function () {
        var delivery_price = parseFloat($(this).val());
        clearTimeout(wto);

        wto = setTimeout(function () {
            if (isNaN(delivery_price)) {
                delivery_price = 0;
            }
            var val = parseInt($('.select-delivery_id').val());
            if (isNaN(val)) {
                return false;
                val = 0;
            }

            $.ajax({
                dataType: 'json',
                url: '/udata/emarket/co_ajaxSetDelivery/' + val + '/' + delivery_price + '/1/.json',
                success: function (response) {
                    if (typeof response.delivery_price !== 'undefined') {
                        $('.delivery-price input').val(response.delivery_price);
                        reloadCart();
                    }
                }
            });
        }, 750);
    });

    $(document).on('keyup', '.bonus input', function () {
        var val = parseFloat($(this).val());
        clearTimeout(wto);

        wto = setTimeout(function () {
            if (isNaN(val)) {
                val = 0;
            }

            $.ajax({
                dataType: 'json',
                url: '/udata/emarket/co_ajaxSetBonus/' + val + '.json',
                success: function (response) {
                    if (response.result) {
                        reloadCart();
                    }
                }
            });
        }, 750);
    });

    $('body').on('keyup', '.item-discount', function () {
        var val = parseFloat($(this).val());
        var id = parseInt($(this).data('id'));

        clearTimeout(wto);

        wto = setTimeout(function () {
            if (isNaN(val)) {
                val = 0;
            }

            if (!isNaN(id) && id) {
                $.ajax({
                    dataType: 'json',
                    url: '/udata/emarket/co_ajaxSetItemDiscount/' + id + '/' + val + '.json',
                    success: function (response) {
                        if (response.result) {
                            reloadCart();
                        }
                    }
                });
            }
        }, 750);
    });

    $('input[name="customer_type"]').change(function () {
        var val = $(this).val();
        if (val == 'existing') {
            //$('#customer_type_guest').html('');
            //$('#customer_type_guest').addClass('hide');
            $('#customer_type_existing').removeClass('hide');
        } else {
            //$('#customer_type_guest').removeClass('hide');
            //$('#customer_type_guest').removeAttr('disabled', 'disabled');
            $('#customer_type_existing').addClass('hide');
            $('#search-customer').val('');
            $('#customer_type_existing .symlinkAutosuggest').addClass('hide');

            var type_id = parseInt($(this).attr('data-type-id'));

            if (!isNaN(type_id) && type_id > 0) {
                jQuery.openPopupLayer({
                    name: "addCustomer",
                    title: 'Добавить нового покупателя',
                    width: '100%',
                    height: 300,
                    url: "/styles/skins/modern/data/modules/emarket/system/ext_createorder/customer.php?lang_id=" + window.lang_id + "&domain_id=" + window.domain_id + "&" + "&type_id=" + type_id + "&" + Math.random()
                });
                return false;
            } else {
                alert('Невозможно создать нового пользователя. Обратитесь к разработчикам модуля');
            }
        }
        return false;
    });

    $('#search-customer').keyup(function () {
        clearTimeout(wto);
        var val = $(this).val();
        $('.symlinkAutosuggest').addClass('hide');
        $('.symlinkAutosuggest ul').html('');
        var li = '';
        var _val = '';
        wto = setTimeout(function () {
            if (val.length > 2) {
                $.ajax({
                    dataType: 'json',
                    url: '/udata/emarket/co_ajaxSearchCustomer/' + val + '/.json',
                    beforeSend: function () {
                        $('#search-customer').attr('readonly', 'readonly');
                        _val = $('#search-customer').val();
                        $('#search-customer').val('Идет поиск...');
                    },
                    success: function (response) {
                        if (response.total) {
                            $.each(response.items.item, function (index, value) {
                                li += '<li>';
                                li += '<span class="active">';
                                li += value.name + ' (ID: ' + value.id + ')<br/><span class="';
                                if (value.status == 'не зарегистрирован') {
                                    li += 'unreg';
                                } else {
                                    li += 'reg';
                                }
                                li += '">' + value.status + '</span>';
                                li += '</span><span class="choose" data-id="' + value.id + '" title="Выбрать покупателя">выбрать</span></li>';
                            });
                        } else {
                            li += '<li><span class="active">Покупатель не найден, попробуйте уточнить запрос.</span></li>';
                        }
                        $('.symlinkAutosuggest ul').append(li);
                        $('.symlinkAutosuggest').removeClass('hide');
                    },
                    complete: function () {
                        $('#search-customer').val(_val);
                        $('#search-customer').removeAttr('readonly');
                    }
                });
            } else if (val.length > 0 && val.length < 3) {
                li += '<li><span class="active">Минимальная длина поискового запроса - 3 символа.</span></li>';
                $('.symlinkAutosuggest ul').append(li);
                $('.symlinkAutosuggest').removeClass('hide');
            } else {
                $('.symlinkAutosuggest ul').append('');
                $('.symlinkAutosuggest').addClass('hide');
            }
        }, 1000);
    });

    $(document).on('click', '#customer_type_existing .symlinkAutosuggest .choose', function () {
        var id = parseInt($(this).attr('data-id'));
        if (!isNaN(id)) {
            $.ajax({
                dataType: 'json',
                url: '/udata/emarket/co_ajaxSetCustomerId/' + id + '/.json',
                beforeSend: function () {
                    showPreloader();
                },
                success: function (response) {
                    if (response.result) {
                        $('#customer_type_existing').addClass('hide');
                        $('#search-customer').val('');
                        $('#customer_type_existing .symlinkAutosuggest').addClass('hide');

                        reloadCustomer();
                        reloadCart();
                    }
                },
                complete: function () {
                    hidePreloader();
                }
            });
        }
        return false;
    });

    $(document).on('click', '.edit-customer', function () {
        var cid = $(this).attr('data-cid');
        if (!isNaN(cid)) {

            $('#customer_type_existing').addClass('hide');
            $('#search-customer').val('');
            $('#customer_type_existing .symlinkAutosuggest').addClass('hide');

            $('input[name="customer_type"]').each(function () {
                $(this).prop('checked', false);
            });

            jQuery.openPopupLayer({
                name: "addCustomer",
                title: 'Редактировать покупателя',
                width: '150%',
                height: 300,
                url: "/styles/skins/modern/data/modules/emarket/system/ext_createorder/customer.php?lang_id=" + window.lang_id + "&domain_id=" + window.domain_id + "&cid=" + cid + "&" + Math.random()
            });
        }
        return false;
    });

    $('#form-createorder input[type="submit"]').click(function () {
        $.ajax({
            dataType: 'json',
            url: '/udata/emarket/co_checkOrderBeforeSubmit/.json',
            beforeSend: function () {
                showPreloader();
            },
            success: function (response) {
                if (!response.result) {
                    var errors = '<p>Возникли следующие ошибки:</p><ol class="errors-popup">';
                    $.each(response.errors.item, function (i, v) {
                        errors += '<li>' + v + '</li>';
                        console.log(v);
                    });
                    errors += '</ol>';

                    openDialog('', 'Ошибки при создании заказа', {
                        title: "Ошибки при создании заказа",
                        html: errors,
                        width: 350,
                        cancelButton: false,
                        confirmText: 'ОК',
                        confirmCallback: function (popupName) {
                            closeDialog(popupName);
                        }
                    });
                } else {
                    $('#form-createorder').submit();
                }
            },
            complete: function () {
                hidePreloader();
            }
        });
        return false;
    });

    $("body").on("click", ".faq_subtitle", function () {
        var this_ = $(this);
        var data_content_id = $(this).attr("data-content-id");
        var faq_content = $(".faq_content[data-content-id = '" + data_content_id + "']");
        if ($(this).hasClass("closed")) {
            this_.removeClass("closed");
            faq_content.css("display", "block");
        } else {
            this_.addClass("closed");
            faq_content.css("display", "none");
        }
    });
});