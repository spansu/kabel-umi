function onGuideClose(_iid, name) {
    var args = getArgs();
    
    var frame = $("#popupLayer_addCustomer iframe.umiPopupFrame", window.parent.document);
    if (frame.size()) {
        contentWindow = frame.get(frame.size() - 1).contentWindow;
        if (args.iid > 0) {
            _iid = args.iid;
        }
        contentWindow.onCloseGuideFrame(_iid, name, args.type_id);
    }
    
    window.parent.$.closePopupLayer();

    return false;
}

$(document).ready(function () {
    var args = getArgs();

    $.ajaxSetup({
        cache: false,
        dataType: 'json',
        error: function (a, b, c) {
            console.log('Error: ' + a + ' ' + b + ' ' + c);
        }
    });

    var url = 'co_getCreateForm/' + args.type_id;
    if (args.iid > 0) {
        url = 'co_getEditForm/' + args.iid;
    }

    $.ajax({
        dataType: 'html',
        url: '/udata/emarket/co_transformXML/?xslpath=/styles/skins/modern/data/modules/emarket/system/ext_createorder/popup-form.xsl&require=emarket/' + url,
        success: function (content) {
            $('#guide-content').html(content);
            $('#guideItem').validate({
                highlight: function (element, errorClass) {
                    $(element).addClass('has-error');
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    autoHeightIframe('', parseInt(100 + $('#guide-content').height()));
                    return true;
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('has-error');
                    autoHeightIframe('', parseInt(100 + $('#guide-content').height()));
                }
            });

            $('#guideItem').ajaxForm({
                dataType: 'json',
                success: function (response) {
                    if (response.guide_item_id > 0) {
                        onGuideClose(response.guide_item_id, response.name);
                    }
                }
            });

            autoHeightIframe('', parseInt(100 + $('#guide-content').height()));
        }
    });

    $('#guide-content').on('click','.del-file', function(){
        var id = $(this).attr('data-file-input-id');
        var name = $(this).attr('data-file-input-name');
        if(id != undefined && name != undefined) {
            $('#'+id).removeClass('hide');
            $('#'+id).attr('name',name);
            $(this).parent().find('.file-link').remove();
            $(this).remove();
            $('#'+id).focus();
        }
        return false;
    });

    autoHeightIframe('load');
});