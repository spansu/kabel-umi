function onCloseGuideFrame(iid, name, type_id) {
    if (!iid) {
        $("#extended-guide-container-" + type_id + " .extended-guide-item-input").each(function () {
            if ($(this).val() == 0) {
                $(this).click();
            }
        });
    } else {
        if ($('#extended-guide-item-' + iid).length) {
            $('#extended-guide-item-' + iid).parent().find('acronym').text(name);
        } else {
            if ($('#extended-guide-container-' + type_id).length) {
                var input_name = $("input[data-type-id='" + type_id + "']").attr('data-input-name');
                var new_value = '<p><label class="inline">';
                new_value += '<input type="radio" id="extended-guide-item-' + iid + '" class="radio extended-guide-item-input" name="customer_data[' + input_name + '][]" value="' + iid + '" data-type-id="' + type_id + '" data-input-name="' + input_name + '" />';
                new_value += '<input type="hidden" name="customer_data_value[' + input_name + '][]" value="' + iid + '" />';
                new_value += '<acronym style="white-space: normal">' + name + '</acronym> <a href="#" class="change-extended-guide-item" data-item-id="' + iid + '" data-type-id="' + type_id + '">редактировать</a>';
                new_value += '<a href="#" class="del-link del-extended-item" data-item-id="' + iid + '">удалить</a>'
                new_value += '</label></p>';
                $('#extended-guide-container-' + type_id + ' .new-values-container').append(new_value);
            }
        }
        $('#extended-guide-item-' + iid).click();

        autoHeightIframe('', parseInt(100 + $('#customer-content').height()));
    }

    /*var contentWindow = window.parent;
    contentWindow.scrollToExtended();*/
	
	if($('#extended-guide-container-' + type_id).length) {
		$('html, body').animate({
			scrollTop: parseInt($('#extended-guide-container-' + type_id).offset().top)
		}, 0);
	}
}

function onCustomerClose() {
    var frame = $("iframe.umiPopupFrame", window.parent.document);
    var contentWindow = window.parent;
    if (frame.size() > 1) {
        contentWindow = frame.get(frame.size() - 2).contentWindow;
    }
    contentWindow.$.closePopupLayer();
    contentWindow.onCloseCustomerFrame();
}

$(document).ready(function () {
    var args = getArgs();

    $.ajaxSetup({
        cache: false,
        dataType: 'json',
        error: function (a, b, c) {
            console.log('Error: ' + a + ' ' + b + ' ' + c);
        }
    });

    var url = '';
    if (args.cid > 0) {
        url = 'co_getEditForm/' + args.cid;
    } else {
        url = 'co_getCreateForm/' + args.type_id;
    }

    $.ajax({
        dataType: 'html',
        url: '/udata/emarket/co_transformXML/?xslpath=/styles/skins/modern/data/modules/emarket/system/ext_createorder/popup-form-customer.xsl&require=emarket/' + url,
        success: function (content) {
            $('#customer-content').html(content);

            $('#customerForm').validate({
                highlight: function (element, errorClass) {
                    $(element).addClass('has-error');
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    autoHeightIframe('', parseInt(120 + $('#customer-content').height()));
                    return true;
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('has-error');
                    autoHeightIframe('', parseInt(120 + $('#customer-content').height()));
                }
            });

            $('#customerForm').ajaxForm({
                dataType: 'json',
                success: function (response) {
                    if (response.result == 1) {
                        var frame = $("iframe.umiPopupFrame", window.parent.document);
                        var contentWindow = window.parent;
                        if (frame.size() > 1) {
                            contentWindow = frame.get(frame.size() - 2).contentWindow;
                        }
                        contentWindow.$.closePopupLayer();
                        contentWindow.reloadCustomer();
                        contentWindow.reloadCart();
                        contentWindow.focusOnCustomer();
                    }
                }, 
				error: function() {
					alert('Ошибка добавления пользователя. Проверьте, корректно ли введеные данные. Особое внимание уделите формату ввода e-mail и загружаемым файлам (если такие поля имеются)');
				}
            });

            autoHeightIframe('', parseInt(100 + $('#customer-content').height()));
        }
    });

    $('#customerForm').on('click', '.extended-guide-item-input', function () {
        var val = $(this).val();
        var type_id = $(this).attr('data-type-id');

        if (val == 'new') {
            $('#choosed-' + type_id).val('');
            jQuery.openPopupLayer({
                name: "guideItem",
                title: 'Добавить значение в справочник',
                width: '100%',
                height: 300,
                url: "/styles/skins/modern/data/modules/emarket/system/ext_createorder/guide_item.php?type_id=" + type_id + "&" + Math.random()
            });
        } else {
            $('#choosed-' + type_id).val(val);
        }
    });

    $('#customerForm').on('click', '.change-extended-guide-item', function () {
        var iid = parseInt($(this).attr('data-item-id'));
        var type_id = parseInt($(this).attr('data-type-id'));

        if (iid > 0) {
            $('#choosed-' + type_id).val('');
            jQuery.openPopupLayer({
                name: "guideItem",
                title: 'Редактировать значение справочника',
                width: '100%',
                height: 300,
                url: "/styles/skins/modern/data/modules/emarket/system/ext_createorder/guide_item.php?type_id=" + type_id + "&iid=" + iid + "&" + Math.random()
            });
        }
        return false;
    });

    $('#customer-content').on('click', '.del-file', function () {
        var id = $(this).attr('data-file-input-id');
        var name = $(this).attr('data-file-input-name');
        if (id != undefined && name != undefined) {
            $('#' + id).removeClass('hide');
            $('#' + id).attr('name', name);
            $(this).parent().find('.file-link').remove();
            $(this).remove();
            $('#' + id).focus();
        }
        return false;
    });

    $('#customer-content').on('click', '.del-extended-item', function () {
        var id = parseInt($(this).attr('data-item-id'));
        var item = $(this).parent().parent();
        if (!isNaN(id)) {
            if (confirm("Вы действительно хотите удалить эту запись?")) {
                $.ajax({
                    dataType: 'json',
                    url: '/udata/emarket/co_delGuideItem/'+id+'/.json',
                    success: function (response) {
                        if(response.result) {
                            if($(item).find('input[type="radio"]').is(':checked')) {
                                var input_name = $(item).find('input[type="radio"]').attr('name');
                                $('input[name="'+input_name+'"][value="0"]').click();
                            }
                            $(item).remove();
                        }
                    }
                });
            }
        }
        return false;
    });


    autoHeightIframe('load');
});