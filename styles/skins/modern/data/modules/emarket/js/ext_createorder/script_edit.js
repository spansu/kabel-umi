$(document).ready(function () {
    $.ajaxSetup({
        cache: false,
        error: function (a, b, c) {
            console.log('Error: ' + a + ' ' + b + ' ' + c);
        }
    });

    $('#addOrderItem').live('click', function () {
        jQuery.openPopupLayer({
            name: "addOrderItem",
            title: "Добавить товар в заказ",
            width: '100%',
            height: 300,
            url: "/styles/skins/modern/data/modules/emarket/system/ext_createorder/add_item.php?lang_id=" + window.lang_id + "&domain_id=" + window.domain_id + "&order_id=" + order_id + "&" + Math.random()
        });
        return false;
    });

});