<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl"
>

    <xsl:template match="/result[@method='createorder']/data[@type = 'form' and (@action = 'modify' or @action = 'create')]">
        <div class="editing-functions-wrapper">
            <div class="tabs editing">
            </div>
        </div>
        <div class="tabs-content module {$module}-module">
            <div class="section selected">
                <div class="location" xmlns:umi="http://www.umi-cms.ru/TR/umi">
                    <a class="btn-action loc-right infoblock-show">
                        <i class="small-ico i-info"></i>
                        <xsl:text>Помощь</xsl:text>
                    </a>
                </div>
                <div class="layout">
                    <xsl:apply-templates select="$errors" />
                    <div class="column">
                        <form class="form_modify" id="form-createorder" method="post" action="do/" enctype="multipart/form-data">
                            <input type="hidden" name="referer" value="{/result/@referer-uri}" id="form-referer" />
                            <input type="hidden" name="domain" value="{$domain-floated}"/>
                            <input type="hidden" name="permissions-sent" value="1" />
                            <script type="text/javascript">
                                //Проверка
                                var treeLink = function(key, value){
                                var settings = SettingsStore.getInstance();

                                return settings.set(key, value, 'expanded');
                                }
                            </script>
                            <xsl:apply-templates mode="form-modify-createorder" />
                            <div class="row">
                                <xsl:call-template name="std-form-buttons-add"/>
                            </div>
                        </form>
                    </div>
                    <div class="column">
                        <div  class="infoblock">
                            <h3>
                                <xsl:text>&label-quick-help;</xsl:text>
                            </h3>
                            <div class="content" title="./man/ru/emarket/createorder.html"></div>
                            <div class="infoblock-hide"></div>
                        </div>
                    </div>
                </div>
            </div>
            <small>
                <a href="http://clean-code.ru/" target="_blank">
                    <img src="/styles/skins/modern/data/modules/emarket/img/ext_createorder/logo.png" alt="Создание сайтов, модулей, расширений для UMI.CMS" align="absmiddle" />&nbsp;&nbsp;
                    <xsl:text>Создание сайтов, модулей, расширений для&nbsp;UMI.CMS</xsl:text>
                </a>
            </small>
        </div>
        <script type="text/javascript">
            var order_id = '<xsl:value-of select="//result/data/object/@id" />';
            var method = '<xsl:value-of select="/result/@method" />';
        </script>
        <link href="/styles/skins/modern/data/modules/emarket/css/ext_createorder/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">	$.getScript("/styles/skins/modern/data/modules/emarket/js/ext_createorder/jquery.blockUI.js");</script>
        <script type="text/javascript">	$.getScript("/styles/skins/modern/data/modules/emarket/js/ext_createorder/script.js");</script>
        <!--<xsl:call-template name="error-checker" >
            <xsl:with-param name="launch" select="1" />
        </xsl:call-template>-->
    </xsl:template>
        
    <!-- Сюда записываем те группы полей, автоматический вывод которых нам не нужен -->
    <xsl:template match="properties/group[@name = 'order_credit_props' or @name = 'statistic_info' or @name = 'purchase_one_click' or @name = 'order_discount_props']" mode="form-modify-createorder" priority="1" />
    <!-- Сюда записываем те поля, автоматический вывод которых нам не нужен -->
    <xsl:template match="//result[@method='createorder']//field[@name = 'number' or @name = 'social_order_id' or @name = 'customer_id' or @name = 'domain_id' or @name = 'yandex_order_id' or @name = 'status_change_date' or @name = 'status_id' or @name = 'order_discount_value' or @name = 'legal_person']" mode="form-modify" priority="1" />

    <xsl:template match="properties/group" mode="form-modify-createorder">
        <xsl:param name="show-name">
            <xsl:text>1</xsl:text>
        </xsl:param>
        <xsl:param name="show-type">
            <xsl:text>1</xsl:text>
        </xsl:param>
        <xsl:variable name="order-info" select="document(concat('udata://emarket/order/',//result/data/object/@id))" />
        
        <div class="panel-settings" name="g_{@name}">
            <a data-name="{@name}" data-label="{@title}"></a>
            <div class="title">
                <div class="round-toggle"></div>
                <h3>
                    <xsl:value-of select="@title"/>
                </h3>
            </div>
            <div class="row">
                <xsl:apply-templates select="." mode="form-modify-group-fields">
                    <xsl:with-param name="show-name" select="$show-name"/>
                    <xsl:with-param name="show-type" select="$show-type"/>
                </xsl:apply-templates>
            </div>
        </div>
        
        <xsl:if test="//result/@method = 'createorder' and @name = 'order_props'">
            <!-- Добавляем "виртуальную" группу, содержащую список товаров в заказе -->
            <xsl:apply-templates select="$order-info" mode="createorder-group-orderitems" />
            <!-- Добавляем "виртуальную" группу, содержащую информацию о покупателе -->
            <xsl:call-template name="createorder-group-customer">
                <xsl:with-param name="order-info" select="$order-info" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="group[@name = 'order_props']" mode="form-modify-group-fields" priority="1">
        <input type="hidden" name="type-id" value="{../../@type-id}" />
        <input type="hidden" name="data[new][domain_id]" value="{//result/@domain-id}" />
        <xsl:apply-templates select="field[not(@type='wysiwyg' or @type='text')]" mode="form-modify" />
        <xsl:if test="//result//field[@name='bonus'] != ''">
            <xsl:apply-templates select="//result//field[@name='bonus']" mode="form-modify" />
        </xsl:if>
        <xsl:apply-templates select="field[@type='wysiwyg' or @type='text']" mode="form-modify" />
    </xsl:template>
    
    <xsl:template match="udata" mode="createorder-group-orderitems">
        <xsl:variable name="order-info" select="document(concat('uobject://', @id))/udata" />
        <div class="panel-settings" name="g_order_items">
            <div class="title">
                <div class="round-toggle"></div>
                <h3>
                    <xsl:text>&label-order-items-group;</xsl:text>
                </h3>
            </div>
            
            <div class="row">
                <div class="col-md-12" style="margin-bottom:10px;">
                    <a href="#" id="addOrderItem" class="btn color-blue btn-small">
                        <xsl:text>&order-add-item;</xsl:text>
                    </a>
                </div>
                <div class="col-md-12">
                    <table class="btable btable-bordered btable-striped">
                        <thead>
                            <tr>
                                <th align="left">
                                    <xsl:text>&label-order-items-group;</xsl:text>
                                </th>
                                <th align="left">
                                    <xsl:text>&label-order-items-current-price;</xsl:text>
                                </th>
                                <th align="left">
                                    <xsl:text>&label-order-items-discount;</xsl:text>
                                </th>
                                <th align="left">
                                    <xsl:text>&label-order-items-original-price;</xsl:text>
                                </th>
                                <th align="left">
                                    <xsl:text>&label-order-items-amount;</xsl:text>
                                </th>
                                <th align="left">
                                    <xsl:text>&label-order-items-summ;</xsl:text>
                                </th>
                                <th>
                                    <xsl:text>&label-delete;</xsl:text>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="createorder-order-items">
                            <xsl:choose>
                                <xsl:when test="count(items/item) &gt; 0">
                                    <xsl:apply-templates select="items/item" mode="order-items-createorder" />
                                </xsl:when>
                                <xsl:otherwise>
                                    <tr>
                                        <td colspan="7" style="text-align: center">
                                            <xsl:text>&order-no-items;</xsl:text>
                                        </td>
                                    </tr>
                                </xsl:otherwise>
                            </xsl:choose>
                            <!--<tr>
                                <td>
                                    <strong>
                                        <xsl:text>&label-order-discount;</xsl:text>
                                    </strong>
                                </td>
                                <td colspan="4">
                                    <a href="{$lang-prefix}/admin/emarket/discount_edit/{discount/@id}/">
                                        <xsl:value-of select="discount/@name" />
                                    </a>
                                    <xsl:apply-templates select="document(concat('uobject://', discount/@id, '.discount_modificator_id'))//item" mode="discount-size" />
                                    <xsl:apply-templates select="discount/description" />
                                </td>
                                <td>
                                    <input type="text" class="default" name="order-discount-value" value="{discount_value}" size="3">
                                        <xsl:attribute name="value">
                                            <xsl:choose>
                                                <xsl:when test="../summary/price/discount">
                                                    <xsl:value-of select="document(concat('udata://emarket/applyPriceCurrency/', ../summary/price/discount, '/'))/udata/price/actual" />
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="discount_value"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:attribute>
                                    </input>
                                </td>
                                <td/>
                            </tr>-->
                            <xsl:apply-templates select="discount" mode="order-summary" />
                            <xsl:apply-templates select="summary/price/bonus" mode="order-summary"/>
                            <xsl:apply-templates select="$order-info//group[@name = 'order_delivery_props']" mode="order_delivery"/>
                            <tr>
                                <td colspan="5">
                                    <strong>
                                        <xsl:text>&label-order-items-result;:</xsl:text>
                                    </strong>
                                </td>
                                <td>
                                    <strong>
                                        <xsl:apply-templates select="summary/price/actual" mode="price"/>
                                    </strong>
                                </td>
                                <td/>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="item" mode="order-items-createorder">
        <tr>
            <td>
                <xsl:apply-templates select="document(concat('uobject://',@id))/udata/object" mode="order-item-name" />
            </td>
            <td>
                <xsl:choose>
                    <xsl:when test="price/original &gt; 0">
                        <xsl:apply-templates select="price/original" mode="price" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="price/actual" mode="price" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td>
                <!--<input type="text" class="default item-discount" data-id="{@id}" name="item-discount-value[{@id}]" value="{discount_value}" size="3" />-->
                <xsl:apply-templates select="discount" />
            </td>
            <td>
                <xsl:apply-templates select="price/actual" mode="price" />
            </td>
            <td>
                <input type="number" min="1" class="default change-amount" data-id="{@id}" name="order-amount-item[{@id}]" value="{amount}" size="3" />
            </td>
            <td>
                <xsl:apply-templates select="total-price/actual" mode="price" />
            </td>
            <td class="center">
                <a href="#" data-item-id="{@id}" class="remove-item">
                    <!--<img src="/images/cms/admin/mac/tree/ico_del.png" alt="Удалить товары из заказа"  />-->
                    <i class="small-ico i-remove" alt=" "></i>
                </a>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template name="createorder-group-customer">
        <xsl:param name="order-info" />
        
        <div class="panel-settings" name="g_customer">
            <div class="title">
                <div class="round-toggle"></div>
                <h3>
                    <xsl:text>&group-customer;</xsl:text>
                </h3>
            </div>
            
            <div class="row createorder-customer-wrap">
                <div class="field col-md-12">
                    <div class="title-edit">
                        <acronym title="&customer-type-tip;">
                            <xsl:text>&customer-type;</xsl:text>
                        </acronym>
                    </div>
                    <p>
                        <label class="inline">
                            <input type="radio" class="checkbox" name="customer_type" value="guest" data-type-id="{document('udata://emarket/co_getNewCustomerTypeId/')}" />
                            <acronym>&new-customer;</acronym>
                        </label>
                    </p>
                    <p>
                        <label class="inline">
                            <input type="radio" class="checkbox" name="customer_type" value="existing" />
                            <acronym>&existing-customer;</acronym>
                        </label>
                    </p>
                    <div class="createorder-customer-info-wrap">
                        <div id="customer_type_existing" class="hide">
                            <div class="field">
                                <span>
                                    <input type="text" name="search_customer" placeholder="Начните вводить имя, или любые другие данные о покупателе" class="default string" id="search-customer" />
                                    <div class="symlinkAutosuggest hide">
                                        <ul></ul>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div id="customer-info" class="hide">
                            <p>
                                <xsl:text>&customer;: </xsl:text>
                                <span></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="group" mode="form-modify-group-fields">
        <xsl:apply-templates select="field" mode="form-modify" />
    </xsl:template>
    
    <xsl:template match="//result[@method='createorder']//field[@name='delivery_price']" mode="form-modify">
        <div class="col-md-6">
            <div class="title-edit">
                <acronym title="{@tip}">
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </div>
            <span class="delivery-price">
                <input class="default" type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
                    <xsl:apply-templates select="@type" mode="number" />
                </input>
                <a href="#" id="calcDeliveryPrice" class="hide">&calc-delivery-price;</a>
            </span>
        </div>
    </xsl:template>
    
    <xsl:template match="//result[@method='createorder']//field[@name='bonus']" mode="form-modify">
        <div class="col-md-6">
            <div class="title-edit">
                <acronym title="{@tip}">
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </div>
            <span class="bonus">
                <input class="default" type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
                    <xsl:apply-templates select="@type" mode="number" />
                </input>
            </span>
        </div>
    </xsl:template>
    
    <xsl:template match="//result[@method='createorder']//field[@type = 'relation' and (@name = 'delivery_id' or @name='payment_id')]" mode="form-modify">
        <div class="col-md-6 relation clearfix" id="{generate-id()}" umi:type="{@type-id}">
            <xsl:if test="not(@required = 'required')">
                <xsl:attribute name="umi:empty">
                    <xsl:text>empty</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <div class="title-edit">
                <span class="label">
                    <acronym title="{@tip}">
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span>

                </span>
            </div>
            <div class="layout-row-icon">
                <div class="layout-col-control">
                    <select autocomplete="off" name="{@input_name}" id="relationSelect{generate-id()}" class="select-{@name}" data-property="{@name}">
                        <xsl:apply-templates select="." mode="required_attr" />
                        <xsl:if test="@multiple = 'multiple'">
                            <xsl:attribute name="multiple">multiple</xsl:attribute>
                            <xsl:attribute name="style">height: 62px;</xsl:attribute>
                        </xsl:if>
                        <xsl:if test="not(values/item/@selected)">
                            <option value=""></option>
                        </xsl:if>
                        <xsl:apply-templates select="values/item" />
                    </select>
                </div>
                <xsl:if test="@public-guide = '1'">
                    <div class="layout-col-icon">
                        <a id="relationButton{generate-id()}" class="icon-action relation-add">
                            <i class="small-ico i-add"></i>
                        </a>
                    </div>
                </xsl:if>
            </div>
            <xsl:if test="@public-guide = '1'">
                <div>
                    <a href="{$lang-prefix}/admin/data/guide_items/{@type-id}/">
                        <xsl:text>&label-edit-guide-items;</xsl:text>
                    </a>
                </div>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="delivery" mode="order-delivery-createorder" />
    <xsl:template match="delivery[method/@id != '']" mode="order-delivery-createorder">
        <xsl:variable name="delivery-item" select="document(concat('uobject://',method/@id))" />
        <xsl:variable name="delivery-price" select="price/delivery" />
        <tr>
            <td>
                <xsl:text>&label-order-delivery;: </xsl:text>
                <a href="{$lang-prefix}/admin/emarket/delivery_edit/{$delivery-item//@id}/">
                    <xsl:value-of select="$delivery-item//@name" />
                </a>
            </td>
            <td colspan="4" />
            <td>
                <xsl:apply-templates select="document(concat('udata://emarket/applyPriceCurrency/', $delivery-price, '/'))/udata/price/actual" mode="price" />
            </td>
            <td />
        </tr>
    </xsl:template>
    
    <xsl:template match="object" mode="order-item-name">
        <xsl:value-of select="@name" />
    </xsl:template>

    <xsl:template match="object[//property/@name = 'item_link']" mode="order-item-name">
        <xsl:choose>
            <xsl:when test="//property/value/page/@link != '/dummycreateorder/'">
                <a href="{$lang-prefix}/admin/catalog/edit/{//property/value/page/@id}/">
                    <xsl:value-of select="@name" disable-output-escaping="yes" />
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@name" disable-output-escaping="yes" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="discount" mode="order-summary">
        <tr>
            <td>
                <strong>
                    <xsl:text>Скидка на заказ (абсолютное значение)</xsl:text>
                </strong>
            </td>

            <td colspan="4">
                <a href="{$lang-prefix}/admin/emarket/discount_edit/{@id}/">
                    <xsl:value-of select="@name" />
                </a>
                <xsl:apply-templates select="document(concat('uobject://', @id, '.discount_modificator_id'))//item" mode="discount-size" />
                <xsl:apply-templates select="description" />
            </td>
            <td>
                <xsl:apply-templates select="document(concat('udata://emarket/applyPriceCurrency/', ../summary/price/discount, '/'))/udata/price/actual" mode="price" />
            </td>
            <td />
        </tr>
    </xsl:template>
    
    <xsl:include href="form.modify.createorder_edit.xsl" />

</xsl:stylesheet>