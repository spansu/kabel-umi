<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="field[@type = 'img_file']" mode="form-modify">
        <xsl:variable name="filemanager-id" select="document(concat('uobject://',/result/@user-id))/udata//property[@name = 'filemanager']/value/item/@id" />
        <xsl:variable name="filemanager">
            <xsl:choose>
                <xsl:when test="not($filemanager-id)">
                    <xsl:text>elfinder</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('uobject://',$filemanager-id))/udata//property[@name = 'fm_prefix']/value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="is-active" select="document('udata://content/img_alt_active/')/udata"/>
        <xsl:variable name="img-alt-data" select="document(concat('udata://content/getAltTitle/(', @relative-path, ')'))/udata"/>

        <div class="col-md-6 img_file" id="{generate-id()}" umi:input-name="{@input_name}"
             umi:field-type="{@type}"
             umi:name="{@name}"
             umi:folder="{@destination-folder}"
             umi:file="{@relative-path}"
             umi:folder-hash="{php:function('elfinder_get_hash', string(@destination-folder))}"
             umi:file-hash="{php:function('elfinder_get_hash', string(@relative-path))}"
             umi:lang="{/result/@interface-lang}"
             umi:filemanager="{$filemanager}"
        >
            <label for="imageField_{generate-id()}">
                <div class="title-edit">
                    <acronym title="{@tip}">
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:if test="$is-active = 1 and string-length(@relative-path)">
                        <xsl:text> </xsl:text>
                        <a id="link-{@id}" href="#" data-img="{@relative-path}" data-alt="{$img-alt-data/alt}" data-title="{$img-alt-data/title}" class="img-alt-edit-link">&alt-img-link-edit;</a>
                    </xsl:if>
                    <xsl:apply-templates select="." mode="required_text" />
                </div>
                <div class="layout-row-icon" id="imageField_{generate-id()}">

                </div>
            </label>
        </div>
    </xsl:template>
    
    <xsl:template match="field[@type = 'multiple_image']" mode="form-modify" priority="1">
        <xsl:variable name="is-active" select="document('udata://content/img_alt_active/')/udata"/>
        <div class="col-md-6 multiimage" id="{generate-id()}">
            <xsl:attribute name="data-prefix" >
                <xsl:value-of select="@input_name"/>
            </xsl:attribute>
            <div class="title-edit">
                <acronym title="{@tip}">
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </div>
            <div class="mimage_wrapper ui-sortable">
                <input type="hidden" name="{@input_name}" value=""/>
                <xsl:for-each select="values/value">
                    <xsl:if test="@relative-path">
                        <div class="multi_image"
                             umi:file="{@relative-path}"
                             umi:alt="{@alt}"
                             umi:order="{@order}"
                             umi:id="{@relative-path}"
                             id = "mifile_{@id}"
                        >
                            <xsl:variable name="img-alt-data" select="document(concat('udata://content/getAltTitle/(', @relative-path, ')'))/udata"/>
                            <xsl:if test="$is-active = 1 and string-length(@relative-path)">
                                <xsl:text> </xsl:text>
                                <a id="link-{generate-id()}" href="#" data-img="{@relative-path}" data-alt="{$img-alt-data/alt}" data-title="{$img-alt-data/title}" class="img-alt-edit-link multiple">&alt-img-link-edit;</a>
                            </xsl:if>
                        </div>
                    </xsl:if>
                </xsl:for-each>
                <div class="emptyfield" ></div>
            </div>
        </div>
    </xsl:template>
    
</xsl:stylesheet>