<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/seo" [
	<!ENTITY sys-module        'seo'>
	<!ENTITY sys-method-add        'add'>
	<!ENTITY sys-method-edit    'edit'>
	<!ENTITY sys-method-del        'del'>

]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="result[@method = 'filter_links']/data">
		<link rel="stylesheet" href="/styles/skins/modern/data/modules/seo/css/ext_filter_links/style.css" />
		<script type="text/javascript">	$.getScript("/styles/skins/modern/data/modules/seo/js/ext_filter_links/script.js");</script>
		<div class="location loc-left">
			<div class="imgButtonWrapper">				
				<a href="{$lang-prefix}/admin/&sys-module;/filter_link_edit/0/" class="btn color-blue">
					<xsl:text>Добавить ссылку</xsl:text>
				</a>
			</div>
			<form method="post" action="/admin/seo/fl_toggleActivity/" class="activity_form">
				<div class="checkbox">
					<xsl:if test="@is_enabled = '1'">
							<xsl:attribute name="checked">
								<xsl:text>checkbox checked</xsl:text>
							</xsl:attribute>
						</xsl:if>
					<input type="checkbox" id="fl_is_enabled" name="is_enabled" value="1">
						<xsl:if test="@is_enabled = '1'">
							<xsl:attribute name="checked">
								<xsl:text>checked</xsl:text>
							</xsl:attribute>
						</xsl:if>
					</input>
				</div>
				<span><xsl:text>Активировать расширение</xsl:text></span>
			</form>
			<a class="btn-action loc-right infoblock-show">
				<i class="small-ico i-info"></i>
				<xsl:text>Помощь</xsl:text>
			</a>
		</div>
		<div class="layout">
			<div class="column">				
				<xsl:call-template name="ui-smc-table">
					<xsl:with-param name="control-params" select="$method" />
					<xsl:with-param name="content-type">objects</xsl:with-param>
					<xsl:with-param name="domains-show">1</xsl:with-param>
					<xsl:with-param name="disable-csv-buttons">1</xsl:with-param>
					<xsl:with-param name="js-ignore-props-edit">['url','is_sitemap','priority']</xsl:with-param>
					
					<xsl:with-param name="on-render-complete">function(){
						$('#table_container_seo-filter_links td div a').each(function(){
							$(this).removeAttr('class');
							$(this).attr('href',window.pre_lang+'/admin/seo/filter_link_edit/'+$(this).parent().parent().parent().attr('rel')+'/');
						});
						
						$('#table_container_seo-filter_links td div a').click(function(){
							location.href = $(this).attr('href');
						});
					}</xsl:with-param>
				</xsl:call-template>
			</div>
			<div class="column">
				<div class="infoblock">
					<h3>Справка</h3>
					<div class="content" title="./man/ru/seo/filter_links.html"></div>
					<div class="infoblock-hide"></div>
				</div>
			</div>
		</div>
		<small>
			<a href="http://clean-code.ru/" target="_blank">
				<img src="/styles/skins/modern/data/modules/seo/img/ext_filter_links/logo.png" align="absmiddle" />
				<xsl:text>&#160;&#160;Создание сайтов, модулей, расширений для UMI.CMS</xsl:text>
			</a>
		</small>
	</xsl:template>
</xsl:stylesheet>