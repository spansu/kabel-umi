<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl"
>

    <xsl:template match="/result[@module = 'seo' and @method = 'filter_link_edit']/data">
        <link rel="stylesheet" href="/styles/skins/modern/data/modules/seo/css/ext_filter_links/style.css" />
        <script type="text/javascript">	$.getScript("/styles/skins/modern/data/modules/seo/js/ext_filter_links/script.js");</script>
        <div class="tabs module">
            <div class="section">
                <a href="/admin/seo/seo/">Анализ позиций</a>
            </div>
            <div class="section">
                <a href="/admin/seo/links/">Анализ ссылок</a>
            </div>
            <div class="section">
                <a href="/admin/seo/webmaster/">Яндекс.Вебмастер</a>
            </div>
            <div class="section selected">
                <a href="/admin/seo/filter_links/">SEO-ссылки для фильтров</a>
            </div>
        </div>
        <div class="editing-functions-wrapper">
            <div class="tabs editing">
            </div>
        </div>
        <div class="tabs-content module {$module}-module">
            <div id="page" class="section selected">
                <div class="location" xmlns:umi="http://www.umi-cms.ru/TR/umi">
                    <a class="btn-action loc-right infoblock-show">
                        <i class="small-ico i-info"></i>Помощь</a>
                </div>
                <div class="layout">
                    <div class="column">
						<xsl:apply-templates select="$errors" />
                        <form method="post" class="form_modify" action="do/" enctype="multipart/form-data">
                            <input type="hidden" name="referer" value="{/result/@referer-uri}"/>
                            <input type="hidden" name="domain" value="{$domain-floated}"/>
                            <script type="text/javascript">
                                var treeLink = function(key, value){
                                var settings = SettingsStore.getInstance();
									
                                return settings.set(key, value, 'expanded');
                                }
                            </script>
                            <xsl:apply-templates mode="form-modify-filters" />
							
                            <div class="row">
                                <xsl:choose>
                                    <xsl:when test="$data-action = 'create'">
                                        <xsl:call-template name="std-form-buttons-add"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:call-template name="std-form-buttons"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>							
                        </form>
                    </div>
                    <div class="column">
                        <div  class="infoblock">
                            <h3>
                                <xsl:text>&label-quick-help;</xsl:text>
                            </h3>
                            <div class="content" title="./man/ru/seo/filter_link_edit.html"></div>
                            <div class="infoblock-hide"></div>
                        </div>
                    </div>
                </div>
                <small>
                    <a href="http://clean-code.ru/" target="_blank">
                        <img src="/styles/skins/modern/data/modules/seo/img/ext_filter_links/logo.png" align="absmiddle" />
                        <xsl:text>&nbsp;&nbsp;Создание сайтов, модулей, расширений для UMI.CMS</xsl:text>
                    </a>
                </small>
            </div>
        </div>
        <xsl:call-template name="error-checker" >
            <xsl:with-param name="launch" select="1" />
        </xsl:call-template>
    </xsl:template>
	
    <xsl:template match="object" mode="form-modify-filters">
        <xsl:apply-templates select="properties/group" mode="form-modify-filters">
            <xsl:with-param name="show-type" select="'0'" />
        </xsl:apply-templates>
    </xsl:template>
	
    <xsl:template match="properties/group" mode="form-modify-filters">
        <xsl:param name="show-name">
            <xsl:text>1</xsl:text>
        </xsl:param>
        <xsl:param name="show-type">
            <xsl:text>1</xsl:text>
        </xsl:param>

        <div class="panel-settings" name="g_{@name}">
            <xsl:if test="@name = 'more_params'">
                <xsl:attribute name="class">panel-settings extended_fields</xsl:attribute>
            </xsl:if>
            <a data-name="{@name}" data-label="{@title}"></a>
            <div class="title">
                <xsl:call-template name="group-tip" />
                <div class="round-toggle"></div>
                <h3>
                    <xsl:value-of select="@title"/>
                </h3>
            </div>
            
            <div class="row">
                <xsl:apply-templates select="." mode="form-modify-group-fields">
                    <xsl:with-param name="show-name" select="$show-name"/>
                    <xsl:with-param name="show-type" select="$show-type"/>
                </xsl:apply-templates>
            </div>			
        </div>
    </xsl:template>
	
    <xsl:template match="/result[@module = 'seo' and @method = 'filter_link_edit']//field[@name = 'domain_id']" mode="form-modify" priority="1" />
	
    <xsl:template match="/result[@module = 'seo' and @method = 'filter_link_edit']//field[@name = 'priority']" mode="form-modify" priority="1">
        <div class="field priority">
            <div class="title-edit">
                <acronym title="{@tip}">
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </div>
            <span>
                <input class="default" type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
                    <xsl:apply-templates select="@type" mode="number" />
                </input>
            </span>
        </div>
        <xsl:text disable-output-escaping="yes">
            &lt;/div&gt;
        </xsl:text>
    </xsl:template>
	
    <xsl:template match="/result[@module = 'seo' and @method = 'filter_link_edit']//field[@name = 'is_sitemap']" mode="form-modify" priority="1">
        <xsl:text disable-output-escaping="yes">
            &lt;div class="col-md-6 is-sitemap"&gt;
        </xsl:text>
        <div class="cb">
            <input type="hidden" name="{@input_name}" value="0" />
            <div class="checkbox">
                <input type="checkbox" name="{@input_name}" value="1" id="{generate-id()}">
                    <xsl:apply-templates select="." mode="required_attr">
                        <xsl:with-param name="old_class" select="'checkbox'" />
                    </xsl:apply-templates>
                    <xsl:if test=". = '1'">
                        <xsl:attribute name="checked">checked</xsl:attribute>
                    </xsl:if>
                </input>
            </div>	
            <span class="label">
                <acronym>
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </span>
        </div>
    </xsl:template>
	
    <xsl:template match="field[@name = 'filters']" mode="form-modify" priority="1">
        <div class="col-md-6 wysiwyg-field">
            <div class="title-edit">
                <acronym title="{@tip}">
                    <xsl:apply-templates select="." mode="sys-tips" />
                    <xsl:value-of select="@title" />
                </acronym>
                <xsl:apply-templates select="." mode="required_text" />
            </div>
            <span>
                <textarea name="{@input_name}" id="{generate-id()}">
                    <xsl:apply-templates select="." mode="required_attr">
                        <xsl:with-param name="old_class" select="@type" />
                    </xsl:apply-templates>
                    <xsl:value-of select="." />
                </textarea>
            </span>
        </div>
    </xsl:template>
	
</xsl:stylesheet>