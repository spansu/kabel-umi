<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/result[@method = 'seo_pager']" priority="1">
		<div class="tabs-content module">
            <div class="section selected">
                <div class="location">
                    <div class="save_size"></div>
                    <a class="btn-action loc-right infoblock-show">
                        <i class="small-ico i-info"></i>
                        <xsl:text>&help;</xsl:text>
                    </a>
                </div>
                <div class="layout">
                    <div class="column">
						<form method="post" action="do/" enctype="multipart/form-data">
							<div class="panel-settings">
								<div class="content">
									<xsl:apply-templates select="." mode="settings.modify" />
								</div>
							</div>
							<div class="row">
								<xsl:call-template name="std-form-buttons-settings" />
							</div>
						</form>
					</div>
					<div class="column">
						<div class="infoblock">
							<h3><xsl:text>&label-quick-help;</xsl:text></h3>
							<div class="content" title="./man/ru/seo/seo_pager.html"></div>
							<div class="infoblock-hide"></div>
						</div>
					</div>	
				</div>
			</div>
			<small>
				<a href="http://clean-code.ru/" target="_blank">
					<img src="/styles/skins/mac/data/modules/seo/img/ext_seo_pager/logo.png" align="absmiddle" />
					<xsl:text>&nbsp;&nbsp;Создание сайтов, модулей, расширений для UMI.CMS</xsl:text>
				</a>
			</small>
		</div>
		
		<xsl:apply-templates select="/result/@demo" mode="stopdoItInDemo" />

		<xsl:call-template name="error-checker" />
		<script>
			var form = $('form').eq(0);
			jQuery(form).submit(function() {
				return checkErrors({
					form: form,
					check: {
						empty: 'input.required',
					}
				});
			});
		</script>
	</xsl:template>
	
	<xsl:template match="/result[@method = 'seo_pager']//group" mode="settings.modify">
        <div class="panel-settings">
            <div class="title">
                <h3>
                    <xsl:value-of select="@label" />
                </h3>
            </div>
            <div class="content">
                <table class="btable btable-striped middle-align">
                    <tbody>
                        <xsl:apply-templates select="option" mode="settings.modify" />
                    </tbody>
                </table>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="/result[@method = 'seo_pager']//option[@type='boolean']" mode="settings.modify">
        <tr>
            <td class="eq-col">
                <label for="{@name}">
					<xsl:value-of select="@label" />
                </label>
            </td>
            <td>
                <xsl:apply-templates select="." mode="settings.modify-option" />
            </td>
        </tr>
    </xsl:template>
	
</xsl:stylesheet>