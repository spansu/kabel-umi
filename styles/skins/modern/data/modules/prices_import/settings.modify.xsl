<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:php="http://php.net/xsl" xmlns:xslt="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/result[@module = 'prices_import' and @method = 'config']/data" priority="1">
        <div class="tabs module">
            <div class="section">
                <a href="/admin/prices_import/import/">
                    <xsl:text>&header-prices_import-import;</xsl:text>
                </a>
            </div>
            <div class="section selected">
                <a href="/admin/prices_import/config/">
                    <xsl:text>Настройки модуля</xsl:text>
                </a>
            </div>
            <!--<div class="section">
                <a href="/admin/prices_import/schedules/">
                    <xsl:text>&header-prices_import-schedules;</xsl:text>
                </a>
            </div>
            <div class="section">
                <a href="/admin/prices_import/export/">
                    <xsl:text>&header-prices_import-export;</xsl:text>
                </a>
            </div>-->
        </div>
        <div class="tabs-content module">
            <div class="section selected">
                <div class="location">
                    <div class="save_size"></div>
                    <a class="btn-action loc-right infoblock-show">
                        <i class="small-ico i-info"></i>
                        <xsl:text>&help;</xsl:text>
                    </a>
                </div>
                <div class="layout">
                    <div class="column">
                        <form method="post" action="do/" id="img-alt-form" enctype="multipart/form-data">
                            <table class="btable btable-striped middle-align">
                                <xsl:apply-templates select="." mode="settings.modify" />
                            </table>
                            <div class="row">
                                <xsl:call-template name="std-form-buttons-settings"/>
                            </div>
                        </form>
                    </div>
                    <div class="column">
                        <div  class="infoblock">
                            <h3>
                                <xsl:text>&label-quick-help;</xsl:text>
                            </h3>
                            <div class="content" title="./man/ru/prices_import/config.html">
                            </div>
                            <div class="infoblock-hide"></div>
                        </div>
                    </div>
                </div>
                <small>
                <a href="http://clean-code.ru/" target="_blank">
                    <img src="/styles/skins/modern/data/modules/config/img/ext_img_alt_config/logo.png" alt="Создание сайтов, модулей, расширений для UMI.CMS" align="absmiddle" />&nbsp;&nbsp;
                    <xsl:text>Создание сайтов, модулей, расширений для&nbsp;UMI.CMS</xsl:text>
                </a>
            </small>
            </div>
        </div>
        
    </xsl:template>
    
    <xsl:template match="/result[@module = 'prices_import'][@method = 'config']//group" mode="settings.modify">
        <div class="panel-settings">
            <div class="title">
                <h3>
                    <xsl:value-of select="@label" />
                </h3>
            </div>
            <div class="content">
                <table class="btable btable-striped middle-align">
                    <tbody>
                        <xsl:apply-templates select="option" mode="settings.modify" />
                    </tbody>
                </table>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="option" mode="settings.modify">
        <tr>
            <td class="eq-col">
                <label for="{@label}">
                    <xsl:value-of select="@label" disable-output-escaping="yes" />
                </label>
            </td>
            <td>
                <xsl:apply-templates select="." mode="settings.modify-option" />
            </td>
        </tr>
    </xsl:template>
    
</xsl:stylesheet>