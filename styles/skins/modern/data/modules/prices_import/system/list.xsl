<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file"[
        <!ENTITY nbsp  "&#xA0;">
        <!ENTITY copy  "&#169;">
        <!ENTITY mdash "&#8212;">

        <!ENTITY laquo  "&#171;">
        <!ENTITY raquo  "&#187;">

        <!ENTITY rarr  "&#8594;">
        <!ENTITY larr  "&#8592;">
        ]>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="udata[@method = 'get_uploaded_file']">
        <xsl:variable name="total_num_rows" select="//total_num_rows" />
        <xsl:variable name="total_sel_num_rows" select="//total_sel_num_rows" />
        <xsl:variable name="per_page" select="//per_page" />

        <div id="content_file_table" total_num_rows="{$total_num_rows}" total_sel_num_rows="{$total_sel_num_rows}" per_page="{$per_page}" key_field="{//key_field}">
            <table id="content_table" cellpadding="3" cellspacing="1" oncontextmenu = "return false;">
                <tr id="head_table">
                    <td id="i0" i_="0" width="25">
                        <div class="checkbox">
                            <input class="empty" type="checkbox" total_sel_num_rows="{$total_sel_num_rows}">
                                <xsl:if test="$total_sel_num_rows &gt; 0">
                                    <xsl:attribute name="checked">
                                        <xsl:text>checked</xsl:text>
                                    </xsl:attribute>
                                </xsl:if>
                            </input>
                        </div>
                    </td>
                    <xsl:apply-templates select="//row[position() = 1]/item" mode="head_row" />
                </tr>
                <xsl:apply-templates select="//row" mode="row" />
            </table>

            <div class="hide">
                <ul class="unique">
                    <li></li>
                    <xsl:apply-templates select="//unique_columns/item" mode="unique" />
                </ul>
            </div>

            <div class="cell_edit" id="cell_edit_input" style="display:none">
                <input id="cell_value" type="text" />
            </div>

            <div class="cell_edit" id="cell_edit_textarea" style="display:none">
                <textarea id="cell_value"></textarea>
            </div>

            <div id="cell_type_id_edit" style="display:none">
                <select id="cell_type_id">
                    <option value="0"></option>
                    <xsl:apply-templates select="document('udata://prices_import/get_fields_type/')//type_id" mode="get_fields_type_id" />
                </select>
            </div>
        </div>

        <img id="preloader" src="/styles/skins/mac/data/modules/prices_import/img/preloader_big.gif" />

        <div id="context_menu" class="cmenu" oncontextmenu = "return false;">
            <div class="cmenuItem" item_id="0">
                <span class="icon"></span>
                <xsl:text>Добавить колонку справа</xsl:text>
            </div>
            <div class="cmenuItem" item_id="1">
                <span class="icon"></span>
                <xsl:text>Добавить строку снизу</xsl:text>
            </div>
            <div class="cmenuItem" item_id="2">
                <span class="icon"></span>
                <xsl:text>Удалить колонку</xsl:text>
            </div>
            <div class="cmenuItem" item_id="3">
                <span class="icon"></span>
                <xsl:text>Удалить строку</xsl:text>
            </div>
            <xsl:if test="$total_sel_num_rows &gt; 0">
                <div class="cmenuItem" item_id="4">
                    <span class="icon"></span>
                    <xsl:text>Удалить отмеченные строки</xsl:text>
                </div>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="head_row">
        <td id="i{@id}" i_="{@id}" field_name="{@field_name}" field_type="{@field_type}">
            <div class="head_item">
                <div>
                    <!-- style="margin-right:14px;" -->
                    <xsl:variable name="get_fields_type" select="document('udata://prices_import/get_fields_type/')" />
                    <select id="fields_id_{position()}" col="{position()}" name="id_{position()}">
                        <xsl:apply-templates select="$get_fields_type//item[not(@title)]" mode="get_fields_type" />
                        <optgroup label="Базовые">
                            <xsl:apply-templates select="$get_fields_type//item[@kind='base' and @title]" mode="get_fields_type" />
                        </optgroup>
                        <xsl:choose>
                            <xsl:when test="count($get_fields_type//item[@kind='stores' and @title]) &gt; 0">
                                <optgroup label="Склады">
                                    <xsl:apply-templates select="$get_fields_type//item[@kind='stores' and @title]" mode="get_fields_type">
                                        <xsl:with-param name="kind">stores</xsl:with-param>
                                    </xsl:apply-templates>
                                </optgroup>
                            </xsl:when>
                            <xsl:otherwise>
                                <optgroup label="Склады">
                                    <option value="not_stores" disabled="disabled">
                                        <xsl:text>В настройках модуля не указан Идентификатор поля «Состояние на складе»</xsl:text>
                                    </option>
                                    <option value="not_stores" disabled="disabled">
                                        <xsl:text>Или список складов пуст</xsl:text>
                                    </option>
                                </optgroup>
                            </xsl:otherwise>
                        </xsl:choose>
                        <optgroup label="Типовые">
                            <xsl:apply-templates select="$get_fields_type//item[@kind='typical' and @title]" mode="get_fields_type" />
                        </optgroup>
                    </select>
                </div>
                <i class="fa fa-key key" title="Назначить поле ключевым идентификатором" aria-hidden="true"></i>
            </div>
        </td>
    </xsl:template>

    <xsl:template match="row" mode="row">
        <tr id="i{@id}" i_="{@id}">
            <xsl:choose>
                <xsl:when test="@error='not_permission'">
                    <xsl:attribute name="turn_off">
                        <xsl:text>1</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="title">
                        <xsl:text>Недостаточно прав для редактирования товара</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@error='type_id_not_fit'">
                    <xsl:attribute name="title">
                        <xsl:text>Не все выбранные поля содержатся в выбранном типе данных. Данные отсутствующих полей будут потеряны</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@error='1'">
                    <xsl:attribute name="error">
                        <xsl:text>1</xsl:text>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>

            <xsl:attribute name="class">
                <xsl:if test="@head='1'">
                    <xsl:text> head </xsl:text>
                </xsl:if>
                <xsl:if test="@is_exist">
                    <xsl:text> is_exist </xsl:text>
                </xsl:if>
                <xsl:if test="@id mod 2 = 0">
                    <xsl:text> even </xsl:text>
                </xsl:if>
            </xsl:attribute>
            <td id="i0" width="25" i_="0" align="right" valign="bottom">
                <div style="font-size:7pt; position:absolute; top:0px; left:2px; width:20px; text-align:left;">
                    <xsl:value-of select="@id"/>
                </div>
                <xsl:choose>
                    <xsl:when test="@head='1'"> <!--  or (@error='1') -->
                        <div style="width:20px;"></div>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="checkbox">
                            <input class="row_select" type="checkbox">
                                <xsl:if test="@selected = '1'">
                                    <xsl:attribute name="checked">
                                        <xsl:text>cheked</xsl:text>
                                    </xsl:attribute>
                                </xsl:if>
                            </input>
                        </div>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:if test="@error">
                    <div class="errors_warning"></div>
                </xsl:if>
            </td>
            <xsl:apply-templates select="item" mode="row_item" />
        </tr>
    </xsl:template>

    <xsl:template match="item" mode="row_item">
        <td id="i{@id}" i_="{@id}" field_name="{@field_name}" field_type="{@field_type}">
            <xsl:attribute name="title">
                <xsl:choose>
                    <xsl:when test="@error = 'parent_id_not_available'">
                        <xsl:text>Такой категории нет</xsl:text>
                    </xsl:when>
                    <xsl:when test="@error = 'parent_id_not_available_in_list'">
                        <xsl:text>В перечне указана несуществующая страница</xsl:text>
                    </xsl:when>
                    <xsl:when test="@error = 'parent_id_repeat'">
                        <xsl:text>Такая категория не одна</xsl:text>
                    </xsl:when>
                    <xsl:when test="@error = 'type_id_not_available'">
                        <xsl:text>Тип данных отсутствует</xsl:text>
                    </xsl:when>
                    <xsl:when test="@error = 'type_id_not_fit'">
                        <xsl:text>Не все выбранные поля содержатся в выбранном типе данных. Данные отсутствующих полей будут потеряны</xsl:text>
                    </xsl:when>
                    <xsl:when test="@error = 'page_not_found'">
                        <xsl:text>Страница не найдена</xsl:text>
                    </xsl:when>
                    <xsl:when test="@error = 'page_not_OC'">
                        <xsl:text>Страница не является объектом каталога</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="count(.//value) &gt; 1">

                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="./value" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>

            <xsl:attribute name="class">
                <xsl:if test="@error">error</xsl:if>
            </xsl:attribute>

            <xsl:if test="@multiple='1'">
                <xsl:attribute name="data-multiple">1</xsl:attribute>
            </xsl:if>
            <xsl:if test="@guide_id">
                <xsl:attribute name="data-guide_id">
                    <xsl:value-of select="@guide_id" />
                </xsl:attribute>
            </xsl:if>

            <xsl:if test="@field_name='type-id'">
                <xsl:if test="not(@error)">
                    <xsl:attribute name="title">
                        <xsl:variable name="get_value" select = "./value" />
                        <xsl:value-of select="document('udata://prices_import/get_types_id//')//item[@id=$get_value]/@name" />
                    </xsl:attribute>
                </xsl:if>
            </xsl:if>

            <div class="content_item">
                <xsl:choose>
                    <xsl:when test="@field_type='symlink'">
                        <xsl:apply-templates select=".//value" mode="valueParent_with_separate_symb">
                            <xsl:with-param name="error" select="@error" />
                        </xsl:apply-templates>
                    </xsl:when>

                    <xsl:when test="@field_name='type-id'">
                        <xsl:variable name="get_value" select = "./value" />
                        <xsl:if test="$get_value != ''">
                            <xsl:text>[</xsl:text>
                            <xsl:value-of select="./value" />
                            <xsl:text>] </xsl:text>
                            <xsl:value-of select="document('udata://prices_import/get_types_id/')//item[@id=$get_value]/@name" />
                        </xsl:if>
                    </xsl:when>

                    <xsl:when test="@field_type='boolean'">
                        <xsl:variable name="get_value" select = "./value" />
                        <xsl:choose>
                            <xsl:when test="$get_value = 1">Да</xsl:when>
                            <xsl:when test="$get_value = 'Да'">Да</xsl:when>
                            <xsl:when test="$get_value = 'да'">Да</xsl:when>
                            <xsl:otherwise>Нет</xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>

                    <xsl:otherwise>
                        <xsl:value-of select="./value" />
                    </xsl:otherwise>
                </xsl:choose>
            </div>

            <xsl:if test="@error">
                <div class="errors_warning"></div>
            </xsl:if>
        </td>
    </xsl:template>

    <xsl:template match="item" mode="get_fields_type">
        <xsl:param name="kind" />
        <xsl:variable name="name" select="@name" />
        <xsl:variable name="title" select="@title" />
        <option value="{@name}">
            <xsl:choose>
                <xsl:when test="$name">
                    <xsl:if test="$kind != 'stores'">
                        <xsl:text>[</xsl:text>
                        <xsl:value-of select="@name" />
                        <xsl:text>] - </xsl:text>
                    </xsl:if>
                    <xsl:if test="$name!=$title">
                        <xsl:value-of select="@title" />
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>Пропустить</xsl:text>
                </xsl:otherwise>
            </xsl:choose>

        </option>
    </xsl:template>

    <xsl:template match="type_id" mode="get_fields_type_id">
        <option value="{@id}">[<xsl:value-of select="@id"/>] <xsl:value-of select="@name"/></option>
    </xsl:template>

    <xsl:template match="item" mode="unique">
        <li id="i{@id}" i_="{@id}">
            <xsl:value-of select="@unique" />
        </li>
    </xsl:template>

    <xsl:template match="udata[@method = 'change_category_default']">
        <xsl:value-of select="//name" />
    </xsl:template>

    <xsl:template match="udata[@method = 'get_types_id']">
        <select id="default_type_id">
            <xsl:apply-templates select="document('udata://prices_import/get_types_id/')//item" mode="get_types_id" />
        </select>
    </xsl:template>

    <xsl:template match="item" mode="get_types_id">
        <option value="{@id}">
            <xsl:if test="@default='1'">
                <xsl:attribute name="selected">
                    <xsl:text>selected</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:text>[</xsl:text>
            <xsl:value-of select="@id"/>
            <xsl:text>] </xsl:text>
            <xsl:value-of select="@name" disable-output-escaping="yes" />
        </option>
    </xsl:template>

    <xsl:template match="udata[@method='getListTemplates']">
        <option value="" disabled="disabled" selected="selected">
            <xls:text>Выберите из списка</xls:text>
        </option>
        <xsl:apply-templates select="//item" mode="get_list_templates" />
    </xsl:template>

    <xsl:template match="item" mode="get_list_templates">
        <option value="{@name}" type_id_default="{@type_id_default}" category_default="{@category_default}" is_active_default="{@is_active_default}" code="{@code}" category_default_path="{@category_default_path}" symb_separate_csv="{@symb_separate_csv}" not_add_element="{@not_add_element}" >
            <xsl:value-of select="@name"/>
        </option>
    </xsl:template>

    <xsl:template match="udata[@method='set_params_div']">
        <div id="set_params_div">
            <form id="set_params" action="">
                <div id="templates">
                    <div class="title">
                        <xsl:text>Шаблоны&#160;</xsl:text>
                        <i class="edit fa fa-pencil" title="Редактировать шаблоны" aria-hidden="true"></i>
                        <xsl:text>&#160;</xsl:text>
                        <i class="quest fa fa-question-circle" aria-hidden="true" title="Если вы ранее сохраняли конфигурацию данного прайс-листа — выберите шаблон, и все настройки восстановятся автоматически. Также, вы можете создать новый шаблон, чтобы повторно не указывать настройки данного прайс-листа."></i>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="body">
                        <div id="insert">
                            <input id="new_template_name" type="text" placeholder="Новый шаблон" oninput="change_template_block()" value="{document(concat('udata://system/convertDate/',//file_name,'/(Y-m-d_H:i:s)'))}">
                                <xsl:if test="//user_file_name != ''">
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="//user_file_name"/>
                                    </xsl:attribute>
                                </xsl:if>
                            </input>
                            <a href="#" id="save_template" class="btn color-blue" >Добавить <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                            <select id="select_templates" onchange="change_template_block()">
                                <xsl:apply-templates select="document('udata://prices_import/getListTemplates/')" />
                            </select>
                        </div>
                        <div id="remove">
                            <div>
                                <xsl:text>Выберите шаблон для удаления:</xsl:text>
                            </div>
                            <select id="delete_templates">
                                <xsl:apply-templates select="document('udata://prices_import/getListTemplates/')" />
                            </select>
                        </div>
                    </div>
                </div>

                <div id="encoding">
                    <div class="title">
                        <xsl:text>Кодировка файла</xsl:text>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="body">
                        <select name="code" onchange="get_file()">
                            <option id="mode_edit_type_auto" value="auto">
                                <xsl:if test="//encoding = 'auto'">
                                    <xsl:attribute name="selected">
                                        <xsl:text>selected</xsl:text>
                                    </xsl:attribute>
                                </xsl:if>
                                <xsl:text>Автоопределение</xsl:text>
                            </option>
                            <option id="mode_edit_type_win-1251" value="win-1251">
                                <xsl:if test="//encoding = 'WIN-1251'">
                                    <xsl:attribute name="selected">
                                        <xsl:text>selected</xsl:text>
                                    </xsl:attribute>
                                </xsl:if>
                                <xsl:text>CP-1251 (ANSI)</xsl:text>
                            </option>
                            <option id="mode_edit_type_utf-8" value="utf-8">
                                <xsl:if test="//encoding = 'UTF-8'">
                                    <xsl:attribute name="selected">
                                        <xsl:text>selected</xsl:text>
                                    </xsl:attribute>
                                </xsl:if>
                                <xsl:text>UTF-8</xsl:text>
                            </option>
                        </select>
                    </div>
                </div>

                <div id="categories_tree">
                    <div class="title">
                        <xsl:text>Категория по умолчанию&#160;</xsl:text>
                        <i class="quest fa fa-question-circle" aria-hidden="true" title="Выберите раздел сайта, в котором будут создаваться новые объекты. В случае, если в прайс-листе есть ячейка, отвечающая за данное поле, рекомендуем задать ее соответствие с полем [parent-id] - (Родительская страница)" />
                    </div>
                    <div style="clear:both;"></div>
                    <div id="change_default_parent" class="change_parent"></div>
                </div>

                <div id="default_id">
                    <div class="title">
                        <xsl:text>Тип данных по умолчанию&#160;</xsl:text>
                        <i class="quest fa fa-question-circle" aria-hidden="true" title="Выберите идентификатор типа данных, который будет назначаться всем новым объектам. В случае, если в прайс-листе есть ячейка, отвечающая за данное поле, рекомендуем задать ее соответствие с полем [parent-id] - (Идентификатор типа данных)" />
                    </div>
                    <div style="clear:both;"></div>
                    <div class="body">
                        <xsl:apply-templates select="document('udata://prices_import/get_types_id/')" />
                    </div>
                </div>

                <xsl:if test="//user_file_type = 'csv'">
                    <div id="symb_separate_csv">
                        <div class="title">
                            <xsl:text>Символ-разделитель для CSV-файлов </xsl:text>
                            <i class="quest fa fa-question-circle" aria-hidden="true" title="Выберите символ, который используется для разделения колонок в загружаемом CSV-файле" />
                        </div>
                        <div style="clear:both;"></div>
                        <div class="body">
                            <select name="symb_separate_csv" onchange="get_file()">
                                <option value="59">
                                    <xsl:if test="//symb_separate_csv = '59'">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>; (точка с запятой)</option>
                                <option value="44">
                                    <xsl:if test="//symb_separate_csv = '44'">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>, (запятая)</option>
                                <option value="9">
                                    <xsl:if test="//symb_separate_csv = '9'">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>TAB (табуляция)</option>
                            </select>
                        </div>
                    </div>
                </xsl:if>

                <div id="additional_settings">
                    <div class="title">
                        <xsl:text>Дополнительные настройки</xsl:text>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="body">
                        <div class="checkbox-wrap">
                            <div class="checkbox">
                                <input id="default_is_active" type="checkbox">
                                    <xsl:if test="//def_isActive = '1'">
                                        <xsl:attribute name="checked">
                                            <xsl:text>checked</xsl:text>
                                        </xsl:attribute>
                                    </xsl:if>
                                </input>
                            </div>
                            <span>
                                <xsl:text>&#160;Делать «видимыми» новые объекты</xsl:text>
                            </span>
                        </div>
                        <div class="checkbox-wrap">
                            <div class="checkbox">
                                <input id="not_add_element" type="checkbox">
                                    <xsl:if test="//not_add_element = '1'">
                                        <xsl:attribute name="checked">
                                            <xsl:text>checked</xsl:text>
                                        </xsl:attribute>
                                    </xsl:if>
                                </input>
                            </div>
                            <span>
                                <xsl:text>&#160;Не создавать новые объекты</xsl:text>
                            </span>
                        </div>
                    </div>
                </div>

                <div id="info_table">
                    <xsl:text>Строк: </xsl:text>
                    <span id="num_rows" style="disaply:inline-block; margin-right:5px;"></span>
                    <xsl:text>Выделено: </xsl:text>
                    <span id="num_selected_rows" style="disaply:inline-block; margin-right:05px;"></span>
                    <xsl:text>Ошибок: </xsl:text>
                    <span id="num_errors_table" style="disaply:inline-block;"></span>
                </div>
            </form>
            
            <div id="close_set_params_div" class="toggle-config-sidebar" title="Свернуть">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </div>
            <div id="open_set_params_div" class="toggle-config-sidebar" title="Развернуть">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </div>
            <!--<img id="close_set_params_div" src="/styles/skins/mac/data/modules/prices_import/img/doc_close.png" />
            <img id="open_set_params_div" src="/styles/skins/mac/data/modules/prices_import/img/doc_open.png" />-->
        </div>
    </xsl:template>

    <xsl:template match="value" mode="valueParent_with_separate_symb">
        <xsl:param name="error" />
        <xsl:variable name="get_value" select = ".//." />
        <span>
            <xsl:if test="not($error)">
                <xsl:attribute name="title">
                    <xsl:value-of select="document(concat('upage://',.))/udata/page/name" />
                </xsl:attribute>
                <xsl:attribute name="class">title</xsl:attribute>
            </xsl:if>
            <xsl:if test="$get_value!= ''">[<xsl:value-of select="$get_value" />]</xsl:if>
        </span>
    </xsl:template>

</xsl:stylesheet>