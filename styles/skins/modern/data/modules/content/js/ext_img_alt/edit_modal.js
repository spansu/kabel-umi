$(document).ready(function () {
    $.ajaxSetup({
        cache: false
    });

    $('.img-alt-edit-link').click(function (e) {
        e.preventDefault();
        var link = $(e.target);
        var html = '<form action="/udata/content/img_alt_save/.json" method="post" id="edit_form">';
        html += '<input type="hidden" name="field_id" value="' + link.attr('id').replace('link-', '') + '"/>';
        html += '<input type="hidden" name="img_path" value="' + link.data('img') + '"/>';
        html += '<div class="input-line"><label for="alt_val"><span>Значение атрибута alt:</span></label><input id="alt_val" name="img_alt" class="default" value="' + link.data('alt') + '"></div>';
        html += '<div class="input-line"><label for="alt_title"><span>Значение атрибута title:</span></label><input id="alt_title" name="img_title" class="default" value="' + link.data('title') + '"></div>';

        openDialog('', link.text(), {
            width: 350,
            html: html,
            cancelButton: true,
            confirmText: 'Сохранить',
            confirmCallback: function (popupName) {
                $("#edit_form").submit();
                closeDialog(popupName);
            },
            cancelCallback: function (popupName) {
                closeDialog(popupName);
            }
        });
    });

    setTimeout(function () {
        $('.img-alt-edit-link.multiple').each(function () {
            $(this).insertAfter($(this).parent().find('.thumbnail'));
        });
    }, 300);


    $('body').on('submit', '#edit_form', function (e) {
        var form = $(e.target);
        //var id = form.find("input[name=field_id]").val();
        //var link = $("#link-" + id);
        var path = form.find("input[name=img_path]").val();
        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function (response) {
                var status = response.status;
                if (status === 'error')
                    //alertError();
                    closeDialog();
                else {
                    $('a[data-img="'+path+'"]').each(function(){
                        $(this).data('alt', response.alt);
                        $(this).data('title', response.title);
                    });
                    closeDialog();
                }
            },
            error: function () {
                //alertError();
                closeDialog();
            }
        });
        return false;
    });
});

function alertError() {
    var html = '<p class="error_alert">Произошла ошибка! Обратитесь к разработчику расширения.</p><div class="eip_buttons">';
    html += '<input type="button" class="primary ok" value="OK" onclick="closeDialog()">';
    html += '<div style="clear: both;"/></div>';
    closeDialog();
    openDialog({
        stdButtons: false,
        text: html
    })
    setTimeout(function () {

    }, 1000);
}
;