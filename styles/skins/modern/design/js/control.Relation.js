/**
 * Контрол Relation для форм
 * @param {object} options
 */
var ControlRelation = function(options) {
	var $ = jQuery || {};

	var container = options.container || null;
	var typeId = options.type || null;
	var id = options.id || "";
	var empty = options.empty || false;
	var sourceUri = options.sourceUri || '/admin/data/guide_items_all/';

	var selectizeObject = null;
	var selectizerInput = null;
	var enteredVal = '';
	var needLoad = true;

	/**
	 * Добавить объект в справочник
	 * @param {string} name название объекта
	 * @param {int} guideId идентификатор справочника
	 * @param {Function} callback вызывается после добавления объекта
	 */
	function addObjectToGuide(name, guideId, callback) {
		var request = {
			url: '',
			data: null
		};
		var newObject = {
			id: 0,
			name: ''
		};

		request.url = '/admin/udata://data/addObjectToGuide/.json';
		request.data = {
			param0: name,
			param1: guideId
		};

		jQuery.ajax({
			url: request.url,
			dataType: 'json',
			data: request.data,
			type: 'post',
			success: function(response) {
				if (typeof(response.data) !== 'undefined' && typeof(response.data.object) !== 'undefined') {
					newObject.id = parseInt(response.data.object.id);

					if (isNaN(newObject.id) || newObject.id <= 0) {
						return;
					}

					newObject.name = response.data.object.name;

					if (typeof(callback) === 'function') {
						callback(newObject.name, newObject.id);
					}

				}
			},
			error: function() {
				addButton.removeAttribute('disabled');
			}
		});
	}

	/** Окно создания нового объекта справочника */
	function addElement() {
		openDialog('', 'Создание нового элемента', {
			html: '<div class="title-edit">Введите название:</div>' +
			'<input id="newRelationVal" type="text" class="default" value="' + String(enteredVal).trim() + '"/>',
			cancelButton: true,
			confirmText: 'Добавить',
			cancelText: 'Отменить',
			confirmOnEnterElement: '#newRelationVal',
			confirmCallback: function(popupName) {
				var val = $('#newRelationVal').val();
				if (val != null && val.length > 0) {
					addObjectToGuide(val, typeId, function(name, data) {
						selectizeObject.addOption({text: name, value: data});
						selectizeObject.addItem(data, true);
						closeDialog(popupName);
					})
				}
			}
		});
	}

	/**
	 * Загрузить все элементы ранее указанного справочника
	 * @param {Function} callback
	 * @param {boolean} override
	 */
	function loadItemsAll(callback, override) {
		jQuery.ajax({
			url: sourceUri + typeId + ".xml?allow-empty",
			type: "get",
			complete: function(r) {
				if (override) {
					callback(r);
					return;
				}

				updateItemsAll(r, callback);
			}
		});
	}

	/** Алиас updateElements */
	function updateItemsAll(r, callback) {
		callback = typeof callback == 'function' ? callback : function() {
		};
		updateElements(r, callback);
	}

	/**
	 * Обновить элементы DOM в соответствии с загруженными данными
	 * @param {Object} response ответ от сервера
	 * @param {Function} callback вызывается после обновления DOM
	 */
	function updateElements(response, callback) {
		callback = (typeof callback === 'function') ? callback : function(r) {};

		if (!selectizeObject) {
			return;
		}

		var items = response.responseXML.getElementsByTagName('object');
		selectizeObject.lock();
		var oldValue = selectizeObject.getValue();

		if (typeof oldValue === 'string') {
			oldValue = [oldValue];
		}

		selectizeObject.clearOptions(true);

		for (var i = 0, count = items.length; i < count; i++) {
			selectizeObject.addOption({
				text: items[i].getAttribute('name'),
				value: items[i].getAttribute('id')
			});
		}

		for (var j = 0, oldCount = oldValue.length; j < oldCount; j++) {
			selectizeObject.addItem(oldValue[j], true);
		}

		selectizeObject.unlock();
		needLoad = false;
		callback(response);
	}

	/** Создать селект с возможностью поиска */
	function makeSearch() {
		$('.selectize-control', container).remove();

		var select = $('select', container);
		var name = select.attr('name');
		var multiple = select.attr('multiple');
		var options = select[0].innerHTML;
		select.remove();

		var newSelect = $('<select>', {
			placeholder: 'Поиск',
			name: name
		});

		if (multiple) {
			newSelect.attr('multiple', 'multiple');
		}

		newSelect[0].innerHTML = options;

		$('.layout-col-control', container).append(newSelect);
		createSearchedSelectize();
	}

	/** Инициализировать объект selectize с возможностью поиска */
	function createSearchedSelectize() {
		selectizeObject = $('select', container).selectize({
			plugins: ['remove_button', 'clear_selection'],
			allowEmptyOption: true,
			create: false,
			onType: function() {
				var input = selectizerInput || $('.selectize-input input', container);
				enteredVal = input.val();
			},
			load: function(query, callback) {
				jQuery.ajax({
					url: sourceUri + typeId + ".xml",
					data: {
						search: [query]
					},
					type: "get",
					complete: function(r) {
						if (callback) {
							var items = r.responseXML.getElementsByTagName('object'),
									result = [];
							for (var i = 0, cnt = items.length; i < cnt; i++) {
								result.push({text: items[i].getAttribute('name'), value: items[i].getAttribute('id')});
							}
							callback(result);
						}
					}
				});
			}
		});

		selectizeObject = selectizeObject[0].selectize;
	}

	/**
	 * Инициализировать объект selectize
	 * @param {object} options
	 */
	function init(options) {
		/** Загрузить элементы с сервера, если они присутствуют */
		function loadItems() {
			if (!needLoad) {
				return;
			}

			loadItemsAll(function(response) {
				var items = response.responseXML.getElementsByTagName('object');

				if (items.length) {
					updateElements(response);
				} else {
					items = response.responseXML.getElementsByTagName('empty');
					var result = String(items[0].getAttribute('result')).toLowerCase().replace(/[\s]/g, '');
					if (result === 'toomuchitems') {
						makeSearch();
					}
				}

				needLoad = false;
			}, true);
		}

		$('.relation-add', options.container).bind('click', function() {
			addElement();
		});

		var preload = options.preload == undefined ? true : options.preload;

		selectizeObject = $('select', options.container).selectize({
			plugins: ['remove_button', 'clear_selection'],
			allowEmptyOption: true,
			create: false,
			onType: function() {
				var input = selectizerInput || $('.selectize-input input', container);
				enteredVal = input.val();
			},
			onFocus: function() {
				loadItems();
			},
			onInitialize: function() {
				if (preload) {
					container.one('mouseover', function() {
						loadItems();
					});
				}
			}
		});

		selectizeObject = selectizeObject[0].selectize;
	}

	init(options);

	return {
		selectize: selectizeObject,
		loadItemsAll: loadItemsAll
	};
};
