(function ($, umi) {
    "use strict";

    $(function () {
        var updateFunction = (umi.data && !!umi.data['demo']) ? updateInDemo : updateSystem;
        jQuery('#update').click(updateFunction);
    });

    var updateInDemo = function (event) {
        event.preventDefault();
        $.jGrowl('<p>' + getLabel('label-stop-in-demo') + '</p>', {
            'header': 'UMI.CMS',
            'life': 10000
        });
    };

    var updateStarted = false;
    var updateSystem = function(event) {
        event.preventDefault();
        updateStarted = true;
        step = 0;
        install();
        return false;
    };

    var stepHeaders = ['Проверка прав пользователя', 'Проверка обновлений', 'Загрузка пакета тестирования', 'Распаковка архива с тестами', 'Запись начальной конфигурации', 'Выполняется тестирование', 'Подготовка сохранения данных', 'Резервное копирование файлов', 'Резервное копирование базы данных', 'Скачивание компонентов', 'Распаковка компонентов', 'Проверка компонентов', 'Обновление подсистемы', 'Обновление базы данных', 'Установка компонентов', 'Обновление конфигурации', 'Очистка кеша', 'Очистка системного кеша'];
    var stepNames = ['check-user', 'check-update', 'download-service-package', 'extract-service-package', 'write-initial-configuration', 'run-tests', 'check-installed', 'backup-files', 'backup-mysql', 'download-components', 'extract-components', 'check-components', 'update-installer', 'update-database', 'install-components', 'configure', 'cleanup', 'clear-cache'];
    var step;
    var for_backup = '';
    var rStep = 0;
    //var loadingSrc = '/styles/skins/modern/design/img/icons/autoupdate-loading.gif';
    var loadingSrc = '/styles/skins/modern/design/img/process.gif';
    var rStepHeaders = ['Восстановление файлов', 'Восстановление базы данных'];
    var rStepNames = ['restore-files', 'restore-mysql'];
    var updateWindowName = 'update_window';

    function error() {
        if (!updateStarted) {
            return false;
        }
        var text = "Произошла ошибка во время выполнения запроса к серверу.<br/>" +
            "<a href=\"http://errors.umi-cms.ru/15000/\" target=\"_blank\" >" +
            "Подробнее об ошибке 15000</a>";
        var  h = '<p style="text-align:center;">' + text + '</p>';
        h += '<p style="text-align:center;">';
        h += '<a class="btn color-blue btn-small retry-button">Повторить попытку</a></p>';
        showMess(h);

        $('.retry-button').click(function(event) {
            event.preventDefault();
            install();
            return false;
        });

        return false;
    }

    function bindCloseButtonHandler() {
        var closeButton = $('.close-dialog');

        closeButton.unbind('click');
        closeButton.click(function() {
            closeDialog(updateWindowName);
        });
    }

    function callBack(r) {
        if (!r) {
            return error();
        }

        if (jQuery('html', r).length > 0 || jQuery('result', r).length == 0) {
            return error();
        }

        var state = jQuery('install', r).attr('state');
        if (state == 'inprogress') {
            install();
            return false;
        }

        var errors = jQuery('error', r);

        // Ошибки на шаге 0, 1 обрабатываются в свитче, для остальных - обработка здесь.
        if (step > 1) {
            if (errors.length > 0) {
                var h = '<p style="text-align:center; font-weight:bold;">В процессе обновления произошла ошибка.</p>';

                var mess = errors.attr('message');
                if (mess.length >= 305) {
                    h += '<p style="text-align:center;"><div style="height: 80px; overflow-y: scroll;">' + mess + '</div></p>';
                } else {
                    h += '<p style="text-align:center;">' + mess + '</p>';
                }

                h += '<p style="text-align:center;">';

                if ((step >= 12) && (for_backup == 'all')) {
                    h += '<a class="btn color-blue btn-small" onclick="rollback(); return false;">Восстановить</a>';
                }

                h += '<a class="btn color-blue btn-small close-dialog">Закрыть</a>';
                h += '<a class="btn color-blue btn-small retry-button">Повторить попытку</a></p>';

                showMess(h);
                bindCloseButtonHandler();


                $('.retry-button').click(function(event) {
                    event.preventDefault();
                    install();
                    return false;
                });

                return false;
            }
        }

        switch (step) {
            case 0:
            {
                if (errors.length > 0) {
                    h = '<p style="text-align:center; font-weight:bold;">Ваших прав недостаточно для обновления.</p>';
                    h += '<p style="text-align:center;">Для дальнейшего обновления системы, пожалуйста, выйдите из авторизованного режима и повторно зайдите как супервайзер.</p>';
                    h += '<div class="to-right"><a class="btn color-blue btn-small close-dialog">Закрыть</a></div>';

                    showMess(h);
                    bindCloseButtonHandler();
                    return false;
                }
            }
                break;
            case 1:
            {
                var hasUpdates = true;
                if (errors.length > 0) {
                    if (errors.attr('message') == 'Updates not avaiable.') {
                        h = '<p style="text-align:center; font-weight:bold;">Доступных обновлений нет.</p>';
                        h += '<div class="to-right"><a class="btn color-blue btn-small close-dialog">Закрыть</a><a class="btn color-blue btn-small force-update">Обновить принудительно</a></div>';
                    } else if (errors.attr('message') == 'Updates avaiable.') {
                        h =  '<div style="text-align:center; font-weight:bold;">Доступны обновления.</div>';
                        h += '<div class="updates-available">';
                        h += '<div>Посмотрите, что изменилось <a href="http://www.umi-cms.ru/product/changelog/" target="_blank">в этой версии</a>&nbsp;<span style="font-size:1.25em">→</span></div>';
                        h += '<div class="confirm-update"><div class="checkbox wish-update"><input type="checkbox" class="checkbox"></div><span>Да, я хочу выполнить обновление.</span></div>';
                        h += '<div class="to-right"><a class="btn color-blue btn-small close-dialog">Закрыть</a><a class="btn color-blue btn-small" id="update_button" disabled="disabled">Обновить систему</a></div>';
                        h += '</div>';
                    } else { // Ожидаемое сообщение - сервер отклонил запрос.
                        hasUpdates = false;
                        h = '<p>' + errors.attr('message') + '</p>';
                        h += '<p style="font-weight:bold;">Продолжение обновления невозможно.</p>';
                        h += '<div class="to-right"><a class="btn color-blue btn-small close-dialog">Закрыть</a></div>';

                    }

                    var options = hasUpdates ? {stdButtons: false} : {};

                    showMess(h, null, options)
                    bindCloseButtonHandler();

                    var continueUpdating = function(event) {
                        if (!$(this).attr('disabled')) {
                            event.preventDefault();
                            step++;
                            install();
                        }
                    };

                    $('.force-update').click(continueUpdating);

                    var updateButton = $('#update_button');
                    updateButton.click(continueUpdating);

                    $('.wish-update').click(function() {
                        if ($(this).hasClass('checked')) {
                            updateButton.removeAttr('disabled');
                        } else {
                            updateButton.attr('disabled', 'true');
                        }
                    });


                    return false;
                }
            }
                break;
            case 5:
            {

                h = '<p style="text-align: center; font-weight:bold;margin-bottom: 10px">Сохранение перед установкой:</p>';
                h += '<p style="text-align: left; font-weight:normal;">';
                h += '<p>Перед обновлением системы необходимо сделать бекап всех файлов и дамп базы средствами хостинг-провайдера.</p>';
                h += '<div class="checkbox for-backup"><input type="checkbox" name="for_backup" value="none"/></div>';
                h += '<span class="has-backup">Бекап сделан</span>'
                h += '</p>';
                h += '<div class="to-right"><a class="btn color-blue btn-small close-dialog">Закрыть</a><a class="btn color-blue btn-small" id="continueBackup" disabled="disabled">Продолжить</a></div>';

                showMess(h);
                bindCloseButtonHandler();
                for_backup = 'none';
                var continueButton = $('#continueBackup');

                continueButton.click(function(event) {
                    event.preventDefault();

                    if (!$(this).attr('disabled')) {
                        prepareBackup();
                    }

                    return false;
                });

                $('.for-backup').click(function() {
                    if ($(this).hasClass('checked')) {
                        continueButton.removeAttr('disabled');
                    } else {
                        continueButton.attr('disabled', 'true');
                    }
                });

                return false;
            }
                break;
            case 6:
            { // Бекапирование подготовлено
                if (for_backup == 'base') { // Пропускаем бекапирование файлов
                    step = 7;
                }
            }
                break;
            case 7:
            { // Файлы забекапированы
                if (for_backup != 'all') { // Пропускаем бекапирование базы
                    step = 8;
                }
            }
                break;
            case 17:
            {
                jQuery(window).unbind('beforeunload');
                jQuery(window).bind('beforeunload', function () {
                    return null;
                });
                h = '<p style="text-align:center; font-weight:bold;">Обновление завершено.</p>';
                h += '<p style="text-align:center;">Узнайте, что нового <a href="http://www.umi-cms.ru/product/changelog/" target="_blank">в этой версии</a>.</p>';
                h += '<p style="text-align:center;"><a class="btn color-blue btn-small" href="/">Перейти на сайт</a></p>';

                showMess(h);
                bindCloseButtonHandler();
                return false;
            }
        }

        bindCloseButtonHandler();

        step++;
        install();
        return false;
    }

    function startPing() {
        jQuery.post('/smu/installer.php', {step: 'ping', guiUpdate: 'true'});
        setTimeout(function() {startPing();}, (3 * 60 * 1000));
    }

    function install() {
        if (step > stepNames.length - 1) {
            return false;
        }

        var h = '<p style="text-align: center;">' + stepHeaders[step] + '. Пожалуйста, подождите.</p>';
        h += '<p style="text-align: center;" class="loading-wrapper"><img src="' + loadingSrc + '" /></p>';
        showMess(h);

        jQuery.post('/smu/installer.php', {step: stepNames[step], guiUpdate: 'true'}, function (r) {
            callBack(r);
        }).fail(function() {
            error();
        });
        return false;
    }

    function showMess(message, title, options) {
        title = title || 'Обновление системы';
        var windowName = updateWindowName;
        var openedWindow = getPopupByName(windowName);

        var content = '<div class="update-info">' + message + '</div>';

        var defaultOptions = {
            name: windowName,
            width: 350,
            html: content,
            stdButtons: false,
            closeButton: false
        };

        options = $.extend(defaultOptions, options);

        if (!openedWindow) {
            openDialog('', title, options);
        } else {
            $(".eip_win_title", openedWindow.id).html(title);
            $(".popupText", openedWindow.id).html(content);
            applyOptions(options);

            $('.checkbox input:checked', openedWindow.id).parent().addClass('checked');
            $('.checkbox', openedWindow.id).click(function () {
                $(this).toggleClass('checked');
            });
        }
    }

    function applyOptions(options, popupContainer) {
        if (typeof options.stdButtons == 'boolean' && !options.stdButtons) {
            $('.eip_buttons', popupContainer).detach();
        }
    }

    function prepareBackup() {
        for_backup = jQuery("input[name='for_backup']:checked").val();

        if (for_backup == 'none') {
            step = 9;
        } else {
            step = 6;
        }

        if (window.session) {
            window.session.destroy();
        }

        if (uAdmin.session.pingIntervalHandler) { // отключаем стандартный пинг
            clearInterval(uAdmin.session.pingIntervalHandler);
        }
        startPing(); // Запускаем постоянное обращение к серверу во избежание потери сессии
        jQuery(window).bind('beforeunload', areYouSure); // Пытаемся предупредить закрытие окна в процессе обновления
        install();
    }

    function areYouSure() {
        return "Вы действительно хотите прервать процесс обновления? Возможны проблемы с работоспособностью сайта!";
    }

    function rollback() {
        t = 'Отмена установки';
        var  h = '<p style="text-align: center;">' + rStepHeaders[rStep] + '. Пожалуйста, подождите.</p>';
        h += '<p style="text-align: center;" class="loading-wrapper"><img src="' + loadingSrc + '" /></p>';
        showMess(h, t);
        jQuery.post('/smu/installer.php', {
            'step': rStepNames[rStep],
            'guiUpdate': 'true'
        }, rollbackBackTrace);
    }

    function rollbackBackTrace(r) {
        var errors = jQuery('error', r);
        if (errors.length > 0) {
            alert('Ошибка');
        }

        var state = jQuery('install', r).attr('state');
        if (state == 'done') {
            rStep++;
        }

        if (rStep > rStepHeaders.length - 1) {
            t = 'Отмена установки';
            var h = '<p style="text-align: center;">Система была восстановлена на сохраненное состояние.</p>';
            showMess(h, t);
            return false;
        }

        rollback();
    }

}(jQuery, uAdmin));