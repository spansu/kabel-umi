$(document).ready(function() {
    $.ajaxSetup({
        cache: false
    });

    $(".img-alt-edit-link").click(function(e) {
        e.preventDefault();
        var link = $(e.target);
        var html = '<form action="/udata/content/img_alt_save/.json" method="post" id="edit_form">';
        html += '<input type="hidden" name="field_id" value="' + link.attr("id").replace("link-", "") + '"/>';
        html += '<input type="hidden" name="img_path" value="' + link.attr("data-img") + '"/>';
        html += '<fieldset class="collapsible">';
        html += '<div class="input-line"><label for="alt_val"><span>Значение атрибута alt:</span></label><input id="alt_val" name="img_alt" value="' + link.attr("data-alt") + '"></div>';
        html += '<div class="input-line"><label for="alt_title"><span>Значение атрибута title:</span></label><input id="alt_title" name="img_title" value="' + link.attr("data-title") + '"></div>';
        html += '</fielset>';

        openDialog({
            title       : link.text(),
            width       : 400,
            height      : 1800,
            text        : html,
            OKCallback  : function() {
                $("#edit_form").submit();
                return false;
            }
        })
    });

    $("body").on("submit", "#edit_form", function(e) {
        var form    = $(e.target);
        var id      = form.find("input[name=field_id]").val();
        var link    = $("#link-" + id);
        $.ajax({
            url         : form.attr("action"),
            data        : form.serialize(),
            dataType    : "json",
            success     : function(response) {
                var status = response.status
                if(status == "error") alertError();
                else {
                    link.attr("data-alt", response.alt);
                    link.attr("data-title", response.title);
                    closeDialog();
                }
            },
            error: function() {
                alertError();
            }
        });
        return false;
    });
});

function alertError() {
    var html    =  '<p class="error_alert">Произошла ошибка! Обратитесь к разработчику расширения.</p><div class="eip_buttons">';
    html        += '<input type="button" class="primary ok" value="OK" onclick="closeDialog()">';
    html        += '<div style="clear: both;"/></div>';
    closeDialog();
    openDialog({
        stdButtons  : false,
        text        : html
    })
    setTimeout(function() {

    }, 1000);
}