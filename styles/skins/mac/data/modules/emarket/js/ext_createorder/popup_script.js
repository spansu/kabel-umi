function getArgs() {
    var args = new Object();
    var query = location.search.substring(1);
    var pairs = query.split("&");
    for (var i = 0; i < pairs.length; i++) {
        var pos = pairs[i].indexOf('=');
        if (pos == -1) {
            continue;
        }
        var argname = pairs[i].substring(0, pos);
        var value = pairs[i].substring(pos + 1);
        args[argname] = unescape(value);
    }
    return args;
}

function onClose() {
    window.parent.$.closePopupLayer();
    return false;
}

function autoHeightIframe(mode, height) {
    var modal_page = document.getElementById('modal_page');
    if (height <= 0) {
        var height = (mode == 'load') ? document.body.scrollHeight : modal_page.offsetHeight;
        height = (height > 500) ? 500 : height;
    }
	window.frameElement.style.height = height;
}