function recalcPrice() {
    var actual_price = parseFloat($('#total_price_value').attr('data-original-price'));
    var amount = parseInt($('#item_amount').val());
    if (isNaN(amount)) {
        amount = 0;
    }

    $('#options select').each(function () {
        var option = $(this).find('option:selected');
        if (option.attr('data-float')) {
            actual_price = parseFloat(parseFloat(option.attr('data-float')) + actual_price);
        }
    });
    actual_price = parseFloat(actual_price * amount);
    $('#total_price_value').text(actual_price);
}

$(document).ready(function () {

	var add_new_oitem = 0;
    var not_show_options = 1;
    var args = getArgs();
	
    $.ajaxSetup({
        cache: false,
        dataType: 'json',
        error: function (a, b, c) {
            //alert('Вонзикла ошибка: ' + a + ' ' + b + ' ' + c + '. В случае повторения ошибки обратитесь к разработчикам модуля.');
            console.log('Error: ' + a + ' ' + b + ' ' + c);
        }
    });

    $("fieldset legend a").click(function () {
        var i;
        if (i = this.href.indexOf('#')) {
            var id = this.href.substring(i + 1);
            if (id == 'options' && not_show_options) {
                //alert('Прежде, чем выбирать дополнительные опции, выберите товар.');
                return false;
            }
            //$("fieldset").children().filter("div").hide();
            $('div#' + id).show();
            autoHeightIframe();
        }
        return false;
    });

    var mode = 'new';
    if(args.order_id != undefined) {
        mode = args.order_id;
    }

    $('#addItemForm').validate({
        messages: {
            item_name: 'Вы не выбрали товар',
            amount: 'Вы не указали количество (1 - 999)'
        },
        highlight: function (element, errorClass) {
            $(element).addClass('has-error');
        },
        showErrors: function (errorMap, errorList) {
            this.defaultShowErrors();
            return true;
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('has-error');
        },
        submitHandler: function () {
            $.ajax({
                url: '/udata/emarket/co_AddItem/' + mode + '/.json',
                data: $('#addItemForm').serialize(),
                method: 'POST',
                success: function (response) {
                    if(response.result) {
                        var frame = $("iframe.umiPopupFrame", window.parent.document);
                        var contentWindow = window.parent;
                        if (frame.size() > 1) {
                            contentWindow = frame.get(frame.size() - 2).contentWindow;
                        }
                        if(mode == 'new') {
                            contentWindow.$.closePopupLayer();
                            contentWindow.reloadCart();
                        } else {
                            contentWindow.location.reload();
                        }
                    }
                }
            });
            return false;
        }
    });

    var wto;
    $('#search_string').keyup(function () {
        clearTimeout(wto);
        var val = $(this).val();
        $('.symlinkAutosuggest').addClass('hide');
        $('.symlinkAutosuggest ul').html('');
        autoHeightIframe('', 300);
        var li = '';
        var _val = '';
        wto = setTimeout(function () {
            if (val.length > 2) {
                $.ajax({
                    url: '/udata/emarket/co_ajaxSearchItem/' + val + '/' + args.domain_id + '/' + args.lang_id + '/.json',
                    beforeSend: function () {
                        $('#search_string').attr('readonly', 'readonly');
                        _val =  $('#search_string').val();
                        $('#search_string').val('Идет поиск...');
                    },
                    success: function (response) {
                        if (response.total) {
                            $.each(response.items.item, function (index, value) {
                                li += '<li';
                                li += ' ';
                                if (value.is_active == 0) {
                                    li += ' title="Товар не активен"';
                                }
                                li += '>';
                                li += '<span class="';
                                if (value.is_active == 0) {
                                    li += 'un';
                                }
                                li += 'active"><a href="' + value.link + '" target="_blank" title="Открыть ссылку в новом окне">'
                                li += value.name + ' (ID: ' + value.id + ')'
                                li += '</a>&nbsp;<a href="' + value.link + '" target="_blank" title="Открыть ссылку в новом окне"><img src="/styles/skins/mac/data/modules/emarket/img/ext_createorder/icon-new-window.gif" alt="" align="absmiddle" /></a></span>';
                                if (value.parent_name) {
                                    li += ' <span class="parent">' + value.parent_name + '</span>';
                                }
                                li += '<span class="choose" data-id="' + value.id + '" data-name="' + value.name + '" data-link="' + value.link + '" title="Выбрать товар">выбрать</span></li>';
                            });

                            if (response.total > 5 && response.total <= 10) {
                                autoHeightIframe('', 520);
                            } else if (response.total > 10 && response.total < 15) {
                                autoHeightIframe('', 720);
                            } else if (response.total >= 15) {
                                autoHeightIframe('', 1000);
                            }

                        } else {
                            li += '<li><span class="active">Товары не найдены, попробуйте уточнить запрос.</span></li>';
                        }
                        $('.symlinkAutosuggest ul').append(li);
                        $('.symlinkAutosuggest').removeClass('hide');
                    },
                    complete: function () {
                        $('#search_string').val(_val);
                        $('#search_string').removeAttr('readonly');
                    }
                });
            } else if (val.length > 0 && val.length < 3) {
                li += '<li><span class="active">Минимальная длина поискового запроса - 3 символа.</span></li>';
                $('.symlinkAutosuggest ul').append(li);
                $('.symlinkAutosuggest').removeClass('hide');
            } else {
                $('.symlinkAutosuggest ul').append('');
                $('.symlinkAutosuggest').addClass('hide');
            }
        }, 1000);
    });

    $('#addItemForm').on('click', '.symlinkAutosuggest .choose', function () {
        var id = parseInt($(this).attr('data-id'));
        var name = $(this).attr('data-name');
        var link = $(this).attr('data-link');
        $('#item_id').val(id);
        $('#item_name').text(name);
        $('#item_link').attr('href', link);
        $('#item_link').text(link);
        $('.pageslist').removeClass('hide');
        $('#search_string').val('');
        $('#search_string').attr('disabled', 'disabled');
        $.ajax({
            url: '/udata/emarket/co_ajaxSearchItemOptions/' + id + '/.json',
            success: function (response) {
                not_show_options = 0;
                if (response.total) {
                    var options = '<ul>';
                    $.each(response.items.item, function (index, value) {
                        options += '<li><label for="item_amount"><span>';
                        options += value.title + ':</span>';
                        options += '<select name="options[' + value.name + ']"><option value="0">выберите из списка</option>';
                        $.each(value.guide.item, function (guide_index, guide_value) {
                            options += '<option value="' + guide_value.id + '" data-float="' + guide_value.float + '">';
                            options += guide_value.title;
                            if (guide_value.float > 0) {
                                options += ' (+' + guide_value.float + ' за 1 шт.)';
                            }
                            options += '</option>';
                        });
                        options += '</select>';
                        options += '</label></li>';
                    });
                    options += '</ul>';
                    $('#options').html(options);
                    $('#options_tab').click();
                    var new_height = 380 + 50 * response.total;
                    autoHeightIframe('', new_height);
                } else {
                    $('#options_tab').click();
                    autoHeightIframe('', 380);
                }
                $('#total_price_value').text(response.original_price);
                $('#total_price_value').attr('data-original-price', response.original_price);
                recalcPrice();
                $('#total_price').removeClass('hide');
            }
        });
        $('.symlinkAutosuggest ul').append('');
        $('.symlinkAutosuggest').addClass('hide');
        //autoHeightIframe('', 530);
        return false;
    });

    $('#item_delete').click(function () {
        $('.pageslist').addClass('hide');
        $('#item_id').val('');
        $('#item_name').text('');
        $('#item_link').attr('href', '#');
        $('#item_link').text('');
        $('#search_string').removeAttr('disabled');
        $('#options').html('<p>У выбранного товара нет дополнительных опций.</p>');
        $('#options').css('display', 'none');
        $('#total_price_value').text('');
        $('#total_price').addClass('hide');
        autoHeightIframe('', 300);
        not_show_options = 1;
        return false;
    });

    $('#addItemForm').on('change', '#options select', function () {
        recalcPrice();
        return false;
    });

    $("#item_amount").inputmask({'mask': '9[99]'});
	$("#new-oitem-price").inputmask({'mask': '9[99999999]'});

    $('#item_amount').on('keyup blur change', function (ev) {
        var val = parseInt($(this).val());
        if (!isNaN(val)) {
            recalcPrice();
        }
        return false;
    });
	
	$('.add-new-oitem').on('click',function(){
		add_new_oitem = add_new_oitem ? 0 : 1;
		if(add_new_oitem) {
			$('.new-oitem-form').each(function(){
				$(this).removeClass('hide');
			});
			$('.item-name-form').addClass('hide');
			$('#item_delete').click();
			autoHeightIframe('', 380);
		} else {
			$('.new-oitem-form').each(function(){
				$(this).addClass('hide');
			});
			$('.item-name-form').removeClass('hide');
			autoHeightIframe('', 300);
		}
		$('#addItemForm input[name="is_new_item"]').val(add_new_oitem);
		return false;
	});

    autoHeightIframe('load');
});