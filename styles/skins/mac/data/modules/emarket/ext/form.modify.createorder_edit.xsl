<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl"
>

    <xsl:template match="udata" mode="order-items" priority="1">
        <xsl:variable name="order-info" select="document(concat('uobject://', @id))/udata" />

        <link href="/styles/skins/mac/data/modules/emarket/css/ext_createorder/style_edit.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">	$.getScript("/styles/skins/mac/data/modules/emarket/js/ext_createorder/jquery.blockUI.js");</script>
        <script type="text/javascript">
            var order_id = '<xsl:value-of select="@id" />';
        </script>
        <script type="text/javascript">	$.getScript("/styles/skins/mac/data/modules/emarket/js/ext_createorder/script_edit.js");</script>


        <div class="panel properties-group">
            <div class="header">
                <span>
                    <xsl:text>&label-order-items-group;</xsl:text>
                </span>
                <div class="l" /><div class="r" />
            </div>

            <div class="content">

                <div>
                    <a href="{$lang-prefix}/admin/emarket/editOrderAsUser/{@id}/">
                        <xsl:attribute name="title">&label-edit-as-user-tip;</xsl:attribute>
                        <xsl:text>&label-edit-as-user;</xsl:text>
                    </a>
                </div>

                <div class="imgButtonWrapper">
                    <a id="addOrderItem" href="#" class="type_select_gray">&order-add-item;</a>
                </div>

                <table class="tableContent left">
                    <thead>
                        <tr>
                            <th align="left">
                                <xsl:text>&label-order-items-group;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-current-price;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-discount;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-original-price;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-amount;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-summ;</xsl:text>
                            </th>

                            <th>
                                <xsl:text>&label-delete;</xsl:text>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:apply-templates select="items/item" mode="order-items" />
                        <xsl:apply-templates select="discount" mode="order-summary" />
                        <xsl:apply-templates select="summary/price/bonus" mode="order-summary" />
                        <xsl:apply-templates select="$order-info//group[@name = 'order_delivery_props']" mode="order_delivery" />

                        <tr>
                            <td>
                                <strong>
                                    <xsl:text>&label-order-items-result;:</xsl:text>
                                </strong>
                            </td>

                            <td colspan="4" />

                            <td>
                                <strong>
                                    <xsl:apply-templates select="summary/price/actual" mode="price" />
                                </strong>
                            </td>
                            <td />
                        </tr>
                    </tbody>
                </table>

                <xsl:call-template name="std-form-buttons" />
            </div>
        </div>
    </xsl:template>
        
</xsl:stylesheet>