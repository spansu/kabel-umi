<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl"
>

    <xsl:template match="/result[@method='createorder']/data[@type = 'form' and (@action = 'modify' or @action = 'create')]">
        <link href="/styles/skins/mac/data/modules/emarket/css/ext_createorder/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">	$.getScript("/styles/skins/mac/data/modules/emarket/js/ext_createorder/jquery.blockUI.js");</script>
        <script type="text/javascript">	$.getScript("/styles/skins/mac/data/modules/emarket/js/ext_createorder/script.js");</script>
        <form class="form_modify" id="form-createorder" method="post" action="do/" enctype="multipart/form-data">
            <input type="hidden" name="referer" value="{/result/@referer-uri}" id="form-referer" />
            <input type="hidden" name="domain" value="{$domain-floated}"/>
            <input type="hidden" name="permissions-sent" value="1" />
            <script type="text/javascript">
                var treeLink = function(key, value){
                var settings = SettingsStore.getInstance();
                return settings.set(key, value, 'expanded');
                }
            </script>
            <xsl:apply-templates mode="form-modify-createorder" />
        </form>
		<small>
			<a href="http://clean-code.ru/" target="_blank"><img src="/styles/skins/mac/data/modules/emarket/img/ext_createorder/logo.png" alt="Создание сайтов, модулей, расширений для UMI.CMS" align="absmiddle" />&nbsp;&nbsp;<xsl:text>Создание сайтов, модулей, расширений для&nbsp;UMI.CMS</xsl:text></a>
		</small>
        <script type="text/javascript">
            var order_id = '<xsl:value-of select="//result/data/object/@id" />';
            var method = '<xsl:value-of select="/result/@method" />';
                <![CDATA[
                    jQuery('form.form_modify').submit(function(e) {
                        var val = true;
                        var str = '<div id="errorList"><p class="error"><strong>' + getLabel('js-label-errors-found') + ':</strong></p><ol class="error">';
                        var isTextArea = false;
                        var editor = null;
                        var editorValue = '';
                        var isEmptyValue = false;

                        jQuery('.required', this).each(function(){
                            isTextArea = this.tagName && this.tagName.toLowerCase() === 'textarea';
                            if (isTextArea) {
                                editor = tinyMCE.get(this.id);

                                if (editor && typeof(editor.getContent) === 'function') {
                                    editorValue = editor.getContent({format: 'text'});
                                    isEmptyValue = (typeof(editorValue) === 'string' && editorValue.length === 0) ||
                                        (editorValue.length === 1 && editorValue.charCodeAt(0) === "\n".charCodeAt(0));
                                }
                            } else {
                                isEmptyValue = (this.value === '');
                            }

                            if (isEmptyValue) {
                                if (val === true) {
                                        val = false;
                                }
                                var innerText = jQuery('label[for=' + this.id + '] acronym').text();
                                str += '<li>' + getLabel('js-error-required-field') + ' "' + innerText + '".' + '</li>';
                            }
                        });
                        str += '</ol></div>';
                        if (val === false) {
                            if (jQuery("div").is("#errorList") === false) {
                                jQuery('div#page').before(str);
                            } else {
                                jQuery("#errorList").remove();
                                jQuery('div#page').before(str);
                            }
                            jQuery('body').scrollTop(0);
                        }
                        return val;
                    });
                ]]>
        </script>
    </xsl:template>
        
    <!-- Сюда записываем те группы полей, автоматический вывод которых нам не нужен -->
    <xsl:template match="properties/group[@name = 'order_credit_props' or @name = 'statistic_info' or @name = 'purchase_one_click' or @name = 'order_discount_props']" mode="form-modify-createorder" priority="1" />
    <!-- Сюда записываем те поля, автоматический вывод которых нам не нужен -->
    <xsl:template match="//result[@method='createorder']//field[@name = 'number' or @name = 'social_order_id' or @name = 'customer_id' or @name = 'domain_id' or @name = 'yandex_order_id' or @name = 'status_change_date' or @name = 'status_id' or @name = 'order_discount_value']" mode="form-modify" priority="1" />

    <xsl:template match="properties/group" mode="form-modify-createorder">
        <xsl:variable name="order-info" select="document(concat('udata://emarket/order/',//result/data/object/@id))" />

        <div class="panel properties-group" name="g_{@name}">
            <div class="header">
                <span class="c">
                    <xsl:value-of select="@title" />
                </span>
                <div class="l" />
                <div class="r" />
            </div>

            <div class="content">
                <xsl:apply-templates select="." mode="form-modify-group-fields" />
                <xsl:call-template name="std-form-buttons-add" />
            </div>
        </div>

        <xsl:if test="//result/@method = 'createorder' and @name = 'order_props'">
            <!-- Добавляем "виртуальную" группу, содержащую список товаров в заказе -->
            <xsl:call-template name="createorder-group-orderitems">
                <xsl:with-param name="order-info" select="$order-info" />
            </xsl:call-template>
            <!-- Добавляем "виртуальную" группу, содержащую информацию о покупателе -->
            <xsl:call-template name="createorder-group-customer">
                <xsl:with-param name="order-info" select="$order-info" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="group[@name = 'order_props']" mode="form-modify-group-fields">
        <input type="hidden" name="type-id" value="{../../@type-id}" />
        <input type="hidden" name="data[new][domain_id]" value="{//result/@domain-id}" />
        <xsl:apply-templates select="field[not(@type='wysiwyg' or @type='text')]" mode="form-modify" />
        <xsl:if test="//result//field[@name='bonus'] != ''">
            <xsl:apply-templates select="//result//field[@name='bonus']" mode="form-modify" />
        </xsl:if>
        <xsl:apply-templates select="field[@type='wysiwyg' or @type='text']" mode="form-modify" />
    </xsl:template>
    
    <xsl:template name="createorder-group-orderitems">
        <xsl:param name="order-info" />
        
        <div class="panel properties-group g_orderitems" name="g_orderitems">
            <div class="header">
                <span class="c">
                    <xsl:text>&group-order-items;</xsl:text>
                </span>
                <div class="l" />
                <div class="r" />
            </div>
            <div class="content">
                <div class="imgButtonWrapper">
                    <a id="addOrderItem" href="#" class="type_select_gray">&order-add-item;</a>
                </div>
                <table class="tableContent left">
                    <thead>
                        <tr>
                            <th align="left">
                                <xsl:text>&label-order-items-group;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-current-price;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-discount;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-original-price;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-amount;</xsl:text>
                            </th>

                            <th align="left">
                                <xsl:text>&label-order-items-summ;</xsl:text>
                            </th>

                            <th>
                                <xsl:text>&label-delete;</xsl:text>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="createorder-order-items">
                        <xsl:choose>
                            <xsl:when test="count($order-info//items/item) &gt; 0">
                                <xsl:apply-templates select="$order-info//items/item" mode="order-items-createorder" />
                            </xsl:when>
                            <xsl:otherwise>
                                <tr>
                                    <td colspan="7" style="text-align: center">
                                        <xsl:text>&order-no-items;</xsl:text>
                                    </td>
                                </tr>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:apply-templates select="$order-info/udata/discount" mode="order-summary" />
                        <xsl:apply-templates select="$order-info//summary/price/bonus" mode="order-summary" />
                        <xsl:apply-templates select="$order-info/udata/delivery" mode="order-delivery-createorder" />
                        <tr>
                            <td>
                                <strong>
                                    <xsl:text>&label-order-items-result;:</xsl:text>
                                </strong>
                            </td>

                            <td colspan="4" />
                            <td>
                                <strong>
                                    <span>
                                        <xsl:choose>
                                            <xsl:when test="$order-info//summary/amount &gt; 0">
                                                <xsl:apply-templates select="$order-info//summary/price/actual" mode="price" />
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="$order-info//summary/price/@prefix != ''">
                                                        <xsl:value-of select="concat($order-info//summary/price/@prefix, ' 0')" />
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="concat('0 ',$order-info//summary/price/@suffix)" />
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </span>
                                </strong>
                            </td>
                            <td />
                        </tr>
                    </tbody>
                </table>
                <xsl:call-template name="std-form-buttons-add" />
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="item" mode="order-items-createorder">
        <tr>
            <td>
                <xsl:apply-templates select="document(concat('uobject://',@id))/udata/object" mode="order-item-name" />
            </td>

            <td>
                <xsl:choose>
                    <xsl:when test="price/original &gt; 0">
                        <xsl:apply-templates select="price/original" mode="price" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="price/actual" mode="price" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>

            <td>
                <xsl:apply-templates select="discount" />
            </td>

            <td>
                <xsl:apply-templates select="price/actual" mode="price" />
            </td>

            <td>
                <input type="text" name="order-amount-item[{@id}]" value="{amount}" class="change-amount" data-item-id="{@id}" size="3" />
            </td>

            <td>
                <xsl:apply-templates select="total-price/actual" mode="price" />
            </td>

            <td class="center">
                <a href="#" data-item-id="{@id}" class="remove-item">
                    <img src="/images/cms/admin/mac/tree/ico_del.png" alt="Удалить товары из заказа"  />
                </a>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template name="createorder-group-customer">
        <xsl:param name="order-info" />
        <div class="panel properties-group g_customer" name="g_customer">
            <div class="header">
                <span class="c">
                    <xsl:text>&group-customer;</xsl:text>
                </span>
                <div class="l" />
                <div class="r" />
            </div>
            <div class="content">
                <div class="field">
                    <span class="label">
                        <acronym title="&customer-type-tip;" class="acr">&customer-type;</acronym>
                    </span>
                    <p>
                        <label class="inline">
                            <input type="radio" class="checkbox" name="customer_type" value="guest" data-type-id="{document('udata://emarket/co_getNewCustomerTypeId/')}" />
                            <acronym>&new-customer;</acronym>
                        </label>
                    </p>
                    <p>
                        <label class="inline">
                            <input type="radio" class="checkbox" name="customer_type" value="existing" />
                            <acronym>&existing-customer;</acronym>
                        </label>
                    </p>
                </div>
                <div class="field"></div>
                <div id="customer_type_existing" class="hide">
                    <div class="field">
                        <span>
                            <input type="text" name="search_customer" placeholder="Начните вводить имя, или любую другую характеристику покупателя" class="string" id="search-customer" />
                            <div class="symlinkAutosuggest hide">
                                <ul></ul>
                            </div>
                        </span>
                    </div>
                </div>
                <div id="customer-info" class="hide">
                    <p>
                        <xsl:text>&customer;: </xsl:text>
                        <span></span>
                    </p>
                </div>
                
                <xsl:call-template name="std-form-buttons-add" />
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="group" mode="form-modify-group-fields">
        <xsl:apply-templates select="field" mode="form-modify" />
    </xsl:template>

    <xsl:template match="//result[@method='createorder']//field[@name = 'legal_person']" mode="form-modify" />
    
    <xsl:template match="//result[@method='createorder']//field[@name = 'order_date']" mode="form-modify">
        <div class="field datePicker">
            <label for="{generate-id()}">
                <span class="label">
                    <acronym>
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span>
                    <input type="text" name="{@input_name}" id="{generate-id()}" value="{document('udata://system/convertDate/now/Y-m-d%20H:i')/udata}">
                        <xsl:apply-templates select="." mode="required_attr" />
                    </input>
                </span>
            </label>
        </div>
    </xsl:template>
    
    <xsl:template match="//result[@method='createorder']//field[@name='delivery_price']" mode="form-modify">
        <div class="field">
            <label for="{generate-id()}">
                <span class="label">
                    <acronym>
                        <xsl:choose>
                            <xsl:when test=". != ''">
                                <xsl:apply-templates select="." mode="sys-tips" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="title">
                                    <xsl:text>&sets-automaticly;</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="class">
                                    <xsl:text>acr</xsl:text>
                                </xsl:attribute>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span class="delivery-price">
                    <input type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
                        <xsl:apply-templates select="." mode="required_attr">
                            <xsl:with-param name="old_class" select="@type" />
                        </xsl:apply-templates>
                    </input>
                    <a href="#" id="calcDeliveryPrice" class="hide">&calc-delivery-price;</a>
                </span>
            </label>
        </div>
    </xsl:template>
    
    <xsl:template match="//result[@method='createorder']//field[@type = 'relation' and (@name = 'delivery_id' or @name='payment_id')]" mode="form-modify">
        <div class="field relation" id="{generate-id()}" umi:type="{@type-id}">
            <xsl:choose>
                <xsl:when test="@multiple = 'multiple'">
                    <xsl:attribute name="style">
                        <xsl:text>height:130px;</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when test="@public-guide = '1'">
                    <xsl:attribute name="style">
                        <xsl:text>height:100px;</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
            <xsl:if test="not(@required = 'required')">
                <xsl:attribute name="umi:empty">
                    <xsl:text>empty</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <label for="relationSelect{generate-id()}">
                <span class="label">
                    <acronym>
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span>
                    <select name="{@input_name}" id="relationSelect{generate-id()}" class="select-{@name}" data-property="{@name}">
                        <xsl:apply-templates select="." mode="required_attr" />
                        <xsl:if test="@multiple = 'multiple'">
                            <xsl:attribute name="multiple">multiple</xsl:attribute>
                            <xsl:attribute name="style">height: 62px;</xsl:attribute>
                        </xsl:if>
                        <xsl:if test="not(values/item/@selected)">
                            <option value=""></option>
                        </xsl:if>
                        <xsl:apply-templates select="values/item" />
                    </select>
                </span>
            </label>
            <input type="text" id="relationInput{generate-id()}" class="search_input" />
        </div>
    </xsl:template>
    
    <xsl:template match="delivery" mode="order-delivery-createorder" />

    <xsl:template match="delivery[method/@id != '']" mode="order-delivery-createorder">
        <xsl:variable name="delivery-item" select="document(concat('uobject://',method/@id))" />
        <xsl:variable name="delivery-price" select="price/delivery" />
        <tr>
            <td>
                <xsl:text>&label-order-delivery;: </xsl:text>
                <a href="{$lang-prefix}/admin/emarket/delivery_edit/{$delivery-item//@id}/">
                    <xsl:value-of select="$delivery-item//@name" />
                </a>
            </td>
            <td colspan="4" />

            <td>
                <xsl:apply-templates select="document(concat('udata://emarket/applyPriceCurrency/', $delivery-price, '/'))/udata/price/actual" mode="price" />
            </td>
            <td />
        </tr>
    </xsl:template>

    <xsl:include href="form.modify.createorder_edit.xsl" />

</xsl:stylesheet>