<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

    <xsl:template match="udata">
        <xsl:apply-templates select="group" mode="form">
            <xsl:with-param name="method" select="@method"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="group" mode="form">
        <xsl:param name="method" select="'co_getCreateForm'"/>

        <xsl:if test="@name != 'idetntify_data' and @name != 'statistic_info'">
            <fieldset class="collapsible">
                <legend>
                    <a href="#group_{@name}">
                        <xsl:value-of select="@title" disable-output-escaping="yes"/>
                    </a>
                </legend>
                <div id="item_{@name}" style="display: block;">
                    <ul>
                        <xsl:apply-templates select="field" mode="form"/>
                    </ul>
                </div>
            </fieldset>
        </xsl:if>
    </xsl:template>

    <xsl:template match="field" mode="form">
        <li>
            <label for="form-control-{@name}">
                <span>
                    <xsl:value-of select="@title"/>
                    <xsl:apply-templates select="@required" mode="form"/>
                    <xsl:text>:</xsl:text>
                </span>
                <xsl:apply-templates select="." mode="form-field"/>
            </label>
        </li>
    </xsl:template>

    <xsl:template match="field" mode="form-field">
        <input type="text" id="form-control-{@name}" class="{@name}" name="{@input_name}" value="{.}"
               placeholder="{@tip}">
            <xsl:choose>
                <xsl:when test="@required = 'required'">
                    <xsl:attribute name="class">
                        <xsl:text>required </xsl:text>
                        <xsl:value-of select="@name"/>
                        <xsl:if test="@type = 'int' or @type = 'float' or @type = 'counter' or @type = 'price'">
                            <xsl:text> number</xsl:text>
                        </xsl:if>
                    </xsl:attribute>
                </xsl:when>
                <xsl:when
                        test="not(@required) and (@type = 'int' or @type = 'float' or @type = 'counter' or @type = 'price')">
                    <xsl:attribute name="class">
                        <xsl:value-of select="@name"/>
                        <xsl:text> number</xsl:text>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
        </input>
    </xsl:template>

    <xsl:template match="field[@type = 'relation']" mode="form-field">
        <select id="form-control-{@name}" class="{@name}" name="{@input_name}">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="class">
                    <xsl:text>required </xsl:text>
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@multiple = 'multiple'">
                <xsl:attribute name="multiple">multiple</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="values/item" mode="form-field"/>
        </select>
    </xsl:template>

    <xsl:template match="item" mode="form-field">
        <option value="{@id}">
            <xsl:copy-of select="@selected"/>
            <xsl:value-of select="."/>
        </option>
    </xsl:template>

    <xsl:template match="field[@type = 'boolean']" mode="form-field">
        <input type="hidden" name="{@input_name}" value="0"/>
        <input id="form-control-{@name}" type="checkbox" class="boolean {@name}" name="{@input_name}" value="1">
            <xsl:copy-of select="@checked"/>
        </input>
    </xsl:template>

    <xsl:template match="field[@type = 'text' or @type = 'wysiwyg']" mode="form-field">
        <textarea type="text" id="form-control-{@name}" class="{@name}" name="{@input_name}" cols="" rows="5"
                  placeholder="{@tip}">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="class">
                    <xsl:text>required </xsl:text>
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="."/>
        </textarea>
    </xsl:template>

    <xsl:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file']" mode="form-field">
        <xsl:if test="@relative-path != ''">
            <a href="{@relative-path}" target="_blank" class="file-link" title="Открыть файл в новом окне">
                <xsl:value-of select="@relative-path" disable-output-escaping="yes"/>
                <span>
                    <img src="/styles/skins/mac/data/modules/emarket/img/ext_createorder/icon-new-window.gif" alt=""
                         align="absmiddle"/>
                </span>
            </a>
            <a href="#" class="del-link del-file" data-file-input-id="form-control-{@name}"
               data-file-input-name="{@input_name}">удалить
            </a>
        </xsl:if>

        <input type="file" id="form-control-{@name}" class="{@name}" name="{@input_name}">
            <xsl:if test="@required = 'required'">
                <xsl:attribute name="class">
                    <xsl:text>required </xsl:text>
                    <xsl:value-of select="@name"/>
                    <xsl:if test="@relative-path != ''">
                        <xsl:text> hide</xsl:text>
                    </xsl:if>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="not(@required) and @relative-path != ''">
                <xsl:attribute name="class">
                    <xsl:value-of select="@name"/>
                    <xsl:if test="@relative-path != ''">
                        <xsl:text> hide</xsl:text>
                    </xsl:if>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="@relative-path != ''">
                <xsl:attribute name="name">
                    <xsl:text>fake_</xsl:text>
                    <xsl:value-of select="@input_name"/>
                </xsl:attribute>
                <xsl:attribute name="aria-invalid">
                    <xsl:text>false</xsl:text>
                </xsl:attribute>
            </xsl:if>
        </input>
    </xsl:template>

    <xsl:template match="@required" mode="form">
        <span class="required">*</span>
    </xsl:template>

</xsl:stylesheet>