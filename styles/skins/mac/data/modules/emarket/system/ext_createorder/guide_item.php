<?php

if(isset($_GET['iid']) && intval($_GET['iid']) > 0) {
    $action = 'co_ajaxSetGuideItem/update/'.intval($_GET['iid']);
    $button_title = 'Обновить';
} else {
    $action = 'co_ajaxSetGuideItem/add/'.intval($_GET['type_id']);
    $button_title = 'Добавить';
}

?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <script type="text/javascript" src="/js/jquery/jquery.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/jquery/jquery-ui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/jquery/jquery.umipopups.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/smc/compressed.js"></script>
        <script type="text/javascript" src="/styles/skins/mac/data/modules/emarket/js/ext_createorder/jquery.inputmask.bundle.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/mac/data/modules/emarket/js/ext_createorder/jquery.validate.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/mac/data/modules/emarket/js/ext_createorder/popup_script.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/mac/data/modules/emarket/js/ext_createorder/jquery.form.js" charset="utf-8"></script>
        <script type="text/javascript" src="/styles/skins/mac/data/modules/emarket/js/ext_createorder/guide_item.js" charset="utf-8"></script>
        <link href="/styles/skins/_eip/css/popup_page.css" rel="stylesheet" type="text/css" />
        <link href="/styles/skins/_eip/css/symlink.control.css" rel="stylesheet" type="text/css" />
        <link href="/styles/skins/mac/data/modules/emarket/css/ext_createorder/popup_forms.css" rel="stylesheet" type="text/css" />
    </head>
    <body id="modal_page">
        <form enctype="multipart/form-data" action="/udata/emarket/<?php echo $action; ?>/.json" method="post" id="guideItem">
            <input type="hidden" name="name" value="<?php if(isset($_GET['name'])) echo $_GET['name']; ?>" />
            <div id="guide-content"></div>
            <fieldset id="errors_overlay">
            </fieldset>
            <div class="eip_buttons">
                <input type="submit" value="<?php echo $button_title;  ?>" class="primary ok">
                <input type="button" value="Вернуться назад" class="back" onclick="onGuideClose()">
                <div style="clear:both;"></div>
            </div>
        </form>
    </body>
</html>