<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:php="http://php.net/xsl" xmlns:xslt="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/result[@method = 'img_alt_config']/data">
        <form method="post" action="do/" id="typograf-config-form" enctype="multipart/form-data">
            <xsl:apply-templates select="." mode="settings.modify" />
        </form>
        <small>
            <a href="http://clean-code.ru/" target="_blank">
                <img src="/styles/skins/mac/data/modules/config/img/ext_img_alt_config/logo.png" align="absmiddle" />
                <xsl:text>&nbsp;&nbsp;Создание сайтов, модулей, расширений для UMI.CMS</xsl:text>
            </a>
        </small>
    </xsl:template>

    <xsl:template match="/result[@method = 'img_alt_config']//group[@name != 'common']" mode="settings.modify">
        <div class="panel properties-group">
            <div class="header">
                <span>
                    <xsl:text>&alt-img-label-for; </xsl:text>
                    <xsl:value-of select="@name" />
                </span>
                <div class="l" /><div class="r" />
            </div>
            <div class="content">
                <table class="tableContent">
                    <tbody>
                        <xsl:apply-templates select="option" mode="settings.modify" />
                    </tbody>
                </table>

                <xsl:call-template name="std-save-button" />
            </div>
        </div>
    </xsl:template>

    <xsl:template match="/result[@method = 'img_alt_config']//group[@name != 'common']/option[@type = 'string']" mode="settings.modify-option">
        <input type="text" name="{@name}[{../@name}]" value="{value}" id="{@name}" />
    </xsl:template>

</xsl:stylesheet>