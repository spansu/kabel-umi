var file_name='';
var total_num_rows = 0;
var shift = 0;
var per_page = 10;
var total_sel_num_rows = 0;
var col_id = false;
var row_id = false;
var symlink_type = "";
var mouse_x = false;
var mouse_y = false;
var hidePosScroll = false;
var search_mode = true;
var refresh_errors = true;
var horScrollContent = 0;
var lang_id='';
var scroll_do = false;
var file_touched = 0;
var for_disabled_active_element = false;
var export_setInterval = false;
/*mousewheel*/!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a:a(jQuery)}(function(a){function b(b){var g=b||window.event,h=i.call(arguments,1),j=0,l=0,m=0,n=0,o=0,p=0;if(b=a.event.fix(g),b.type="mousewheel","detail"in g&&(m=-1*g.detail),"wheelDelta"in g&&(m=g.wheelDelta),"wheelDeltaY"in g&&(m=g.wheelDeltaY),"wheelDeltaX"in g&&(l=-1*g.wheelDeltaX),"axis"in g&&g.axis===g.HORIZONTAL_AXIS&&(l=-1*m,m=0),j=0===m?l:m,"deltaY"in g&&(m=-1*g.deltaY,j=m),"deltaX"in g&&(l=g.deltaX,0===m&&(j=-1*l)),0!==m||0!==l){if(1===g.deltaMode){var q=a.data(this,"mousewheel-line-height");j*=q,m*=q,l*=q}else if(2===g.deltaMode){var r=a.data(this,"mousewheel-page-height");j*=r,m*=r,l*=r}if(n=Math.max(Math.abs(m),Math.abs(l)),(!f||f>n)&&(f=n,d(g,n)&&(f/=40)),d(g,n)&&(j/=40,l/=40,m/=40),j=Math[j>=1?"floor":"ceil"](j/f),l=Math[l>=1?"floor":"ceil"](l/f),m=Math[m>=1?"floor":"ceil"](m/f),k.settings.normalizeOffset&&this.getBoundingClientRect){var s=this.getBoundingClientRect();o=b.clientX-s.left,p=b.clientY-s.top}return b.deltaX=l,b.deltaY=m,b.deltaFactor=f,b.offsetX=o,b.offsetY=p,b.deltaMode=0,h.unshift(b,j,l,m),e&&clearTimeout(e),e=setTimeout(c,200),(a.event.dispatch||a.event.handle).apply(this,h)}}function c(){f=null}function d(a,b){return k.settings.adjustOldDeltas&&"mousewheel"===a.type&&b%120===0}var e,f,g=["wheel","mousewheel","DOMMouseScroll","MozMousePixelScroll"],h="onwheel"in document||document.documentMode>=9?["wheel"]:["mousewheel","DomMouseScroll","MozMousePixelScroll"],i=Array.prototype.slice;if(a.event.fixHooks)for(var j=g.length;j;)a.event.fixHooks[g[--j]]=a.event.mouseHooks;var k=a.event.special.mousewheel={version:"3.1.12",setup:function(){if(this.addEventListener)for(var c=h.length;c;)this.addEventListener(h[--c],b,!1);else this.onmousewheel=b;a.data(this,"mousewheel-line-height",k.getLineHeight(this)),a.data(this,"mousewheel-page-height",k.getPageHeight(this))},teardown:function(){if(this.removeEventListener)for(var c=h.length;c;)this.removeEventListener(h[--c],b,!1);else this.onmousewheel=null;a.removeData(this,"mousewheel-line-height"),a.removeData(this,"mousewheel-page-height")},getLineHeight:function(b){var c=a(b),d=c["offsetParent"in a.fn?"offsetParent":"parent"]();return d.length||(d=a("body")),parseInt(d.css("fontSize"),10)||parseInt(c.css("fontSize"),10)||16},getPageHeight:function(b){return a(b).height()},settings:{adjustOldDeltas:!0,normalizeOffset:!0}};a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})});

$(document).ready(function()
{
    $("body").click(function(){
        $("#context_menu").css("display","none");
        refresh_width();
    });

    //Инициализация модуля
    $.ajax({
        url : "/udata/prices_import/init_module/",
        dataType : 'html',
        success : function(data) {
        }
    });

    //Определение языка
    lang_id = $("#gen_content").attr("lang-id");
    lang_id = (lang_id != undefined) ? lang_id : $(".panel.export").attr("lang_id");

    $.getScript("/styles/skins/mac/data/modules/prices_import/js/jquery.form.js").then(function(){

        var options = {
            beforeSend: function()
            {
                $("#upload_select_file").css('display','none');
                $("#upload_info").css('display','inline');
            },
            uploadProgress: function(event, position, total, percentComplete)
            {
                $("#upload_progress_bar").html(percentComplete+'%');
            },
            success: function()
            {
            },
            complete: function(response)
            {
                $("#content_file").slideDown("fast");
                file_name=response.responseText;

                file_name = parse_xml(file_name,'udata');

                $("#upload_info").css('display','none');
                $("#upload_button").css('display','none');

                //Предустановленные параметры
                $.ajax({
                    url : "/udata/prices_import/transformXML/?xslpath=/styles/skins/mac/data/modules/prices_import/system/list.xsl&require=prices_import/set_params_div/"+file_name+"/"+lang_id,
                    dataType : 'html',
                    success : function(data) {
                        $("#td_set_params_div").html(data);
                        $("#td_set_params_div").css('display','');
                        $("#gen_content").css("height","516px");
                        refresh_errors = true;
                        shift = 0;
                        get_file(false, 2);
                    }
                });

            },
            error: function()
            {
                openDialog({
                    title: "Ошибка",
                    text: "Произошла ошибка.<br/><br/>",
                    width: 390,
                    stdButtons : false
                });
            }
        };

        $("#upload").ajaxForm(options);
        $("#upload_button").css('display','');
    });

});

//Выбор и загрузка файла
$("#select_file_upload a").live("click", function(){
    file_touched = 1;
    $("#upload_file").val("");
    if ($(this).hasClass("new_upload")){ 
        openDialog({
            title: "Загрузка файла",
            text: "В случае загрузки другого файла, все несохраненные данные удалятся. Вы хотите продолжить?",
            width: 390,
            OKCallback : function () {
                $("#upload_file").trigger("click");
            }
        });
    } else {
        $("#upload_file").trigger("click");
    }
    return false;
});

$("#upload_file").live("change", function(){
    if ($(this).val() != "") $(this).closest("form").submit();
});

//Закртыие панели настроек
$("#close_set_params_div").live("click", function(){
    $(this).css("display","none");
    $("#set_params").css("display","none");
    $("#open_set_params_div").css("display","block");
    refresh_width();
});

//Открытие панели настроек
$("#open_set_params_div").live("click", function(){
    $(this).css("display","none");
    $("#set_params").css("display","block");
    $("#close_set_params_div").css("display","block");
    refresh_width();
});

//Справка по модулю
$("body").on("click",".faq_subtitle", function(){
    var this_ = $(this);
    var data_content_id = $(this).attr("data-content-id");
    var faq_content = $(".faq_content[data-content-id = '"+data_content_id+"']");
    if ($(this).hasClass("closed")){
        this_.removeClass("closed");
        faq_content.css("display","block");
    } else {
        this_.addClass("closed");
        faq_content.css("display","none");
    }
});

// Изменение идентификаторов
$("#head_table select").live("change", function(){
    var value = $(this).val();
    var col = $(this).attr('col');
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/apply_change_id/"+file_name,
        dataType : 'html',
        data:{value:value, col:col},
        success : function(data) {
            refresh_errors = true;
            get_file();
        }
    });
});

//Изменение активности по умолчанию
$("#default_is_active").live("change", function(){
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/default_is_active/"+file_name+"/"+$(this).prop("checked"),
        dataType : 'html',
        success : function(data) {
            $("#preloader").css("display","none");
        }
    });
});

//Изменение видимости новых объектов по умолчанию
$("#not_add_element").live("change", function(){
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/not_add_element/"+file_name+"/"+$(this).prop("checked"),
        dataType : 'html',
        success : function(data) {
            $("#preloader").css("display","none");
        }
    });
});

//Перезагрузить таблицу
$("#reload_page").live("click",function(){
    get_file();
});

//Удаление строки
$(".cmenuItem[item_id='3']").live("click",function(){
    $("#preloader").css("display","inline");
    row_id = $("#content_table tr.select:first").attr("i_");
    $.ajax({
        url : "/udata/prices_import/del_selected_str/"+file_name+"/"+row_id,
        dataType : 'html',
        success : function(data) {
            refresh_errors = true
            get_file();
        }
    });
});

//Удаление выбранных элементов
$(".cmenuItem[item_id='4']").live("click",function(){
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/del_selected_str/"+file_name,
        dataType : 'html',
        success : function(data) {
            refresh_errors = true;
            get_file();
        }
    });
});

//Выделение  ячеек таблицы
$("#content_table tr:not(#head_table) td").live("mousedown", function(event){
    disabled_active_element();
    row_id = $(this).closest("tr").attr("i_");
    col_id = $(this).attr("i_");
    if ($(this).closest("tr").attr("class")!="head") {
        $("#content_table tr").removeClass("select");
        $("#content_table tr#i"+row_id).addClass("select");
        $("#content_table td").removeClass("select");
        $("#content_table td#i"+col_id).addClass("select");
        $(".edit_row_col").css('display', 'inline');
    }
    if(event.button != 2) $("#context_menu").css("display","none");
    if(event.button == 2){
        var content_bottom = $("#content_file_table").position().top + $("#content_file_table").height();
        var shift = content_bottom - (mouse_y + $("#context_menu").height() + 35);
        if (shift > 0) shift = 0;
        $("#context_menu").css("top",(mouse_y + shift) + "px");
        $("#context_menu").css("left",mouse_x + "px");
        $("#context_menu").css("display","block");
        return false;
    }
});
$("body").on("click","#content_file",function(){
    for_disabled_active_element = true;
});
$("body").on("click","#content_file *", function(){
    if (for_disabled_active_element){
        if ($(this).attr("id") == "content_file_table"){
            disabled_active_element();
        }
        for_disabled_active_element = false;
    }
});

//Выделение строки 
$(".row_select").live("change", function(){
    row_id = $(this).closest('tr').attr("i_");
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/select_row/"+file_name+"/"+row_id,
        dataType : 'html',
        success : function(data) {
            refresh_errors = true;
            get_file();
        }
    });
});

//Выделение всех строк 
$(".empty").live("change", function(){
    var uri = "";
    if ($(this).prop("checked")){
        if ($("#first_row_head").prop("checked")) uri = "sel_all"; else uri = "sel_all_head";
    }
    else {
        uri = "del_all";
    }
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/select_row/"+file_name+"/"+uri,
        dataType : 'html',
        success : function(data) {
            refresh_errors = true;
            get_file();
        }
    });

});

//Сохранение нового шаблона ===
$("#save_template").live("click",function(){
    var template_name = $("#new_template_name").val();
    if (template_name == ""){
        openDialog({
            title: "Сохранение шаблона",
            text: "Укажите название шаблона<br/><br/>",
            width: 390,
            stdButtons : false
        });
        return false;
    }
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/save_template/"+file_name+"/",
        dataType : 'html',
        data : {template_name: template_name},
        success : function(data) {
            get_file();
            //Обновление списка шаблонов
            $.ajax({
                url : "/udata/prices_import/transformXML/?xslpath=/styles/skins/mac/data/modules/prices_import/system/list.xsl&require=prices_import/getListTemplates/",
                dataType : 'html',
                success : function(data) {
                    $("#templates #select_templates").html(data);
                    $("#templates #select_templates").val(template_name);
                    $("#templates #select_templates").attr("data-current", template_name);
                    $("#templates #select_templates option:first").html("Отменить выбранный шаблон");
                    $("#templates #select_templates option:first").prop("disabled", false);
                    $('#save_template').val("Обновить");
                }
            });
        }
    });
    return false;
});

//Применить выбранный шаблон для прайса 
$("#templates #select_templates").live("change",function(){
    var template_name = $(this).val();
    var code = $("#templates #select_templates option:selected").attr('code');
    var type_id_default = $("#templates #select_templates option:selected").attr('type_id_default');
    var category_default = $("#templates #select_templates option:selected").attr('category_default');
    var is_active_default = $("#templates #select_templates option:selected").attr('is_active_default');
    var not_add_element = $("#templates #select_templates option:selected").attr('not_add_element');
    var category_default_path = $("#templates #select_templates option:selected").attr('category_default_path');
    var symb_separate_csv = $("#templates #select_templates option:selected").attr('symb_separate_csv');
    if (template_name == "") {
        $("#templates #select_templates option:first").html("Выберите из списка");
        $("#templates #select_templates option:first").prop("disabled", true);
        $("#preloader").css("display","inline");
        $.ajax({
            url : "/udata/prices_import/applyTemplate/"+file_name+"/",
            dataType : 'html',
            data : {template_name: template_name},
            success : function(data) {
                $.ajax({
                    url : "/udata/prices_import/transformXML/?xslpath=/styles/skins/mac/data/modules/prices_import/system/list.xsl&require=prices_import/set_params_div/"+file_name+"/"+lang_id,
                    dataType : 'html',
                    success : function(data) {
                        $("#td_set_params_div").html(data);
                        refresh_errors = true;
                        get_file(true);
                    }
                });
            }
        });
        return false;
    } else {
        openDialog({
            title: "Применение шаблона",
            text: "Применить шаблон \""+template_name+"\" ?",
            width: 390,
            OKCallback : function () {
                $("#preloader").css("display","inline");
                $.ajax({
                    url : "/udata/prices_import/applyTemplate/"+file_name+"/",
                    dataType : 'html',
                    data : {template_name: template_name},
                    success : function(data) {
                        if (data.indexOf("error")!= -1) {
                            openDialog({
                                title: "Применение шаблона",
                                text: "Данный шаблон не может быть применен к этому прайс-листу. Несоответствие данных.<br/><br/>",
                                width: 390,
                                stdButtons : false
                            });
                            $("#preloader").css("display","none");
                            var current_template = $("#templates #select_templates").attr("data-current");
                            if (current_template != undefined){
                                $("#templates #select_templates").val(current_template);
                            } else $("#templates #select_templates").val('');

                            return false;
                        }

                        $("#templates #select_templates option:first").html("Отменить выбранный шаблон");
                        $("#templates #select_templates option:first").prop("disabled", false);
                        $("#templates #select_templates").attr("data-current", template_name);
                        if (is_active_default == '1') $("#default_is_active").prop('checked', true); else $("#default_is_active").prop('checked', false);
                        if (not_add_element == '1') $("#not_add_element").prop('checked', true); else $("#not_add_element").prop('checked', false);
                        $("#mode_edit_type_"+code).prop("selected", true);
                        $("#default_type_id").val(type_id_default);
                        $("#change_default_parent").html(category_default_path);
                        $("#symb_separate_csv select").val(symb_separate_csv);
                        $("#new_template_name").val(template_name);
                        $('#save_template').val("Обновить");
                        refresh_errors = true;
                        get_file(true);
                    }
                });
            },
            cancelCallback : function() {
                var current_template = $("#templates #select_templates").attr("data-current");
                if (current_template != undefined){
                    $("#templates #select_templates").val(current_template);
                } else $("#templates #select_templates").val('');
            }
        });
    }
});

//Удалить выбранный шаблон для прайса 
$("#templates #delete_templates").live("change",function(){
    var template_name = $(this).val();
    if (template_name == "") return false;
    openDialog({
        title: "Удаление шаблона",
        text: "Удалить шаблон \""+template_name+"\" ?",
        width: 390,
        OKCallback : function () {
            $.ajax({
                url : "/udata/prices_import/deleteTemplate/",
                dataType : 'html',
                data : {template_name: template_name},
                success : function(data) {
                    $.ajax({
                        url : "/udata/prices_import/transformXML/?xslpath=/styles/skins/mac/data/modules/prices_import/system/list.xsl&require=prices_import/getListTemplates/",
                        dataType : 'html',
                        success : function(data) {
                            $("#templates #select_templates").html(data);
                            $("#templates #delete_templates").html(data);
                        }
                    });
                }
            });
        }
    });
});

//Войти в панель удаления шаблонов
$("#templates .edit").live("click", function(){
    if ($(this).hasClass("open")) {
        $(this).removeClass("open");
        $("#templates .body #insert").css("display","block");
        $("#templates .body #remove").css("display","none");
    } else {
        $(this).addClass("open");
        $("#templates .body #insert").css("display","none");
        $("#templates .body #remove").css("display","block");
    }


});

$("#stop_btn").live("click", function(){
    process = false;
});

//Импорт 
$(".do_import").live("click",function(){
    var type=$(this).data('type');
    if (!parseInt(total_sel_num_rows)){
        openDialog({
            title: "Импорт",
            text: "Нет выделенных строк<br/><br/>",
            width: 390,
            stdButtons : false
        });
        return false;
    }

    var warning_message = "";
    var warning_1 = false;
    var warning_2 = false;

    var verify=true;
    var arr=[];

    $("#head_table select option:selected").each(function (i) {
        //Проверка повтора выбранных идентификаторов полей
        if ($.inArray($(this).val(),arr)==-1) {
            if($(this).val()!="") arr.push($(this).val());
            else warning_1 = true;
        }
        else {
            openDialog({
                title: "Импорт",
                text: "Идентификаторы полей не должны повторяться.<br/><br/>",
                width: 390,
                stdButtons : false
            });
            //alert("Идентификаторы полей не должны повторяться.");
            verify=false;
            return false;
        }
    });

    if (!$(".key.active").length) warning_2 = true;

    if (verify){} else return false;

    if (warning_1) warning_message = "- Не на всех колонках таблицы установлены идентификаторы полей.<br />";
    if (warning_2) warning_message = warning_message + "- Не установлен ключевой идентификатор (возможно будут созданы новые объекты).<br />";
    warning_message = warning_message + "- Проверьте правильность указания категории по умолчанию.<br />";

    if (type == 'current'){
        openDialog({
            title: "Импорт",
            text: warning_message+"<br/><br/>Продолжить?",
            width: 390,
            OKCallback : function () {
                $.ajax({
                    url : "/udata/prices_import/start_import/"+file_name,
                    dataType : 'html',
                    success : function(data) {
                        if(window.session) {
                            window.session.startAutoActions();
                        }
                        refresh_errors = -1;
                        do_selected("import_");
                    }
                });
            }
        });
    }
    return false;
});

//Экспорт
$(".export #export").on("click",function(){
    if (!$("input[name='data[parent-id][]']:last").val()) {
        $("div[name='data[parent-id][]'] input[type='text']").addClass("error");
    } else $("div[name='data[parent-id][]'] input[type='text']").removeClass("error");
    if (!$("[name='data[type]']").val()) {
        $("[name='data[type]']").addClass("error");
    } else $("[name='data[type]']").removeClass("error");
    if (!$("[name='data[template]']").val()) {
        $("[name='data[template]']").addClass("error");
    } else $("[name='data[template]']").removeClass("error");

    if (export_setInterval !== false) clearInterval(export_setInterval);
    export_setInterval = setInterval(function(){
        if (!$("input[name='data[parent-id][]']:last").val()) {
            $("div[name='data[parent-id][]'] input[type='text']").addClass("error");
        } else $("div[name='data[parent-id][]'] input[type='text']").removeClass("error");
    },1000);

    if ((!$("input[name='data[parent-id][]']:last").val()) || (!$("[name='data[type]']").val()) || (!$("[name='data[template]']").val())) return false;

    var this_ = $(this);
    this_.prop("disabled",true);

    var h  = '<div class="exchange_container">';
    h += '<div id="process-header">Подождите, выполняется экспорт данных...</div>';
    h += '<div><img id="process-bar" src="/images/cms/admin/mac/process.gif" class="progress" /></div>';
    h += '</div>';
    h += '<div class="eip_buttons">';
    h += '<input id="stop_btn" type="button" value="Стоп" class="stop" style="margin-right:0px" />';
    h += '<div style="clear: both;"/>';
    h += '</div>';

    openDialog({
        title      : "Выполняется экспорт",
        text       : h,
        width: 410,
        stdButtons : false
    });
    $('#stop_btn').one("click", function() {location.reload(); });

    $.ajax({
        url : "/udata/prices_import/export_/.json",
        dataType : 'json',
        data : $(".export form").serialize(),
        success : function(data) {
            this_.prop("disabled",false);
            setTimeout(function(){closeDialog();},2000);
            location.href = data.file_name;
        }
    });
});
$("body").on("change", ".export select", function(){
    if (!$(this).val()) $(this).addClass("error"); else $(this).removeClass("error");
});

//Изменение ключевого идентификатора
$(".key").live("click",function(){
    var click_key_id=$(this).closest('td').attr("i_");
    var click_key_filedName = $(this).closest("td").find("select").find("option[selected = 'selected']").attr("value");
    var field_type = $(this).closest("td").attr('field_type');
    if ($(this).closest('td').hasClass("disabled") || (field_type == 'symlink') || (field_type == 'optioned') || (field_type == 'boolean')){
        openDialog({
            title: "Импорт",
            text: "Выбранный тип поля не может быть ключевым идентификатором<br/><br/>",
            width: 390,
            stdButtons : false
        });
    }
    else{
        if (($(".hide ul.unique li#i"+click_key_id).html()!="1") && (!$(this).hasClass('active')))
            openDialog({
                title: "Ключевой идентификатор",
                text: "<b style='color:red;'>Внимание!</b> Значения ячеек в текущем столбце не уникальные<br/><br/>",
                width: 390,
                stdButtons : false
            });

        $("#preloader").css("display","inline");
        $.ajax({
            url : "/udata/prices_import/change_key_id/"+file_name+"/"+click_key_filedName,
            dataType : 'html',
            success : function(data) {
                refresh_errors = true;
                get_file();
            }
        });
    }
});

//Изменение идентификатора типа по умолчанию
$("#default_type_id").live("change",function(){
    def_type_id = $(this).val();
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/change_default_type_id/"+file_name+"/"+def_type_id,
        dataType : 'html',
        success : function(data) {
            refresh_errors = true;
            get_file();
        }
    });
});

//Отменить все изменения 
$("#undo_all_changes").live("click",function(){
    openDialog({
        title: 'Отменение изменений',
        text: "Вы действительно хотите отменить все изменения?",
        width: 500,
        OKCallback: function () {
            $("#preloader").css("display","inline");
            $.ajax({
                url : "/udata/prices_import/undo_changes/"+file_name,
                dataType : 'html',
                success : function(data) {
                    refresh_errors = true;
                    get_file();
                }
            });
        },
        cancelCallback : function() {
        }
    });
    return false;
});

//Добавить колонку ===
$(".cmenuItem[item_id='0']").live("click",function(){
    //Определение номера выделенной колонки
    col_id = $("#content_table td.select:first").attr("i_");
    row_id = $("#content_table tr.select:first").attr("i_");
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/add_col/"+file_name+"/"+col_id,
        dataType : 'html',
        success : function(data) {
            get_file();
            col_id = parseInt(col_id) + 1;
        }
    });
});

//Удалить колонку
$(".cmenuItem[item_id='2']").live("click",function(){
    col_id = $("#content_table td.select:first").attr("i_");
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/del_col/"+file_name+"/"+col_id,
        dataType : 'html',
        success : function(data) {
            refresh_errors = true;
            get_file();
        }
    });
});

//Добавить строку
$(".cmenuItem[item_id='1']").live("click",function(){
    row_id = $("#content_table tr.select:first").attr("i_");
    $("#preloader").css("display","inline");
    $.ajax({
        url : "/udata/prices_import/add_row/"+file_name+"/"+row_id,
        dataType : 'html',
        success : function(data) {
            get_file(false,1,row_id);
        }
    });
});

//Кнопка esc
$(document).live("keydown", function(e) {
    if( e.keyCode === 27 ) {
        //Отмена редактирования ячейки
        $(".cell_edit").css("display","none");
        $("#cell_type_id_edit").css("display","none");
        $("#content_table tr").removeClass("select");
        $("#content_table td").removeClass("select");
        $(".edit_row_col").css('display', 'none');
        $("#context_menu").css("display","none");
        return false;
    }
});

//Изменение катгории по умолчанию
$("#change_default_parent").live("click",function(){
    symlink_type = 'default_category';
    jQuery.openPopupLayer({
        name	 : "Sitetree",
        title	 : "Выбор категории",
        width	 : 620,
        height	 : 335,
        url		 : "/styles/skins/mac/data/modules/prices_import/system/parents.html?hierarchy_types=catalog-category&lang_id="+lang_id+"&element=default_category&file_name="+file_name+"&"+Math.random()
    });
});


//Поиск
$("input[name='search']").live("change", function(){
    var value=$(this).val();
    if (value != ''){
        var search_uri = (search_mode) ? '/1' : '/0';
        search_mode = true;
        $("#preloader").css("display","inline");
        $.ajax({
            url : "/udata/prices_import/search/"+file_name+"/"+row_id+"/"+(col_id-1)+search_uri+"/.json",
            dataType : 'json',
            data: {search:value},
            success : function(data) {
                if (data.result == 'empty'){
                    openDialog({
                        stdButtons: false,
                        title      : 'Поиск',
                        text       : "По вашему запросу ничего не найдено.<br/>Попробуйте уточнить текст запроса<br/><br/>",
                        width      : 390,
                        OKCallback : function () {
                        }
                    });
                    $("#preloader").css("display","none");
                    return false;
                } else{
                    row_id = data.row;
                    col_id = data.col + 1;
                    shift = (Math.floor(row_id) - 7)>0 ? (Math.floor(row_id) - 7) : 0;
                }
                get_file(false, 3);
            }
        });
    }
});
$("#search_next").live("click", function(){
    if ($("input[name='search']").val() != ''){
        search_mode = true;
        $("input[name='search']").change();
    }
});
$("#search_prev").live("click", function(){
    if ($("input[name='search']").val() != ''){
        search_mode = false;
        $("input[name='search']").change();
    }
});
//Очитска отчета о выполненных загрузках в schedule
$("#del_schedule_item").live("click", function(){
    var obj_id = $(this).attr("data-obj-id");
    $.ajax({
        url : "/udata/prices_import/delReportSchedule/"+obj_id,
        dataType : 'html',
        success : function(getValue) {
            location.reload();
        }
    });
});

function get_file(compel, do_, var1, var2, var3, var4){
    if (file_touched == 1){
        var compel_ = false;
        if (compel) compel_ = '1'; else compel_ = '';
        $("#content_file_table").css({opacity:0.4});
        $("#preloader").css("display","inline");
        var urls_param = $("#set_params").serialize();
        if ($("#content_file_table").length > 0) horScrollContent = $("#content_file_table").scrollLeft();
        $.ajax({
            url : "/udata/prices_import/transformXML/?xslpath=/styles/skins/mac/data/modules/prices_import/system/list.xsl&require=prices_import/get_uploaded_file/"+file_name+"/"+shift+"/"+compel_+"/"+lang_id+"/?"+urls_param,
            dataType : 'html',
            async : true,
            success : function(data) {
                if (data == "error") {
                    openDialog({
                        title: "Ошибка загрузки файла",
                        text: "Невозможно определить формат прайса. Поддерживаемые форматы: xls, csv, xlsx.<br/><br/>",
                        width: 250,
                        OKCallback: function () {
                            location.reload();
                        },
                        cancelCallback : function() {
                            location.reload();
                        }
                    });
                    return false;
                };
                if (data.indexOf('memory_error') != -1){
                    openDialog({
                        title: "Ошибка загрузки файла",
                        text: "<span style='color:red; font-weight:bold;'>Ошибка!</span><br/><br/>Обработка прайса невозможна из-за нехватки памяти. Вы можете увеличить объем памяти в настройках модуля.<br/><br/>Обращаем Ваше внимание, что хостинг может блокировать увеличение объема памяти.<br/><br/>",
                        width: 250,
                        OKCallback: function () {
                            location.reload();
                        },
                        cancelCallback : function() {
                            location.reload();
                        }
                    });
                    return false;
                }
                $("#content_file").empty();
                $("#content_file").html(data);
                refresh_width();
                $("#content_file_table").scrollLeft(horScrollContent);
                $("#position_scroll").css("display","none");

                //Событие скролла в таблице ================================================================================
                var setTime = false;
                $("#content_table tr:not(#head_table)").mousewheel(function(event, delta, deltaX, deltaY){
                    return false;
                    var current_position = $("#vertical_scroll").scrollTop();
                    if (delta>0) {
                        $("#vertical_scroll").scrollTop($("#vertical_scroll").scrollTop() - 30);
                        if (setTime) clearTimeout(setTime);
                    } else {
                        $("#vertical_scroll").scrollTop($("#vertical_scroll").scrollTop() + 30);
                        if (setTime) clearTimeout(setTime);
                    }
                    if ($("#vertical_scroll").scrollTop() != current_position){
                        scroll_do = true;
                    }
                    if (scroll_do) {
                        setTime = setTimeout(function(){
                            $("#preloader").css("display","inline");
                            $("#vertical_scroll").mouseup();
                            scroll_do = false;
                        }, 300)
                    }
                });
                //==========================================================================================================

                $("#content_file_table").css({opacity:1.0});
                $("#preloader").css("display","none");

                switch (do_){
                    case 1:
                        //При добавлении новой строки сделать первую ячейку в нижней строке редактируемой
                        row_id = parseInt(var1) + 1;
                        $("tr#i"+row_id+" td#i1").dblclick();
                        break;
                    case 2:
                        //Выполняется сразу после загрузки файла ===========================================================
                        //Событие по правому скроллу ==================================================
                        $("#vertical_scroll").scroll(function(){
                            var height_scroll = $("#vertical_scroll #scroll").height();
                            var scroll_pos = $("#vertical_scroll").scrollTop();

                            shift = Math.floor(scroll_pos/30);
                            $("#position_scroll").css("top", ((scroll_pos/(height_scroll-509))*446));
                            $("#position_scroll").css("display","block");
                            if ($("#first_row_head").prop("checked")){
                                var end_pos = shift+per_page;
                                if (end_pos>=total_num_rows) end_pos = total_num_rows-1;
                                $("#position_scroll").html((shift+1)+"..."+(end_pos));
                            }else{
                                var end_pos = shift+per_page;
                                if (end_pos>=total_num_rows) end_pos = total_num_rows;
                                $("#position_scroll").html(shift+"..."+(end_pos-1));
                            }
                            if (hidePosScroll) clearTimeout(hidePosScroll);
                            hidePosScroll = setTimeout(function(){
                                $("#position_scroll").css("display","none");
                            },3000);
                        });

                        $("#vertical_scroll").mouseup(function(){
                            get_file();
                        });
                        // ============================================================================
                        break;
                    case 3:
                        //переводит горизонтальный скролл в требуемое положение, чтобы ячейка с col_id и row_id была видна
                        var top = $("#content_table tr#i"+row_id+" td#i"+col_id).position().top;
                        $("#vertical_scroll").scrollTop(shift*30);
                        var left = $("#content_table tr#i"+row_id+" td#i"+col_id).position().left;
                        $("#content_file_table").scrollLeft(left-250);
                        break;
                }

                content_initial();	//Установка идентификаторов, ключевого идент. и т.д., валидация
            }
        });
    }
}

// Установка идентификаторов, ключевого идент. и т.д.===
function content_initial(){
    //Инициализация редактирования ячеек
    init_cellEdit();

    //Получение параметров таблицы из аттрибутов #content_file_table
    total_num_rows = parseInt($("#content_file_table").attr("total_num_rows"));
    total_sel_num_rows = parseInt($("#content_file_table").attr("total_sel_num_rows"));
    per_page = parseInt($("#content_file_table").attr("per_page"));

    $("#vertical_scroll").css('display','');
    $("#vertical_scroll #scroll").css('height',Math.round(80 + total_num_rows*30)+'px');

    //Установка идентификаторов в таблице-заголовке=
    var total_col = $("#content_table tr:first td").length - 1;
    var key_field = $("#content_file_table").attr('key_field');
    $("#content_table tr:first td").each(function (i) {
        if (i!=0){
            field_name=$(this).attr("field_name");
            $("#head_table select#fields_id_"+i+" [value='"+field_name+"']").attr("selected","selected");
            if (field_name==""){
                $("#content_table td#i"+i).addClass("unactive_column");
                $("#head_table td#i"+i).addClass('disabled');
                return;
            }
            if (field_name == key_field){
                $("#head_table td#i"+i).find(".key").addClass("active");
                $("#content_table tr td#i"+i).addClass("selected_column");
            }
            //Удаление этих option из последующих select'ов
            /*for (var pos=1; pos<=total_col; pos++){
                if (i == pos) continue;
                $("#head_table select#fields_id_"+pos+" [value='"+field_name+"']").remove();
            }*/
        }
    });

    //Категория по умолчанию
    $.ajax({
        url : "/udata/prices_import/get_category_default/"+file_name+"/.json",
        dataType : 'json',
        success : function(getValue) {
            $("#change_default_parent").html(getValue.path);
        }
    });

    //Количество ошибок в таблице
    solveErrors();

    //Выделение строки и столбца, которые были выделены до перезагрузки страницы
    if ($("#content_table tr#i"+row_id).length > 0) {} else row_id = shift;
    if (!row_id) row_id = shift ? shift : 0;
    if (!col_id) col_id = 1;
    $("#content_table tr").removeClass("select");
    $("#content_table tr#i"+row_id).addClass("select");
    $("#content_table td").removeClass("select");
    $("#content_table td#i"+col_id).addClass("select");
    $(".edit_row_col").css('display', 'inline');

    //Вывод статистики
    $("#num_rows").text(total_num_rows);
    $("#num_selected_rows").text(total_sel_num_rows);

    $('#content_file_table').mousemove(function(e){
        var content_file_table = $('#content_file_table').offset();
        mouse_x = e.pageX - content_file_table.left;
        mouse_y = e.pageY - content_file_table.top;
    });

    //Отключение контекстного меню
    $('#content_file_table, #context_menu').on('contextmenu', function(e){
        e.stopPropagation();
    });

    $(".content_initial").css("display","block");
    $("#select_file_upload a").addClass("new_upload");
    $("#search_block").css("display","block");

    $("#new_template_name").keydown(function(event){
        if(event.keyCode == 13){
            return false;
        }
    });
}

//Инициализация редактирования ячеек
function init_cellEdit(){
    //Изменение ячейки
    $("#content_table td").dblclick(function(){
        row_id = $(this).closest("tr").attr("i_");
        col_id = $(this).attr("i_");
        var row_id_edit = row_id;
        var col_id_edit = col_id;
        var this_ = $(this);

        if ((col_id_edit!="0") && (this_.closest("tr").attr("id")!="head_table")) {
            $("#content_table tr").removeClass("select");
            $("#content_table tr#i"+row_id_edit).addClass("select");
            $("#content_table td").removeClass("select");
            $("#content_table td#i"+col_id_edit).addClass("select");
            $(".edit_row_col").css('display', 'inline');

            //Определение идентификатора поля
            field_type = this_.attr("field_type");
            field_name = this_.attr("field_name");

            switch (field_type){
                case "boolean":
                    disabled_active_element();
                    value = this_.find("div").text();
                    $("#preloader").css("display","inline");
                    $.ajax({
                        url : "/udata/prices_import/cell_change/"+file_name+"/"+row_id_edit+"/"+col_id_edit+"/"+field_type,
                        dataType : 'html',
                        data: {value:value},
                        success : function(getValue) {
                            refresh_errors = true;
                            get_file();
                        }
                    });
                    break


                case "symlink":
                    symlink_type = 'cell';
                    var hierarchy_types = '';
                    if (field_name == 'parent-id') {
                        symlink_type = 'parent-id';
                        hierarchy_types = 'hierarchy_types=catalog-category&';
                    }
                    disabled_active_element();
                    //Для ячеек
                    jQuery.openPopupLayer({
                        name	 : "Sitetree",
                        title	 : "Выбор категории",
                        width	 : 620,
                        height	 : 335,
                        url		 : "/styles/skins/mac/data/modules/prices_import/system/parents.html?"+hierarchy_types+"lang_id="+lang_id+"&element=cell&file_name="+file_name+"&row_id="+row_id+"&col_id="+col_id+"&"+Math.random()
                    });
                    break

                case "text":
                case "wysiwyg":
                    disabled_active_element();
                    pos_top_id = this_.position().top + $("#content_file_table").scrollTop();
                    //pos_left_id = this_.position().left + $("#content_file_table").scrollLeft();
                    pos_left_id = this_.position().left;
                    width_id = this_.width() - 5;
                    value = this_.find("div").text();

                    $("#cell_edit_textarea").css("display","");
                    $("#cell_edit_textarea").css("top",pos_top_id+"px");
                    $("#cell_edit_textarea").css("left",pos_left_id+"px");
                    $("#cell_edit_textarea").css("width",width_id+"px");
                    $("#cell_edit_textarea textarea#cell_value").focus();
                    $("#cell_edit_textarea textarea#cell_value").val(value);
                    $("#cell_edit_textarea textarea#cell_value").css("width",(width_id+10)+"px");

                    $("#cell_edit_textarea textarea#cell_value").focusout(function(){
                        $("#cell_edit_textarea").css("display","none");
                        value = $("#cell_edit_textarea textarea#cell_value").val();
                        $("#content_table tr#i"+row_id_edit+" td#i"+col_id_edit).addClass("edit_wait");
                        $("#content_table tr#i"+row_id_edit+" td#i"+col_id_edit).find("div").text(value);
                        $("#preloader").css("display","inline");
                        $.ajax({
                            url : "/udata/prices_import/cell_change/"+file_name+"/"+row_id_edit+"/"+col_id_edit+"/"+field_type,
                            dataType : 'html',
                            data: {value : value},
                            success : function(getValue) {
                                refresh_errors = true;
                                get_file();
                            }
                        });
                    });
                    break

                case "date":
                    disabled_active_element();
                    pos_top_id = this_.position().top + $("#content_file_table").scrollTop();
                    //pos_left_id = this_.position().left + $("#content_file_table").scrollLeft();
                    pos_left_id = this_.position().left;
                    width_id = this_.innerWidth();
                    value = this_.find("div").text();

                    $("#cell_edit_input").css("display","");
                    $("#cell_edit_input").css("top",pos_top_id+"px");
                    $("#cell_edit_input").css("left",pos_left_id+"px");
                    $("#cell_edit_input").css("width",width_id+"px");
                    $("#cell_edit_input input#cell_value").focus();
                    $("#cell_edit_input input#cell_value").val(value);
                    $("#cell_edit_input input#cell_value").css("width",(width_id-17)+"px");
                    $("#cell_edit_input").addClass("datePicker");
                    jQuery.datepicker.setDefaults(jQuery.extend({
                        dateFormat		: 'yy-mm-dd'}, jQuery.datepicker.regional["ru"]));
                    jQuery("div.datePicker").each(function(){
                        var input = jQuery("input", jQuery(this));
                        input.datepicker({dateFormat : 'yy-mm-dd',
                            onClose: function(dateText, inst) {
                                if(!/\d{1,2}:\d{1,2}(:\d{1,2})?$/.exec(dateText)) {
                                    dateText = dateText + " 00:00:00";
                                }
                                var tempDate = input.val(dateText);
                            }
                        });
                    });
                    $("#cell_edit_input input#cell_value").change(function(){
                        $("#cell_edit_input").css("display","none");
                        value = $("#cell_edit_input input#cell_value").val();
                        $("#content_table tr#i"+row_id_edit+" td#i"+col_id_edit).addClass("edit_wait");
                        $("#content_table tr#i"+row_id_edit+" td#i"+col_id_edit).find("div").text(value);
                        $("#preloader").css("display","inline");
                        $.ajax({
                            url : "/udata/prices_import/cell_change/"+file_name+"/"+row_id_edit+"/"+col_id_edit+"/"+field_type,
                            dataType : 'html',
                            data: {value : value},
                            success : function(getValue) {
                                refresh_errors = true;
                                get_file();
                            }
                        });
                    });
                    break

                case "relation":
                    disabled_active_element();
                    value = this_.find("div").text();
                    var multiple = (this_.attr("data-multiple") != undefined) ? this_.attr("data-multiple") : "";
                    multiple = (multiple == '1') ? ' multiple="multiple" ' : "";
                    var guide_id = (this_.attr("data-guide_id") != undefined) ? this_.attr("data-guide_id") : "";

                    if (guide_id){
                        openDialog({
                            title      : 'Выпадающий список',
                            text       : '' +
                                '<div id="cell_relation_edit">' +
                                '<label>Значение:</label>' +
                                '<input type="text" class="current_value" value="'+value+'"/>' +
                                '<label>Справочник:</label>' +
                                '<div class="relation" id="idp1" umi:type="'+guide_id+'">' +
                                '<select id="relationSelectidp1"'+multiple+'></select>' +
                                '<input type="text" id="relationInputidp1" class="search_input"/>' +
                                '</div>' +
                                '</div>',
                            width      : 600,
                            OKCallback : function () {
                                var val = $("#cell_relation_edit .current_value").val();
                                $.ajax({
                                    url : "/udata/prices_import/cell_change/"+file_name+"/"+row_id_edit+"/"+col_id_edit+"/"+field_type,
                                    dataType : 'html',
                                    data : {value: val},
                                    success : function(getValue) {
                                        refresh_errors = true;
                                        get_file();
                                    }
                                });
                            }
                        });
                        jQuery("div.relation").each(function() {
                            var e = jQuery(this);
                            new relationControl(e.attr("umi:type"), e.attr("id"), (e.attr("umi:empty") === "empty") );
                        });
                        var symb = $("#gen_content").attr("data-symb_separate_type_id");
                        $("#cell_relation_edit select").on("change", function(){
                            var val = $(this).val();
                            var str = '';
                            if (multiple){
                                val.forEach(function(entry) {
                                    str += $("#cell_relation_edit select option[value='"+entry+"']").html()+symb;
                                });
                            } else str = $("#cell_relation_edit select option[value='"+$(this).val()+"']").html()+symb;
                            str = str.substring(0, str.length - 1);
                            $("#cell_relation_edit .current_value").val(str);
                        });
                    }
                    break

                case "img_file":
                    disabled_active_element();
                    value = this_.find("div").text();
                    openDialog({
                        title      : 'Изображение',
                        text       : '' +
                            '<div id="cell_image_edit">' +
                            '<label>Значение:</label>' +
                            '<input type="text" class="current_value" value="'+value+'"/>' +
                            '<label>Выбрать:</label>' +
                            '<div class="field file" id="idp2" umi:field-type="img_file" umi:folder="./images/cms/data" umi:lang="ru" umi:filemanager="elfinder">' +
                            '<label for="symlinkInputidp2">' +
                            '<span id="fileControlContainer_idp2"></span>' +
                            '</label>' +
                            '</div>'+
                            '</div>',
                        width      : 600,
                        OKCallback : function () {
                            var val = $("#cell_image_edit .current_value").val();
                            $.ajax({
                                url : "/udata/prices_import/cell_change/"+file_name+"/"+row_id_edit+"/"+col_id_edit+"/"+field_type,
                                dataType : 'html',
                                data : {value: val},
                                success : function(getValue) {
                                    refresh_errors = true;
                                    get_file();
                                }
                            });
                        }
                    });
                    var defaultFolder = './images/cms/data/';
                    jQuery("div.file").each(function() {
                        var e = jQuery(this);
                        var options = {
                            inputName : e.attr("umi:input-name"),
                            folderHash : e.attr("umi:folder-hash"),
                            fileHash : e.attr("umi:file-hash"),
                            lang : e.attr("umi:lang"),
                            fm : e.attr("umi:filemanager")
                        };
                        switch( e.attr("umi:field-type") ) {
                            case "file"       :
                            case "swf_file"   :defaultFolder = './files';break;
                            case "video_file" :options.videosOnly = true;defaultFolder = './files/video';break;
                            case "img_file"   : {
                                options.imagesOnly = true;
                                switch( e.attr("umi:name") ) {
                                    case "header_pic" :defaultFolder = './images/cms/headers';break;
                                    case "menu_pic_a" :
                                    case "menu_pic_ua":defaultFolder = './images/cms/menu';break;
                                }
                            }
                        }
                        var c = new fileControl( e.attr("id"), options);
                        c.setFolder(defaultFolder, true);
                        c.setFolder(e.attr("umi:folder"));
                        c.add(e.attr("umi:file"), true);
                    });
                    $("#cell_image_edit select").on("DOMSubtreeModified", function(){
                        if (($(this).val() != defaultFolder) && ($(this).val() != null) && $(this).val()) $("#cell_image_edit .current_value").val($(this).val());
                    });
                    $("#cell_image_edit select").on("change", function(){
                        if (($(this).val() != defaultFolder) && ($(this).val() != null) && $(this).val()) $("#cell_image_edit .current_value").val($(this).val());
                    });
                    break;



                default:
                    //Изменение type-id
                    if (field_name == "type-id"){
                        disabled_active_element();
                        pos_top_id = this_.position().top + $("#content_file_table").scrollTop();
                        //pos_left_id = this_.position().left + $("#content_file_table").scrollLeft();
                        pos_left_id = this_.position().left;
                        width_id = this_.width();
                        $("#cell_type_id_edit").css("display","");
                        $("#cell_type_id_edit").css("top",pos_top_id+"px");
                        $("#cell_type_id_edit").css("left",pos_left_id+"px");
                        $("#cell_type_id_edit").css("width",width_id+"px");
                        $("#cell_type_id_edit select#cell_type_id").css("width",(width_id+10)+"px");
                        $("#cell_type_id :first").attr("selected", "selected");
                        $("#cell_type_id").change(function(){
                            type_id = $("#cell_type_id").val();
                            $("#preloader").css("display","inline");
                            $.ajax({
                                url : "/udata/prices_import/cell_change/"+file_name+"/"+row_id_edit+"/"+col_id_edit+"/"+field_type,
                                dataType : 'html',
                                data : {value: type_id},
                                success : function(getValue) {
                                    refresh_errors = true;
                                    get_file();
                                }
                            });
                        })
                    } else {
                        disabled_active_element();
                        pos_top_id = this_.position().top + $("#content_file_table").scrollTop();
                        //pos_left_id = this_.position().left + $("#content_file_table").scrollLeft();
                        pos_left_id = this_.position().left;
                        width_id = this_.width() - 5;
                        value = this_.find("div").text();

                        $("#cell_edit_input").css("display","");
                        $("#cell_edit_input").css("top",pos_top_id+"px");
                        $("#cell_edit_input").css("left",pos_left_id+"px");
                        $("#cell_edit_input").css("width",width_id+"px");
                        $("#cell_edit_input input#cell_value").focus();
                        $("#cell_edit_input input#cell_value").val(value);
                        $("#cell_edit_input input#cell_value").css("width",(width_id+10)+"px");

                        $("#cell_edit_input input#cell_value").focusout(function(){
                            $("#cell_edit_input").css("display","none");
                            value = $("#cell_edit_input input#cell_value").val();
                            $("#content_table tr#i"+row_id_edit+" td#i"+col_id_edit).addClass("edit_wait");
                            $("#content_table tr#i"+row_id_edit+" td#i"+col_id_edit).find("div").text(value);
                            $("#preloader").css("display","inline");
                            $.ajax({
                                url : "/udata/prices_import/cell_change/"+file_name+"/"+row_id_edit+"/"+col_id_edit+"/"+field_type,
                                dataType : 'html',
                                data: {value : value},
                                success : function(getValue) {
                                    refresh_errors = true;
                                    get_file();
                                }
                            });
                        });
                        $("#cell_edit_input input#cell_value").keypress(function(e){
                            if(e.keyCode==13){
                                $("#cell_edit_input").css("display","none");
                                value = $("#cell_edit_input input#cell_value").val();
                                $("#content_table tr#i"+row_id_edit+" td#i"+col_id_edit).addClass("edit_wait");
                                $("#content_table tr#i"+row_id_edit+" td#i"+col_id_edit).find("div").text(value);
                                $("#preloader").css("display","inline");
                                $.ajax({
                                    url : "/udata/prices_import/cell_change/"+file_name+"/"+row_id_edit+"/"+col_id_edit+"/"+field_type,
                                    dataType : 'html',
                                    data: {value : value},
                                    success : function(getValue) {
                                        refresh_errors = true;
                                        get_file();
                                    }
                                });
                            }
                        });
                    }
            }
        }
    });

}

//Постепенный подсчет количества ошибок
function solveErrors(shift){
    if (refresh_errors === true) {
        refresh_errors = false;
        $("#num_errors_table").html('0');
        shift = 0;
    }

    if ((refresh_errors === false) && (shift >= 0))
    $.ajax({
        url : "/udata/prices_import/validation/"+file_name+"/"+shift+"/.json",
        dataType : 'json',
        success : function(data) {
            if (refresh_errors != -1) {
                var current_num = parseInt($("#num_errors_table").html());
                if(isNaN(current_num)) {
                    current_num = 0;
                }
                var err = (data.errors && data.errors>0) ? data.errors : 0;
                $("#num_errors_table").html(parseInt(current_num + err));
                if ((data.next != '0') && (data.next)) solveErrors(data.next); else refresh_errors = -1;
            }
        }
    });
}

//Отменить активность элементов редактирования =
function disabled_active_element(){
    $("#cell_type_id_edit").css("display","none");
    $(".cell_edit").css("display","none");
    $(".cell_edit").removeClass("datePicker");
}

//Обновить ширину таблицы
function refresh_width(){
    $("#content_file_table").width($("#gen_content").width() - $("#vertical_scroll").width() - $("#td_set_params_div").width() - 23);
}

//Изменение данных в блоке "Шаблоны" ==
function change_template_block(){
    var new_template_name = $("#templates #new_template_name").val();
    var select_templates = $("#templates #select_templates").val();
    var update = false;
    $('#select_templates option').each(function(){
        if (this.text == new_template_name) {update = true; return false;}

    });
    if (update) $('#save_template').val("Обновить"); else $('#save_template').val("Добавить");
}

//Выполнить операцию над выделенными элементами
var selected_rows = new Array();	//Массив выделенных строк
var step=1;
var time_step;
var process = false;		//Если true идет процесс, false - остановлен
function do_selected(func_name){
    selected_rows = [];
    var progress=0;

    for (var i=0; i<total_sel_num_rows; i++) selected_rows.push(i);

    process = true;

    var h  = '<div class="exchange_container">';
    h += '<div id="process-header">Подождите, выполняется импорт данных...</div>';
    h += '<div><img id="process-bar" src="/images/cms/admin/mac/process.gif" class="progress" /></div>';
    h += '<div class="status">Всего выделено:<span id="total_selected">0</span></div>';
    h += '<div class="status">Всего обработано:<span id="total_counter">0</span></div>';
    h += '<div class="status">- создано:<span id="created_counter">0</span></div>';
    h += '<div class="status">- обновлено:<span id="updated_counter">0</span></div>';
    h += '</div>';
    h += '<div class="eip_buttons">';
    h += '<input id="ok_btn" type="button" value="Готово" class="ok" style="margin:0;" disabled="disabled" onclick="closeDialog();" />';
    h += '<input id="stop_btn" type="button" value="Остановить" class="stop" />';
    h += '<div style="clear: both;"/>';
    h += '</div>';
    openDialog({
        stdButtons: false,
        title      : 'Импорт',
        text       : h,
        width      : 390,
        OKCallback : function () {
        }
    });
    do_ajax(func_name);
}

function do_ajax(func_name){
    //Если прервано пользователем
    if (!process) {
        $("#process-header").text("Прервано пользователем");
        $("#process-header").addClass("error");
        $("#process-bar").css("visibility","hidden");
        $("#ok_btn").prop( "disabled", false );
        $("#stop_btn").prop( "disabled", true );
        refresh_errors = true;
        get_file();
        return false;
    }

    var string_url="";
    for (var i=0; i<step; i++){
        if (i in selected_rows) string_url=string_url+selected_rows[i]+",";
        else break;
    }

    $("#content_file_table").css({opacity:0.4});
    var startStep = new Date().getTime();
    $.ajax({
        url : "/udata/prices_import/"+func_name+"/"+file_name+"/.json",
        dataType : 'json',
        data : {id:string_url},
        success : function(data) {
            if (!('total' in data) || !('created' in data) || !('updated' in data)) {
                console.log("error1, repeat");
                do_ajax(func_name);
            }

            var endStep = new Date().getTime();
            time_step = endStep - startStep;

            $("#total_selected").text((data.total != undefined) ? data.total : "");
            $("#created_counter").text((data.created != undefined) ? data.created : "");
            $("#updated_counter").text((data.updated != undefined) ? data.updated : "");
            if ((data.created != undefined) && (data.updated != undefined))
                $("#total_counter").text(parseInt(data.created) + parseInt(data.updated));

            selected_rows.splice(0,step);
            if (selected_rows.length>0){
                //Регулирование количества элементов в одном шаге
                time_step_koef = time_step / step;
                time_step_koef = time_step_koef ? time_step_koef : 3000;
                step = Math.floor(3000 / time_step_koef);
                step = (step < 1) ? 1 : step;
                step = (step > 50) ? 50 : step;

                do_ajax(func_name);
            }
            else{
                $("#process-bar").css("visibility","hidden");
                $("#ok_btn").prop( "disabled", false );
                $("#stop_btn").prop( "disabled", true );
                $("#process-header").removeClass("error");
                $("#process-header").text("Загрузка данных завершена");
                refresh_errors = true;
                $.ajax({
                    url : "/udata/prices_import/end_import/"+file_name,
                    dataType : 'html',
                    success : function(data) {
                        get_file();
                        if(window.session) {
                            window.session.stopAutoActions();
                        }
                    }
                });
            }
            $.ajax({
                url : "/udata/prices_import/log/"+file_name+"/success",
                dataType : 'html',
                data : {id:string_url},
                success : function(data) {}
            });
        },
        error: function() {
            setTimeout(function(){
                $.ajax({
                    url : "/udata/prices_import/log/"+file_name+"/error_1",
                    dataType : 'html',
                    data : {id:string_url},
                    success : function(data) {}
                });
                do_ajax(func_name)
            }, 3000);
        }
    });
}

function listener(event){
    $("#preloader").css("display","inline");
    if(((symlink_type=="default_category") || (symlink_type == "parent-id")) && (event.data != 'del')) $(".popupClose").trigger("click");
    get_file();
}

if (window.addEventListener){
    window.addEventListener("message", listener,false);
} else {
    window.attachEvent("onmessage", listener);
}

function parse_xml(value, elem){
    xmlDoc = $.parseXML( value );
    $xml = $( xmlDoc );
    $title = $xml.find( elem );
    return $title.text();
}