<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xlink="http://www.w3.org/TR/xlink">

    <xsl:template match="/result[@method = 'add']/data/object" mode="form-modify">
        <xsl:variable name="getTemplates" select="document('udata://prices_import/getListTemplates/')//item" />

        <link rel="stylesheet" href="/styles/skins/mac/data/modules/prices_import/css/styles.css"/>
        <div class="panel edit_schedule">
            <div class="tabs">
                <a href="/admin/prices_import/import/" class="header first" style="margin-left:9px;">
                    <span class="c">&header-prices_import-import;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/schedules/" class="header act">
                    <span class="c">&header-prices_import-schedules;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/export/" class="header last">
                    <span class="c">&header-prices_import-export;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
            </div>
            <div class="content">
                <xsl:choose>
                    <xsl:when test="count($getTemplates) &gt; 0">
                        <form method="post" action="do/" enctype="multipart/form-data">
                            <input type="hidden" name="referer" value="{/result/@referer-uri}"/>
                            <input type="hidden" name="domain" value="{$domain-floated}"/>
                            <xsl:apply-templates mode="form-modify" />
                        </form>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="warning">Для того, чтобы создать отложенную загрузку, Вам необходимо создать хотя бы один шаблон импорта.</div>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="/result[@method = 'edit']/data/object" mode="form-modify">
        <xsl:variable name="getTemplates" select="document('udata://prices_import/getListTemplates/')//item" />

        <link rel="stylesheet" href="/styles/skins/mac/data/modules/prices_import/css/styles.css"/>
        <div class="panel edit_schedule">
            <div class="tabs">
                <a href="/admin/prices_import/import/" class="header first" style="margin-left:9px;">
                    <span class="c">&header-prices_import-import;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/schedules/" class="header act">
                    <span class="c">&header-prices_import-schedules;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/export/" class="header last">
                    <span class="c">&header-prices_import-export;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
            </div>
            <div class="content">
                <xsl:choose>
                    <xsl:when test="count($getTemplates) &gt; 0">
                        <form method="post" action="do/" enctype="multipart/form-data">
                            <input type="hidden" name="referer" value="{/result/@referer-uri}"/>
                            <input type="hidden" name="domain" value="{$domain-floated}"/>
                            <xsl:apply-templates mode="form-modify">
                                <xsl:with-param name="object_id" select="/result/data/object/@id" />
                            </xsl:apply-templates>
                        </form>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="warning">Для того, чтобы экспортировать данные, Вам необходимо создать хотя бы один шаблон импорта.</div>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </div>
        <script type="text/javascript">	$.getScript("/styles/skins/mac/data/modules/prices_import/js/prices_import.js");</script>
    </xsl:template>

    <xsl:template match="/result[@module = 'prices_import' and @method = 'add']/data//group[@name = 'settings']" mode="form-modify">
        <xsl:param name="show-name"><xsl:text>1</xsl:text></xsl:param>
        <xsl:param name="show-type"><xsl:text>1</xsl:text></xsl:param>

        <div class="panel properties-group" name="g_{@name}">
            <div class="header">
                <span class="c">
                    <xsl:value-of select="@title" />
                </span>
                <div class="l" /><div class="r" />
            </div>

            <div class="content">

                <xsl:apply-templates select="." mode="form-modify-group-fields">
                    <xsl:with-param name="show-name" select="$show-name" />
                    <xsl:with-param name="show-type" select="$show-type" />
                </xsl:apply-templates>

                <div class="buttons">
                    <div>
                        <input type="submit" value="Сохранить" name="save-mode" /><span class="l"></span><span class="r"></span>
                    </div>
                    <div>
                        <input type="submit" value="Добавить и выйти" name="save-mode" /><span class="l"></span><span class="r"></span>
                    </div>
                    <div>
                        <input type="button" value="Отменить" onclick="javascript: window.location = 'http://import.umicms.pro/admin/prices_import/schedules/';" /><span class="l"></span><span class="r"></span>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="/result[@module = 'prices_import' and @method = 'add']/data//group[@name = 'reports']" mode="form-modify" />
    <xsl:template match="/result[@module = 'prices_import' and @method = 'add']/data//group[@name = 'system']" mode="form-modify" />


    <xsl:template match="/result[@module = 'prices_import' and @method = 'edit']/data//group[@name = 'settings']" mode="form-modify">
        <xsl:param name="show-name"><xsl:text>1</xsl:text></xsl:param>
        <xsl:param name="show-type"><xsl:text>1</xsl:text></xsl:param>

        <div class="panel properties-group" name="g_{@name}">
            <div class="header">
                <span class="c">
                    <xsl:value-of select="@title" />
                </span>
                <div class="l" /><div class="r" />
            </div>

            <div class="content">

                <xsl:apply-templates select="." mode="form-modify-group-fields">
                    <xsl:with-param name="show-name" select="$show-name" />
                    <xsl:with-param name="show-type" select="$show-type" />
                </xsl:apply-templates>

                <div class="buttons">
                    <div>
                        <input type="submit" value="Сохранить" name="save-mode" /><span class="l"></span><span class="r"></span>
                    </div>
                    <div>
                        <input type="submit" value="Сохранить и выйти" name="save-mode" /><span class="l"></span><span class="r"></span>
                    </div>
                    <div>
                        <input type="button" value="Отменить" onclick="javascript: window.location = 'http://import.umicms.pro/admin/prices_import/schedules/';" /><span class="l"></span><span class="r"></span>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="/result[@module = 'prices_import' and @method = 'edit']/data//group[@name = 'reports']" mode="form-modify">
        <xsl:param name="show-name"><xsl:text>1</xsl:text></xsl:param>
        <xsl:param name="show-type"><xsl:text>1</xsl:text></xsl:param>
        <xsl:param name="object_id" />

        <div class="panel properties-group" name="g_{@name}">
            <div class="header">
                <span class="c">
                    <xsl:value-of select="@title" />
                </span>
                <div class="l" /><div class="r" />
            </div>

            <div class="content">

                <xsl:apply-templates select="." mode="form-modify-group-fields">
                    <xsl:with-param name="show-name" select="$show-name" />
                    <xsl:with-param name="show-type" select="$show-type" />
                </xsl:apply-templates>

                <div class="buttons">
                    <div>
                        <input type="button" id="del_schedule_item" value="Очистить" data-obj-id="{$object_id}" /><span class="l"></span><span class="r"></span>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="/result[@module = 'prices_import' and @method = 'edit']/data//group[@name = 'system']" mode="form-modify" />

    <xsl:template match="field[@type = 'string' and @name='template']" mode="form-modify">
        <div class="field">
            <label for="{generate-id()}">
                <span class="label">
                    <acronym>
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span>
                    <xsl:variable name="getTemplates" select="document('udata://prices_import/getListTemplates/')//item" />
                    <select name="{@input_name}" id="{generate-id()}">
                        <xsl:apply-templates select="$getTemplates" mode="getListTemplates">
                            <xsl:with-param name="selected" select="." />
                        </xsl:apply-templates>
                    </select>
                </span>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="field[@type = 'wysiwyg' and @name='report']" mode="form-modify">
        <div class="field text">
            <label for="{generate-id()}">
                <span class="label">
                    <acronym>
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <div class="report">
                    <xsl:value-of select="." disable-output-escaping="yes" />
                </div>
                <!--<textarea name="{@input_name}" id="{generate-id()}">
                    <xsl:apply-templates select="." mode="required_attr">
                        <xsl:with-param name="old_class" select="@type" />
                    </xsl:apply-templates>
                    <xsl:value-of select="." />
                </textarea>-->
            </label>
        </div>
    </xsl:template>

    <xsl:template match="item" mode="getListTemplates">
        <xsl:param name="selected" />
        <option value="{@name}">
            <xsl:if test="@name = $selected">
                <xsl:attribute name="selected">selected</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="@name" />
        </option>
    </xsl:template>

</xsl:stylesheet>