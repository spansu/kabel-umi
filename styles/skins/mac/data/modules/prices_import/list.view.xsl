<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/prices_import" [
        <!ENTITY sys-module        'prices_import'>
        <!ENTITY sys-method-list    'lists'>
        ]>
<xsl:stylesheet version="1.0"   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi">
    <xsl:template match="result[@method = 'import']">
        <xsl:variable name="lang-id" select="@lang-id" />
        <xsl:variable name="regedit" select="document('udata://prices_import/getRegEdit')" />
        <link rel="stylesheet" href="/styles/skins/mac/data/modules/prices_import/css/styles.css"/>
        <div class="panel">
            <div class="tabs">
                <!--<a href="/admin/prices_import/import/" class="header act first" style="margin-left:9px;">
                    <span class="c">&header-prices_import-import;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/schedules/" class="header next">
                    <span class="c">&header-prices_import-schedules;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/export/" class="header last">
                    <span class="c">&header-prices_import-export;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>-->
                <a href="/admin/prices_import/import/" class="header act first last" style="margin-left:9px;">
                    <span class="c">&header-prices_import-import;</span>
                    <span class="l"></span>
                    <span class="r"></span>
                </a>
            </div>
            <div class="content">
                <table width="100%">
                    <tr>
                        <td valign="top" width="300">
                            <form id="upload" action="/udata/prices_import/upload" enctype="multipart/form-data" method="post">
                                <div id="select_file_upload" xmlns:umi="http://www.umi-cms.ru/TR/umi" class="imgButtonWrapper">
                                    <a href="">Выберите файл для загрузки</a>
                                    <div>Поддерживаемые форматы: xls, xlsx, csv</div>
                                    <input id="upload_file" type="file" name="file_name" />
                                </div>

                                <div id="upload_info">
                                    <img src="/styles/skins/mac/data/modules/prices_import/img/preloader.gif" />
                                    <span id="upload_progress_bar"></span>
                                </div>
                            </form>
                        </td>
                        <td valign="top" width="170">
                            <a href="#" id="selected_import" data-type="current" class="content_initial do_import">Выполнить импорт</a>
                        </td>
                        <td valign="top" width="140">
                            <a href="#" id="undo_all_changes" class="content_initial">Отменить все</a>
                        </td>
                        <td valign="top" align="right">
                            <div id="search_block">
                                <input type="text" name="search" />
                                <input class="search_button" id="search_prev" type="button" value="&lt;" /> 
                                <input class="search_button" id="search_next" type="button" value="&gt;" />
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="clear:both;"></div>

                <div id="gen_content" lang-id="{$lang-id}"
                     data-def_parentId="{$regedit//def_parentId}"
                     data-def_typeId="{$regedit//def_typeId}"
                     data-def_isActive="{$regedit//def_isActive}"
                     data-encoding="{$regedit//encoding}"
                     data-symb_separate_type_id="{$regedit//symb_separate_type_id}"
                     data-symb_separate_csv="{$regedit//symb_separate_csv}"
                     data-memory_limit="{$regedit//memory_limit}"
                     data-stores_field="{$regedit//stores_field}"
                >
                    <table width="100%">
                        <tr>
                            <td valign="top" width="100%">
                                <div id="content_file">
                                    <img id="preloader" src="/styles/skins/mac/data/modules/prices_import/img/preloader_big.gif" />
                                </div>
                            </td>
                            <td valign="top" width="15" style="position:relative;">
                                <div id="vertical_scroll" style="display:none;">
                                    <div id="scroll"></div>
                                </div>
                                <div id="position_scroll"></div>
                            </td>
                            <td valign="top" id="td_set_params_div"></td>
                        </tr>
                    </table>
                </div>
                <small>
                    <a href="http://clean-code.ru/" target="_blank">
                        <img src="/styles/skins/mac/data/modules/prices_import/img/logo.png" alt="Создание сайтов, модулей, расширений для UMI.CMS" align="absmiddle" />&nbsp;&nbsp;Создание сайтов, модулей, расширений для&nbsp;UMI.CMS</a>
                </small>
            </div>
        </div>

        <script type="text/javascript">	$.getScript("/styles/skins/mac/data/modules/prices_import/js/prices_import.js");</script>
    </xsl:template>


    <xsl:template match="result[@method = 'schedules']/data">
        <link rel="stylesheet" href="/styles/skins/mac/data/modules/prices_import/css/styles.css"/>
        <xsl:variable name="getTypeIdSchedule" select="document('udata://prices_import/getTypeIdSchedule')/udata" />
        <div class="panel">
            <div class="tabs">
                <a href="/admin/prices_import/import/" class="header first" style="margin-left:9px;">
                    <span class="c">&header-prices_import-import;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/schedules/" class="header act">
                    <span class="c">&header-prices_import-schedules;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/export/" class="header last">
                    <span class="c">&header-prices_import-export;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
            </div>
            <div class="content">
                <div class="imgButtonWrapper">
                    <a id="addSchedules" href="/admin/prices_import/add/?type-id={$getTypeIdSchedule}">Добавить отложенную загрузку</a>
                </div>
                <xsl:call-template name="ui-smc-table">
                    <xsl:with-param name="control-params" select="$method" />
                    <xsl:with-param name="content-type">objects</xsl:with-param>
                    <xsl:with-param name="search-show">0</xsl:with-param>
                    <xsl:with-param name="enable-objects-activity">1</xsl:with-param>
                    <xsl:with-param name="js-ignore-props-edit">['name','status','last_date','template','price_list','price_list_url','is_active','date','queue','rows_total','rows_processed','create','update','last_active','period','from','to','report','price_list_id','file_date']</xsl:with-param>
                    <xsl:with-param name="disable-csv-buttons">1</xsl:with-param>
                    <xsl:with-param name="hide-csv-import-button">1</xsl:with-param>
                    <xsl:with-param name="js-required-props-menu">['status','price_list','is_active','date','queue','rows_total','rows_processed','create','update','last_active','from','to','report','price_list_id','file_date']</xsl:with-param>
                </xsl:call-template>
                <small>
                    <a href="http://clean-code.ru/" target="_blank">
                        <img src="/styles/skins/mac/data/modules/prices_import/img/logo.png" alt="Создание сайтов, модулей, расширений для UMI.CMS" align="absmiddle" />&nbsp;&nbsp;Создание сайтов, модулей, расширений для&nbsp;UMI.CMS</a>
                </small>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="result[@method = 'export']/data">
        <link rel="stylesheet" href="/styles/skins/mac/data/modules/prices_import/css/styles.css"/>
        <div class="panel export" lang_id="{/result/@lang-id}">
            <div class="tabs">
                <a href="/admin/prices_import/import/" class="header first" style="margin-left:9px;">
                    <span class="c">&header-prices_import-import;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/schedules/" class="header prev">
                    <span class="c">&header-prices_import-schedules;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
                <a href="/admin/prices_import/export/" class="header act last">
                    <span class="c">&header-prices_import-export;</span>
                    <span class="l"/>
                    <span class="r"/>
                </a>
            </div>
            <div class="content">
                <xsl:variable name="getTemplates" select="document('udata://prices_import/getListTemplates/')//item" />
                <xsl:choose>
                    <xsl:when test="count($getTemplates) &gt; 0">
                        <div class="panel properties-group">
                            <div class="header">
                                <span class="c">Настройки экспорта</span>
                                <div class="l"></div>
                                <div class="r"></div>
                            </div>
                            <div class="content">
                                <form>
                                    <div class="field symlink" id="idp3" name="data[parent-id][]" style="margin-bottom:20px;">
                                        <label for="symlinkInputidp3">
                                            <span class="label">
                                                <acronym>Категория</acronym>
                                            </span>
                                            <span id="symlinkInputidp3">
                                                <ul></ul>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="field relation" id="idp4" umi:type="141" style="height:60px;" umi:empty="empty">
                                        <label for="relationSelectidp4">
                                            <span class="label">
                                                <acronym>Шаблон для экспорта</acronym>
                                            </span>
                                            <span>
                                                <select name="data[template]" id="relationSelectidp4">
                                                    <xsl:apply-templates select="$getTemplates" mode="getListTemplates" />
                                                </select>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="field relation" id="idp5" umi:type="142" style="height:60px;" umi:empty="empty">
                                        <label for="relationSelectidp5">
                                            <span class="label">
                                                <acronym>Формат выгружаемого файла</acronym>
                                            </span>
                                            <span>
                                                <select name="data[type]" id="relationSelectidp5">
                                                    <option value="csv">csv</option>
                                                    <option value="xls">xls</option>
                                                    <option value="xlsx">xlsx</option>
                                                </select>
                                            </span>
                                        </label>
                                    </div>
                                    <div style="display:inline-block;">
                                        <div class="field" style="min-height:auto; height:20px;">
                                            <label class="inline" for="idp6">
                                                <span class="label">
                                                    <input type="checkbox" name="data[unactive]" id="idp6" class="checkbox" checked="checked"/>
                                                    <acronym>Не выгружать неактивные товары</acronym>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="field" style="min-height:auto; height:20px;">
                                            <label class="inline" for="idp7">
                                                <span class="label">
                                                    <input type="checkbox" name="data[vcopies]" id="idp7" class="checkbox" checked="checked" />
                                                    <acronym>Не выгружать виртуальные копии</acronym>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="buttons">
                                        <div>
                                            <input  id="export" type="button" value="Выполнить экспорт" name="save-mode"/>
                                            <span class="l"></span>
                                            <span class="r"></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="warning">Для того, чтобы экспортировать данные, Вам необходимо создать хотя бы один шаблон импорта.</div>
                    </xsl:otherwise>
                </xsl:choose>
                <small>
                    <a href="http://clean-code.ru/" target="_blank">
                        <img src="/styles/skins/mac/data/modules/prices_import/img/logo.png" alt="Создание сайтов, модулей, расширений для UMI.CMS" align="absmiddle" />&nbsp;&nbsp;Создание сайтов, модулей, расширений для&nbsp;UMI.CMS</a>
                </small>
            </div>
        </div>
        <script type="text/javascript">	$.getScript("/styles/skins/mac/data/modules/prices_import/js/prices_import.js");</script>
    </xsl:template>

    <xsl:template match="item" mode="getListTemplates">
        <option value="{@name}">
            <xsl:value-of select="@name" />
        </option>
    </xsl:template>

</xsl:stylesheet>