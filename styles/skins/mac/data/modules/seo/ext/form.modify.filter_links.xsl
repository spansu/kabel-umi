<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:php="http://php.net/xsl"
	>

	<xsl:template match="/result[@module = 'seo' and @method = 'filter_link_edit']/data">
		<link rel="stylesheet" href="/styles/skins/mac/data/modules/seo/css/ext_filter_links/style.css" />
		<script type="text/javascript">	$.getScript("/styles/skins/mac/data/modules/seo/js/ext_filter_links/script.js");</script>
		<form method="post" class="form_modify" action="do/" enctype="multipart/form-data">
			<input type="hidden" name="referer" value="{/result/@referer-uri}"/>
			<input type="hidden" name="domain" value="{$domain-floated}"/>
			<script type="text/javascript">
				var treeLink = function(key, value){
					var settings = SettingsStore.getInstance();
					
					return settings.set(key, value, 'expanded');
				}
			</script>
			<xsl:apply-templates mode="form-modify-filters" />
		</form>
		<script type="text/javascript">
			var method = '<xsl:value-of select="/result/@method" />';
			<![CDATA[
				jQuery('form.form_modify').submit(function(e) {
					var val = true;
					var str = '<div id="errorList"><p class="error"><strong>' + getLabel('js-label-errors-found') + ':</strong></p><ol class="error">';
					var isTextArea = false;
					var editor = null;
					var editorValue = '';
					var isEmptyValue = false;

					jQuery('.required', this).each(function(){
						isTextArea = this.tagName && this.tagName.toLowerCase() === 'textarea';
						if (isTextArea) {
							editor = tinyMCE.get(this.id);

							if (editor && typeof(editor.getContent) === 'function') {
								editorValue = editor.getContent({format: 'text'});
								isEmptyValue = (typeof(editorValue) === 'string' && editorValue.length === 0) ||
											   (editorValue.length === 1 && editorValue.charCodeAt(0) === "\n".charCodeAt(0));
							}
						} else {
							isEmptyValue = (this.value === '');
						}

						if (isEmptyValue) {
							if (val === true) {
								val = false;
							}
							var innerText = jQuery('label[for=' + this.id + '] acronym').text();
							str += '<li>' + getLabel('js-error-required-field') + ' "' + innerText + '".' + '</li>';
						}
					});
					str += '</ol></div>';
					if (val === false) {
						if (jQuery("div").is("#errorList") === false) {
							jQuery('div#page').before(str);
						} else {
							jQuery("#errorList").remove();
							jQuery('div#page').before(str);
						}
						jQuery('body').scrollTop(0);
					}
					return val;
				});
			]]>
		</script>
	</xsl:template>
	
	<xsl:template match="object" mode="form-modify-filters">
		<xsl:apply-templates select="properties/group" mode="form-modify">
			<xsl:with-param name="show-type" select="'0'" />
		</xsl:apply-templates>
		<small>
			<a href="http://clean-code.ru/" target="_blank">
				<img src="/styles/skins/mac/data/modules/seo/img/ext_filter_links/logo.png" align="absmiddle" />
				<xsl:text>&nbsp;&nbsp;Создание сайтов, модулей, расширений для UMI.CMS</xsl:text>
			</a>
		</small>
	</xsl:template>
	
	<xsl:template match="/result[@module = 'seo' and @method = 'filter_link_edit']//field[@name = 'domain_id']" mode="form-modify" priority="1" />
	
	<xsl:template match="/result[@module = 'seo' and @method = 'filter_link_edit']//field[@name = 'priority']" mode="form-modify" priority="1">
		<div class="field priority hide">
			<xsl:value-of select=".//property[@name='is_sitemap']/value" />
			<label for="{generate-id()}">
				<span class="label">
					<acronym>
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</span>
				<span>
					<input type="text" name="{@input_name}" value="{.}" id="{generate-id()}">
						<xsl:apply-templates select="." mode="required_attr">
							<xsl:with-param name="old_class" select="@type" />
						</xsl:apply-templates>
					</input>
				</span>
			</label>
		</div>
	</xsl:template>
	
	<xsl:template match="/result[@module = 'seo' and @method = 'filter_link_edit']//field[@name = 'is_sitemap']" mode="form-modify" priority="1">
		<div class="field is-sitemap">
			<label class="inline" for="{generate-id()}">
				<span class="label">
					<input type="hidden" name="{@input_name}" value="0" />
					<input type="checkbox" name="{@input_name}" value="1" id="{generate-id()}">
						<xsl:apply-templates select="." mode="required_attr">
							<xsl:with-param name="old_class" select="'checkbox'" />
						</xsl:apply-templates>
						<xsl:if test=". = '1'">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<acronym>
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</span>
			</label>
		</div>
	</xsl:template>
	
	<xsl:template match="field[@name='filters']" mode="form-modify" priority="1">
		<div class="field">
			<label for="{generate-id()}">
				<span class="label">
					<acronym>
						<xsl:apply-templates select="." mode="sys-tips" />
						<xsl:value-of select="@title" />
					</acronym>
					<xsl:apply-templates select="." mode="required_text" />
				</span>
				<textarea name="{@input_name}" id="{generate-id()}">
					<xsl:apply-templates select="." mode="required_attr">
						<xsl:with-param name="old_class" select="@type" />
					</xsl:apply-templates>
					<xsl:value-of select="." />
				</textarea>
			</label>
		</div>
	</xsl:template>
	
</xsl:stylesheet>