<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/result[@method = 'seo_pager']" priority="1">
		<form method="post" action="do/" enctype="multipart/form-data">
			<xsl:apply-templates select="." mode="settings.modify" />
		</form>
		<small>
			<a href="http://clean-code.ru/" target="_blank">
				<img src="/styles/skins/mac/data/modules/seo/img/ext_seo_pager/logo.png" align="absmiddle" />
				<xsl:text>&nbsp;&nbsp;Создание сайтов, модулей, расширений для UMI.CMS</xsl:text>
			</a>
		</small>
	</xsl:template>

</xsl:stylesheet>