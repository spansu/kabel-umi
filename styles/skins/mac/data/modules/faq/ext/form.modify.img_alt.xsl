<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:php="http://php.net/xsl">

    <xsl:template match="/result/data">
        <link href="/styles/skins/mac/data/modules/content/css/ext_img_alt/edit_modal.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">$.getScript('/styles/skins/mac/data/modules/content/js/ext_img_alt/edit_modal.js')</script>
		
		<form class="form_modify" method="post" action="do/" enctype="multipart/form-data">
			<input type="hidden" name="referer" value="{/result/@referer-uri}" id="form-referer" />
			<input type="hidden" name="domain" value="{$domain-floated}"/>
			<input type="hidden" name="permissions-sent" value="1" />
			<script type="text/javascript">
				var treeLink = function(key, value){
					var settings = SettingsStore.getInstance();
					
					return settings.set(key, value, 'expanded');
				}
			</script>
			<xsl:apply-templates mode="form-modify" />
			<xsl:apply-templates select="page" mode="permissions" />

			<xsl:if test="@action = 'modify' and count(page) = 1">
				<xsl:apply-templates select="document(concat('udata://backup/backup_panel/', page/@id))/udata" />
			</xsl:if>
		</form>
		<script type="text/javascript">
			var method = '<xsl:value-of select="/result/@method" />';
			<![CDATA[
				jQuery('form.form_modify').submit(function(e) {
					var val = true;
					var str = '<div id="errorList"><p class="error"><strong>' + getLabel('js-label-errors-found') + ':</strong></p><ol class="error">';
					var isTextArea = false;
					var editor = null;
					var editorValue = '';
					var isEmptyValue = false;

					jQuery('.required', this).each(function(){
						isTextArea = this.tagName && this.tagName.toLowerCase() === 'textarea';
						if (isTextArea) {
							editor = tinyMCE.get(this.id);

							if (editor && typeof(editor.getContent) === 'function') {
								editorValue = editor.getContent({format: 'text'});
								isEmptyValue = (typeof(editorValue) === 'string' && editorValue.length === 0) ||
											   (editorValue.length === 1 && editorValue.charCodeAt(0) === "\n".charCodeAt(0));
							}
						} else {
							isEmptyValue = (this.value === '');
						}

						if (isEmptyValue) {
							if (val === true) {
								val = false;
							}
							var innerText = jQuery('label[for=' + this.id + '] acronym').text();
							str += '<li>' + getLabel('js-error-required-field') + ' "' + innerText + '".' + '</li>';
						}
					});
					str += '</ol></div>';
					if (val === false) {
						if (jQuery("div").is("#errorList") === false) {
							jQuery('div#page').before(str);
						} else {
							jQuery("#errorList").remove();
							jQuery('div#page').before(str);
						}
						jQuery('body').scrollTop(0);
					}
					return val;
				});
			]]>
		</script>
    </xsl:template>

    <xsl:template match="field[@type = 'img_file']" mode="form-modify">
        <xsl:variable name="filemanager-id" select="document(concat('uobject://',/result/@user-id))/udata//property[@name = 'filemanager']/value/item/@id" />
        <xsl:variable name="filemanager">
            <xsl:choose>
                <xsl:when test="not($filemanager-id)">
                    <xsl:text>elfinder</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="document(concat('uobject://',$filemanager-id))/udata//property[@name = 'fm_prefix']/value" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="is-active" select="document('udata://content/img_alt_active/')/udata"/>
        <xsl:variable name="img-alt-data" select="document(concat('udata://content/getAltTitle/(', @relative-path, ')'))/udata"/>

        <div class="field file" id="{generate-id()}" umi:input-name="{@input_name}"
             umi:field-type="{@type}"
             umi:name="{@name}"
             umi:folder="{@destination-folder}"
             umi:file="{@relative-path}"
             umi:folder-hash="{php:function('elfinder_get_hash', string(@destination-folder))}"
             umi:file-hash="{php:function('elfinder_get_hash', string(@relative-path))}"
             umi:lang="{/result/@interface-lang}"
             umi:filemanager="{$filemanager}">

            <label for="symlinkInput{generate-id()}">
                <span class="label">
                    <acronym>
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:if test="$is-active = 1 and string-length(@relative-path)">
                        <xsl:text> </xsl:text>
                        <a id="link-{@id}" href="#" data-img="{@relative-path}" data-alt="{$img-alt-data/alt}" data-title="{$img-alt-data/title}" class="img-alt-edit-link">Редактировать alt и title</a>
                    </xsl:if>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span id="fileControlContainer_{generate-id()}">

                </span>
            </label>
        </div>
    </xsl:template>


</xsl:stylesheet>