<?php

	if (!defined("CURRENT_WORKING_DIR")) {
		define("CURRENT_WORKING_DIR", str_replace("\\", "/", dirname(dirname(__FILE__))));
	}

	if (!defined('CONFIG_INI_PATH')) {
		define('CONFIG_INI_PATH', CURRENT_WORKING_DIR . '/config.ini');
	}

	if (!function_exists('showWorkTime')) {
		require_once CURRENT_WORKING_DIR . '/libs/root-src/profiler.php';
	}

	if (!class_exists('mainConfiguration')) {
		require_once CURRENT_WORKING_DIR . '/libs/iConfiguration.php';
		require CURRENT_WORKING_DIR . '/libs/configuration.php';
	}

	try {
		$config = mainConfiguration::getInstance();
	} catch (Exception $e) {
		echo 'Critical error: ', $e->getMessage();
		exit;
	}

	$ini = $config->getParsedIni();
	initConfigConstants($ini);

	if (!defined('SYS_KERNEL_PATH')) {
		define("SYS_KERNEL_PATH", $config->includeParam('system.kernel'));
	}
	define("SYS_KERNEL_ASM", $config->includeParam('system.kernel.assebled'));
	define("SYS_LIBS_PATH", $config->includeParam('system.libs'));
	if (!defined('SYS_DEF_MODULE_PATH')) {
		define("SYS_DEF_MODULE_PATH", $config->includeParam('system.default-module') . getCompatibleModulesPath());
	}
	define("SYS_TPLS_PATH", $config->includeParam('templates.tpl'));
	define("SYS_XSLT_PATH", $config->includeParam('templates.xsl'));
	define("SYS_SKIN_PATH", $config->includeParam('templates.skins'));
	define("SYS_ERRORS_PATH", $config->includeParam('system.error'));
	define("SYS_MODULES_PATH", $config->includeParam('system.modules') . getCompatibleModulesPath());
	define("SYS_CACHE_RUNTIME", $config->includeParam('system.runtime-cache'));
	define("SYS_MANIFEST_PATH", $config->includeParam('system.manifest'));
	define("SYS_KERNEL_STREAMS", $config->includeParam('system.kernel.streams'));
	define("KEYWORD_GRAB_ALL", $config->get('kernel', 'grab-all-keyword'));

	$cacheSalt = $config->get('system', 'salt');

	if (!$cacheSalt) {
		$cacheSalt = sha1(rand());
		$config->set('system', 'salt', $cacheSalt);
	}

	define("SYS_CACHE_SALT", $cacheSalt);

	if (!class_exists('umiAutoload')) {
		require_once CURRENT_WORKING_DIR . '/libs/umiAutoload.php';
	}

	spl_autoload_register('umiAutoload::autoload');

	if (isset($classes) && is_array($classes)) {
		addSystemClassesToAutoload([]);
	} else {
		addSystemClassesToAutoload();
	}

	if (!defined('_C_REQUIRES')) {
		require SYS_LIBS_PATH . 'requires.php';
	}

	if (!class_exists('XSLTProcessor')) {
		xslt_fatal();
	}

	// [debug]
	$debug = false;
	if ($config->get('debug', 'enabled')) {
		$ips = $config->get('debug', 'filter.ip');
		if (is_array($ips)) {
			if (in_array(getServer('REMOTE_ADDR'), $ips)) {
				$debug = true;
			}
		} else {
			$debug = true;
		}
	}
	if (!defined('DEBUG')) {
		define('DEBUG', $debug);
	}
	if (!defined('DEBUG_SHOW_BACKTRACE')) {

		$showBacktrace = false;
		$allowedIps = $config->get('debug', 'allowed-ip');
		$allowedIps = is_array($allowedIps) ? $allowedIps : [];
		if ($config->get('debug', 'show-backtrace') && (!count($allowedIps) || in_array(getServer('REMOTE_ADDR'), $allowedIps))) {
			$showBacktrace = true;
		}
		define('DEBUG_SHOW_BACKTRACE', $showBacktrace);
	}

	if (!defined('_C_ERRORS')) {
		require SYS_LIBS_PATH . 'errors.php';
	}

	if ($timezone = $config->get("system", "time-zone")) {
		@date_default_timezone_set($timezone);
	}

	initConfigConnections($ini);

	if (isset($ini['system']['image-compression'])) {
		define("IMAGE_COMPRESSION_LEVEL", $ini['system']['image-compression']);
	} else {
		define("IMAGE_COMPRESSION_LEVEL", 75);
	}

	if (defined("LIBXML_VERSION")) {
		define("DOM_LOAD_OPTIONS", (LIBXML_VERSION < 20621) ? 0 : LIBXML_COMPACT);
	} else {
		define("DOM_LOAD_OPTIONS", LIBXML_COMPACT);
	}
	if (!defined("PHP_INT_MAX")) {
		define("PHP_INT_MAX", 4294967296 / 2 - 1);
	}
	// disable load external entities by default
	//libxml_disable_entity_loader(true);

	if (!isset($_ENV['OS']) || strtolower(substr($_ENV['OS'], 0, 3)) != "win") {
		setlocale(LC_NUMERIC, 'en_US.utf8');
	}

	if (function_exists("mb_internal_encoding")) {
		mb_internal_encoding('UTF-8');
	}

	// system.session-lifetime
	ini_set("session.cookie_lifetime", "0");
	ini_set("session.use_cookies", "1");
	ini_set("session.use_only_cookies", "1");

	// kernel:cluster-cache-correction
	if (CLUSTER_CACHE_CORRECTION) {
		cacheFrontend::getInstance();
		clusterCacheSync::getInstance();
	}

	checkIpAddress($config);

	/**
	 * Проверяет не был ли текущий ip пользователя добавлен в черный список ip адресов.
	 * Если был добавлен, то обратившемуся отдастся белый экран со статусом 403.
	 * Добавить ip в черный список можно либо через справочник "Список IP-адресов, которым недоступен сайт"
	 * (если включена опция "use-ip-blacklist-guide" в config.ini), либо через опцию "ip-blacklist" в config.ini.
	 * Обе управляющие опции расположены в секции "kernel", синтаксис добавления ip адреса в "ip-blacklist":
	 * ip-blacklist = "XXX.XXX.X.XXX,XXX.XXX.X.XXX"
	 * @param mainConfiguration $config
	 * @throws coreException
	 */
	function checkIpAddress(mainConfiguration $config) {
		$remoteIP = getServer('REMOTE_ADDR');
		$blackIps = [];

		$useIpBlacklistGuide = intval($config->get('kernel', 'use-ip-blacklist-guide'));
		$connection = ConnectionPool::getInstance()->getConnection();

		if ($useIpBlacklistGuide === 1) {
			$sql = "SELECT name FROM `cms3_objects` WHERE type_id = (SELECT id FROM `cms3_object_types` WHERE guid='ip-blacklist')";
			$result = $connection->queryResult($sql);
			$result->setFetchType(IQueryResult::FETCH_ARRAY);

			if ($connection->errorOccurred()) {
				throw new coreException($connection->errorDescription($sql));
			}

			foreach ($result as $row) {
				$blackIps[] = array_shift($row);
			}
		}

		$ipList = $config->get('kernel', 'ip-blacklist');
		if (!empty($ipList) && $remoteIP !== null) {
			$ips = explode(",", $ipList);
			$blackIps = array_merge($blackIps, $ips);
		}

		foreach ($blackIps as $id => $blackIp) {
			$blackIp = trim($blackIp);
			if ($blackIp == $remoteIP) {
				$buffer = OutputBuffer::current('HTTPOutputBuffer');
				$buffer->contentType('text/html');
				$buffer->charset('utf-8');
				$buffer->status('403 Forbidden');
				$buffer->clear();
				$buffer->end();
			}
		}
	}

	function initConfigConstants($ini) {
		$defineConstants = [
				'system:db-driver' => ['DB_DRIVER', '%value%'],
				'system:version-line' => ['CURRENT_VERSION_LINE', '%value%'],
				'system:session-lifetime' => ['SESSION_LIFETIME', '%value%'],
				'system:default-date-format' => ['DEFAULT_DATE_FORMAT', '%value%'],
				'kernel:use-reflection-extension' => ['USE_REFLECTION_EXT', '%value%'],
				'kernel:cluster-cache-correction' => ['CLUSTER_CACHE_CORRECTION', '%value%'],
				'kernel:xslt-nested-menu' => ['XSLT_NESTED_MENU', '%value%'],
				'kernel:pages-auto-index' => ['PAGES_AUTO_INDEX', '%value%'],
				'kernel:enable-pre-auth' => ['PRE_AUTH_ENABLED', '%value%'],
				'kernel:ignore-module-names-overwrite' => ['IGNORE_MODULE_NAMES_OVERWRITE', '%value%'],
				'kernel:xml-format-output' => ['XML_FORMAT_OUTPUT', '%value%'],
				'kernel:selection-max-joins' => ['MAX_SELECTION_TABLE_JOINS', '%value%'],
				'kernel:property-value-mode' => ['XML_PROP_VALUE_MODE', '%value%'],
				'kernel:xml-macroses-disable' => ['XML_MACROSES_DISABLE', '%value%'],
				'kernel:selection-calc-found-rows-disable' => ['DISABLE_CALC_FOUND_ROWS', '%value%'],
				'kernel:sql-query-cache' => ['SQL_QUERY_CACHE', '%value%'],
				'seo:calculate-e-tag' => ['CALC_E_TAG', '%value%'],
				'seo:calculate-last-modified' => ['CALC_LAST_MODIFIED', '%value%']
		];

		foreach ($defineConstants as $name => $const) {
			list($section, $variable) = explode(':', $name);
			$value = $const[1];

			if (is_string($value)) {
				$iniValue = isset($ini[$section][$variable]) ? $ini[$section][$variable] : "";
				$value = str_replace('%value%', $iniValue, $value);
			} else {
				if (!$value && isset($const[2])) {
					$value = $const[2];
				}
			}

			if (!defined($const[0])) {
				if ($const[0] == 'CURRENT_VERSION_LINE' && !$value) {
					continue;
				}
				define($const[0], $value);
			}
		}
	}

	function initConfigConnections($ini) {
		$connections = [];

		foreach ($ini['connections'] as $name => $value) {
			list($class, $pname) = explode('.', $name);
			if (!isset($connections[$class])) {
				$connections[$class] = [
						'type' => 'mysql',
						'host' => 'localhost',
						'login' => 'root',
						'password' => '',
						'dbname' => 'umi',
						'port' => false,
						'persistent' => false,
						'compression' => false];
			}
			$connections[$class][$pname] = $value;
		}

		$pool = ConnectionPool::getInstance();

		foreach ($connections as $class => $con) {
			$mysqlApi = 'mysqli';
			if (version_compare(phpversion(), '7.0.0', '<')) {
				$mysqlApi = (isset($con['api']) && $con['api'] == 'mysqli') ? 'mysqli' : 'mysql';
			}
			$mysqlApiClassName = $mysqlApi . 'Connection';
			$pool->setConnectionObjectClass($mysqlApiClassName);

			if ($con['dbname'] == '-=demo=-' || $con['dbname'] == '-=custom=-') {
				if ($con['dbname'] == '-=demo=-') {
					require './demo-center.php';
				}

				$con['host'] = MYSQL_HOST;
				$con['login'] = MYSQL_LOGIN;
				$con['password'] = MYSQL_PASSWORD;
				$con['dbname'] = ($con['dbname'] == '-=custom=-') ? MYSQL_DB_NAME : DEMO_DB_NAME;
			}

			$pool->addConnection($class, $con['host'], $con['login'], $con['password'], $con['dbname'],
					($con['port'] !== false) ? $con['port'] : false,
					(bool) (int) $con['persistent']);
		}

		if (DB_DRIVER == "mysql") {
			$connection = ConnectionPool::getInstance()->getConnection();
			ini_set('mysql.trace_mode', false);

			$config = mainConfiguration::getInstance();

			if ($config->get('kernel', 'mysql-queries-log-enable')) {
				$logType = $config->get('kernel', 'mysql-queries-log-type');
				$mysqlLoggerCreator = MysqlLoggerCreator::getInstance();
				$mysqlLogger = $mysqlLoggerCreator->createMysqlLogger($logType, $config);
				/* @var mysqlConnection $connection */
				$connection->setLogger($mysqlLogger);
			}
		}
	}

	/**
	 * @deprecated
	 */
	function mysql_fatal() {
		header(':', true, 503);
		require "./errors/mysql_failed.html";
		exit();
	}

	function xslt_fatal() {
		header(':', true, 503);
		require("./errors/xslt_failed.html");
		exit();
	}

	/**
	 * Читаем настройки переданные в fastcgi_param от сервера и заменяет ими прочитанные из config.ini
	 * собавляет новые которых нет в файле конфигурации
	 * @param $ini
	 * @return mixed
	 */
	function getParamFromFastcgiParams($ini) {
		foreach ($_SERVER as $key => $val) {
			if (strpos($key, 'UMI_') !== false) {
				$key = str_replace('UMI_', '', $key);
				$key = str_replace('_', '.', $key);
				$key = explode('.', $key, 2);
				$ini[$key[0]][$key[1]] = $val;
			}
		}
		return $ini;
	}

	/**
	 * Возвращает путь до директории с совместимыми модулями
	 * @return string путь до директории или пустая строка
	 */
	function getCompatibleModulesPath() {
		$config = mainConfiguration::getInstance();
		$isCompatible = (bool) $config->get('system', 'compatible-modules');

		if (version_compare(phpversion(), '7.0.0', '>=')) {
			$isCompatible = true;
		}

		$modulesPath = '../components';
		return ($isCompatible ? $modulesPath . DIRECTORY_SEPARATOR : '');
	}

	/**
	 * Добавить в автозагрузку системные классы и классы, заданные пользователем в config.ini
	 * @param array $classes = null добавляемые классы, если не передано - загрузит из autoload.php
	 */
	function addSystemClassesToAutoload(array $classes = null) {
		if ($classes === null) {
			$classes = [];
			require SYS_LIBS_PATH . 'autoload.php';
		}

		$umiConfigs = mainConfiguration::getInstance();
		$classesToLoad = $umiConfigs->getList('autoload');

		if (is_array($classesToLoad) && count($classesToLoad) > 0) {
			foreach ($classesToLoad as $classToLoad) {
				if (isset($classes[$classToLoad])) {
					continue;
				}

				$classFilesPath = $umiConfigs->get('autoload', $classToLoad);
				$filesToLoad = [];

				if (!is_array($classFilesPath) || count($classFilesPath) == 0) {
					continue;
				}

				foreach ($classFilesPath as $classFilePath) {
					if (substr($classFilePath, 0, 2) == "~/") {
						$classFilePath = CURRENT_WORKING_DIR . substr($classFilePath, 1);
					}

					$filesToLoad[] = $classFilePath;
				}

				$classes[$classToLoad] = $filesToLoad;
			}
		}

		umiAutoload::addClassesToAutoload($classes);
	}
