<?php
	$classes = [
		'regedit' => [
			SYS_KERNEL_PATH . 'subsystems/regedit/iRegedit.php',
			SYS_KERNEL_PATH . 'subsystems/regedit/regedit.php'
		],

		'searchModel' => [
			SYS_KERNEL_PATH . 'subsystems/models/search/iSearchModel.php',
			SYS_KERNEL_PATH . 'subsystems/models/search/searchModel.php'
		],

		'FilterIndexGenerator' => [
			SYS_KERNEL_PATH . 'subsystems/models/filter/FilterIndexGenerator.php'
		],

		'FilterQueriesMaker' => [
			SYS_KERNEL_PATH . 'subsystems/models/filter/FilterQueriesMaker.php'
		],

		'permissionsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/permissions/iPermissionsCollection.php',
			SYS_KERNEL_PATH . 'subsystems/models/permissions/permissionsCollection.php'
		],

		'ranges' => [
			SYS_KERNEL_PATH . 'utils/ranges/ranges.php'
		],

		'translit' => [
			SYS_KERNEL_PATH . 'utils/translit/iTranslit.php',
			SYS_KERNEL_PATH . 'utils/translit/translit.php'
		],

		'dbSchemeConverter' => [
			SYS_KERNEL_PATH . 'utils/dbSchemeConverter/iDbSchemeConverter.php',
			SYS_KERNEL_PATH . 'utils/dbSchemeConverter/dbSchemeConverter.php'
		],

		'umiTarCreator' => [
			SYS_KERNEL_PATH . 'utils/tar/umiTarCreator.php'
		],

		'umiTarExtracter' => [
			SYS_KERNEL_PATH . 'utils/tar/umiTarExtracter.php'
		],

		'adminModuleTabs' => [
			SYS_KERNEL_PATH . 'utils/adminModuleTabs/iAdminModuleTabs.php',
			SYS_KERNEL_PATH . 'utils/adminModuleTabs/adminModuleTabs.php'
		],

		'umiTemplater' => [
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/umiTemplater.php',
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/IFullResult.php',
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/IPhpExtension.php',
		],

		'umiTemplaterXSLT' => [
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/types/umiTemplaterXSLT.php'
		],

		'umiTemplaterTPL' => [
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/types/umiTemplaterTPL.php'
		],

		'umiTemplaterPHP' => [
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/types/umiTemplaterPHP.php',
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/engine/PhpTemplateEngine.php',
			SYS_KERNEL_PATH . 'subsystems/umiTemplaters/engine/ViewPhpExtension.php'
		],

		'templater' => [
			SYS_KERNEL_PATH . 'subsystems/templaters/iTemplater.php',
			SYS_KERNEL_PATH . 'subsystems/templaters/templater.php'
		],

		'tplTemplater' => [
			SYS_KERNEL_PATH . 'subsystems/templaters/tpls/tplTemplater.php'
		],

		'xslTemplater' => [
			SYS_KERNEL_PATH . 'subsystems/templaters/xslt/xslTemplater.php'
		],

		'xslAdminTemplater' => [
			SYS_KERNEL_PATH . 'subsystems/templaters/xslt/xslAdminTemplater.php'
		],

		'cmsController' => [
			SYS_KERNEL_PATH . 'subsystems/cmsController/iCmsController.php',
			SYS_KERNEL_PATH . 'subsystems/cmsController/cmsController.php'
		],

		'umiExporter' => [
			SYS_KERNEL_PATH . 'subsystems/export/iUmiExporter.php',
			SYS_KERNEL_PATH . 'subsystems/export/umiExporter.php'
		],

		'xmlExporter' => [
			SYS_KERNEL_PATH . 'subsystems/export/iXmlExporter.php',
			SYS_KERNEL_PATH . 'subsystems/export/xmlExporter.php'
		],

		'umiXmlExporter' => [
			SYS_KERNEL_PATH . 'subsystems/export/iUmiXmlExporter.php',
			SYS_KERNEL_PATH . 'subsystems/export/umiXmlExporter.php'
		],

		'umiXmlImporter' => [
			SYS_KERNEL_PATH . 'subsystems/import/iUmiXmlImporter.php',
			SYS_KERNEL_PATH . 'subsystems/import/umiXmlImporter.php'
		],

		'xmlImporter' => [
			SYS_KERNEL_PATH . 'subsystems/import/iXmlImporter.php',
			SYS_KERNEL_PATH . 'subsystems/import/xmlImporter.php'
		],

		'umiImportRelations' => [
			SYS_KERNEL_PATH . 'subsystems/import/iUmiImportRelations.php',
			SYS_KERNEL_PATH . 'subsystems/import/umiImportRelations.php'
		],

		'umiImportSplitter' => [
			SYS_KERNEL_PATH . 'subsystems/import/iUmiImportSplitter.php',
			SYS_KERNEL_PATH . 'subsystems/import/umiImportSplitter.php'
		],

		'language_morph' => [
			SYS_KERNEL_PATH . 'utils/languageMorph/iLanguageMorph.php',
			SYS_KERNEL_PATH . 'utils/languageMorph/language_morph.php'
		],

		'umiDate' => [
			SYS_KERNEL_PATH . 'entities/umiDate/iUmiDate.php',
			SYS_KERNEL_PATH . 'entities/umiDate/umiDate.php'
		],

		'umiFile' => [
			SYS_KERNEL_PATH . 'entities/umiFile/iUmiFile.php',
			SYS_KERNEL_PATH . 'entities/umiFile/umiFile.php'
		],

		'umiImageFile' => [
			SYS_KERNEL_PATH . 'entities/umiFile/iUmiImageFile.php',
			SYS_KERNEL_PATH . 'entities/umiFile/umiImageFile.php'
		],

		'umiRemoteFileGetter' => [
			SYS_KERNEL_PATH . 'entities/umiFile/umiRemoteFileGetter.php'
		],

		'umiDirectory' => [
			SYS_KERNEL_PATH . 'entities/umiDirectory/iUmiDirectory.php',
			SYS_KERNEL_PATH . 'entities/umiDirectory/umiDirectory.php'
		],

		'umiMail' => [
			SYS_KERNEL_PATH . 'entities/umiMail/interfaces.php',
			SYS_KERNEL_PATH . 'entities/umiMail/umiMail.php',
			SYS_KERNEL_PATH . 'entities/umiMail/umiMimePart.php'
		],

		'umiBasket' => [
			SYS_KERNEL_PATH . 'utils/basket/iUmiBasket.php',
			SYS_KERNEL_PATH . 'utils/basket/umiBasket.php'
		],

		'umiPagenum' => [
			SYS_KERNEL_PATH . 'utils/pagenum/iPagenum.php',
			SYS_KERNEL_PATH . 'utils/pagenum/pagenum.php'
		],

		'umiCaptcha' => [
			SYS_KERNEL_PATH . 'utils/captcha/iUmiCaptcha.php',
			SYS_KERNEL_PATH . 'utils/captcha/umiCaptcha.php'
		],

		'lang' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/lang.php'
		],

		'langsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/langsCollection.php'
		],

		'domain' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/domainMirrow.php',
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/domain.php'
		],

		'domainsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/domainsCollection.php'
		],

		'template' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/template.php'
		],

		'templatesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/templatesCollection.php'
		],

		'umiHierarchyType' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/umiHierarchyType.php'
		],

		'umiHierarchyTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/umiHierarchyTypesCollection.php'
		],

		'umiHierarchyElement' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/umiHierarchyElement.php'
		],

		'umiHierarchy' => [
			SYS_KERNEL_PATH . 'subsystems/models/hierarchy/umiHierarchy.php'
		],

		'umiSelection' => [
			SYS_KERNEL_PATH . 'subsystems/models/selection/iUmiSelection.php',
			SYS_KERNEL_PATH . 'subsystems/models/selection/umiSelection.php'
		],

		'umiSelectionsParser' => [
			SYS_KERNEL_PATH . 'subsystems/models/selection/iUmiSelectionsParser.php',
			SYS_KERNEL_PATH . 'subsystems/models/selection/umiSelectionsParser.php'
		],

		'umiFieldType' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiFieldType.php'
		],

		'umiField' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiField.php'
		],

		'umiFieldsGroup' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiFieldsGroup.php'
		],

		'umiObjectType' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObjectType.php'
		],

		'umiTypesHelper' => [
			SYS_KERNEL_PATH . 'subsystems/helpers/umiTypesHelper.php'
		],

		'umiLinksHelper' => [
			SYS_KERNEL_PATH . 'subsystems/helpers/umiLinksHelper.php'
		],

		'umiPropertiesHelper' => [
			SYS_KERNEL_PATH . 'subsystems/helpers/umiPropertiesHelper.php'
		],

		'ClassConfig' => [
			SYS_KERNEL_PATH . 'subsystems/helpers/ClassConfig.php'
		],

		'umiObjectProperty' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObjectProperty.php'
		],

		'umiObject' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObject.php'
		],

		'umiFieldTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiFieldTypesCollection.php'
		],

		'umiFieldsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiFieldsCollection.php'
		],

		'umiObjectTypesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObjectTypesCollection.php'
		],

		'umiObjectsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiObjectsCollection.php'
		],

		'cacheFrontend' => [
			SYS_KERNEL_PATH . 'subsystems/cache/iCacheFrontend.php',
			SYS_KERNEL_PATH . 'subsystems/cache/cacheFrontend.php'
		],

		'umiEventPoint' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/umiEventPoint.php'
		],

		'umiEventListener' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/umiEventListener.php'
		],

		'umiEventsController' => [
			SYS_KERNEL_PATH . 'subsystems/models/events/umiEventsController.php'
		],

		'umiEventFeed' => [
			SYS_KERNEL_PATH . 'subsystems/models/eventFeed/umiEventFeed.php'
		],

		'umiEventFeedType' => [
			SYS_KERNEL_PATH . 'subsystems/models/eventFeed/umiEventFeedType.php'
		],

		'umiEventFeedUser' => [
			SYS_KERNEL_PATH . 'subsystems/models/eventFeed/umiEventFeedUser.php'
		],

		'backupModel' => [
			SYS_KERNEL_PATH . 'subsystems/models/backup/iBackupModel.php',
			SYS_KERNEL_PATH . 'subsystems/models/backup/backupModel.php',
			SYS_KERNEL_PATH . 'subsystems/models/backup/backupChange.php'
		],

		'xmlTranslator' => [
			SYS_KERNEL_PATH . 'utils/translators/xmlTranslator.php'
		],

		'jsonTranslator' => [
			SYS_KERNEL_PATH . 'utils/translators/jsonTranslator.php'
		],

		'baseModuleAdmin' => [
			SYS_DEF_MODULE_PATH . 'baseModuleAdmin.php'
		],

		'matches' => [
			SYS_KERNEL_PATH . 'subsystems/matches/iMatches.php',
			SYS_KERNEL_PATH . 'subsystems/matches/matches.php'
		],

		'baseSerialize' => [
			SYS_KERNEL_PATH . 'subsystems/matches/serializes/iBaseSerialize.php',
			SYS_KERNEL_PATH . 'subsystems/matches/serializes/baseSerialize.php'
		],

		'RSSFeed' => [
			SYS_KERNEL_PATH . 'entities/RSS/interfaces.php',
			SYS_KERNEL_PATH . 'entities/RSS/RSSFeed.php',
			SYS_KERNEL_PATH . 'entities/RSS/RSSItem.php'
		],

		'umiCron' => [
			SYS_KERNEL_PATH . 'subsystems/cron/iUmiCron.php',
			SYS_KERNEL_PATH . 'subsystems/cron/umiCron.php'
		],

		'umiMessages' => [
			SYS_KERNEL_PATH . 'subsystems/messages/umiMessages.php'
		],

		'umiMessage' => [
			SYS_KERNEL_PATH . 'subsystems/messages/umiMessage.php'
		],

		'umiSubscriber' => [
			SYS_KERNEL_PATH . 'utils/subscriber/iUmiSubscriber.php',
			SYS_KERNEL_PATH . 'utils/subscriber/umiSubscriber.php'
		],

		'umiOpenSSL' => [
			SYS_KERNEL_PATH . 'utils/openssl/iUmiOpenSSL.php',
			SYS_KERNEL_PATH . 'utils/openssl/umiOpenSSL.php'
		],

		'umiAuth' => [
			SYS_KERNEL_PATH . 'subsystems/models/permissions/iUmiAuth.php',
			SYS_KERNEL_PATH . 'subsystems/models/permissions/umiAuth.php'
		],

		'umiLogger' => [
			SYS_KERNEL_PATH . 'utils/logger/iLogger.php',
			SYS_KERNEL_PATH . 'utils/logger/logger.php'
		],

		'umiConversion' => [
			SYS_KERNEL_PATH . 'utils/conversion/umiConversion.php',
			SYS_KERNEL_PATH . 'utils/conversion/iGenericConversion.php'
		],

		'__charssequences_ru' => [
			SYS_KERNEL_PATH . 'utils/conversion/__charssequences.ru.php'
		],

		'garbageCollector' => [
			SYS_KERNEL_PATH . 'subsystems/garbageCollector/iGarbageCollector.php',
			SYS_KERNEL_PATH . 'subsystems/garbageCollector/garbageCollector.php'
		],

		'manifest' => [
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/interfaces.php',
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/manifest.php',
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/sampleCallback.php',
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/jsonCallback.php'
		],

		'transaction' => [
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/transaction.php'
		],

		'atomicAction' => [
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/atomicAction.php'
		],

		'umiBranch' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/umiBranch.php'
		],

		'umiObjectPropertyBoolean' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyBoolean.php'
		],

		'umiObjectPropertyImgFile' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyImgFile.php'
		],

		'umiObjectPropertyRelation' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyRelation.php'
		],

		'umiObjectPropertyTags' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyTags.php'
		],

		'umiObjectPropertyDate' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyDate.php'
		],

		'umiObjectPropertyInt' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyInt.php'
		],

		'umiObjectPropertyString' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyString.php'
		],

		'umiObjectPropertyText' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyText.php'
		],

		'umiObjectPropertyFile' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyFile.php'
		],

		'umiObjectPropertyPassword' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyPassword.php'
		],

		'umiObjectPropertyWYSIWYG' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyWYSIWYG.php'
		],

		'umiObjectPropertyFloat' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyFloat.php'
		],

		'umiObjectPropertyPrice' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyPrice.php'
		],

		'umiObjectPropertySymlink' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertySymlink.php'
		],

		'umiObjectPropertyCounter' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyCounter.php'
		],

		'umiObjectPropertyOptioned' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyOptioned.php'
		],

		'umiObjectPropertyColor' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyColor.php'
		],

		'umiObjectPropertyLinkToObjectType' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyLinkToObjectType.php'
		],

		'umiObjectPropertyMultipleImgFile' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/properties/umiObjectPropertyMultipleImgFile.php'
		],

		'baseXmlConfig' => [
			SYS_KERNEL_PATH . 'utils/xml/config/iBaseXmlConfig.php',
			SYS_KERNEL_PATH . 'utils/xml/config/baseXmlConfig.php'
		],

		'quickCsvExporter' => [
			SYS_KERNEL_PATH . 'utils/csv/export/quickCsvExporter.php'
		],

		'quickCsvImporter' => [
			SYS_KERNEL_PATH . 'utils/csv/import/quickCsvImporter.php'
		],

		'baseRestriction' => [
			SYS_KERNEL_PATH . 'subsystems/models/data/baseRestriction.php'
		],

		'redirects' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/iRedirects.php',
			SYS_KERNEL_PATH . 'subsystems/redirects/redirects.php'
		],

		'umiBaseStream' => [
			SYS_KERNEL_PATH . 'subsystems/streams/iUmiBaseStream.php',
			SYS_KERNEL_PATH . 'subsystems/streams/umiBaseStream.php'
		],

		'clusterCacheSync' => [
			SYS_KERNEL_PATH . 'subsystems/cacheSync/clusterCacheSync.php'
		],

		'umiObjectProxy' => [
			SYS_KERNEL_PATH . 'patterns/umiObjectProxy.php'
		],

		'umiObjectsExpiration' => [
			SYS_KERNEL_PATH . 'subsystems/expirations/iUmiObjectsExpiration.php',
			SYS_KERNEL_PATH . 'subsystems/expirations/umiObjectsExpiration.php'
		],

		'HTTPOutputBuffer' => [
			SYS_KERNEL_PATH . 'subsystems/buffers/HTTPOutputBuffer.php'
		],

		'HTTPDocOutputBuffer' => [
			SYS_KERNEL_PATH . 'subsystems/buffers/HTTPDocOutputBuffer.php'
		],

		'CLIOutputBuffer' => [
			SYS_KERNEL_PATH . 'subsystems/buffers/CLIOutputBuffer.php'
		],

		'objectProxyHelper' => [
			SYS_KERNEL_PATH . 'patterns/objectProxyHelper.php'
		],

		'antiSpamService' => [
			SYS_KERNEL_PATH . 'utils/antispam/antiSpamService.php'
		],

		'antiSpamHelper' => [
			SYS_KERNEL_PATH . 'utils/antispam/antiSpamHelper.php'
		],

		'alphabeticalIndex' => [
			SYS_KERNEL_PATH . 'utils/indexes/alphabetical/alphabeticalIndex.php'
		],

		'calendarIndex' => [
			SYS_KERNEL_PATH . 'utils/indexes/calendar/calendarIndex.php'
		],

		'selector' => [
			SYS_KERNEL_PATH . 'subsystems/selector/selector.php',
			SYS_KERNEL_PATH . 'subsystems/selector/where.php',
			SYS_KERNEL_PATH . 'subsystems/selector/types.php',
			SYS_KERNEL_PATH . 'subsystems/selector/order.php',
			SYS_KERNEL_PATH . 'subsystems/selector/executor.php',
			SYS_KERNEL_PATH . 'subsystems/selector/getter.php',
			SYS_KERNEL_PATH . 'subsystems/selector/options.php',
			SYS_KERNEL_PATH . 'subsystems/selector/group.php'
		],

		'selectorHelper' => [
			SYS_KERNEL_PATH . 'subsystems/selector/helper.php'
		],

		'loginzaAPI' => [
			SYS_KERNEL_PATH . '/utils/loginza/loginzaAPI.class.php'
		],

		'loginzaUserProfile' => [
			SYS_KERNEL_PATH . '/utils/loginza/loginzaUserProfile.class.php'
		],

		'session' => [
			SYS_KERNEL_PATH . '/subsystems/session/interfaces.php',
			SYS_KERNEL_PATH . '/subsystems/session/session.php'
		],

		'idna_convert' => [
			SYS_KERNEL_PATH . '/utils/idnaConverter/idna_convert.class.php'
		],

		'systemInfo' => [
			SYS_KERNEL_PATH . 'subsystems/regedit/systemInfo.php'
		],

		'imageUtils' => [
			SYS_KERNEL_PATH . '/utils/imageUtils/imageUtils.php',
			SYS_KERNEL_PATH . '/utils/imageUtils/iImageProcessor.php',
			SYS_KERNEL_PATH . '/utils/imageUtils/imageMagickProcessor.php',
			SYS_KERNEL_PATH . '/utils/imageUtils/gdProcessor.php',

		],

		'MysqlLoggerCreator' => [
			SYS_KERNEL_PATH . 'utils/logger/mysql/iMysqlLoggerCreator.php',
			SYS_KERNEL_PATH . 'utils/logger/mysql/iMysqlLogger.php',
			SYS_KERNEL_PATH . 'utils/logger/mysql/MysqlLoggerCreator.php'
		],

		'mysqliConnection' => [
			SYS_KERNEL_PATH . 'subsystems/database/mysqliConnection.php',
			SYS_KERNEL_PATH . 'subsystems/database/mysqliQueryResult.php',
			SYS_KERNEL_PATH . 'subsystems/database/mysqliQueryResultIterator.php'
		],

		'jsonManifestCallback' => [
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/interfaces.php',
			SYS_KERNEL_PATH . 'subsystems/manifest/classes/jsonCallback.php',
		],

		'SphinxClient' => [
			SYS_KERNEL_PATH . 'subsystems/models/search/sphinx/sphinxapi.php',
		],

		'SphinxIndexGenerator' => [
			SYS_KERNEL_PATH . 'subsystems/models/search/sphinx/SphinxIndexGenerator.php',
		],

		'UmiZipArchive' => [
			SYS_KERNEL_PATH . 'utils/UmiZipArchive/IUmiZipArchive.php',
			SYS_KERNEL_PATH . 'utils/UmiZipArchive/UmiZipArchive.php'
		],

		'BrowserDetect' => [
			SYS_KERNEL_PATH . 'utils/browser/Browser.php'
		],

		'iUmiBufferWorker' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiBufferWorker.php'
		],

		'iUmiCollection' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiCollection.php'
		],

		'iUmiCollectionItem' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiCollectionItem.php'
		],

		'iUmiConfigWorker' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiConfigWorker.php'
		],

		'iUmiDataBaseWorker' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiDataBaseWorker.php'
		],

		'iUmiService' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiService.php'
		],

		'iClassConfigManager' => [
			SYS_KERNEL_PATH . 'interfaces/iClassConfigManager.php'
		],

		'iClassConfig' => [
			SYS_KERNEL_PATH . 'interfaces/iClassConfig.php'
		],

		'iRedirects' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/iRedirects.php'
		],

		'iUmiRedirect' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/iUmiRedirect.php'
		],

		'umiRedirect' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/umiRedirect.php'
		],

		'umiRedirectsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/redirects/umiRedirectsCollection.php'
		],

		'AbstractReference' => [
			SYS_KERNEL_PATH . 'subsystems/services/AbstractReference.php'
		],

		'ParameterReference' => [
			SYS_KERNEL_PATH . 'subsystems/services/ParameterReference.php'
		],

		'ServiceReference' => [
			SYS_KERNEL_PATH . 'subsystems/services/ServiceReference.php'
		],

		'umiServiceContainer' => [
			SYS_KERNEL_PATH . 'subsystems/services/umiServiceContainer.php'
		],

		'umiServiceContainers' => [
			SYS_KERNEL_PATH . 'subsystems/services/umiServiceContainers.php'
		],

		'tUmiDataBaseWorker' => [
			SYS_KERNEL_PATH . 'traits/tUmiDataBaseWorker.php'
		],

		'tUmiService' => [
			SYS_KERNEL_PATH . 'traits/tUmiService.php'
		],

		'tUmiBufferWorker' => [
			SYS_KERNEL_PATH . 'traits/tUmiBufferWorker.php'
		],

		'tUmiConfigWorker' => [
			SYS_KERNEL_PATH . 'traits/tUmiConfigWorker.php'
		],

		'tCommonCollectionItem' => [
			SYS_KERNEL_PATH . 'traits/tCommonCollectionItem.php'
		],

		'tClassConfigManager' => [
			SYS_KERNEL_PATH . 'traits/tClassConfigManager.php'
		],

		'tCommonCollection' => [
			SYS_KERNEL_PATH . 'traits/tCommonCollection.php'
		],

		'AppointmentService' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentService.php'
		],

		'iAppointmentService' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentService.php'
		],

		'AppointmentServiceGroup' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentServiceGroup.php'
		],

		'iAppointmentServiceGroup' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentServiceGroup.php'
		],

		'AppointmentEmployee' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentEmployee.php'
		],

		'iAppointmentEmployee' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentEmployee.php'
		],

		'AppointmentEmployeeService' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentEmployeeService.php'
		],

		'iAppointmentEmployeeService' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentEmployeeService.php'
		],

		'AppointmentEmployeeSchedule' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentEmployeeSchedule.php'
		],

		'iAppointmentEmployeeSchedule' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentEmployeeSchedule.php'
		],

		'AppointmentOrder' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/items/AppointmentOrder.php'
		],

		'iAppointmentOrder' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/interfaces/iAppointmentOrder.php'
		],

		'AppointmentServicesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentServicesCollection.php'
		],

		'AppointmentServiceGroupsCollection' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentServiceGroupsCollection.php'
		],

		'AppointmentEmployeesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentEmployeesCollection.php'
		],

		'AppointmentEmployeesServicesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentEmployeesServicesCollection.php'
		],

		'AppointmentEmployeesSchedulesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentEmployeesSchedulesCollection.php'
		],

		'AppointmentOrdersCollection' => [
			SYS_KERNEL_PATH . 'subsystems/appointment/collections/AppointmentOrdersCollection.php'
		],

		'MailTemplatesCollection' => [
			SYS_KERNEL_PATH . 'subsystems/mailTemplates/MailTemplatesCollection.php'
		],

		'MailTemplate' => [
			SYS_KERNEL_PATH . 'subsystems/mailTemplates/MailTemplate.php'
		],

		'staticCache' => [
			CURRENT_WORKING_DIR . '/libs/cacheControl.php'
		],

		'iUmiMap' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiMap.php'
		],

		'iUmiMapWorker' => [
			SYS_KERNEL_PATH . 'interfaces/iUmiMapWorker.php'
		],

		'tCommonMap' => [
			SYS_KERNEL_PATH . 'traits/tCommonMap.php'
		],

		'tUmiMapWorker' => [
			SYS_KERNEL_PATH . 'traits/tUmiMapWorker.php'
		],

		'baseUmiCollectionMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/baseUmiCollectionMap.php'
		],

		'umiRedirectsMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/umiRedirectsMap.php'
		],

		'tableControlMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/tableControlMap.php'
		],

		'appointmentEmployeesMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/appointmentEmployeesMap.php'
		],

		'appointmentEmployeesSchedulesMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/appointmentEmployeesSchedulesMap.php'
		],

		'appointmentEmployeesServicesMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/appointmentEmployeesServicesMap.php'
		],

		'appointmentOrdersMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/appointmentOrdersMap.php'
		],

		'appointmentServiceGroupsMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/appointmentServiceGroupsMap.php'
		],

		'appointmentServicesMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/appointmentServicesMap.php'
		],

		'mailTemplatesMap' => [
			SYS_KERNEL_PATH . 'subsystems/mappings/mailTemplatesMap.php'
		]
	];
