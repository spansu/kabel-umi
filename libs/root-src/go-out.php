<?php
	require './libs/config.php';

	$url = getRequest('url');
	$host = getServer('HTTP_HOST') ? str_replace('www.', '', getServer('HTTP_HOST')) : false;
	$referer = getServer('HTTP_REFERER') ? parse_url(getServer('HTTP_REFERER')) : false;

	$refererHost = false;
	if ($referer && isset($referer['host'])) {
		$refererHost = $referer['host'];
	}

	if (!$url || !$refererHost || !$host || strpos($refererHost, $host) === false) {
		header("HTTP/1.0 404 Not Found");
		exit();
	}

	header("Content-type: text/html; charset=utf8");
	header('Location:' . $url);
	exit();
?>