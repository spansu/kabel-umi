/**
 * Extend panel for cloud controller
 */
(function (){

	// кастомы применяются только на облачном контроллере
	if (!window.cloudController) return;
	if (!window.cloudController.onController) return;

	// меняем структуру панели
	uAdmin('drawControls', function () {
		this.exitButton = this.addExitButton();
		this.helpButton = this.addHelpButton();
		this.butterfly = this.addButtrfly();
		this.eipHolder = this.addEipHolder();
		this.lastDoc = this.addLastDoc();
		this.changelogDd = this.addChangelogDd();
		this.note = this.addNote();
		this.metaHolder = this.addMetaHolder();
	}, 'panel');

	// заменяем кнопку "Модули"
	uAdmin('addButtrfly', function () {
		if (!this.quickpanel.length) return null;
		return jQuery('<a id="adminzone" href="/adminzone/" title="Административная панель"><span class="in_ico_bg">&nbsp;</span>Главная</a>')
			.appendTo(this.quickpanel);
	}, 'panel');

	// переделываем уже созданные элементы
	uAdmin.onLoad('panel', function () {

		// меняем кнопку помощи
		uAdmin.panel.helpButton.unbind('click');
		uAdmin.panel.helpButton.bind('click', function() {
			window.open(window.cloudController.helpPageLink || "http://help-cms.ru/");
			return false;
		});

		// меняем ссылку выхода
		uAdmin.panel.exitButton.unbind('click');
		uAdmin.panel.exitButton.bind('click', function() {
			window.location = "/adminzone/exit/";
			return false;
		});

	});

})();